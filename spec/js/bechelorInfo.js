describe('Informacje o pracy dyplomowej', function () {

    var popupMock = {
        name: [ "Filmy", "Test" ],
        fetch: function () {
            _.defer(function () {
            });
        }
    };

    it('Pobrano logo', function () {
        var logoDownloaded = false;

        setTimeout(function () {
            logoDownloaded = $('.bechelor-img').attr('src') !== '';
        }, 1000);

        waitsFor(function() {
            return logoDownloaded;
        }, "Element nie został wyświetlony", 1500);

        runs(function () {
            expect(logoDownloaded).toBe(true);
        });     
    });

    it('Dane są widoczne', function () {
        var isVisible = false;
        setTimeout(function () {
            isVisible = !!($('.bechelor-info').css('visibility') == 'visible');
        }, 1000);

        waitsFor(function() {
            return isVisible;
        }, "Element nie został wyświetlony", 1500);

        runs(function () {
            expect(isVisible).toBe(true);
        });   
    });

    it('Przycisk jest aktywny', function () {
        var isActive = false;
        setTimeout(function () {
            isActive = !!$('.bechelor-button').hasClass('active');
        }, 1000);

        waitsFor(function() {
            return isActive;
        }, "Przycisk nie jest aktywny", 1500);

        runs(function () {
            expect(isActive).toBe(true);
        });   
    });
});