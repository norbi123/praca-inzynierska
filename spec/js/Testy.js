describe('Testz', function () {
    var component;

    var popupMock = {
        name: [ "Polski", "Matematyka", "Programowanie", "Analiza", "Historia", "Bazy danych", "Automatyka" ],
        fetch: function () {
            _.defer(function () {
            });
        }
    };
    afterEach(function () {
        component = null;
    });

    it('Uruchamiają się', function () {
        setTimeout(function () {
            var isVisible = !!$('.subject').css('visibility');
            expect(isVisible).toBe(true);
        }, 1000);

    });

    it('Działa zaznaczanie odpowiedzi', function () {
        setTimeout(function () {
            var hasText = !!$('.subject0').text();
            expect(hasText).toBe(true);
        }, 1000);

    });

    it('Działa sprawdzanie poprawności odpowiedzi', function () {
        setTimeout(function () {
            var hasText = !!$('.subject0').text();
            expect(hasText).toBe(true);
        }, 1000);
    });

    it('Działa obliczanie wyniku testu', function () {
        setTimeout(function () {
            var hasText = !!$('.subject0').text();
            expect(hasText).toBe(true);
        }, 1000);

    });
});