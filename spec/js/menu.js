describe('Menu', function () {

    var popupMock = {
        name: [ "Filmy", "Test" ],
        fetch: function () {
            _.defer(function () {
            });
        }
    };

    it('Zawiera elementy', function () {
        var hasElements = false;

        setTimeout(function () {
            hasElements = !!$('.menu').children();
        }, 1000);

        waitsFor(function() {
            return hasElements;
        }, "Komponent nie zawiera elementów", 1500);

        runs(function () {
            expect(hasElements).toBe(true);
        });   
    });

    it('Jest widoczne', function () {
        var isVisible = false;

        setTimeout(function () {
            isVisible = !!($('.menu').css('visibility') == 'visible');
        }, 1000);

        waitsFor(function() {
            return isVisible;
        }, "Komponent nie jest wyświetlony", 1500);

        runs(function () {
            expect(isVisible).toBe(true);
        });   
    });
});