define([
    'views/topicsView'
], function (
    Topics
    ) {
    'use strict';

    describe('Topic', function () {
        var component;

        var popupMock = {
            name: [ "Temat 1", "Temat 2", "Temat 3", "Temat 4", "Temat 5", "Temat 6", "Temat 7", "Temat 8" ],
            fetch: function () {
                _.defer(function () {

                });
            }
        };
        afterEach(function () {
            component = null;
        });

        it('Posiada szablon', function () {
            component = new Topics();
            expect(typeof component.template).toBe('function');
        });

        it ('Komponent się renderuje.', function () {
            component = popupMock;
            expect(component.el).not.toBe('');
        });
    });
    return true;
});
