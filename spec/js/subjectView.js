describe('Subjects', function () {
    var component;

    var popupMock = {
        name: [ "Polski", "Matematyka", "Programowanie", "Analiza", "Historia", "Bazy danych", "Automatyka" ],
        fetch: function () {
            _.defer(function () {
            });
        }
    };
    afterEach(function () {
        component = null;
    });

    it('Jest wyświetlony', function () {
        var isVisible = false;
        setTimeout(function () {
            isVisible = $('.subject').length !== 0;
        }, 1000);

        waitsFor(function() {
            return isVisible;
        }, "Element nie został wyświetlony", 1500);

        runs(function() {
            expect(isVisible).toBe(true);
        });
    });

    it('Komponent wypełnia się danymi z serwera', function () {
        var hasText = false;

        setTimeout(function () {
            hasText = !!$('.subject0').text();            
        }, 1000);

        waitsFor(function() {
            return hasText;
        }, "Element nie został wyświetlony", 1500);

        runs(function() {
            expect(hasText).toBe(true);
        }); 
    });
});