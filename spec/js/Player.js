describe('Player', function () {
    it('Pobrano dane z serwera', function () {
        var hasText = false;

        setTimeout(function () {
            hasText = !!$('.subject0').text();
        }, 1000);

        waitsFor(function() {
            return hasText;
        }, "Element nie został wyświetlony", 1500);

        runs(function() {
            expect(hasText).toBe(true);
        });
    });

    it('Odtwarzacz uruchamia się', function () {
        var hasText = false;

        setTimeout(function () {
            hasText = !!$('.subject0').text();
        }, 1000);

        waitsFor(function() {
            return hasText;
        }, "Element nie został wyświetlony", 1500);

        runs(function() {
            expect(hasText).toBe(true);
        });
    });
});