var defaultURL = "http://blandzi.platinum.edu.pl:28017/mydb/";
// Filename: app.js
define([
    'jquery',
    'backbone',
    'router', // Request router.js
    'views/videoItemView',
    'views/menuView',
    'views/subjectView'
], function(
    $,
    Backbone,
    Router,
    VideoItemView,
    MenuView,
    SubjectView
    ){
    var initialize = function(){
        Router.initialize();

        var menu = new MenuView(),
            subject = new SubjectView();

//        $.ajax({
//            url: 'config.json',
//            success: function (data) {
//                defaultURL = JSON.parse(data);
//
//            }
//        })
//        ;
        var turnOnTests = false;

        if (turnOnTests) {
            (function() {
                var jasmineEnv = jasmine.getEnv();
                jasmineEnv.updateInterval = 1000;

                window.curWidget.setPreference("ready", "true");

                var htmlReporter = new jasmine.HtmlReporter();

                jasmineEnv.addReporter(htmlReporter);

                jasmineEnv.specFilter = function(spec) {
                    return htmlReporter.specFilter(spec);
                };

                var currentWindowOnload = window.onload;

                window.onload = function() {
                    if (currentWindowOnload) {
                        currentWindowOnload();
                    }
                    console.log('execJasmine');
                    execJasmine();
                };

                function execJasmine() {
                    jasmineEnv.execute();
                }

            })();
            setTimeout(function () {
                $('#HTMLReporter').css({width: '100%', height: '100%', background: 'white', position: 'absolute', 'z-index': '99999'});
            }, 100);
        }



    };

    return {
        initialize: initialize
    };
});
