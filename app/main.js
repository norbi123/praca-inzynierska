require.config({
  baseUrl: 'app',
  paths: {
    jquery: 'libs/jquery/jquery-1.9.1.min',
    underscore: 'libs/underscore/underscore-min',
    backbone: 'libs/backbone/backbone-min',
    templates: '../templates',
    tpl: 'libs/doT/node_modules/dot/doT'
  },
  waitSeconds: 200

});

require([
  'app'

], function(App){
  App.initialize();
});
