// Filename: router.js
define([
  'underscore',
  'backbone'
], function(
   _,
    Backbone
) {  
  var AppRouter = Backbone.Router.extend({
    routes: {
       '/projects': 'showContributors',
      
      // Default
      '*actions': 'defaultAction'
    }
  });
  
  var initialize = function(){
    var app_router = new AppRouter;
    
    // app_router.on('route:showProjects', function(){
   
       

    // });

     app_router.on('route:showContributors', function () {
      console.log('routeing dziala');
     });

    app_router.on('route:defaultAction', function (actions) {
      app_router.navigate('showContributors');
       
    });
    

    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
