var widgetAPI = new Common.API.Widget(),
	tvKey = new Common.API.TVKeyValue(),
	helpBarButtons = {
		ENTER: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-enter"></div>Wybierz</li>',
		NAV: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-move"></div>Nawigacja</li>',
		NAVLR: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-leftright"></div>Nawigacja</li>',
		NAVUD: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-updown"></div>Nawigacja</li>',
		RED: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-red"></div>Odp A</li>',
		GREEN: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-green"></div>Odp B</li>',
		YELLOW: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-yellow"></div>Odp C</li>',
		BLUE: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-blue"></div>Odp D</li>',
		RETURN: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-return"></div>Powrót</li>',
		PLAY: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-play"></div>Odtwarzaj</li>',
		PAUSE: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-pause"></div>Zatrzymaj</li>',
		FF: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-ff"></div>Przewiń w przód</li>',
		RW: '<li class="cook-ui-navhelp-item"><div class="cook-ui-navhelp-icon cook-ui-navhelp-icon-rw"></div>Przewiń w tył</li>'
	},
	setHelpBar = function (view) {
		var $helpBar = $('.cook-ui-navhelp');
		if($helpBar.children()) {
			$('.cook-ui-navhelp').children().remove();

			switch (view) {
				case 'Main':
      				$helpBar.append(helpBarButtons.NAV);
      				$helpBar.append(helpBarButtons.ENTER);
      				$helpBar.append(helpBarButtons.RETURN);
					break;
				case 'Test':
					$helpBar.append(helpBarButtons.ENTER);
      				$helpBar.append(helpBarButtons.RED);
      				$helpBar.append(helpBarButtons.GREEN);
      				$helpBar.append(helpBarButtons.YELLOW);
      				$helpBar.append(helpBarButtons.BLUE);
					break;
				case 'Player':
					$helpBar.append(helpBarButtons.PAUSE);
					$helpBar.append(helpBarButtons.PLAY);
					$helpBar.append(helpBarButtons.FF);
					$helpBar.append(helpBarButtons.RW);
					$helpBar.append(helpBarButtons.RETURN);
					break;
			}
		}
	},
	scrollDown = false,
	scrollUp = false;



var Main =
{

};

Main.onLoad = function()
{
	// Enable key event processing
	this.enableKeys();
	widgetAPI.sendReadyEvent();
};

Main.onUnload = function()
{

};

Main.enableKeys = function()
{
	document.getElementById("anchor").focus();
};

Main.keyDown = function(keycode)
{
	var curKeyCode = event && event.keyCode ? event.keyCode : keycode,
	node = null,
	prevElem = null,
	nextElem = null,
	answer = null;
	$.fn.classList = function() {return this.className.split(/\s+/);};
console.log(curKeyCode);
	switch(curKeyCode)
	{
		case tvKey.KEY_RETURN:
		case tvKey.KEY_PANEL_RETURN:
			console.log("RETURN");
			widgetAPI.sendReturnEvent();

			if ($('div[id^="_pluginObjectPlayerContainer"]').length) {
				if (playerGlobal){
					playerGlobal.stop();	
					playerGlobal = null;
				}				
				$('div[id^="_pluginObjectPlayerContainer"]').remove();
			} else if($('.course-popup').css('visibility') === 'visible') {
				$('.course-popup').remove();
				$('[dev-last]').addClass('active').removeAttr('dev-last');
			} else if ( $('.test-begin-button').hasClass('active') ) {
				$('.test-popup').remove();
				$('[dev-last]').addClass('active').removeAttr('dev-last');
			}
			break;
		case tvKey.KEY_LEFT:
			console.log("LEFT");
			node = $('.active');
			prevElem = node.prev();
			
			if(prevElem.hasClass('menuNavItem') === true){
				prevElem.addClass('active');
				node.removeClass('active');	

				//clear screen event
				$('.menu').trigger('clearScreen');
			} else if (node.hasClass('topic') || 
					   node.hasClass('test-item')
			) {
				// przejscie na subject list
				var $dataLast = $('[data-last]');
				$dataLast.addClass('active');
				$dataLast.removeAttr('data-last');
				node.removeClass('active');	
			}
			break;
		case tvKey.KEY_RIGHT:
			console.log("RIGHT");
			node = $('.active');
			nextElem = node.next();
			if(nextElem.hasClass('menuNavItem') === true){
				nextElem.addClass('active');
				node.removeClass('active');	
				//clear screen event
				$('.menu').trigger('clearScreen');				
			}
			
			break;
		case tvKey.KEY_UP:
			console.log("UP");
			node = $('.active');
			prevElem = node.prev()
			if(prevElem.hasClass('subject') === true ||
			   prevElem.hasClass('topic') === true ||
			   prevElem.hasClass('test-item') === true
			){
				//scrollUp
				var subScroll = $('.subject-scroll');
				
				if($('.active').attr('data-scroll') === 'last') {
					scrollUp = true;
				}

				if(scrollUp) {
					subScroll.scrollTop(subScroll.scrollTop()-37);
					$('[data-scroll]').removeAttr('data-scroll').prev().attr('data-scroll', 'last');
				}
				
				if(node.attr('dev-last')){
					node.removeAttr('dev-last'); 	
				}				
				prevElem.addClass('active');
				node.removeClass('active');	

				console.log('scroll2');
				if($('.subject0').hasClass('active')) {
					console.log('scroll1');
					$(subScroll.children()[0]).removeAttr('data-scroll');	
					scrollDown = false;
				}
			} else if (prevElem.length === 0 ||
					   prevElem.hasClass('topic-top') === true ||
					   prevElem.hasClass('test-list-top') === true
			) {
				node.removeClass('active');
				node = $('[data-menu]'); 
				node.addClass('active');
				node.removeAttr('data-menu');
			}
			break;
		case tvKey.KEY_DOWN:
			console.log("DOWN");
			node = $('.active');
			nextElem = node.next();
			if(nextElem.hasClass('subject') === true || 
				nextElem.hasClass('topic') === true ||
				nextElem.hasClass('test-item') === true
			){
				if(node.attr('dev-last')){
					node.removeAttr('dev-last'); 	
				}				
				nextElem.addClass('active');
				node.removeClass('active');	

				//scroll
				var subScroll = $('.subject-scroll');
				if( nextElem.hasClass('subject9') ) {
					scrollDown = true;
					if($('[data-scroll]').length) {
						$('[data-scroll]').removeAttr('data-scroll');
					}
					$(subScroll.children()[0]).attr('data-scroll', 'last');
				}

				if (scrollDown) {					
					subScroll.scrollTop(subScroll.scrollTop()+37);
					$('[data-scroll]').removeAttr('data-scroll').next().attr('data-scroll', 'last');
				}
			}
			break;
		case tvKey.KEY_ENTER:
		case tvKey.KEY_PANEL_ENTER:
			console.log("ENTER");
			var node = $('.active'),
				last;

			if (node.hasClass('bechelor-button')) {
				$('.bechelor-button').trigger('enter');
			} else if(node.text() === 'Filmy' || node.text() === 'Testy'){ 
				// wybór w menu
				node.attr('data-menu', 'active');
				node.removeClass('active');
				node = $('.subject').first(); 
				node.addClass('active');
			} else if(node.hasClass('subject')) { 
				//subject panel
				$('.subject0').trigger('enter');
			} else if(node.hasClass('topic')) {	
				//topic panel
				last = node.attr('data-topic');
				$("[data-topic="+ last +"]").trigger('enter');
			} else if (node.hasClass('test-item')) {
				// wybor testu
				last = node.attr('data-test');
				$("[data-test="+ last +"]").trigger('enter');
			} else if(node.hasClass('course-button')) {	
				//course popup button
				$(".course-button").trigger('enter');
			} else if(node.hasClass('test-begin-button')) {	
				//test popup button
				$(".test-begin-button").trigger('enter');
			} else if (node.hasClass('test-result-button')) {
				$('.test-result-button').trigger('enter');
				console.log('trigger na koniec testu');
			} else if (node.hasClass('test-button')) {
				$('.test-button').trigger('enter');
			} else if (node.hasClass('error-button')) {
				$('.error-button').trigger('enter');
			}			
			break;
		case tvKey.KEY_STOP:
			if ($('div[id^="_pluginObject"]') && playerGlobal) {
				playerGlobal.stop();
				playerGlobal = null;
				$('div[id^="_pluginObject"]').remove();
			}
			break;
		case tvKey.KEY_PAUSE:
			if(playerGlobal) {
				playerGlobal.pause();
			}
			break;
		case tvKey.KEY_PLAY:
			if(playerGlobal) {
				playerGlobal.resume();
			}
			break;
		case 72:
			//FF 10s
			if(playerGlobal) {
				playerGlobal.jumpForward(10);
			}
			
			break;
		case 69:
			//RW 10s
			if(playerGlobal) {
				playerGlobal.jumpBackward(10);
			}
			break;
		case 108:
		case tvKey.KEY_1:
			//red 108
			answer = $(".test-answer-0").prev();
			if(answer.hasClass('cook-checked')) {
				answer.removeClass('cook-checked');
			} else {
				answer.addClass('cook-checked');
			}
			break;
		case 20:
		case tvKey.KEY_2:
			// green
			answer = $(".test-answer-1").prev();
			if(answer.hasClass('cook-checked')) {
				answer.removeClass('cook-checked');
			} else {
				answer.addClass('cook-checked');
			}
			break;
		case 21:
		case tvKey.KEY_3:
			// yellow
			answer = $(".test-answer-2").prev();
			if(answer.hasClass('cook-checked')) {
				answer.removeClass('cook-checked');
			} else {
				answer.addClass('cook-checked');
			}
			break;
		case 22:
		case tvKey.KEY_4:
			//blue
			answer = $(".test-answer-3").prev();
			if(answer.hasClass('cook-checked')) {
				answer.removeClass('cook-checked');
			} else {
				answer.addClass('cook-checked');
			}
			break;
		case tvKey.KEY_5:
			answer = $(".test-answer-4").prev();
			if(answer.hasClass('cook-checked')) {
				answer.removeClass('cook-checked');
			} else {
				answer.addClass('cook-checked');
			}
			break;
		case tvKey.KEY_6:
			answer = $(".test-answer-5").prev();
			if(answer.hasClass('cook-checked')) {
				answer.removeClass('cook-checked');
			} else {
				answer.addClass('cook-checked');
			}
			break;
		default:
			console.log("Unhandled key");
			console.log(keyCode);
			break;
	}
};
