define([
  'jquery',
  'underscore',
  'backbone'
], function(
	$,
	_,
	Backbone
){
	var VideoItemModel = Backbone.Model.extend({
		defaults: {
	      id: 0,
	      name: "Learning Video"
	    },

	    initialize: function () {
	    	this.render();
	    },

	    render: function () {
	    	console.log('VideoItemModel render');
	    }
	});

  return VideoItemModel;
});
