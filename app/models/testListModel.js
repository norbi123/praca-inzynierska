define([
  'jquery',
  'underscore',
  'backbone'
], function(
	$,
	_,
	Backbone
){

	var TestListModel = Backbone.Model.extend({
		url: 'http://192.168.0.12:3000/api/course/52e3bd9a21e1642e20e24f04/lesson',
		defaults: {
	      name: ["Test1", "Test2", "Test3", "Test4", 'Test5']
	    },

	    initialize: function () {
	    	this.render();
	    },

	    render: function () {
	    	console.log('TestListModel render');
	    },

	    parse: function (data) {
	    	var subjects = [];
	    	for (var i = 0, j = data.lessons.length; i < j; i++) {
	    		subjects.push(data.lessons[i].subject);
	    	}
	    	return {'name': subjects};	    	
	    }
	});

  return TestListModel;
});
