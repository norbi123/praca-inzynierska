define([
  'jquery',
  'underscore',
  'backbone'
], function(
	$,
	_,
	Backbone
){
	var SubjectModel = Backbone.Model.extend({
		//url: defaultURL + 'subject/',
		url: 'http://192.168.0.12:3000/api/course',
		// defaults: {
	 //      name: ["Matematyka", "Informatyka", "Automatyka", "Programowanie"]
	 //    },

	    initialize: function () {
	    	this.render();
	    },

	    render: function () {
	    	console.log('SubjectModel render');
	    },

	    parse: function (data) {
	    	var names = [];
	    	for (var i = 0, j = data.courses.length; i < j; i++) {
	    		names.push(data.courses[i].name);
	    	}
	    	return {'name': names};
	    }
	});

  return SubjectModel;
});
