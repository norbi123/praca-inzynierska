define([
  'jquery',
  'underscore',
  'backbone'
], function(
	$,
	_,
	Backbone
){
	var CoursePopupModel = Backbone.Model.extend({
		defaults: {
	      title: 'Matematyka',
	      subtitle: 'Lekcja 1',
	      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
	    },

	    initialize: function () {
	    	this.render();
	    },

	    render: function () {
	    	console.log('coursePopupModel render');
	    }
	});

  return CoursePopupModel;
});
