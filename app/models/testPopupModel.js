define([
  'jquery',
  'underscore',
  'backbone'
], function(
	$,
	_,
	Backbone
){
	var TestModel = Backbone.Model.extend({
		url: defaultURL + 'quiz/',

	    initialize: function () {
	    	var that = this;
	    	
	    	this.fetch({
	    		success: function () {
	    			that.render();		
	    		}
	    	});	    	
	    },

	    render: function () {
	    	console.log('TestModel render');
	    },

	    parse: function (data) {		    
	    	return data.rows[0];
	    }
	});

  return TestModel;
});
