define([
  'jquery',
  'underscore',
  'backbone'
], function(
	$,
	_,
	Backbone
){
	var MenuModel = Backbone.Model.extend({
		defaults: {
	      logo: 0,
	      item1: "Filmy",
	      item2: "Testy",
	      item3: "Gry"
	    },

	    initialize: function () {
	    	this.render();
	    },

	    render: function () {
	    	console.log('VideoItemModel render');
	    }
	});

  return MenuModel;
});
