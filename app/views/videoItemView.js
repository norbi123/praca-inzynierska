define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/videoItemT.html',
  '../models/videoItemModel'
], function(
  $,
  _,
  Backbone,
  videoItemTemplate,
  VideoItemModel
){
  var videoItem = new VideoItemModel({id: '2', name: 'polk'}),
      VideoItemView = Backbone.View.extend({
    el: $('.video-items'),
    
    initialize: function() {
      this.render();
    },

    render: function(){
      var compiledTemplate = _.template( videoItemTemplate, videoItem.attributes);
      this.$el.append( compiledTemplate );

    }
  });
  return VideoItemView;
});