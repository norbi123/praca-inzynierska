define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/topicT.html',
  'tpl',
  '../models/topicsModel',
  'views/coursePopupView'
], function(
  $,
  _,
  Backbone,
  topicsTemplate,
  tpl,
  TopicsModel,
  CoursePopupView
  
){
  var topics = new TopicsModel();

  var TopicsView = Backbone.View.extend({
    el: 'body',

    template: tpl.compile(topicsTemplate),

    initialize: function() {
      var that = this;
      topics.fetch({
        success: function (data) {
          that.render(data);    
        }
      });      
    },   

    render: function(data){
      var compiledTemplate = this.template( data.attributes),
          nodeLength = 0,
          that = this,
          coursePopupView = null,
          nodeActive = null;
      
      this.$el.append( compiledTemplate );
      nodeLength = $('[data-topic]').length;
      $('.active').removeClass('active');
      $("[data-topic='0']").addClass('active');
      
      $("[data-topic]").each(function (index) {
         
          $(this).on('enter', function () {
            nodeActive = $('.active');
            coursePopupView = new CoursePopupView();
            nodeActive.attr('dev-last', 'true');
            nodeActive.removeClass('active');
            $('.course-button').addClass('active');
          
        });
      })
    }
  });
  return TopicsView;
});