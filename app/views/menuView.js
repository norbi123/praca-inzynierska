define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/menuT.html',
  '../models/menuModel',
  'tpl',
  'views/subjectView',
  'text!templates/bechelorThesisInfoT.html'
], function(
  $,
  _,
  Backbone,
  menuTemplate,
  MenuModel,
  tpl,
  SubjectView,
  bechelorTemplate
){

  var menu = new MenuModel();
  var MenuView = Backbone.View.extend({
    el: 'body',

    template: tpl.compile(menuTemplate),
    bechelorT: tpl.compile(bechelorTemplate),

    initialize: function() { 
      var subscribeResult = webapis.recognition.SubscribeExEvent(
          webapis.recognition.PL_RECOGNITION_TYPE_VOICE, "learning",
          this.voiceHandler),
          helpBarInfo = {
          helpbarType: "HELPBAR_TYPE_VOICE_CUSTOMIZE",
          helpbarItemsList: [
              { itemText: "Wybierz", commandList: [{command: "Wybierz"}, {command: "Otwórz"}]},
              { itemText: "Lewo", commandList: [{command: "Lewo"}, {command: "W lewo"}]},
              { itemText: "Prawo", commandList: [{command: "Prawo"}, {command: "W prawo"}]},
              { itemText: "Góra", commandList: [{command: "Góra"}, {command: "W górę"}]},
              { itemText: "Dół", commandList: [{command: "Dół"}, {command: "W dół"}]},
              { itemText: "Cofnij", commandList: [{command: "Cofnij"}]},
              { itemText: "Odpowiedź A", commandList: [{command: "Odpowiedź A"}]},
              { itemText: "Odpowiedź B", commandList: [{command: "Odpowiedź B"}]},
              { itemText: "Odpowiedź C", commandList: [{command: "Odpowiedź C"}]},
              { itemText: "Odpowiedź D", commandList: [{command: "Odpowiedź D"}]},
              { itemText: "Odpowiedź E", commandList: [{command: "Odpowiedź E"}]},
              { itemText: "Odpowiedź F", commandList: [{command: "Odpowiedź F"}]},
              { itemText: "Przewiń w tył", commandList: [{command: "Przewiń w tył"}]},
              { itemText: "Przewiń w przód", commandList: [{command: "Przewiń w przód"}]},
              { itemText: "Odtwarzaj", commandList: [{command: "Odtwarzaj"}, {command: "Wznów"}]},
              { itemText: "Zatrzymaj", commandList: [{command: "Zatrzymaj"}]}
          ]
      };

      // Sprawdzenie czy voice i gesty są dostępne na tv
             
      if (webapis.recognition.IsRecognitionSupported() !== true) {
          console.log('rozpoznawanie głosu jest nie dostępne');
          return;
      }

      // sprawdzenie czy voice jest włączony w tv

      if (webapis.recognition.IsVoiceRecognitionEnabled() !== true) {
          console.log('rozpoznawanie głosu jest wyłączone');
          return;
      }
      webapis.recognition.SetVoiceHelpbarInfo(JSON.stringify(helpBarInfo)); 

      setHelpBar('Main');
      this.render();
    },

    render: function(){

      var compiledTemplate = this.template( menu.attributes),
          compiledBechelorTemplate = this.bechelorT(),
          subjectView = null,
          activeElem = null,
          testL = null;
      
      this.$el.append( compiledTemplate );
      this.$el.append( compiledBechelorTemplate );

      $('.bechelor-button').on('enter', function () {
        $('.bechelor-info').remove();
        $('.item1').addClass('active');
      }); 

      $('.menu').on('clearScreen', function () {
        activeElem = $('.active').attr('class').split(' ').slice(0,1)[0];
        
        switch (activeElem) {
          case 'item1':
              testL = $('.tests-list');
              if(testL.length) {
                  testL.remove();
              }
              $('.subjects').remove();
              subjectView = new SubjectView();
            break;
          case 'item2':
              $('.subjects').remove();
              $('.topics').remove();
              subjectView = new SubjectView();
              break;        
          case 'item3':

            break;
          default: 
            //default behaviour here
        }    
      });
    },

    voiceHandler: function (e) {
      var event = {},
      keyCode = {
        ENTER: 29443,
        RETURN: 88,
        PAUSE: 74,
        PLAY: 71,
        STOP: 70,
        RIGHT: 5,
        LEFT: 4,
        UP: 29460,
        DOWN: 29461,
        RED: 108,
        GREEN: 20,
        YELLOW: 21,
        BLUE: 22,
        NUM1: 101,
        NUM2: 98,
        NUM3: 6,
        NUM4: 8,
        NUM5: 9,
        NUM6: 10,
        NUM7: 12,
        NUM8: 13,
        NUM9: 14,
        RW: 69,
        FF: 72

      };

      event.keyCode = keyCode.ENTER;
      switch (e.result.toLowerCase()) {
        case "lewo": 
        case "w lewo":
            Main.keyDown(keyCode.LEFT);
            break;
        case "prawo":
        case "w prawo":
            Main.keyDown(keyCode.RIGHT);
             break;
         case "góra":
         case "w górę": 
            Main.keyDown(keyCode.UP);
            break;
        case "dół":
        case "w dół":
            Main.keyDown(keyCode.DOWN);
             break;
        case "wyłącz":
            Main.keyDown(keyCode.ENTER);
            break;
        case "cofnij":
            Main.keyDown(keyCode.RETURN);
            break;
        case "wybierz":            
            Main.keyDown(keyCode.ENTER);
            break;
        case "odpowiedź a":
            Main.keyDown(keyCode.RED);
             break;
        case "odpowiedź b":
            Main.keyDown(keyCode.GREEN);
            break;
        case "odpowiedź c":
            Main.keyDown(keyCode.YELLOW);
            break;
        case "odpowiedź d":
            Main.keyDown(keyCode.BLUE);
            break;
        case "odpowiedź e":
            Main.keyDown(keyCode.NUM5);
            break;
        case "odpowiedź f":
            Main.keyDown(keyCode.NUM6);
            break;
        case "zatrzymaj":
            Main.keyDown(keyCode.PAUSE);
            break;
        case "odtwarzaj":
        case "wznów":
            Main.keyDown(keyCode.PLAY);
            break;
        case "przewiń w przód":
            Main.keyDown(keyCode.FF);
            break;
        case "przewiń w tył":
            Main.keyDown(keyCode.RW);
            break;
        default:
            break;
    }
    }
  });
  return MenuView;
});