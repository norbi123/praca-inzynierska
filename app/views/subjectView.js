var subjectNotParsedData = null;

define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/subjectT.html',
  'tpl',
  '../models/subjectModel',
  '../views/topicsView',
  '../views/testListView'
], function(
  $,
  _,
  Backbone,
  subjectTemplate,
  tpl,
  SubjectModel,
  TopicView,
  TestListView
){
  var subject = new SubjectModel();

  var SubjectView = Backbone.View.extend({
    el: 'body',

    template: tpl.compile(subjectTemplate),

    topic: null,

    testList: null,

    initialize: function() {
      var that = this;
      subject.fetch({
        success: function (data, fullData) {
          subjectNotParsedData = fullData;
          that.render(data);    
      }});      
    },

    render: function(data){
      console.log('render subject view');
      console.log(data.attributes);
      var compiledTemplate = this.template( data.attributes);
          this.$el.append( compiledTemplate ),
          elem = null;

          $('.subject0').on('enter', function () {
            title = $('.active').text();
            elem = $('[data-menu]').text();
            $('.active').attr('data-last', 'true');
            
            if(elem === 'Filmy') {
              $('.topics').remove();
              this.topic = new TopicView();  
              
            } else if (elem === 'Testy') {
              $('.tests-list').remove();
              this.testList = new TestListView();               
            }
          });
    }
  });
  return SubjectView;
});