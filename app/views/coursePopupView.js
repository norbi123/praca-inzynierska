playerGlobal = null;

define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/coursePopupT.html',
  '../models/coursePopupModel',
  'tpl'
], function(
  $,
  _,
  Backbone,
  coursePopupTemplate,
  CoursePopupModel,
  tpl
){

  var coursePopupModel = new CoursePopupModel();
  
  var CoursePopupView = Backbone.View.extend({
    el: 'body',

    template: tpl.compile(coursePopupTemplate),

    player: null, 

    successOpenCallback: function (player) {
      playerGlobal = player;
      var successCallback = function () {
         console.log('successCallback');
      },
      errorCallback = function () {
        console.log('errorCallback');
      }
      player.init({
        zIndex: 999,
        displayRect: {
            width: 1280,
            height: 720,
            top: 0,
            left: 0
        }
      });
      player.open('http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4');

      
      player.play(successCallback, errorCallback);
    },

    errorOpenCallback: function () {
      console.log('errorOpenCallback');
    },

    playSuccess: function () {
      console.log('playSuccess');
    },

    playError: function () {
      console.log('playError');
    },   

    initialize: function() {
      this.render();
      var that = this;

      $(".course-button").on('enter', function () {
        webapis.avplay.getAVPlay(that.successOpenCallback, that.errorOpenCallback);
      })
    },

    render: function(){
      var compiledTemplate = this.template( coursePopupModel.attributes );
      
      this.$el.append( compiledTemplate );      
    }
  });
  return CoursePopupView;
});