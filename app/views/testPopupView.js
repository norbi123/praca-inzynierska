playerGlobal = null;
title = '';
subtitle = '';


define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/testPopupT.html',
  '../models/testPopupModel',
  'tpl',
  'text!templates/testWindowT.html',
  'text!templates/testResultT.html',
  'text!templates/errorPopupT.html',
  'text!templates/answerT.html'
], function(
  $,
  _,
  Backbone,
  testPopupTemplate,
  TestPopupModel,
  tpl,
  testWindowTemplate,
  testResultTemplate,
  errorPopupTemplate,
  testAnswerTemplate
){

  var testPopupModel = new TestPopupModel();
  
  var TestPopupView = Backbone.View.extend({
    el: 'body',

    template: tpl.compile(testPopupTemplate),

    testTemplate: tpl.compile(testWindowTemplate),

    resultTemplate: tpl.compile(testResultTemplate),

    errorTemplate:  tpl.compile(errorPopupTemplate),

    answerTemplate: tpl.compile(testAnswerTemplate),

    player: null, 

    questionCounter: 0,

    counter: 1,

    goodAnswer: 0,

    initialize: function() {
      this.render();
    },

    nextQuestion: function () {      
      var $testQuestion = $('.test-question'),
          $cookCheckedLen = $('.cook-checked').length;

      this.checkAnswers();

      this.counter = Number($testQuestion.attr('data-counter'));
      this.counter++;

      if (!$cookCheckedLen) {
        this.showError(); 
      }     
      
      if (testPopupModel.attributes.questions.length == this.counter) {
        this.showResult();
      } else {
        if ($cookCheckedLen) {
          this.showNextQuestion();
        }
      }      
    },

    checkAnswers: function () {
      var answers = testPopupModel.attributes.questions[this.counter].answers,
          $testAnwers = $('.cook-ui-checkbox'),
          isGoodAnswer = false;

      for (var i=0; i < $testAnwers.length; i++) {
        if ($($testAnwers[i]).hasClass('cook-checked')) {
          if ($($testAnwers[i]).hasClass('cook-checked') == answers[i].isGood) {

            isGoodAnswer = true;     
          }else {
            isGoodAnswer = false;
            break;
          }
        } 
      }
      if (isGoodAnswer) {
        this.goodAnswer++;  
      }      
    },

    showError: function () {
      var $errorButton= null;
      $(this.el).append(this.errorTemplate());
          $('.active').removeClass('active');
          $errorButton = $('.error-button');
          $errorButton.addClass('active');

          $errorButton.on('enter', function () {
            $('.error-popup').remove();
            $('.test-button').addClass('active');
          });
    },

    showNextQuestion: function () {
      var compiledAnswerTemplate = null,
          $testQuestion = null,
          that = this;

      compiledAnswerTemplate = this.answerTemplate(testPopupModel.attributes.questions[this.counter-1]);
      $('.test-window').remove();
      $('.test-popup').append( compiledAnswerTemplate );

      $testQuestion = $('.test-question');      
      $testQuestion.attr('data-counter', this.counter);

      $('.test-button').on('enter', function () {
          that.nextQuestion();
      });
    },

    showResult: function () {
      var compiledResultTemplate = this.resultTemplate( {result: this.goodAnswer, total: testPopupModel.attributes.questions.length}),
          $testResultButton = null,
          $testPopup = $('.test-popup');

      $('.test-window').remove();      
      $testPopup.append( compiledResultTemplate );
      $testResultButton = $('.test-result-button');
      $testResultButton.addClass('active');

      $testResultButton.on('enter', function () {
        $testPopup.remove();
        $('[dev-last=true]').addClass('active');
        setHelpBar('Main');
      });
    },

    render: function(){
      testPopupModel.attributes.title = title;
      testPopupModel.attributes.subtitle = subtitle;
      var compiledTemplate = this.template( testPopupModel.attributes ),
      that = this,
      compiledTestTemplate = null,
      $testPopup = null;
      
      this.$el.append( compiledTemplate );

      $(".test-begin-button").on('enter', function () {
        // Rozpoczęcie testu
        compiledTestTemplate = that.testTemplate( testPopupModel.attributes );
        $testPopup = $('.test-popup');
        $testPopup.children().remove();
        $testPopup.append( compiledTestTemplate );             

        $('.test-button').on('enter', function () {
          that.nextQuestion();
        });
        setHelpBar('Test');
      });      
    }
  });
  return TestPopupView;
});