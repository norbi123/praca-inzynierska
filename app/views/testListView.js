define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/testListItemT.html',
  'tpl',
  '../models/testListModel',
  'views/testPopupView'
], function(
  $,
  _,
  Backbone,
  testListTemplate,
  tpl,
  TestListModel,
  TestPopupView  
){
  var testsList = new TestListModel();

  var TestListView = Backbone.View.extend({
    el: 'body',

    template: tpl.compile(testListTemplate),

    initialize: function() {
      var that = this;
      testsList.fetch({
        success: function (data) {
          that.render(data);    
        }
      });   
    },   

    render: function(data){
      var compiledTemplate = this.template( data.attributes),
          nodeLength = 0,
          that = this,
          nodeActive = null;
      
      this.$el.append( compiledTemplate );
      
      nodeLength = $('[data-topic]').length;

      $('.active').removeClass('active');
      $("[data-test='0']").addClass('active');
      
      $("[data-test]").each(function (index) {
         
          $(this).on('enter', function () {
             nodeActive = $('.active');
             subtitle = nodeActive.text();
             testPopupView = new TestPopupView();
             nodeActive.attr('dev-last', 'true');
             nodeActive.removeClass('active'); 
             $('.test-begin-button').addClass('active');         
        })
      })
    }
  });
  return TestListView;
});