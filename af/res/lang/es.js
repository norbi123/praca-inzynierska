﻿alert("Language Spanish(Spa) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Servicio no disponible debido a una interferencia de red o servicio.";
sf.lang.SID_BLIND = "Persiana";
sf.lang.SID_CANCEL = "Cancelar";
sf.lang.SID_CHECKER = "Tablero";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "Conexión cerrada por error de servidor o red. Comprobar y volver a intentar.";
sf.lang.SID_DESELECT_ALL = "Deseleccionar todo";
sf.lang.SID_FADE1 = "Fundido 1";
sf.lang.SID_FADE2 = "Fundido 2";
sf.lang.SID_FAST = "Rápida";
sf.lang.SID_LINEAR = "Lineal";
sf.lang.SID_MIX_FILE_SELECTED = "Archivo <<A>> sel.";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "Archivos <<A>> sel.";
sf.lang.SID_MOVE = "Mover";
sf.lang.SID_MUSIC_DIF_KR = "Música";
sf.lang.SID_NONE = "Ninguno";
sf.lang.SID_NORMAL = "Normal";
sf.lang.SID_NOT_SUPPORT_FORMAT = "No se puede ver la imagen. Forma no compatible.";
sf.lang.SID_OK = "Aceptar";
sf.lang.SID_PATH = "Ruta";
sf.lang.SID_PAUSE = "Pausa";
sf.lang.SID_PHOTOS = "Fotos";
sf.lang.SID_PICTURE_SETTING = "Configuración de la imagen";
sf.lang.SID_PLAY = "Reprod.";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "La grabación de voz no se admite en este archivo.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "La entrada de vídeo no se admite en este archivo.";
sf.lang.SID_PLAYER_RENDERERROR = "Error al reproducir el archivo.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "No es posible reproducir el archivo.";
sf.lang.SID_PREV_NEXT = "Ant./Sig.";
sf.lang.SID_RANDOM = "Aleatorio";
sf.lang.SID_RETURN = "Volver";
sf.lang.SID_SELECT = "Seleccionar";
sf.lang.SID_SELECT_ALL = "Seleccionar todos";
sf.lang.SID_SKIP = "Omitir";
sf.lang.SID_SLOW = "Lenta";
sf.lang.SID_SLOW_FAST = "Lento/Rápido";
sf.lang.SID_SOUND_SETTING = "Configuración del sonido";
sf.lang.SID_SPIRAL = "Espiral";
sf.lang.SID_STAIRS = "Escaleras";
sf.lang.SID_START_SLIDESHOW = "Iniciar presentación";
sf.lang.SID_STOP_SLIDESHOW = "Detener presentación";
sf.lang.SID_TOOLS = "Herramientas";
sf.lang.SID_UPPER_FOLDER = "Carpeta superior";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "USB no conectado.";
sf.lang.SID_VIDEOS = "Vídeos";
sf.lang.SID_WIPE = "Cortinilla";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "No hay archivo de vídeo disponible actualmente.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "No hay archivo de fotos disponible actualmente.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "Sin archivo de música disponible actualmente.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Ningún dispositivo AllShare conectado.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "No hay ningún dispositivo AllShare conectado.  Con AllShare, podrá disfrutar del contenido del PC y los dispositivos móviles directamente en la pantalla del televisor sin conectar cables adicionales. Para obtener más información, consulte el manual.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Elegir disp para abrir un archivo.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Conectar a disp que contiene archiv para abrir.";
sf.lang.COM_SELECT_PHOTOS = "Selec. fotos.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "<<A>> foto/s seleccionad.";
sf.lang.COM_SELECT_VIDEOS = "Selecc. vídeos.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "<<A>> vídeo/s seleccionad.";
sf.lang.COM_SELECT_SONGS = "Selec canciones.";
sf.lang.COM_MIX_SONGS_SELECTED = "<<A>> canción/es selec.";
sf.lang.COM_SELECT_A_PHOTO = "Seleccionar foto.";
sf.lang.COM_SELECT_A_VIDEO = "Seleccionar vídeo.";
sf.lang.COM_SELECT_A_SONG = "Selec canción.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Ir a lista disposit";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Ir a carpeta superior";
sf.lang.COM_UPLOAD_COMPLETE = "Carga completada.";
sf.lang.TV_SID_UPLOAD_FAILED = "Error en carga.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "No hay dispositivo conectado.";
sf.lang.TV_UPLOADING_PROGRESS = "Cargando…";
sf.lang.SID_TOTAL_SIZE_COLON = "Tamaño total:";
sf.lang.TV_SID_TOTAL = "Total";
sf.lang.SID_SIZE_M_FILE_SIZE = "Tamaño";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MB";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "Enero";
sf.lang.SID_FEBRUARY = "Febrero";
sf.lang.SID_MARCH = "Marzo";
sf.lang.SID_APRIL = "Abril";
sf.lang.SID_MAY_FULL = "Mayo";
sf.lang.SID_JUNE = "Junio";
sf.lang.SID_JULY = "Julio";
sf.lang.SID_AUGUST = "Agosto";
sf.lang.SID_SEPTEMBER = "Septiembre";
sf.lang.SID_OCTOBER = "Octubre";
sf.lang.SID_NOVEMBER = "Noviembre";
sf.lang.SID_DECEMBER = "Diciembre";
sf.lang.SID_SUNDAY = "Domingo";
sf.lang.SID_MONDAY = "Lunes";
sf.lang.SID_TUESDAY = "Martes";
sf.lang.SID_WEDNESDAY = "Miércoles";
sf.lang.SID_THURSDAY = "Jueves";
sf.lang.SID_FRIDAY = "Viernes";
sf.lang.SID_SATURDAY = "Sábado";
sf.lang.SID_SUN = "Dom";
sf.lang.SID_MON = "Lun";
sf.lang.SID_TUE = "Mar";
sf.lang.SID_WED = "Mié";
sf.lang.SID_THU = "Jue";
sf.lang.SID_FRI = "Vie";
sf.lang.SID_SAT = "Sáb";
sf.lang.SID_AM = "a.m.";
sf.lang.SID_PM = "p.m.";
sf.lang.SID_AM_PM = "a.m./p.m.";
sf.lang.SID_HOUR = "Hora";
sf.lang.SID_MINUTE = "Minuto";
sf.lang.TV_SID_PRE_NEXT_MONTH = "Mes pasado o próximo";
sf.lang.SID_CALENDAR = "Calendario";
sf.lang.SID_DAY = "Día";
sf.lang.SID_MONTH = "Mes";
sf.lang.SID_YEAR = "Año";
sf.lang.TV_SID_TIMER_SETTING = "Config. hora";
sf.lang.TV_SID_SETTING = "Config.";
sf.lang.SID_SELECT = "Seleccionar";
sf.lang.SID_CHANGE_KR_CHANGE = "Cambio";
sf.lang.SID_JAN = "Ene";
sf.lang.SID_FEB = "Feb";
sf.lang.SID_MAR = "Mar";
sf.lang.SID_APR = "Abr";
sf.lang.SID_MAY = "May";
sf.lang.SID_JUN = "Jun";
sf.lang.SID_JUL = "Jul";
sf.lang.SID_AUG = "Ago";
sf.lang.SID_SEP = "Sep";
sf.lang.SID_OCT = "Oct";
sf.lang.SID_NOV = "Nov";
sf.lang.SID_DEC = "Dic";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "Accederá a la lista de dispositivos, ya que se ha desconectado el dispositivo en uso.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "No puede seleccionar más de 100 archivos simultáneamente.";
sf.lang.COM_FILE_BROWSER = "Expl. archivos";