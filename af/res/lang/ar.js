﻿alert("Language Arabic(Arb) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "الخدمة غير متوفرة نظرًا لحدوث تداخل في الشبكة أو الخدمة.";
sf.lang.SID_BLIND = "مخفي";
sf.lang.SID_CANCEL = "إلغاء الأمر";
sf.lang.SID_CHECKER = "رقعة شطرنج";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "The connection is closed due to network or server error. Please check and retry.";
sf.lang.SID_DESELECT_ALL = "إلغاء تحديد الكل";
sf.lang.SID_FADE1 = "باهت1";
sf.lang.SID_FADE2 = "باهت2";
sf.lang.SID_FAST = "سريعة";
sf.lang.SID_LINEAR = "طولي";
sf.lang.SID_MIX_FILE_SELECTED = "<<A>> ملف محدد";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "<<A>> ملفات محددة";
sf.lang.SID_MOVE = "تقل";
sf.lang.SID_MUSIC_DIF_KR = "موسيقى";
sf.lang.SID_NONE = "بلا";
sf.lang.SID_NORMAL = "عادي";
sf.lang.SID_NOT_SUPPORT_FORMAT = "لا يمكنك مشاهدة الصورة. نحن لا ندعم النموذج.";
sf.lang.SID_OK = "موافق";
sf.lang.SID_PATH = "الطريق";
sf.lang.SID_PAUSE = "إيقاف";
sf.lang.SID_PHOTOS = "الصور";
sf.lang.SID_PICTURE_SETTING = "إعدادات الصورة";
sf.lang.SID_PLAY = "تشغيل";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "تسجيل الصوت لهذا الملف غير مدعوم.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "دخل الفيديو لهذا الملف غير مدعوم.";
sf.lang.SID_PLAYER_RENDERERROR = "حدث خطأ أثناء تشغيل الملف.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "يتعذّر تشغيل الملف.";
sf.lang.SID_PREV_NEXT = "سابق/تال";
sf.lang.SID_RANDOM = "عشوائي";
sf.lang.SID_RETURN = "عودة";
sf.lang.SID_SELECT = "تحديد";
sf.lang.SID_SELECT_ALL = "تحديد الكل";
sf.lang.SID_SKIP = "تخطي";
sf.lang.SID_SLOW = "بطيئة";
sf.lang.SID_SLOW_FAST = "بطيء/سريع";
sf.lang.SID_SOUND_SETTING = "إعدادات الصوت";
sf.lang.SID_SPIRAL = "حلزوني";
sf.lang.SID_STAIRS = "مدرّج";
sf.lang.SID_START_SLIDESHOW = "بدء عرض الشرائح";
sf.lang.SID_STOP_SLIDESHOW = "إيقاف عرض الشرائح";
sf.lang.SID_TOOLS = "أدوات";
sf.lang.SID_UPPER_FOLDER = "المجلد الأعلى";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "USB جهاز غير متصل";
sf.lang.SID_VIDEOS = "الفيديو";
sf.lang.SID_WIPE = "مسح";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "لا يتوفر ملف فيديو حاليًا.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "لا تتوفر أي ملفات لصور حاليًا.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "لا تتوفر أي ملفات موسيقى حاليًا.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "لا يوجد جهاز AllShare متصل.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "لا يوجد جهاز AllShare متصل.  يتيح لك AllShare الاستمتاع بالمحتويات المتنوعة من الكمبيوتر الخاص بك أو أجهزة الهاتف المحمول على شاشة التلفزيون مباشرةً دون توصيل أسلاك إضافية. لمزيد من المعلومات، يرجى الرجوع إلى الدليل.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "اختر جهازًا لفتح ملف.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "قم بتوصيل جهاز يحتوي على ملفات لفتحها.";
sf.lang.COM_SELECT_PHOTOS = "حدد صورًا.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "تم تحديد <<A>> صورة.";
sf.lang.COM_SELECT_VIDEOS = "حدد مقاطع فيديو.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "تم تحديد <<A>> مقطع فيديو.";
sf.lang.COM_SELECT_SONGS = "حدد أغان.";
sf.lang.COM_MIX_SONGS_SELECTED = "تم تحديد <<A>> أغنية.";
sf.lang.COM_SELECT_A_PHOTO = "حدد صورة.";
sf.lang.COM_SELECT_A_VIDEO = "حدد مقطع فيديو.";
sf.lang.COM_SELECT_A_SONG = "حدد أغنية.";
sf.lang.COM_GO_TO_DEVICE_LIST = "انتقل لقائمة الأجهزة";
sf.lang.COM_GO_TO_UPPER_FOLDER = "انتقل للمجلد العلوي";
sf.lang.COM_UPLOAD_COMPLETE = "اكتمل التحميل.";
sf.lang.TV_SID_UPLOAD_FAILED = "فشل التحميل.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "لا يوجد جهاز متصل.";
sf.lang.TV_UPLOADING_PROGRESS = "جار التحميل...";
sf.lang.SID_TOTAL_SIZE_COLON = "الحجم الكلي:";
sf.lang.TV_SID_TOTAL = "الإجمالي";
sf.lang.SID_SIZE_M_FILE_SIZE = "الحجم";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "ميجابايت";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "يناير";
sf.lang.SID_FEBRUARY = "فبراير";
sf.lang.SID_MARCH = "مارس";
sf.lang.SID_APRIL = "أبريل";
sf.lang.SID_MAY_FULL = "مايو";
sf.lang.SID_JUNE = "يونيو";
sf.lang.SID_JULY = "يوليو";
sf.lang.SID_AUGUST = "أغسطس";
sf.lang.SID_SEPTEMBER = "سبتمبر";
sf.lang.SID_OCTOBER = "??????";
sf.lang.SID_NOVEMBER = "نوفمبر";
sf.lang.SID_DECEMBER = "ديسمبر";
sf.lang.SID_SUNDAY = "الأحد";
sf.lang.SID_MONDAY = "الاثنين";
sf.lang.SID_TUESDAY = "الثلاثاء";
sf.lang.SID_WEDNESDAY = "الأربعاء";
sf.lang.SID_THURSDAY = "الخميس";
sf.lang.SID_FRIDAY = "الجمعة";
sf.lang.SID_SATURDAY = "السبت";
sf.lang.SID_SUN = "Sun";
sf.lang.SID_MON = "Mon";
sf.lang.SID_TUE = "Tue";
sf.lang.SID_WED = "Wed";
sf.lang.SID_THU = "Thu";
sf.lang.SID_FRI = "Fri";
sf.lang.SID_SAT = "Sat";
sf.lang.SID_AM = "صباحًا ‏";
sf.lang.SID_PM = "مساءً";
sf.lang.SID_AM_PM = "مرة";
sf.lang.SID_HOUR = "ساعة";
sf.lang.SID_MINUTE = "دقيقة";
sf.lang.TV_SID_PRE_NEXT_MONTH = "الشهر السابق/التالي";
sf.lang.SID_CALENDAR = "التقويم";
sf.lang.SID_DAY = "يوم";
sf.lang.SID_MONTH = "شهر";
sf.lang.SID_YEAR = "سنة";
sf.lang.TV_SID_TIMER_SETTING = "ضبط الوقت";
sf.lang.TV_SID_SETTING = "الضبط";
sf.lang.SID_SELECT = "تحديد";
sf.lang.SID_CHANGE_KR_CHANGE = "تغيير";
sf.lang.SID_JAN = "يناير";
sf.lang.SID_FEB = "فبراير";
sf.lang.SID_MAR = "مارس";
sf.lang.SID_APR = "أبريل";
sf.lang.SID_MAY = "مايو";
sf.lang.SID_JUN = "يونيو";
sf.lang.SID_JUL = "يوليو";
sf.lang.SID_AUG = "أغسطس";
sf.lang.SID_SEP = "سبتمبر";
sf.lang.SID_OCT = "أكتوبر";
sf.lang.SID_NOV = "نوفمبر";
sf.lang.SID_DEC = "ديسمبر";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "سيتم نقلك إلى قائمة الأجهزة لأن الجهاز المستخدم تم فصله.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "لا يمكنك تحديد أكثر من 100 ملف في نفس الوقت.";
sf.lang.COM_FILE_BROWSER = "مستعرض الملفات";