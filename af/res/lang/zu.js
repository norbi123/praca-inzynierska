﻿alert("Language Zulu(Zul) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Isevisi ayitholakali ngenxa yokuphazamiseka kwenethiwekhi noma kwesevisi.";
sf.lang.SID_BLIND = "Mpumputhe";
sf.lang.SID_CANCEL = "Khansela";
sf.lang.SID_CHECKER = "I-Checker";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "The connection is closed due to network or server error. Please check and retry.";
sf.lang.SID_DESELECT_ALL = "Yek Ukukht Konk";
sf.lang.SID_FADE1 = "Fiphala1";
sf.lang.SID_FADE2 = "Fiphala2";
sf.lang.SID_FAST = "Sheshayo";
sf.lang.SID_LINEAR = "Samugqa";
sf.lang.SID_MIX_FILE_SELECTED = "<<A>> ifay ekhethiw";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "<<A>> amafay akheth";
sf.lang.SID_MOVE = "Hmbisa";
sf.lang.SID_MUSIC_DIF_KR = "Umculo";
sf.lang.SID_NONE = "Lutho";
sf.lang.SID_NORMAL = "Vamile";
sf.lang.SID_NOT_SUPPORT_FORMAT = "Ngeke usibone isithombe. Asiyisekeli ifomathi.";
sf.lang.SID_OK = "OK";
sf.lang.SID_PATH = "Umzila";
sf.lang.SID_PAUSE = "Misa";
sf.lang.SID_PHOTOS = "Amafotho";
sf.lang.SID_PICTURE_SETTING = "Isethingi Yesithombe";
sf.lang.SID_PLAY = "Dlala";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "Voice recording not supported for this file.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "Video input not supported for this file.";
sf.lang.SID_PLAYER_RENDERERROR = "Kb nenkng ngsikhth udll ifyl.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "Unable to play file.";
sf.lang.SID_PREV_NEXT = "Okdl./Okul";
sf.lang.SID_RANDOM = "Ngenahlelo";
sf.lang.SID_RETURN = "Buyisa";
sf.lang.SID_SELECT = "Khetha";
sf.lang.SID_SELECT_ALL = "Khetha Konke";
sf.lang.SID_SKIP = "Yeqa";
sf.lang.SID_SLOW = "Nensayo";
sf.lang.SID_SLOW_FAST = "Nensayo/Sheshayo";
sf.lang.SID_SOUND_SETTING = "Isethingi Yomsindo";
sf.lang.SID_SPIRAL = "Sontene";
sf.lang.SID_STAIRS = "Izitezi";
sf.lang.SID_START_SLIDESHOW = "Qal Umbuk wama-slide";
sf.lang.SID_STOP_SLIDESHOW = "Mis Umbuk wama-slide";
sf.lang.SID_TOOLS = "Amathul";
sf.lang.SID_UPPER_FOLDER = "Ifolda Engenhla";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "I-USB ayixhunyiwe.";
sf.lang.SID_VIDEOS = "Amavidyo";
sf.lang.SID_WIPE = "Sula";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "Ayikho ifayela yevidiyo etholakalayo kwamanje.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "Ayikho ifayela Yesithombe etholak okwamanje.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "Alikho ifayela lomculo elitho okwam.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Ayikho idivaysi ye-Allshare exhunyiwe.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "Ayikho idivaysi ye-AllShare exhunyiwe.  AllShare ikuvumela ukuthi ujabulele okuningi okuqukethwe kwi-PC noma idivaysi kwisikirini se-TV yakho ngaphandle kokuxhuma ezinye izintambo. Ukuze uthole olunye ulwazi, iya kwibhulu lolwazi.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Khetha ithuluzi ukuvula lefayela.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Xhuma ithuluzi eliqukethe amafayela azovulwa.";
sf.lang.COM_SELECT_PHOTOS = "Khetha iisithomb.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "<<A>> (iz)isithmb sikhethw.";
sf.lang.COM_SELECT_VIDEOS = "Khetha iividiyo.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "<<A>> (ama)vidiyo akhethiw.";
sf.lang.COM_SELECT_SONGS = "Khetha izingoma.";
sf.lang.COM_MIX_SONGS_SELECTED = "<<A>> (iz)ingoma ekhethiw.";
sf.lang.COM_SELECT_A_PHOTO = "Khetha isithombe.";
sf.lang.COM_SELECT_A_VIDEO = "Khetha ividiyo.";
sf.lang.COM_SELECT_A_SONG = "Khetha ingoma.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Iya Kuhlu Lwethuluzi";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Iya Kwfolida Ephezulu";
sf.lang.COM_UPLOAD_COMPLETE = "Ukulayisha Kuphlel.";
sf.lang.TV_SID_UPLOAD_FAILED = "Ukulayisha kuhlulekile.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "Akunadivaysi exhunyiwe.";
sf.lang.TV_UPLOADING_PROGRESS = "Iyalayisha…";
sf.lang.SID_TOTAL_SIZE_COLON = "Inani Losayizi:";
sf.lang.TV_SID_TOTAL = "Ithoth";
sf.lang.SID_SIZE_M_FILE_SIZE = "Usayizi";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MB";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "u-January";
sf.lang.SID_FEBRUARY = "u-February";
sf.lang.SID_MARCH = "u-March";
sf.lang.SID_APRIL = "u-April";
sf.lang.SID_MAY_FULL = "u-May";
sf.lang.SID_JUNE = "u-June";
sf.lang.SID_JULY = "u-July";
sf.lang.SID_AUGUST = "u-August";
sf.lang.SID_SEPTEMBER = "u-September";
sf.lang.SID_OCTOBER = "u-October";
sf.lang.SID_NOVEMBER = "u-November";
sf.lang.SID_DECEMBER = "u-December";
sf.lang.SID_SUNDAY = "ISonto";
sf.lang.SID_MONDAY = "Umsomb";
sf.lang.SID_TUESDAY = "ULwesib";
sf.lang.SID_WEDNESDAY = "ULwesthath";
sf.lang.SID_THURSDAY = "ULwesine";
sf.lang.SID_FRIDAY = "ULwesihl";
sf.lang.SID_SATURDAY = "UMgqibel";
sf.lang.SID_SUN = "Son";
sf.lang.SID_MON = "Mso";
sf.lang.SID_TUE = "Bil";
sf.lang.SID_WED = "Tha";
sf.lang.SID_THU = "Sin";
sf.lang.SID_FRI = "Hla";
sf.lang.SID_SAT = "Mgq";
sf.lang.SID_AM = "am";
sf.lang.SID_PM = "pm";
sf.lang.SID_AM_PM = "am/pm";
sf.lang.SID_HOUR = "Ihora";
sf.lang.SID_MINUTE = "Umzuzu";
sf.lang.TV_SID_PRE_NEXT_MONTH = "Ngenva/Inyanga Ezayo";
sf.lang.SID_CALENDAR = "Ikhalenda";
sf.lang.SID_DAY = "Usuku";
sf.lang.SID_MONTH = "Inyanga";
sf.lang.SID_YEAR = "Unyaka";
sf.lang.TV_SID_TIMER_SETTING = "Isikhathi";
sf.lang.TV_SID_SETTING = "Ukuhlela";
sf.lang.SID_SELECT = "Khetha";
sf.lang.SID_CHANGE_KR_CHANGE = "Ushintsho";
sf.lang.SID_JAN = "Jan";
sf.lang.SID_FEB = "Feb";
sf.lang.SID_MAR = "Mas";
sf.lang.SID_APR = "Apr";
sf.lang.SID_MAY = "Mey";
sf.lang.SID_JUN = "Jun";
sf.lang.SID_JUL = "Jul";
sf.lang.SID_AUG = "Aga";
sf.lang.SID_SEP = "Sep";
sf.lang.SID_OCT = "Okt";
sf.lang.SID_NOV = "Nov";
sf.lang.SID_DEC = "Dis";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "Uzoyiswa kuhlu lwethuluzi ngoba ithuluzi elisebenzayo alixhunyiwe.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "ungakhetha amafayili angaphezu kuka 100.";
sf.lang.COM_FILE_BROWSER = "Ibhurawuz Fayel";