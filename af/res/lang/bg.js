﻿alert("Language Bulgarian(Bul) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Услугата е недостъпна поради смущения в мрежата или услугата.";
sf.lang.SID_BLIND = "Ивици";
sf.lang.SID_CANCEL = "Отказ";
sf.lang.SID_CHECKER = "Шахматно";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "Връзката затворена от сървъра или грешка в мрежата. Опитайте пак след проверка.";
sf.lang.SID_DESELECT_ALL = "Демаркиране всички";
sf.lang.SID_FADE1 = "Бледнее1";
sf.lang.SID_FADE2 = "Бледнее2";
sf.lang.SID_FAST = "Бързо";
sf.lang.SID_LINEAR = "Линейно";
sf.lang.SID_MIX_FILE_SELECTED = "Избран е <<A>> файл";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "Избр. <<A>> файла";
sf.lang.SID_MOVE = "Прем.";
sf.lang.SID_MUSIC_DIF_KR = "Музика";
sf.lang.SID_NONE = "Няма";
sf.lang.SID_NORMAL = "Нормална";
sf.lang.SID_NOT_SUPPORT_FORMAT = "Не можете да видите картината. Не поддържаме този формат.";
sf.lang.SID_OK = "OK";
sf.lang.SID_PATH = "Път";
sf.lang.SID_PAUSE = "Пауза";
sf.lang.SID_PHOTOS = "Снимки";
sf.lang.SID_PICTURE_SETTING = "Настройка картина";
sf.lang.SID_PLAY = "Възпр.";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "Не се поддържа функцията записване на глас за този файл.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "Не се поддържа видеосигнал за този файл.";
sf.lang.SID_PLAYER_RENDERERROR = "Грешка при изпълнението на файла.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "Файлът не може да бъде изпълнен.";
sf.lang.SID_PREV_NEXT = "Пред./Следв.";
sf.lang.SID_RANDOM = "Случайно";
sf.lang.SID_RETURN = "Връщане";
sf.lang.SID_SELECT = "Избор";
sf.lang.SID_SELECT_ALL = "Избор всички";
sf.lang.SID_SKIP = "Преск.";
sf.lang.SID_SLOW = "Бавно";
sf.lang.SID_SLOW_FAST = "Бавно/Бързо";
sf.lang.SID_SOUND_SETTING = "Настройка звук";
sf.lang.SID_SPIRAL = "Спирала";
sf.lang.SID_STAIRS = "Стълби";
sf.lang.SID_START_SLIDESHOW = "Започни слайдшоу";
sf.lang.SID_STOP_SLIDESHOW = "Спри слайдшоуто";
sf.lang.SID_TOOLS = "Инструм";
sf.lang.SID_UPPER_FOLDER = "По горна папка";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "USB не е свързан.";
sf.lang.SID_VIDEOS = "Видео";
sf.lang.SID_WIPE = "Изчиств.";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "В момента няма видео файл на разположение.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "В момента няма фото файл на разположение.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "В момента няма достъпен музикален файл.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Няма свързано у-во AllShare.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "Няма свързано у-во AllShare.  AllShare ви позволява да гледате разнообразно съдържание от компютър и мобилни устройства направо на екрана на телевизора, без да свързвате допълнителни кабели. За още информация вж. ръководството.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Изб. у-во, за да отворите файла.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Свържете у-во, съдържащо файлове за отваряне.";
sf.lang.COM_SELECT_PHOTOS = "Избор на снимки.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "Избрани <<A>> снимки.";
sf.lang.COM_SELECT_VIDEOS = "Изб.видеоклипове.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "Избрани <<A>> видеоклипа.";
sf.lang.COM_SELECT_SONGS = "Изберете песни.";
sf.lang.COM_MIX_SONGS_SELECTED = "Избрани <<A>> песни.";
sf.lang.COM_SELECT_A_PHOTO = "Изберете снимка.";
sf.lang.COM_SELECT_A_VIDEO = "Избер. видеоклип.";
sf.lang.COM_SELECT_A_SONG = "Изберете песен.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Отиване в спис. у-ва";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Отиди по-горна папка";
sf.lang.COM_UPLOAD_COMPLETE = "Качване завършено.";
sf.lang.TV_SID_UPLOAD_FAILED = "Неуспешно качване.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "Няма свързано устройство.";
sf.lang.TV_UPLOADING_PROGRESS = "Качване…";
sf.lang.SID_TOTAL_SIZE_COLON = "Сумарна големина:";
sf.lang.TV_SID_TOTAL = "Общо";
sf.lang.SID_SIZE_M_FILE_SIZE = "Размер";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MB";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "Януари";
sf.lang.SID_FEBRUARY = "Февруари";
sf.lang.SID_MARCH = "Март";
sf.lang.SID_APRIL = "Април";
sf.lang.SID_MAY_FULL = "Май";
sf.lang.SID_JUNE = "Юни";
sf.lang.SID_JULY = "Юли";
sf.lang.SID_AUGUST = "Август";
sf.lang.SID_SEPTEMBER = "Септември";
sf.lang.SID_OCTOBER = "Октомври";
sf.lang.SID_NOVEMBER = "Ноември";
sf.lang.SID_DECEMBER = "Декември";
sf.lang.SID_SUNDAY = "Неделя";
sf.lang.SID_MONDAY = "Понеделник";
sf.lang.SID_TUESDAY = "Вторник";
sf.lang.SID_WEDNESDAY = "Сряда";
sf.lang.SID_THURSDAY = "Четвъртък";
sf.lang.SID_FRIDAY = "Петък";
sf.lang.SID_SATURDAY = "Събота";
sf.lang.SID_SUN = "Нед";
sf.lang.SID_MON = "Пон";
sf.lang.SID_TUE = "Вто";
sf.lang.SID_WED = "Сря";
sf.lang.SID_THU = "Чет";
sf.lang.SID_FRI = "Пет";
sf.lang.SID_SAT = "Съб";
sf.lang.SID_AM = "am";
sf.lang.SID_PM = "pm";
sf.lang.SID_AM_PM = "am/pm";
sf.lang.SID_HOUR = "Час";
sf.lang.SID_MINUTE = "Минути";
sf.lang.TV_SID_PRE_NEXT_MONTH = "Предх./следв. месец";
sf.lang.SID_CALENDAR = "Календар";
sf.lang.SID_DAY = "Ден";
sf.lang.SID_MONTH = "Месец";
sf.lang.SID_YEAR = "Година";
sf.lang.TV_SID_TIMER_SETTING = "Настройка час";
sf.lang.TV_SID_SETTING = "Настройка";
sf.lang.SID_SELECT = "Избор";
sf.lang.SID_CHANGE_KR_CHANGE = "Смяна";
sf.lang.SID_JAN = "Ян";
sf.lang.SID_FEB = "Фев";
sf.lang.SID_MAR = "Мар";
sf.lang.SID_APR = "Апр";
sf.lang.SID_MAY = "Май";
sf.lang.SID_JUN = "Юни";
sf.lang.SID_JUL = "Юли";
sf.lang.SID_AUG = "Авг";
sf.lang.SID_SEP = "Сеп";
sf.lang.SID_OCT = "Окт";
sf.lang.SID_NOV = "Ное";
sf.lang.SID_DEC = "Дек";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "Ще бъдете пренасочени към спис. у-ва, тъй като връзката с изп. се у-во е прекъсната.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "Не може да избирате повече от 100 файла едновременно.";
sf.lang.COM_FILE_BROWSER = "Файлов браузър";