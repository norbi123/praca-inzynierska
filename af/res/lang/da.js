﻿alert("Language Danish(Dan) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Tjenesten er ikke tilgængelig pga. netværks- eller tjenestefejl.";
sf.lang.SID_BLIND = "Gardin";
sf.lang.SID_CANCEL = "Annuller";
sf.lang.SID_CHECKER = "Tern";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "Tilslutningen lukkes pga. server- eller netværksfejl. Kontroller og prøv igen.";
sf.lang.SID_DESELECT_ALL = "Fravælg alle";
sf.lang.SID_FADE1 = "Toning 1";
sf.lang.SID_FADE2 = "Toning 2";
sf.lang.SID_FAST = "Hurtig";
sf.lang.SID_LINEAR = "Lineær";
sf.lang.SID_MIX_FILE_SELECTED = "<<A>> fil valgt";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "<<A>> filer valgt";
sf.lang.SID_MOVE = "Flyt";
sf.lang.SID_MUSIC_DIF_KR = "Musik";
sf.lang.SID_NONE = "Ingen";
sf.lang.SID_NORMAL = "Normal";
sf.lang.SID_NOT_SUPPORT_FORMAT = "Du kan ikke se billedet. Vi understøtter ikke formen.";
sf.lang.SID_OK = "OK";
sf.lang.SID_PATH = "Sti";
sf.lang.SID_PAUSE = "Pause";
sf.lang.SID_PHOTOS = "Billeder";
sf.lang.SID_PICTURE_SETTING = "Billedindstilling";
sf.lang.SID_PLAY = "Afspil";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "Videoafspilning understøttes ikke for denne fil.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "Videoinput understøttes ikke for denne fil.";
sf.lang.SID_PLAYER_RENDERERROR = "Fejl ved filafspilning.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "Kan ikke afspille fil.";
sf.lang.SID_PREV_NEXT = "For./Næste";
sf.lang.SID_RANDOM = "Tilfældig";
sf.lang.SID_RETURN = "Retur";
sf.lang.SID_SELECT = "Vælg";
sf.lang.SID_SELECT_ALL = "Vælg alle";
sf.lang.SID_SKIP = "Skip";
sf.lang.SID_SLOW = "Langsom";
sf.lang.SID_SLOW_FAST = "Langsom/Hurtig";
sf.lang.SID_SOUND_SETTING = "Lydindstilling";
sf.lang.SID_SPIRAL = "Spiral";
sf.lang.SID_STAIRS = "Stjerner";
sf.lang.SID_START_SLIDESHOW = "Start diasshow";
sf.lang.SID_STOP_SLIDESHOW = "Stop diasshow";
sf.lang.SID_TOOLS = "Værktøj";
sf.lang.SID_UPPER_FOLDER = "Øverste mappe";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "USB er ikke tilsluttet.";
sf.lang.SID_VIDEOS = "Videoer";
sf.lang.SID_WIPE = "Visker";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "Der er i øjeblikket ingen tilgængelig videofil.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "Der er i øjeblikket ingen tilgængelig fotofil.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "Der er i øjeblikket ingen tilgængelig musikfil.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Ingen AllShare-enheder tilsluttet.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "Ingen AllShare-enheder tilsluttet.  Med AllShare kan du glæde dig over forskelligt indhold fra din pc eller mobile enheder lige på din tv-skærm uden at tilslutte yderligere kabler. Se i vejledningen for mere info.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Vælg en enhed for at åbne en fil.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Tilslut en enhed med filer, der skal åbnes.";
sf.lang.COM_SELECT_PHOTOS = "Vælg fotos.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "<<A>> foto(s) valgt.";
sf.lang.COM_SELECT_VIDEOS = "Vælg videoer.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "<<A>> video(er) valgt.";
sf.lang.COM_SELECT_SONGS = "Vælg sange.";
sf.lang.COM_MIX_SONGS_SELECTED = "<<A>> sang(e) valgt.";
sf.lang.COM_SELECT_A_PHOTO = "Vælg et foto.";
sf.lang.COM_SELECT_A_VIDEO = "Vælg en video.";
sf.lang.COM_SELECT_A_SONG = "Vælg en sang.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Gå til enhedsliste";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Gå til øverste mappe";
sf.lang.COM_UPLOAD_COMPLETE = "Overførsel udført.";
sf.lang.TV_SID_UPLOAD_FAILED = "Upload mislykket.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "Der er ingen tilsluttet enhed.";
sf.lang.TV_UPLOADING_PROGRESS = "Overfører…";
sf.lang.SID_TOTAL_SIZE_COLON = "Størrelse i alt:";
sf.lang.TV_SID_TOTAL = "Total";
sf.lang.SID_SIZE_M_FILE_SIZE = "Størrelse";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MB";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "Januar";
sf.lang.SID_FEBRUARY = "Februar";
sf.lang.SID_MARCH = "Marts";
sf.lang.SID_APRIL = "April";
sf.lang.SID_MAY_FULL = "Maj";
sf.lang.SID_JUNE = "Juni";
sf.lang.SID_JULY = "Juli";
sf.lang.SID_AUGUST = "August";
sf.lang.SID_SEPTEMBER = "September";
sf.lang.SID_OCTOBER = "Oktober";
sf.lang.SID_NOVEMBER = "November";
sf.lang.SID_DECEMBER = "December";
sf.lang.SID_SUNDAY = "Søndag";
sf.lang.SID_MONDAY = "Mandag";
sf.lang.SID_TUESDAY = "Tirsdag";
sf.lang.SID_WEDNESDAY = "Onsdag";
sf.lang.SID_THURSDAY = "Torsdag";
sf.lang.SID_FRIDAY = "Fredag";
sf.lang.SID_SATURDAY = "Lørdag";
sf.lang.SID_SUN = "Søn";
sf.lang.SID_MON = "Man";
sf.lang.SID_TUE = "Tirs";
sf.lang.SID_WED = "Ons";
sf.lang.SID_THU = "Tors";
sf.lang.SID_FRI = "Fre";
sf.lang.SID_SAT = "Lør";
sf.lang.SID_AM = "am";
sf.lang.SID_PM = "pm";
sf.lang.SID_AM_PM = "am/pm";
sf.lang.SID_HOUR = "Time";
sf.lang.SID_MINUTE = "Minut";
sf.lang.TV_SID_PRE_NEXT_MONTH = "For./næste måned";
sf.lang.SID_CALENDAR = "Kalender";
sf.lang.SID_DAY = "Dato";
sf.lang.SID_MONTH = "Måned";
sf.lang.SID_YEAR = "År";
sf.lang.TV_SID_TIMER_SETTING = "Indstil. af tid";
sf.lang.TV_SID_SETTING = "Indstill.";
sf.lang.SID_SELECT = "Vælg";
sf.lang.SID_CHANGE_KR_CHANGE = "Skift";
sf.lang.SID_JAN = "Jan";
sf.lang.SID_FEB = "Feb";
sf.lang.SID_MAR = "Mar";
sf.lang.SID_APR = "Apr";
sf.lang.SID_MAY = "Maj";
sf.lang.SID_JUN = "Jun";
sf.lang.SID_JUL = "Jul";
sf.lang.SID_AUG = "Aug";
sf.lang.SID_SEP = "Sep";
sf.lang.SID_OCT = "Okt";
sf.lang.SID_NOV = "Nov";
sf.lang.SID_DEC = "Dec";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "Du bliver sendt videre til enhedslisten, fordi enheden i brug er blevet frakoblet.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "Du kan ikke vælge mere end 100 filer på samme tid.";
sf.lang.COM_FILE_BROWSER = "Filbrowser";