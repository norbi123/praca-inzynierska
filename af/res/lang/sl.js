﻿alert("Language Slovenian(Slv) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Storitev ni na voljo zaradi motenj omrežja ali storitve.";
sf.lang.SID_BLIND = "Roleta";
sf.lang.SID_CANCEL = "Prekliči.";
sf.lang.SID_CHECKER = "Šahovnica";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "Povezava je bila prekinjena zaradi napake omrežja ali strežnika. Preverite in poskusite znova.";
sf.lang.SID_DESELECT_ALL = "Prekliči izbiro.";
sf.lang.SID_FADE1 = "Pojem. 1";
sf.lang.SID_FADE2 = "Pojem. 2";
sf.lang.SID_FAST = "Hitro";
sf.lang.SID_LINEAR = "Linearno";
sf.lang.SID_MIX_FILE_SELECTED = "Izbrana je <<A>> datoteka.";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "Izbranih je <<A>> datotek.";
sf.lang.SID_MOVE = "Prema.";
sf.lang.SID_MUSIC_DIF_KR = "Glasba";
sf.lang.SID_NONE = "Brez";
sf.lang.SID_NORMAL = "Normalno";
sf.lang.SID_NOT_SUPPORT_FORMAT = "Slika ni prikazana. Nepodprta oblika.";
sf.lang.SID_OK = "V redu.";
sf.lang.SID_PATH = "Pot";
sf.lang.SID_PAUSE = "Premor";
sf.lang.SID_PHOTOS = "Fotografije";
sf.lang.SID_PICTURE_SETTING = "Nastavitev slike";
sf.lang.SID_PLAY = "Predvajaj";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "Snemanje glasu ni podprto za to datoteko.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "Video vhod ni podprt za to datoteko.";
sf.lang.SID_PLAYER_RENDERERROR = "Napaka pri predvajanju datoteke.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "Predvajanje datoteke ni mogoče.";
sf.lang.SID_PREV_NEXT = "Nazaj/Naprej";
sf.lang.SID_RANDOM = "Naključno";
sf.lang.SID_RETURN = "Nazaj";
sf.lang.SID_SELECT = "Izberi";
sf.lang.SID_SELECT_ALL = "Izberi vse";
sf.lang.SID_SKIP = "Preskoči.";
sf.lang.SID_SLOW = "Počasi";
sf.lang.SID_SLOW_FAST = "Počasi/Hitro";
sf.lang.SID_SOUND_SETTING = "Nastavitev zvoka";
sf.lang.SID_SPIRAL = "Spirala";
sf.lang.SID_STAIRS = "Stopnice";
sf.lang.SID_START_SLIDESHOW = "Začni diaprojekcijo.";
sf.lang.SID_STOP_SLIDESHOW = "Ustavi diaprojekcijo.";
sf.lang.SID_TOOLS = "Orodja";
sf.lang.SID_UPPER_FOLDER = "Zgornja mapa";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "USB ni povezan.";
sf.lang.SID_VIDEOS = "Videi";
sf.lang.SID_WIPE = "Brisanje";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "Trenutno ni na voljo nobena video datoteka.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "Trenutno ni na voljo nobena foto datoteka.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "Trenutno ni na voljo nobena glasbena datoteka.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Povez. ni nobena naprava AllShare.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "Povezana ni nobena naprava AllShare.  Funkcija AllShare brez povezovanja dodatnih kablov omogoča uživanje v različnih vsebinah iz računalnika ali mobilnih naprav neposredno na TV zaslonu. Za več informacij glejte priročnik.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Izberite napravo, da odpr. datot.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Povežite napravo z datotekami, ki jih žel. odpr.";
sf.lang.COM_SELECT_PHOTOS = "Izberite fotogr.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "Izbranih fotografij: <<A>>.";
sf.lang.COM_SELECT_VIDEOS = "Izberite videe.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "Izbranih videov: <<A>>.";
sf.lang.COM_SELECT_SONGS = "Izberite pesmi.";
sf.lang.COM_MIX_SONGS_SELECTED = "Izbranih pesmi: <<A>>.";
sf.lang.COM_SELECT_A_PHOTO = "Izber. fotografij.";
sf.lang.COM_SELECT_A_VIDEO = "Izberite video.";
sf.lang.COM_SELECT_A_SONG = "Izberite pesem.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Pojdi na sezn. napr.";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Pojdi v zgornjo mapo";
sf.lang.COM_UPLOAD_COMPLETE = "Nalaganje končano.";
sf.lang.TV_SID_UPLOAD_FAILED = "Nalaganje v splet ni uspelo.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "Nobena naprava ni povezana.";
sf.lang.TV_UPLOADING_PROGRESS = "Nalaganje …";
sf.lang.SID_TOTAL_SIZE_COLON = "Skupna velikost:";
sf.lang.TV_SID_TOTAL = "Skupno";
sf.lang.SID_SIZE_M_FILE_SIZE = "Velikost";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MB";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "Januar";
sf.lang.SID_FEBRUARY = "Februar";
sf.lang.SID_MARCH = "Marec";
sf.lang.SID_APRIL = "April";
sf.lang.SID_MAY_FULL = "Maj";
sf.lang.SID_JUNE = "Junij";
sf.lang.SID_JULY = "Julij";
sf.lang.SID_AUGUST = "Avgust";
sf.lang.SID_SEPTEMBER = "September";
sf.lang.SID_OCTOBER = "Oktober";
sf.lang.SID_NOVEMBER = "November";
sf.lang.SID_DECEMBER = "December";
sf.lang.SID_SUNDAY = "Nedelja";
sf.lang.SID_MONDAY = "Ponedeljek";
sf.lang.SID_TUESDAY = "Torek";
sf.lang.SID_WEDNESDAY = "Sreda";
sf.lang.SID_THURSDAY = "Četrtek";
sf.lang.SID_FRIDAY = "Petek";
sf.lang.SID_SATURDAY = "Sobota";
sf.lang.SID_SUN = "Ned";
sf.lang.SID_MON = "Pon";
sf.lang.SID_TUE = "Tor";
sf.lang.SID_WED = "Sre";
sf.lang.SID_THU = "Čet";
sf.lang.SID_FRI = "Pet";
sf.lang.SID_SAT = "Sob";
sf.lang.SID_AM = "dop.";
sf.lang.SID_PM = "pop.";
sf.lang.SID_AM_PM = "dopoldne/popoldne (am/pm)";
sf.lang.SID_HOUR = "Ura";
sf.lang.SID_MINUTE = "Minuta";
sf.lang.TV_SID_PRE_NEXT_MONTH = "Prejš./nasl. mesec";
sf.lang.SID_CALENDAR = "Koledar";
sf.lang.SID_DAY = "Dan";
sf.lang.SID_MONTH = "Mesec";
sf.lang.SID_YEAR = "Leto";
sf.lang.TV_SID_TIMER_SETTING = "Časovna nast.";
sf.lang.TV_SID_SETTING = "Nastavitev";
sf.lang.SID_SELECT = "Izberi";
sf.lang.SID_CHANGE_KR_CHANGE = "Spremeni.";
sf.lang.SID_JAN = "Jan";
sf.lang.SID_FEB = "Feb";
sf.lang.SID_MAR = "Mar";
sf.lang.SID_APR = "Apr";
sf.lang.SID_MAY = "Maj";
sf.lang.SID_JUN = "Jun";
sf.lang.SID_JUL = "Jul";
sf.lang.SID_AUG = "Avg";
sf.lang.SID_SEP = "Sep";
sf.lang.SID_OCT = "Okt";
sf.lang.SID_NOV = "Nov";
sf.lang.SID_DEC = "Dec";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "Usmerjeni boste na seznam naprav, ker je bila povezava z napravo v uporabi prekinjena.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "Hkrati lahko izberete največ 100 datotek.";
sf.lang.COM_FILE_BROWSER = "Brskal. po dat.";