﻿alert("Language Igbo(Igb) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Ije ozi ya adịghị n'ihi mmebi netwọọk maọbụ nke ije ozi ya.";
sf.lang.SID_BLIND = "Isi";
sf.lang.SID_CANCEL = "Kagbuo";
sf.lang.SID_CHECKER = "Ihe nlele";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "The connection is closed due to network or server error. Please check and retry.";
sf.lang.SID_DESELECT_ALL = "Wepu nhọrọ ha niile";
sf.lang.SID_FADE1 = "Fade 1";
sf.lang.SID_FADE2 = "Fade 2";
sf.lang.SID_FAST = "Ọsọ ọsọ";
sf.lang.SID_LINEAR = "Ogologo";
sf.lang.SID_MIX_FILE_SELECTED = "Ahọtala faịlị <<A>>";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "Ahl. Fl.  Nl. <<A>>";
sf.lang.SID_MOVE = "Bgrịa.";
sf.lang.SID_MUSIC_DIF_KR = "Egwu";
sf.lang.SID_NONE = "Enweghị";
sf.lang.SID_NORMAL = "Otu okwesiri";
sf.lang.SID_NOT_SUPPORT_FORMAT = "Ịnweghị ike ịhụ eserese ahụ. Anyị anaghị akwado ụdị ahụ.";
sf.lang.SID_OK = "ỌDM.";
sf.lang.SID_PATH = "Ụzọ";
sf.lang.SID_PAUSE = "Kwụsịtụ";
sf.lang.SID_PHOTOS = "Foto";
sf.lang.SID_PICTURE_SETTING = "Ntọala Eserese";
sf.lang.SID_PLAY = "Kpọọ";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "Voice recording not supported for this file.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "Video input not supported for this file.";
sf.lang.SID_PLAYER_RENDERERROR = "Mperi na-ịkpọ faịlụ.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "Unable to play file.";
sf.lang.SID_PREV_NEXT = "Gara aga/Osote";
sf.lang.SID_RANDOM = "Enweghị usoro";
sf.lang.SID_RETURN = "Laghachi";
sf.lang.SID_SELECT = "Họrọ";
sf.lang.SID_SELECT_ALL = "Họrọ ha niile";
sf.lang.SID_SKIP = "Mafee";
sf.lang.SID_SLOW = "Nwayọ";
sf.lang.SID_SLOW_FAST = "Nwayọọ/Ọsọ ọsọ";
sf.lang.SID_SOUND_SETTING = "Ntọala Ụda";
sf.lang.SID_SPIRAL = "Okirikiri";
sf.lang.SID_STAIRS = "Otu otu n'elu";
sf.lang.SID_START_SLIDESHOW = "Malite Ngosi mmịfe";
sf.lang.SID_STOP_SLIDESHOW = "Kwụsị Ngosi mmịfe";
sf.lang.SID_TOOLS = "Ngwa ọrụ";
sf.lang.SID_UPPER_FOLDER = "Folda dị elu";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "Ejikọghị USB.";
sf.lang.SID_VIDEOS = "V.dị ic.Ic.";
sf.lang.SID_WIPE = "Hichaa";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "Faịlụ vido adịghị ugbu a.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "Faịlụ Foto adịghị ugbu a.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "Ọnweghị faịlụ egwu dị ugbu a.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Ọnweghị ngwaọrụ AllShare ejikọrọ.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "Ọnweghị ngwaọrụ AllShare ejikọrọ.  AllShare na-enye gị ohere ịnwe ọhụụ ọtụtụ ọdịnaya site na PC ma ọ bụ ngwaọrụ mkpanaaka gị na ihuenyo TV gị na-ejikọghị eriri ọzọ. Maka ozi ndị ọzọ, tụnye aka na ntuziaka ahụ.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Họrọ otu ngwaọrụ iji mepee faịlụ.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Jikọọ ngwaọrụ nwere faịlụ ndị a ga-emepe.";
sf.lang.COM_SELECT_PHOTOS = "Họrọ foto.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "Ahọpụtara foto <<A>>.";
sf.lang.COM_SELECT_VIDEOS = "Họrọ vidiyo.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "Ahọpụtara vidiyo <<A>>.";
sf.lang.COM_SELECT_SONGS = "Họrọ egwu.";
sf.lang.COM_MIX_SONGS_SELECTED = "Ahọpụtara egwu <<A>>.";
sf.lang.COM_SELECT_A_PHOTO = "Họrọ foto.";
sf.lang.COM_SELECT_A_VIDEO = "Họrọ otu vidiyo.";
sf.lang.COM_SELECT_A_SONG = "Họrọ otu egwu.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Gaa n Ndpta Ngwaọrụ";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Gaa na Folda Dị N'elu";
sf.lang.COM_UPLOAD_COMPLETE = "Mbugo emezuola.";
sf.lang.TV_SID_UPLOAD_FAILED = "Mbugo agaghị.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "Enweghị ngwaọrụ ejikọrọ.";
sf.lang.TV_UPLOADING_PROGRESS = "Na-ebugo...";
sf.lang.SID_TOTAL_SIZE_COLON = "Mgbakọta Nha:";
sf.lang.TV_SID_TOTAL = "Mgbakọta";
sf.lang.SID_SIZE_M_FILE_SIZE = "Nha";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MB";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "Jenụwarị";
sf.lang.SID_FEBRUARY = "Febụwarị";
sf.lang.SID_MARCH = "Machị";
sf.lang.SID_APRIL = "Eprelu";
sf.lang.SID_MAY_FULL = "Mey";
sf.lang.SID_JUNE = "Juun";
sf.lang.SID_JULY = "Julaị";
sf.lang.SID_AUGUST = "Ọgọst";
sf.lang.SID_SEPTEMBER = "Septemba";
sf.lang.SID_OCTOBER = "?ktoba";
sf.lang.SID_NOVEMBER = "Nọvemba";
sf.lang.SID_DECEMBER = "Disemba";
sf.lang.SID_SUNDAY = "Sọnde";
sf.lang.SID_MONDAY = "Mọnde";
sf.lang.SID_TUESDAY = "Tusde";
sf.lang.SID_WEDNESDAY = "Wednesde";
sf.lang.SID_THURSDAY = "Tọsde";
sf.lang.SID_FRIDAY = "Fraịde";
sf.lang.SID_SATURDAY = "Satọde";
sf.lang.SID_SUN = "Sọn";
sf.lang.SID_MON = "Mọn";
sf.lang.SID_TUE = "Tuu";
sf.lang.SID_WED = "Wed";
sf.lang.SID_THU = "Tọs";
sf.lang.SID_FRI = "Fra";
sf.lang.SID_SAT = "Sat";
sf.lang.SID_AM = "ụt.";
sf.lang.SID_PM = "ab.";
sf.lang.SID_AM_PM = "ụt./ab.";
sf.lang.SID_HOUR = "Elekere";
sf.lang.SID_MINUTE = "Nkeji";
sf.lang.TV_SID_PRE_NEXT_MONTH = "Ọnwa Ikpez/Na-esote";
sf.lang.SID_CALENDAR = "Kalịnda";
sf.lang.SID_DAY = "Ehihie";
sf.lang.SID_MONTH = "Ọnwā";
sf.lang.SID_YEAR = "Afọ";
sf.lang.TV_SID_TIMER_SETTING = "Ntọala Oge";
sf.lang.TV_SID_SETTING = "Ntọala";
sf.lang.SID_SELECT = "Họrọ";
sf.lang.SID_CHANGE_KR_CHANGE = "Gbanwee";
sf.lang.SID_JAN = "Jen";
sf.lang.SID_FEB = "Feb";
sf.lang.SID_MAR = "Mar";
sf.lang.SID_APR = "Epr";
sf.lang.SID_MAY = "Mey";
sf.lang.SID_JUN = "Jun";
sf.lang.SID_JUL = "Jul";
sf.lang.SID_AUG = "Ọgọ";
sf.lang.SID_SEP = "Sep";
sf.lang.SID_OCT = "Ọkt";
sf.lang.SID_NOV = "Nọv";
sf.lang.SID_DEC = "Dis";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "A ga-eduje gị na ndepụta ngwaọrụ n'ihi akwụpụla ngwaọrụ a na-eji.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "Ịnweghị ike ịhọrọ karịa faịlụ 100 n'otu oge.";
sf.lang.COM_FILE_BROWSER = "Ih Nchghr Faịlụ";