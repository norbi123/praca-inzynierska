﻿alert("Language Romanian(Rom) is loaded");
sf = sf || {};
sf.lang = sf.lang || {};
sf.lang.SID_ALERT_NETWORKERROR_MSG = "Serviciu indisponibil din cauza interfer. cu reţeaua sau serviciul.";
sf.lang.SID_BLIND = "Jaluzea";
sf.lang.SID_CANCEL = "Anulare";
sf.lang.SID_CHECKER = "Tablă şah";
sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG = "Conexiune închisă: eroare de server sau de reţea. Reîncercaţi după verificare.";
sf.lang.SID_DESELECT_ALL = "Deselectare toate";
sf.lang.SID_FADE1 = "Atenuare1";
sf.lang.SID_FADE2 = "Atenuare2";
sf.lang.SID_FAST = "Rapid";
sf.lang.SID_LINEAR = "Liniar";
sf.lang.SID_MIX_FILE_SELECTED = "<<A>> fişier sel.";
sf.lang.SID_MIX_FILES_SELECTED_MSG = "<<A>> fişiere sel.";
sf.lang.SID_MOVE = "Mutare";
sf.lang.SID_MUSIC_DIF_KR = "Muzică";
sf.lang.SID_NONE = "Fără";
sf.lang.SID_NORMAL = "Normal";
sf.lang.SID_NOT_SUPPORT_FORMAT = "Nu puteţi vedea imaginea. Formatul nu este acceptat.";
sf.lang.SID_OK = "OK";
sf.lang.SID_PATH = "Cale";
sf.lang.SID_PAUSE = "Pauză";
sf.lang.SID_PHOTOS = "Fotografii";
sf.lang.SID_PICTURE_SETTING = "Setări imagine";
sf.lang.SID_PLAY = "Redare";
sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO = "Înregistrarea vocii nu este acceptată pentru acest fişier.";
sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO = "Semnalul video nu este acceptat pentru acest fişier.";
sf.lang.SID_PLAYER_RENDERERROR = "Eroare la redarea fişierului.";
sf.lang.SID_PLAYER_UNSPPORT_ABBR_10 = "Nu se poate reda fişierul.";
sf.lang.SID_PREV_NEXT = "Anter./Urm.";
sf.lang.SID_RANDOM = "Aleatoriu";
sf.lang.SID_RETURN = "Revenire";
sf.lang.SID_SELECT = "Selectare";
sf.lang.SID_SELECT_ALL = "Select. globală";
sf.lang.SID_SKIP = "Omitere";
sf.lang.SID_SLOW = "Lent";
sf.lang.SID_SLOW_FAST = "Lent/Rapid";
sf.lang.SID_SOUND_SETTING = "Setări sunet";
sf.lang.SID_SPIRAL = "Spirală";
sf.lang.SID_STAIRS = "Scări";
sf.lang.SID_START_SLIDESHOW = "Start expunere diap.";
sf.lang.SID_STOP_SLIDESHOW = "Stop expunere diap.";
sf.lang.SID_TOOLS = "Instr.";
sf.lang.SID_UPPER_FOLDER = "Folderul superior";
sf.lang.SID_USB = "USB";
sf.lang.SID_USB_IS_NOT_CONNECTED = "Intrarea USB nu este conectată.";
sf.lang.SID_VIDEOS = "Videocl.";
sf.lang.SID_WIPE = "Ştergere";
sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG = "În prezent nu sunt disponibile fişiere video.";
sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG = "În prezent nu sunt disponibile fişiere foto.";
sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG = "Nu sunt disponibile fişiere de muzică.";
sf.lang.BDP_SID_HOME_POPUP_ERR_NO_ALLSHARE_TEXT = "Niciun disp. AllShare conectat.";
sf.lang.SID_DLNA = "DLNA";
sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG = "Niciun dispozitiv AllShare conectat.  Cu AllShare vă veţi bucura de diverse tipuri de conţinut de pe PC sau de pe dispozitive mobile, direct pe ecranul lat TV, fără să conectaţi cabluri suplimentare. Pentru informaţii suplimentare, consultaţi manualul.";
sf.lang.COM_CHOOSE_A_DEVICE_TO_OPEN_A_FILE = "Sel. disp. pt. a deschide un fiş.";
sf.lang.COM_CONNECT_A_DEVICE_THAT_CONTAINS_FILES_TO_OPEN = "Conectaţi un dispozitiv cu fişiere de deschis.";
sf.lang.COM_SELECT_PHOTOS = "Sel. fotografii.";
sf.lang.COM_MIX_PHOTOS_SELECTED = "Fotogr. selectate: <<A>>.";
sf.lang.COM_SELECT_VIDEOS = "Select. videocl.";
sf.lang.COM_MIX_VIDEOS_SELECTED = "Videocl. selectate: <<A>>.";
sf.lang.COM_SELECT_SONGS = "Select. melodii.";
sf.lang.COM_MIX_SONGS_SELECTED = "Melodii selectate: <<A>>.";
sf.lang.COM_SELECT_A_PHOTO = "Sel. o fotografie.";
sf.lang.COM_SELECT_A_VIDEO = "Sel. un videoclip.";
sf.lang.COM_SELECT_A_SONG = "Sel. o melodie.";
sf.lang.COM_GO_TO_DEVICE_LIST = "Accesare Listă disp.";
sf.lang.COM_GO_TO_UPPER_FOLDER = "Acces. folder super.";
sf.lang.COM_UPLOAD_COMPLETE = "Încărcare finaliz.";
sf.lang.TV_SID_UPLOAD_FAILED = "Încărcare nereuşită.";
sf.lang.COM_BDP_SID_POPUP_MSG_NO_CONNECTED_DEV_OF_MYCONTENTS_TEXT = "Niciun dispozitiv conectat.";
sf.lang.TV_UPLOADING_PROGRESS = "Se încarcă…";
sf.lang.SID_TOTAL_SIZE_COLON = "Dimensiune totală:";
sf.lang.TV_SID_TOTAL = "Total";
sf.lang.SID_SIZE_M_FILE_SIZE = "Dimensiune";
sf.lang.SID_KB = "KB";
sf.lang.SID_MB = "MO";
sf.lang.SID_GB = "GB";
sf.lang.SID_TB = "TB";
sf.lang.SID_JANUARY = "Ianuarie";
sf.lang.SID_FEBRUARY = "Februarie";
sf.lang.SID_MARCH = "Martie";
sf.lang.SID_APRIL = "Aprilie";
sf.lang.SID_MAY_FULL = "Mai";
sf.lang.SID_JUNE = "Iunie";
sf.lang.SID_JULY = "Iulie";
sf.lang.SID_AUGUST = "August";
sf.lang.SID_SEPTEMBER = "Septembrie";
sf.lang.SID_OCTOBER = "Octombrie";
sf.lang.SID_NOVEMBER = "Noiembrie";
sf.lang.SID_DECEMBER = "Decembrie";
sf.lang.SID_SUNDAY = "Duminică";
sf.lang.SID_MONDAY = "Luni";
sf.lang.SID_TUESDAY = "Marţi";
sf.lang.SID_WEDNESDAY = "Miercuri";
sf.lang.SID_THURSDAY = "Joi";
sf.lang.SID_FRIDAY = "Vineri";
sf.lang.SID_SATURDAY = "Sâmbătă";
sf.lang.SID_SUN = "Du";
sf.lang.SID_MON = "Lu.";
sf.lang.SID_TUE = "Ma.";
sf.lang.SID_WED = "Mi.";
sf.lang.SID_THU = "Jo.";
sf.lang.SID_FRI = "Vin.";
sf.lang.SID_SAT = "Sâm";
sf.lang.SID_AM = "a.m.";
sf.lang.SID_PM = "p.m.";
sf.lang.SID_AM_PM = "am/pm";
sf.lang.SID_HOUR = "Oră";
sf.lang.SID_MINUTE = "Minut";
sf.lang.TV_SID_PRE_NEXT_MONTH = "Luna ant./următoare";
sf.lang.SID_CALENDAR = "Calendar";
sf.lang.SID_DAY = "Zi";
sf.lang.SID_MONTH = "Lună";
sf.lang.SID_YEAR = "An";
sf.lang.TV_SID_TIMER_SETTING = "Setare oră";
sf.lang.TV_SID_SETTING = "Setare";
sf.lang.SID_SELECT = "Selectare";
sf.lang.SID_CHANGE_KR_CHANGE = "Modific.";
sf.lang.SID_JAN = "Ian.";
sf.lang.SID_FEB = "Feb.";
sf.lang.SID_MAR = "Mar.";
sf.lang.SID_APR = "Apr";
sf.lang.SID_MAY = "Mai";
sf.lang.SID_JUN = "Iun.";
sf.lang.SID_JUL = "Iul.";
sf.lang.SID_AUG = "Aug.";
sf.lang.SID_SEP = "Sep.";
sf.lang.SID_OCT = "Oct.";
sf.lang.SID_NOV = "Nov.";
sf.lang.SID_DEC = "Dec.";
sf.lang.COM_MOVE_LIST_DEVICE_DISCONNECTED_MSG = "Se va afişa lista de dispozitive, deoarece dispozitivul în uz a fost deconectat.";
sf.lang.COM_CANT_SELECT_MORE_THAN_100_FILES_MSG = "Puteţi selecta maximum 100 de fişiere simultan.";
sf.lang.COM_FILE_BROWSER = "Browser fişiere";