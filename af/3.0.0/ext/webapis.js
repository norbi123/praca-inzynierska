﻿/** 
 * Title: webapis.js
 * version : 
 * date : 2012-06-22
 * description : ChannelInfo 수정, Valencia 호환 수정. webapis.js , serviceapis.js 통합 첫 버전
 * 
 * ( API 추가시 유의 사항 )
 * 1) 10년도, 11년도, 12년도 plaform에서 각각 지원하는 플러그인이 다르므로 분기하여 처리 필요.
 * 	  -> 10년도 plaform : SEF 미 지원, Legacy plugin 사용
 *    	 11년도 plaform : SEF 특정 기능 미 지원
 *    	 12년도 plaform : SEF Plugin 사용
 * 
 * 2) 새로운 Plugin 등록
 * 	  -> SEFPluginName에 추가
 * 
 * 3) Plugin 호출 방법
 *    -> webapis._plugin("plugin Name", "API Name", Parameter1, Parameter2...);
 *    -> 사용 예) webapis._plugin("AUDIO", "SetVolume", 10);
 * 
 * 4) 명명 규칙
 * 	  -> 변수, 함수명 모두 소문자로 시작하는 CamelCase를 사용
 *    -> 함수, ENUM을 가지는 NameSpace명이나 클래스명은 대문자로 시작
 * 
 */
var webapis = window.webapis || {};

(function () {
    try {
        var useragent = window.navigator.userAgent + '';
        //2012: useragent = 'Mozilla/5.0 (SmartHub; SMART-TV; U; Linux/SmartTV; Maple2012) AppleWebKit/534.7 (KHTML, like Gecko) SmartTV Safari/534.7';
        //2013: useragent = 'Mozilla/5.0 (SmartHub; SMART-TV; U; Linux/SmartTV+2013; Maple2012) AppleWebKit/535.20+ (KHTML, like Gecko) SmartTV Safari/535.20+';
        
        if (!useragent.match(/smarttv/i) && !useragent.match(/maple/i)) {
            webapis.pcmode = true;
        }
        else {
            webapis.pcmode = false;
        }
    }
    catch (e) {
    }
})();

webapis.platform = 'Samsung SmartTV';
webapis.ver = '1.2.11';

/*  
 * brief Root API for all device APIs
 */ 
var webapis_FeatureArray = new Array();
var webapis_ParamArray = new Array();

webapis_ParamArray.push(null, null);

webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/webapis", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/network", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/filesystem", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/displaycontrol", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/audiocontrol", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/tv/info", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/tv/channel", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/tv/window", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/avplay", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/imageplay", false, webapis_ParamArray));
webapis_FeatureArray.push(new Feature("http://samsungapps.com/api/mediashare.mscp", false, webapis_ParamArray));

	
webapis.listAvailableFeatures = function () {
	alert("webapis_listAvailableFeatures()");
	return webapis_FeatureArray;
}

webapis.listActivatedFeatures = function () {
	return webapis_FeatureArray;
}

function Feature (uri, required, params) {
	this.uri = uri;
	this.required = required;
	this.params = params;
}

function Param (name, value) {
	this.name = name;
	this.value = value;
}

//=============================================================================
// WebAPIException and WebAPIError helpers
//=============================================================================

function ErrorsHelper()
{
} // function ErrorsHelper


//-------------------------------------------------------------------------
// next object keeps description of possible error
ErrorsHelper.ErrorValue = function(name, legacyCode, legacyCodeName, description)
{
    this.name = name;

    if (legacyCode == null)
    {
        this.legacyCode = 0;
    }
    else
    {
        this.legacyCode = legacyCode;
    }

    if (legacyCodeName == null)
    {
        this.legacyCodeName = 0;
    }
    else
    {
        this.legacyCodeName = legacyCodeName;
    }

    this.description = description;


    this.show = function()
    {
        alert("ErrorValue { " + toStringDebug() + "}");
    }

    this.toStringDebug = function()
    {
        var str = "";
        str += "name:" + this.name;
        str += "   legacyCode:" + this.legacyCode;
        str += "   legacyCodeName:" + this.legacyCodeName;
        str += "   description:" + this.description;
        return str;
    }

} // function ErrorValue


//-------------------------------------------------------------------------
ErrorsHelper.attachReadonlyProperty = function(object, propertyName, propertyValue)
{
    // alert("attachReadonlyProperty(, propertyName:" + propertyName + ", propertyValue:" + propertyValue);
    var getter = function()
    {
        return propertyValue;
    }

    try
    {
        object.__defineGetter__(propertyName, getter);
    }
    catch(e)
    {
         alert("ErrorsHelper.attachReadonlyProperty __defineGetter__ cause exception");
    }
}


ErrorsHelper.errorValuesList = []; // this array keeps list of possible errors


//-------------------------------------------------------------------------
ErrorsHelper.registerErrorValue = function(name, legacyCode, legacyCodeName, description)
{
    var exceptionValue = new ErrorsHelper.ErrorValue(name, legacyCode, legacyCodeName, description);
    this.errorValuesList.push(exceptionValue);
}


//-------------------------------------------------------------------------
// Searching error value object by error name
ErrorsHelper.searchByName = function(name)
{
    var errorValue;

    for (idx = 0; idx < this.errorValuesList.length; idx++)
    {
        var errorValueItem = this.errorValuesList[idx];
// alert ("ErrorsHelper.searchByName()   idx:" + idx + "   errorValueItem.name:" + errorValueItem.name + "   name:" + name);
        if (errorValueItem.name == name)
        {
            errorValue = errorValueItem;
            break;
        }
    }

    return errorValue;
}


// Searching error value object by numeric legacy code
ErrorsHelper.searchByLegacyCode = function(legacyCode)
{
    var errorValue;

    for (idx = 0; idx < this.errorValuesList.length; idx++)
    {
        var errorValueItem = this.errorValuesList[idx];
// alert ("ErrorsHelper.searchByLegacyCode()   idx:" + idx + "   errorValueItem.name:" + errorValueItem.name + "   name:" + name);
        if (errorValueItem.legacyCode == legacyCode)
        {
            errorValue = errorValueItem;
            break;
        }
    }

    return errorValue;
}


// Searching error value object by legacy code as string
ErrorsHelper.searchByLegacyCodeName = function(legacyCodeName)
{
    var errorValue;

    for (idx = 0; idx < this.errorValuesList.length; idx++)
    {
        var errorValueItem = this.errorValuesList[idx];
// alert ("ErrorsHelper.searchByLegacyCodeName()   idx:" + idx + "   errorValueItem.name:" + errorValueItem.name + "   name:" + name);
        if (errorValueItem.legacyCodeName == legacyCodeName)
        {
            errorValue = errorValueItem;
            break;
        }
    }

    return errorValue;
}


//-------------------------------------------------------------------------
// Registering error values


// additional errors
ErrorsHelper.registerErrorValue("UnknownError", 0, "UNKNOWN_ERR", "An unknown error has occurred.");
ErrorsHelper.registerErrorValue("InvalidValuesError", 0, "INVALID_VALUES_ERR", "The content of an object does not contain valid values.");
ErrorsHelper.registerErrorValue("IOError", 0, "IO_ERR", "An error occurred in communication with the underlying implementation that meant the requested method could not complete.");
ErrorsHelper.registerErrorValue("OutOfMemoryError", 0, "OUT_OF_MEMORY_ERR", "");
ErrorsHelper.registerErrorValue("NotConnectedServiceError", 0, "NOT_CONNECTED_SERVICE_ERR", "");
ErrorsHelper.registerErrorValue("FailToRequestError", 0, "FAIL_TO_REQUEST_ERR", "");
ErrorsHelper.registerErrorValue("InvalidResponseError", 0, "INVALID_RESPONSE_ERR", "");
ErrorsHelper.registerErrorValue("NoAvailableNetworkError", 0, "NO_AVAILABLE_NETWORK_ERR", "");
ErrorsHelper.registerErrorValue("InvalidDeviceError", 0, "INVALID_DEVICE_ERR", "");
ErrorsHelper.registerErrorValue("NotSupportedFuncitonError", 0, "NOT_SUPPORTED_FUNCTION_ERR", "");
ErrorsHelper.registerErrorValue("NoPermisionError", 0, "NO_PERMISSION_ERR", "");
ErrorsHelper.registerErrorValue("InvalidItemError", 0, "INVALID_ITEM_ERR", "");
ErrorsHelper.registerErrorValue("FailToPlayError", 0, "FAIL_TO_PLAY_ERR", "");
ErrorsHelper.registerErrorValue("ServiceNotAvailableError", 0, "SERVICE_NOT_AVAILABLE_ERR", "");
ErrorsHelper.registerErrorValue("NetworkSlowError", 0, "NETWORK_SLOW_ERR", "");
ErrorsHelper.registerErrorValue("RenderError", 0, "RENDER_ERR", "");
ErrorsHelper.registerErrorValue("AvplayUnsupportedContainerError", 0, "AVPLAY_UNSUPPORTED_CONTAINER_ERR", "");
ErrorsHelper.registerErrorValue("AvplayUnsupportedVideoFormatError", 0, "AVPLAY_UNSUPPORTED_VIDEO_FORMAT_ERR", "");
ErrorsHelper.registerErrorValue("AvplayUnsupportedAudioFormatError", 0, "AVPLAY_UNSUPPORTED_AUDIO_FORMAT_ERR", "");
ErrorsHelper.registerErrorValue("AvplayUnsupportedVideoResolutionError", 0, "AVPLAY_UNSUPPORTED_VIDEO_RESOLUTION_ERR", "");
ErrorsHelper.registerErrorValue("AvplayUnsupportedVideoFramerateError", 0, "AVPLAY_UNSUPPORTED_VIDEO_FRAMERATE_ERR", "");
ErrorsHelper.registerErrorValue("AvplayCurruptedStreamError", 0, "AVPLAY_CURRUPTED_STREAM_ERR", "");



// Errors defined in web api spec
ErrorsHelper.registerErrorValue("IndexSizeError", 1, "INDEX_SIZE_ERR", "The index is not in the allowed range.");
ErrorsHelper.registerErrorValue("historical", 2, "DOMSTRING_SIZE_ERR", "The specified range of text does not fit into a DOMString."); // only in webapis.widl
ErrorsHelper.registerErrorValue("HierarchyRequestError", 3, "HIERARCHY_REQUEST_ERR", "The operation would yield an incorrect node tree.");
ErrorsHelper.registerErrorValue("WrongDocumentError", 4, "WRONG_DOCUMENT_ERR", "The object is in the wrong document.");
ErrorsHelper.registerErrorValue("InvalidCharacterError", 5, "INVALID_CHARACTER_ERR", "The string contains invalid characters.");
ErrorsHelper.registerErrorValue("historical", 6, "NO_DATA_ALLOWED_ERR", "Data is specified for a Node which does not support data."); // only in webapis.widl
ErrorsHelper.registerErrorValue("NoModificationAllowedError", 7, "NO_MODIFICATION_ALLOWED_ERR", "The object can not be modified.");
ErrorsHelper.registerErrorValue("NotFoundError", 8, "NOT_FOUND_ERR", "The object can not be found here.");
ErrorsHelper.registerErrorValue("NotSupportedError", 9, "NOT_SUPPORTED_ERR", "The operation is not supported.");
ErrorsHelper.registerErrorValue("historical", 10, "INUSE_ATTRIBUTE_ERR", "Attempt is made to add an attribute that is already in use elsewhere."); // only in webapis.widl
ErrorsHelper.registerErrorValue("InvalidStateError", 11, "INVALID_STATE_ERR", "The object is in an invalid state.");
ErrorsHelper.registerErrorValue("SyntaxError", 12, "SYNTAX_ERR", "The string did not match the expected pattern.");
ErrorsHelper.registerErrorValue("InvalidModificationError", 13, "INVALID_MODIFICATION_ERR", "The object can not be modified in this way.");
ErrorsHelper.registerErrorValue("NamespaceError", 14, "NAMESPACE_ERR", "The operation is not allowed by Namespaces in XML.");
ErrorsHelper.registerErrorValue("TypeMismatchError", 17, "TYPE_MISMATCH_ERR", "The type of the object does not match the expected type.");
ErrorsHelper.registerErrorValue("SecurityError", 18, "SECURITY_ERR", "The operation is insecure.");
ErrorsHelper.registerErrorValue("NetworkError", 19, "NETWORK_ERR", "A network error occurred.");
ErrorsHelper.registerErrorValue("AbortError", 20, "ABORT_ERR", "The operation was aborted.");
ErrorsHelper.registerErrorValue("URLMismatchError", 21, "URL_MISMATCH_ERR", "The given URL does not match another URL.");
ErrorsHelper.registerErrorValue("QuotaExceededError", 22, "QUOTA_EXCEEDED_ERR", "The quota has been exceeded.");
ErrorsHelper.registerErrorValue("TimeoutError", 23, "TIMEOUT_ERR", "The operation timed out.");
ErrorsHelper.registerErrorValue("InvalidNodeTypeError", 24, "INVALID_NODE_TYPE_ERR", "The supplied node is incorrect or has an incorrect ancestor for this operation.");
ErrorsHelper.registerErrorValue("DataCloneError", 25, "DATA_CLONE_ERR", "The object can not be cloned.");


//=============================================================================
// WebAPIException
//=============================================================================
// Helpers function used for this object creation and initialization
function WebAPIException()
{
//    this.code   // added as readonly attribute
//    this.name   // added as readonly attribute
//    this.meessage   // added as readonly attribute
}

WebAPIException.UNKNOWN_ERR	= 0;
WebAPIException.INDEX_SIZE_ERR = 1;
WebAPIException.DOMSTRING_SIZE_ERR = 2;
WebAPIException.HIERARCHY_REQUEST_ERR = 3;
WebAPIException.WRONG_DOCUMENT_ERR = 4;
WebAPIException.INVALID_CHARACTER_ERR = 5;
WebAPIException.NO_DATA_ALLOWED_ERR = 6;
WebAPIException.NO_MODIFICATION_ALLOWED_ERR	= 7;
WebAPIException.NOT_FOUND_ERR = 8;
WebAPIException.NOT_SUPPORTED_ERR = 9;
WebAPIException.INUSE_ATTRIBUTE_ERR = 10;
WebAPIException.INVALID_STATE_ERR = 11;
WebAPIException.SYNTAX_ERR = 12;
WebAPIException.INVALID_MODIFICATION_ERR = 13;
WebAPIException.NAMESPACE_ERR = 14;
WebAPIException.INVALID_ACCESS_ERR = 15;
WebAPIException.VALIDATION_ERR = 16;
WebAPIException.TYPE_MISMATCH_ERR = 17;
WebAPIException.SECURITY_ERR = 18;
WebAPIException.NETWORK_ERR = 19;
WebAPIException.ABORT_ERR = 20;
WebAPIException.URL_MISMATCH_ERR = 21;
WebAPIException.QUOTA_EXCEEDED_ERR = 22;
WebAPIException.TIMEOUT_ERR = 23;
WebAPIException.INVALID_NODE_TYPE_ERR = 24;
WebAPIException.DATA_CLONE_ERR = 25;
//-------------------------------------------------------------------------

ErrorsHelper.initWebAPIException = function()
{
    for (idx = 0; idx < this.errorValuesList.length; idx++)
    {
        var exceptionValueItem = this.errorValuesList[idx];
        ErrorsHelper.attachReadonlyProperty(WebAPIException, exceptionValueItem.legacyCodeName, exceptionValueItem.legacyCode);
    }

    //alert("loadProperties into WebAPIException");
}

ErrorsHelper.initWebAPIException();


//-------------------------------------------------------------------------


ErrorsHelper.createWebAPIException = function(name, message)
{
    if (message == null)
    {
        message = "";
    }

    var webAPIException = new WebAPIException;
    
//    webAPIException.code = 0;
//    webAPIException.name = name;
//    webAPIException.message = description;
    
    ErrorsHelper.attachReadonlyProperty(webAPIException, "code", 0); // 0 - default
    ErrorsHelper.attachReadonlyProperty(webAPIException, "name", name);
    ErrorsHelper.attachReadonlyProperty(webAPIException, "message", message);
    
    return webAPIException;
}


ErrorsHelper.createWebAPIException_byLegacyCode = function(legacyCode)
{
    var webAPIException = new WebAPIException;
    
    var errorValue = ErrorsHelper.searchByLegacyCode(legacyCode);

//    webAPIException.code = errorValue.legacyCode;
//    webAPIException.name = errorValue.name;
//    webAPIException.message = errorValue.description;
    
    ErrorsHelper.attachReadonlyProperty(webAPIException, "code", errorValue.legacyCode);
    ErrorsHelper.attachReadonlyProperty(webAPIException, "name", errorValue.name);
    ErrorsHelper.attachReadonlyProperty(webAPIException, "message", errorValue.description);

    
    return webAPIException;
}


ErrorsHelper.createWebAPIException_byType = function(name)
{
    var webAPIException = new WebAPIException;

    var errorValue = ErrorsHelper.searchByName(name);

//    webAPIException.code = errorValue.legacyCode;
//    webAPIException.name = errorValue.name;
//    webAPIException.message = errorValue.description;
    
    ErrorsHelper.attachReadonlyProperty(webAPIException, "code", errorValue.legacyCode);
    ErrorsHelper.attachReadonlyProperty(webAPIException, "name", errorValue.name);
    ErrorsHelper.attachReadonlyProperty(webAPIException, "message", errorValue.description);
    
    return webAPIException;
}


//=============================================================================
// WebAPIError
//=============================================================================
// Helpers function used for this object creation and initialization
function WebAPIError()
{
//    this.name    // added as readonly attribute
//    this.message    // added as readonly attribute
}

WebAPIError.UNKNOWN_ERR	= 0;
WebAPIError.INDEX_SIZE_ERR = 1;
WebAPIError.DOMSTRING_SIZE_ERR = 2;
WebAPIError.HIERARCHY_REQUEST_ERR = 3;
WebAPIError.WRONG_DOCUMENT_ERR = 4;
WebAPIError.INVALID_CHARACTER_ERR = 5;
WebAPIError.NO_DATA_ALLOWED_ERR = 6;
WebAPIError.NO_MODIFICATION_ALLOWED_ERR	= 7;
WebAPIError.NOT_FOUND_ERR = 8;
WebAPIError.NOT_SUPPORTED_ERR = 9;
WebAPIError.INUSE_ATTRIBUTE_ERR = 10;
WebAPIError.INVALID_STATE_ERR = 11;
WebAPIError.SYNTAX_ERR = 12;
WebAPIError.INVALID_MODIFICATION_ERR = 13;
WebAPIError.NAMESPACE_ERR = 14;
WebAPIError.INVALID_ACCESS_ERR = 15;
WebAPIError.VALIDATION_ERR = 16;
WebAPIError.TYPE_MISMATCH_ERR = 17;
WebAPIError.SECURITY_ERR = 18;
WebAPIError.NETWORK_ERR = 19;
WebAPIError.ABORT_ERR = 20;
WebAPIError.URL_MISMATCH_ERR = 21;
WebAPIError.QUOTA_EXCEEDED_ERR = 22;
WebAPIError.TIMEOUT_ERR = 23;
WebAPIError.INVALID_NODE_TYPE_ERR = 24;
WebAPIError.DATA_CLONE_ERR = 25;

//-------------------------------------------------------------------------
/*
ErrorsHelper.initWebAPIException = function()
{
    for (idx = 0; idx < this.errorValuesList.length; idx++)
    {
        var exceptionValueItem = this.errorValuesList[idx];
        ErrorsHelper.attachReadonlyProperty(WebAPIException, exceptionValueItem.legacyCodeName, exceptionValueItem.legacyCode);
    }

    //alert("loadProperties into WebAPIException");
}

ErrorsHelper.initWebAPIException();
*/

//-------------------------------------------------------------------------

ErrorsHelper.createWebAPIError = function(name, message)
{
    if (message == null)
    {
        message = "";
    }

    var webAPIError = new WebAPIError;

//    webAPIError.name = name;
//    webAPIError.message = message;


    ErrorsHelper.attachReadonlyProperty(webAPIError, "name", name);
    ErrorsHelper.attachReadonlyProperty(webAPIError, "message", message);
    
    return webAPIError;
}


ErrorsHelper.createWebAPIError_byLegacyCode = function(legacyCode)
{
    var webAPIError = new WebAPIError;
	alert("ErrorsHelper.createWebAPIError_byLegacyCode 1 , legacyCode:" + legacyCode);
    var errorValue = ErrorsHelper.searchByLegacyCode(legacyCode);

//    webAPIException.code = errorValue.legacyCode;
//    webAPIException.name = errorValue.name;
//    webAPIException.message = errorValue.description;
    
//    ErrorsHelper.attachReadonlyProperty(webAPIError, "code", errorValue.legacyCode);
    ErrorsHelper.attachReadonlyProperty(webAPIError, "name", errorValue.name);

    ErrorsHelper.attachReadonlyProperty(webAPIError, "message", errorValue.description);
    
    return webAPIError;
}


ErrorsHelper.createWebAPIError_byType = function(name)
{
    var webAPIError = new WebAPIError;
    
    var errorValue = ErrorsHelper.searchByName(name);

//    webAPIException.code = errorValue.legacyCode;
//    webAPIException.name = errorValue.name;
//    webAPIException.message = errorValue.description;
    
//    ErrorsHelper.attachReadonlyProperty(webAPIError, "code", errorValue.legacyCode);
    ErrorsHelper.attachReadonlyProperty(webAPIError, "name", errorValue.name);
    ErrorsHelper.attachReadonlyProperty(webAPIError, "message", errorValue.description);
    
    return webAPIError;
}



//=============================================================================

/*
 * Class: SRect
 *  The rectangular region in a coordinate space, which is specified by the rectangle object's top-left point in the coordinate space, along with its width and height.
 * 
 * Properties:
 *	left	- (Number) The x coordinates of the top-left corner of the rectangle.
 *	top		- (Number) The y coordinates of the top-left corner of the rectangle.
 *	width	- (Number) The width of the rectangle.
 *	height	- (Number) The height of the rectangle.
 * 
 * Function: SRect (constructor)
 * 
 * Parameters:
 * 	o left	- (Number) left of the rectangle
 * 	o top	- (Number) top of the rectangle
 * 	o width	- (Number) width of the rectangle
 * 	o height	- (Number) height of the rectangle
 */
function SRect (left, top, width, height) {
	this.left = left;
	this.top = top;
	this.width = width;
	this.height = height;
}
SRect.prototype.toString = function(){
	return "SRect(left:" + this.left + ", top:" + this.top + ", width:" + this.width + ", height:" + this.height + ")";
}

/*
 * Class: PlayTime
 * 
 * Properties:
 *	millisecond	- (Number)
 *	timeString	- (String)
 * 
 * Function: PlayTime (constructor)
 * 
 * Parameters:
 * 	o ms	- (Number) play time in milliseconds.
 */
function PlayTime (ms) {
	this.millisecond = ms;
	
	var hour = parseInt(ms/(3600*1000), 10);
	ms -= hour * (3600*1000);
	var min = parseInt(ms/(60*1000), 10);
	ms -= min * (60*1000);
	var sec = parseInt(ms/(1000), 10);
	this.timeString = (hour>9?hour:'0'+hour)+':'+(min>9?min:'0'+min)+':'+(sec>9?sec:'0'+sec);
}
PlayTime.prototype.toString = function(){
	return this.timeString;
};

/*
 * window.onShow, onHide 에 function 할당시, 이전 것이 덮어써지지 않도록 처리
 */
(function(){
	// window.onHide 처리
	var aOnHideFunc = [];
	function onHide () {
		alert('[webapis] ' +aOnHideFunc.length+ ' onHide functions.');
		for (var i = 0; i < aOnHideFunc.length; i++) {
			if (typeof aOnHideFunc[i] == 'function') {
				aOnHideFunc[i]();
			}
		}
	}
	if (typeof window.onHide == 'function') {
		aOnHideFunc.push(window.onHide);
	}
	window.__defineGetter__('onHide', function () { 
		return onHide;
	});
	window.__defineSetter__('onHide', function (value) {
		alert('[webapis] onHide function added.');
		aOnHideFunc.push(value);
	});
	
	// window.onShow 처리
	var aOnShowFunc = [];
	function onShow () {
		alert('[webapis] ' +aOnShowFunc.length+ ' onShow functions.');
		for (var i = 0; i < aOnShowFunc.length; i++) {
			if (typeof aOnShowFunc[i] == 'function') {
				aOnShowFunc[i].apply(window, arguments);
			}
		}
	}
	if (typeof window.onShow == 'function') {
		aOnShowFunc.push(window.onShow);
	}
	window.__defineGetter__('onShow', function () { 
		return onShow;
	});
	window.__defineSetter__('onShow', function (value) {
		alert('[webapis] onShow function added.');
		aOnShowFunc.push(value);
	});
	
	// window.onPause 처리
	var aOnPauseFunc = [];
	function onPause () {
		alert('[webapis] ' +aOnPauseFunc.length+ ' onPause functions.');
		for (var i = 0; i < aOnPauseFunc.length; i++) {
			if (typeof aOnPauseFunc[i] == 'function') {
				aOnPauseFunc[i].apply(window, arguments);
			}
		}
	}
	if (typeof window.onPause == 'function') {
		aOnPauseFunc.push(window.onPause);
	}
	window.__defineGetter__('onPause', function () { 
		return onPause;
	});
	window.__defineSetter__('onPause', function (value) {
		alert('[webapis] onPause function added.');
		aOnPauseFunc.push(value);
	});
	
	// window.onResume 처리
	var aOnResumeFunc = [];
	function onResume () {
		alert('[webapis] ' +aOnResumeFunc.length+ ' onResume functions.');
		for (var i = 0; i < aOnResumeFunc.length; i++) {
			if (typeof aOnResumeFunc[i] == 'function') {
				aOnResumeFunc[i].apply(window, arguments);
			}
		}
	}
	if (typeof window.onResume == 'function') {
		aOnResumeFunc.push(window.onResume);
	}
	window.__defineGetter__('onResume', function () { 
		return onResume;
	});
	window.__defineSetter__('onResume', function (value) {
		alert('[webapis] onResume function added.');
		aOnResumeFunc.push(value);
	});
	
})();

function _checkNumberType (num){
	var nNum = Number(num);
   	
   	if(!isFinite(nNum)){
		nNum = 0;   			
		return nNum;
   	} else {
   		return nNum;
   	}
}

/*
 * Namespace: audiocontrol
 * 	The AudioControl module that provide the control of audio relative functions (ex: Volume, Mute, etc) of DTV platform.
 * 
 * Available From:
 * 	0.1
 */
webapis.audiocontrol = {
	/*
	 * Enums:
	 *	AUDIO_MODE_PCM	- identifier for the PCM audio output mode
	 *	AUDIO_MODE_DOLBY	- identifier for the DOLBY audio output mode
	 *	AUDIO_MODE_DTS	- identifier for the DTS audio output mode
	 */
	/* ExternalAudioOutputMode */
	MODE_AUDIO_OUTPUT_PCM:		0,
	MODE_AUDIO_OUTPUT_DOLBY:	1,
	MODE_AUDIO_OUTPUT_DTS:		2,
	
	/* AUISound */
    AUDIO_SOUND_TYPE_UP: 1,
    AUDIO_SOUND_TYPE_DOWN: 2,
    AUDIO_SOUND_TYPE_LEFT: 3,
    AUDIO_SOUND_TYPE_RIGHT: 4,
    AUDIO_SOUND_TYPE_PAGE_LEFT: 5,
    AUDIO_SOUND_TYPE_PAGE_RIGHT: 6,
    AUDIO_SOUND_TYPE_BACK: 7,
    AUDIO_SOUND_TYPE_SELECT: 8,
    AUDIO_SOUND_TYPE_CANCEL: 9,
    AUDIO_SOUND_TYPE_WARNING: 10,
    AUDIO_SOUND_TYPE_KEYPAD: 11,
    AUDIO_SOUND_TYPE_KEYPAD_ENTER: 12,
    AUDIO_SOUND_TYPE_KEYPAD_DEL: 13,
    AUDIO_SOUND_TYPE_SMARTCONTROL_MOVE: 14,
    AUDIO_SOUND_TYPE_SMARTCONTROL_SELECT: 15,
    AUDIO_SOUND_TYPE_MOVE: 16,
    AUDIO_SOUND_TYPE_PREPARING: 17,
	AUDIO_SOUND_TYPE_SHUTTER_PICTURE: 18,        
    AUDIO_SOUND_TYPE_SHUTTER_VIDEO: 19,
	
	/*
	 * Function: setMute
	 * 	sets the muting state of audio device.
	 * 
	 * Parameters:
	 * 	o mute	- (Boolean) whether set mute or not
	 * 		true	- set state as mute
	 * 		false	- set state as non-mute
	 * 
	 * Returns:
	 * 	nothing
	 * 
	 * Throws:
	 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type. 
     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	 */
	setMute: function(mute) {
		mute = Boolean(mute);
		if (mute === undefined || typeof mute != 'boolean') {			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);						
		}
		var pluginVer = webapis._plugin("AUDIO", "GetVersion");
		alert("Audio plugin version: " + pluginVer.isSEF + ", " + pluginVer.ver);
		var retValue = null;
		if (!pluginVer.isSEF) {
			retValue = webapis._plugin("AUDIO", "SetMute", mute ? webapis._pluginDef.PL_AUDIO_MUTE_ON : webapis._pluginDef.PL_AUDIO_MUTE_OFF);
		}
		else {
			retValue = webapis._plugin("AUDIO", "SetUserMute", mute ? webapis._pluginDef.PLR_TRUE : webapis._pluginDef.PLR_FALSE);
		}		
		return retValue == webapis._pluginDef.PLR_TRUE;
	},
	
	/*
	 * Function: getMute
	 * 	gets the muting state of audio device.
	 * 
	 * Parameters:
	 * 	N/A
	 * 
	 * Returns:
	 * 	(Boolean) whether the system is muted or not
	 * 		true	- the system is muted
	 * 		false	- the system is not muted
	 * 
	 * Throws:
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.  
	 */
	getMute: function() {
		var pluginVer = webapis._plugin("AUDIO", "GetVersion");
		var retValue = null;
		if (!pluginVer.isSEF) {
			retvalue = webapis._plugin("AUDIO", "GetMute");
			return retvalue == webapis._pluginDef.PL_AUDIO_MUTE_OFF;
		}
		else {
			retvalue = webapis._plugin("AUDIO", "GetUserMute");
			return retvalue == webapis._pluginDef.PLR_TRUE;
		}
	},
	
	/*
	 * Function: getVolume
	 * 	gets volume level on DTV platform. The rage of volume is 0 ~ 100.
	 * 
	 * Parameters:
	 * 	N/A
	 * 
	 * Returns:
	 * 	(Number) audio volume level (0 ~ 100)
	 * 
	 * Throws:
	 * 	o WebAPIError	with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value 
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	 */
	getVolume: function() {
		var retvalue = webapis._plugin("AUDIO", "GetVolume");
		alert("getVolume: " + retvalue);
		return retvalue>=0 ? retvalue : -1 ;
	},
	
	/*
	 * Function: setVolume
	 * 	sets the volume level on DTV system. The rage of volume is 0 ~ 100.
	 * 
	 * Parameters:
	 * 	o volume	- (Number) volume level. (0~100)
	 * 
	 * Returns:
	 * 	nothing
	 * 
	 * Throws:
	 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.  
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	 */
	setVolume: function(volume) {
		volume = _checkNumberType(volume);
		
		if (volume < 0 || volume > 100) {			
			throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
		}
		else {
			alert("Before SetVolume: " + webapis._plugin("AUDIO", "GetVolume"));
			alert("SetVolume with " + volume)
			webapis._plugin("AUDIO", "SetVolume", volume);
			alert("After SetVolume: " + webapis._plugin("AUDIO", "GetVolume"));
		}		
	},
	
	/*
	 * Function: setVolumeUp
	 * 	Increase the volume level on DTV system.
	 * 
	 * Parameters:
	 * 	N/A
	 * 
	 * Returns:
	 * 	nothing
	 * 
	 * Throws:
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	 */
	setVolumeUp: function () {
		var pluginVer = webapis._plugin("AUDIO", "GetVersion");		
		alert(pluginVer.isSEF + ' ' + pluginVer.ver);
		if (!pluginVer.isSEF) {			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		}
		else {
			var curVolume = webapis._plugin("AUDIO", "GetVolume");
			
			if( 0 <= curVolume && curVolume < 100 ) {
				alert("Before SetVolumeUp: " + curVolume);
				webapis._plugin("AUDIO", "SetVolumeWithKey", webapis._pluginDef.PL_AUDIO_VOLUME_KEY_UP);
				curVolume = webapis._plugin("AUDIO", "GetVolume");
				alert("After SetVolumeUp: " + curVolume);
			}
			else {				
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			}
		}
	},
	
	/*
	 * Function: setVolumeDown
	 * 	Decrease the volume level on DTV system.
	 * 
	 * Parameters:
	 * 	N/A
	 * 
	 * Returns:
	 * 	nothing
	 * 
	 * Throws:
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	 */
	setVolumeDown: function () {
		var pluginVer = webapis._plugin("AUDIO", "GetVersion");		
		if (!pluginVer.isSEF) {			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		}
		else {
			var curVolume = webapis._plugin("AUDIO", "GetVolume");
			
			if( 0 < curVolume && curVolume <= 100 ) {
				alert("Before SetVolumeDown: " + curVolume);
				webapis._plugin("AUDIO", "SetVolumeWithKey", webapis._pluginDef.PL_AUDIO_VOLUME_KEY_DOWN);
				curVolume = webapis._plugin("AUDIO", "GetVolume");
				alert("After SetVolumeDown: " + curVolume);
			}
			else {				
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			}
		}
	},
	
	/*
	 * Function: getOutputMode
	 * 	gets the current audio output mode (PCM, DOLBY, DTS, ..)
	 * 
	 * Parameters:
	 * 	N/A
	 * 
	 * Returns:
	 * 	(Number) If success, returns AUDIO_MODE, otherwise, returns a negative number.
	 * 
	 * Throws:
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. . 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	 */
	getOutputMode: function() {
		var pluginVer = webapis._plugin("AUDIO", "GetVersion");		
		if (!pluginVer.isSEF) {			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		}
		else {
			var audioOutMode = webapis._plugin("AUDIO", "GetExternalOutMode");
			return audioOutMode;
		}
	},
	
	/*
     * \brief	play a specific beep sound
     * \param       soundType
     * 
     * \return	none
     * \throw	WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 * \throw	WebAPIError with error code UNKNOWN_ERR, In any other error case.  
     */
	playSound: function(soundType) {
	
		soundType = _checkNumberType(soundType);
		
		if (1 > soundType || soundType > 19 ) {			
			throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
		}
		
		var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
		alert('Firmware : ' + sFirmware);
		
		/*
		 * aResult[0] = 2011-1007
		 * aResult[1] = 2011
		 * aResult[2] = 1007
		 */
		var aResult = sFirmware.match(/(\d+)-(\d+)/);
		var nFirmwareYear = aResult[1];	
		alert('nFirmwareYear == ' + nFirmwareYear);		
		var retValue = null;
		if (nFirmwareYear >= 2012) { // Echo
			retValue = webapis._plugin("AUI", "PlayAudio", soundType);
		}
		else {			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		}
		
		if(retValue > 0) {
			return true;
		}
		else {
			return false;
		}
			 
	}
}


/*
 * Namespace: displaycontrol
 * 	DisplayControl module deals with display functions of TV. It can check whether your device is supported 3D or not, etc.
 * 
 * Available From:
 * 	0.1
 */
webapis.displaycontrol = {
	/*
	 * Enums:
	 *	MODE_3D_EFFECT_OFF	- identifier for 3DEffect mode off
	 *	MODE_3D_EFFECT_TOP_BOTTOM	- identifier for 3DEffect mode top_bottom
	 *	MODE_3D_EFFECT_SIDE_BY_SIDE	- identifier for 3DEffect mode side_by_side
	 *	MODE_3D_EFFECT_LINE_BY_LINE	- identifier for 3DEffect mode line_by_line
	 *	MODE_3D_EFFECT_VERTICAL_STRIPE	- identifier for 3DEffect mode vertical_stripe
	 *	MODE_3D_EFFECT_FRAME_SEQUENCE	- identifier for 3DEffect mode frame_sequence
	 *	MODE_3D_EFFECT_CHECKER_BD	 - identifier for 3DEffect mode checker_bd
	 *	MODE_3D_EFFECT_FROM_2D_TO_3D	- identifier for 3DEffect mode convert 2D to 3D
	 */
	/* interface Mode3DEffect */
	MODE_3D_EFFECT_OFF: 0,
	MODE_3D_EFFECT_TOP_BOTTOM: 1,
	MODE_3D_EFFECT_SIDE_BY_SIDE: 2,
	MODE_3D_EFFECT_LINE_BY_LINE: 3,
	MODE_3D_EFFECT_VERTICAL_STRIPE: 4,
	MODE_3D_EFFECT_FRAME_SEQUENCE: 5,
	MODE_3D_EFFECT_CHECKER_BD: 6,
	MODE_3D_EFFECT_FROM_2D_TO_3D: 7,
	
	/* Support3DEffect */
	MODE_3D_DEVICE_NOT_CONNECTED: -1,
	MODE_3D_NOT_SUPPORTED: 0,
	MODE_3D_ENABLE_OK: 1,
	
	/*
	 * Function: get3DEffectMode
	 * 	gets the current 3D Effect mode.
	 *
	 * Parameters:
	 * 	N/A
	 *
	 * Returns:
	 * 	(Number) 3D effect mode number
	 *
	 * Throws:
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR if the operation is not supported
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
	 * 	o WebAPIError with error code UNKNOWN_ERR In any other error case.
	 */
	get3DEffectMode: function(){
		var sFirmware = webapis._plugin('NNavi', 'GetFirmware');	
		var pluginVer = webapis._plugin("SCREEN", "GetVersion");
		
		if (sFirmware >= 'T-INFOLINK2012' || sFirmware < 'T-INFOLINK2011') {			
			if (!pluginVer.isSEF) {
				alert("Not support");
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			else {
				return webapis._plugin("SCREEN", "Get3DEffectMode");
			}
		}
		else {
			if (!pluginVer.isSEF) {
				alert("Not support");
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			else {
				return webapis._plugin("SCREEN", "Get3DEffectMode", 1);
			}
		}
	},
	
	/*
	 * Function: check3DModeEnable
	 * 	Check whether the 3D mode is available or not.
	 *
	 * Parameters:
	 * 	o none
	 *
	 * Returns:
	 * 	(Number) 3D Effect Mode index
	 *
	 * Throws:
	 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
	 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
	 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed. 
	 * 	o WebAPIError with error code UNKNOWN_ERR In any other error case. 
	 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	 */
	check3DModeEnable: function(){
		var nProductType = webapis._plugin('TV', 'GetProductType');
		alert("nProductType : " + nProductType);
		if (nProductType == 0) { //TV
			var pluginVer = webapis._plugin("SCREEN", "GetVersion");
			
			if (!pluginVer.isSEF) {
				alert("Not support");
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			else {
				var retVal = webapis._plugin("SCREEN", "Flag3DEffectSupport");
				if( retVal > 0 ) {
					return webapis.displaycontrol.MODE_3D_ENABLE_OK;
				}
				else {
					return webapis.displaycontrol.MODE_3D_NOT_SUPPORTED;
				}
			}
		}
		else if (nProductType == 2) { //BD
			var pluginVer = webapis._plugin("SCREEN", "GetVersion");
			if (!pluginVer.isSEF) {
				alert("Not support");
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			else {
				var retVal = webapis._plugin("SCREEN", "Flag3DTVConnect");
				if( retVal ) {
					return webapis.displaycontrol.MODE_3D_ENABLE_OK;
				}
				else {
					return webapis.displaycontrol.MODE_3D_DEVICE_NOT_CONNECTED;
				}
			}
		}
		else {
			alert('Device is not TV or BD.');
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		}
	},
	
	/*
	 * Function: get3DModeSupportList
	 * 	Check the 3D Effect is supported.
	 *
	 * Parameters:
	 * 	o successCallback	- (Function()) effect 3D effect mode. Please refer to Enums.
	 * 	o errorCallback		- (Function(WebAPIError)) effect 3D effect mode. Please refer to Enums.
	 *
	 * Returns:
	 * 	nothing. pending(Async) operation.
	 *
	 * Throws:
	 * 	o NOT_SUPPORTED_ERR: If this feature is not supported.
	 * 	o SECURITY_ERR: If the operation is not allowed. 
	 * 	o UNKNOWN_ERR: In any other error case.
	 */
	
	get3DModeSupportList: function(successCallback, errorCallback){
		if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			return;
		}
		if((typeof successCallback != 'function' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback != null)) {
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
		} 
		if(successCallback == null && errorCallback == null) {
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
		}
		
		var pluginVer = webapis._plugin("SCREEN", "GetVersion");
		
		if (!pluginVer.isSEF) {
			alert("Not support");
			if(typeof errorCallback == 'function') {
				errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
			}
			return;
		}
		else {
			var Mode3DEffectListSupport = new Array();
			Mode3DEffectListSupportLength = 8;
			
			var nSupportFlag = 0;

			for( var i = 0; i < Mode3DEffectListSupportLength; i++ ) {
				if( webapis._plugin("SCREEN", "Check3DEffectMode", i) ) {
					Mode3DEffectListSupport[i] = 1;
					nSupportFlag++;
				}
				else {
					Mode3DEffectListSupport[i] = 0;
				}
			}

			if( nSupportFlag ) {
				if(typeof successCallback == 'function') {
					successCallback( Mode3DEffectListSupport );
				}
			}
			else {
				if(typeof errorCallback == 'function') {
					errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
			}
			return;
		}
	}
}

/*
 * Namespace: application
 * 	.
 * 
 * Available From:
 * 	0.1
 */
webapis.application = {
	
	/*
	 * Function: getPopupOpacity
	 * 	.
	 *
	 * Parameters:
	 * 	o 
	 *
	 * Returns:
	 * 	.
	 *
	 * Throws:
	 * 	o 
	 */
	
	getPopupOpacity: function(){
		var firmware = webapis._plugin("NNavi", "GetFirmware");
		var nOpacity = 0;
		alert('[SEC Web API] FirmwareVersion : '+firmware);
		if('T-INFOLINK2011-9999' < firmware) {
			var nTransparency = webapis._plugin("TV", "GetCurrentOSDTransparency"); // 0~255
			nOpacity = (Number(nTransparency) / 255).toFixed(2);
			alert(nTransparency + ' -> ' + nOpacity);			
		} else {
			nOpacity = 0.8;
		}		
		alert('[SEC Web API] getPopupOpacity() returns ' + nOpacity);
		return nOpacity;
	}
}

/*
 * value의 type이 types에 포함되어 있을 경우 true.
 * types : undefined|object|string|number|boolean|function|null 중 조합.(ex. object|string)
 * (JavaScript 기본 타입 + null)
 */
function _isType(value, types){
	var sValueType = '';
	if (value === null) {
		sValueType = 'null';
	}
	else {
		sValueType = typeof value;
	}
    return types.indexOf(sValueType) != -1;
}

/*
 * Namespace: avplay
 * 	Player module provides functions for multimedia application to implement playback of media contents.
 * 
 * Available From:
 * 	0.1
 */

(function(){ // Start webapis.avplay

	webapis.avplay = {
		// Player Plugin Event Enum 
		CONNECTION_FAILED: 1,
		AUTHENTICATION_FAILED: 2,
		STREAM_NOT_FOUND: 3,
		NETWORK_DISCONNECTED: 4,
		NETWORK_SLOW: 5,
		RENDER_ERROR: 6,
		RENDERING_START: 7,	//Movie and Music only
		RENDERING_COMPLETE: 8,
		STREAM_INFO_READY: 9,
		DECODING_COMPLETE: 10,	//Picture only
		BUFFERING_START: 11,
		BUFFERING_COMPLETE: 12,
		BUFFERING_PROGRESS: 13,
		CURRENT_DISPLAY_TIME: 14,
		CURRENT_PLAYBACK_TIME: 14,
		AD_START: 15,
		AD_END: 16,
		RESOLUTION_CHANGED: 17,
		BITRATE_CHANGED: 18,
		SUBTITLE: 19,
		CUSTOM: 20,
		// RENDER_ERROR Detail
		UNKNOWN_ERROR: 0,
		UNSUPPORTED_CONTAINER: 1,
		UNSUPPORTED_VIDEO_CODEC: 2,
		UNSUPPORTED_AUDIO_CODEC: 3,
		UNSUPPORTED_VIDEO_RESOLUTION: 4,
		UNSUPPORTED_VIDEO_FRAMERATE: 5,
		CURRUPTED_STREAM: 6,
		
		//custom error 20121101
		CUSTOM_ERROR:100,
		
		//custom error 20121207
		RTSP_STATE : 95,
		
		// AVPlay State Enum
		PLAY_STATE_IDLE: 0,
		PLAY_STATE_INITIALIZED: 1,
		PLAY_STATE_STOPPED: 2,
		PLAY_STATE_PREPARED: 3,
		PLAY_STATE_STARTED: 4,
		PLAY_STATE_PAUSED: 5,
		
		
		//121206 박일우K 협의
		/* interface Mode3D*/		
		MODE_3D_EFFECT_OFF: 0,
		MODE_3D_EFFECT_SIDE_BY_SIDE: 1,
		MODE_3D_EFFECT_TOP_BOTTOM: 2,
		MODE_3D_EFFECT_FRAME_PACKING: 3,
		MODE_3D_EFFECT_FROM_2D_TO_3D: 4,
		MODE_3D_EFFECT_CHECKER_BD: 5,		
		MODE_3D_EFFECT_LINE_BY_LINE: 6,
		MODE_3D_EFFECT_VERTICAL_STRIPE: 7,
		MODE_3D_EFFECT_FRAME_SEQUENCE: 8,
		MODE_3D_EFFECT_UNI_3D_MODE_INIT : 9,
		
		/*
	     * Function: getAVPlay
	     *  The getAVPlay() method returns a reference to the different avplay available in the device. This reference can be used to play av contents.
	     *
	     * Parameters:
	     * 	o successCallback	- (AvaiableAVPlaySuccessCallback)
	     *
	     * Returns:
	     * 	nothing. Pending(Async) Operation
	     * 
	     * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	     */
		getAVPlay: function (successCallback, errorCallback) {
			
	        // Parameter Type Checking
	        if (
	            !_isType(successCallback, 'function') ||
	            !_isType(errorCallback, 'undefined|function|null')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
	        if(!webapis.pcmode){
	        	if (!bInit) {
					init();
				}
				var iAVPlay = new _AVPlay(nId);
			} else {
				var iAVPlay = new _AVPlay_PC(nId);
			}
			
			alert('[AVPlay] getAVPlay() succeeded. -> ' + nId + 'th ' + iAVPlay);
			
			aAVPlayInstance[nId] = iAVPlay;
			nId++;
			successCallback(iAVPlay);	// 생성된 instance를 전달하기로 함(박일우K)
		},
		_getAllInstance: function () {
			return aAVPlayInstance;
		},
		_destroyAll: function () {
			var aInstance = webapis.avplay._getAllInstance();
			alert('[AVPlay] _destroyAll() with ' + aInstance.length + ' instances.');
			for (var i = 0; i < aInstance.length; i++) {
				alert('[AVPlay] Destroy ' + i + 'th avplay.');
				aInstance[i].destroy();
			}
		},
		/*
		_deactivateAll: function () {
			var nPlayerNum = webapis._plugin('PlayerManager', 'GetTotalNumOfPlayer');
			for(var i=0 ; i< nPlayerNum ; i++)
			{
				var tPlayerID = webapis._plugin('PlayerManager', 'GetPlayerID', i);
				alert('[AVPlay] GetPlayerID : ' + tPlayerID);
				if(webapis._plugin('PlayerManager', 'GetActivationState', tPlayerID) == 0)
				{
					alert('[AVPlay] DeactivatePlayer : ' + tPlayerID);
					webapis._plugin('PlayerManager', 'DeactivatePlayer', tPlayerID);
				}
				else
				{
					alert('[AVPlay] Skip DeactivatePlayer : ' + tPlayerID);
				}
			}
		}
		*/
	}

	var bInit = false,
		nId = 0,
		aAVPlayInstance = [],
		bSupportMultiApplication = false;

	function init(){		
		var sServiceConfig = webapis._plugin('NNavi', 'GetServiceConfig'),
			oServiceConfig = null;
			
		alert('[AVPlay] sServiceConfig: ' + sServiceConfig);
		alert(typeof sServiceConfig);
		
		if (sServiceConfig) {
			try {
				oServiceConfig = eval('(' + sServiceConfig + ')');
			}
			catch(e) {
				alert('[AVPlay] ' + e);
			}
		}
		
		bSupportMultiApplication = false;
		var notSupportMultiTasking = webapis._plugin('TaskManager', 'CheckSupportMultiTaskingApp', 0);
		var supportValue = webapis._plugin('TaskManager', 'CheckSupportMultiTaskingApp', 2);
		if (notSupportMultiTasking == 0 && supportValue == 1) {
			bSupportMultiApplication = true;
		}
		
		alert('[AVPlay] bSupportMultiApplication: ' + bSupportMultiApplication);
				
		window.onHide = webapis.avplay._destroyAll;
		
		/*	
		if (bSupportMultiApplication) {
			// [120706] Multi application 지원시 onHide 에서 stop하지 않고 pause 하도록 함
			window.onHide = webapis.avplay._deactivateAll;
		}
		else {
			// onHide 시 모든 AVPlay instance의 destroy() 가 호출되도록 함
			window.onHide = webapis.avplay._destroyAll;
		}
		*/
	}
	
	
	function _setDefaultRect(){					
	    if(!webapis.pcmode){
	    	var rect = new SRect(0, 0, curWidget.width, curWidget.height);	
	    } else {
	    	var rect = new SRect(0, 0, 960, 540);
	    }
		return rect;
	}

	/*
	 * Class: AVPlay
	 */
	function _AVPlay(id) {
		var sName = 'AVPlay' + id;	// Instance name
		var sVersion = '3.0';
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// avplay properties
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		/*
		 * Property: id
		 *  AVPlay identification
		 *  
		 * Property: url
		 *  This string stores the url of the video that is currently being played.
		 *  
		 * Property: duration
		 *  This string stores the duration of the video currently being played. The value is stored in the form of milli seconds.
		 *  
		 * Property: videoWidth
		 *  This string stores the width of the video that is currently being played. The video width is stored in the form of no of pixels and it is always less than the width of the DTV.
		 *  
		 * Property: videoHeight
		 *  This string stores the height of the video that is currently being played. The video height is stored in the form of no of pixels and it is always less than the height of the DTV.
		 *  
		 * Property: displayRect
		 *  This string stores the display area of the video that is currently being played. The displayArea stores four values. The first two values represent the x and y co-ordinate of the top left corner of the video and the next two parameters give us the width and height of the video.
		 *  
		 * Property: containerID
	     *  container identifier
	     *  
	     * Property: zIndex
	     *  z-index
	     *  
		 * Property: totalNumOfVideo
		 *  This value stores the number of video tracks in the current file.
		 *  
		 * Property: totalNumofAudio
		 *  This value stores the number of audio tracks in the current video file.
		 *  
		 * Property: totalNumOfSubtitle
		 *  This value stores the number of subtitle tracks in the current video file.		 		 
		 *  
		 * Property: totalBufferSize
		 *  This value stores the maximum amount of buffer size. If the total buffer size is 5MB it wont be possible to play a video larger than 5MB.
		 *  
		 * Property: initialBufferSize
		 *  This value stores the amount of buffer size that strating the video.
		 *  
		 * Property: macrovision
		 *  This value stores the value of enabled Macrovision copy protection. Macrovision is used to stop you connecting your DVD to your VCR and making copies of your DVD's. It is a form of copy protection that makes the picture go bright and dark, and also you may see some lines and distortion on the screen too.
		 *  
		 * Property: status
		 *  This value stores the current status of the player. Available states are : [[enums]]
		 */
		
		this.__defineGetter__('id', function(){return id;});													// (Number) unique id, AVPlayFactory generates this. [create instance]
		this.__defineGetter__('url', function(){return null;});													// (String) source url. [open()]
		this.__defineGetter__('duration', function(){return null;});											// (Number) total play time. [STREAM_INFO_READY]
		this.__defineGetter__('videoWidth', function(){return null;});											// (Number) video width. [STREAM_INFO_READY]
		this.__defineGetter__('videoHeight', function(){return null;});											// (Number) video height. [STREAM_INFO_READY]
		this.__defineGetter__('displayRect', function(){return iDisplayRect}); // (SRect) the position of player object area. [init()]
		this.__defineGetter__('displayArea', function(){return null;});											// (SRect) the position of displayed screen area. [STREAM_INFO_READY]
		this.__defineGetter__('containerID', function(){return null;});											// (String) the container div id of <object> tag.
		this.__defineGetter__('zIndex', function(){return null;});												// (Number) the z-index of the AVPlay. If an user specifies the container div, "zIndex" would be set to <object>. If not, "zIndex" would be set to default container div created by AVPlay.
		this.__defineGetter__('cropArea', function(){return null;});											// (SRect) the crop area of the original source. [?]
		this.__defineGetter__('totalNumOfVideo', function(){return null;});										// (Number) the total number of video. [?]
		this.__defineGetter__('totalNumOfAudio', function(){return null;});										// (Number) the total number of audio. [STREAM_INFO_READY]
		this.__defineGetter__('totalNumOfSubtitle', function(){return null;});									// (Number) the total number of subtile. [STREAM_INFO_READY]
		//this.__defineGetter__('mode3D', function(){return null;});												// (Number) 3D mode. [play()]
		this.__defineGetter__('totalBufferSize', function(){return -1;});										// (Number) the total number of playing buffer bytes. [before StartPlayback()]
		this.__defineGetter__('pendingBufferSize', function(){return -1;});										// (Number) the total number of pending buffer bytes. [before StartPlayback()]
		this.__defineGetter__('initialBufferSize', function(){return -1;});										// (Number) the total number of initial buffer bytes. [before StartPlayback()]
		this.__defineGetter__('macrovision', function(){return false;});										// (Boolean) macrovision. [init()]
		this.__defineGetter__('status', function(){return webapis.avplay.PLAY_STATE_IDLE;});					// (Number) vod play status. [Many points]
		
		// [120801] 차별화 서비스 튜토리얼 동영상 Genoa 에서 Skip 안되는 문제 수정
		this.__defineGetter__('authHeader', function(){return 'basic';});										// (String) Authorization Header setting. 'basic', 'none' (Default: 'basic) [open()]
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// IDL 에 있는 함수들
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		/*
		 * Function: init
		 * 	initializes avplay with the specified option parameter. This has to be called before all of other avplay functions.
		 *
		 * Parameters:
		 * 	o option	- (AVPlayInitialOption) a option for the initialize avplay
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.init = function (option) {	// [IDL]
			alert('[AVPlay'+id+'] init('+option+')');
			
			/*
		     option = {
			     containerID: id,	// [init()]
			     zIndex: 0,	// [init()]
			     bufferingCallback: {
				     onbufferingstart : onBufferingStart,
				     onbufferingprogress : onBufferingProgress,
				     onbufferingcomplete : onBufferingComplete
			     },
			     playCallback: {
				     oncurrentplaytime : onCurrentPlayTime,
				     onresolutionchanged : onResolutionChange,
				     onstreamcompleted : onStreamComplete,
				     onerror : onRederingError
			     },
			     displayRect: {	// SRect instance [init()]
				     top: 100,
				     left : 100,
				     width : 200,
				     height : 200
			     },
			     autoRatio: true,	// [STREAM_INFO_READY]
			     frontPanelLock: false // Front panel 표시 여부. MP3 재생시 표시하지 않도록 하기 위함
		     }
		     */
			
	        // Parameter Type Checking
			if (_isType(option, 'object')) {				
				if (
					!_isType(option.containerID, 'undefined|string') ||
					!_isType(option.zIndex, 'undefined|number') ||
					!_isType(option.bufferingCallback, 'undefined|object') ||
					!_isType(option.playCallback, 'undefined|object') ||
					!_isType(option.displayRect, 'undefined|object') ||
					!_isType(option.autoRatio, 'undefined|boolean')
				) {
					throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
				}
				
				if (_isType(option.bufferingCallback, 'object')) {
					if (
						!_isType(option.bufferingCallback.onbufferingstart, 'undefined|function') ||
						!_isType(option.bufferingCallback.onbufferingprogress, 'undefined|function') ||
						!_isType(option.bufferingCallback.onbufferingcomplete, 'undefined|function')
					) {
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
				
				if (_isType(option.playCallback, 'object')) {
					if (
						!_isType(option.playCallback.oncurrentplaytime, 'undefined|function') ||
						!_isType(option.playCallback.onresolutionchanged, 'undefined|function') ||
						!_isType(option.playCallback.onstreamcompleted, 'undefined|function') ||
						!_isType(option.playCallback.onerror, 'undefined|function')
					) {
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
				
				if (_isType(option.displayRect, 'object')) {
					var top = _checkNumberType(option.displayRect.top);
					var left = _checkNumberType(option.displayRect.left);
					var width = _checkNumberType(option.displayRect.width);
					var height = _checkNumberType(option.displayRect.height);
					if (
						!_isType(top, 'number') ||
						!_isType(left, 'number') ||
						!_isType(width, 'number') ||
						!_isType(height, 'number')
					) {
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
			}
			else {
				if (!_isType(option, 'undefined|null')) {
					throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
				}
			}
			// Parameter Type Checking End
			
			oInitOption = option || {};
			
			initializeEnv();
			
			/*
			 * <object> 삽입
			 * - pluginObjectID는 강제 설정.
			 * - init이 처음 불리는데 containerID를 지정 안했을 경우, body에 삽입함.
			 */
			if (!bInitialize) {
				initialize(oInitOption.containerID || null, oInitOption.zIndex || null, PLAYER_OBJECT_ID + id);
			}
			else if (oInitOption.containerID) {
				this.setPlayerPluginObject(oInitOption.containerID, oInitOption.zIndex || null, PLAYER_OBJECT_ID + id);
			}
	
			/*
			 * displayRect 설정
			 */
			if (oInitOption.displayRect) {
				this.setDisplayRect(oInitOption.displayRect);
			}
			else {				
				alert('[AVPlay'+id+'] !WARNNING! > You did Not set displayRect.');
			}
			
			bFrontPanelLock = oInitOption.frontPanelLock || false; // 기본 값은 false
			
			if (oInitOption.autoRatio !== undefined) {
				bAutoRatio = oInitOption.autoRatio;
			}
			
			// [120503] IDLE 이 아닌 다른 상태였을 경우에는 상태 유지 -> 상태 변경시 비정상 동작을 하게됨.
			if (this.status == webapis.avplay.PLAY_STATE_IDLE) {
				this._setStatus(webapis.avplay.PLAY_STATE_INITIALIZED);
			}
			
			return true;
		}
		/*
		 * Function: open
		 * 	opens media content with the URL specified by parameter.
		 *
		 * Parameters:
		 *  o url		- (String) URL of the video file
		 *  o option 	- (AVPlayOption)(Optional) a option for opening the specific video contents.
		 *
		 * Returns:
		 * 	True on success and false on failure.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.open = function (url, option) {
			alert('[AVPlay'+id+'] open('+url+','+(option?option:'')+')');
			
			/*
	         * option = {
	             totalBufferSize: 10240000, // [play()]
	             pendingBufferSize: 10240000,   // [play()]
	             initialBufferSize: 10240000,   // [play()]
	             adaptive: {
	                 type: ,
	                 bitrates: ,
	                 upTimer: ,
	                 startBitrate: ,
	                 startTime: ,
	                 admode: ,
	             },
	             drm: {
	                 type: ,
	                 company: ,
	                 deviceID: ,
	                 deviceType: ,
	                 streamID: ,
	                 drmURL: ,
	                 ackURL: ,
	                 heartbeatPeriod: ,
	                 portal: ,
	                 userData: ,
	                 cookie: 
	             },
	             macrovision: {
	                 type: webapis.avplay.APS_ALL_OFF, // macrovision을 설정하기 위해 macrovisionType, ict, vbi 모든 값이 있어야 함. [STREAM_INFO_READY]
	                 ict: webapis.avplay.ICT_ON, // [STREAM_INFO_READY]
	                 dot: ,
	                 vbi: webapis.avplay.CGMS_COPY_FREE, // CGMS type 이 와야하는 것인지? [STREAM_INFO_READY]
	             },
	             subtitle: {
	                 path: ,
	                 streamID: ,
	                 sync: ,
	                 callback: 
	             }
	             
	             // 임시
	             mode3D: webapis.displaycontrol.MODE_3D_EFFECT_SIDE_BY_SIDE, [play()] // [120406] IDL에 항목 없음
	             authHeader: // 'basic' or 'none' (Default: 'basic')
	         * }
	         */
			
	        // Parameter Type Checking
	        var iurl = String(url);	        
	        
	        if (
	            !_isType(iurl, 'string') ||
	            !_isType(option, 'undefined|object|null')
	        ) {	        	
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
			
			if (_isType(option, 'object')) {				
	            if (
	                !_isType(option.totalBufferSize, 'undefined|number') ||
	                !_isType(option.pendingBufferSize, 'undefined|number') ||
	                !_isType(option.initialBufferSize, 'undefined|number') ||
	                !_isType(option.adaptive, 'undefined|object') ||
	                !_isType(option.drm, 'undefined|object') ||
	                !_isType(option.macrovision, 'undefined|object') ||
	                !_isType(option.subtitle, 'undefined|object') ||
	                !_isType(option.mode3D, 'undefined|number') ||
	                !_isType(option.authHeader, 'undefined|string') // [120801] 차별화 서비스 튜토리얼 동영상 Genoa 에서 Skip 안되는 문제 수정
	            ) {	            	
	                throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	            }
				
				if (_isType(option.adaptive, 'object')) {
					if (
						!_isType(option.adaptive.type, 'string') ||
						!_isType(option.adaptive.bitrates, 'undefined|string') ||
						!_isType(option.adaptive.upTimer, 'undefined|string') ||
						!_isType(option.adaptive.startBitrate, 'undefined|string') ||
						!_isType(option.adaptive.startTime, 'undefined|string') ||
						!_isType(option.adaptive.admode, 'undefined|string')
					) {						
						throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
					}
				}
				
				if (_isType(option.drm, 'object')) {
					if (
						!_isType(option.drm.type, 'string') ||
						!_isType(option.drm.company, 'undefined|string') ||
						!_isType(option.drm.deviceID, 'undefined|string') ||
						!_isType(option.drm.deviceType, 'undefined|string') ||
						!_isType(option.drm.streamID, 'undefined|string') ||
						!_isType(option.drm.drmURL, 'undefined|string') ||
						!_isType(option.drm.ackURL, 'undefined|string') ||
						!_isType(option.drm.heartbeatPeriod, 'undefined|string') ||
						!_isType(option.drm.portal, 'undefined|string') ||
						!_isType(option.drm.userData, 'undefined|string') ||
						!_isType(option.drm.cookie, 'undefined|string')
					) {						
						throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
					}
				}
				
				if (_isType(option.macrovision, 'object')) {
					if (
						!_isType(option.macrovision.type, 'undefined|number') ||
						!_isType(option.macrovision.ict, 'undefined|number') ||
						!_isType(option.macrovision.dot, 'undefined|number') ||
						!_isType(option.macrovision.vbi, 'undefined|number')
					) {						
						throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
					}
				}
				
				if (_isType(option.subtitle, 'object')) {
					if (
						!_isType(option.subtitle.path, 'string') ||
						!_isType(option.subtitle.streamID, 'number') ||
						!_isType(option.subtitle.sync, 'undefined|number') ||
						!_isType(option.subtitle.callback, 'function')
					) {
						throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
					}
				}
	        }
	        // Parameter Type Checking End			
			if (!bInitialize) {				
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
	        // PLAY_STATE_INITIALIZED, PLAY_STATE_STOPPED 상태에서만 open 할 수 있도록 강제
	        if (this.status != webapis.avplay.PLAY_STATE_STOPPED &&
	        this.status != webapis.avplay.PLAY_STATE_INITIALIZED) {	        	
	            alert('[AVPlay' + id + '] !THROW ERROR! WebAPIDOMErrorCode.INVALID_STATE_ERR');
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.INVALID_STATE_ERR);
	        }
			
			oPlayOption = option || {};
			
			// subtitle 지원 안하는 firmware의 경우 subtitle 항목을 삭제
			if (oPlayOption.subtitle && !this.getSubtitleAvailable()) {				
				delete oPlayOption.subtitle;
			}			
			iurl = iurl.replace(/&amp;/g, '&');			
	        this.__defineGetter__('url', function(){return iurl;}); // [Instance properties]			
			if (oPlayOption.adaptive) {				
				// HAS/HLS는 있는 것만 URL에 추가
				if (oPlayOption.adaptive.bitrates) url += '|BITRATES=' + oPlayOption.adaptive.bitrates;	// OIPF HAS, Apple
				if (oPlayOption.adaptive.upTimer) url += '|UPTIMER=' + oPlayOption.adaptive.upTimer;	// OIPF HAS, Apple
				if (oPlayOption.adaptive.startBitrate) url += '|STARTBITRATE=' + oPlayOption.adaptive.startBitrate;	// OIPF HAS, Apple
				if (oPlayOption.adaptive.startTime) url += '|STARTTIME=' + oPlayOption.adaptive.startTime;	// Apple
				if (oPlayOption.adaptive.admode) url += '|ADMODE=' + oPlayOption.adaptive.admode;	// OIPF HAS
				if (oPlayOption.adaptive.type) url += '|COMPONENT=' + oPlayOption.adaptive.type;	// OIPF HAS, Apple
			}
			else if (oPlayOption.drm) {
				// WIDEVINE은 없는 것도 추가
				if(oPlayOption.drm.type && oPlayOption.drm.data){ //WV 외의 모든 drm type 대응을 위한 수정	130315									
					for(var key in oPlayOption.drm.data){
						url += '|' + key + '=' + oPlayOption.drm.data[key];
					}
				} else if (oPlayOption.drm.type == 'WV') {					
					url += '|DEVICE_ID=' + oPlayOption.drm.deviceID || '';
					url += '|DEVICET_TYPE_ID=' + oPlayOption.drm.deviceType || ''; // If your widget is also released on 2010 devices, use DEVICET_TYPE_ID instead of DEVICE_TYPE_ID
					url += '|STREAM_ID=' + oPlayOption.drm.streamID || '';
					url += '|IP_ADDR=' + getIPAddr(); // webapis 함수로 교체 필요
					url += '|DRM_URL=' + oPlayOption.drm.drmURL || '';
					url += '|ACK_URL=' + oPlayOption.drm.ackURL || '';
					url += '|HEARTBEAT_URL=' + oPlayOption.drm.heartbeatURL || '';
					url += '|HEARTBEAT_PERIOD=' + oPlayOption.drm.heartbeatPeriod || '';
					url += '|PORTAL=' + oPlayOption.drm.portal || '';
					url += '|USER_DATA=' + oPlayOption.drm.userData || '';
					url += '|COMPONENT=' + oPlayOption.drm.type || '';
				}
			}
			
			// [120801] 차별화 서비스 튜토리얼 동영상 Genoa 에서 Skip 안되는 문제 수정			
			var authHeader = (oPlayOption.authHeader == 'basic' || oPlayOption.authHeader == 'none') ? oPlayOption.authHeader : 'basic';			
			this.__defineGetter__('authHeader', function(){return authHeader;});	// (String) Authorization Header setting. 'basic', 'none' (Default: 'basic) [open()]
			
			//
			
			var retValue = webapis._plugin(ePlayerPlugin, 'InitPlayer', url);	// 1: success, -1: fail
			nVolume = 100; // IniptPlayer시 Music Volume이 100으로 초기화됨.			
			this._setStatus(webapis.avplay.PLAY_STATE_PREPARED);			
			alert('[AVPlay'+id+'] open('+url+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: play
		 * 	Starts playback from the second specified by parameter. If the operation is failed due to any other error, the errorCallback is launched with the appropriate error code:
		 *  INVALID_VALUES_ERR: If the input attributes do not contain a valid value
		 *  UNKNOWN_ERR: In any other error case.
		 *
		 * Parameters:
		 * 	o successCallback	- (Function)(Optional) Callback upon Success.
		 * 	o errorCallback		- (Function)(Optional) Callback upon Failure.
		 * 	o sec				- (Function)(Optional) Seconds at which playback has to be started.
		 *
		 * Returns:
		 * 	nothing. Pending(Async) Operation
		 *
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.play = function (successCallback, errorCallback, sec) {	// [IDL]
			alert('[AVPlay'+id+'] play('+typeof successCallback+','+typeof errorCallback+','+(sec!==undefined?sec:'')+')');
			var isec = _checkNumberType(sec);
	        // Parameter Type Checking
	        if (
	            !_isType(successCallback, 'function') ||
	            !_isType(errorCallback, 'function')
	        ) {	        	
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        
	        if(isec < 0) {
	        	if (typeof errorCallback == 'function') {					
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
					this._setStatus(webapis.avplay.PLAY_STATE_STOPPED);
				}
				return;					
			} 
	        
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			// PLAY_STATE_PREPARED 상태에서만 play 할 수 있도록 강제
	        if (this.status != webapis.avplay.PLAY_STATE_PREPARED) {
				alert('[AVPlay'+id+'] !THROW ERROR! WebAPIDOMErrorCode.INVALID_STATE_ERR'); 
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.INVALID_STATE_ERR);
	        }
			
			if (oPlayOption.totalBufferSize) { // oInitOption -> totalBufferSize 처리
				this.setTotalBufferSize(oPlayOption.totalBufferSize);
			}
			if (oPlayOption.pendingBufferSize) {
				this.setPendingBufferSize(oPlayOption.pendingBufferSize);
			}
			if (oPlayOption.initialBufferSize) {
				this.setInitialBufferSize(oPlayOption.initialBufferSize);
			}
			if (oPlayOption.mode3D) {
				// 2nd parameter means that the format of 3D contents is user type, not MPO, SVAF, M2TS.
				this.setPlayerProperty(this.PROPERTY_TYPE_3D, 3, oPlayOption.mode3D);
				//this.__defineGetter__('mode3D', function(){return oInitOption.mode3D;}); // [Instance properties]
			}
			if (oPlayOption.drm && oPlayOption.drm.cookie) {
				this.setPlayerProperty(this.PROPERTY_TYPE_COOKIE, oPlayOption.drm.cookie, oPlayOption.drm.cookie.length);
			}
			if (oPlayOption.macrovision && oPlayOption.macrovision.dot !== undefined) {
				this.setOutputDOT(oPlayOption.macrovision.dot);
			}
			
			if (oPlayOption.HEVC) { //HEVC 대응을 위해 추가 130315
				alert('oPlayOption.HEVC.customData...................'+oPlayOption.HEVC.customData);
				alert('oPlayOption.HEVC.licenseURL...................'+oPlayOption.HEVC.licenseURL);
				var ret = this.setHEVC(oPlayOption.HEVC.customData,oPlayOption.HEVC.licenseURL);
				if(ret){
					alert('[VideoPlayer] setHEVC succeed!!');
				} else {
					alert('[VideoPlayer] setHEVC failed!!');
					if (typeof errorCallback == 'function') {
						errorCallback(ErrorsHelper.createWebAPIException_byType('AvplayUnsupportedVideoFormatError'));
						this._setStatus(webapis.avplay.PLAY_STATE_STOPPED);
					}
					return;
				}
			}
			
			// [120801] 차별화 서비스 튜토리얼 동영상 Genoa 에서 Skip 안되는 문제 수정
			if (this.authHeader == 'none') {
				this.setPlayerProperty(this.PROPERTY_TYPE_AUTH_BASIC, 0, 0);
			}
			else {
				// do nothing. UniPlayer 기본이 Auth: Basic 임.
			}
			//
			
			cbOnPlaySuccess = successCallback;	// onStreamInfoReady에서 실행됨.
			
			var retValue = webapis._plugin(ePlayerPlugin, 'StartPlayback', (isec !== undefined ? Number(isec) : 0) );	// 1: success, -1: fail			
			if (retValue == -1) {
				if (typeof errorCallback == 'function') {					
					errorCallback(ErrorsHelper.createWebAPIException_byType('UnknownError'));
					this._setStatus(webapis.avplay.PLAY_STATE_STOPPED);
				}
				return;
			}
			
			if (!bFrontPanelLock && bBDPlayer) {
				iFrontPanel.setState(iFrontPanel.Enum.FRONT_DISPLAY_PLAY);
			}
			this._setStatus(webapis.avplay.PLAY_STATE_STARTED);
			setScreenSaver(false);
			
			return retValue == 1;	
		}
		
		/*
		 * Function: stop
		 * 	stops playback of content.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.stop = function () {	// [IDL]
			alert('[AVPlay'+id+'] stop()');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
	//        // PLAY_STATE_STARTED, PLAY_STATE_PAUSED, PLAY_STATE_PREPARED 상태에서만 stop 할 수 있도록 강제
	//        if (this.status != webapis.avplay.PLAY_STATE_STARTED &&
	//        this.status != webapis.avplay.PLAY_STATE_PAUSED &&
	//        this.status != webapis.avplay.PLAY_STATE_STOPPED &&	// stop() 중복 호출시 throw 하지 않도록 하기 위함
	//        this.status != webapis.avplay.PLAY_STATE_PREPARED) {
	//            alert('[' + sName + '] !THROW ERROR! WebAPIDOMErrorCode.INVALID_STATE_ERR');
	//            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.INVALID_STATE_ERR);
	//        }
			
			if (self.macrovision) {	// widget has to clear to level 0 before widget calls 'Stop', if widget set macrovision level.
				self.setMacrovision(self.APS_ALL_OFF);	// 0: APS_ALL_OFF, 
			}
			
			var retValue = webapis._plugin(ePlayerPlugin, 'Stop');	// 1: success, -1: fail
			
			if (!bFrontPanelLock && bBDPlayer) {
				iFrontPanel.setState(iFrontPanel.Enum.FRONT_DISPLAY_ONLINE); // App 에서는 Stop 대신 ONLINE 표시
			}
			self._setStatus(webapis.avplay.PLAY_STATE_STOPPED);
			setScreenSaver(true);
			
			return retValue == 1;
		}
		
		/*
		 * Function: pause
		 * 	pauses the current playback.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.pause = function () {	// [IDL]
			alert('[AVPlay'+id+'] pause()');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			var retValue = webapis._plugin(ePlayerPlugin, 'Pause');	// 1: success, -1: fail
	        
			if (retValue == -1) {
				
			}
			else {
				if (!bFrontPanelLock && bBDPlayer) {
		            iFrontPanel.setState(iFrontPanel.Enum.FRONT_DISPLAY_PAUSE);
		        }
		        this._setStatus(webapis.avplay.PLAY_STATE_PAUSED);
		        setScreenSaver(true);
			}
			
			alert('[AVPlay'+id+'] pause() returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: resume
		 * 	resumes the playback of paused content.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.resume = function () {	// [IDL]
			alert('[AVPlay'+id+'] resume()');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			var retValue = webapis._plugin(ePlayerPlugin, 'Resume');	// 1: success, -1: fail
			
			if (retValue == -1) {
	            
	        }
	        else {
	            if (!bFrontPanelLock && bBDPlayer) {
		            iFrontPanel.setState(iFrontPanel.Enum.FRONT_DISPLAY_PLAY);
		        }
		        this._setStatus(webapis.avplay.PLAY_STATE_STARTED);
		        setScreenSaver(false);
	        }
			
			alert('[AVPlay'+id+'] resume() returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: jumpForward
		 * 	jumps forward from the current playback point for amount of second specified by parameter.
		 *
		 * Parameters:
		 * 	o sec	- (Number) relative time offset from current time in second. 
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.jumpForward = function (sec) {	// [IDL]
			alert('[AVPlay'+id+'] jumpForward('+sec+')');
			var isec = _checkNumberType(sec);
	        // Parameter Type Checking
	        
	        if(isec < 0) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
	        
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
		   
		    var FINISH_OFFSET = 5000; // jumpForward 시 너무 duration에 가깝게 값을 주면 오동작 하는 문제 방지
		    if ((Number(iCurrentPlayTime.millisecond) + (isec * 1000)) > (self.duration - FINISH_OFFSET)) {
				alert('[AVPlay'+id+'] The offset is too big! '+isec+' -> ' + (isec - (FINISH_OFFSET / 1000)));
				isec = isec - (FINISH_OFFSET / 1000);
			}
		    
			var retValue = webapis._plugin(ePlayerPlugin, 'JumpForward', isec);	// 1: success, -1: fail
			alert('[AVPlay'+id+'] jumpForward('+isec+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: jumpBackward
		 * 	Jumps backward from the current playback point for the amount of second specified by parameter.
		 *
		 * Parameters:
		 * 	o sec	- (Number) relative time offset from current time in second.
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.jumpBackward = function (sec) {	// [IDL]
			alert('[AVPlay'+id+'] jumpBackward('+sec+')');
			var isec = _checkNumberType(sec);
	        // Parameter Type Checking
	        if(isec < 0) {	        	
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (iCurrentPlayTime.millisecond - (isec * 1000) < 0) {
				alert('[AVPlay'+id+'] The offset is too big! '+isec+' -> ' + parseInt(iCurrentPlayTime.millisecond / 1000));
				isec = parseInt(iCurrentPlayTime.millisecond / 1000);
			}
			
			var retValue = webapis._plugin(ePlayerPlugin, 'JumpBackward', isec);	// 1: success, -1: fail
			alert('[AVPlay'+id+'] jumpBackward('+isec+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: setSpeed
		 * 	sets the playback speed of currently playing content.
		 *
		 * Parameters:
		 * 	o speed	- (Number) specifies the playback speed in multiple of 2. This can be negative integer for backward playback.
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setSpeed = function (speed) {	// [IDL] // for contents which use Widevine or Has
			alert('[AVPlay'+id+'] setSpeed('+speed+')');
			var ispeed = _checkNumberType(speed);
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			var retValue = webapis._plugin(ePlayerPlugin, 'SetPlaybackSpeed', ispeed);	// 1: success, -1: fail
			if(retValue == -1)
			{
				alert('[AVPlay'+id+'] setSpeed() returns fail.');
				return false;
			}
			this._setStatus(webapis.avplay.PLAY_STATE_STARTED);
			alert('[AVPlay'+id+'] setSpeed('+ispeed+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: setAudioStreamID
		 * 	sets audio track or subtitle widget wants to play.
		 *
		 * Parameters:
		 * 	o index	- (Number) index of audio stream
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setAudioStreamID = function (index) {	// [IDL]
			alert('[AVPlay'+id+'] setAudioStreamID('+index+')');
			var iIndex = _checkNumberType(index);						
	        // Parameter Type Checking	        
	        if(iIndex < 0){
	        	throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
	        }
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
		
			var streamType = 1;	// 1 means audio, and 5 is subtitle
			return this.setStreamID (streamType, iIndex);
		}
		
		/*
		 * Function: setSubtitleStreamID
		 * 	This function sets the current subtitle track to the subtitle track id given in the parameter.
		 *
		 * Parameters:
		 * 	o index	- (Number) subtitle track index
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setSubtitleStreamID = function (index) {	// [IDL]
			alert('[AVPlay'+id+'] setSubtitleStreamID('+index+')');
			var iIndex = _checkNumberType(index);
	        // Parameter Type Checking	        
	        if(iIndex < 0){
	        	throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
	        }
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (!this.getSubtitleAvailable()) return;
			
			var streamType = 5;	// 1 means audio, and 5 is subtitle
			return this.setStreamID (streamType, iIndex);
		}
		
		/*
		 * Function: getCurrentBitrate
		 * 	Gets the current playing bitrate.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Number) returns the current bitrate information for currently playing content.
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.getCurrentBitrate = function () {	// [IDL]
			alert('[AVPlay'+id+'] getCurrentBitrates()');
		
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			var retValue = webapis._plugin(ePlayerPlugin, 'GetCurrentBitrates');	// x: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : getCurrentBitrates()');
				return false;
			}
			else {
				alert('[AVPlay'+id+'] getCurrentBitrates() returns ' + retValue);
				return retValue;
			}
		}
		
		/*
		 * Function: getAvailableBitrates
		 * 	retrieves available bitrates information, if playing content supports adaptive streaming.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) returns available bitrates information for currently playing content in String format on success, otherwise it returns "".
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.getAvailableBitrates = function () {	// [IDL]
			alert('[AVPlay'+id+'] getAvailableBitrates()');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
		
			var retValue = webapis._plugin(ePlayerPlugin, 'GetAvailableBitrates');	// x: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : getAvailableBitrates()');
				return false;
			}
			else {
				alert('[AVPlay'+id+'] getAvailableBitrates() returns ' + retValue);
				return retValue;
			}
		}
		
		var _bstartSubtitle = false;

		
		/*
		 * Function: startSubtitle
		 * 	requests media player to parse specified smi file and to send event which includes subtitle string at the time it has to be displayed.
		 *
		 * Parameters:
		 * 	o option	- (SubtitleOption) option for the subtitle
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.startSubtitle = function (option) {	// [IDL]
			alert('[AVPlay'+id+'] startSubtitle('+option+')');
	        /*
		         option: {
			         path: ,
			         streamID: ,
			         sync: ,
			         callback:
		         }
	         *
	         */// Parameter Type Checking
	        if (
	            !_isType(option, 'object') ||
				!_isType(option.path, 'string') ||
	            !_isType(option.streamID, 'undefined|number') ||
	            !_isType(option.sync, 'undefined|number') ||
	            !_isType(option.callback, 'function')
	        ) {	        	
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        	        
	        if(!_isType(option.path, 'undefined')){	        	
	        	var tempPath = String(option.path);
	        	option.path = tempPath;
	        }
	        if(!_isType(option.streamID, 'undefined')){	        	
	        	var tempStrID = Number(option.streamID);
	        	option.streamID = tempStrID;
	        }
	        if(!_isType(option.sync, 'undefined')){	        	
	        	var tempSync = Number(option.sync);
	        	option.sync = tempSync;
	        }
	        
	        
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (!this.getSubtitleAvailable()) return;
		
			alert('[AVPlay'+id+'] Subtitle Path : ' + option.path);
			
			var retValue = webapis._plugin(ePlayerPlugin, 'StartSubtitle', option.path);	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				_bstartSubtitle = false;
				alert('[AVPlay'+id+'] Not support API : startSubtitle()');
				return false;
			}
			else {
				if ( retValue == 1 ) {
					bBlockSubtitleEvent = false;
					cbOnSubtitle = option.callback;
					
					this.setSubtitleStreamID(option.streamID || 0);
					this.setSubtitleSync(option.sync || 0);
					
					_bstartSubtitle = true;
				}
				alert('[AVPlay'+id+'] startSubtitle() returns ' + (retValue == 1));
				return retValue == 1;
			}
		}
		
		/*
		 * Function: stopSubtitle
		 * 	stops displaying subtilte.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.stopSubtitle = function () {
			alert('[AVPlay'+id+'] stopSubtitle()');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (!this.getSubtitleAvailable()) return;
			
			bBlockSubtitleEvent = true;
		}
		
		/*
		 * Function: setSubtitleSync
		 * 	adjusts displayed subtitle timeing when sync between subtitle and a/v playback is not matched.
		 *
		 * Parameters:
		 * 	o millisec	- (Number) sync delay
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setSubtitleSync = function (millisec) {	// [IDL]
			alert('[AVPlay'+id+'] setSubtitleSync('+millisec+')');
			// Parameter Type Checking
			var imillisec = _checkNumberType(millisec);	        
	        
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (!this.getSubtitleAvailable()) return;
			
			var retValue = webapis._plugin(ePlayerPlugin, 'SetSubtitleSync', imillisec);	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : setSubtitleSync()');
				return false;
			}
			else {
				alert('[AVPlay'+id+'] setSubtitleSync('+imillisec+') returns ' + (retValue == 1));
				if (retValue == 1) {
					nSubtitleSyncTime = imillisec;
				}
				return retValue == 1;
			}
		}
		
		
				
		/*
		 * Function: GetStreamLnaguageInfo
		 * 	retrieves language value of specified subtitle
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(string) returns the current subtitle language information for currently playing content.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */		
		this.GetStreamLanguageInfo = function (index) {	// [IDL]
			
			var nIdx = _checkNumberType(index);
			
			 // Parameter Type Checking
			 
	        if(nIdx < 0) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			}
			
			 // Parameter Type Checking End 
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (!this.getSubtitleAvailable()) return;
			if (!_bstartSubtitle) throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.INVALID_STATE_ERR); 
			
			var retValue = webapis._plugin(ePlayerPlugin, 'GetStreamLanguageInfo', 5,nIdx);	//-1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : GetStreamLanguageInfo()');
				return false;
			}
			else {
				if(retValue != -1) {
					alert('[AVPlay'+id+'] GetStreamLanguageInfo() returns ' + retValue);
					var val = LanguageNumToStr(retValue);				
					return val; 					
				} else {
					alert('[AVPlay'+id+'] GetStreamLanguageInfo() false');
				}
				
			}
		};
		
		var LanguageNumToStr = function(num){
			var nHex = num.toString(16);
			
			var sHex1 = '0x'+nHex.substr(0,2);
			var sHex2 = '0x'+nHex.substr(2,2);
			var sHex3 = '0x'+nHex.substr(4,2);
			
			var str1 = String.fromCharCode(sHex1);
			var str2 = String.fromCharCode(sHex2);
			var str3 = String.fromCharCode(sHex3);
			
			var str = str1 + str2 + str3;
			
			return str; 
		}
		
		
		/*
		streamLanguageStr = {
			7040882 : 'kor',
			6647399 : 'eng',
			7565409 : 'spa',
			6713957 : 'fre',
			6975598 : 'jpn',
			6514793 : 'chi',
			6776178 : 'ger',
			6911073 : 'ita',
			7501171 : 'rus',
			7368562 : 'por',			
		}
		*/
		
		
		
		/*
		 * Function: setDisplayRect
		 * 	sets the display area of video content in TV screen
		 *
		 * Parameters:
		 * 	o rect - (SRect) the rectagle postion for the display area
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setDisplayRect = function (rect) {
			alert('[AVPlay'+id+'] setDisplayRect('+rect+')');
			
	        // Parameter Type Checking
	        if (
	            !_isType(rect, 'object') ||
	            !_isType(rect.top, 'number') ||
	            !_isType(rect.left, 'number') ||
	            !_isType(rect.width, 'number') ||
	            !_isType(rect.height, 'number')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			if (!rect) {
				alert('[AVPlay'+id+'] No rect parameter. Set DisplayRect to Full screen..');
	            rect = new SRect(0, 0, curWidget.width, curWidget.height);
			}
	
			iDisplayRect = rect;
			this.__defineGetter__('displayRect', function(){return rect;}); // [Instance properties]
			
			eInnerContainerDiv.style.left = iDisplayRect.left + 'px';
			eInnerContainerDiv.style.top = iDisplayRect.top  + 'px';
			eInnerContainerDiv.style.width = iDisplayRect.width + 'px';
			eInnerContainerDiv.style.height = iDisplayRect.height + 'px';
			
	//		ePlayerPlugin.style.left = iDisplayRect.left + 'px';
	//		ePlayerPlugin.style.top = iDisplayRect.top  + 'px';
			ePlayerPlugin.style.width = iDisplayRect.width + 'px';
			ePlayerPlugin.style.height = iDisplayRect.height + 'px';
			
			if (this.status == webapis.avplay.PLAY_STATE_STARTED || this.status == webapis.avplay.PLAY_STATE_PAUSED) {
				var fitDisplayArea = this.getFitDisplayArea(this.videoWidth, this.videoHeight);
				this.setDisplayArea(fitDisplayArea);
			}
		}
		/*
		 * Function: clear
		 * 	erases TV screen with black.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.clear = function () {
			alert('[AVPlay'+id+'] clear()');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			var retValue = webapis._plugin(ePlayerPlugin, 'ClearScreen');	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : ClearScreen()');
				return false;
			}
			else {
				alert('[AVPlay'+id+'] clear() returns ' + (retValue == 1));
				return retValue == 1;
			}
		}
		/*
	     * Function: show
	     *  shows the display area.
	     *
	     * Parameters:
	     *  N/A
	     *
	     * Returns:
	     *  (Boolean) True on success and false on failure.
	     *
	     * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	     */
	    this.show = function(){
	        alert('[AVPlay'+id+'] show()');
	        
	        if (!bInitialize) {
	            alert('[AVPlay'+id+'] Do init() first..');
	            return false;
	        };
	        
	        /*
	         * display:none 으로 설정하면 Plugin 함수 콜이 불가능하다. (Webkit)
	         * -> [111220] visibility 속성으로 조절하도록 함
	         * -> [120118] 마우스 대응을 위해 width, height를 조정하도록 수정.
	         * -> [120125] visibility 로 마우스 대응 가능
	         */
	        
	        // [111220] visibility 속성으로 조절하도록 함
	        eInnerContainerDiv.style.visibility = 'visible';
	    }
	    /*
	     * Function: hide
	     *  hides display area.
	     *
	     * Parameters:
	     *  N/A
	     *
	     * Returns:
	     *  (Boolean) True on success and false on failure.
	     *
	     * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	     */
	    this.hide = function(){
	        alert('[AVPlay'+id+'] hide()');
	        
	        if (!bInitialize) {
	            alert('[AVPlay'+id+'] Do init() first..');
	            return false;
	        };
	        
			eInnerContainerDiv.style.visibility = 'hidden';
	    }
		
		// get/setZIndex는 innerContainer의 값을 조작한다.
	    /*
	     * Function: getZIndex
	     * 	This function returns z-index of the AVPlay.
	     *
	     * Parameters:
	     * 	o N/A
	     *
	     * Returns:
	     * 	(Number) the z-index of the AVPlay.
	     */
	    this.getZIndex = function(){
			alert('[AVPlay'+id+'] getZIndex()');
	        if (!bInitialize) {
	            alert('[AVPlay'+id+'] Do init() first..');
	            return false;
	        };
			
	        alert('[AVPlay'+id+'] getZIndex() returns ' + this.zIndex);
	        return this.zIndex;
	    }
	    /*
	     * Function: setZIndex
	     * 	This function sets z-index of the AVPlay.
	     *
	     * Parameters:
	     * 	o zIndex - (Number) z-index
	     *
	     * Returns:
	     * 	nothing
	     */
	    this.setZIndex = function(zIndex){
			alert('[AVPlay'+id+'] setZIndex('+zIndex+')');
	        if (!bInitialize) {
	            alert('[AVPlay'+id+'] Do init() first..');
	            return false;
	        };
			
			// Parameter Type Checking
	        if (
	            !_isType(zIndex, 'number')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
			// [120327] jQuery 사용 제거
			document.getElementById(PLAYER_CONTAINER_DIV_ID + id).style.zIndex = zIndex; // "PLAYER_CONTAINER_DIV_ID + id" 는 innerContainer의 ID 
			
			/*
			 * this.zIndex 를 function(){return innerContainer.style.zIndex;} 로 지정했기 때문에,
			 * this.zIndex 는 자동으로 변경됨.
			 */
	    }
		this.getVolume = function () {
	        alert('[AVPlay'+id+'] getVolume()');
	        
	        if (!bInitialize) {
	            alert('[AVPlay'+id+'] Do init() first..');
	            return false;
	        };
			
			if (this.status != webapis.avplay.PLAY_STATE_STARTED && this.status != webapis.avplay.PLAY_STATE_PAUSED) {
				alert('[AVPlay'+id+'] getVolume() is available on PLAY_STATE_STARTED or PLAY_STATE_PAUSED.');
				return false;
			}
			
			alert('[AVPlay'+id+'] getVolume() returns ' + nVolume);
			return nVolume;
	    }
		this.setVolume = function (volume) {
	        alert('[AVPlay'+id+'] setVolume('+volume+')');
	        
	        // Parameter Type Checking
	        if (
	            !_isType(volume, 'number')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
	        if (!bInitialize) {
	            alert('[AVPlay'+id+'] Do init() first..');
	            return false;
	        };
			
			if (this.status != webapis.avplay.PLAY_STATE_STARTED && this.status != webapis.avplay.PLAY_STATE_PAUSED) {
	            alert('[AVPlay'+id+'] setVolume() is available on PLAY_STATE_STARTED or PLAY_STATE_PAUSED.');
	            return false;
	        }
	        
			var retValue = webapis._plugin(ePlayerPlugin, 'SetVolume', volume);
			if (retValue == 1) {
				nVolume = volume;
			}
			
			alert('[AVPlay'+id+'] setVolume('+volume+') returns ' + (retValue == 1));
			return retValue == 1;
	    }
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// IDL 에 없는 EMP Plugin API들
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		this.setCropArea = function (successCallback, errorCallback, rect) {	// [IDL]
			alert('[AVPlay'+id+'] setCropArea('+typeof successCallback+','+typeof errorCallback+','+rect+')');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			
			var retValue = webapis._plugin(ePlayerPlugin, 'SetCropArea', rect.left, rect.top, rect.width, rect.height);	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : setCropArea()');
				errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
			}
			else {
				alert('[AVPlay'+id+'] setCropArea('+rect+') returns ' + (retValue == 1));
	//			this.cropArea = rect;	// [Instance properties]
				this.__defineGetter__('cropArea', function(){return rect;}); // [Instance properties]
				successCallback();
			}
			
			alert('[AVPlay'+id+'] setCropArea() returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setDisplayArea = function (rect) {	// [IDL]
			alert('[AVPlay'+id+'] setDisplayArea('+rect+')');
			
			if (!bInitialize) {
				alert('[AVPlay'+id+'] Do init() first..');
				return false;
			};
			oPlayOption.displayArea = rect;
			var retValue = null;
			if (oPluginVer.isSEF) {
				
				retValue = webapis._plugin(ePlayerPlugin, 'SetDisplayArea', rect.left, rect.top, Math.min(rect.width+1, curWidget.width), Math.min(rect.height+1, curWidget.height), curWidget.height);	// 1: success, -1: fail
			}
			else {
				// Converting for resolution
				var resolutionConstant; 
				if (curWidget.height == 540) resolutionConstant = 1;
				else if (curWidget.height == 720) resolutionConstant = 0.75;	// 540 / 720
				else if (curWidget.height == 1080) resolutionConstant = 0.5;	// 540 / 1080
				else resolutionConstant = 1;
				
				var nLeft = Math.ceil(rect.left * resolutionConstant);
				var nTop =  Math.ceil(rect.top * resolutionConstant);
				var nWidth =  Math.ceil(rect.width * resolutionConstant)+1;
				var nHeight =  Math.ceil(rect.height * resolutionConstant)+1;
				retValue = webapis._plugin(ePlayerPlugin, 'SetDisplayArea', nLeft, nTop, Math.min(nWidth, 960), Math.min(nHeight, 540));	// 1: success, -1: fail
			}
			
			this.__defineGetter__('displayArea', function(){return rect;}); // [Instance properties]
			return retValue;
		}
		this.getDuration = function () {
			var retValue = webapis._plugin(ePlayerPlugin, 'GetDuration');	// x: success, -1: fail
			alert('[AVPlay'+id+'] getDuration() returns ' + retValue);
			
			this.__defineGetter__('duration', function(){return retValue;}); // [Instance properties]
			return retValue;
		}
		this.getVideoResolution = function () {
			var retValue = null;
			if (oPluginVer.isSEF) {
				retValue = webapis._plugin(ePlayerPlugin, 'GetVideoResolution');	// x: success, -1: fail
			}
			else {
				retValue = ePlayerPlugin.GetVideoWidth() + '|' + ePlayerPlugin.GetVideoHeight();	// EMP Player 형태로 맞춤
			}
			alert('[AVPlay'+id+'] getVideoResolution() returns ' + retValue);
			return retValue;
		}
		this.getTotalNumOfStreamID = function (streamType) {
			/*
			 * streamType
			 * 1: audio
			 * 5: subtitle
			 */
			var retValue = webapis._plugin(ePlayerPlugin, 'GetTotalNumOfStreamID', streamType);	// x: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : getTotalNumOfStreamID()');
			}
			else {
				alert('[AVPlay'+id+'] getTotalNumOfStreamID('+streamType+') returns ' + retValue);
				return retValue;
			}
		}
		this.setStreamID = function (streamType, index) {
			var retValue = webapis._plugin(ePlayerPlugin, 'SetStreamID', streamType, index);	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : setStreamID()');
			}
			else {
				alert('[AVPlay'+id+'] setStreamID('+streamType+','+index+') returns ' + (retValue == 1));
				return retValue == 1;
			}
		}
		this.getStreamLanguageInfo = function (streamType, index) {
			var retValue = webapis._plugin(ePlayerPlugin, 'GetStreamLanguageInfo', streamType, index);	// x: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : getStreamLanguageInfo()');
			}
			else {
				alert('[AVPlay'+id+'] getStreamLanguageInfo('+streamType+','+index+') returns ' + retValue);
				return retValue;
			}
		}
		this.getStreamExtraData = function (streamType, index) {
			var retValue = webapis._plugin(ePlayerPlugin, 'GetStreamExtraData', streamType, index);	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : getStreamExtraData()');
			}
			else {
				alert('[AVPlay'+id+'] getStreamExtraData('+streamType+','+index+') returns ' + retValue);
				return retValue;
			}
		}
		this.setPlayerProperty = function (propertyType, param1, param2) {
			var retValue = webapis._plugin(ePlayerPlugin, 'SetPlayerProperty', propertyType, param1, param2);	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : setPlayerProperty()');
			}
			else {
				alert('[AVPlay'+id+'] setPlayerProperty('+propertyType+','+param1+','+param2+') returns ' + (retValue == 1));
				return retValue == 1;
			}
		}
		this.setTotalBufferSize = function (bytes) {
	//		this.totalBufferSize = bytes;	// [Instance properties]
			this.__defineGetter__('totalBufferSize', function(){return bytes;}); // [Instance properties]
			var retValue = webapis._plugin(ePlayerPlugin, 'SetTotalBufferSize', bytes);	// 1: success, -1: fail
			alert('[AVPlay'+id+'] setTotalBufferSize('+bytes+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setInitialBufferSize = function (bytes) {
	//	    this.initialBufferSize = bytes;	// [Instance properties]
		    this.__defineGetter__('initialBufferSize', function(){return bytes;}); // [Instance properties]
			var retValue = null;
			if (oPluginVer.isSEF) {
				retValue = webapis._plugin(ePlayerPlugin, 'SetInitialBufferSize', bytes);	// 1: success, -1: fail
			}
			else {
				retValue = webapis._plugin(ePlayerPlugin, 'SetInitialBuffer', bytes);	// 1: success, -1: fail
			}
			alert('[AVPlay'+id+'] setInitialBufferSize('+bytes+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setPendingBufferSize = function (bytes) {
	//		this.pendingBufferSize = bytes;	// [Instance properties]
			this.__defineGetter__('pendingBufferSize', function(){return bytes;}); // [Instance properties]
			var retValue = null;
			if (oPluginVer.isSEF) {
				retValue = webapis._plugin(ePlayerPlugin, 'SetPendingBufferSize', bytes);	// 1: success, -1: fail
			}
			else {
				retValue = webapis._plugin(ePlayerPlugin, 'SetPendingBuffer', bytes);	// 1: success, -1: fail
			}
			alert('[AVPlay'+id+'] setPendingBufferSize('+bytes+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setOutputDOT = function (disable) {
			var retValue = webapis._plugin(ePlayerPlugin, 'SetOutputDOT', (disable ? 1 : 0));	// 1: success, -1: fail
			if (retValue == webapis._pluginDef.PLR_NOT_IMPLEMENT) {
				alert('[AVPlay'+id+'] Not support API : setOutputDOT()');
			}
			else {
				alert('[AVPlay'+id+'] setOutputDOT('+disable+') returns ' + (retValue == 1));
				return retValue == 1;
			}
		}
		this.setMacrovision = function (macrovisionLevel) {
			var retValue = webapis._plugin(ePlayerPlugin, 'SetMacrovision', macrovisionLevel);	// 1: success, -1: fail
			alert('[AVPlay'+id+'] setMacrovision('+macrovisionLevel+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setVBIData = function (macrovisionType, cgmsType) {
			var retValue = webapis._plugin(ePlayerPlugin, 'SetVBIData', macrovisionType, cgmsType);	// 1: success, -1: fail
			alert('[AVPlay'+id+'] setVBIData('+macrovisionType+','+cgmsType+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setICT = function (on) {
			var retValue = webapis._plugin(ePlayerPlugin, 'SetICT', on);	// 1: success, -1: fail
			alert('[AVPlay'+id+'] setICT('+on+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		this.setHEVC = function(customData,licenseURL) {
			var retValue = webapis._plugin('Device', 'SupportHEVC');	// 1: success, -1: fail
			alert('[AVPlay'+id+'] supportHEVC returns ' + (retValue == 1));
			if(retValue == 1){
				var retData = this.setPlayerProperty(this.PROPERTY_TYPE_PLAY_READY_CUSTOM_DATA, customData, customData.length); // 3: MSD_SET_CUSTOMDATA
				var retLicense = this.setPlayerProperty(this.PROPERTY_TYPE_PLAY_READY_LICENSE_SERVER, licenseURL, licenseURL.length);
				alert('[AVPlay'+id+'] setHEVC returns customData : ' + retData +' / ' + 'licenseURL :' + retLicense);
				return (retData == 1 && retLicense == 1);
			}else {
				alert('[AVPlay'+id+'] This device is not supported HEVC!!')
				return false;	
			}			
		}			
	
		
		
		
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// 기타 AVPlay 기능
		//// 1. <object> 관리  
		//// 2. 자동 display area 조정
		//// 3. Event listener 
		//// 4. Status 관리
		//// 5. Front panel 관리
		//// 6. Screen saver 관리  
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		// SetPlayerProperty
		// Property type
		this.PROPERTY_TYPE_COOKIE =						1;
		this.PROPERTY_TYPE_3D =							2;
		this.PROPERTY_TYPE_PLAY_READY_CUSTOM_DATA =		3;
		this.PROPERTY_TYPE_PLAY_READY_LICENSE_SERVER =	4;
		this.PROPERTY_TYPE_MP3_LIVE_STREAM =			5;
		this.PROPERTY_TYPE_AUTH_BASIC =					7; // [120801] 차별화 서비스 튜토리얼 동영상 Genoa 에서 Skip 안되는 문제 수정
		
		// Macrovision Type/Level
		this.APS_ALL_OFF =			0;
		this.APS_AGC_ON_ONLY =		1;
		this.APS_AGC_ON_CS_2L =		2;
		this.APS_AGC_ON_CS_4L =		3;
		
		// CGMS Type (for setVBIData)
		this.CGMS_COPY_FREE = 		0;
		this.CGMS_COPY_NO_MORE = 	1;
		this.CGMS_COPY_ONCE = 		2;
		this.CGMS_COPY_NEVER = 		3;
		
		// ICT On/Off (for setICT)
		this.ICT_OFF =		0;
		this.ICT_ON =		1;
		
		var PLAYER_OBJECT_ID = '_plugin_Player_';	// webapis._plugin() 에서 사용하는 것과 동일하게 한다. [110810]
		var PLAYER_OBJECT_Z_INDEX = 10;
		var PLAYER_CONTAINER_DIV_ID = '_pluginObjectPlayerContainer_';	// webapis -> addPluginObject() 에서 사용하는 것과 동일하게 한다. [110810] 
		var PLAYER_CONTAINER_DIV_Z_INDEX = 10;
		
		var self = this;
		var ePlayerPlugin = null;
		var eInnerContainerDiv = null;
		var bInitialize = false;
		var iDisplayRect = new _setDefaultRect();	// instance of SRect
		var bAutoRatio = true;
		var bFrontPanelLock = false;
		
		var cbOnSubtitle = null;
		var bBlockSubtitleEvent = false;	// StopSubtitle()은 Subtitle event를 무시하는 것으로 구현됨.
		var cbOnPlaySuccess = null;	// .play()의 첫번재 파라미터로, onStreamInfoReady에서 실행되야 함.
		
		var oInitOption = {};	// AVPlayInitialOption -> init()에서 저장
		var oPlayOption = {};	// AVPlayOption -> open()에서 저장
		var nSubtitleSyncTime = 0;	// ms
		var bSubtitleAvailable = null;
		
		var nFirmwareYear = null;
		var nFirmwareVer = null;
		var oPluginVer = null;
		var iFrontPanel = null;
		var bBDPlayer = null; // BD Player 여부. Front Panel 설정 여부를 결정함.
		
		var iCurrentPlayTime = null;
		
		/*
		 * Player의 Volume이 분리되었음.
		 * InitPlayer 시 100으로 설정, 
		 * Plugin->SetVolume을 통해 변경되나, GetVolume 하는 함수는 없음.
		 * AVPlay 내부적으로 nVolume 변수를 유지하여 지원하도록 함.
		 */
		var nVolume = 100;
		
		this.Event2String = {};	// serial log 에 보여주기 위한 string
		this.Event2String[webapis.avplay.CONNECTION_FAILED] = 'CONNECTION_FAILED';
		this.Event2String[webapis.avplay.AUTHENTICATION_FAILED] = 'AUTHENTICATION_FAILED';
		this.Event2String[webapis.avplay.STREAM_NOT_FOUND] = 'STREAM_NOT_FOUND';
		this.Event2String[webapis.avplay.NETWORK_DISCONNECTED] = 'NETWORK_DISCONNECTED';
		this.Event2String[webapis.avplay.NETWORK_SLOW] = 'NETWORK_SLOW';
		this.Event2String[webapis.avplay.RENDER_ERROR] = 'RENDER_ERROR';
		this.Event2String[webapis.avplay.RENDERING_START] = 'RENDERING_START';
		this.Event2String[webapis.avplay.RENDERING_COMPLETE] = 'RENDERING_COMPLETE';
		this.Event2String[webapis.avplay.STREAM_INFO_READY] = 'STREAM_INFO_READY';
		this.Event2String[webapis.avplay.DECODING_COMPLETE] = 'DECODING_COMPLETE';
		this.Event2String[webapis.avplay.BUFFERING_START] = 'BUFFERING_START';
		this.Event2String[webapis.avplay.BUFFERING_COMPLETE] = 'BUFFERING_COMPLETE';
		this.Event2String[webapis.avplay.BUFFERING_PROGRESS] = 'BUFFERING_PROGRESS';
		this.Event2String[webapis.avplay.CURRENT_PLAYBACK_TIME] = 'CURRENT_PLAYBACK_TIME';
		this.Event2String[webapis.avplay.AD_START] = 'AD_START';
		this.Event2String[webapis.avplay.AD_END] = 'AD_END';
		this.Event2String[webapis.avplay.RESOLUTION_CHANGED] = 'RESOLUTION_CHANGED';
		this.Event2String[webapis.avplay.BITRATE_CHANGED] = 'BITRATE_CHANGED';
		this.Event2String[webapis.avplay.SUBTITLE] = 'SUBTITLE';
		this.Event2String[webapis.avplay.CUSTOM] = 'CUSTOM';
		
		this.State2String = {};
		this.State2String[webapis.avplay.PLAY_STATE_IDLE] = 'PLAY_STATE_IDLE';
		this.State2String[webapis.avplay.PLAY_STATE_INITIALIZED] = 'PLAY_STATE_INITIALIZED';
		this.State2String[webapis.avplay.PLAY_STATE_STOPPED] = 'PLAY_STATE_STOPPED';
		this.State2String[webapis.avplay.PLAY_STATE_PREPARED] = 'PLAY_STATE_PREPARED';
		this.State2String[webapis.avplay.PLAY_STATE_STARTED] = 'PLAY_STATE_STARTED';
		this.State2String[webapis.avplay.PLAY_STATE_PAUSED] = 'PLAY_STATE_PAUSED';
		
		function initialize(containerID, zIndex, pluginObjectID) {
			alert('[AVPlay'+id+'] initialize('+(containerID||'')+','+(zIndex||'')+','+(pluginObjectID||'')+')');
			
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			alert('[AVPlay'+id+'] Firmware : ' + sFirmware);
			
			/*
			 * aResult[0] = 2011-1007
			 * aResult[1] = 2011
			 * aResult[2] = 1007
			 */
			var aResult = sFirmware.match(/(\d+)-(\d+)/);
			nFirmwareYear = aResult[1];	
			nFirmwareVer = aResult[2];
			
			// <object> 삽입
			self.setPlayerPluginObject(containerID, zIndex, pluginObjectID);
			
			// App 종료시 .destory() 호출
			if (nFirmwareYear >= 2012) { // Echo
				window.addEventListener('unload', function(){ // Webkit 은 body 에 addEventListener()로 unload 이벤트 등록 불가.
					alert('[AVPlay'+id+'] !!!WINDOW UNLOAD!!!');
					self.destroy();
				}); 
			}
			else { // Genoa, Valencia
				document.getElementsByTagName('body')[0].addEventListener('unload', self.destroy);
			}
			
			webapis._plugin('TVMW', 'GetVersion');	// unload -> stop -> setScreenSaver(false) -> TVMW 삽입시 에러나는 것 방지
			
			iFrontPanel = new FrontPanel();
			bBDPlayer = webapis.tv.info.getProduct() == webapis.tv.info.PRODUCT_TYPE_BD;
			
			// GetVersion()을 한번만 호출하도록 한다.
			oPluginVer = webapis._plugin(ePlayerPlugin, 'GetVersion');
			
			bInitialize = true;
		}
		
		function initializeEnv () {
			bBlockSubtitleEvent = false;
			
	//		this.__defineGetter__('id', function(){return id;});
		    this.__defineGetter__('url', function(){return null;});
		    this.__defineGetter__('duration', function(){return null;});
		    this.__defineGetter__('videoWidth', function(){return null;});
		    this.__defineGetter__('videoHeight', function(){return null;});
	//	    this.__defineGetter__('displayRect', function(){return null;});
		    this.__defineGetter__('displayArea', function(){return null;});
	//	    this.__defineGetter__('containerID', function(){return null;}); // init()으로 지정할 때까지 변하지 않음.
	//	    this.__defineGetter__('zIndex', function(){return null;}); // init()으로 지정할 때까지 변하지 않음.
		    this.__defineGetter__('cropArea', function(){return null;});
		    this.__defineGetter__('totalNumOfVideo', function(){return null;});
		    this.__defineGetter__('totalNumOfAudio', function(){return null;});
		    this.__defineGetter__('totalNumOfSubtitle', function(){return null;});
		    //this.__defineGetter__('mode3D', function(){return null;});
		    this.__defineGetter__('totalBufferSize', function(){return -1;});
		    this.__defineGetter__('pendingBufferSize', function(){return -1;});
		    this.__defineGetter__('initialBufferSize', function(){return -1;});
		    this.__defineGetter__('macrovision', function(){return false;});
	//	    this.__defineGetter__('status', function(){return webapis.avplay.PLAY_STATE_IDLE;});
		}
		this.destroy = function () {
			alert('[AVPlay'+id+'] destroy()');
			self.stop();
		}
		this._setStatus = function (status) {
			alert('[AVPlay'+id+'] _setStatus('+status+') -> ' + this.State2String[status]);
			this.__defineGetter__('status', function(){return status;}); // [Instance properties]
		}
		this.setPlayerPluginObject = function (containerID, zIndex, pluginObjectID) {
			alert('[AVPlay'+id+'] setPlayerPluginObject('+(containerID?containerID:'')+','+(zIndex?zIndex:'')+','+(pluginObjectID?pluginObjectID:'')+')');
			alert('[AVPlay'+id+'] old ePlayerPlugin : ' + ePlayerPlugin);
			
			if (ePlayerPlugin) {	// 이전 Player Object를 제거
				this.stop();	// Play() 상태에서 DOM을 건들면 죽는다.
				ePlayerPlugin.parentNode.removeChild(ePlayerPlugin);
				ePlayerPlugin = null;
			}
			
			if (pluginObjectID) {	// 동일한 object id 가 있으면 제거
				var ePluginObject = document.getElementById(pluginObjectID);
				if (ePluginObject) {
					webapis._plugin(ePluginObject, 'Stop');	// Play() 상태에서 DOM을 건들면 죽는다.
					ePluginObject.parentNode.removeChild(ePluginObject);
				}
			}
			
			/*
			 * [111220] 아래 요구사항을 충족할 수 있도록 구조 변경
			 * 1. 자동으로 생성해준 Container 의 위치가 (0, 0) 이라 (0, 0) 부터 <object> 위치까지 mouse 클릭 불가 문제
			 *  -> 내부 Container 생성 & 내부 Container 위치를 사용자 설정값으로 지정, <object>는 width, height만 지정  
			 * 2. 사용자가 같은 Container 에 <object> 삽입할 경우, 첫번째 넣은 <object>가 DOM 에서 빠졌다가 들어가 오동작
			 *  -> 내부 Container 생성 
			 */
			
			var innerContainer = document.createElement('div');
			innerContainer.id = PLAYER_CONTAINER_DIV_ID + id;
			innerContainer.style.position = 'absolute';
			innerContainer.style.left = '0px'; // 위치는 setDisplayRect() 에서 지정됨
			innerContainer.style.top = '0px';
			innerContainer.style.width = '0px';
			innerContainer.style.height = '0px';
			innerContainer.style.zIndex = (zIndex !== undefined && zIndex !== null) ? zIndex : PLAYER_CONTAINER_DIV_Z_INDEX; // z-index가 있을 경우, container div 에 적용
			
			// Inner Container 삽입			
			if (containerID) { // 사용자가 지정한 container에 삽입
				var eContainerDiv = document.getElementById(containerID);
				if (eContainerDiv) { // Element가 있으면 내부 container를 삽입
					eContainerDiv.appendChild(innerContainer);
					this.__defineGetter__('containerID', function(){return containerID;}); // [Instance properties]
				}
				else { // 없으면 에러
					alert('[AVPlay'+id+'] !ERROR! cannot get ' + containerID + ' element.');
					ePlayerPlugin = null;
					return;
				}
			}
			else { // 지정 안한 경우, body에 삽입
				document.body.appendChild(innerContainer);
				this.__defineGetter__('containerID', function(){return innerContainer.id;}); // [Instance properties]
			}
			
			var bUseSEF = webapis._plugin.getSEFAvailable();
			var sPluginObjectId = pluginObjectID || PLAYER_OBJECT_ID;
			var sZIndex = 'z-index:' + PLAYER_OBJECT_Z_INDEX + ';';
			
			eInnerContainerDiv = document.getElementById(PLAYER_CONTAINER_DIV_ID + id);
			eInnerContainerDiv.innerHTML = getPluginObjectHTML(bUseSEF, sPluginObjectId, sZIndex);
			ePlayerPlugin = document.getElementById(sPluginObjectId);
			
			this.__defineGetter__('zIndex', function(){return innerContainer.style.zIndex;}); // [Instance properties]
			
			if (ePlayerPlugin) {
				// Event listener 생성 -> Multi instance를 지원하기 위함
				// eval은 현재 어휘 유효범위에서 실행되므로 this가 넘어갈 수 있다.
				
				//var strEval = 'PlayerEventListener' + id + ' = new _PlayerEventListener('+id+', this)'; // Genoa:OK
				//eval(strEval);
				
				//121203 woosik 개행문자 포함된 smi 파일일 경우 자막 보이지 않는 이슈 해결을 위해 
				//webkit 이상에서는 object 형태로 이벤트 등록, Genoa 는 String
				var evtListener = window['PlayerEventListener' + id] = new _PlayerEventListener(id, this); // Genoa:OK
				
				// Register Event handler
				if (bUseSEF) {	// SEF 에서는 함수 자체를 넣어야 함, Instance의 메서드 호출은 불가.
					ePlayerPlugin.Open('Player', '1.000', 'Player');
					// [111208] String 형태로도 OnEvent에 등록될 수 있게 Webkit 수정되었음.(by 송동현D)
					//ePlayerPlugin.OnEvent = this.onEvent;
					
                    // 121204 kookheon.kim@samsung.com
                    // LG U+대응, 자막이벤트 핸들러가 호출될때, 자막 데이터에 개행문자 표함된 경우 핸들러 호출이 제대로 되지 않는 문제.
                    // maple 지원을 위해 플러그인 onEvent 등록시 문자열로 함수명을 등록하고 있었으나,
                    // 이 경우 웹킷에서 함수명에 "("와 파라미터를 붙여서 호출하다가 파라미터 내의 개행문자로 인해 호출이 제대로 되지 않음.
                    // 웹킷(2012이상)에 대해서 anonymous 함수를 이용하여 내부에서 이벤트 핸들러 호출하도록 수정함.
                    if(typeof nFirmwareYear != 'null' && nFirmwareYear <= 2011){    // Maple (2011 or lower)
                        alert('register event handler by string');
                        ePlayerPlugin.OnEvent = 'PlayerEventListener' + id + '.onEvent';    // Maple의 경우 문제 발생 소지 남아있음
                    } else {    // Webkit or others
                        alert('register event handler by function');
                        ePlayerPlugin.OnEvent = function () {               
                            evtListener.onEvent.apply(evtListener, arguments);
                        }
                    }
				}
				else {
					if(typeof nFirmwareYear != 'null' && nFirmwareYear <= 2011){
                        ePlayerPlugin.OnBufferingStart      = 'PlayerEventListener' + id + '.onBufferingStart';
                        ePlayerPlugin.OnBufferingComplete   = 'PlayerEventListener' + id + '.onBufferingComplete';
                        ePlayerPlugin.OnBufferingProgress   = 'PlayerEventListener' + id + '.onBufferingProgress';
                        ePlayerPlugin.OnRenderingComplete   = 'PlayerEventListener' + id + '.onRenderingComplete';
                        ePlayerPlugin.OnStreamInfoReady     = 'PlayerEventListener' + id + '.onStreamInfoReady';
                        ePlayerPlugin.OnCurrentPlayTime     = 'PlayerEventListener' + id + '.onCurrentPlayTime';
                        ePlayerPlugin.OnRenderError         = 'PlayerEventListener' + id + '.onRenderError';
                        ePlayerPlugin.OnNetworkDisconnected = 'PlayerEventListener' + id + '.onNetworkDisconnected';
                        ePlayerPlugin.OnConnectionFailed    = 'PlayerEventListener' + id + '.onConnectionFailed';
                        ePlayerPlugin.OnStreamNotFound      = 'PlayerEventListener' + id + '.onStreamNotFound';
                        ePlayerPlugin.OnAuthenticationFailed = 'PlayerEventListener' + id + '.onAuthenticationFailed';
                        ePlayerPlugin.OnResolutionChanged   = 'PlayerEventListener' + id + '.onResolutionChanged';
                        ePlayerPlugin.OnAdStart             = 'PlayerEventListener' + id + '.onAdStart';
                        ePlayerPlugin.OnAdEnd               = 'PlayerEventListener' + id + '.onAdEnd';
                    } else {
                        ePlayerPlugin.onBufferingStart = function () {evtListener.onBufferingStart.apply(evtListener, arguments);}
                        ePlayerPlugin.OnBufferingComplete   = function () {evtListener.onBufferingComplete.apply(evtListener, arguments);}
                        ePlayerPlugin.OnBufferingProgress   = function () {evtListener.onBufferingProgress.apply(evtListener, arguments);}
                        ePlayerPlugin.OnRenderingComplete   = function () {evtListener.onRenderingComplete.apply(evtListener, arguments);}
                        ePlayerPlugin.OnStreamInfoReady     = function () {evtListener.onStreamInfoReady.apply(evtListener, arguments);}
                        ePlayerPlugin.OnCurrentPlayTime     = function () {evtListener.onCurrentPlayTime.apply(evtListener, arguments);}
                        ePlayerPlugin.OnRenderError         = function () {evtListener.onRenderError.apply(evtListener, arguments);}
                        ePlayerPlugin.OnNetworkDisconnected = function () {evtListener.onNetworkDisconnected.apply(evtListener, arguments);}
                        ePlayerPlugin.OnConnectionFailed    = function () {evtListener.onConnectionFailed.apply(evtListener, arguments);}
                        ePlayerPlugin.OnStreamNotFound      = function () {evtListener.onStreamNotFound.apply(evtListener, arguments);}
                        ePlayerPlugin.OnAuthenticationFailed = function () {evtListener.onAuthenticationFailed.apply(evtListener, arguments);}
                        ePlayerPlugin.OnResolutionChanged   = function () {evtListener.onResolutionChanged.apply(evtListener, arguments);}
                        ePlayerPlugin.OnAdStart             = function () {evtListener.onAdStart.apply(evtListener, arguments);}
                        ePlayerPlugin.OnAdEnd               = function () {evtListener.onAdEnd.apply(evtListener, arguments);}
                    }
				}		
			}
			else {
				alert('[AVPlay'+id+'] ! ERROR ! Fail to set Player plugin object.');
			}
			
			return ePlayerPlugin;
			
			function getPluginObjectHTML (useSEF, pluginObjectID, zIndex) {
				var sPluginObjectHTML = '';
				if (useSEF) {
					sPluginObjectHTML = '<OBJECT id="'+pluginObjectID+'" classid="clsid:SAMSUNG-INFOLINK-SEF" style="position:absolute;left:0px;top:0px;width:0px;height:0px;' + zIndex + 'display:block;"></OBJECT>';
				}
				else {
					sPluginObjectHTML = '<OBJECT id="'+pluginObjectID+'" classid="clsid:SAMSUNG-INFOLINK-PLAYER" style="position:absolute;left:0px;top:0px;width:0px;height:0px;' + zIndex + 'display:block;"></OBJECT>';
				}
				alert('[AVPlay'+id+'] getPluginObjectHTML('+pluginObjectID+','+zIndex+') returns ' + sPluginObjectHTML);
				return sPluginObjectHTML;
			}
		}
		
		function getIPAddr () {
			var nNetworkInterface = webapis._plugin('Network', 'GetActiveType');	// 1: wired, 0: wireless, -1: no active connection
			var sIPAddr = webapis._plugin('Network', 'GetIP', nNetworkInterface);
			alert('getIPAddr() returns ' + sIPAddr);
			return sIPAddr;
		}
		this.getSubtitleAvailable = function () {
			/*
			 * T-INFOLINK2011-1006 부터 가능 (배세현D)
			 */
			if (bSubtitleAvailable === null) {	// 체크 안됐을 때만 확인하도록 함
				if (nFirmwareYear >= 2012 || (nFirmwareYear == 2011 && nFirmwareVer >= 1006)) {
					bSubtitleAvailable = true;
				}
				else {
					bSubtitleAvailable = false;
				}
			}
			
			if (!bSubtitleAvailable) {
				alert('[AVPlay'+id+'] Not Support Subtitle function! It is available with "T-INFOLINK2011-1006" or later.');
			}
			
			alert('[AVPlay'+id+'] getSubtitleAvailable() returns ' + bSubtitleAvailable);
			return bSubtitleAvailable;
		}
		this.onEvent = function (type, data, data2) {
			//alert('[AVPlay'+id+'] onEvent('+type+','+data+')');
			
			// Ratio 설정 적용
			switch(type){
				case webapis.avplay.STREAM_INFO_READY:
					var resolution = this.getVideoResolution();					
					if (typeof resolution == 'string') {
						resolution = resolution.split('|');	// '1280|720'
						
						// oPlayOption.displayArea 가 있으면 설정, autoRatio flag 무시
						if (oPlayOption.displayArea) {	// oPlayOption -> displayArea 처리
							this.setDisplayArea(oPlayOption.displayArea);
						}
						else if (bAutoRatio) {
							var fitDisplayArea = this.getFitDisplayArea(resolution[0], resolution[1]);
							this.setDisplayArea(fitDisplayArea);
						}
						
						this.__defineGetter__('videoWidth', function(){return resolution[0];}); // [Instance properties]
						this.__defineGetter__('videoHeight', function(){return resolution[1];}); // [Instance properties]
					}
					else {
						this.__defineGetter__('videoWidth', function(){return null;}); // [Instance properties]
						this.__defineGetter__('videoHeight', function(){return null;}); // [Instance properties]
					}
				
					var nDuration = this.getDuration(); // [120731] Instance properties 가 getDuration() 에서 설정됨
					var nTotalNumOfVideo = this.getTotalNumOfStreamID(3);
					var nTotalNumOfAudio = this.getTotalNumOfStreamID(1);
					var nTotalNumOfSubtitle = this.getTotalNumOfStreamID(5);
					
					// this.__defineGetter__('duration', function(){return nDuration;}); // [Instance properties]
					this.__defineGetter__('totalNumOfVideo', function(){return nTotalNumOfVideo;}); // [Instance properties]
					this.__defineGetter__('totalNumOfAudio', function(){return nTotalNumOfAudio;}); // [Instance properties]
					this.__defineGetter__('totalNumOfSubtitle', function(){return nTotalNumOfSubtitle;}); // [Instance properties]
					
					/*
					 * macrovision 설정
					 */
	                if (oPlayOption.macrovision &&
	                oPlayOption.macrovision.type !== undefined &&
	                oPlayOption.macrovision.ict !== undefined &&
	                oPlayOption.macrovision.vbi !== undefined) {
	                    this.setMacrovision(oPlayOption.macrovision.type);
	                    this.setVBIData(oPlayOption.macrovision.type, oPlayOption.macrovision.vbi);
	                    this.setICT(oPlayOption.macrovision.ict);
	                    
	                    this.__defineGetter__('macrovision', function(){return true;}); // [Instance properties]
	                }
	                else {
						this.__defineGetter__('macrovision', function(){return false;}); // [Instance properties]
	                }
					
					/*
					 * Subtitle 설정
					 */
					if (oPlayOption.subtitle) {
						this.startSubtitle(oPlayOption.subtitle);
					}
					/*
					 * play() 에서 받은 successCallback 실행
					 */
					if (typeof cbOnPlaySuccess == 'function') {
						cbOnPlaySuccess();
					}
					break;
					
				case webapis.avplay.BUFFERING_START:
					if (this.status == webapis.avplay.PLAY_STATE_STOPPED) { // [111222] RENDER_ERROR 이후 발생하는 이벤트 무시
						alert('[AVPlay'+id+'] This BUFFERING_START event occured after stop() call. skip.');
						return;
					}
				
					this._setStatus(webapis.avplay.PLAY_STATE_STARTED);
					if (oInitOption.bufferingCallback) {	// oInitOption -> onbufferingstart 처리
						if (typeof oInitOption.bufferingCallback.onbufferingstart == 'function') {
							oInitOption.bufferingCallback.onbufferingstart();
						}
					}
					break;
					
				case webapis.avplay.BUFFERING_PROGRESS:
					if (this.status == webapis.avplay.PLAY_STATE_STOPPED) { // [111222] RENDER_ERROR 이후 발생하는 이벤트 무시
						alert('[AVPlay'+id+'] This BUFFERING_PROGRESS event occured after stop() call. skip.');
						return;
					}
				
					if (oInitOption.bufferingCallback) {	// oInitOption -> onbufferingstart 처리
						if (typeof oInitOption.bufferingCallback.onbufferingprogress == 'function') {
							oInitOption.bufferingCallback.onbufferingprogress(data);
						}
					}
					break;
				
				case webapis.avplay.BUFFERING_COMPLETE:
					if (oInitOption.bufferingCallback) {	// oInitOption -> onbufferingcomplete 처리
						if (typeof oInitOption.bufferingCallback.onbufferingcomplete == 'function') {
							oInitOption.bufferingCallback.onbufferingcomplete();
						}
					}
					break;
					
				case webapis.avplay.RENDERING_START:
					if (this.status == webapis.avplay.PLAY_STATE_STOPPED) {
						alert('[AVPlay'+id+'] This RENDERING_START event occured after stop() call. skip.');
						return;
					}
					break;
					
				case webapis.avplay.CURRENT_PLAYBACK_TIME:
					if (this.status != webapis.avplay.PLAY_STATE_STARTED) {
						alert('[AVPlay'+id+'] This CURRENT_PLAYBACK_TIME event is not occured on "PLAY_STATE_STARTED". skip..');
						return;
					}
					
					iCurrentPlayTime = new PlayTime(data);
					
					if (!bFrontPanelLock && bBDPlayer) {
						iFrontPanel.setTime(iCurrentPlayTime);
					}
					
					if (oInitOption.playCallback) {	// oInitOption -> oncurrentplaytime 처리
						if (typeof oInitOption.playCallback.oncurrentplaytime == 'function') {
							oInitOption.playCallback.oncurrentplaytime(iCurrentPlayTime);
						}
					}
					break;
				
				case webapis.avplay.RENDERING_COMPLETE:
					setTimeout(function(){ // 바로 stop() 호출시 마지막 CURRENT_PLAYBACK_TIME 이 화면에 반영되지 않음.
						self.stop();
						if (oInitOption.playCallback) {	// oInitOption -> onstreamcompleted 처리
							if (typeof oInitOption.playCallback.onstreamcompleted == 'function') {
								oInitOption.playCallback.onstreamcompleted();
							}
						}
					}, 0);
					break;
				
				case webapis.avplay.CONNECTION_FAILED:
				case webapis.avplay.STREAM_NOT_FOUND:
					this.stop();
					if (typeof oInitOption.playCallback.onerror == 'function') {
						oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR));
					}
					break;
				
				case webapis.avplay.AUTHENTICATION_FAILED:
					this.stop();
					if (typeof oInitOption.playCallback.onerror == 'function') {
						oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR));
					}
					break;
				
				case webapis.avplay.NETWORK_DISCONNECTED:
					this.stop();
					if (typeof oInitOption.playCallback.onerror == 'function') {
						oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NETWORK_ERR));
					}
					break;
				
				case webapis.avplay.NETWORK_SLOW:
					this.stop();
					if (typeof oInitOption.playCallback.onerror == 'function') {
						oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('NetworkSlowError'));
					}
					break;
				
				case webapis.avplay.RENDER_ERROR:
					this.stop();
					if (oInitOption.playCallback) {
						if (typeof oInitOption.playCallback.onerror == 'function') {
							switch (Number(data)) {
								case webapis.avplay.UNKNOWN_ERROR:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('UnknownError'));
									break;
								case webapis.avplay.UNSUPPORTED_CONTAINER:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('AvplayUnsupportedContainerError'));
									break;
								case webapis.avplay.UNSUPPORTED_VIDEO_CODEC:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('AvplayUnsupportedVideoFormatError'));
									break;
								case webapis.avplay.UNSUPPORTED_AUDIO_CODEC:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('AvplayUnsupportedAudioFormatError'));
									break;
								case webapis.avplay.UNSUPPORTED_VIDEO_RESOLUTION:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('AvplayUnsupportedVideoResolutionError'));
									break;
								case webapis.avplay.UNSUPPORTED_VIDEO_FRAMERATE:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('AvplayUnsupportedVideoFramerateError'));
									break;
								case webapis.avplay.CURRUPTED_STREAM:
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('AvplayCurruptedStreamError'));
									break;
								default:
									alert('[AVPlay'+id+'] !ERROR! No detail category..');
									oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException_byType('UnknownError'));
									break;
							}
						}
					}
					break;
					
				case webapis.avplay.CUSTOM_ERROR:
					this.stop();
					if (typeof oInitOption.playCallback.onerror == 'function') {
						oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException('CustomError',data));
					}
					break;
					
				case webapis.avplay.RTSP_STATE:
					//this.stop();
					if (typeof oInitOption.playCallback.onerror == 'function') {
						oInitOption.playCallback.onerror(ErrorsHelper.createWebAPIException('RTSPState',data));
					}
					break;		
				
				case webapis.avplay.AD_START:
				case webapis.avplay.AD_END:
					break;
					
				case webapis.avplay.RESOLUTION_CHANGED:
					if (oInitOption.playCallback) {	// oInitOption -> onresolutionchanged 처리
						if (typeof oInitOption.playCallback.onresolutionchanged == 'function') {
							var resolution = this.getVideoResolution().split('|');	// '1280|720'
							oInitOption.playCallback.onresolutionchanged(resolution[0], resolution[1]);
						}
					}
					break;
					
				case webapis.avplay.BITRATE_CHANGED:
				case webapis.avplay.CUSTOM:
					break;
					
				case webapis.avplay.SUBTITLE:
					if (bBlockSubtitleEvent) {
						alert('[AVPlay'+id+'] stopSubtitle() was called. skip this event.');
						return;
					}
					else {
						if (typeof cbOnSubtitle == 'function') {
							cbOnSubtitle(nSubtitleSyncTime, data, data2);
						}
					}
					break;
					
				default:
					break; 
			}
		}
		
		this.getFitDisplayArea = function (width, height) {	// displayRect에 fit되는 영역 구하기
			var FRAME_LEFT = iDisplayRect.left,
				FRAME_TOP = iDisplayRect.top,
				FRAME_WIDTH = iDisplayRect.width,
				FRAME_HEIGHT = iDisplayRect.height,
				nLeft, 
				nTop, 
				nWidth, 
				nHeight,
				retValue,
				fnRound = Math.round;
			
			if (width / height > FRAME_WIDTH / FRAME_HEIGHT) {	// frame 의 가로/세로 비율보다 크다 -> 가로가 길다 -> 가로를 frame width 로 고정하고 세로를 줄인다.
				nHeight = fnRound((FRAME_WIDTH * height) / width);	// width : height = FRAME_WIDTH : x
				nWidth = FRAME_WIDTH;		// FRAME_WIDTH 로 고정
			}
			else {		// 세로가 길다 -> 세로를 frame height로 고정하고 가로를 줄인다.
				 nWidth = fnRound ((FRAME_HEIGHT * width) / height);	// width : height = x : FRAME_HEIGHT
				 nHeight = FRAME_HEIGHT;
			}
			
	        nLeft = FRAME_LEFT + fnRound((FRAME_WIDTH - nWidth) / 2);
	        nTop = FRAME_TOP + fnRound((FRAME_HEIGHT - nHeight) / 2);
			
			retValue = new SRect(nLeft, nTop, nWidth, nHeight);
			alert('[AVPlay'+id+'] getFitDisplayArea('+width+','+height+') returns ' + retValue);
			return retValue;
		}
		
		// ScreenSaver
		function setScreenSaver (on, second) {
			alert('[AVPlay'+id+'] setScreenSaver('+on+','+(second?second:'')+')');
			
			if (on) {
				if (!second) {
					second = getAutoProtectionTime();
				}
				webapis._plugin('NNavi', 'SendEventToDevice', 3, second);	// 3: EVENT_TO_DEVICE_SCREEN_SAVER_ON
			}
			else {	// off
				webapis._plugin('NNavi', 'SendEventToDevice', 4, 0);	// 4: EVENT_TO_DEVICE_SCREEN_SAVER_OFF, 0: Param이 두개여야 동작함
			}
			
			function getAutoProtectionTime() {
    			var second = 0;
    			var profile = webapis._plugin('TVMW', 'GetProfile', webapis._pluginDef.PL_PRFID_AUTO_PROTECTION_TIME);

                var duration_min = null;
                var fimwareVer = webapis._plugin('NNavi', 'GetFirmware');
                if(typeof fimwareVer === 'string' && fimwareVer >= 'T-INFOLINK2013') {   // 2013 or later
                    duration_min = {
                        0: 5, //PROFILE_DURATION_5MIN
                        1: 10, //PROFILE_DURATION_10MIN
                        2: 20, //PROFILE_DURATION_20MIN
                        3: 30, //PROFILE_DURATION_30MIN
                        4: 40, //PROFILE_DURATION_40MIN
                        5: 60, //PROFILE_DURATION_1HOUR
                        6: 120, //PROFILE_DURATION_2HOUR
                        7: 240, //PROFILE_DURATION_4HOUR
                        8: 480, //PROFILE_DURATION_8HOUR
                        9: 600, //PROFILE_DURATION_10HOUR
                        10: -1, //PROFILE_DURATION_ALWAYS
                    };
                }
                else {
                    duration_min = {
                        0: 10, //PROFILE_DURATION_10MIN
                        1: 20, //PROFILE_DURATION_20MIN
                        2: 40, //PROFILE_DURATION_40MIN
                        3: 60, //PROFILE_DURATION_1HOUR
                        4: 120, //PROFILE_DURATION_2HOUR
                        5: 240, //PROFILE_DURATION_4HOUR
                        6: 480, //PROFILE_DURATION_8HOUR
                        7: 600, //PROFILE_DURATION_10HOUR
                        8: -1, //PROFILE_DURATION_ALWAYS
                    };
                }

    			if ( duration_min && duration_min.hasOwnProperty(parseInt(profile)) ) {
                    if(duration_min[parseInt(profile)] > 0) {
                       second = duration_min[parseInt(profile)] * 60;
                    }
                    else {
                        // Always
                        return;
                    }
                }
                else {
                    second = 60 * 60;   // default 1 hour
   				}
    			return second;
   			}
		}
		
		// BD Player Front Panel
		function FrontPanel () {
			
			this.Enum = {
				FRONT_DISPLAY_PLAY: 100,
				FRONT_DISPLAY_STOP: 101,
				FRONT_DISPLAY_PAUSE: 102,
				FRONT_DISPLAY_ONLINE: 200
			}
			
			this.setState = function (state) {
				alert('[FrontPanel] setState('+state+')');
				webapis._plugin('FrontPanel', 'DisplayVFD_Show', state); // [111127] App 에서는 VFD 조정하지 않기로 함 -> Plugin에서 11, 12 다르게 동작하기로 함.
			}
			this.setTime = function (playTime) {
				alert('[FrontPanel] setTime('+playTime+')');
				var aTime = playTime.timeString.split(':');
				webapis._plugin('FrontPanel', 'DisplayVFD_Time', aTime[0], aTime[1], aTime[2]); // [111127] App 에서는 VFD 조정하지 않기로 함 -> Plugin에서 11, 12 다르게 동작하기로 함.
			}
		}
	}
	
	function _PlayerEventListener (id, avPlay) {
		var sName = 'AVPlay' + id;	// Instance name
		this.iAVPlay = avPlay;
		this.setMasterObject = function(avPlay){
			this.iAVPlay = avPlay;
		}
		this.onBufferingStart = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onBufferingStart()');
			this.iAVPlay.onEvent(webapis.avplay.BUFFERING_START);
		}
		this.onBufferingComplete = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onBufferingComplete()');
			this.iAVPlay.onEvent(webapis.avplay.BUFFERING_COMPLETE);
		}
		this.onBufferingProgress = function(percent){
			alert('[AVPlay'+id+'] PlayerEventListener : onBufferingProgress('+percent+')');
			this.iAVPlay.onEvent(webapis.avplay.BUFFERING_PROGRESS, percent);
		}
		this.onStreamInfoReady = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onStreamInfoReady()');
			this.iAVPlay.onEvent(webapis.avplay.STREAM_INFO_READY);
		}
		this.onCurrentPlayTime = function (ms){
			alert('[AVPlay'+id+'] PlayerEventListener : onCurrentPlayTime('+ms+')');
			this.iAVPlay.onEvent(webapis.avplay.CURRENT_PLAYBACK_TIME, ms);
		}
		//this.onRenderingStart = function(){	// EMP only
		//	alert('[AVPlay'+id+'] PlayerEventListener : onRenderingStart()');
		//	this.iAVPlay.onEvent(webapis.avplay.RENDERING_START);
		//}
		this.onRenderingComplete = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onRenderingComplete()');
			this.iAVPlay.onEvent(webapis.avplay.RENDERING_COMPLETE);
		}
		this.onConnectionFailed = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onConnectionFailed()');
			this.iAVPlay.onEvent(webapis.avplay.CONNECTION_FAILED);
		}
		this.onAuthenticationFailed = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onAuthenticationFailed()');
			this.iAVPlay.onEvent(webapis.avplay.AUTHENTICATION_FAILED);
		}
		this.onStreamNotFound = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onStreamNotFound()');
			this.iAVPlay.onEvent(webapis.avplay.STREAM_NOT_FOUND);
		}
		this.onNetworkDisconnected = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onNetworkDisconnected()');
			this.iAVPlay.onEvent(webapis.avplay.NETWORK_DISCONNECTED);
		}
		this.onRenderError = function(errorCode){
			alert('[AVPlay'+id+'] PlayerEventListener : onRenderError(' + errorCode + ')');
			this.iAVPlay.onEvent(webapis.avplay.RENDER_ERROR, errorCode);
		}
		this.onAdStart = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onAdStart()');
			this.iAVPlay.onEvent(webapis.avplay.AD_START);
		}
		this.onAdEnd = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onAdEnd()');
			this.iAVPlay.onEvent(webapis.avplay.AD_END);
		}
		this.onResolutionChanged = function(){
			alert('[AVPlay'+id+'] PlayerEventListener : onResolutionChanged()');
			this.iAVPlay.onEvent(webapis.avplay.RESOLUTION_CHANGED);
		}
		//this.onBitrateChanged = function(){	// EMP only
		//	alert('[AVPlay'+id+'] PlayerEventListener : onBitrateChanged()');
		//	this.iAVPlay.onEvent(webapis.avplay.BITRATE_CHANGED);
		//}
		//this.onSubtitle = function(param1){	// EMP only
		//    alert('[AVPlay'+id+'] PlayerEventListener : onSubtitle()');
		//	this.iAVPlay.onEvent(webapis.avplay.SUBTITLE, param1);
		//}
		this.onEvent = function(type, param, param2){
			alert('[AVPlay'+id+'] PlayerEventListener : onEvent('+type+','+param+','+param2+') -> ' + this.iAVPlay.Event2String[type]);
			this.iAVPlay.onEvent(type, param, param2);
		}
	}
	
	/*
	 * Object: AvaiableAVPlaySuccessCallback
	 * 	Success callback for retrieving an avplay object.
	 *
	 * Properties:
	 *	onsuccess			- (Function) Method involked when the available avplay object is retrieved successfully. [[parameters]]
	 */
	
	/*
	 * Object: AVPlayInitialOption
	 * 	Specifies the options for the initialize avplay.
	 *
	 * Properties:
	 *	containerID			- (String) The container <DIV> ID. Plugin <OBJECT> will be inserted to the <DIV> by .innerHTML.
	 *	zIndex				- (Number) The z-index property to be set to "containerID" <DIV>.
	 *	bufferingCallback	- (BufferingCallback)
	 *	playCallback		- (AVPlayCallback) 
	 *  displayRect			- (SRect)
	 *  autoRatio			- (Boolean) Resizes display area automatically.
	 */
	
	/*
	 * Object: AVPlayOption
	 * 	Specifies the options for the contenst playing.
	 *
	 * Properties:
	 *	totalBufferSize		- (Number) This value gives the total number of bytes buffered.
	 *	pendingBufferSize	- (Number) This value gives the number of bytes still pending to get buffered.
	 *	initialBufferSize	- (Number) This value gives the number of initial bytes Buffered.
	 *	adaptive			- (AdaptiveStreamingOption) 
	 *  drm					- (DrmOption)
	 *  macrovision			- (MacrovisionOption)
	 *  subtitle			- (SubtitleOption)
	 */
	
	/*
	 * Object: AdaptiveStreamingOption
	 * 	Specifies the options for adaptive steaming.
	 *
	 * Properties:
	 *	type				- (String) Specifies the type of adaptive streaming.
	 *	startTime			- (String) Specifies the initial playback time for the HLS.
	 *	startBitrate		- (String) Specifies the initial playback bitrate.
	 *	upTimer				- (String) Specifies the initial up switching time between bitrate group.
	 *  bitrates			- (String) Specifies available content bitrates and bitrate group.
	 *  admode				- (String) Specifies the advertizment play mode while seek(jump) operation.
	 */
	
	/*
	 * Object: SubtitleOption
	 * 	Specifies the options for subtitle.
	 *
	 * Properties:
	 *	path				- (String) This value gives the path to the subtitle file associated with the video file that is currently being played.
	 *	streamID			- (Number) This value gives the index of substitles.
	 *	sync				- (Number) This value gives the value that is to be set in order to synchronize the subtitles with the current video being played. This value is given in the order of milli seconds.
	 *	callback			- (SubtiltleCallback) Specifies the callback for the subtitle.
	 */
	
	/*
	 * Object: MacrovisionOption
	 * 	Specifies the options for Macrovision.
	 *
	 * Properties:
	 *	type				- (Number) It specifies macrovision level to protect analog video output during playback. [[enums]]
	 *	ict					- (Number) It specifies ICT output level.
	 *	dot					- (Number) The dot is used to disable analog video output on BD. This value has to be used only for BD and must be not used on TV.
	 *	vbi					- (Number) It specifies VBIData(cgms type) output level.
	 */
	
	/*
	 * Object: DrmOption
	 * 	Specifies the options for DRM(Digital Rights Management).
	 *
	 * Properties:
	 *	type				- (String) Specifies the type of DRM.
	 *	company				- (String) Specifies the name of DRM Company.
	 *	deviceID			- (String) This value gives the specific value that is to be set in order to check the avaialbe device.
	 *	deviceType			- (String) This value gives the specific value that is to be set in order to check the avaialbe type of device.
	 *	streamID			- (String) This value gives the id of stream.
	 *	drmURL				- (String) This value gives the url of DRM server.
	 *	ackURL				- (String) This value gives the url of DRM ack server.
	 *	heartbeatPeriod		- (String) This value gives the period of heartbeat.
	 *	portal				- (String) This value gives the address of portal.
	 *	userData			- (String) This value gives the extra user data.
	 *	cookie				- (String) This value gives the extra cooki data.
	 */
	
	/*
	 * Object: BufferingCallback
	 * 	Specifies the options for buffering callback subscriptions.
	 *
	 * Properties:
	 *	onbufferingstart	- (Function) This is the method that is invoked on successfully starting the Buffering.
	 *	onbufferingprogress	- (Function) This is the method that is invoked when the buffering is in progress. A parameter is sent which the amount of buffering has completed. [[parameters]]
	 *  onbuffercomplete	- (Function) This is the method that is invoked on successfully completing the Buffering.
	 */
	
	/*
	 * Object: AVPlayCallback
	 * 	Specifies the options for playing callback subscriptions.
	 *
	 * Properties:
	 *	oncurrentplaytime	- (Function) This method is invoked when a video is being played. It is called by passing the current play time as the parameter. The current time is in the form of milli seconds. [[parameters]]
	 *	onresolutionchanged	- (Function) This method is invoked when the resolution changes. This method is invoked by passing the new width and height of the video as parameters. [[parameters]]
	 *  onstreamcompleted	- (Function) This method is invoked when the stream that is currently being played gets completed.
	 *  onerror				- (Function) This method is invoked when there is an error playing the video. It is called by passing the WebAPIError as parameter. [[parameters]]
	 */
	
	/*
	 * Object: SubtitileDataCallback
	 * 	Specifies the options for subtitle callback subscriptions.
	 *
	 * Properties:
	 *	onsubtitle	- (Function) This method is called when the subtitle is thrown for the current frame. The dataStr has the subtitle value for the current frame. The user has to print this string on the video. [[parameters]]
	 */	
	
	
	/*
	 * Class: AVPlay_PC Mode
	 */
	function _AVPlay_PC(id) {
		var sName = 'AVPlay' + id;	// Instance name
		var sVersion = '3.0';
		var iInstance = null;
		var oInitOption = null;
		var iDisplayRect = null;
		/*
		 * Function: init
		 * 	initializes avplay with the specified option parameter. This has to be called before all of other avplay functions.
		 *
		 * Parameters:
		 * 	o option	- (AVPlayInitialOption) a option for the initialize avplay
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		
		this.init = function (option) {	// [IDL]
			alert('[AVPlay'+id+'] init('+option+')');
			
			/*
		     option = {
			     containerID: id,	// [init()]
			     zIndex: 0,	// [init()]
			     bufferingCallback: {
				     onbufferingstart : onBufferingStart,
				     onbufferingprogress : onBufferingProgress,
				     onbufferingcomplete : onBufferingComplete
			     },
			     playCallback: {
				     oncurrentplaytime : onCurrentPlayTime,
				     onresolutionchanged : onResolutionChange,
				     onstreamcompleted : onStreamComplete,
				     onerror : onRederingError
			     },
			     displayRect: {	// SRect instance [init()]
				     top: 100,
				     left : 100,
				     width : 200,
				     height : 200
			     },
			     autoRatio: true,	// [STREAM_INFO_READY]
			     frontPanelLock: false // Front panel 표시 여부. MP3 재생시 표시하지 않도록 하기 위함
		     }
		     */
			
	        // Parameter Type Checking
			if (_isType(option, 'object')) {				
				if (
					!_isType(option.containerID, 'undefined|string') ||
					!_isType(option.zIndex, 'undefined|number') ||
					!_isType(option.bufferingCallback, 'undefined|object') ||
					!_isType(option.playCallback, 'undefined|object') ||
					!_isType(option.displayRect, 'undefined|object') ||
					!_isType(option.autoRatio, 'undefined|boolean')
				) {
					throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
				}
				
				if (_isType(option.bufferingCallback, 'object')) {
					if (
						!_isType(option.bufferingCallback.onbufferingstart, 'undefined|function') ||
						!_isType(option.bufferingCallback.onbufferingprogress, 'undefined|function') ||
						!_isType(option.bufferingCallback.onbufferingcomplete, 'undefined|function')
					) {
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
				
				if (_isType(option.playCallback, 'object')) {
					if (
						!_isType(option.playCallback.oncurrentplaytime, 'undefined|function') ||
						!_isType(option.playCallback.onresolutionchanged, 'undefined|function') ||
						!_isType(option.playCallback.onstreamcompleted, 'undefined|function') ||
						!_isType(option.playCallback.onerror, 'undefined|function')
					) {
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
				
				if (_isType(option.displayRect, 'object')) {
					var top = _checkNumberType(option.displayRect.top);
					var left = _checkNumberType(option.displayRect.left);
					var width = _checkNumberType(option.displayRect.width);
					var height = _checkNumberType(option.displayRect.height);
					if (
						!_isType(top, 'number') ||
						!_isType(left, 'number') ||
						!_isType(width, 'number') ||
						!_isType(height, 'number')
					) {
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
			}
			else {
				if (!_isType(option, 'undefined|null')) {
					throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
				}
			}
			// Parameter Type Checking End
			
			
			//var bVideo = document.getElementsByTagName('video')[0];
			//if(!bVideo){
			oInitOption = option || {};
			var videoObj = document.createElement('video');				
			videoObj.id='VideoIns'+nId;
			document.body.appendChild(videoObj);
			
			iInstance = document.getElementById('VideoIns'+nId);
			
			if (oInitOption.displayRect) {
				this.setDisplayRect(oInitOption.displayRect);
			}
			else {
				alert('[AVPlay'+id+'] !WARNNING! > You did Not set displayRect.');
			}
			
			iInstance.addEventListener('timeupdate',function(){
				var curTime = new PlayTime(Math.floor(iInstance.currentTime) * 1000);
				oInitOption.playCallback.oncurrentplaytime(curTime); //ms 단위로 리턴
			},false);
			
			iInstance.addEventListener('ended',function(){
				oInitOption.playCallback.onstreamcompleted();
			},false);
			
			iInstance.addEventListener('loadeddata',function(){
				oInitOption.bufferingCallback.onbufferingcomplete();
			},false);
			
			iInstance.addEventListener('loadstart',function(){
				oInitOption.bufferingCallback.onbufferingstart();
			},false);
			
			iInstance.addEventListener('loadedmetadata',function(){
        		console.log('loadedmetadata~~~~~~~~~~~~~~~~~~~!!');
			},false);
			
			//}
			
			return true;
			
			
		}
		/*
		 * Function: open
		 * 	opens media content with the URL specified by parameter.
		 *
		 * Parameters:
		 *  o url		- (String) URL of the video file
		 *  o option 	- (AVPlayOption)(Optional) a option for opening the specific video contents.
		 *
		 * Returns:
		 * 	True on success and false on failure.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.open = function (url, option) {
			alert('[AVPlay'+id+'] open('+url+','+(option?option:'')+')');	
			iInstance.src = String(url);
			
			return true;
		}
		
		/*
		 * Function: play
		 * 	Starts playback from the second specified by parameter. If the operation is failed due to any other error, the errorCallback is launched with the appropriate error code:
		 *  INVALID_VALUES_ERR: If the input attributes do not contain a valid value
		 *  UNKNOWN_ERR: In any other error case.
		 *
		 * Parameters:
		 * 	o successCallback	- (Function)(Optional) Callback upon Success.
		 * 	o errorCallback		- (Function)(Optional) Callback upon Failure.
		 * 	o sec				- (Function)(Optional) Seconds at which playback has to be started.
		 *
		 * Returns:
		 * 	nothing. Pending(Async) Operation
		 *
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.play = function (successCallback, errorCallback, sec) {	// [IDL]			
			alert('[AVPlay'+id+'] play('+typeof successCallback+','+typeof errorCallback+','+(sec!==undefined?sec:'')+')');
			this._setStatus(webapis.avplay.PLAY_STATE_STARTED);															
			iInstance.play();
			if(typeof successCallback == 'function'){
				successCallback();	
			}
			return true;
		}
		
		/*
		 * Function: stop
		 * 	stops playback of content.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.stop = function () {	// [IDL]
			alert('[AVPlay'+id+'] stop()');
			//document.body.removeChild(iInstance);
			//iInstance = null;
			this._setStatus(webapis.avplay.PLAY_STATE_STOPPED);
			iInstance.pause();
			
			return true;
		}
		
		/*
		 * Function: pause
		 * 	pauses the current playback.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.pause = function () {	// [IDL]
			alert('[AVPlay'+id+'] pause()');
			
			this._setStatus(webapis.avplay.PLAY_STATE_PAUSED);
			iInstance.pause();
			
			return true;
			
		}
		
		/*
		 * Function: resume
		 * 	resumes the playback of paused content.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.resume = function () {	// [IDL]
			alert('[AVPlay'+id+'] resume()');
			this._setStatus(webapis.avplay.PLAY_STATE_STARTED);
			iInstance.play();
			
			return true;
		}
		
		/*
		 * Function: jumpForward
		 * 	jumps forward from the current playback point for amount of second specified by parameter.
		 *
		 * Parameters:
		 * 	o sec	- (Number) relative time offset from current time in second. 
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.jumpForward = function (sec) {	// [IDL]
			alert('[AVPlay'+id+'] jumpForward('+sec+')');
			var isec = _checkNumberType(sec);
	        // Parameter Type Checking
	        
	        if(isec < 0) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
	        
	        console.log('jumpForward.....................!');
	        var curTime = iInstance.currentTime;
	        var seekTime = curTime + sec; 
	        if(seekTime >= iInstance.duration){
	        	seekTime = iInstance.duration; 	
	        } 
	        iInstance.currentTime = seekTime;
	        var retValue = 1;
	        
			alert('[AVPlay'+id+'] jumpForward('+isec+') returns ' + (retValue == 1));
			return true;
		}
		
		/*
		 * Function: jumpBackward
		 * 	Jumps backward from the current playback point for the amount of second specified by parameter.
		 *
		 * Parameters:
		 * 	o sec	- (Number) relative time offset from current time in second.
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.jumpBackward = function (sec) {	// [IDL]
			alert('[AVPlay'+id+'] jumpBackward('+sec+')');
			var isec = _checkNumberType(sec);
	        // Parameter Type Checking
	        if(isec < 0) {	        	
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
	        // Parameter Type Checking End
			console.log('jumpBackward.....................!');
        	var curTime = iInstance.currentTime;
        	var seekTime = curTime - sec; 
        	if(seekTime <= 0){
        		seekTime = 0; 	
        	} 
        	iInstance.currentTime = seekTime;
        	var retValue = 1;
        	
			alert('[AVPlay'+id+'] jumpBackward('+isec+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		
		/*
		 * Function: setSpeed
		 * 	sets the playback speed of currently playing content.
		 *
		 * Parameters:
		 * 	o speed	- (Number) specifies the playback speed in multiple of 2. This can be negative integer for backward playback.
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setSpeed = function (speed) {	// [IDL] // for contents which use Widevine or Has
			alert('[AVPlay'+id+'] setSpeed('+speed+')');
			var ispeed = _checkNumberType(speed);
			console.log('setSpeed............');
			iInstance.playbackRate = speed;
			var retValue = 1;
			
			alert('[AVPlay'+id+'] setSpeed('+ispeed+') returns ' + (retValue == 1));
			return retValue == 1;
		}	
		
		/*
		 * Function: setDisplayRect
		 * 	sets the display area of video content in TV screen
		 *
		 * Parameters:
		 * 	o rect - (SRect) the rectagle postion for the display area
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure.
		 *
		 * Throws:
	     *  o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value.
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type.
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setDisplayRect = function (rect) {
			alert('[AVPlay'+id+'] setDisplayRect('+rect+')');
			
	        // Parameter Type Checking
	        if (
	            !_isType(rect, 'object') ||
	            !_isType(rect.top, 'number') ||
	            !_isType(rect.left, 'number') ||
	            !_isType(rect.width, 'number') ||
	            !_isType(rect.height, 'number')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			iDisplayRect = rect;
			iInstance.style.position = 'absolute';
			iInstance.style.left = iDisplayRect.left + 'px';
			iInstance.style.top = iDisplayRect.top + 'px';
			iInstance.style.width = iDisplayRect.width + 'px';
			iInstance.style.height = iDisplayRect.height + 'px';
		}
		
		this.setDisplayArea = function () {
		}
		
		this.getDuration = function () {			
			var retValue = Math.round(iInstance.duration) * 1000; //ms 단위로 리턴
			
			alert('[AVPlay'+id+'] getDuration() returns ' + retValue);
			
			this.__defineGetter__('duration', function(){return retValue;}); // [Instance properties]
			return retValue;
		}
		
		this._setStatus = function (status) {
			alert('[AVPlay'+id+'] _setStatus('+status+') -> ' + this.State2String[status]);
			this.__defineGetter__('status', function(){return status;}); // [Instance properties]
		}
		

	    this.getZIndex = function(){
			var zindex = iInstance.style.zIndex;
			return zindex
	    }
	    
	    this.setZIndex = function(zIndex){
			iInstance.style.zIndex = zIndex;
	    }
	    
	     this.show = function(){
			// [111220] visibility 속성으로 조절하도록 함
			iInstance.style.visibility = 'visible';
	    }
	    
	    this.hide = function(){
			iInstance.style.visibility = 'hidden';
	    }
		
	    this.getVideoResolution = function () {
	    	return (iInstance.videoHeight || 1280) + '|' + (iInstance.videoHeight || 720);
	    }
	    this.getFitDisplayArea = function (width, height) {
	    	var FRAME_LEFT = iDisplayRect.left,
			FRAME_TOP = iDisplayRect.top,
			FRAME_WIDTH = iDisplayRect.width,
			FRAME_HEIGHT = iDisplayRect.height,
			nLeft, 
			nTop, 
			nWidth, 
			nHeight,
			retValue,
			fnRound = Math.round;
		
			if (width / height > FRAME_WIDTH / FRAME_HEIGHT) {	// frame 의 가로/세로 비율보다 크다 -> 가로가 길다 -> 가로를 frame width 로 고정하고 세로를 줄인다.
				nHeight = fnRound((FRAME_WIDTH * height) / width);	// width : height = FRAME_WIDTH : x
				nWidth = FRAME_WIDTH;		// FRAME_WIDTH 로 고정
			}
			else {		// 세로가 길다 -> 세로를 frame height로 고정하고 가로를 줄인다.
				 nWidth = fnRound ((FRAME_HEIGHT * width) / height);	// width : height = x : FRAME_HEIGHT
				 nHeight = FRAME_HEIGHT;
			}
			
	        nLeft = FRAME_LEFT + fnRound((FRAME_WIDTH - nWidth) / 2);
	        nTop = FRAME_TOP + fnRound((FRAME_HEIGHT - nHeight) / 2);
			
			retValue = new SRect(nLeft, nTop, nWidth, nHeight);
			alert('[AVPlay'+id+'] getFitDisplayArea('+width+','+height+') returns ' + retValue);
			return retValue;
	    }
		
		this.State2String = {};
		this.State2String[webapis.avplay.PLAY_STATE_IDLE] = 'PLAY_STATE_IDLE';
		this.State2String[webapis.avplay.PLAY_STATE_INITIALIZED] = 'PLAY_STATE_INITIALIZED';
		this.State2String[webapis.avplay.PLAY_STATE_STOPPED] = 'PLAY_STATE_STOPPED';
		this.State2String[webapis.avplay.PLAY_STATE_PREPARED] = 'PLAY_STATE_PREPARED';
		this.State2String[webapis.avplay.PLAY_STATE_STARTED] = 'PLAY_STATE_STARTED';
		this.State2String[webapis.avplay.PLAY_STATE_PAUSED] = 'PLAY_STATE_PAUSED';
		
	} //_avPlay_PC End !!

})(); // End webapis.avplay




/*
 * Namespace: imageview
 * 	This interface exposes APIs to display image.
 * 
 * Available From:
 * 	0.1
 */
;
(function(){ // Start webapis.imageview
	
	webapis.imageview = {
		// ImageViewer Plugin Effect Enum
		EFFECT_INIT: -1,
		EFFECT_FADE1: 0,
		EFFECT_FADE_1: 0,
		EFFECT_FADE2: 1,
		EFFECT_FADE_2: 1,
		EFFECT_BLIND: 2,
		EFFECT_SPIRAL: 3,
		EFFECT_CHECKER: 4,
		EFFECT_LINEAR: 5,
		EFFECT_STAIR: 6,
		EFFECT_STAIRS: 6,
		EFFECT_WIPE: 7,
		EFFECT_RANDOM: 8,
		EFFECT_NONE: 9,
		EFFECT_NORMAL: 9,
		
		// ImageViewer Plugin Event Enum
		CONNECTION_FAILED: 1,
		AUTHENTICATION_FAILED: 2,
		STREAM_NOT_FOUND: 3,
		NETWORK_DISCONNECTED: 4,
		NETWORK_SLOW: 5,
		RENDER_ERROR: 6,
		RENDERING_START: 7,	//Movie and Music only
		RENDERING_COMPLETE: 8,
		STREAM_INFO_READY: 9,
		DECODING_COMPLETE: 10,	//Picture only
		BUFFERING_START: 11,
		BUFFERING_COMPLETE: 12,
		BUFFERING_PROGRESS: 13,
		CURRENT_DISPLAY_TIME: 14,
		AD_START: 15,
		AD_END: 16,
		RESOLUTION_CHANGED: 17,
		BITRATE_CHANGED: 18,
		SUBTITLE: 19,
		CUSTOM: 100,
		
		// ImageView State Enum
		IMAGEVIEW_STATE_IDLE: 0,
		IMAGEVIEW_STATE_INITIALIZED: 1,
		IMAGEVIEW_STATE_PREPARED: 3,
		IMAGEVIEW_STATE_DRAWN: 4,
		IMAGEVIEW_STATE_STOPPED: 5,
		
		/*
		 * Function: getImageView
		 *  Returns ImageView instance.
		 *
		 * Parameters:
		 * 	o successCallback	- (Function) 
		 *
		 * Returns:
		 * 	nothing. Pending(Async) Operation
		 *
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		getImageView: function (successCallback, errorCallback) {
			// Parameter Type Checking
	        if (
	            !_isType(successCallback, 'function') ||
	            !_isType(errorCallback, 'undefined|function|null')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
			if (!bInit) {
				init();
			}
			
			var iImageView = null;
			if (nDevice == DEVICE_BROADCOM2010) {
				if (nId == 0) {
					iImageView = new _ImageView_Broadcom2010(nId);
				}
				else {
					// Broadcom 용 imageviewer <object>가 두 번 삽입되면 세트 die..
					alert('[ImageView] 2010 Broadcom can use only one ImageView instance.');
					if (typeof errorCallback == 'function') {
						errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
					}
					return;
				}
			}
			else if (nDevice == DEVICE_2010 || nDevice == DEVICE_2011) { // Valencia, Genoa
				if (nId == 0) {
					iImageView = new _ImageView(nId);
				}
				else {
					/*
					 * # Valencia 
					 *  - <object>를 여러개 삽입하면 하나의 ImageViewer에서 이벤트 발생시 모든 <object>에 이벤트가 전달되는 문제가 있음.
					 * # Genoa [120611]
					 *  - Family Story 에서 슬라이드쇼시 죽는 문제 있음.
					 *  - 죽는 위치는 UniPlayer 이나, 원인을 찾지는 못했음.(일정 문제, OTN 불가) 
					 *  - 회피책: Genoa 에서 멀티 <object> 불가능하도록 수정.
					 */
					alert('[ImageView] 2010, 2011 device can use only one ImageView instance.');
					if (typeof errorCallback == 'function') {
						errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
					}
					return;
				}
			}
			else {
				iImageView = new _ImageView(nId);
			}
			
			alert('[ImageView] getImageView() succeeded. -> ' + nId + 'th ' + iImageView);
			
			aImageViewInstance[nId] = iImageView;
			nId += 1;
			successCallback(iImageView);	// 생성된 instance를 전달하기로 함(박일우K)
		},
		
		_getAllInstance: function () {
			return aImageViewInstance;
		},
		_destroyAll: function () {
			alert('[ImageView] _destroyAll() with ' + aImageViewInstance.length + ' instances.');
			for (var i = 0; i < aImageViewInstance.length; i++) {
				alert('[ImageView] Destroy ' + i + 'th imageview.');
				aImageViewInstance[i].destroy();
			}
		},
		/*
		_deactivateAll: function () {
			var nPlayerNum = webapis._plugin('PlayerManager', 'GetTotalNumOfPlayer');
			for(var i=0 ; i< nPlayerNum ; i++)
			{
				var tPlayerID = webapis._plugin('PlayerManager', 'GetPlayerID', i);
				alert('[ImageView] GetPlayerID : ' + tPlayerID);
				if(webapis._plugin('PlayerManager', 'GetActivationState', tPlayerID) == 0)
				{
					alert('[ImageView] DeactivatePlayer : ' + tPlayerID);
					webapis._plugin('PlayerManager', 'DeactivatePlayer', tPlayerID);
				}
				else
				{
					alert('[ImageView] Skip DeactivatePlayer : ' + tPlayerID);
				}
			}
		}
		*/
	}
	
	var bInit = false,
		aBroadcom2010ModelCode = [
			'BD-C5500_US',
			'BD-C6500_US',
			'Broadcom_BD_2010_3D'
		],
		
		DEVICE_BROADCOM2010 = 1,
		DEVICE_2010 = 2,
		DEVICE_2011 = 3,
		DEVICE_2012 = 4,
		
		nDevice = DEVICE_2012,
		nId = 0,
		aImageViewInstance = [],
		
		bSupportMultiApplication = false;
	
	function init(){
		var sModelCode = webapis.tv.info.getModel(),
			sServiceConfig = webapis._plugin('NNavi', 'GetServiceConfig'),
			oServiceConfig = null;
	//		sModelCode = 'BD-C5500_US'; // for test
		
		if (aBroadcom2010ModelCode.join(',').indexOf(sModelCode) != -1) {
			nDevice = DEVICE_BROADCOM2010;
			alert('[ImageView] DEVICE_BROADCOM2010');
		}
		else if (navigator.userAgent.toLowerCase().indexOf('applewebkit')>=0 && window.curWidget) {
			nDevice = DEVICE_2012;
			alert('[ImageView] DEVICE_2012');
		}
		else {
			var browserVersion = navigator.appVersion.toLowerCase();
			if (parseInt(browserVersion.split('(')[0], 10) < 6.0) {
				nDevice = DEVICE_2010;
				alert('[ImageView] DEVICE_2010');
			}
			else {
				nDevice = DEVICE_2011;
				alert('[ImageView] DEVICE_2011');
			}
		}
		
		alert('[ImageView] sServiceConfig: ' + sServiceConfig);
		
		if (sServiceConfig) {
			try {
				oServiceConfig = eval('(' + sServiceConfig + ')');
			}
			catch(e) {
				alert('[ImageView] ' + e);
			}
		}
		
		bSupportMultiApplication = false;
		var notSupportMultiTasking = webapis._plugin('TaskManager', 'CheckSupportMultiTaskingApp', 0);
		var supportValue = webapis._plugin('TaskManager', 'CheckSupportMultiTaskingApp', 2);
		alert('notSupportMultiTasking......................'+notSupportMultiTasking);
		alert('supportValue......................'+supportValue);
		if (notSupportMultiTasking == 0 && supportValue == 1) {
			bSupportMultiApplication = true;
		}
		
		alert('[ImageView] bSupportMultiApplication: ' + bSupportMultiApplication);
			
		window.onHide = webapis.imageview._destroyAll;
		
		/*
		if (bSupportMultiApplication) {
			// [120731] Multi application 지원시 onHide 에서 stop하지 않고 deactivate 처리하도록 함
			window.onHide = webapis.imageview._deactivateAll;
		}
		else {
			// onHide 시 모든 ImageView instance의 destroy() 가 호출되도록 함
			window.onHide = webapis.imageview._destroyAll;
		}
		*/
		bInit = true;
	}
	
	/*
	 * SlideShow를 하려면 SetEffect(INIT)이 슬라이드쇼 시작과 끝에 호출되야 함.
	 * 반드시 pair 로 불려야 함. (UniPlayer의 상태가 변경됨.)
	 * ImageView instance는 여러개가 생성될 수 있으므로 webapis.imageview 에서 관리하도록 함.
	 * ImageView instance는 InitPlayer() 직후 webapis.imageview._bCallSetEffectWithINIT 을 확인하여 SetEffect(INIT)을 호출.
	 */
	webapis.imageview._bSlideShowMode = false;
	webapis.imageview._bCallSetEffectWithINIT = false;
	webapis.imageview._setSlideShow = function (bSlideShowMode, iImageView) {
		/*
		 * 현재 SlideShow 상태 -> 신규 SlideShow 상태
		 * true -> true   : DO NOTHING
		 * true -> false  : 즉시 SetEffect(INIT) 호출
		 * false -> true  : bCallSetEffectWithINIT -> true 로 설정하여 InitPlayer 직후 SetEffect(INIT) 가 호출되도록 함 
		 * false -> false : DO NOTHING
		 */
		if (webapis.imageview._bSlideShowMode && !bSlideShowMode) { // true -> false
			webapis.imageview._bCallSetEffectWithINIT = false;
			/*
			 * Media Instance가 생성안되서 SetEffect가 실패했을 경우,
			 * 임시 Instance 생성 후 SetEffect 실행. 
			 */
			if (!iImageView.setTransitionEffect(webapis.imageview.EFFECT_INIT)) {
				// 아래와 같이 SetEffect(-1)을 호출하기 위해 임시 instance를 생성하고 있으나, 정상적인 방법이 아님(김영현D) 
				iImageView.initPlayer('http://for_create_media_instance/');
	            iImageView.setTransitionEffect(webapis.imageview.EFFECT_INIT);
				iImageView.stop();
			}
			webapis.imageview._bSlideShowMode = false;
		}
		else if (!(webapis.imageview._bSlideShowMode) && bSlideShowMode) { // false -> true
			webapis.imageview._bCallSetEffectWithINIT = true; // initPlayer()에서 SetEffect(INIT) 호출 후 false로 바뀜
			webapis.imageview._bSlideShowMode = true;
		}
	}
		
	/*
	 * Class: ImageView
	 *  The ImageView is a common module that enables JPEG files to be played in SAMSUNG DTV. With this module, Images can be displayed on DTV big screen with high resolution,1920x1080 pixels. In addition, images are marked by scaling the size you need at the location you prefer, and transition effect in various manners is available.
	 */
	function _ImageView(id) {
		var sName = 'ImageView' + id;	// Instance name
		var sVersion = '3.0';
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// imageview properties
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		/*
		 * Property: id
		 *  This string stores the id of the ImageView instance.
		 *  
		 * Property: url
		 *  This string stores the url of the image that is currently being played.
		 *  
		 * Property: imageWidth
		 *  This string stores the width of the image that is currently being played. The image width is stored in the form of no of pixels and it is always less than the width of the DTV.
		 *  
		 * Property: imageHeight
		 *  This string stores the height of the image that is currently being played. The image height is stored in the form of no of pixels and it is always less than the height of the DTV.
		 *  
		 * Property: displayRect
		 *  This string stores the display area of the image that is currently being played. The displayArea stores four values. The first two values represent the x and y co-ordinate of the top left corner of the video and the next two parameters give us the width and height of the video.
		 *  
		 * Property: status
		 *  This value stores the current status of the player. Available states are : [[enums]]
		 */
		
		this.__defineGetter__('id', function(){return id;});			// (Number) unique id, ImageViewFactory generates this. [create instance]
		this.__defineGetter__('url', function(){return null;});			// (String) source url. [prepare()]
		this.__defineGetter__('imageWidth', function(){return null;});	// (Number) image width. [prepare()]
		this.__defineGetter__('imageHeight', function(){return null;});	// (Number) image height. [prepare()]
		this.__defineGetter__('displayRect', function(){return null;});	// (SRect) the position of player object area. [setDisplayRect()]
		this.__defineGetter__('displayArea', function(){return null;});	// (SRect) the position of displayed screen area. [setDisplayArea()]
		this.__defineGetter__('containerID', function(){return null;});	// (String) the container div id of <object> tag.
		this.__defineGetter__('zIndex', function(){return null;});		// (Number) the z-index of the ImageView. If an user specifies the container div, "zIndex" would be set to <object>. If not, "zIndex" would be set to default container div created by ImageView.
		this.__defineGetter__('effect', function(){return null;});		// (Number) image effect mode
		this.__defineGetter__('status', function(){return webapis.imageview.IMAGEVIEW_STATE_IDLE;});	// (Number) imageview status. [Many points]
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// IDL 에 있는 함수들
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		/*
		 * Function: init
		 * 	This method initializes the imageview with the the ImageViewOption.
		 *
		 * Parameters:
		 * 	o option	- (Object) a option for the initialize imageview
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure. 
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.init = function (option) {	// [IDL]			
			alert('[ImageView'+id+'] init('+option+')');
			
			option = option || {};
			
			/*
		     option = {
		     	containerID: 'divID',
		     	zIndex: 10,
		     	displayRect: {
	                 top: 100,
	                 left : 100,
	                 width : 200,
	                 height : 200
	            }
		     }
		     */			
	        // Parameter Type Checking
	        if (_isType(option, 'object')) {	        	
		        if (
		            !_isType(option.containerID, 'undefined|string') ||
		            !_isType(option.zIndex, 'undefined|number') ||
		            !_isType(option.displayRect, 'undefined|object')
		        ) {		        	
		            throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
		        }
				if (_isType(option.displayRect, 'object')) {
					var top = _checkNumberType(option.displayRect.top);
					var left = _checkNumberType(option.displayRect.left);
					var width = _checkNumberType(option.displayRect.width);
					var height = _checkNumberType(option.displayRect.height);					
					if (
						!_isType(top, 'number') ||
						!_isType(left, 'number') ||
						!_isType(width, 'number') ||
						!_isType(height, 'number')
					) {						
						throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
					}
				}
			} else {
				if (!_isType(option, 'undefined|null')) {
					throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
				}
			}
	        // Parameter Type Checking End			
			oInitOption = option || {};   // 기존 option 유지 안함.			
			initializeEnv();		
			
			/*
			 * <object> 삽입
			 * - pluginObjectID는 강제 설정.
			 * - init이 처음 불리는데 containerID를 지정 안했을 경우, body에 삽입함.
			 */
			if (!bInitialize) {				
				initialize(oInitOption.containerID || null, oInitOption.zIndex || null, IMAGEVIEWER_OBJECT_ID + id);
			}
			else if (oInitOption.containerID) {
				this.setImageViewerPluginObject(oInitOption.containerID, oInitOption.zIndex || null, IMAGEVIEWER_OBJECT_ID + id);
			}
			
			/*
			 * displayRect 설정
			 */
			if (oInitOption.displayRect) {
				this.setDisplayRect(oInitOption.displayRect);
			}
			else {
				alert('[ImageView'+id+'] !WARNNING! > You did Not set displayRect.');
			}
			
			if (oInitOption.autoRatio !== undefined) {	// Valencia, Echo 용. Genoa 에서는 지원 안함.
				bAutoRatio = oInitOption.autoRatio;
			}
			
			this._setStatus(webapis.imageview.IMAGEVIEW_STATE_INITIALIZED);
			
			return true;
		}
		/*
		 * Function: prepare
		 * 	This method opens the image with the url of the image file that is supposed to be displayed and the ImageOption.
		 *
		 * Parameters:
		 * 	o successCallback	- (Function) Callback upon Success.
		 * 	o errorCallback		- (Function) Callback upon Failure.
		 * 	o url				- (String) URL of the image file
		 * 	o option			- (Object) a option for opening the specific image contents.
		 * (start code)
		 * {
		 *     width: 1920,	// (Number)
		 *     height: 1080	// (Number) 
		 *     effect: webapis.imageview.EFFECT_FADE_1
		 * }
		 * (end)
		 * 
		 * Returns:
		 * 	nothing.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.prepare = function (url, successCallback, errorCallback, option) {
			alert('[ImageView'+id+'] prepare('+typeof successCallback+','+typeof errorCallback+','+url+','+option+')');
			
			/*
	         * successCallback: DECODING_COMPLETE 에서 호출됨.
	         * errorCallback: initPlayer() ~ DECODING_COMPLETE 사이 발생한 에러 상황에서 호출됨.
	         * option: {
	         *   width: 500,
	         *   height: 400,
	         *   effect: webapis.imageview.EFFECT_FADE_1
	         * }
	         */
	        var iurl = String(url);
			
	        // Parameter Type Checking
	        if (
	            !_isType(successCallback, 'function') ||
	            !_isType(errorCallback, 'undefined|function|null') ||
	            !_isType(iurl, 'string') ||
	            !_isType(option, 'undefined|object|null')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
			if (_isType(option, 'object')) {
				if (
					!_isType(option.width, 'undefined|number|null') ||
					!_isType(option.height, 'undefined|number|null') || 
					!_isType(option.effect, 'undefined|number|null') 
				) {
					throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				}
			}
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[ImageView'+id+'] Do init() first..');
				return false;
			};
			
			cbOnDecordingComplete = successCallback;
			cbOnPrepareError = errorCallback;
			
			this.__defineGetter__('url', function(){return iurl;}); // [Instance properties]
			
			this.stop();
			this.initPlayer(iurl);
			if (bSlideshowMode) {
				if (option && option.effect !== undefined) {
					this.setTransitionEffect(option.effect);
					this.__defineGetter__('effect', function(){return option.effect;}); // [Instance properties]
				}
				else { // [120406] 사용자가 effect 안넣었을 경우 NONE 으로 설정함. 
					this.setTransitionEffect(webapis.imageview.EFFECT_NONE);
					this.__defineGetter__('effect', function(){return webapis.imageview.EFFECT_NONE;}); // [Instance properties]
				}
			}
			else {
				this.__defineGetter__('effect', function(){return webapis.imageview.EFFECT_NONE;}); // [Instance properties]
			}
			
			this.setDisplayLock(true);
			
			/*
			 * [120229]
			 * Uniplayer 가 설정된 SetDisplayArea 에 맞게 사진을 스케일링하여 가운데에 표시함.
			 * -> width, height 필요없음. 
			 * -> width, height 를 넣었을 경우, 해당 사이즈에 맞게 스케일링 함. 더 작게 그리려고 할 때만 의미있음.
			 */ 
			if (!bSlideshowMode) { 
				if(option && option.width && option.height) { // width/height를 넣었을 경우, 해당 사이즈로 맞춤. 
					var fitDisplayArea = this.getFitDisplayArea(option.width, option.height);
					this.setDisplayArea(fitDisplayArea);
				}
				else {
					this.setDisplayArea(iDisplayRect);
				}
			}
	
			// imageWidth, imageHeight 값은 STREAM_INFO_READY 이벤트 이후에 설정됨. 그 전에는 0을 리턴.
			this.__defineGetter__('imageWidth', function(){return 0;}); // [Instance properties]
			this.__defineGetter__('imageHeight', function(){return 0;}); // [Instance properties]
	
			this.showImage();
		}
		/*
		 * Function: draw
		 * 	This method is used to start drawing image that is stored in the url passed as a parameter to the prepare function. Upon successfully drawing the image file successCallback is called and upon failure errorCallback is called.
		 *
		 * Parameters:
		 * 	o successCallback	- (Function) Callback upon Success
		 * 	o errorCallback	- (Function) Callback upon Failure
		 *
		 * Returns:
		 * 	nothing.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.draw = function (successCallback, errorCallback) {
			alert('[ImageView'+id+'] draw('+typeof successCallback+','+typeof errorCallback+')');
						
	        // Parameter Type Checking
	        if (
	            !_isType(successCallback, 'function') ||
	            !_isType(errorCallback, 'function')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[ImageView'+id+'] Do init() first..');
				return false;
			};
			
			/*
			 * successCallback: RENDERING_COMPLETE 에서 호출됨.
			 * errorCallback: setDisplayLock(false) ~ RENDERING_COMPLETE 사이 발생한 에러 상황에서 호출됨.
			 */
			cbOnRenderingComplete = successCallback;
			cbOnDrawError = errorCallback;
			this.setDisplayLock(false);
		}
		/*
		 * Function: clear
		 * 	This function shows the display area on the tv screen.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure. 
		 *
		 * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.clear = function () {
			alert('[ImageView'+id+'] clear()');
			
			if (!bInitialize) {
				alert('[ImageView'+id+'] Do init() first..');
				return false;
			};
	
			this.stop();
	
			var bUseSEF = webapis._plugin.getSEFAvailable();
			if (bUseSEF) {
				// SEF 에서는 ClearScreenForImage
				var retValue = webapis._plugin(eImageViewerPlugin, 'ClearScreenForImage');	// 1: success, -1: fail
			}
			else {
				// Device API 에서는 ClearScreen
				var retValue = webapis._plugin(eImageViewerPlugin, 'ClearScreen');	// 1: success, -1: fail
			}
	
			alert('[ImageView'+id+'] clear() returns ' + (retValue == 1));
			return retValue == 1;
		}
		/*
	     * Function: show
	     *  This function shows the display area on the tv screen.
	     *
	     * Parameters:
	     *  N/A
	     *
	     * Returns:
	     *  nothing.
	     *  
	     * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	     */
	    this.show = function(){
	        alert('[ImageView'+id+'] show()');
	        
	        if (!bInitialize) {
	            alert('[ImageView'+id+'] Do init() first..');
	            return false;
	        };
	        
	        /*
	         * display:none 으로 설정하면 Plugin 함수 콜이 불가능하다. (Webkit)
	         * -> [111220] visibility 속성으로 조절하도록 함
	         * -> [120118] 마우스 대응을 위해 width, height를 조정하도록 수정.
	         * -> [120125] visibility 로 마우스 대응 가능
	         */
	        eInnerContainerDiv.style.visibility = 'visible';
	        return true;
	        
	    }
	    /*
	     * Function: hide
	     *  This function hides the display area on the tv screen.
	     *
	     * Parameters:
	     *  N/A
	     *
	     * Returns:
	     *  nothing.
	     *  
	     * Throws:
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
	     */
	    this.hide = function(){
	        alert('[ImageView'+id+'] hide()');
	        
	        if (!bInitialize) {
	            alert('[ImageView'+id+'] Do init() first..');
	            return false;
	        };
	        
	        eInnerContainerDiv.style.visibility = 'hidden';
	        
	        return true;
	    }
		/*
		 * Function: setDisplayRect
		 * 	This method sets the display area to the values in the srect. It has four values. The first two values give the x and y co-ordinate of the top left corner of the area and the next two values give the width and height of the image.
		 *
		 * Parameters:
		 * 	o rect - (SRect)
		 *
		 * Returns:
		 * 	nothing.
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type. 
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setDisplayRect = function (rect) {	// [IDL]
			alert('[ImageView'+id+'] setDisplayRect('+rect+')');
			
			// Parameter Type Checking
	        if (
	            !_isType(rect, 'object') ||
	            !_isType(rect.top, 'number') ||
	            !_isType(rect.left, 'number') ||
	            !_isType(rect.width, 'number') ||
	            !_isType(rect.height, 'number')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[ImageView'+id+'] Do init() first..');
				return false;
			};
			
			if (!rect) {
				alert('[ImageView'+id+'] No rect parameter. Set DisplayRect to Full screen..');
				rect = new SRect (0,0,curWidget.width,curWidget.height);
			}
			
			iDisplayRect = rect;
			this.__defineGetter__('displayRect', function(){return rect;}); // [Instance properties]
			
			eInnerContainerDiv.style.left = iDisplayRect.left + 'px';
			eInnerContainerDiv.style.top = iDisplayRect.top  + 'px';
			eInnerContainerDiv.style.width = iDisplayRect.width + 'px';
			eInnerContainerDiv.style.height = iDisplayRect.height + 'px';
			
	//		eImageViewerPlugin.style.left = iDisplayRect.left + 'px';
	//		eImageViewerPlugin.style.top = iDisplayRect.top  + 'px';
			eImageViewerPlugin.style.width = iDisplayRect.width + 'px';
			eImageViewerPlugin.style.height = iDisplayRect.height + 'px';
			
			return true;
		}
		/*
		 * Function: setSlideShow
		 * 	sets the slide show mode.
		 *
		 * Parameters:
		 * 	o slideshowMode	- (Boolean) true for setting slide show mode, false for viewing a still image.
		 *
		 * Returns:
		 * 	(Boolean) True on success and false on failure. 
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type. 
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.setSlideShow = function (on) {
			alert('[ImageView'+id+'] setSlideShow('+on+')');
			var ion = Boolean(on);
			// Parameter Type Checking
	        if (
	            !_isType(ion, 'boolean')
	        ) {
	            throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
	        }
	        // Parameter Type Checking End
			
			if (!bInitialize) {
				alert('[ImageView'+id+'] Do init() first..');
				return false;
			};
			
			if (ion) {
				bSlideshowMode = true;
				webapis.imageview._setSlideShow(true); // SetEffect(INIT) 중앙 관리 함수 호출
				bSetSlideShowStatusOn = true;	// STREAM_INFO_READY에서 setSlideShowStatus(true)가 실행되도록 함
			}
			else {
				bSlideshowMode = false;
				webapis.imageview._setSlideShow(false, this); // SetEffect(INIT) 중앙 관리 함수 호출
				this.setSlideShowStatus(false);
			}
			
			return true;
		}
		/*
		 * Function: getTransitionEffectList
		 * 	gets the list of the supported effect modes.
		 *
		 * Parameters:
		 * 	N/A 
		 *
		 * Returns:
		 * 	(String)
		 * 
		 * Throws:
	     *  o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type. 
	     *  o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported. 
	     *  o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		this.getTransitionEffectList = function () {
			
			if (!bInitialize) {
				alert('[ImageView'+id+'] Do init() first..');
				return false;
			};
			
			// firmware
			var bEffectAvailable = this.isTransitionEffectAvailable();
			var effectList = '';
			alert('nFirmwareYear : ' + nFirmwareYear + ', bEffectAvailable : ' + bEffectAvailable);
			/*
			 * 2011 모델부터 (Broadcom, Firenze, Genoa-S, Genoa-P, X6, ...)
			 */
			if (nFirmwareYear >= 2011) {
				if (bEffectAvailable) effectList = getTransitionEffectList();
				else effectList = 'NONE';
			}
			/*
			 * 2010 모델 (Barcelona, Valencia, Trident)
			 * - GetTransitionEffectList() API가 구현되어 있지 않지만, 모든 Effect 지원
			 * - Braodcom 은 Event 받는 방식부터 다르기 때문에 따로 개발 필요
			 */
			else if (nFirmwareYear == 2010) {	
				if (bEffectAvailable) effectList = 'FADE1,FADE2,BLIND,SPIRAL,CHECKER,LINEAR,STAIR,WIPE,RANDOM,NONE';
				else effectList = 'NONE';
			}
			/*
			 * 2009(Chelsea), 2008(Ruby?) -> Framework 릴리즈되지 않음.
			 */
			else if (nFirmwareYear == -1) {
				effectList = 'NONE';
			}
			/*
			 * Unknown
			 */
			else {
				effectList = getTransitionEffectList();	// Plugin에 해당 API가 있으면 실행
				if (effectList == webapis._pluginDef.PLR_NOT_IMPLEMENT) {	// 없으면 NONE 으로 설정
					effectList = 'NONE';
				}
			}
			
			alert('[AF service.imagecore] Available Effects : ' + effectList);
			
			// string to define value array
			var aEffectListStr = effectList.split(',');
			var aEffectListEnum = [];
			for (var i = 0; i < aEffectListStr.length; i++) {
				if (webapis.imageview['EFFECT_' + aEffectListStr[i]] !== undefined) {
					aEffectListEnum.push(webapis.imageview['EFFECT_' + aEffectListStr[i]]);
				}
			}
			
			alert('[AF service.imagecore] getTransitionEffectList() returns ' + aEffectListEnum);
			return aEffectListEnum;
			
			function getTransitionEffectList() {
				var retValue = webapis._plugin(eImageViewerPlugin, 'GetTransitionEffectList');
				alert('[ImageView'+id+'] getTransitionEffectList() returns ' + retValue);
				return retValue;
			}
		}
		// get/setZIndex는 innerContainer의 값을 조작한다.
	    /*
	     * Function: getZIndex
	     * 	This function returns z-index of the ImageView.
	     *
	     * Parameters:
	     * 	o N/A
	     *
	     * Returns:
	     * 	(Number) the z-index of the ImageView.
	     */
	    this.getZIndex = function(){
			
	        if (!bInitialize) {
	            alert('[ImageView'+id+'] Do init() first..');
	            return false;
	        };
			
	        alert('[ImageView'+id+'] getZIndex() returns ' + this.zIndex);
	        return this.zIndex;
	    }
	    /*
	     * Function: setZIndex
	     * 	This function sets z-index of the ImageView.
	     *
	     * Parameters:
	     * 	o zIndex - (Number) z-index
	     *
	     * Returns:
	     * 	nothing
	     */
	    this.setZIndex = function(zIndex){
			alert('[ImageView'+id+'] setZIndex('+zIndex+')');
	        if (!bInitialize) {
	            alert('[ImageView'+id+'] Do init() first..');
	            return false;
	        };
			
			// [120406] jQuery 사용 제거
			document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID + id).style.zIndex = zIndex; // "IMAGEVIEWER_CONTAINER_DIV_ID + id" 는 innerContainer의 ID 
			/*
			 * this.zIndex 를 function(){return innerContainer.style.zIndex;} 로 지정했기 때문에,
			 * this.zIndex 는 자동으로 변경됨.
			 */
	    }
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// IDL 에 없는 EMP Plugin API들
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		this.initPlayer = function (url) {
			if (!bInitialize) initialize();
			url = url.replace(/&amp;/g, '&');
			var retValue = webapis._plugin(eImageViewerPlugin, 'InitPlayer', url);	// 1: success, -1: fail
			
			alert('[ImageView'+id+'] initPlayer('+url+') returns ' + (retValue == 1));
			
			if (webapis.imageview._bCallSetEffectWithINIT) {
				this.setTransitionEffect(webapis.imageview.EFFECT_INIT);
				webapis.imageview._bCallSetEffectWithINIT = false;
			}
			return retValue == 1;
		}
		this.showImage = function () {
			if (!bInitialize) initialize();
			var bUseSEF = webapis._plugin.getSEFAvailable();
			if (bUseSEF) {
				// SEF 에서는 StartPlayback
				var retValue = webapis._plugin(eImageViewerPlugin, 'StartPlayback');	// 1: success, -1: fail
			}
			else {
				// Device API 에서는 ShowImage
				var retValue = webapis._plugin(eImageViewerPlugin, 'ShowImage');	// 1: success, -1: fail
			}
			alert('[ImageView'+id+'] showImage() returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.stop = function () {
			var retValue = webapis._plugin(eImageViewerPlugin, 'Stop');	// 1: success, -1: fail
			self._setStatus(webapis.imageview.IMAGEVIEW_STATE_STOPPED);
			
			alert('[ImageView'+id+'] stop() returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setTransitionEffect = function (effect) {
			var retValue = webapis._plugin(eImageViewerPlugin, 'SetTransitionEffect', effect);	// 1: success, -1: fail
			alert('[ImageView'+id+'] setTransitionEffect('+effect+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setDisplayLock = function (bLock) {
			var retValue = webapis._plugin(eImageViewerPlugin, 'SetDisplayLock', bLock == true ? 1 : 0);	// 1: success, -1: fail
			alert('[ImageView'+id+'] setDisplayLock('+bLock+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setDisplayArea = function (rect) {
			var retValue = null;
			var pluginVer = webapis._plugin(eImageViewerPlugin, 'GetVersion');
			if (pluginVer.isSEF) {
				retValue = webapis._plugin(eImageViewerPlugin, 'SetDisplayArea', rect.left, rect.top, Math.min(rect.width+1, curWidget.width), Math.min(rect.height+1, curWidget.height), curWidget.height);	// 1: success, -1: fail
			}
			else {
				// Converting for resolution
				var resolutionConstant; 
				if (curWidget.height == 540) resolutionConstant = 1;
				else if (curWidget.height == 720) resolutionConstant = 0.75;	// 540 / 720
				else if (curWidget.height == 1080) resolutionConstant = 0.5;	// 540 / 1080
				else resolutionConstant = 1;
				
				var nLeft = Math.ceil(rect.left * resolutionConstant);
				var nTop =  Math.ceil(rect.top * resolutionConstant);
				var nWidth =  Math.ceil(rect.width * resolutionConstant)+1;
				var nHeight =  Math.ceil(rect.height * resolutionConstant)+1;
				
				retValue = webapis._plugin(eImageViewerPlugin, 'SetDisplayArea', nLeft, nTop, Math.min(nWidth, 960), Math.min(nHeight, 540));	// 1: success, -1: fail
			}
			
			this.__defineGetter__('displayArea', function(){return rect;}); // [Instance properties]
			
			alert('[ImageView'+id+'] setDisplayArea('+rect+') returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.isTransitionEffectAvailable = function () {
			var retValue = webapis._plugin(eImageViewerPlugin, 'IsTransitionEffectAvailable');	// 1: success, -1: fail
			alert('[ImageView'+id+'] isTransitionEffectAvailable() returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setSlideShowStatus = function (on) {
			var retValue = webapis._plugin(eImageViewerPlugin, 'SetSlideShowStatus', on);
			alert('[ImageView'+id+'] setSlideShowStatus('+on+') returns ' + retValue);
			return retValue;
		}
		this.getVideoResolution = function () {
			var retValue = null;
			var bUseSEF = webapis._plugin.getSEFAvailable();
			if (bUseSEF) {
				retValue = webapis._plugin(eImageViewerPlugin, 'GetVideoResolution');	// x: success, -1: fail
			}
			else {
				retValue = eImageViewerPlugin.GetVideoWidth() + '|' + eImageViewerPlugin.GetVideoHeight();	// EMP Player 형태로 맞춤
			}
			alert('[ImageView'+id+'] getVideoResolution() returns ' + retValue);
			return retValue;
		}
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// 기타 ImaegViewer 기능
		//// 1. <object> 관리  
		//// 3. Event listener 
		//// 4. Status 관리
		//// 5. Front panel 관리
		//// 6. Screen saver 관리  
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		var IMAGEVIEWER_OBJECT_ID = '_plugin_imageviewer_';	// webapis._plugin() 에서 사용하는 것과 동일하게 한다. [110810]
		var IMAGEVIEWER_OBJECT_Z_INDEX = 10;
		var IMAGEVIEWER_CONTAINER_DIV_ID = '_pluginObjectImageviewerContainer_';	// webapis -> addPluginObject() 에서 사용하는 것과 동일하게 한다. [110810] 
		var IMAGEVIEWER_CONTAINER_DIV_Z_INDEX = 10;
		
		var self = this;
		var eImageViewerPlugin = null;
		var eInnerContainerDiv = null;
		var bInitialize = false;
		var iDisplayRect = null;	// instance of SRect
		var bAutoRatio = false;		// Genoa에서는 STREAM_INFO_READY 에서 SetDisplayArea()가 동작 안한다. (Valencia에서는 가능)
		var bSlideshowMode = false;
		var bSetSlideShowStatusOn = false;	// Slideshow 중 불리는 Stop()에 의해 Menu 가 닫히는 문제 방지용 flag
		
		var oInitOption = {};	// init()에서 property 추가됨
		var cbOnDecordingComplete = null;
		var cbOnPrepareError = null;
		var cbOnRenderingComplete = null;
		var cbOnDrawError = null;	// 불리지 않음..
		
		var nFirmwareYear = null;
		var nFirmwareVer = null;
		
		/*
		 * Valencia 에서는 DecodingComplete 가 없는 대신,
		 * BufferingComplete 가 두번 불리고, 그 중 두 번째가 DecodingComplete 임.
		 */
		var bBufferingComplete = true;
		
		this.Event2String = {};	// serial log 에 보여주기 위한 string
		this.Event2String[webapis.imageview.STREAM_INFO_READY] = 'STREAM_INFO_READY';
		this.Event2String[webapis.imageview.BUFFERING_START] = 'BUFFERING_START';
		this.Event2String[webapis.imageview.BUFFERING_COMPLETE] = 'BUFFERING_COMPLETE';
		this.Event2String[webapis.imageview.DECODING_COMPLETE] = 'DECODING_COMPLETE';
		this.Event2String[webapis.imageview.RENDERING_COMPLETE] = 'RENDERING_COMPLETE';
		this.Event2String[webapis.imageview.CONNECTION_FAILED] = 'CONNECTION_FAILED';
		this.Event2String[webapis.imageview.AUTHENTICATION_FAILED] = 'AUTHENTICATION_FAILED';
		this.Event2String[webapis.imageview.STREAM_NOT_FOUND] = 'STREAM_NOT_FOUND';
		this.Event2String[webapis.imageview.NETWORK_DISCONNECTED] = 'NETWORK_DISCONNECTED';
		this.Event2String[webapis.imageview.NETWORK_SLOW] = 'NETWORK_SLOW';
		this.Event2String[webapis.imageview.RENDER_ERROR] = 'RENDER_ERROR';
		
		this.State2String = {};
		this.State2String[webapis.imageview.IMAGEVIEW_STATE_IDLE] = 'IMAGEVIEW_STATE_IDLE';
		this.State2String[webapis.imageview.IMAGEVIEW_STATE_INITIALIZED] = 'IMAGEVIEW_STATE_INITIALIZED';
		this.State2String[webapis.imageview.IMAGEVIEW_STATE_PREPARED] = 'IMAGEVIEW_STATE_PREPARED';
		this.State2String[webapis.imageview.IMAGEVIEW_STATE_DRAWN] = 'IMAGEVIEW_STATE_DRAWN';
		this.State2String[webapis.imageview.IMAGEVIEW_STATE_STOPPED] = 'IMAGEVIEW_STATE_STOPPED';
					
		function initialize(containerID, zIndex, pluginObjectID) {
			alert('[ImageView'+id+'] initialize('+(containerID||'')+','+(zIndex||'')+','+(pluginObjectID||'')+')');
			
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			alert('[ImageView'+id+'] Firmware : ' + sFirmware);
			
			/*
			 * aResult[0] = 2011-1007
			 * aResult[1] = 2011
			 * aResult[2] = 1007
			 */
			var aResult = sFirmware.match(/(\d+)-(\d+)/);
			nFirmwareYear = aResult[1];	
			nFirmwareVer = aResult[2];
			
			// <object> 삽입
			self.setImageViewerPluginObject(containerID, zIndex, pluginObjectID);
			
			// App 종료시 .destory() 호출
			if (nFirmwareYear >= 2012) { // Echo
				window.addEventListener('unload', function(){ // Webkit 은 body 에 addEventListener()로 unload 이벤트 등록 불가.
					alert('[ImageView'+id+'] !!!WINDOW UNLOAD!!!');
					self.destroy();
				}); 
			}
			else { // Genoa, Valencia
				document.getElementsByTagName('body')[0].addEventListener('unload', self.destroy);
			}
			
			webapis._plugin('TVMW', 'GetVersion');	// unload -> stop -> setScreenSaver(false) -> TVMW 삽입시 에러나는 것 방지
	
			bInitialize = true;
		}
		function initializeEnv () {
			// initialize instance properties
	//		self.__defineGetter__('id', function(){return id;});
		    self.__defineGetter__('url', function(){return null;});
		    self.__defineGetter__('imageWidth', function(){return null;});
		    self.__defineGetter__('imageHeight', function(){return null;});
	//	    self.__defineGetter__('displayRect', function(){return null;});
		    self.__defineGetter__('displayArea', function(){return null;});
	//	    self.__defineGetter__('containerID', function(){return null;});
	//	    self.__defineGetter__('zIndex', function(){return null;});
		    self.__defineGetter__('effect', function(){return null;});
	//	    self.__defineGetter__('status', function(){return webapis.imageview.IMAGEVIEW_STATE_IDLE;});
		}
		this.destroy = function () {
			alert('[ImageView'+id+'] destroy()');
			if (bSlideshowMode) {
				self.setSlideShow(false);
				self.stop();
				self.clear();
			}
			else {
				self.stop();
			}
		}
		this._setStatus = function (status) {
			alert('[ImageView'+id+'] _setStatus('+status+') -> ' + this.State2String[status]);
			this.__defineGetter__('status', function(){return status;}); // [Instance properties]
		}
		this.setImageViewerPluginObject = function (containerID, zIndex, pluginObjectID) {
			alert('[ImageView'+id+'] setImageViewerPluginObject('+(containerID?containerID:'')+','+(zIndex?zIndex:'')+','+(pluginObjectID?pluginObjectID:'')+')');
			alert('[ImageView'+id+'] old eImageViewerPlugin : ' + eImageViewerPlugin);
			
			if (eImageViewerPlugin) {	// 이전 Player Object를 제거
				this.stop();	// Play() 상태에서 DOM을 건들면 죽는다.
				eImageViewerPlugin.parentNode.removeChild(eImageViewerPlugin);
				eImageViewerPlugin = null;
			}
			
			if (pluginObjectID) {	// 동일한 object id 가 있으면 제거
				var ePluginObject = document.getElementById(pluginObjectID);
				if (ePluginObject) {
					webapis._plugin(ePluginObject, 'Stop');	// Play() 상태에서 DOM을 건들면 죽는다.
					ePluginObject.parentNode.removeChild(ePluginObject);
				}
			}
			
			// [120328] 같은 ID의 innerContainer가 있으면 제거 (init() 두 번 호출하면서 containerID를 지정한 경우)
			var eOldInnerContainer = document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID + id);
			if ( eOldInnerContainer ) {
				eOldInnerContainer.parentNode.removeChild(eOldInnerContainer);
			}
			
			/*
			 * [111220] 아래 요구사항을 충족할 수 있도록 구조 변경
			 * 1. 자동으로 생성해준 Container 의 위치가 (0, 0) 이라 (0, 0) 부터 <object> 위치까지 mouse 클릭 불가 문제
			 *  -> 내부 Container 생성 & 내부 Container 위치를 사용자 설정값으로 지정, <object>는 width, height만 지정  
			 * 2. 사용자가 같은 Container 에 <object> 삽입할 경우, 첫번째 넣은 <object>가 DOM 에서 빠졌다가 들어가 오동작
			 *  -> 내부 Container 생성 
			 */
			
			var innerContainer = document.createElement('div');
			innerContainer.id = IMAGEVIEWER_CONTAINER_DIV_ID + id;
			innerContainer.style.position = 'absolute';
			innerContainer.style.left = '0px'; // 위치는 setDisplayRect() 에서 지정됨
			innerContainer.style.top = '0px';
			innerContainer.style.width = '0px';
			innerContainer.style.height = '0px';
			innerContainer.style.zIndex = (zIndex !== undefined && zIndex !== null) ? zIndex : IMAGEVIEWER_CONTAINER_DIV_Z_INDEX; // z-index가 있을 경우, container div 에 적용			
			if (containerID) { // 사용자가 지정한 container에 삽입
				var eContainerDiv = document.getElementById(containerID);				
				if (eContainerDiv) { // Element가 있으면 내부 container를 삽입					
					eContainerDiv.appendChild(innerContainer);
					this.__defineGetter__('containerID', function(){return containerID;}); // [Instance properties]
				}
				else { // 없으면 에러
					alert('[' + sName + '] !ERROR! cannot get ' + containerID + ' element.');
					eImageViewerPlugin = null;
					return;
				}
			}
			else { // 지정 안한 경우, body에 삽입
				document.body.appendChild(innerContainer);
				this.__defineGetter__('containerID', function(){return innerContainer.id;}); // [Instance properties]
			}
			
			var bUseSEF = webapis._plugin.getSEFAvailable();
			var sPluginObjectId = pluginObjectID || IMAGEVIEWER_OBJECT_ID;
			var sZIndex = 'z-index:' + IMAGEVIEWER_OBJECT_Z_INDEX + ';';
			
			eInnerContainerDiv = document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID + id);
			eInnerContainerDiv.innerHTML = getPluginObjectHTML(bUseSEF, sPluginObjectId, sZIndex);
			eImageViewerPlugin = document.getElementById(sPluginObjectId);
			
			this.__defineGetter__('zIndex', function(){return innerContainer.style.zIndex;}); // [Instance properties]
			
			if (eImageViewerPlugin) {
				// Event listener 생성 -> Multi instance를 지원하기 위함
				// eval은 현재 어휘 유효범위에서 실행되므로 this 전달 가능.
				var strEval = 'window.ImageViewerEventListener' + id + ' = new _ImageViewerEventListener('+id+', this)';
				eval(strEval);
				
				// Register Event handler
				if (bUseSEF) {
					eImageViewerPlugin.Open('Player', '1.000', 'Player');
					// [111208] String 형태로도 OnEvent에 등록될 수 있게 Webkit 수정되었음.(by 송동현D)
					eImageViewerPlugin.OnEvent = 'ImageViewerEventListener' + id + '.onEvent';
				}
				else {
					eImageViewerPlugin.OnBufferingStart 	= 'ImageViewerEventListener' + id + '.onBufferingStart';
					eImageViewerPlugin.OnBufferingComplete 	= 'ImageViewerEventListener' + id + '.onBufferingComplete';
					eImageViewerPlugin.OnStreamInfoReady 	= 'ImageViewerEventListener' + id + '.onStreamInfoReady';
					eImageViewerPlugin.OnDecodingComplete 	= 'ImageViewerEventListener' + id + '.onDecodingComplete';
					eImageViewerPlugin.OnRenderingComplete 	= 'ImageViewerEventListener' + id + '.onRenderingComplete';
					eImageViewerPlugin.OnConnectionFailed 	= 'ImageViewerEventListener' + id + '.onConnectionFailed';
					eImageViewerPlugin.OnAuthenticationFailed = 'ImageViewerEventListener' + id + '.onAuthenticationFailed';
					eImageViewerPlugin.OnStreamNotFound 	= 'ImageViewerEventListener' + id + '.onStreamNotFound';
					eImageViewerPlugin.OnNetworkDisconnected = 'ImageViewerEventListener' + id + '.onNetworkDisconnected';
					eImageViewerPlugin.OnNetworkSlow 		= 'ImageViewerEventListener' + id + '.onNetworkSlow';
					eImageViewerPlugin.OnRenderError 		= 'ImageViewerEventListener' + id + '.onRenderError';
				}
			}
			else {
				alert('[ImageView'+id+'] ! ERROR ! Fail to set ImageViewer plugin object.');
			}
			
			return eImageViewerPlugin;
			
			function getPluginObjectHTML (useSEF, pluginObjectID, zIndex) {
				var sPluginObjectHTML = '';
				if (useSEF) {
					sPluginObjectHTML = '<OBJECT id="'+pluginObjectID+'" classid="clsid:SAMSUNG-INFOLINK-SEF" style="position:absolute;left:0px;top:0px;width:0px;height:0px;' + zIndex + 'display:block;"></OBJECT>';
				}
				else {
					sPluginObjectHTML = '<OBJECT id="'+pluginObjectID+'" classid="clsid:SAMSUNG-INFOLINK-IMAGEVIEWER" style="position:absolute;left:0px;top:0px;width:0px;height:0px;' + zIndex + 'display:block;"></OBJECT>';
				}
				alert('[ImageView'+id+'] getPluginObjectHTML('+useSEF+','+pluginObjectID+','+zIndex+') returns ' + sPluginObjectHTML);
				return sPluginObjectHTML;
			}
		}
		
		this.onEvent = function (type, data) {
	//		alert('[ImageView'+id+'] onEvent('+type+','+data+')');
			
			switch(type){
				case webapis.imageview.STREAM_INFO_READY:
					bBufferingComplete = true;
					
					// [120229] fitDisplayArea 를 설정할 필요 없음.
	//				if (!bSlideshowMode) {	// slideshow mode 에서는 SetDisaplayArea()를 사용하지 않음.
	//					if (bAutoRatio) {
	//						var resolution = this.getVideoResolution().split('|');	// '1280|720'
	//						var fitDisplayArea = this.getFitDisplayArea(resolution[0], resolution[1]);
	//						this.setDisplayArea(fitDisplayArea); 
	//					}
	//				}
					
					// [120229] Resolution property
					var resolution = this.getVideoResolution();					
					if (typeof resolution == 'string') {
						resolution = resolution.split('|');	// '1280|720'
						
						this.__defineGetter__('imageWidth', function(){return resolution[0];}); // [Instance properties]
						this.__defineGetter__('imageHeight', function(){return resolution[1];}); // [Instance properties]
						
						/*
						 * [120426] Valencia는 STREAM_INFO_READY에 SetDisplayArea()를 해야 정해진 영역에 원본 비율로 그릴 수 있다.
						 * resolution 이 정상적으로 얻어졌을 경우만 동작하도록 함.
						 */ 
						if (nDevice == DEVICE_2010 && !bSlideshowMode) {
							alert('[ImageView'+id+'] DEVICE_2010. Set displayArea.');
							var resolution = this.getVideoResolution().split('|'),	// '1280|720'
								fitDisplayArea = this.getFitDisplayArea(resolution[0], resolution[1]);
							this.setDisplayArea(fitDisplayArea);
						}
					}
					else {
						this.__defineGetter__('imageWidth', function(){return null;}); // [Instance properties]
						this.__defineGetter__('imageHeight', function(){return null;}); // [Instance properties]
					}
					
					if (bSetSlideShowStatusOn) {	// Menu 띄웠을 때 Stop() 호출하면 내려가는 문제 해결
						this.setSlideShowStatus(true);
						bSetSlideShowStatusOn = false;
					}
					break;
					
				case webapis.imageview.BUFFERING_START:
					break;
					
				case webapis.imageview.BUFFERING_COMPLETE:
					if (bBufferingComplete) {
						bBufferingComplete = false;
					}
					else {
						this._setStatus(webapis.imageview.IMAGEVIEW_STATE_PREPARED);	// DECODING_COMPLETE 발생 == 준비 완료 
						if (typeof cbOnDecordingComplete == 'function') {
							cbOnDecordingComplete();
						}
					}
					break;
					
				case webapis.imageview.DECODING_COMPLETE:
					this._setStatus(webapis.imageview.IMAGEVIEW_STATE_PREPARED);	// DECODING_COMPLETE 발생 == 준비 완료 
					if (typeof cbOnDecordingComplete == 'function') {
						cbOnDecordingComplete();
					}
					break;
					
				case webapis.imageview.RENDERING_COMPLETE:
					if (this.status == webapis.imageview.IMAGEVIEW_STATE_STOPPED) {
						alert('[ImageView'+id+'] This RENDERING_COMPLETE event occured after stop() call. skip.');
						break;
					}
					this._setStatus(webapis.imageview.IMAGEVIEW_STATE_DRAWN);
					if (typeof cbOnRenderingComplete == 'function') {
						cbOnRenderingComplete();
					}
					break;
				
				case webapis.imageview.CONNECTION_FAILED:
				case webapis.imageview.STREAM_NOT_FOUND:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR));
					}
					break;
				
				case webapis.imageview.AUTHENTICATION_FAILED:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR));
					}
					break;
				
				case webapis.imageview.NETWORK_DISCONNECTED:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NETWORK_ERR));
					}
					break;
					
				case webapis.imageview.NETWORK_SLOW:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byType('NetworkSlowError'));
					}
					break;
				
				case webapis.imageview.RENDER_ERROR:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byType('RenderError'));
					}
					break;
				
				default:
					break; 
			}
		}
		this.getFitDisplayArea = function (width, height) {	// displayRect에 fit되는 영역 구하기
			var FRAME_LEFT = iDisplayRect.left;
			var FRAME_TOP = iDisplayRect.top;
			var FRAME_WIDTH = iDisplayRect.width;
			var FRAME_HEIGHT = iDisplayRect.height;
			
			var nLeft, nTop, nWidth, nHeight;
			
			if (width / height > FRAME_WIDTH / FRAME_HEIGHT) {	// frame 의 가로/세로 비율보다 크다 -> 가로가 길다 -> 가로를 frame width 로 고정하고 세로를 줄인다.
				nHeight = Math.round((FRAME_WIDTH * height) / width);	// width : height = FRAME_WIDTH : x
				nWidth = FRAME_WIDTH;		// FRAME_WIDTH 로 고정
			}
			else {		// 세로가 길다 -> 세로를 frame height로 고정하고 가로를 줄인다.
				 nWidth = Math.round ((FRAME_HEIGHT * width) / height);	// width : height = x : FRAME_HEIGHT
				 nHeight = FRAME_HEIGHT;
			}
			
	        nLeft = FRAME_LEFT + Math.round((FRAME_WIDTH - nWidth) / 2);
	        nTop = FRAME_TOP + Math.round((FRAME_HEIGHT - nHeight) / 2);
			
			var retValue = new SRect(nLeft, nTop, nWidth, nHeight);
			alert('[ImageView'+id+'] getFitDisplayArea('+width+','+height+') returns ' + retValue);
			return retValue;
		}
	}
	
	function _ImageViewerEventListener (id, imageView) {
		var sName = 'ImageView' + id;	// Instance name
		this.iImageView = imageView;
		this.setMasterObject = function(imageView){
			this.iImageView = imageView;
		}
		this.onBufferingStart = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onBufferingStart()');
			this.iImageView.onEvent(webapis.imageview.BUFFERING_START);
		}
		this.onBufferingComplete = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onBufferingComplete()');
			this.iImageView.onEvent(webapis.imageview.BUFFERING_COMPLETE);
		}
		this.onStreamInfoReady = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onStreamInfoReady()');
			this.iImageView.onEvent(webapis.imageview.STREAM_INFO_READY);
		}
		this.onDecodingComplete = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onDecodingComplete()');
			this.iImageView.onEvent(webapis.imageview.DECODING_COMPLETE);
		}
		this.onRenderingComplete = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onRenderingComplete()');
			this.iImageView.onEvent(webapis.imageview.RENDERING_COMPLETE);
		}
		this.onConnectionFailed = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onConnectionFailed()');
			this.iImageView.onEvent(webapis.imageview.CONNECTION_FAILED);
		}
		this.onAuthenticationFailed = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onAuthenticationFailed()');
			this.iImageView.onEvent(webapis.imageview.AUTHENTICATION_FAILED);
		}
		this.onStreamNotFound = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onStreamNotFound()');
			this.iImageView.onEvent(webapis.imageview.STREAM_NOT_FOUND);
		}
		this.onNetworkDisconnected = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onNetworkDisconnected()');
			this.iImageView.onEvent(webapis.imageview.NETWORK_DISCONNECTED);
		}
		this.onNetworkSlow = function(){
			alert('[ImageView'+id+'] ImageViewerEventListener : onNetworkSlow()');
			this.iImageView.onEvent(webapis.imageview.NETWORK_DISCONNECTED);
		}
		this.onRenderError = function(errorCode){
			alert('[ImageView'+id+'] ImageViewerEventListener : onRenderError(' + errorCode + ')');
			this.iImageView.onEvent(webapis.imageview.RENDER_ERROR, errorCode);
		}
		this.onEvent = function(type, param){
			alert('[ImageView'+id+'] ImageViewerEventListener : onEvent('+type+','+param+') -> ' + this.iImageView.Event2String[type]);
			this.iImageView.onEvent(type, param);
		}
	}
	
	/*
	 * Object: ImageViewInitialOption
	 * 	Specifies the options for the initialize ImageView_Broadcom2010.
	 *
	 * Properties:
	 *	containerID			- (String) The container <DIV> ID. Plugin <OBJECT> will be inserted to the <DIV> by .innerHTML.
	 *	zIndex				- (Number) The z-index property to be set to "containerID" <DIV>.
	 *  displayRect			- (SRect) The position of displayed screen area.
	 */
	
	/*
	 * Object: ImageOption
	 * 	Specifies the options for preparing the image.
	 *
	 * Properties:
	 *	width			- (Number) Image width.
	 *	height			- (Number) Image height.
	 *  effect			- (Number) slide-show effect.
	 */
	
	
	/*
	 * Namespace: imageview for 2010 Broadcom BD Player 
	 * 	
	 * 
	 * Available From:
	 * 	0.1
	 */
	function _ImageView_Broadcom2010(id) {
		var sName = 'ImageView' + id;	// Instance name
		var sVersion = '1.0';
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// IDL 에 있는 함수들
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		this.init = function (option) {	// [IDL]
			alert('[ImageView_BRCM2010'+id+'] init('+option+')');
			/*
		     oInitOption = {
		     	displayRect: new SRect(),
		     	containerID: 'divID',
		     	zIndex: 10
		     }
		     */
			oInitOption = option || {};	// 기존 option 유지 안함.
			
			initializeEnv();
			
			// <object> 삽입, pluginObjectID 는 정해진 ID로 강제함.
			this.setImageViewerPluginObject(oInitOption.containerID, oInitOption.zIndex, IMAGEVIEWER_OBJECT_ID + id);
			if (oInitOption.displayRect) {	// oInitOption -> displayRect 처리
				this.setDisplayRect(oInitOption.displayRect);
			}
			if (!bInitialize) initialize();
		}
		/*
		 * Function: show
		 * 	Shows ImageView. You can set ImageView's area by setDisplayRect().
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing.
		 */
		this.show = function(){
			alert('[ImageView_BRCM2010'+id+'] show()');
			if (!bInitialize) initialize();
			eImageViewerPlugin.style.display = 'block';
		}
		/*
		 * Function: hide
		 * 	Hides ImageView.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing.
		 */
		this.hide = function(){
			alert('[ImageView_BRCM2010'+id+'] hide()');
	        if (!bInitialize) initialize();
			eImageViewerPlugin.style.display = 'none';
		}
		/*
		 * Function: prepare
		 * 	Prepares drawing image. 
		 *
		 * Parameters:
		 * 	o successCallback	- (Function) called right after prepare(). You should call draw() method in this callback.
		 * 	o errorCallback	- (Function) called on error during draw() operation.
		 * 	o url	- (String) image URL. 
		 * 	o option	- (Object) image detail information. option should contain 'width' and 'height' of image.
		 * (start code)
		 * {
		 *     width: 1920,	// (Number) image width(px). 
		 *     height: 1080	// (Number) image height(px).
		 * }
		 * (end)
		 * 
		 * Returns:
		 * 	nothing.
		 */
		this.prepare = function (successCallback, errorCallback, url, option) {
			alert('[ImageView_BRCM2010'+id+'] prepare('+typeof successCallback+','+typeof errorCallback+','+url+','+option+')');
			if (!bInitialize) initialize();
			/*
			 * successCallback: 지원 안함, DECODING_COMPLETE 에 해당하는 이벤트가 없음.
			 * errorCallback: 실제로는 draw() 호출 후 에러가 발생하면 실행된다. webapis 와 같은 스펙으로 동작시키기 위함.
			 * url: image url,
			 * option: {
			 *   width: 500,	// [[필요한지 테스트 필요]]
			 *   height: 400,
			 *   effect: 지원 안함
			 * }
			 */
			
			cbOnDecordingComplete = successCallback;
			cbOnPrepareError = errorCallback;
			
			oImageInfo = {
				url: url,
				width: option.width,
				height: option.height
			}
			
			if (typeof cbOnDecordingComplete == 'function') {
				cbOnDecordingComplete();
			}
		}
		/*
		 * Function: draw
		 * 	Draws image. This function should be called in prepare()'s first parameter - successCallback.
		 *
		 * Parameters:
		 * 	o successCallback	- (Function) called when RENDERING_COMPLETE event occurs.
		 * 	o errorCallback	- (Function) not work on Broadcom 2010.
		 *
		 * Returns:
		 * 	nothing.
		 */
		this.draw = function (successCallback, errorCallback) {
			alert('[ImageView_BRCM2010'+id+'] draw('+typeof successCallback+','+typeof errorCallback+')');
			if (!bInitialize) initialize();
			/*
			 * successCallback: RENDERING_COMPLETE 에서 호출됨.
			 * errorCallback: webapis 와 같은 스펙으로 동작시키기 위함
			 */
			cbOnRenderingComplete = successCallback;
			cbOnDrawError = errorCallback;
			
			var fitDisplayArea = this.getFitDisplayArea(oImageInfo.width, oImageInfo.height);
			this.setDisplayArea(fitDisplayArea);
			var retValue = eImageViewerPlugin.Play(oImageInfo.url);
			
			if (retValue == 0) {
				alert('Play() returns 0 -> Fail -> Generate error..');
				onServerError();
			}
		}
		/*
		 * Function: clear
		 * 	Clears image. On Broadcom 2010, works same as stop().
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.clear = function () {
			alert('[ImageView_BRCM2010'+id+'] clear()');
			if (!bInitialize) initialize();
			var retValue = this.stop();
			alert('[ImageView_BRCM2010'+id+'] clear() returns ' + (retValue == 1));
			return retValue == 1;
		}
		/*
		 * Function: getDisplayRect
		 * 	Gets display area you set by setDisplayRect().
		 *
		 * Parameters:
		 * 	N/A 
		 *
		 * Returns:
		 * 	nothing.
		 */
		this.getDisplayRect = function () {
			alert('[ImageView_BRCM2010'+id+'] getDisplayRect() returns ' + iDisplayRect);
			return iDisplayRect;
		}
		/*
		 * Function: setDisplayRect
		 * 	Sets display rect.
		 *
		 * Parameters:
		 * 	o rect - (Object) display rect information.
		 * (start code)
		 * {
		 *   left: 100,
		 *   top: 100,
		 *   width: 500,
		 *   height: 400
		 * }
		 * or 
		 * new SRect(100,100,500,400)
		 * (end)
		 * 
		 * Returns:
		 * 	nothing.
		 */
		this.setDisplayRect = function (rect) {	// [IDL]
			alert('[ImageView_BRCM2010'+id+'] setDisplayRect('+rect+')');
			
			iDisplayRect = rect;
			
			if (!bInitialize) initialize();
			
			eImageViewerPlugin.style.left = iDisplayRect.left + 'px';
			eImageViewerPlugin.style.top = iDisplayRect.top  + 'px';
			eImageViewerPlugin.style.width = iDisplayRect.width + 'px';
			eImageViewerPlugin.style.height = iDisplayRect.height + 'px';
		}
		this.setSlideShow = function (start) {
			alert('[ImageView_BRCM2010'+id+'] setSlideShow('+start+') Not support..');
		}
		this.getTransitionEffectList = function () {
			alert('[ImageView_BRCM2010'+id+'] getTransitionEffectList() Not support..');
		}
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// IDL 에 없는 EMP Plugin API들
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		this.stop = function () {
			var retValue = eImageViewerPlugin.Stop();
			alert('[ImageView_BRCM2010'+id+'] stop() returns ' + (retValue == 1));
			return retValue == 1;
		}
		this.setDisplayArea = function (rect) {
			// Converting for resolution
			var resolutionConstant; 
			if (curWidget.height == 540) resolutionConstant = 1;
			else if (curWidget.height == 720) resolutionConstant = 0.75;	// 540 / 720
			else if (curWidget.height == 1080) resolutionConstant = 0.5;	// 540 / 1080
			else resolutionConstant = 1;
			
			var nLeft = Math.round(rect.left * resolutionConstant);
			var nTop =  Math.round(rect.top * resolutionConstant);
			var nWidth =  Math.round(rect.width * resolutionConstant);
			var nHeight =  Math.round(rect.height * resolutionConstant);
			
			eImageViewerPlugin.SetImagePos(nLeft, nTop, nWidth, nHeight);	// SetImagePos()은 항상 0을 리턴
		}
		this.getVideoResolution = function () {
			var retValue = eImageViewerPlugin.GetVideoW() + '|' + eImageViewerPlugin.GetVideoH();	// EMP Player 형태로 맞춤
			alert('[ImageView_BRCM2010'+id+'] getVideoResolution() returns ' + retValue);
			return retValue;
		}
		
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//// 기타 ImaegViewer 기능
		//// 1. <object> 관리  
		//// 3. Event listener 
		//// 4. Status 관리
		//// 5. Front panel 관리
		//// 6. Screen saver 관리  
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		
		var IMAGEVIEWER_OBJECT_ID = '_plugin_imageviewer_';	// webapis._plugin() 에서 사용하는 것과 동일하게 한다. [110810]
		var IMAGEVIEWER_OBJECT_Z_INDEX = 10;
		var IMAGEVIEWER_CONTAINER_DIV_ID = '_pluginObjectImageviewerContainer_';	// webapis -> addPluginObject() 에서 사용하는 것과 동일하게 한다. [110810] 
		var IMAGEVIEWER_CONTAINER_DIV_Z_INDEX = 10;
		
		var self = this;
		var eImageViewerPlugin = null;
		var bInitialize = false;
		var iDisplayRect = null;	// instance of SRect
		var oImageInfo = {};
		
		var oInitOption = {};	// init()에서 property 추가됨
		var cbOnDecordingComplete = null;
		var cbOnPrepareError = null;
		var cbOnRenderingComplete = null;
		var cbOnDrawError = null;	// 불리지 않음..
		
		this.Event2String = {};	// serial log 에 보여주기 위한 string
		this.Event2String[webapis.imageview.STREAM_INFO_READY] = 'STREAM_INFO_READY';
		this.Event2String[webapis.imageview.BUFFERING_START] = 'BUFFERING_START';
		this.Event2String[webapis.imageview.BUFFERING_COMPLETE] = 'BUFFERING_COMPLETE';
		this.Event2String[webapis.imageview.DECODING_COMPLETE] = 'DECODING_COMPLETE';
		this.Event2String[webapis.imageview.RENDERING_COMPLETE] = 'RENDERING_COMPLETE';
		this.Event2String[webapis.imageview.CONNECTION_FAILED] = 'CONNECTION_FAILED';
		this.Event2String[webapis.imageview.AUTHENTICATION_FAILED] = 'AUTHENTICATION_FAILED';
		this.Event2String[webapis.imageview.STREAM_NOT_FOUND] = 'STREAM_NOT_FOUND';
		this.Event2String[webapis.imageview.NETWORK_DISCONNECTED] = 'NETWORK_DISCONNECTED';
		this.Event2String[webapis.imageview.NETWORK_SLOW] = 'NETWORK_SLOW';
		this.Event2String[webapis.imageview.RENDER_ERROR] = 'RENDER_ERROR';
					
		function initialize() {
			if (!eImageViewerPlugin) {	// eImageViewerPlugin 이 설정 안됐을 경우, default로 들어가도록 함 (body 아래, default z-index로)
				self.setImageViewerPluginObject(null, null, IMAGEVIEWER_OBJECT_ID + id);
			}
			if (!iDisplayRect) {	// displayRect 설정 안됐을 경우, 전체화면으로 설정함
				self.setDisplayRect(new SRect(0, 0, curWidget.width, curWidget.height)); 
			}
			document.getElementsByTagName('body')[0].addEventListener('unload', self.destroy);	// App 종료시 Stop() 처리	[[-> 동작할지?]]
	
			bInitialize = true;
		}
		function initializeEnv () {
			// initialize instance properties
		}
		this.destroy = function () {
			alert('[ImageView_BRCM2010'+id+'] destroy()');
			self.stop();
		}
		this._setStatus = function (status) {
			this.status = status;
		}
		this.setImageViewerPluginObject = function (containerID, zIndex, pluginObjectID) {
			alert('[ImageView_BRCM2010'+id+'] setImageViewerPluginObject('+(containerID?containerID:'')+','+(zIndex?zIndex:'')+','+(pluginObjectID?pluginObjectID:'')+')');
			
			alert('[ImageView_BRCM2010'+id+'] old eImageViewerPlugin : ' + eImageViewerPlugin);
			
			if (eImageViewerPlugin) {	// 이전 Player Object를 제거
				this.stop();	// Play() 상태에서 DOM을 건들면 죽는다.
				eImageViewerPlugin.parentNode.removeChild(eImageViewerPlugin);
				eImageViewerPlugin = null;
			}
			
			if (pluginObjectID) {	// 동일한 object id 가 있으면 제거
				var ePluginObject = document.getElementById(pluginObjectID);
				if (ePluginObject) {
					webapis._plugin(ePluginObject, 'Stop');	// Play() 상태에서 DOM을 건들면 죽는다.
					ePluginObject.parentNode.removeChild(ePluginObject);
				}
			}
			
			if (containerID) { // 사용자가 container div를 지정했을 경우 -> container에 object 삽입
				var eContainerDiv = document.getElementById(containerID);
				if (eContainerDiv) {
					var sPluginObjectId = pluginObjectID || IMAGEVIEWER_OBJECT_ID;
					var sZIndex = 'z-index:' + ((zIndex !== undefined && zIndex !== null) ? zIndex : IMAGEVIEWER_CONTAINER_DIV_Z_INDEX) +';';	// container 지정한 경우, z-index는 <object> 에 적용
	                eContainerDiv.innerHTML += getPluginObjectHTML(sPluginObjectId, sZIndex);
					eImageViewerPlugin = document.getElementById(sPluginObjectId);
				}
				else {
					alert('[ImageView_BRCM2010'+id+'] cannot get ' + containerID + ' element.');
					eImageViewerPlugin = null;
				}
			}
			else {	// container div 지정을 안했을 경우 -> default container div 생성 -> container에 object 삽입, z-index, object id 있을 경우 사용
				if (!document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID + id)) {	// default container div 가 없을 경우만 새로 생성
					var divNode = document.createElement('div');
					divNode.id = IMAGEVIEWER_CONTAINER_DIV_ID + id;
					divNode.style.position = 'absolute';
					divNode.style.left = '0px';
					divNode.style.top = '0px';
					divNode.style.zIndex = (zIndex !== undefined) ? zIndex : IMAGEVIEWER_CONTAINER_DIV_Z_INDEX; // z-index가 있을 경우, container div 에 적용
					document.body.appendChild(divNode);
				}
				
				var sPluginObjectId = pluginObjectID || IMAGEVIEWER_OBJECT_ID;
				var sZIndex = 'z-index:' + IMAGEVIEWER_OBJECT_Z_INDEX + ';';
				document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID + id).innerHTML = getPluginObjectHTML(sPluginObjectId, sZIndex);
				eImageViewerPlugin = document.getElementById(sPluginObjectId);
			}
			
			if (eImageViewerPlugin) {
				
				// Broadcom2010 용 event listener 생성. 기 정의된 함수를 Plugin에서 호출하는 형식임.
				// eval 은 어휘적 유효범위에서 실행되기 때문에 self 를 참조할 수 있다.
				
				var eventListener = '';
				eventListener += 'startDrawLoading = function(){self.onEvent(webapis.imageview.BUFFERING_START);};';
				eventListener += 'OnStreamInfoReady = function(){self.onEvent(webapis.imageview.STREAM_INFO_READY);};';
				eventListener += 'endDrawLoading = function(){self.onEvent(webapis.imageview.RENDERING_COMPLETE);};';
				eventListener += 'onDecoderReady = function(){};';
				eventListener += 'onNotSupport = function(){self.onEvent(webapis.imageview.RENDER_ERROR);};';
				eventListener += 'popupNetworkErr = function(){self.onEvent(webapis.imageview.STREAM_NOT_FOUND);};';
				eventListener += 'onServerError = function(){self.onEvent(webapis.imageview.STREAM_NOT_FOUND);};';
				eventListener += 'OnConnectionFailed = function(){self.onEvent(webapis.imageview.CONNECTION_FAILED);};';
				eventListener += 'OnAuthenticationFailed = function(){self.onEvent(webapis.imageview.AUTHENTICATION_FAILED);};';
				eventListener += 'stopPlayer = function(){};';
				
				eval(eventListener);
			}
			else {
				alert('[ImageView_BRCM2010'+id+'] ! ERROR ! Fail to set ImageViewer plugin object.');
			}
			
			return eImageViewerPlugin;
			
			function getPluginObjectHTML (pluginObjectID, zIndex) {
				var sPluginObjectHTML = '';
				sPluginObjectHTML = '<OBJECT id="'+pluginObjectID+'" classid="clsid:D27CDB6E-444553540000" style="position:absolute;width:0px;height:0px;' + zIndex + 'opacity:0.0;background-color:#000000;"></OBJECT>';
				alert('[ImageView_BRCM2010'+id+'] getPluginObjectHTML('+pluginObjectID+','+zIndex+') returns ' + sPluginObjectHTML);
				return sPluginObjectHTML;
			}
		}
		
		this.onEvent = function (type, data) {
			alert('[ImageView_BRCM2010'+id+'] onEvent('+type+','+(data||'')+') -> ' + this.Event2String[type]); 
			
			switch(type){
				case webapis.imageview.BUFFERING_START:
					break;
				
				case webapis.imageview.STREAM_INFO_READY:
					// STREAM_INFO_READY 시점에서 SetDisplayArea() 설정 동작 안함
	//				var resolution = this.getVideoResolution().split('|');
	//				var fitDisplayArea = this.getFitDisplayArea(resolution[0], resolution[1]);
	//				this.setDisplayArea(fitDisplayArea);
					break;
					
				case webapis.imageview.RENDERING_COMPLETE:
					if (typeof cbOnRenderingComplete == 'function') {
						cbOnRenderingComplete();
					}
					break;
				
				case webapis.imageview.CONNECTION_FAILED:
				case webapis.imageview.STREAM_NOT_FOUND:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR));
					}
					break;
				
				case webapis.imageview.AUTHENTICATION_FAILED:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR));
					}
					break;
				
				case webapis.imageview.NETWORK_DISCONNECTED:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NETWORK_ERR));
					}
					break;
					
				case webapis.imageview.NETWORK_SLOW:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byType('NetworkSlowError'));
					}
					break;
				
				case webapis.imageview.RENDER_ERROR:
					this.stop();
					if (typeof cbOnPrepareError == 'function') {
						cbOnPrepareError(ErrorsHelper.createWebAPIException_byType('RenderError'));
					}
					break;
	
				default:
					break;
			}
		}
		this.getFitDisplayArea = function (width, height) {	// displayRect에 fit되는 영역 구하기
			var FRAME_LEFT = iDisplayRect.left;
			var FRAME_TOP = iDisplayRect.top;
			var FRAME_WIDTH = iDisplayRect.width;
			var FRAME_HEIGHT = iDisplayRect.height;
			
			var nLeft, nTop, nWidth, nHeight;
			
			if (width / height > FRAME_WIDTH / FRAME_HEIGHT) {	// frame 의 가로/세로 비율보다 크다 -> 가로가 길다 -> 가로를 frame width 로 고정하고 세로를 줄인다.
				nHeight = Math.round((FRAME_WIDTH * height) / width);	// width : height = FRAME_WIDTH : x
				nWidth = FRAME_WIDTH;		// FRAME_WIDTH 로 고정
			}
			else {		// 세로가 길다 -> 세로를 frame height로 고정하고 가로를 줄인다.
				 nWidth = Math.round ((FRAME_HEIGHT * width) / height);	// width : height = x : FRAME_HEIGHT
				 nHeight = FRAME_HEIGHT;
			}
			
	        nLeft = FRAME_LEFT + Math.round((FRAME_WIDTH - nWidth) / 2);
	        nTop = FRAME_TOP + Math.round((FRAME_HEIGHT - nHeight) / 2);
			
			var retValue = new SRect(nLeft, nTop, nWidth, nHeight);
			alert('[ImageView_BRCM2010'+id+'] getFitDisplayArea('+width+','+height+') returns ' + retValue);
			return retValue;
		}
	}

})(); // End webapis.imageview

/*
 * Namespace: tv
 * 	TV module defines the hierarchy of the TV specific feature which tunes channel and gets the specified information of TV.
 * 
 * Available From:
 * 	0.1
 */
webapis.tv = {
	/*
	 * Namespace: tv.info
	 * 	info module supports to get TV specific system functions in DTV Platform.
	 *
	 *	This module offers methods to get information about the DTV device information
	 *
	 * Available From:
	 * 	0.1
	 */
	info: {
        /*
         * Enums:
         * 	Contants for TV/BD Product Type
         * 	PRODUCT_TYPE_TV - 0
         *  PRODUCT_TYPE_BD - 1
         *  PRODUCT_TYPE_MONITOR - 2
         *
         *  Contants for Timezone
         * 	TimeZone.TIMEZONE_USA_NEWFOUNDLAND	- 0
         * 	TimeZone.TIMEZONE_USA_ATLANTIC		- 1
         * 	TimeZone.TIMEZONE_USA_EASTERN		- 2
         * 	TimeZone.TIMEZONE_USA_CENTRAL		- 3
         * 	TimeZone.TIMEZONE_USA_MOUNTAIN		- 4
         * 	TimeZone.TIMEZONE_USA_PACIFIC		- 5
         * 	TimeZone.TIMEZONE_USA_ALASKA			- 6
         * 	TimeZone.TIMEZONE_USA_HAWAII			- 7
         * 	TimeZone.TIMEZONE_KOR_SEOUL			- 8
         * 	TimeZone.TIMEZONE_DVB_REGION_0		- 9 - DVB Time Zone Offset (Region 0) - Australia : Use in a contury that don't have multi time zone
         * 	TimeZone.TIMEZONE_DVB_REGION_1		- 10 - DVB Time Zone Offset (Region 1) - Australia : reserved , Spain.
         * 	TimeZone.TIMEZONE_DVB_REGION_2		- 11 - DVB Time Zone Offset (Region 2) - Australia : New Southwales/ACT , Spain
         * 	TimeZone.TIMEZONE_DVB_REGION_3		- 12 - DVB Time Zone Offset (Region 3) - Australia : Victoria
         * 	TimeZone.TIMEZONE_DVB_REGION_4		- 13 - DVB Time Zone Offset (Region 4) - Australia : Queensland
         * 	TimeZone.TIMEZONE_DVB_REGION_5		- 14 - DVB Time Zone Offset (Region 5) - Australia : South Australia
         * 	TimeZone.TIMEZONE_DVB_REGION_6		- 15 - DVB Time Zone Offset (Region 6) - Australia : Western Australia
         * 	TimeZone.TIMEZONE_DVB_REGION_7		- 16 - DVB Time Zone Offset (Region 7) - Australia : Tasmania
         * 	TimeZone.TIMEZONE_DVB_REGION_8		- 17 - DVB Time Zone Offset (Region 8) - Australia : Northern Territory
         * 
         * 	brief Timezone DST
         * 	TimeZone.TIMEZONE_DST_ON	- 1
         * 	TimeZone.TIMEZONE_DST_OFF	- 2
         * 	TimeZone.TIMEZONE_DST_AUTO	- 3
         */
		
		PRODUCT_TYPE_TV:	0,
		PRODUCT_TYPE_BD:	1,
		PRODUCT_TYPE_MONITOR:	2,
		
		//120705 
		TIMEZONE_USA_NEWFOUNDLAND : 0,
		TIMEZONE_USA_ATLANTIC :1,
		TIMEZONE_USA_EASTERN : 2,
		TIMEZONE_USA_CENTRAL : 3,
		TIMEZONE_USA_MOUNTAIN : 4,
		TIMEZONE_USA_PACIFIC : 5,
		TIMEZONE_USA_ALASKA	: 6,
		TIMEZONE_USA_HAWAII	: 7,
		TIMEZONE_KOR_SEOUL : 8,
		TIMEZONE_DVB_REGION_0 : 9, // DVB Time Zone Offset (Region 0) - Australia : Use in a contury that don't have multi time zone
		TIMEZONE_DVB_REGION_1 : 10, // DVB Time Zone Offset (Region 1) - Australia : reserved , Spain.
		TIMEZONE_DVB_REGION_2 : 11, // DVB Time Zone Offset (Region 2) - Australia : New Southwales/ACT , Spain
		TIMEZONE_DVB_REGION_3 : 12, // DVB Time Zone Offset (Region 3) - Australia : Victoria
		TIMEZONE_DVB_REGION_4 : 13, // DVB Time Zone Offset (Region 4) - Australia : Queensland
		TIMEZONE_DVB_REGION_5 : 14, // DVB Time Zone Offset (Region 5) - Australia : South Australia
		TIMEZONE_DVB_REGION_6 : 15, // DVB Time Zone Offset (Region 6) - Australia : Western Australia
		TIMEZONE_DVB_REGION_7 : 16, // DVB Time Zone Offset (Region 7) - Australia : Tasmania
		TIMEZONE_DVB_REGION_8 : 17, // DVB Time Zone Offset (Region 8) - Australia : Northern Territory
		TIMEZONE_DST_ON	: 1,
		TIMEZONE_DST_OFF : 2,
		TIMEZONE_DST_AUTO : 3,
		
		

		/*
		 * Function: getProduct
		 * 	gets a TV/BD product type.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Number) if success returns Product Type number
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getProduct: function(){
			var retValue = null;
			retValue = webapis._plugin("TV", "GetProductType");
			return retValue;
		},
		
		/*
		 * Function: getModel
		 * 	Gets the model name.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) if success returns a string that specifies the device product model name
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getModel: function(){
			var retValue = null;
			retValue = webapis._plugin("NNavi", "GetModelCode");	// returns modified model code.
			return retValue;
		},
		
		/*
		 * Function: getFirmware
		 * 	Gets the firmware version.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) if success returns the product Firmware version
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getFirmware: function(){
			var retValue = null;
			retValue = webapis._plugin("NNavi", "GetFirmware");
			return retValue;
		},
		
		/*
		 * Function: getVersion
		 * 	Gets Smart Hub Version.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) if success returns the SmartHub Version
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getVersion: function(){	
			var retValue = null;
			var smartHub = window.location.search;
			if(smartHub) {
				var smartHubFull = smartHub.split('mgrver=');
			}
			if(smartHubFull[1]) {
				var smartHubVer = smartHubFull[1].split('&');
				if(smartHubVer[0]) {
					retValue = smartHubVer[0];	// smartHubVersion 
					alert("smartHubVer[0] : " + smartHubVer[0]);
					return retValue;
				}
			}
			return false;
		},
		
		/*
		 * Function: getCountry
		 * 	Gets the country code in which device is located.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) if success returns the country code (ISO 3166) in which the device is located
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getCountry: function(){
			var retValue = null;
			var countryCode = webapis._plugin("TV", "GetCountry");
			retValue = webapis._pluginDef.PL_TV_COUNTRY_CODE[countryCode];
			return retValue;				
		},
		
		/*
		 * Function: getLanguage
		 * 	Gets the language code.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) if success returns the language code in which the user interface will be displayed.
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getLanguage: function(){
			var retValue = null;
			var languageCode = webapis._plugin("TV", "GetLanguage");
			retValue = webapis._pluginDef.PL_TV_LANGUAGE_CODE[languageCode];
			return retValue;
		},
		
		/*
		 * Function: getDeviceID
		 * 	Gets the device id. The device ID should be unique across all devices for a given product. It is useful for unique device identification.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) if success return unique device identification.
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getDeviceID: function(){
			var sMacAddr = webapis._plugin('Network', 'GetMAC',1);	// wired
			alert("sMacAddr == " + sMacAddr);
			var sDUID = webapis._plugin("NNavi", "GetDUID", sMacAddr);	// DUID(Device Unique ID ) of each DTV. 
			
			return sDUID;
		},
		
		/*
		 * Function: getESN
		 * 	Gets the value of service specific ESN number. It can be used for service partner identification and specific customizations for apps.
		 *
		 * Parameters:
		 * 	o serviceName	- (String) service name
		 *
		 * Returns:
		 * 	(String) if success returns the information which predefined profile field of DTV platform.
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getESN: function(serviceName){			
			serviceName = String(serviceName);
			if(typeof serviceName != 'string') {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			var sDeviceID = null;
			var sTargetLocation = webapis._pluginDef.PL_TV_LOCATION_CODE[webapis._plugin('TV', 'GetTargetLocation')];	

			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			alert("sFirmware ==== "+sFirmware);
			alert("sFirmware.substr(10,4) === "+sFirmware.substr(10,4));
					
			if(serviceName == "WIDEVINE") {
				if(sTargetLocation != "USA") {// 미국 외 국가
					if (sFirmware.substr(10,4) >= '2011') {	// 11년향
						sDeviceID = getESNnumber(serviceName);
					}
					else {	// 10년향
						var nProductType = webapis._plugin('TV', 'GetProductType');
						if (nProductType == webapis._pluginDef.PL_TV_PRODUCT_TYPE_TV) {	
							sDeviceID = getDUID();
						}
						else if (nProductType == webapis._pluginDef.PL_TV_PRODUCT_TYPE_BD){	
							sDeviceID = getESNnumber(serviceName);
						}
						else {
							alert('Device is not TV or BD.');
							throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
						}											
					}	
				}
				else {	// 미국
					if (sFirmware.substr(10,4) >= '2011') {	// 11년향
						sDeviceID = getESNnumber(serviceName);
					}
					else {	// 10년향
						throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
					}
				}
			}
			else {
				sDeviceID = getESNnumber(serviceName);
			}
			alert('getESN() returns ' + sDeviceID);
			return sDeviceID;
			
			function getESNnumber(serviceName) {
				var sESN = null;
				sESN = webapis._plugin("EXTERNALWIDGETINTERFACE", "GetESN", serviceName);
				return sESN;
			}
			
			function getDUID() {
				var nNetworkInterface = webapis._plugin('Network', 'GetActiveType');	// 1: wired, 0: wireless, -1: no active connection
				var sMacAddr = webapis._plugin('Network', 'GetMAC',1);
				var sDUID = webapis._plugin("NNavi", "GetDUID", sMacAddr);	// DUID(Device Unique ID ) of each DTV. 
				
				return sDUID;
			}
		},
		
		/*
		 * Function: getTick
		 * 	Gets tick count.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Number) tick.
		 *
		 * Throws:
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported
		 */
		getTick: function(){
			var retValue = null;
			retValue = webapis._plugin("Time", "GetTick");
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			if (sFirmware.substr(10,4) == '2010') {	// 10년향
				retValue = parseInt(retValue);
			}
			return retValue;
		},
		
		/*
		 * Function: getEpochTime
		 * 	gets epoch time.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Number) time
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError	with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		getEpochTime: function(){
			var retValue = null;
			retValue = webapis._plugin("Time", "GetEpochTime");
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			if (sFirmware.substr(10,4) == '2010') {	// 10년향
				retValue = parseInt(retValue);
			}
			return retValue;
		},
		
		/*
		 * Function: convertEpochToTime
		 * 	converts epoch time to time.
		 *
		 * Parameters:
		 * 	o epoch	- (unsigned long) epoch time to convert.
		 *
		 * Returns:
		 * 	(Date) converted time string
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError	with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		convertEpochToTime: function(epoch){
			epoch = _checkNumberType(epoch);
			
			if(epoch <= 0) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			}			
			var retValue = null;
			retValue = webapis._plugin("Time", "ConvertEpochToLocalTime", epoch);
			if(retValue < 0) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			else {
				if (retValue) {
					var localTime = retValue.split('/');
					var date = new Date(localTime[0], localTime[1]-1, localTime[2], localTime[3], localTime[4], localTime[5]);
					return date;
				}
				else {
					throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
				}
			}
		},
		
		/*
		 * Function: convertTimeToEpoch
		 * 	converts time to epoch time.
		 *
		 * Parameters:
		 * 	o time	- (Date) local time.
		 *
		 * Returns:
		 * 	(unsigned long) converted epoch time.
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError	with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		convertTimeToEpoch: function(time){
			if(typeof time != 'object' || time === null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			
			var retValue = null;
			retValue = parseInt(time.getTime()/1000);
			return retValue;
		},
		
		/*
		 * Function: getTimeZone
		 * 	gets a timezone information.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(TimeZone) timezone. (webapis.tv.info.TimeZone)
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError	with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		getTimeZone: function(){	
			var timezone = webapis._plugin("TV", "GetTimeZone");
			var timezoneOffset = webapis._plugin("TV", "GetTimeZone_Offset");
			var timezoneDst = webapis._plugin("TV", "GetDST");
			
			var retValue = new this.TimeZone(timezone, timezoneOffset, timezoneDst);	
			return retValue;
		},
		
		/*
		 * Class: TimeZone
		 * 
		 * Properties:
		 *	left	- (Number)
		 *	top		- (Number)
		 *	width	- (Number)
		 *	height	- (Number)
		 * 
		 * Function: TimeZone (constructor)
		 * 
		 * Parameters:
		 * 	o timezone	- (Number) timezone
		 * 	o offset	- (Number) offset of the timezone
		 * 	o dst		- (Number) dst of the timezone
		 */
		TimeZone: function(timezone, offset, dst) {
			this.__defineGetter__("timezone", function () {return timezone;});
			this.__defineGetter__("offset", function () {return offset;});
			this.__defineGetter__("dst", function () {return dst;});
		}
	},


   	/*
	 * Namespace: tv.closedcaption
	 * The closedcaption namespace.
	 * This namespace is used for set/get the Closed Caption Property on DTV Platform.
	 * 
	 */
	  closedcaption: {
       /*
         * Enums:
         * 	Profile Index
         * 	
         **/
       PROFILE_CC: 0,
       PROFILE_CC_FCC:1,

       /*Closed Caption Properties. For these Property to set, developer has to set
         PROFILE_CC as Profile Index*/
       CAPTION_FONT_SIZE:0,
	   CAPTION_FONT_STYLE:1,
	   CAPTION_FG_COLOR:2,
	   CAPTION_FG_OPACITY:3,
	   CAPTION_BG_COLOR:4,
	   CAPTION_BG_OPACITY:5,
	   CAPTION_LANGUAGE_TYPE:6,

       /*Closed Caption Edge Properties.For these Property to set, developer has to set
         PROFILE_CC_FCC as Profile Index*/
       CAPTION_EDGE_TYPE:0,
	   CAPTION_EDGE_COLOR:1,

       /*Caption Font Size Property Value*/
		CAPTION_SIZE_SMALL:0,
		CAPTION_SIZE_STANDARD:1,
		CAPTION_SIZE_LARGE:2,
		CAPTION_SIZE_EXTRA_LARGE:3,
		CAPTION_SIZE_DEFAULT:4,

		/*Caption Font Syle Property Value*/
		CAPTION_FONT_DEFAULT:0,
		CAPTION_FONT_STYLE0:1,
		CAPTION_FONT_STYLE1:2,
		CAPTION_FONT_STYLE2:3,
		CAPTION_FONT_STYLE3:4,
		CAPTION_FONT_STYLE4:5,
		CAPTION_FONT_STYLE5:6,
		CAPTION_FONT_STYLE6:7,
		CAPTION_FONT_STYLE7:8,
		CAPTION_FONT_UNDEFINED:9,

		/*Caption FG COLOR, BG COLOR and Edge COLOR
		Property Value*/
		CAPTION_COLOR_DEFAULT:0,
		CAPTION_COLOR_WHITE:1,
		CAPTION_COLOR_BLACK:2,	
		CAPTION_COLOR_RED:3,
		CAPTION_COLOR_GREEN:4,
		CAPTION_COLOR_BLUE:5,
		CAPTION_COLOR_YELLOW:6,
		CAPTION_COLOR_MAGENTA:7,
		CAPTION_COLOR_CYAN:8,

		/*Caption FG and BG OPACITY
		 Property Value*/
		CAPTION_OPACITY_SOLID:0,
		CAPTION_OPACITY_FLASH:1,
		CAPTION_OPACITY_TRANSLUCENT:2,
		CAPTION_OPACITY_TRANSPARENT:3,
		CAPTION_OPACITY_DEFAULT:4,		

		/*CAPTION LANGUAGE TYPE Property
		 Value*/
		CAPTION_LANGUAGE_AUTO:0,
		CAPTION_LANGUAGE_JPN:1,
		CAPTION_LANGUAGE_ENG:2,
		CAPTION_LANGUAGE_GER:3,
		CAPTION_LANGUAGE_FRE:4,
		CAPTION_LANGUAGE_ITA:5,
		CAPTION_LANGUAGE_RUS:6,
		CAPTION_LANGUAGE_CHS:7,
		CAPTION_LANGUAGE_KOR:8,
		CAPTION_LANGUAGE_SPA:9,
		CAPTION_LANGUAGE_UNDEFINED:10,

        /*CAPTION EDGE TYPE Property
		 Value*/
        CAPTION_EDGE_NONE:0,
	    CAPTION_EDGE_RAISED:1,
	    CAPTION_EDGE_DEPRESSED:2,
	    CAPTION_EDGE_UNIFORM:3,
	    CAPTION_EDGE_DROP_SHADOWED:4,

        _tvPlugin : null,
        ManagerCallBack : null,

        /*
		* Function: registerCaptionChangeCallback
		* register a call back function to receive caption change event.
         	* If null is passed as callback function type, then caption change
         	* event cant be received from callback function.
		* Parameters:
		* 	o callback function that has prototype given as "CallbackFunction()"
		* Returns:
		*/
         registerCaptionChangeCallback: function(Callback){

            alert("Entering [registerCaptionChangeCallback]");

            this._tvPlugin = webapis._plugin("TV"); 

           /* webapis._plugin(_tvPlugin, "SetEvent", PL_TV_EVENT_CAPTION_CHANGED);*/

            if (typeof Callback == 'function')
			{
                alert("Assigning Manager Call Back");
				this.ManagerCallBack = Callback;
			}

            this._tvPlugin.OnEvent = _onTVCaptionChangedEvent;
          },
           
         /*Private
		 * Function: Call Back Function which calls when Caption is changed
         * from TV Menu.
		 * Returns:
		 */                 
         CaptionChangedCallback : function() {
               
		    if (this.ManagerCallBack != null)
		    {
			    	alert("[SetCaptionChangeInformation] Calling Call Back");
		             this.ManagerCallBack();
			}
	     },
		/*
		 * Function: setClosedCaptionOption
		 * 	Set the specified Closed Caption with given Index, Property, Value
		 *
		 * Parameters:
		 * 	o ccparamindex - Closed Caption Property Index(PROFILE_CC, PROFILE_CC_FCC)
		 * 	o ccParam	-    Closed Caption Property
		 *  o ccValue	-    Closed Caption Property Value
		 * Returns:
		 * 	true if the operation is successful, otherwise false  
		 */
        setClosedCaptionOption: function(ccparamindex, ccParam, ccValue)
		{
			alert("Entering [SetCloseCaptionOption]");
			
			if (webapis._pluginDef.PLR_TRUE == webapis._plugin('TV', 'SetCloseCaptionOption', ccparamindex, ccParam, ccValue))
			{
				 alert('[Set CC Option] Success');
				 return webapis._pluginDef.PLR_TRUE;
			}
			else
			{
				alert('[Set CC Option] Fails!!');
				 return webapis._pluginDef.PLR_FALSE;
			}
						
		},
        /*
		 * Function: getClosedCaptionOption
		 * 	Get the specified Closed Caption Parameter Value with given index, Property.
		 *
		 * Parameters:
		 * 	o ccparamindex		- Closed Caption Property Index(PROFILE_CC, PROFILE_CC_FCC) 
		 *  o ccParam		    - Closed Caption Property 
		 * Returns:
		 * 	the Parameter Value if the operation is successful, otherwise -1 
		 */
        getClosedCaptionOption: function(ccparamindex, ccParam)
		{
			alert("GetCloseOption");
			 
			 var retval = webapis._plugin('TV', 'GetCloseCaptionOption',ccparamindex, ccParam);
			 if (retval < 0)			 
			 {
				//Return -1
				alert('[Get CC Option] Fails!!');
				retval = webapis._pluginDef.PLR_FAIL;
			 }
			 return retval;
		},
    },

	/*
	 * Namespace: tv.channel
	 * 	The channel namespace.
	 *
	 * Available From:
	 * 	0.1
	 */
	channel: {
		/*
         * Enums:
         * 	Contants for TV/BD Product Type
         * 	NAVIGATOR_MODE_ALL      - 0
		 * 	NAVIGATOR_MODE_DIGITAL  - 1
		 * 	NAVIGATOR_MODE_ANALOG   - 2
		 * 	NAVIGATOR_MODE_FAVORITE - 3
         */
		
		NAVIGATOR_MODE_ALL:			0,
		NAVIGATOR_MODE_DIGITAL:		1,
		NAVIGATOR_MODE_ANALOG:		2,
		NAVIGATOR_MODE_FAVORITE:	3,

		/*
		 * Function: tune
		 * 	tune the specified channel
		 *
		 * Parameters:
		 * 	o tuneOption		- (Object)
		 * 	o successCallback	- (Function)
		 * 	o errorCallback		- (Function(code:Number, message:String))		 
		 *
		 * Returns:
		 * 	true if the operation is successful, otherwise false  
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		tune: function(tuneOption, successCallback, errorCallback, windowID){			
			var ID = _checkNumberType(windowID);			
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function') || (typeof errorCallback != 'function')) {				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			
			if(typeof tuneOption != 'object' || tuneOption === null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			
			if((ID > 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			} else if((ID < 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			} 
			
			// tuneOption Type Conversion 20120704
			
			tuneOption.ptc = _checkNumberType(tuneOption.ptc);
			tuneOption.major = _checkNumberType(tuneOption.major);
			tuneOption.minor = _checkNumberType(tuneOption.minor);			
			tuneOption.sourceID = _checkNumberType(tuneOption.sourceID);
			tuneOption.programNumber = _checkNumberType(tuneOption.programNumber);
			tuneOption.transportStreamID = _checkNumberType(tuneOption.transportStreamID);
			tuneOption.originalNetworkID = _checkNumberType(tuneOption.originalNetworkID);			
			
			// tuneOption Type Checking
	        if (
	        	/*
	            (tuneOption.ptc != null) ||
	            (tuneOption.major != null) ||
	            (tuneOption.minor != null) ||
	            (tuneOption.sourceID != null) ||
	            (tuneOption.programNumber != null) ||
	            (tuneOption.transportStreamID != null) ||
	            (tuneOption.originalNetworkID != null) ||
	            */
	            (typeof tuneOption.tunecallback != "object" && tuneOption.tunecallback != null)	            
	        ) {
	            if(typeof errorCallback == 'function') {
	            	errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR));
	            }
	            return;
	        }			
			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				if(typeof errorCallback == 'function') {
					errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			
			// 2010(Valencia) 일 때 Not Supported
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			if (sFirmware.substr(10,4) == '2010') {	// 10년향				
				errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				return;
			}		
			
			var retValue = null;
			var windowPlugin = webapis._plugin("WINDOW");
			if(!windowPlugin) {				
				if(typeof errorCallback == 'function') {					
			 		errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
		//	windowPlugin.style.visibility = "hidden";
			if(tuneOption.major) {				
				retValue = webapis._plugin(windowPlugin, "SetChannel", tuneOption.major, tuneOption.minor);
			}
			else if(tuneOption.ptc) {				
				retValue = webapis._plugin(windowPlugin, "SetChannel_PTC", tuneOption.ptc);
			}
			else {				
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			}			
			if(retValue <= 0 || retValue == null) {				
				if(typeof errorCallback == 'function') {					
					errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NO_MODIFICATION_ALLOWED_ERR));
				}
				return;
			} 
			else {	// retValue > 0				
				if(typeof successCallback == 'function') {					
					successCallback();
				}
				return;
			}
		},

		/*
		 * Function: tuneUp
		 * 	tune the previous channel
		 *
		 * Parameters:		  	
		 *  o successCallback	- (Function)
		 * 	o errorCallback		- (Function(code:Number, message:String))
		 *  o tunemode	    	- (Number)
		 *
		 * Returns:
		 * 	true if the operation is successful, otherwise false       
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		tuneUp: function(successCallback, errorCallback, tuneMode, windowID){						
			var ID = _checkNumberType(windowID);
			var Mode = _checkNumberType(tuneMode);
						
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function') || (typeof errorCallback != 'function')) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}	
			/*
			if(bCheckWindowIDType == false && windowID !== undefined) {				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(bCheckTuneModeType == false && tuneMode !== undefined) {				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			*/
			if((Mode < 0 || Mode > 3) ) {				
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			}			
			
			if((ID > 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			} else if((ID < 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			} 			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			
			// 2010(Valencia) 일 때 Not Supported
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			if (sFirmware.substr(10,4) == '2010') {	// 10년향
				errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				return;
			}
			
			var retValue = null;
			var windowPlugin = webapis._plugin("WINDOW");
			if(!windowPlugin) {
				if(typeof errorCallback == 'function') {
				 	errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			//windowPlugin.style.visibility = "hidden";			
			retValue = webapis._plugin(windowPlugin, "SetChannel_Seek", webapis._pluginDef.PL_WINDOW_SEEK_UP, Mode);			
			if(retValue < 0) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			else {
				if(typeof successCallback == 'function') {
					successCallback();
				}
				return;
			}	
		},

		/*
		 * Function: tuneDown
		 * 	tune the previous channel
		 *
		 * Parameters:
		 *  o successCallback	- (Function)
		 * 	o errorCallback		- (Function(code:Number, message:String))
		 *  o tunemode	    	- (Number)
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		tuneDown: function(successCallback, errorCallback, tuneMode, windowID){
			var ID = _checkNumberType(windowID);
			var Mode = _checkNumberType(tuneMode);
			
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function') || (typeof errorCallback != 'function')) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
						
			if((Mode < 0 || Mode > 3) ) {				
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));					
				}
				return;
			}			
			
			if((ID > 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			} else if((ID < 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			} 
			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			
			// 2010(Valencia) 일 때 Not Supported
			var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
			if (sFirmware.substr(10,4) == '2010') {	// 10년향
				errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				return;
			}
			
			var retValue = null;			
			var windowPlugin = webapis._plugin("WINDOW");
			if(!windowPlugin) {
			 	if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			//windowPlugin.style.visibility = "hidden";
			
			retValue = webapis._plugin(windowPlugin, "SetChannel_Seek", webapis._pluginDef.PL_WINDOW_SEEK_DOWN, Mode);
			
			if(retValue < 0) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			else {
				if(typeof successCallback == 'function') {
					successCallback();
				}
				return;
			}
		},

		/*
		 * Function: getChannelList
		 * 	get channel list
		 *
		 * Parameters:
		 * 	o successcallback	- (Function)
		 * 	o errorcallback		- (Function(code:Number, message:String))
		 * 	o mode				- (Number)
		 * 	o nStart			- (Number)
		 * 	o number			- (Number)
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		getChannelList: function(successCallback, errorCallback, mode, nStart, number){
			var nMode = _checkNumberType(mode);
			var Start = _checkNumberType(nStart);
			var num = _checkNumberType(number);
									
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback != null)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}	
						
			if((Start < 0 || num < 0) ) {				
				if(typeof errorCallback == 'function') {					
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			}
			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			
			//var retValue;	
			var type, ptc, major, minor, programNumber, transportStreamID, originalNetworkID, serviceName, channelName = null;
			 	
			var windowPlugin = webapis._plugin("WINDOW");
			if(!windowPlugin) {
			 	if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			//windowPlugin.style.visibility = "hidden";		
			
			var FindChannel = webapis._plugin(windowPlugin, "FindChannel", webapis.tv.channel.NAVIGATOR_MODE_ALL, nMode);
			alert('FindChannel : '+FindChannel);
			
			var channelSize = webapis._plugin(windowPlugin, "GetChannel_Size");
			alert("ChannelSize : " +channelSize);
			
			var channelNumber = null;
			if (channelSize > num && num != null) {
				channelNumber = num;
			}
			else {
				channelNumber = channelSize;
			}
			
			var channelList = new Array();
			for(var i=0+Start ; i<channelNumber ; ++i)
			{	
				type = webapis._plugin(windowPlugin, "GetChannel_Type", i);	
				ptc = webapis._plugin(windowPlugin, "GetChannel_PTC", i);	
				major = webapis._plugin(windowPlugin, "GetChannel_Major", i);
				minor = webapis._plugin(windowPlugin, "GetChannel_Minor", i);	
				programNumber = webapis._plugin(windowPlugin, "GetChannel_ProgramNumber", i);	
				transportStreamID = webapis._plugin(windowPlugin, "GetChannel_TransportStreamID", i);	
				originalNetworkID = webapis._plugin(windowPlugin, "GetChannel_OriginNetID", i);	
				serviceName = webapis._plugin(windowPlugin, "GetChannel_ServiceName", i);	
				channelName = webapis._plugin(windowPlugin, "GetChannel_Name", i);
				
				channelList.push(new this.ChannelInfo(ptc, major, minor, null, null, programNumber, transportStreamID, originalNetworkID, serviceName, channelName));
				//alert("channelList channelName " + i + " == " + channelList[i].channelName);
			}		
					
			if(channelSize < 0) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
			}
			else {
				if(typeof successCallback == 'function') {
					successCallback(channelList);
				}
			}
			return;
		},

		/*
		 * Function: getCurrentChannel
		 * 	gets the current channel information
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(ChannelInfo) current channel info.
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		getCurrentChannel: function(windowID){
			var ID = _checkNumberType(windowID);			
			
			if((ID > 0)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}else if((ID < 0)) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
				
			var windowPlugin = webapis._plugin("WINDOW");
			if(!windowPlugin) {
			 	throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			//windowPlugin.style.visibility = "hidden";
			
			//var currentId = webapis._plugin(windowPlugin, "GetCurrentChannel_Id");
			var currentPtc = webapis._plugin(windowPlugin, "GetCurrentChannel_PTC");
 			var currentMajor = webapis._plugin(windowPlugin, "GetCurrentChannel_Major");
 			var currentMinor = webapis._plugin(windowPlugin, "GetCurrentChannel_Minor");
 			var currentLcn = webapis._plugin(windowPlugin, "GetCurrentChannel_Major"); //120913 박일우K 협의
 			//var currentLcn = null; 
 			//var currentLcn = webapis._plugin(windowPlugin, "GetChannel_Lcn");
 			var currentSourceID = webapis._plugin(windowPlugin, "GetCurrentChannel_ProgramNumber"); //120913 박일우K 협의
 			//var currentSourceID = null;
 			//var currentSourceID = webapis._plugin(windowPlugin, "GetChannel_SourceID");
 			var currentProgramNumber = webapis._plugin(windowPlugin, "GetCurrentChannel_ProgramNumber");
 			var currentTransportStreamID = webapis._plugin(windowPlugin, "GetCurrentChannel_TransportStreamID");
 			var currentOriginalNetworkID = webapis._plugin(windowPlugin, "GetCurrentChannel_OriginNetID");
 			var currentServiceName = webapis._plugin(windowPlugin, "GetCurrentChannel_ServiceName");
 			var currentChannelName = webapis._plugin(windowPlugin, "GetCurrentChannel_Name");
			
			var currentChannelInfo = new this.ChannelInfo(currentPtc, currentMajor, currentMinor, currentLcn, currentSourceID, currentProgramNumber, currentTransportStreamID, currentOriginalNetworkID, currentServiceName, currentChannelName);			
			return currentChannelInfo;
		},

		/*
		 * Function: findChannel
		 * 	gets the specified channel information
		 *
		 * Parameters:
		 * 	o major				- (Number)
         * 	o minor				- (Number)
		 * 	o successCallback	- (Function)
		 * 	o errorCallback		- (Function(code:Number, message:String))
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		findChannel: function(major, minor, successCallback, errorCallback){
			major = _checkNumberType(major);
			minor = _checkNumberType(minor);
			
			if((typeof successCallback != 'function') || (typeof errorCallback != 'function')) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				return;
			}
			
			var windowPlugin = webapis._plugin("WINDOW");
			if(!windowPlugin) {
			 	errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				return;
			}
			
			var retValue = null;	
			var type, ptc, ch_major, ch_minor, programNumber, transportStreamID, originalNetworkID, serviceName, channelName = null;
				
			retValue = webapis._plugin(windowPlugin, "FindChannel", webapis.tv.channel.NAVIGATOR_MODE_ALL, webapis._pluginDef.PL_WINDOW_TV_MODE_AIR);
			
			var channelSize = webapis._plugin(windowPlugin, "GetChannel_Size");
			alert("ChannelSize ===== " +channelSize);
			
			var channelInfoArray = new Array();
			for(var i=0 ; i<channelSize ; ++i)
			{
				ch_major = webapis._plugin(windowPlugin, "GetChannel_Major", i);		
				ch_minor = webapis._plugin(windowPlugin, "GetChannel_Minor", i);
				if (ch_major == major && ch_minor == minor) {						
					type = webapis._plugin(windowPlugin, "GetChannel_Type", i);		
					ptc = webapis._plugin(windowPlugin, "GetChannel_PTC", i);															
					programNumber = webapis._plugin(windowPlugin, "GetChannel_ProgramNumber", i);	
					transportStreamID = webapis._plugin(windowPlugin, "GetChannel_TransportStreamID", i);	
					originalNetworkID = webapis._plugin(windowPlugin, "GetChannel_OriginNetID", i);	
					serviceName = webapis._plugin(windowPlugin, "GetChannel_ServiceName", i);	
					channelName = webapis._plugin(windowPlugin, "GetChannel_Name", i);
					
					channelInfoArray.push(new this.ChannelInfo(ptc, major, minor, null, null, programNumber, transportStreamID, originalNetworkID, serviceName, channelName));
					//alert("channelInfoArray channelName " + i + " == " + channelInfoArray[i].channelName);
				}
			}		
			
			if(retValue <= 0) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			else {
				if(typeof successCallback == 'function') {
					successCallback(channelInfoArray);
				}
				return;
			}	
		},

		/*
		 * Function: getCurrentProgram
		 * 	gets the current program of tuned channel
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(ProgramInfo).
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		getCurrentProgram: function(windowID){	// returns title of current program
			var ID = _checkNumberType(windowID);						
			var bCheckType = isFinite(ID);			 	
			
			if((ID > 0)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}else if((ID < 0)) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
			
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			
			var currentTitle = webapis._plugin("TV", "GetPresentProgram_Title");
			var currentDuration = webapis._plugin("TV", "GetPresentProgram_Duration");
			var epochStartTime = webapis._plugin("TV", "GetPresentProgram_StartTime");	
			var currentStartTime = webapis._plugin("Time", "ConvertEpochToLocalTime", epochStartTime);	
		
			/* //추가
			var currentDetailedDescription = webapis._plugin();
			var currentLanguage = webapis._plugin();
			var currentRating = webapis._plugin();	*/	
			
			var currentProgramInfo = new this.ProgramInfo(currentTitle, currentStartTime, currentDuration, null, null, null);					
			return currentProgramInfo;
		},

		/*
		 * Function: getProgramList
		 * 	gets the current program of tuned channel
		 *
		 * Parameters:
		 * 	o channelInfo		- (ChannelInfo)
		 * 	o startTime			- (Number) 
		 * 	o successCallback	- (ProgramListSuccuessCallback)
		 * 	o errorCallback		- (Function(code:Number, message:String))		 
		 * 	o number			- (Number)
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case
		 */
		getProgramList: function(channelInfo, startTime, successCallback, errorCallback, duration){
			startTime = _checkNumberType(startTime);
			var nDuration = _checkNumberType(duration);						
				
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback != null)) {				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
		 
			if(typeof channelInfo != 'object' || channelInfo === null) {				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
						
			if((startTime < 0 || nDuration < 0)) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			}
			
			// channelInfo Type Conversion 20120704
			var iserviceName = String(channelInfo.serviceName);
			var ichannelName = String(channelInfo.channelName); 
			var iptc = _checkNumberType(channelInfo.ptc);
			var imajor = _checkNumberType(channelInfo.major);
			var iminor = _checkNumberType(channelInfo.minor);
			var ilcn = _checkNumberType(channelInfo.lcn);
			var isourceID = _checkNumberType(channelInfo.sourceID);
			var iprogramNumber = _checkNumberType(channelInfo.programNumber);
			var itransportStreamID = _checkNumberType(channelInfo.transportStreamID);
			var ioriginalNetworkID = _checkNumberType(channelInfo.originalNetworkID);
			/*
			var checkPtcType = isFinite(iptc);
			var checkMajorType = isFinite(imajor);
			var checkMinorType = isFinite(iminor);
			var checkLcnType = isFinite(ilcn);
			var checkSourceIDType = isFinite(isourceID);
			var checkProgramNumberType = isFinite(iprogramNumber);
			var checkTransportStreamIDType = isFinite(itransportStreamID);
			var checkOriginalNetworkIDType = isFinite(ioriginalNetworkID);
			*/
			
			
			// channelInfo Type Checking
	        if (
	        	/*
	            (channelInfo.ptc != null) ||
	            (channelInfo.major != null) ||
	            (channelInfo.minor != null) ||
	            (channelInfo.lcn != null) ||
	            (channelInfo.sourceID != null) ||
	            (channelInfo.programNumber != null) ||
	            (channelInfo.transportStreamID != null) ||
	            (channelInfo.originalNetworkID != null) ||
	            */
	            (typeof channelInfo.serviceName != "string" && channelInfo.serviceName != null) ||
	            (typeof channelInfo.channelName != "string" && channelInfo.channelName != null)
	        ) {
	            if(typeof errorCallback == 'function') {	            	
	            	errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR));
	            }
	            return;
	        }
				
			// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				if(typeof errorCallback == 'function') {
					errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			
			var retValue = null;
			var title, start_Time, duration, end_Time;
			
			var epochTime = webapis._plugin("Time", "GetEpochTime");
			retValue = webapis._plugin("TV", "GetProgramList", epochTime, nDuration);	
			var listSize = webapis._plugin("TV", "GetProgramList_Size");					
			alert("listSize : " + listSize);
			
			var programList = new Array();
			for(var i = 0; i < listSize; ++i) {
				title = webapis._plugin("TV", "GetProgram_Title", i);
				start_Time = webapis._plugin("TV", "GetProgram_StartTime", i);
				duration = webapis._plugin("TV", "GetProgram_Duration", i);
				end_Time = webapis._plugin("TV", "GetProgram_EndTime", i);
				if (start_Time > startTime) {
					programList.push(new this.ProgramInfo(title, start_Time, duration, null, null, null));
					//alert("programList Title " + i + " == " + programList[i].title);
				}				
			}
			
			if(retValue < 0) {
				if(typeof errorCallback == 'function') {
					errorCallback(new ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));	
				}
			}
			else {
				if(typeof successCallback == 'function') {
					successCallback(programList);
				}
			}
			return;
		},
		
		 /**
         * /brief	Gets the number of avaiable tuner. 
         * \param	none
         * \return	the number of avaiable tuner
		 * \throw	WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */ 
        getNumOfAvailableTuner: function () {
        	// BD 일때 Not Supported
			var nProductType = webapis._plugin('TV', 'GetProductType');
			alert("nProductType : " + nProductType);
			if (nProductType == 2) { //BD
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}
			
        	return 1;	// 1개 지원
        },
        		
		/*
		 * Class: TuneCallback 	
		 * 
		 * Properties:
		 * 	onnosignal	- (Function)
		 * 	onclosedcaptionfound	- (Function)
		 *	onresolutionchanged	- (Function(width:Number, height:Number))
		 * 	onaudiomodechanged	- (Function(mode:Number))
		 * 
		 * Function: TuneCallback (constructor)
		 * 
		 * Parameters:
		 * 	o nosignal		- (Function) callback to be called when no-signal occured during the operation
		 * 	o ccfound		- (Function) callback to be called when closed caption is found during the operation
		 * 	o reschanged	- (Function) callback to be called when resolution is changed during the operation
		 * 	o audiomodechanged	- (Function) callback to be called when audio mode is changed during the operation
		 */
		TuneCallback: function(nosignal, ccfound, reschanged, audiomodechanged) {
			this.onsuccess = null;
			this.onnosignal = null;
			this.onresolutionchanged = null;
			this.onaudiomodechanged = null;
		},
		
		/*
		 * Class: ChannelListSuccuessCallback 	
		 * 
		 * Properties:
		 * 	onsuccess	- (Function(Array<ChannelInfo>)
		 * 
		 * Function: ChannelListSuccuessCallback (constructor)
		 * 
		 * Parameters:
		 * 	o nosignal		- (Function) callback to be called when no-signal occured during the operation
		 * 	o ccfound		- (Function) callback to be called when closed caption is found during the operation
		 * 	o reschanged	- (Function) callback to be called when resolution is changed during the operation
		 * 	o audiomodechanged	- (Function) callback to be called when audio mode is changed during the operation
		 */
		ChannelListSuccuessCallback: function(channelList) {
			this.onsuccess = channelList;
		},
		
		/*
		 * Class: ProgramListSuccuessCallback 	
		 * 
		 * Properties:
		 * 	onsuccess	- (Function(Array<ProgramInfo>)
		 * 
		 * Function: ProgramListSuccuessCallback (constructor)
		 * 
		 * Parameters:
		 * 	o onsuccess		- (Function) callback to be called when the operation is succeded
		 */
		ProgramListSuccuessCallback: function(onsuccess) {
			this.onsuccess = onsuccess;
		},
		
		/*
		 * Class: TuneOption 	
		 * 
		 * Properties:
		 * 	ptc				- (Number)
         * 	major				- (Number)
         * 	minor				- (Number)
         * 	sourceID			- (Number)
         * 	programNumber		- (Number)
         * 	transportStreamID	- (Number)
         * 	originalNetworkID	- (Number)
         * 	tunecallback		- (TuneCallback)
		 * 
		 * Function: TuneOption (constructor)
		 * 
		 * Parameters:
		 * 	o ptc				- (Number)
         * 	o major				- (Number)
         * 	o minor				- (Number)
         * 	o sourceID			- (Number)
         * 	o programNumber		- (Number)
         * 	o transportStreamID	- (Number)
         * 	o originalNetworkID	- (Number)
         * 	o tunecallback		- (TuneCallback)
		 */
		TuneOption: function(ptc, major, minor, sourceID, programNumber, transportStreamID, originalNetworkID, tunecallback) {			
			this.ptc = ptc;
			this.major = major;
			this.minor = minor;
			this.sourceID = sourceID;
			this.programNumber = programNumber;
			this.transportStreamID = transportStreamID;
			this.originalNetworkID = originalNetworkID;
			this.tunecallback = tunecallback;			
		},
		
		/*
		 * Class: ChannelInfo 	
		 * 
		 * Properties:		 
		 * 	ptc					- (Number)
         * 	major				- (Number)
         * 	minor				- (Number)
         * 	lcn					- (Number)
         * 	sourceID			- (Number)
         * 	programNumber		- (Number)
         * 	transportStreamID	- (Number)
         * 	originalNetworkID	- (Number)
         * 	serviceName			- (String)
         * 	channelName			- (String)
		 * 
		 * Function: ChannelInfo  (constructor)
		 * 
		 * Parameters:		 
		 * 	o ptc				- (Number)
         * 	o major				- (Number)
         * 	o minor				- (Number)
         * 	o lcn				- (Number)
         * 	o sourceID			- (Number)
         * 	o programNumber		- (Number)
         * 	o transportStreamID	- (Number)
         * 	o originalNetworkID	- (Number)
         * 	o serviceName		- (String)
         * 	o channelName		- (String)
		 */
		ChannelInfo: function(ptc, major, minor, lcn, sourceID, programNumber, transportStreamID, originalNetworkID, serviceName, channelName) {
			this.ptc = ptc;
			this.major = major;
			this.minor = minor;
			this.__defineGetter__("lcn", function () {return lcn;});
			this.__defineGetter__("sourceID", function () {return sourceID;});
			this.__defineGetter__("programNumber", function () {return programNumber});
			this.__defineGetter__("transportStreamID", function () {return transportStreamID;});
			this.__defineGetter__("originalNetworkID", function () {return originalNetworkID;});
			this.__defineGetter__("serviceName", function () {return serviceName;});
			this.__defineGetter__("channelName", function () {return channelName;});
		},
		
		/*
		 * Class: ProgramInfo 	
		 * 
		 * Properties:
		 * 	title				- (String)
		 * 	startTime			- (Number)
         * 	duration			- (Number)
         * 	detailedDescription	- (String)
         * 	language			- (String)
         * 	rating				- (String)
		 * 
		 * Function: ProgramInfo  (constructor)
		 * 
		 * Parameters:
		 * 	o title					- (String)
		 * 	o startTime				- (Number)
         * 	o duration				- (Number)
         * 	o detailedDescription	- (String)
         * 	o language				- (String)
         * 	o rating				- (String)
		 */
		ProgramInfo: function(title, startTime, duration, detailedDescription, language, rating) {
			this.__defineGetter__("title", function () {return title;});
			this.__defineGetter__("startTime", function () {return startTime;});
			this.__defineGetter__("duration", function () {return duration;});
			this.__defineGetter__("detailedDescription", function () {return detailedDescription;});
			this.__defineGetter__("language", function () {return language;});
			this.__defineGetter__("rating", function () {return rating;});
			
			/*this.title = title;
			this.startTime = startTime;
			this.duration = duration;
			this.detailedDescription = detailedDescription;
			this.language = language;
			this.rating = rating;*/
		}
    },
	
	
	/*
	 * Namespace: tv.window
	 * 	TV window module. The TV window module that provide the control of tv source relative functions of DTV platform.
	 *
	 * Available From:
	 * 	0.1
	 */
	window: {
		/*
	 * Enums:
	 * 	Contants for TV/BD Product Type
	 * 	SOURCE_MODE_TV		- 0
	 * 	SOURCE_MODE_AV		- 1
	 * 	SOURCE_MODE_SVIDEO	- 2
	 * 	SOURCE_MODE_COMP	- 3
	 * 	SOURCE_MODE_PC		- 4
	 * 	SOURCE_MODE_HDMI	- 5
	 * 	SOURCE_MODE_SCART	- 6
	 * 	SOURCE_MODE_DVI		- 7
	 * 	SOURCE_MODE_MEDIA	- 8
	 * 	SOURCE_MODE_IPTV	- 9
	 */
		SOURCE_MODE_TV: 0,
		SOURCE_MODE_AV: 1,
		SOURCE_MODE_SVIDEO: 2,
		SOURCE_MODE_COMP: 3,
		SOURCE_MODE_PC: 4,
		SOURCE_MODE_HDMI: 5,
		SOURCE_MODE_SCART: 6,
		SOURCE_MODE_DVI: 7,
		SOURCE_MODE_MEDIA: 8,
		SOURCE_MODE_IPTV: 9,
		SOURCE_MODE_RVU:10,//20120920 박일우K 협의 후 추가
		SOURCE_MODE_RUI:11,
		SOURCE_MODE_ISP:12,
		
		/*
		 * Function: getAvailableWindow
		 * 	gets the number of available sub-window. If the operation is failed due to any other error, the errorCallback is launched with the appropriate error code:
		 * 
		 * 	INVALID_VALUES_ERR	- If the input attributes do not contain a valid value
		 * 	NOT_SUPPORTED_ERR	- If this feature is not supported.
		 * 	SECURITY_ERR		- If the operation is not allowed. 
		 * 	UNKNOWN_ERR			- In any other error case.
		 *
		 * Parameters:
		 * 	o successCallback	- (AvailableWindowListCallback )
		 * 	o errorCallback		- (Function(code:Number, message:String))
		 *
		 * Returns:
		 * 	nothing.	Pending(async) Operation
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type
		 */
		getAvailableWindow: function(successCallback, errorCallback){			
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback !== undefined)) {
				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
				
			var retValue = 0;			
			if(retValue == -1) {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
			}
			else {
				if(typeof successCallback == 'function') {
					successCallback(retValue);
				}
			}	
			return;	
		},
		
		/*
		 * Function: setSource
		 * 	change the tv source. (TV or PC or DVI ...)
		 *
		 * Parameters:
		 * 	o sourceID	 	- (Number)
		 * 	o sourceNum		- (Number)
		 *
		 * Returns:
		 * 	(Boolean) true if the operation is successful, otherwise false.
		 *
		 * Throws:
		 * 	o WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type 
		 * 	o WebAPIError	with error code INVALID_VALUES_ERR If the input attributes do not contain a valid value
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		_tvPlugin : null,
		
		setSource: function(sourceInfo, successCallback, errorCallback, windowID){			
			var ID = _checkNumberType(windowID);										
			
			if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
				return;
			}
			if((typeof successCallback != 'function' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback !== undefined)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			} 
			if(successCallback == null && errorCallback == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
		 
			if(typeof sourceInfo != 'object') {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			
			if((ID > 0 || ID < 0)) {
				if(typeof errorCallback == 'function') {					
					errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
				}
				return;
			}
			
			var _THIS_ = this;
			
			SourceChangedSuccessCallback = function() {
				webapis._plugin(_THIS_._tvPlugin, "UnsetEvent", PL_TV_EVENT_SOURCE_CHANGED);
				webapis._plugin(_THIS_._tvPlugin, "UnsetEvent", PL_TV_EVENT_SOURCE_CONNECTED);
				_THIS_._tvPlugin = null;
				successCallback(sourceInfo, windowID);
			};
								
			var retValue = null;
			
			var windowPlugin = webapis._plugin("WINDOW");
			//windowPlugin.style.visibility = "hidden";
			var currentSource = webapis._plugin(windowPlugin, "GetSource");	
	
			_THIS_._tvPlugin = webapis._plugin("TV");
			
			webapis._plugin(_THIS_._tvPlugin, "SetEvent", PL_TV_EVENT_SOURCE_CHANGED);
			webapis._plugin(_THIS_._tvPlugin, "SetEvent", PL_TV_EVENT_SOURCE_CONNECTED);
			
			_THIS_._tvPlugin.OnEvent = _onTVPluginEvent;

			if(!this._sourcePLMap) {
				this._sourcePLMap = {};
				this._sourcePLMap[this.SOURCE_MODE_TV] = "PL_WINDOW_SOURCE_TV";
				this._sourcePLMap[this.SOURCE_MODE_AV] = "PL_WINDOW_SOURCE_AV";
				this._sourcePLMap[this.SOURCE_MODE_SVIDEO] = "PL_WINDOW_SOURCE_SVIDEO";
				this._sourcePLMap[this.SOURCE_MODE_COMP] = "PL_WINDOW_SOURCE_COMP";
				this._sourcePLMap[this.SOURCE_MODE_PC] = "PL_WINDOW_SOURCE_PC";
				this._sourcePLMap[this.SOURCE_MODE_HDMI] = "PL_WINDOW_SOURCE_HDMI";
				this._sourcePLMap[this.SOURCE_MODE_SCART] = "PL_WINDOW_SOURCE_SCART";
				this._sourcePLMap[this.SOURCE_MODE_DVI] = "PL_WINDOW_SOURCE_DVI";
				this._sourcePLMap[this.SOURCE_MODE_MEDIA] = "PL_WINDOW_SOURCE_MEDIA";
				this._sourcePLMap[this.SOURCE_MODE_IPTV] = "PL_WINDOW_SOURCE_IPTV";
				this._sourcePLMap[this.SOURCE_MODE_RVU] = "PL_WINDOW_SOURCE_RVU";
				this._sourcePLMap[this.SOURCE_MODE_RUI] = "PL_WINDOW_SOURCE_RUI";
				this._sourcePLMap[this.SOURCE_MODE_ISP] = "PL_WINDOW_SOURCE_ISP";		
			}	
				
			if(eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]) || eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]) == 0) {				
				if(currentSource == eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type])) {
					if(typeof successCallback == 'function') {						
						successCallback(sourceInfo, windowID);
					}
					return;
				}
				else {
					retValue = webapis._plugin(windowPlugin, "SetSource", eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]));
				}
			}
			else if(eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]+sourceInfo.number) || eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]+sourceInfo.number) == 0) {				
				if(currentSource == eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]+sourceInfo.number)) {
					if(typeof successCallback == 'function') {						
						successCallback(sourceInfo, windowID);
					}
					return;
				}
				else {
					retValue = webapis._plugin(windowPlugin, "SetSource", eval("webapis._pluginDef."+this._sourcePLMap[sourceInfo.type]+sourceInfo.number));
				}
			}
			else {
				if(typeof errorCallback == 'function') {
					errorCallback(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
				}
				return;
			}  
			
	  //	successCallback(sourceInfo);
			
		   /*if(retValue < 0) {	// TV source 일때 또  TV source로 바꾸면 return값 -1
				errorCallback(new WebAPIError(WebAPIDOMErrorCode.NOT_SUPPORTED_ERR));
				return;
			}
			else {
				successCallback(sourceInfo);
				return;
			}*/
		},
		_sourcePLMap: null,
		
		/*
		 * Function: getSource
		 * 	Returns a current source. (TV or PC or DVI ...)
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Number) current source type which is one of Window Source Type Constants.
		 *
		 * Throws:
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		getSource: function(windowID){
			var ID = _checkNumberType(windowID);										
			
			if((ID > 0)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}else if((ID < 0)) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 

			if(!this._sourceAPIMap) {
			    this._sourceAPIMap = {};
			    this._sourceAPIMap["PL_WINDOW_SOURCE_TV"] = this.SOURCE_MODE_TV;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_AV"] = this.SOURCE_MODE_AV;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_SVIDEO"] = this.SOURCE_MODE_SVIDEO;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_COMP"] = this.SOURCE_MODE_COMP;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_PC"] = this.SOURCE_MODE_PC;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_HDMI"] = this.SOURCE_MODE_HDMI;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_SCART"] = this.SOURCE_MODE_SCART;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_DVI"] = this.SOURCE_MODE_DVI;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_MEDIA"] = this.SOURCE_MODE_MEDIA;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_IPTV"] = this.SOURCE_MODE_IPTV;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_RVU"] = this.SOURCE_MODE_RVU;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_RUI"] = this.SOURCE_MODE_RUI;
			    this._sourceAPIMap["PL_WINDOW_SOURCE_ISP"] = this.SOURCE_MODE_ISP;
		   }
		   
		   var windowPlugin = webapis._plugin("WINDOW");
			//windowPlugin.style.visibility = "hidden";
			
		   var source = webapis._plugin(windowPlugin, "GetSource");
		   //var source = webapis._pluginDef.PL_WINDOW_SOURCE_HDMI3;
		   
		    for(var val in webapis._pluginDef) {
		    	if(val.indexOf("PL_WINDOW_SOURCE_") == 0 && source == webapis._pluginDef[val]) {
		     		if(this._sourceAPIMap[val.substr(0, val.length)] || this._sourceAPIMap[val.substr(0, val.length)] == 0) {
		     			alert("this._sourceAPIMap[val.substr(0, val.length)] == " + this._sourceAPIMap[val.substr(0, val.length)]);
		      			return new this.SourceInfo(this._sourceAPIMap[val.substr(0, val.length)], null); // 뒤에 숫자가 없는 경우
		     		}
		     		else if(this._sourceAPIMap[val.substr(0, val.length-1)] || this._sourceAPIMap[val.substr(0, val.length-1)] == 0) {
		     			alert("this._sourceAPIMap[val.substr(0, val.length-1)] == " + this._sourceAPIMap[val.substr(0, val.length-1)]);
		      			return new this.SourceInfo(this._sourceAPIMap[val.substr(0, val.length-1)], Number(val.substr(val.length-1, 1))); // 뒤에 숫자가 있는 경우
		    		}	
		     		else { // 정의되지 않음
		      			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		     		}
		    	}
		   }
		   throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
		},
		 _sourceAPIMap: null,
		
		/*
		 * Function: setRect
		 * 	sets the display area fro video content in TV screen
		 *
		 * Parameters:
		 * 	o rect	- (SRect)
		 * 
		 * Returns:
		 * 	(Boolean)
		 *
		 * Throws:
		 * 	o WebAPIError with error code NOT_SUPPORTED_ERR, If this feature is not supported.
		 * 	o WebAPIError with error code SECURITY_ERR, If the operation is not allowed.
		 * 	o WebAPIError with error code UNKNOWN_ERR, In any other error case.
		 */
		_coldSetOnHideFlag : false,
		setRect: function(rect, windowID){
			var ID = _checkNumberType(windowID);										
			
			if(typeof rect != 'object' || rect == null) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
			
			if((ID > 0)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}else if((ID < 0)) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
				
			var retValue = null;
			
			var windowPlugin = webapis._plugin("WINDOW");
						
			// set Window position
		//	windowPlugin.style.visibility = "visible";
			windowPlugin.style.left = rect.left + 'px';
			windowPlugin.style.top = rect.top + 'px';
			windowPlugin.style.width = rect.width + 'px';
			windowPlugin.style.height = rect.height + 'px';
			
			// Converting for resolution
			var resolutionConstant; 
			if (curWidget.height == 540) resolutionConstant = 1;
			else if (curWidget.height == 720) resolutionConstant = 0.75;	// 540 / 720
			else if (curWidget.height == 1080) resolutionConstant = 0.5;	// 540 / 1080
			else resolutionConstant = 1;
			
			var nLeft = Math.ceil(rect.left * resolutionConstant);
			var nTop =  Math.ceil(rect.top * resolutionConstant);
			var nWidth =  Math.ceil(rect.width * resolutionConstant)+1;
			var nHeight =  Math.ceil(rect.height * resolutionConstant)+1;
			
			retValue = webapis._plugin(windowPlugin, "SetScreenRect", nLeft, nTop, nWidth, nHeight);
	
			var _THIS_ = this;
			/* 위젯 종료될때 */
			if(!_THIS_._coldSetOnHideFlag){				
				window.onHide = docOnHide;
				_THIS_._coldSetOnHideFlag = true;	
			}			
			function docOnHide() {
			    alert("[webapis] docOnHide");
			    webapis._plugin(windowPlugin, "SetScreenRect", -1, 0, 0, 0);			    
			    _THIS_._coldSetOnHideFlag = false;
			}

			if(retValue != -1 && retValue != null) {
				return true;
			}
			else {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}	
		},		
				
		show: function(windowID){
			ID = _checkNumberType(windowID);										
			
			if((ID > 0)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}else if((ID < 0)) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
			var retValue = null;
			
			var windowPlugin = webapis._plugin("WINDOW");
			windowPlugin.style.visibility = "visible";
			/*
			// TV 소스로 강제 전환 삭제 120627		 
			this.preSource = webapis._plugin(windowPlugin, "GetSource");
			if(this.preSource != webapis._pluginDef.PL_WINDOW_SOURCE_TV) {
				retValue = webapis._plugin(windowPlugin, "SetSource", webapis._pluginDef.PL_WINDOW_SOURCE_TV);
			}
			*/
			retValue = windowPlugin.style.visibility;			
			
			if(retValue == 'visible') {
				return true;
			}
			else {
				return false;
			}
		},
		
		hide: function(windowID){
			ID = _checkNumberType(windowID);
			
			if((ID > 0)) {
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR);
			}else if((ID < 0)) {
				throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
			} 
			var retValue = null;
			
			var windowPlugin = webapis._plugin("WINDOW");
			windowPlugin.style.visibility = "hidden";
			
			
			retValue = windowPlugin.style.visibility;
			
			if(retValue == 'hidden') {
				return true;
			}
			else {
				return false;
			}
		},
		
		SourceInfo: function (mode, number) {
			this.type = mode;
			this.number = number;
		},
		
		SubWindow: function (id) {
			this.prototype.id = id;
			this.prototype.show = function () {
			}
			this.prototype.hide = function () {
			}
			this.prototype.setRect = function (rect, subWindowID) {
			}
			this.prototype.setSource = function (sourceID, sourceNum) {
			}
			this.prototype.getSource = function () {
			}
		},
		
		SuccessSubWindowArrayCallback: function(){
		}
	}
}

var SourceChangedSuccessCallback  = null;

var PL_TV_EVENT_SOURCE_CHANGED = 114;        // 소스가 변경될 때 발생
var PL_TV_EVENT_SOURCE_CONNECTED = 126;      // 소스 전환이 완료되면 발생
var PL_TV_EVENT_CAPTION_CHANGED = 612;

function _onTVPluginEvent(event, param1, param2)
{
 // ev 는 항상 0 , param1에 ID 가 들어옴.
 	alert("TV Plugin Event");
 	alert("TVEvent : " + event + " id: " + param1 + " data:"+ param2);
 	
 	var sFirmware = webapis._plugin('NNavi', 'GetFirmware');
	alert('Firmware : ' + sFirmware);
	
	/*
	 * aResult[0] = 2011-1007
	 * aResult[1] = 2011
	 * aResult[2] = 1007
	 */
	var aResult = sFirmware.match(/(\d+)-(\d+)/);
	var nFirmwareYear = aResult[1];	
	alert('nFirmwareYear == ' + nFirmwareYear);
		
 	switch(parseInt(param1)) {
 		case PL_TV_EVENT_SOURCE_CONNECTED:               // occurs when source was changed.
  			alert("########## PL_TV_EVENT_SOURCE_CONNECTED #######");
  			if (nFirmwareYear >= 2012) {
  				alert("<2012>");
  				// setSource
  				SourceChangedSuccessCallback();
  			}
  			break;            
 		case PL_TV_EVENT_SOURCE_CHANGED:
  			alert("########## PL_TV_EVENT_SOURCE_CHANGED ########");            // occurs when source has been changing.
  			if (nFirmwareYear == 2011) {
  				alert("<2011>");
  				// setSource
  				//setTimeout(SourceChangedSuccessCallback(), 100);
  				SourceChangedSuccessCallback();
  			}
  			break;
 	}
}

function _onTVCaptionChangedEvent(event, param1, param2)
{
    alert("TV Caption Changed Event");
 	alert("TV Caption changed Event : " + event + " id: " + param1 + " data:"+ param2);

    switch(parseInt(event)) {
 		case PL_TV_EVENT_CAPTION_CHANGED:               // occurs when source was changed.
  			alert("########## PL_TV_EVENT_CAPTION_CHANGED #######");
            webapis.tv.closedcaption.CaptionChangedCallback();
  			break;
 	}
}


// static members of TimeZone 

/*
webapis.tv.info.TimeZone.TIMEZONE_USA_NEWFOUNDLAND	= 0;
webapis.tv.info.TimeZone.TIMEZONE_USA_ATLANTIC		= 1;
webapis.tv.info.TimeZone.TIMEZONE_USA_EASTERN		= 2;
webapis.tv.info.TimeZone.TIMEZONE_USA_CENTRAL		= 3;
webapis.tv.info.TimeZone.TIMEZONE_USA_MOUNTAIN		= 4;
webapis.tv.info.TimeZone.TIMEZONE_USA_PACIFIC		= 5;
webapis.tv.info.TimeZone.TIMEZONE_USA_ALASKA			= 6;
webapis.tv.info.TimeZone.TIMEZONE_USA_HAWAII			= 7;
webapis.tv.info.TimeZone.TIMEZONE_KOR_SEOUL			= 8;
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_0		= 9; // DVB Time Zone Offset (Region 0) - Australia : Use in a contury that don't have multi time zone
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_1		= 10; // DVB Time Zone Offset (Region 1) - Australia : reserved , Spain.
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_2		= 11; // DVB Time Zone Offset (Region 2) - Australia : New Southwales/ACT , Spain
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_3		= 12; // DVB Time Zone Offset (Region 3) - Australia : Victoria
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_4		= 13; // DVB Time Zone Offset (Region 4) - Australia : Queensland
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_5		= 14; // DVB Time Zone Offset (Region 5) - Australia : South Australia
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_6		= 15; // DVB Time Zone Offset (Region 6) - Australia : Western Australia
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_7		= 16; // DVB Time Zone Offset (Region 7) - Australia : Tasmania
webapis.tv.info.TimeZone.TIMEZONE_DVB_REGION_8		= 17; // DVB Time Zone Offset (Region 8) - Australia : Northern Territory
webapis.tv.info.TimeZone.TIMEZONE_DST_ON	= 1;
webapis.tv.info.TimeZone.TIMEZONE_DST_OFF	= 2;
webapis.tv.info.TimeZone.TIMEZONE_DST_AUTO	= 3;
*/


webapis.network = new getNetworkList ();
var NetworkListLength = 2;
var networkPlugin = null;

function getNetworkList () {
	/*
	 * Function: getAvailableNetworks
	 *
	 * Parameters:
	 * 	o successCallback	- (Function) 
	 * 	o errorCallback		- (Function) 
	 *
	 * Returns:
	 * 	nothing. Pending(Async) Operation
	 *
	 * Throws:
	 * 	o 
	 */
	this.getAvailableNetworks = function (successCallback, errorCallback) {
		if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			return;
		}
		if((typeof successCallback != 'function' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback != null)) {
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
		} 
		if(successCallback == null && errorCallback == null) {
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
		}  
		var NetworkList = new Array();
			
		// 0 - wireless, 1 - wired
		for( var i = 0; i < NetworkListLength; i++ ) {
			NetworkList[i] = new _Network(i);
			alert("The available network interface is " + NetworkList[i].interfaceType);
		}
		
		if( !NetworkList[0].isActive && !NetworkList[1].isActive ) {
			if(typeof errorCallback == 'function') {
				errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
			}
		}
		else {
			for( var i = 0; i < NetworkListLength; i++ ) {
				NetworkList[i] = new _Network(i);
				alert("The available network interface is " + NetworkList[i].interfaceType);
				alert("The available network interface is " + NetworkList[i].dnsMode);
				alert("The available network interface is " + NetworkList[i].ipMode);
			}
			if(typeof successCallback == 'function') {				
				successCallback(NetworkList);
			}
			
		}
		return;
	}
	
}
NetworkConnectionStatusChnageCallback = null;
_networkID = null;
function _onNetworkPluginEvent(event, data1, data2) {
		alert("Network Plugin Event");
		alert("event = [" + event + "]");
		alert("data1 = [" + data1 + "]");
		alert("data2 = [" + data2 + "]");
	
		var status = null;
		switch(event){
			case 0:
				alert('EVENT_NET_CABLE' + data1);
				if(_networkID == 1) {	// _networkID : wired
					if (data1 == 0 ){
						alert("data1 == 0");
						status = new ConnectonStatus(ConnectonStatus.DISCONNECT, "disconnected");
						NetworkConnectionStatusChnageCallback.ondisconnect(status);
					}
					else if (data1 == 1 ){
						alert("data1 == 1");
						status = new ConnectonStatus(ConnectonStatus.CONNECT, "connected");
						NetworkConnectionStatusChnageCallback.onconnect(status);
					}
					alert("status.code == " +status + " / " + status.code + " / " + status.message);
					
				}
				break;

			case 1:
				alert('EVENT_NET_WIRELESS' + data1);
				if(_networkID == 0) {	// _networkID : wireless
					if (data1 == 0 ){
						status = new ConnectonStatus(ConnectonStatus.DISCONNECT, "disconnected");
						NetworkConnectionStatusChnageCallback.ondisconnect(status);
					}
					else if (data1 == 1 ){
						status = new ConnectonStatus(ConnectonStatus.CONNECT, "connected");
						NetworkConnectionStatusChnageCallback.onconnect(status);
					}
					alert("status.code == " +status + " / " + status.code + " / " + status.message);
				}
				break;

			case 2:
				alert('EVENT_GATEWAY_STATUS:' + data1);
				if(_networkID == 0) {   // _networkID : wireless
					if (data1 == 0 ){
						status = new ConnectonStatus(ConnectonStatus.DISCONNECT, "disconnected");
                        NetworkConnectionStatusChnageCallback.ondisconnect(status);
                    }
                    else if (data1 == 1 ){
						status = new ConnectonStatus(ConnectonStatus.CONNECT, "connected");
                        NetworkConnectionStatusChnageCallback.onconnect(status);
                    }
					alert("status.code == " +status + " / " + status.code + " / " + status.message);
                }
				break;

			default:
				break;
		}
	}

function ConnectonStatus(code, message) {
	this.__defineGetter__("code", function () {return code;});
	this.__defineGetter__("message", function () {return message;});
	//this.code = code;
	//this.message = message;
}

ConnectonStatus.prototype.toString = function () {
	return "("+this.code+") " + this.message;
}

ConnectonStatus.DISCONNECT    = 0;
ConnectonStatus.CONNECT       = 1;

/*
 * Class: Network
 * 	The network class. This interface represents the network interface abstraction in use. A network handle can address the information about network settings. If the isActive property is true this network interface is connected currently.
 * 
 * Example:
 * 	(start code)
 * 		function successCB(networks) {
 * 			for (var i = 0; networks.length; i++) {
 * 				alert("The available network interface is " + networks[i].interfaceType); 
 * 			}
 * 		}
 *      
 * 		function errorCB(error) {
 * 			alert("The error " + error.message + " occurred when getting the network interfaces");
 * 		}
 * 
 * 		webapis.network.getAvailableNetworks(successCB, errorCB);
 * 	(end)
 *
 * Parameters:
 * 	o options	- (Object) structures are below.
 * 	dns	- (String)
 *  dnsMode - (String)
 *  gateway - (String)
 *  subnetMask - (String)
 *  ip - (String)
 *  ipMode - (String)
 *  mac - (String)
 *  interfaceType - (String)
 *  isActive - (String)
 */
function _Network( pInterfaceType ) {
	// 0 - wireless, 1 - wired
	this.__defineGetter__("interfaceType", function () {return pInterfaceType});
	this.__defineGetter__("dns", function () {return webapis._plugin("NETWORK", "GetDNS", this.interfaceType);});
	this.__defineGetter__("dnsMode", function () {var nDnsMode = webapis._plugin("NETWORK", "GetDNSMode", this.interfaceType); var sDnsMode = this.toString(nDnsMode); return sDnsMode});
	this.__defineGetter__("gateway", function () {return webapis._plugin("NETWORK", "GetGateway", this.interfaceType);});
	this.__defineGetter__("subnetMask", function () {return webapis._plugin("NETWORK", "GetNetMask", this.interfaceType);});
	this.__defineGetter__("ip", function () {return webapis._plugin("NETWORK", "GetIP", this.interfaceType);});
	this.__defineGetter__("ipMode", function () {var nIpMode = webapis._plugin("NETWORK", "GetIPMode", this.interfaceType); var sIpMode = this.toString(nIpMode); return sIpMode});
	this.__defineGetter__("mac", function () {return webapis._plugin("NETWORK", "GetMAC", this.interfaceType);});
	
	//this.interfaceType	= pInterfaceType;
	//this.dns			= webapis._plugin("NETWORK", "GetDNS", this.interfaceType);		// DNS address string if success, NULL string if error
	//this.dnsMode		= webapis._plugin("NETWORK", "GetDNSMode", this.interfaceType);	// 0 if Auto, 1 if Manual, -1 if error 
	//this.gateway		= webapis._plugin("NETWORK", "GetGateway", this.interfaceType);	// Gateway address string if success, NULL string if error
	//this.subnetMask		= webapis._plugin("NETWORK", "GetNetMask", this.interfaceType);	// NetMask address string if success, NULL string if error
	//this.ip				= webapis._plugin("NETWORK", "GetIP", this.interfaceType);		// IP address string if success, NULL string if error
	//this.ipMode			= webapis._plugin("NETWORK", "GetIPMode", this.interfaceType);	// 0 if Auto, 1 if Manual, -1 if error 
	//this.mac			= webapis._plugin("NETWORK", "GetMAC", this.interfaceType);		// MAC address string if success, NULL string if error 
	
	/*
	 * Function: isActive
	 * 	Check the availabilty of network setting attributes (DNS, gateway, HTTP, physical connection).
	 * 	If the operation is failed due to any other error, the errorCallback is launched with the appropriate error code:
	 * 
	 * 	INVALID_VALUES_ERR	- If the input attributes do not contain a valid value
	 * 	NOT_SUPPORTED_ERR	- If this feature is not supported.
	 * 	SECURITY_ERR		- If the operation is not allowed. 
	 * 	TIMEOUT_ERR			- If the pinging operation in which used to test could not be reachable 
	 * 	UNKNOWN_ERR			- In any other error case.
	 * 
	 * Parameters:
	 * 	o none
	 * 
	 * Returns
	 */
	this.isActive = function () {		
		if( webapis._plugin("NETWORK", "CheckDNS", this.interfaceType) == 1 && webapis._plugin("NETWORK", "CheckGateway", this.interfaceType) == 1 && webapis._plugin("NETWORK", "CheckHTTP", this.interfaceType) == 1 && webapis._plugin("NETWORK", "CheckPhysicalConnection", this.interfaceType) == 1 ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*this.setNetworkConnectionEvent = function () {
		networkPlugin = webapis._plugin("NETWORK");
		networkPlugin.OnEvent = onNetworkConnectionEvent;
	}*/
	
	/**
	 * \brief Check the availabilty of network setting attributes (DNS, gateway, HTTP, physical connection)
	 * \param
	 *
	 * \throw WebAPIError with error code TYPE_MISMATCH_ERR If any of the input attributes are of the wrong type
	 */
	this.setWatchListener = function(successCallback, errorCallback) {
		if(successCallback == null && typeof errorCallback == 'function') {	// successCallback = null/undefined
			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			return;
		}
		if((typeof successCallback != 'object' && successCallback != null) || (typeof errorCallback != 'function' && errorCallback !== undefined)) {
			
			throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
		}
		
		if(typeof successCallback == 'object') {
			if(typeof successCallback.onconnect != 'function' || typeof successCallback.ondisconnect != 'function'){				
				throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
			}
		}
			
			 
		_networkID = this.interfaceType;
		
		networkPlugin = webapis._plugin("NETWORK");
		if(!networkPlugin) {
			if(typeof errorCallback == 'function') {
				errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_SUPPORTED_ERR));
			}
			return;
		}
		networkPlugin.OnEvent = _onNetworkPluginEvent;	
		
		NetworkConnectionStatusChnageCallback = successCallback;	
	}
	
	 /**
	  * \brief Stop network connection status change event listener off
      * \param None
      * \return None
      */
	this.unsetWatchListener = function () {
		alert("stopWatchConnectionStatus");
		NetworkConnectionStatusChnageCallback = null;
	}
	
	this.toString = function (num) {
		//0 if Auto, 1 if Manual, -1 if error
		var str = null; 
		if(num == 0){
			str = 'Auto';
		} else if(num == 1){
			str = 'Manual';
		} else {
		    str = 'NotSupported';	
		}		
		return str;
	} 
}


/*
 * Namespace: filesystem
 * 	This API provides an additional access mechanism to the filesystem of a device. When using those APIs, the below vitual roots, defined at the filesystem module, can be used for there processing.
 * 
 * 	images			- the images location 
 *  videos			- the videos location 
 *  music			- the sounds location 
 *  documents		- the documents location 
 *  downloads		- the location for downloaded items 
 *  wgt-package		- the widget package location (read only location) 
 *  wgt-private		- the widget private storage 
 *  wgt-private-tmp	- the widget private volatile storage area
 *  removable		- the removable storage area location.
 *  
 *  The implementation MUST support the use of the following characters in file names:
 * 
 * 	o Letters (azAZ)
 * 	o Numbers (0-9)
 * 	o White spaces
 * 	o Underscores ("_")
 * 	o Hyphens ("-")
 * 	o Dots (".").
 * 
 * The implementation MAY support additional characters in file names depending on platform support.
 *
 * The implementation MAY forbid the use of additional characters in filenames depending on the platform. The use of the path separator "/" in file names MUST NOT be allowed. The "/" character is used as the (path)component separator.
 *
 * Some other file name and path characteristics are platform dependent, for example, maximum path length, file name length, case sensitivity, additional character support etc. Therefore, it is recommended to avoid any dependency on aspects that cannot be supported across many platforms.
 *
 * When a path is used to interact with the underlying filesystem, the encoding used for the file path SHOULD be the platform default.
 * 
 * Available From:
 * 	0.1
 */



// core objects. let's make sure they exist in some form at least.
Object.freeze = Object.freeze || function () {};
Object.defineProperties = Object.defineProperties || function () {};

// this one is a pain in the back. It MUST be present, otherwise library won't work.
JSON = JSON || {"stringify": function(){ throw "JSON not implemented"; },"parse":function(){ throw "JSON not implemented"; }};

// main closure.
webapis.filesystem = (function () {
    var empErrorCodes = [
                'SUCCESS',
                'INVALID_VALUES_ERR',
                'IO_ERR',
                'NOT_SUPPORTED_ERR',
                'SECURITY_ERR',
                'UNKNOWN_ERR',
                'OP_ABORTED',
                'OP_FAILED'
            ],
        empErrorMessages = [
                'Success',
                'Invalid values',
                'Input/output error',
                'Not supported',
                'Security error',
                'Unknown error',
                'Operation aborted',
                'Operation failed'
            ],
        usedIds = [],
        MAXINT = 65535,
        /**FileFilter interface for filtering directory listings.
         * Usage: a literal object in predefined format is given to <code>WACFile.listFiles</code> object
         * as a third argument (after both callbacks).
         */
        FileFilter = (function () {

            /**Function parses a string into a Date object.
             * Accepted parameters are the one that are accepted by Date.parse
             * and a string format: "YYYY-MM-DD HH:MM:SS".
             * Empty string will NOT result in current date/time.
             */
            function parseDate(dateStr) {
                var de = null,
                    dt = null;

                if (!isNaN(Date.parse(dateStr))) {
                    return new Date(Date.parse(dateStr));
                }

                if (!dateStr || dateStr.match(/[0-9]{4}(?:-[0-9]{2}){2} [0-9]{2}(?::[0-9]{2}){2}/) === null) {
                    return false;
                }

                // assuming format "YYYY-MM-DD HH:MM:SS"; anything else that would be correct
                // would have been caught by Date.parse
                de = dateStr.split(/[ :-]/);
                dt = new Date();
                dt.setFullYear(de[0], de[1] - 1, de[2]);
                dt.setHours(de[3], de[4], de[5]);
                if (dt instanceof Date && dt.toString() !== "Invalid Date") {
                    return dt;
                }
                return null;
            }

            return {
                /**This function checks if a file object matches criteria of a given filter.
                 * @param {WACFile}fileObj file object to check
                 * @param filter filter object to use for validation
                 * @returns {Boolean} true if file should be included in listing, false otherwise.
                 */
                validate: function (fileObj, filter) {
                    if (filter.name && fileObj.name.match(new RegExp(filter.name, "i")) === null) {
                        return false;
                    }
                    if (filter.startModified && parseDate(fileObj.modified) < filter.startModified) {
                        return false;
                    }
                    if (filter.endModified && parseDate(fileObj.modified) > filter.endModified) {
                        return false;
                    }
                    if (filter.startCreated && parseDate(fileObj.created) < filter.startCreated) {
                        return false;
                    }
                    if (filter.endCreated && parseDate(fileObj.created) > filter.endCreated) {
                        return false;
                    }
                    return true;
                },
                /**This is a helper function for building a valid filter object.
                 * User may provide only some fields required in filter format. User may also
                 * provide some fields that should not be accepted (code injection attacks).
                 * This function helps to avoid such kind of problems.
                 * @param {Object}f user-provided object
                 * @returns {FileFilter} a correct FileFilter object
                 */
                build: function (f) {
                    var filter = {};
                    if (!f) {
                        return null;
                    }
                    filter = {
                        "name": f.name && f.name.replace(/\\%/g, "/").replace(/\./, '\\.').replace(/%/g, '.*').replace(/\//g, '%'),
                        "startModified": parseDate(f.startModified),
                        "endModified": parseDate(f.endModified),
                        "startCreated": parseDate(f.startCreated),
                        "endCreated": parseDate(f.endCreated)
                    };
                    if ((filter.startModified && filter.endModified && filter.startModified > filter.endModified) ||
                            (filter.startCreated && filter.endCreated && filter.startCreated > filter.endCreated)) {
                        filter = null;
                    }
                    return filter;
                }
            };
        }()),
        /**RequestManager is an internal object that handles EMP requests.
         * It is used for creating PendingOperation objects, sending requests to EMP,
         * receiving requests from EMP and managing PendingOperations, specifically
         * finalizing them and maintaining the list of active PO's.
         */
        RequestManager = (function  () {
            /**EMPCallbacks are functions required to process EMP-originated requests.
             * EMP asynchronous calls happen inside of EMP and when they're finished, EMP
             * calls the OnEvent function. RequestManager.onEvent intercepts such call
             * and dispatches work to EMPCallbacks, if necessary.
             */
            var EMPCallbacks = {
                /**After unsuccessful Delete operation it is important to display a proper error message.
                 * Also, success callback is fed with an argument.
                 */
                "Delete": function (eventType, pendingOperation, EMPReturn) {
                    if (EMPReturn instanceof WACError) {
                        return RequestManager.finalize(pendingOperation.id, false, [EMPReturn]);
                    } else if (EMPReturn && EMPReturn.ErrorNumber) {
                        RequestManager.finalize(pendingOperation.id, false, [(new WACError("Error: " + EMPReturn.ErrorDescription, empErrorCodes[EMPReturn.ErrorNumber]))]);
                        return false;
                    } else if (eventType) { // this shouldn't be here; it's a workaround for missing error structure in EMPReturn.
                        RequestManager.finalize(pendingOperation.id, false, [(new WACError("Error: " + empErrorMessages[eventType], empErrorCodes[eventType]))]);
                        return false;
                    }
                    // path of the deleted file must be used as a successCallback' argument
                    return RequestManager.finalize(pendingOperation.id, true, [pendingOperation.path]);
                },
                /**ListFiles operation is complex, because EMP returns a plain list of file names.
                 * This list must be transformed into an array of WACFile objects. This is potentially
                 * a lengthy operation, thus it was wrapped into setInterval construction to avoid blocking
                 * whole JavaScript thread. Each name from list is queried to EMP for FileStat structure, which
                 * in turn is used for building a WACFile object.
                 */
                "ListFiles": function (eventType, pendingOperation, EMPReturn) {
                    var objArr = EMPReturn && EMPReturn.ListFiles || [],
                        parent = pendingOperation && pendingOperation.owner || {},
                        virtualRoot = parsePath(parent.fullPath)[0],
                        relativePath = parsePath(parent.fullPath)[1],
                        filter = false,
                        ret = [],
                        intrvl = null;

                    if (EMPReturn.ErrorNumber == 2) {
                        return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byType('IOError'))]);
                    }

                    if (eventType) {
                        return RequestManager.finalize(pendingOperation.id, false, [(new WACError("Error: " + empErrorMessages[eventType], empErrorCodes[eventType]))]);
                    }

                    if (!virtualRoot) {
                        return RequestManager.finalize(pendingOperation.id, false, [(new WACError("Error: " + EMPReturn.ErrorDescription, empErrorCodes[EMPReturn.ErrorNumber]))]);
                    }

                    // filter is passed through PendingOperation object. There was no other way.
                    if (pendingOperation.filter) {
                        filter = FileFilter.build(pendingOperation.filter);
                    }

                    // some variables not defined in this anonymous function are in its scope, like <code>ret</code>
                    // or <code>objArr</code>.
                    intrvl = setInterval(function(){
                        var on = "",
                            fs = null,
                            fileObj = null;

                        if (objArr.length < 1) {
                            clearInterval(intrvl);
                            RequestManager.finalize(pendingOperation.id, true, [ret]);
                        } else {
                            on = objArr.shift().ObjName;
                            try {
                                // trying to get a regular file name in UCS-2 encoding. EMP sometimes sends
                                // string in form of raw bytes and this is a workaround. Not always works :(
                                on = decodeURIComponent(escape(on));
                            } catch (e) {
                                // console.debug("File name " + on + "(" + escape(on) + ") must be bad!");
                            }

                            fs = RequestManager.sendRequest("FileStat", [virtualRoot, relativePath + "/" + on]);
                            if (fs.ErrorNumber) {
                                // FileStat did not succeed. Library will try hard to display some information
                                // about failed entry. User will not be able to do anything with it - it's not
                                // a valid WACFile object (thus "Created": "virtual"). Nevertheless they HAVE information
                                // that there is something in the file system they try to access.
                                fs = {
                                    "Created" : "virtual",
                                    "Description" : "",
                                    "ErrorNumber" : 0,
                                    "FileSize" : -1,
                                    "IsDirectory" : false,
                                    "IsFile" : true,
                                    "Length" : 0,
                                    "Modified" : "",
                                    "Name" : on,
                                    "Path" : virtualRoot + relativePath + "/" + on,
                                    "ReadOnly" : true,
                                    "RealPath" : ""
                                 };
                            }

                            try {
                                fileObj = new WACFile(fs, parent, pendingOperation.mode);
                                if (!filter || FileFilter.validate(fileObj, filter)) {
                                    ret.push(fileObj);
                                }
                            } catch (e) {
                                //console.error("File " + on + " must be bad. (Error: " + e.message + " [#" + e.code + "]");
                            }
                        }
                    }, 0);

                    return true;
                }
            },
            pName = "FSAPI";

            return {
                fsapi: null,
                list: {},
                /**Creates a new PendingOperation object or uses the one given as argument and adds it to list.
                 * If third argument is given, only adds PendingOperation object to internal list.
                 */
                add: function (pendingOperation, callbacksArray, doNotTouch) {
                    if (!doNotTouch) {
                        pendingOperation = pendingOperation || new PendingOperation();
                        if (!(callbacksArray instanceof Array) || typeof callbacksArray[0] !== "function") {
                            throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                        } else {
                            pendingOperation.successCB = callbacksArray[0];
                        }

                        if (callbacksArray.length > 1 && typeof callbacksArray[1] === "function") {
                            pendingOperation.errorCB = callbacksArray[1];
                        }
                        if (!pendingOperation.id) {
                            pendingOperation.id = generateId();
                        }
                    }
                    this.list[pendingOperation.id] = pendingOperation;
                    return pendingOperation;
                },
                get: function (id) {
                    return this.list[id];
                },
                remove: function (id) {
                    delete this.list[id];
                },
                /**Sends a request to EMP.
                 * @param {Array} callbacksArray (optional) Array of two functions, the Success Callback and Error callback, or single value array, the PendingOperation ID.
                 * @param {String} command a command to send to EMP
                 * @param {Array} args an array of arguments to pass to EMP
                 */
                sendRequest: function (callbacksArray, command, args) {
                    var EMPReturn = null, pendingOperation = null;
                    if (typeof callbacksArray === "string" && !args) {
                        args = command;
                        command = callbacksArray;
                        callbacksArray = null;
                    }
                    // the check below might not be necessary
                    if (!(args instanceof Array)) {
                        args = [args];
                    }
                    args.unshift(pName, command);
                    // TODO security: somehow validate the command
                    EMPReturn = webapis._plugin.apply(null, args);
                    try {
                        // line below relies on JSON library, which may not be present. FIXME TODO
                        EMPReturn = JSON.parse(EMPReturn);
                    } catch (e) {
                        // no exception thrown; some actions return raw data, though none should.
                    }
                    if (EMPReturn.OperationId && callbacksArray instanceof Array) {
                        if (callbacksArray.length > 1) {
                            // new async operation
                            pendingOperation = new PendingOperation();
                            pendingOperation.id = EMPReturn.OperationId;
                            this.add(pendingOperation, callbacksArray);
                        } else if (typeof callbacksArray[0] !== "function") {
                            // another request in the same async call
                            pendingOperation = this.get(callbacksArray[0]);
                            if (pendingOperation instanceof PendingOperation) {
                                this.remove(pendingOperation.id);
                                pendingOperation.id = EMPReturn.OperationId;
                                this.add(pendingOperation, null, true);
                            } else {
                                // unknown error, probably wrong parameters
                                throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                            }
                        } else {
                            // unknown error, probably wrong parameters
                            throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                        }
                        return pendingOperation;
                    }
                    return EMPReturn;
                },
                /**Cancels user request. If there is an asynchronous operation performed by EMP,
                 * a command to cancel this operation is sent to EMP. If EMP operation is synchronized,
                 * but JavaScript operation is async, no further requests are sent to EMP.
                 */
                cancelRequest: function (id) {
                    var pendingOperation = this.get(id),
                        ret = null;
                    if (isNaN(id)) {
                        // fake async operation
                        ret = new WACError("Operation aborted.", "OP_ABORTED");
                    } else {
                        ret = webapis._plugin(pName, "CancelOperation", pendingOperation.id);
                    }
                    return this.finalize(id, false, [ret]);
                },
                /**Initialization of EMP. This relies on webapis._plugin method.
                 * It also sends first request to EMP which sets internal paths to virtual roots.
                 */
                init: function () {
                    var ref = this;
                    if (this.fsapi === null) {
                        this.fsapi = webapis._plugin(pName);
                        webapis._plugin(pName, "SetWgtId", curWidget.id, location.hostname + location.pathname.substring(0, location.pathname.indexOf(curWidget.id)));
                        this.fsapi.OnEvent = function () {
                            ref.onEvent.apply(ref, arguments);
                        };
                    }
                },
                /** Removes PendingOperation with specified id from list.
                 * If success argument is true, fire success callback.
                 * If success argument is false, fire error callback.
                 * If success argument is neither true nor false, no callback is fired.
                 * PendingOperation with specified id is removed from list in any case.
                 * Callbacks are fed with array of arguments taken from third function argument.
                 * @argument {String} id
                 * @argument {Boolean} success
                 * @argument {Array} argsArray
                 */
                finalize: function (id, success, argsArray) {
                    var pendingOperation = this.get(id);
                    if (!pendingOperation) {
                        return false;
                    }
                    if (success === true) {
                        pendingOperation.successCB.apply(null, argsArray);
                    } else if (pendingOperation.errorCB !== null && success === false) {
                        pendingOperation.errorCB.apply(null, argsArray); // there should be error structure. It would be nice to check if it's really there.
                    }
                    this.remove(id);
                    pendingOperation.cancel = function(){ console.log("Can't cancel, operation finalized."); };
                    return true;
                },
                /**Event hook. This function receives a product of EMP's asynchronous operations
                 * and dispatches it to a proper EMPCallback function, if present, or simply
                 * finalizes PendingOperation.
                 * @param {Integer} eventType if 0, success. Otherwise it's an error code.
                 * @param {Integer} pendingOperationId id of PendingOperation.
                 * @param {Mixed} data actual product of async operation.
                 */
                onEvent: function (eventType, pendingOperationId, data) {
                    var pendingOperation = null;
                    pendingOperation = this.get(pendingOperationId);
                    //console.debug("OnEvent fired", arguments, pendingOperation);
                    try {
                        // line below relies on JSON library, which may not be present. FIXME TODO
                        data = JSON.parse(data);
                    } catch (e) {
                        // no exception thrown, because some actions return raw data
                    }
                    if (pendingOperation) {
                        if (typeof EMPCallbacks[pendingOperation.action] === "function") {
                            // if there is a callback, it will handle both success and failure
                            EMPCallbacks[pendingOperation.action](eventType, pendingOperation, data);
                        } else if (eventType) {
                            // failure
                            this.finalize(pendingOperation.id, false, [(new WACError("Error: " + empErrorMessages[eventType], empErrorCodes[eventType]))]);
                        } else {
                            // success
                            this.finalize(pendingOperation.id, true, []);
                        }
                    } // else fail silently
                }
            };
        }()),
        /**Helper object for easy creation of PendingOperation object.
         */
        PendingOperationFactory = {
            /**Function that produces PendingOperation object.
             * It simply creates an empty PendingOperation object (only with callbacks)
             * or creates object and sends request to EMP in one command.
             */
            get: function (successCB, errorCB, command, args) {
                if (!command && !args) {
                    // simulated async operation
                    return RequestManager.add(null, [successCB, errorCB]);
                } else {
                    // real async operation
                    return RequestManager.sendRequest([successCB, errorCB], command, args);
                }
            }
        };

    // ***************************** private stuff *****************************

    /**Generate new identifier for PendingOperation
     * @private
     */
    function generateId() {
        var salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", idCandidate = '';
        function sortRandom() {
            return (0.5 - Math.random());
        }
        function filterRandom() {
            return (0.5 < Math.random());
        }
        do {
            idCandidate = salt.split("").filter(filterRandom).sort(sortRandom).join("");
        } while (usedIds.indexOf(idCandidate) > -1);
        usedIds.push(idCandidate);
        return idCandidate;
    }

    /**Function splits full path to two segments: virtual root and the rest.
     * In case of virtual root, second segment is empty.
     * @param {String} fullPath full path to be parsed
     * @returns {Array} an array of two elements, virtual root and relative path.
     */
    function parsePath(fullPath) {
    		var aa = (typeof fullPath === "string") && fullPath.replace(/"/g,'');
        var a = (typeof aa === "string") && aa.split("/") || [""];
        return [a.shift(), ((a.length) ? "/" + a.join("/").replace(/\/$/,'') : "")];
    }

    /**Class PendingOperation for objects representing operations performed by library.
     * @class PendingOperation
     * @constructor
     * @param {Mixed}id desired identifier for pending operation
     */
    function PendingOperation(id) {
        var ref = this;
        this.id = id || null;
        this.successCB = null;
        this.errorCB = null;

        this.action = ""; // name of EMP action
        this.owner = null; // owner object

        /**Internal function, sends actual cancel request to EMP.
         * @private
         * @returns {Boolean} indicates success or failure in cancelling
         */
        function cancel() {
            return RequestManager.cancelRequest(ref.id);
        }

        /**Function cancels pending operation.
         * @returns {Boolean} indicates success or failure in cancelling
         */
        this.cancel = function () {
            return cancel();
        };

    }

    // arbitrary chosen values, extending default object. not really good idea.
    DOMException.INVALID_VALUES_ERR = 97;
    DOMException.IO_ERR = 98;
    DOMException.UNKNOWN_ERR = 99;

    /**Definition of WAC error structure.
     * @class WACError
     * @constructor
     * @extends Error
     * @param {String} message : an informative message about the nature of the problem
     * @param {Number|String} code : string symbol or a number, e.g. "SECURITY_ERR", 18
     */
    function WACError(message, code) {
        this.name = "WACError";
        this.message = message || "No info";
        this.code = (isNaN(code)) ? DOMException[code] : code;
    }

    WACError.prototype = new Error();
    WACError.prototype.constructor = WACError;

    // encoding is ignored.
    /**FileStream interface definition.
     * @class FileStream
     * @constructor
     * @param {WACFile}fileObj an object representing file to be manipulated
     * @param {String}mode a single-char string (r, w or a) determinig stream mode
     * @param {String}encoding text encoding. This param is ignored, as only UTF-8 is supported.
     */
    function FileStream(fileObj, mode, encoding) {
        var fp = null,
            virtualRoot = "",
            relPath = "",
            seek = null,
            position = 0;

        // one cannot create a FileStream to a directory.
        if (!(fileObj instanceof WACFile) || !fileObj.isFile) {
            throw ErrorsHelper.createWebAPIException_byType('IOError');
        }

        // streams are one-way only: a/w are for writing, r is for reading.
        // simultaneous reading and writing to a single stream is forbidden.
        if (!mode || mode.match(/^[awr]$/i) === null) {
            throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
        }

        // writing to a read-only file should be forbidden
        if (mode.match(/^[aw]$/) && fileObj.readOnly) {
            throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
        }

        mode = mode.toLowerCase();
        virtualRoot = parsePath(fileObj.fullPath)[0];
        relPath = parsePath(fileObj.fullPath)[1];

        fp = RequestManager.sendRequest("Open", [virtualRoot, relPath, mode]);
        if (fp.ErrorNumber) {
            fp.ErrorDescription = fp.ErrorDescription || empErrorMessages[fp.ErrorNumber] || "unknown error.";
            throw new WACError("Could not open file: " + fp.ErrorDescription, empErrorCodes[fp.ErrorNumber]);
        } else {
            fp = fp.FileDescriptor;
        }

        this.eof = false;
        this.position = position = 0;
        this.bytesAvailable = fileObj.fileSize;

        if (mode === "a") {
            // no need to request EMP to seek to position; opening in "a" mode is sufficient.
            this.position = position = fileObj.fileSize;
            this.bytesAvailable = 0;
        }

        /**Internal dispatcher. Accepts variable number of arguments,
         * but first two are fixed. First one is a FileStream handle,
         * second one is method name, which is used to fire appropriate
         * callback function. All callbacks must be wrapped with some
         * other procedures (position pointer control, file size checking
         * etc.).
         * @private
         */
        function rq() {
            // DO NOT change the order of assigning variables!
            var argsArr = Array.prototype.slice.call(arguments),
                fs = argsArr.shift(),
                fName = argsArr.shift(),
                ret = null,
                tell = null,
                callbacks = {
                    "seek": function (position) {
                        return RequestManager.sendRequest("Seek", [fp, position]);
                    },
                    "close": function () {
                        fs.eof = true;
                        return RequestManager.sendRequest("Close", [fp]);
                    },
                    "read64": function (byteCount) {
                        var r64 = null;

                        if (mode !== "r") {
                            throw ErrorsHelper.createWebAPIException_byType('IOError');
                        }
                        if (fs.eof) {
                            return "";
                        }
                        if (fs.bytesAvailable === 0) {
                            // end of file reached
                            fs.eof = true;
                            fs.bytesAvailable = -1;
                            return "";
                        }
                        if (byteCount > fs.bytesAvailable) {
                            // try not to read beyond file
                            byteCount = fs.bytesAvailable;
                        }
                        r64 = RequestManager.sendRequest("Read64", [fp, byteCount]);
                        if (r64 && !r64.ErrorNumber && r64.Data) {
                            return r64.Data;
                        } else if (r64 && r64.ErrorNumber) {
                            throw new WACError("Error: " + r64.ErrorDescription, empErrorCodes[r64.ErrorNumber]);
                        }
                        return r64;
                    },
                    "write64": function (base64Data) {
                        var w64 = null;
                        if (mode.match(/^[aw]$/i) === null) {
                            throw ErrorsHelper.createWebAPIException_byType('IOError');
                        }
                        w64 = RequestManager.sendRequest("Write64", [fp, base64Data.length, base64Data]);
                        if (w64 && !w64.ErrorNumber && w64.Size) {
                            return w64.Size;
                        } else if (w64 && w64.ErrorNumber) {
                            throw new WACError("Error writing to file: " + w64.ErrorDescription, empErrorCodes[w64.ErrorNumber]);
                        }
                        return w64;
                    }
                };
            if (typeof fName === "string" && typeof callbacks[fName] === "function") {
                if (fs.position !== position) {
                    // user has set the position by setting FileStream's property
                    callbacks.seek(fs.position);
                }

                // perform actual operation
                ret = callbacks[fName].apply(null, argsArr);

                // did we reach end of file?
                if (fs.eof === true) {
                    return ret;
                }

                // let's update position of pointer
                tell = RequestManager.sendRequest("Tell", [fp]);
                if (!tell.ErrorNumber) {
                    fs.position = position = tell.StreamPosition;
                    fs.bytesAvailable = (fileObj.fileSize < fs.position) ? 0 : fileObj.fileSize - fs.position;
                    if (!fs.bytesAvailable) {
                        fs.bytesAvailable = -1;
                        fs.eof = true;
                    }
                } else {
                    fs.eof = true;
                    fs.bytesAvailable = -1;
                    fs.position = position = fileObj.fileSize;
                }
            }
            return ret;
        }

        /**This method closes file stream. It is vital to always close
         * streams in order not to leave opened file descriptors, as it
         * may lead to OS problems.
         */
        this.close = function () {
            return rq(this, "close");
        };

        /**This method reads specified amount of characters from file stream.
         * It is assumed that the stream provides textual data encoded in UTF-8.
         * FileStream itself is not aware of expected encoding, because it
         * deliberately <strong>ignores</strong> the <code>encoding</code> constructor argument.
         *
         * FileStream.read method needs to watch out for multibyte characters. In UTF-8,
         * a single character may be encoded using more than one byte. Thus the algorythm
         * for reading a specified amount of chars is:
         * 1. Read specified amount of bytes, as many, as there should be characters.
         * 2. Encode to UTF-8, check string length.
         * 3. Compare string length with amount of characters specified in argument.
         *    If necessary, read again from stream, one byte per character.
         * There is also a problem of UTF-8 decoding. It may happen that reading byte-by-byte
         * might cut a multi-byte character in half. In such case operation decoding such string
         * will fail. Next the procedure will attempt to read one byte less and try to decode UTF-8 again.
         * Should this loop fail 5 times, it is assumed that the stream does not represent a UTF-8
         * encoded text file and the whole procedure will exit (longest multibyte characters in UTF-8
         * take up to 4 bytes at most).
         *
         * @param {Integer}charCount number of multibyte characters to read
         * @returns {String} valid string read from stream
         * @throws {INVALID_VALUES_ERR} "Error: this is not a text file."
         * @throws {IO_ERR} "Can not read file, wrong mode: " + mode
         * @throws {UNKNOWN_ERR} "Error: " + empErrorDescription
         */
        this.read = function (charCount) {
            var decodedBuffer = "", encodedBuffer = "", charsToRead = charCount, retry = 0;
            do {
                // hack with decodeURIComponent / escape found somewhere on the internet, no license provided --> implied public domain
                encodedBuffer = rq(this, "read64", charsToRead);
                try {
                    decodedBuffer += decodeURIComponent(escape(atob(encodedBuffer)));
                    // if the line above will throw, execution will not reach line below
                    charsToRead = charCount - decodedBuffer.length;
                    retry = 0;
                } catch (e) {
                    if (retry > 5) {
                        // too many retries. this is not a text file encoded in UTF-8. we must stop
                        // and return an error. rq/read64 throws, so we will throw too.
                        throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                    } else {
                        // reading next byte for a multibyte character
                        // rewind back the file and decrease number of bytes to read in next iteration
                        rq(this, "seek", this.position - charsToRead);
                        charsToRead--;
                        retry++;
                    }
                }
            } while (charsToRead > 0 && this.bytesAvailable > 0);
            return decodedBuffer;
        };

        /**This method reads specified amount of bytes from file stream.
         * Data is read in a form of base64-encoded string and then is
         * transformed into an array of integers in range between 0-255.
         * @param {Integer}byteCount bytes amount to read
         * @returns {Array} array of integers 0-255
         * @throws {IO_ERR} "Can not read file, wrong mode: " + mode
         * @throws {UNKNOWN_ERR} "Error: " + empErrorDescription
         */
        this.readBytes = function (byteCount) {
            var decodedBuffer = "", encodedBuffer = "", bytesToRead = byteCount;
            do {
                encodedBuffer = rq(this, "read64", bytesToRead);
                try {
                    decodedBuffer += atob(encodedBuffer);
                    bytesToRead = byteCount - decodedBuffer.length;
                } catch (e) {
                    rq(this, "seek", this.position - bytesToRead);
                    bytesToRead--;
                }
            } while (bytesToRead > 0 && this.bytesAvailable > 0);
            return Array.prototype.map.call(decodedBuffer, function(x){return x.charCodeAt(0)}); // octet[]
        };

        /**This method reads specified amount of bytes from file stream encoded as base64 string.
         * Data is read in a form of base64-encoded string and returned as-is.
         * @param {Integer}byteCount bytes amount to read
         * @returns {String} base64-encoded string
         * @throws {IO_ERR} "Can not read file, wrong mode: " + mode
         * @throws {UNKNOWN_ERR} "Error: " + empErrorDescription
         */
        this.readBase64 = function (byteCount) {
            return rq(this, "read64", (byteCount < this.bytesAvailable ? byteCount : this.bytesAvailable));
        };

        /**This method writes text to file stream.
         * String data is assumed to be UCS-2 encoded (internal JavaScript
         * string encoding). Thus a conversion to raw UTF-8 encoded bytes is needed.
         * It then is encoded to base64 string and sent to EMP.
         * @param {String}stringData text to write to file stream
         * @returns {Integer} amount of bytes written
         * @throws {IO_ERR} "Can not write to file, wrong mode: " + mode
         * @throws {UNKNOWN_ERR} "Error: " + empErrorDescription
         */
        this.write = function (stringData) {
            // implicitly taking data as UTF8
            return rq(this, "write64", btoa(unescape(encodeURIComponent(stringData))));
        };

        /**This method writes byte data to file stream. Data is passed to method
         * in form of an array of integers in range between 0-255.
         * @param {octet[]}byteData array of integers in range between 0-255.
         * @returns {Integer} a number of bytes written
         * @throws {IO_ERR} "Can not write to file, wrong mode: " + mode
         * @throws {UNKNOWN_ERR} "Error: " + empErrorDescription
         */
        this.writeBytes = function (byteData) {
            var rawString = Array.prototype.map.call(
                                byteData,
                                function (x) {
                                    if (isNaN(x) || x < 0 || x > 255 ) {
                                        throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                                    } else {
                                        return String.fromCharCode(x);
                                    }
                                }).join("");
            return rq(this, "write64", btoa(rawString));
        };

        /**This method writes base64-encoded data to file stream. Data is passed to method
         * as-is, because EMP accepts such format and decodes it internally.
         * @param {String}base64Data base64-encoded string
         * @returns {Integer} a number of bytes written
         * @throws {IO_ERR} "Can not write to file, wrong mode: " + mode
         * @throws {UNKNOWN_ERR} "Error: " + empErrorDescription
         */
        this.writeBase64 = function (base64Data) {
            return rq(this, "write64", base64Data);
        };
    }

    /**WACFile main closure. Contains both class constructor and prototype methods.
     */
    var WACFile = (function () {

        /**Class that is used for all file system objects.
         * @class WACFile
         * @constructor
         */
        function WACFile(FileData, parent, mode) {
            // FileData comes directly from EMP.
            if (!FileData || FileData.ErrorNumber > 0) {
                throw new WACError("Error: " + FileData.ErrorDescription, empErrorCodes[FileData.ErrorNumber]);
            }

            if (!mode.match(/^[rw]+$/)) {
                throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
            }

            FileData.Path = FileData.Path.replace(/\/\.$/, ''); // ugly hack, yuck :(
            FileData.Name = FileData.Name.replace(/\//, "");

            // parsing data from EMP and transforming it into WACFile properties,
            // with default values provided
            this.readOnly = FileData.ReadOnly || false;
            this.isFile = FileData.IsFile || false;
            this.isDirectory = FileData.IsDirectory || false;
            this.created = FileData.Created || undefined;
            this.modified = FileData.Modified || undefined;
            this.fileSize = FileData.FileSize || 0;
            this.length = FileData.Length || 0;
            this.parent = parent || null;
            this.path = (FileData.Path.lastIndexOf("/") > -1) ? FileData.Path.substring(0, FileData.Path.lastIndexOf("/") + 1) : FileData.Path;
            this.name = (FileData.Path.lastIndexOf("/") > -1) ? FileData.Name : "";
            this.fullPath = FileData.Path || "";

            // Functions and methods below MUST reside in constructor, because
            // they share the "mode" variable value, which is unique for each WACFile object
            // and thus cannot be put nor used in WACFile.prototype.
            // To make WACFile objects smaller, only certain methods are included in Directory type objects.
            if (this.isDirectory) {
                /**Function called on a directory, creates a child directory or a whole branch of directory tree.
                 * @param {String}dirPath a path of one or more directory names to create
                 * @returns {WACFile} a directory handle to the deepest created directory
                 * @throws {SECURITY_ERR} "No permission."
                 * @throws {INVALID_VALUES_ERR} "Bad path: " + dirPath
                 */
                this.createDirectory = function (dirPath) {
                    var dirObj = this, nDirObj = null, segments = null, i = 0, path = normalizePath(dirPath);
                    if (mode.match(/w/i) === null || this.readOnly) {
                        throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR);
                    }
                    if (!path) {
                        throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                    }
                    segments = path.split("/");
                    if (segments.length && segments[0].length) {
                        for (i = 0; i < segments.length; i++) {
                            nDirObj = dirObj.resolve(segments[i]);
                            if (!nDirObj) {
                                dirObj = create(segments[i], "D", dirObj, mode);
                            } else {
                                dirObj = nDirObj;
                            }
                        }
                    }
                    return dirObj;
                };

                /**Function called on a directory, creates a file in that directory.
                 * @param {String}filePath a path of a file to create
                 * @returns {WACFile} a file handle to created file
                 * @throws {SECURITY_ERR} "No permission."
                 * @throws {INVALID_VALUES_ERR} "Bad path: " + dirPath
                 */
                this.createFile = function (filePath) {
                    var parent = this, path = normalizePath(filePath);
                    if (mode.match(/w/i) === null || this.readOnly) {
                        throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR);
                    }
                    if (!path) {
                        throw nErrorsHelper.createWebAPIException_byType('InvalidValuesError');
                    }
                    if (path.lastIndexOf("/") > -1) {
                        parent = this.resolve(path.substring(0, path.lastIndexOf("/")));
                    }
                    return create(path.substring(path.lastIndexOf("/")), "F", parent, mode);
                };

                /**Function called on a directory, deletes its own child directory.
                 * @param {Function}successCB function to call after successful operation
                 * @param {Function}errorCB function to call after error
                 * @param {String}directory name of directory to delete
                 * @param {Boolean}recursive whether to delete non-empty directories
                 * @returns {PendingOperation} a PendingOperation object
                 */
                this.deleteDirectory = function (successCB, errorCB, directory, recursive) {
                    var pendingOperation = null, ref = this;
                    if (typeof errorCB !== "function" && !recursive) {
                        recursive = directory;
                        directory = errorCB;
                        errorCB = function () {};
                    }
                    pendingOperation = PendingOperationFactory.get(successCB, errorCB);
                    pendingOperation.action = "Delete";
                    pendingOperation.owner = this;
                    if (recursive === undefined) {
                        recursive = true;
                    }
                    setTimeout(function () {
                        deleteObjects(pendingOperation, ref, mode, directory, "D", recursive);
                    }, 0);
                    return pendingOperation;
                };

                /**Function called on a directory, deletes a file in its own child directory.
                 * @param {Function}successCB function to call after successful operation
                 * @param {Function}errorCB function to call after error
                 * @param {String}file name of file to delete
                 * @returns {PendingOperation} a PendingOperation object
                 */
                this.deleteFile = function (successCB, errorCB, file) {
                    var pendingOperation = null, ref = this;
                    pendingOperation = PendingOperationFactory.get(successCB, errorCB);
                    pendingOperation.action = "Delete";
                    pendingOperation.owner = this;
                    setTimeout(function () {
                        deleteObjects(pendingOperation, ref, mode, file, "F");
                    }, 0);
                    return pendingOperation;
                };

                /**Function called on a directory, copies descendant objects to other path.
                 * Source items must be on this directory path.
                 * @param {Function}successCB function to call after successful operation
                 * @param {Function}errorCB function to call after error
                 * @param {String}originFilePath file or directory path that is to be copied
                 * @param {String}destinationFilePath destination path
                 * @param {Boolean}overwrite if target exists, should it be overwritten?
                 * @returns {PendingOperation} a PendingOperation object
                 */
                this.copyTo = function (successCB, errorCB, originFilePath, destinationFilePath, overwrite) {
                    var pendingOperation = null, ref = this;
                    if (typeof errorCB !== "function" && !overwrite) {
                        overwrite = destinationFilePath;
                        destinationFilePath = originFilePath;
                        originFilePath = errorCB;
                        errorCB = function () {};
                    }
                    pendingOperation = PendingOperationFactory.get(successCB, errorCB);
                    pendingOperation.action = "CopyTo";
                    pendingOperation.owner = this;
                    setTimeout(function () {
                        changeFiles(pendingOperation, ref, originFilePath, destinationFilePath, overwrite, mode);
                    }, 0);
                    return pendingOperation;
                };

                /**Function called on a directory, moves descendant objects to other path.
                 * Source items must be on this directory path.
                 * @param {Function}successCB function to call after successful operation
                 * @param {Function}errorCB function to call after error
                 * @param {String}originFilePath file or directory path that is to be copied
                 * @param {String}destinationFilePath destination path
                 * @param {Boolean}overwrite if target exists, should it be overwritten?
                 * @returns {PendingOperation} a PendingOperation object
                 */
                this.moveTo = function (successCB, errorCB, originFilePath, destinationFilePath, overwrite) {
                    var pendingOperation = null, ref = this;
                    if (typeof errorCB !== "function" && !overwrite) {
                        overwrite = destinationFilePath;
                        destinationFilePath = originFilePath;
                        originFilePath = errorCB;
                        errorCB = function () {};
                    }
                    pendingOperation = PendingOperationFactory.get(successCB, errorCB);
                    pendingOperation.action = "MoveTo";
                    pendingOperation.owner = this;
                    setTimeout(function () {
                        changeFiles(pendingOperation, ref, originFilePath, destinationFilePath, overwrite, mode);
                    }, 0);
                    return pendingOperation;
                };

                /**Function called on a directory, checks if a relative path given as argument is valid.
                 * If the path is not valid, function <strong>returns null</strong> and shows an alert
                 * using <code>alert</code> function.
                 * @param {String}filePath path to file or directory to check
                 * @returns {WACFile} a handle to resolved file or directory
                 */
                this.resolve = function (filePath) {
                    var fsObj = this, i = 0, path = null, segments = null;
                    filePath = new String(filePath); // ensuring that this IS a String

                    if (filePath.length === 0) {
                        return null;
                    }

                    path = normalizePath(filePath)

                    if (path === false) {
                        alert("Path " + filePath + " contains illegal characters!");
                        return null;
                    }
                    segments = path.split("/");
                    if (segments.length) {
                        for (i=0; i < segments.length && fsObj; i++) {
                            // resolveSync may return null.
                            fsObj= resolveSync(segments[i], fsObj, mode);
                        }
                    }
                    return fsObj;
                };

                /**Function called on a directory, lists its contents.
                 * @param {Function}successCB function to call after successful operation
                 * @param {Function}errorCB function to call after error
                 * @param {Object}fileFilter object used to filter the listing
                 * @returns {PendingOperation} a PendingOperation object
                 */
                this.listFiles = function (successCB, errorCB, fileFilter) {
                    var pendingOperation = null, po = null;

                    if (errorCB && typeof errorCB === "object" && !fileFilter) {
                        fileFilter = errorCB;
                        errorCB = null;
                    }
                    pendingOperation = PendingOperationFactory.get(successCB, errorCB, "ListFiles", [parsePath(this.fullPath)[0], parsePath(this.fullPath)[1]]);
                   if (pendingOperation instanceof PendingOperation) {
                        pendingOperation.action = "ListFiles";
                        pendingOperation.owner = this;
                        pendingOperation.filter = fileFilter;
                        pendingOperation.mode = mode;
                        return pendingOperation;
                    } else if (pendingOperation.ErrorNumber) {
                        po = PendingOperationFactory.get(successCB, errorCB);
                        setTimeout(function(){
                            synchronouslyThrownErrorRethrowAsynchronously(pendingOperation, po);
                        }, 10);
                        return po;
                    } else {
                        // SNAFU :(
                        return null;
                    }
                };
            }

            // seal the object, prevent modifications. works on JavaScript 1.8.5, that is, platform 2012.
            Object.defineProperties(this, {
                "readOnly": {"writable": false},
                "isFile": {"writable": false},
                "isDirectory": {"writable": false},
                "created": {"writable": false},
                "modified": {"writable": false},
                "fileSize": {"writable": false},
                "length": {"writable": false},
                "parent": {"writable": false},
                "path": {"writable": false},
                "name": {"writable": false},
                "fullPath": {"writable": false}
            });
            Object.freeze(this);
            //*/
        }

        // ****************************** misc stuff *****************************

        /**Function returns false if path contains certain strings. These strings are:
         * '?'
         * '*'
         * '..'
         * '/..'
         * '../'
         * './'
         * or a whole path is just a single dot.
         * @private
         */
        function validatePath(path) {
            return !path.match(/\/\.\.|\.\.\/|\.\/|^\.$|\?|\*/);
        }

        /**This function calls <code>validatePath</code> to validate the path and trims trailing slash, if any.
         * @param {String}path a path to check
         * @returns {Mixed} trimmed path or false if not validated.
         * @private
         */
        function normalizePath(path) {
            if (validatePath(path)) {
                return path.replace(/\/$/, "");
            }
            return false;
        }

        /**Function returns a device filesystem path to a given virtual root.
         * @param {String}vRoot name of virtual root to check
         * @returns {String} real path to virtual root or empty string on error
         * @private
         */
        function getRootRealPath(vRoot){
            var ret = RequestManager.sendRequest("FileStat", [vRoot]);
            if (ret && ret.RealPath) {
                return ret.RealPath.replace(/\/\.$/, '');
            }
            return "";
        }

        /**Helper function to use instead of real WACFile methods for objects that are not directories.
         * Used for asynchronous functions.
         * @param {Mixed}x irrelevant. Normally this would be success callback.
         * @param {Function}f error callback. If present, called with appropriate error message. Otherwise, an error is thrown.
         * @private
         * @throws {IO_ERR} "This method can only be invoked on a directory."
         */
        function notADir(x, f) {
            var err = ErrorsHelper.createWebAPIException_byType('IOError');
            if (typeof f === "function") {
                f(err);
                return null;
            }
            throw err;
        }

        /**Helper function to use instead of real WACFile methods for objects that are not directories.
         * Used for synchronized calls.
         * @private
         * @throws {IO_ERR} "This method can only be invoked on a directory."
         */
        function notADirSync() {
            throw ErrorsHelper.createWebAPIException_byType('IOError');
        }

        // ***************************** async methods *****************************
        // ***************************** private stuff *****************************

        /**Wrapper used to asynchronously "throw" an error using PendingOperation object.
         * @param {Object}er error structure returned by EMP
         * @param {PendingOperation}pendingOperation pending operation object required by RequestManager.
         * @returns {Boolean} result of finalizing operation
         * @private
         */
        function synchronouslyThrownErrorRethrowAsynchronously(er, pendingOperation){
            return RequestManager.finalize(pendingOperation.id, false, [(new WACError("Error: " + er.ErrorDescription, empErrorCodes[er.ErrorNumber]))]);
        }

        /**Worker function that does actual job reading file contents. Function uses setInterval to ensure
         * that the JavaScript thread is not blocked when reading large files. Moreover, function will fire
         * pending operation's error callback if it detects that a file to read is binary; exception is "raw" encoding,
         * which allows reading binary files as text.
         * @param {PendingOperation}pendingOperation pending operation object required by RequestManager.
         * @param {WACFile}ref an object representing file which contents are to be read
         * @param {String}encoding encoding of file. Right now only two values are accepted as valid: "UTF-8" and "raw".
         * @private
         */
        function readAsText(pendingOperation, ref, encoding) {
            var fs = null, str = "", buflen = 65535, intrvl = null; // ~64k buffer length

            if (encoding && !encoding.match(/utf[-]*8|raw/i)) {
                return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'))]);
            } else if (!encoding || encoding.match(/utf-?8/i)){
                encoding = true;
            } else {
                encoding = false;
            }

            function doRead(size){
                if (encoding) {
                    return fs.read(size);
                } else {
                    return atob(fs.readBase64(size));
                }
            }

            try {
                // this will throw if a FileStream is attempted on a directory
                fs = new FileStream(ref, "r", encoding);
            } catch (e) {
                return RequestManager.finalize(pendingOperation.id, false, [e]);
            }

            if (fs) {
                try {
                    if (ref.fileSize < buflen) {
                        str = doRead(ref.fileSize);
                        // whole file is read, closing...
                        fs.close();
                        return RequestManager.finalize(pendingOperation.id, true, [str]);
                    } else {
                        // hijacking pendingOperation's cancel() method
                        pendingOperation.cancel = function () {
                            clearInterval(intrvl);
                            RequestManager.finalize(pendingOperation.id, false, [(new WACError("Operation aborted.", "OP_ABORTED"))]);
                        };
                        intrvl = setInterval(function(){
                            // reading functions throw on errors
                            try {
                                str += doRead(buflen);
                            } catch (e) {
                                clearInterval(intrvl);
                                // can't read any more, closing file...
                                fs.close();
                                e.message += "; Text read so far: " + str;
                                RequestManager.finalize(pendingOperation.id, false, [e]);
                            }
                            if (fs.eof) {
                                clearInterval(intrvl);
                                // whole file is read, closing...
                                fs.close();
                                RequestManager.finalize(pendingOperation.id, true, [str]);
                            }
                        }, 0);
                    }
                } catch (e) {
                    e.message += "; Text read so far: " + str;
                    // can't read any more, closing file...
                    fs.close();
                    return RequestManager.finalize(pendingOperation.id, false, [e]);
                }
            }
            return false;
        }

        /**Internal function that creates a FileStream object. Used to hide RequestManager's
         * existence from end-user.
         * @param {PendingOperation}pendingOperation pending operation object required by RequestManager.
         * @param {WACFile}ref an object representing file which contents are to be streamed
         * @param {String}fsmode FileStream mode ("r" for reading, "w" or "a" for writing).
         * @param {String}encoding encoding of file. Right now only two values are accepted as valid: "UTF-8" and "raw".
         * @private
         */
        function openStream(pendingOperation, ref, fsmode, encoding) {
            var fs = null;
            try {
                fs = new FileStream(ref, fsmode, encoding);
                RequestManager.finalize(pendingOperation.id, true, [fs]);
            } catch (e) {
                RequestManager.finalize(pendingOperation.id, false, [e]);
            }
        }

        /**Internal function that is used by copyTo and moveTo WACFile methods. Errors are thrown asynchronously,
         * using error callback from callee. This function does not check if source or destination path
         * is read-only.
         * @param {PendingOperation}pendingOperation pending operation object required by RequestManager.
         * @param {WACFile}ref an object representing file which contents are to be streamed
         * @param {String}originFilePath file or directory path that is to be copied or moved
         * @param {String}destinationFilePath destination path
         * @param {Boolean}overwrite if target exists, should it be overwritten?
         * @param {String}mode mode in which virtual root was resolved (r/rw).
         * @private
         */
        function changeFiles(pendingOperation, ref, originFilePath, destinationFilePath, overwrite, mode) {
            var parsedOrigin = [], parsedDestination = [],
                relativePath = parsePath(ref.fullPath)[1], virtualRoot = parsePath(ref.fullPath)[0],
                EMPReturn = null;

            if (mode.match(/w/i) === null) {
                // NOT checking if folder is readOnly.
                return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR))]);
            }
            if (ref.isFile) {
                return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byType('IOError'))]);
            }

            if (!validatePath(destinationFilePath)) {
                return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR))]);
            }

            parsedOrigin = parsePath(originFilePath);
            parsedDestination = parsePath(destinationFilePath);

            if (parsedOrigin[0] !== virtualRoot || parsedOrigin[1].match(new RegExp(relativePath)) === null) {
                return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR))]);
            }

            if (!overwrite) {
                EMPReturn = RequestManager.sendRequest("FileStat", [parsedDestination[0], parsedDestination[1]]);
                if (!EMPReturn.ErrorNumber) {
                    // file exists
                    return RequestManager.finalize(pendingOperation.id, false, [(ErrorsHelper.createWebAPIException_byType('IOError'))]);
                }
            }

            EMPReturn = RequestManager.sendRequest([pendingOperation.id], pendingOperation.action, [virtualRoot, parsedOrigin[1], parsedDestination[0], parsedDestination[1]]);
            if (EMPReturn.ErrorNumber) {
                RequestManager.onEvent(EMPReturn.ErrorNumber, pendingOperation.id, (new WACError("Error: " + EMPReturn.ErrorDescription, empErrorCodes[EMPReturn.ErrorNumber])));
                return false;
            }
            return true;
        }

        /**Internal function that is used by deleteFile and deleteFolder WACFile methods. Errors are thrown asynchronously,
         * using error callback from callee. There is an EMPCallback for Delete, so RequestManager.onEvent is used instead
         * of RequestManager.finalize to throw errors.This function does not check if source or destination path is read-only.
         * EMP request has one additional argument, "force", which forces EMP to delete directories that contain read-only files.
         * This argument is always on, because WAC File System API does not provide any means to have it input by a user.
         * @param {PendingOperation}pendingOperation pending operation object required by RequestManager.
         * @param {WACFile}ref an object representing file which contents are to be streamed
         * @param {String}mode mode in which virtual root was resolved (r/rw).
         * @param {String}path file or directory path that is to be deleted
         * @param {String}type type of object to delete ("F"ile or "D"irectory)
         * @param {Boolean}recursive should non-empty directories be removed?
         * @private
         */
        function deleteObjects(pendingOperation, ref, mode, path, type, recursive) {
            var o = (type === "D") ? "Directory" : "File", parsedPath = [],
                relativePath = parsePath(ref.fullPath)[1], virtualRoot = parsePath(ref.fullPath)[0],
                EMPReturn = null;

            if (mode.match(/w/i) === null || ref.readOnly) {
                RequestManager.onEvent(true, pendingOperation.id, (ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR)));
                return false;
            }
            if (!validatePath(path)) {
                RequestManager.onEvent(true, pendingOperation.id, (ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR)));
                return false;
            }
            if (ref.isFile) {
                RequestManager.onEvent(true, pendingOperation.id, (ErrorsHelper.createWebAPIException_byType('IOError')));
                return false;
            }
            if (type === "D" && !recursive && ref.length) {
                RequestManager.onEvent(true, pendingOperation.id, (ErrorsHelper.createWebAPIException_byType('IOError')));
                return false;
            }

            parsedPath = parsePath(path);

            if (parsedPath[0] !== virtualRoot || parsedPath[1].match(new RegExp(relativePath)) === null) {
                RequestManager.onEvent(true, pendingOperation.id, (ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR)));
                return false;
            }
            pendingOperation.path = path;
            EMPReturn = RequestManager.sendRequest([pendingOperation.id], "Delete", [virtualRoot, parsedPath[1], type, "force"]); // always delete, even if r/o
            if (EMPReturn.ErrorNumber) {
                RequestManager.onEvent(EMPReturn.ErrorNumber, pendingOperation.id, EMPReturn);
                return false;
            }
            return true;
        }

        // ***************************** public stuff *****************************

        // Fake methods in prototype for objects that are files.
        // They will get overwritten by object methods, if object represents directory.
        WACFile.prototype.createDirectory = notADirSync;
        WACFile.prototype.createFile = notADirSync;
        WACFile.prototype.deleteDirectory = notADir;
        WACFile.prototype.deleteFile = notADir;
        WACFile.prototype.copyTo = notADir;
        WACFile.prototype.moveTo = notADir;
        WACFile.prototype.resolve = notADirSync;
        WACFile.prototype.listFiles = notADir;

        /**Function that creates a FileStream object.
         * @param {Function}successCB function to call after successful operation
         * @param {Function}errorCB function to call after error
         * @param {String}fsmode FileStream mode ("r" for reading, "w" or "a" for writing).
         * @param {String}encoding encoding of file. Right now only two values are accepted as valid: "UTF-8" and "raw".
         * @returns {PendingOperation} a PendingOperation object
         */
        WACFile.prototype.openStream = function (successCB, errorCB, fsmode, encoding) {
            var pendingOperation = null, ref = this;
            if (typeof errorCB != "function" && !encoding) {
                encoding = fsmode;
                fsmode = errorCB;
                errorCB = function () {};
            }
            pendingOperation = PendingOperationFactory.get(successCB, errorCB);
            setTimeout(function () {
                openStream(pendingOperation, ref, fsmode, encoding);
            }, 0);
            return pendingOperation;
        };

        /**Function that reads file contents as text. This is the only asynchronous operation
         * to synchronously throw an error (according to WAC File System API specification, it MUST throw
         * if success callback is not provided or when invalid encoding has been given). It calls an internal
         * worker function that does all the dirty job in separate thread and returns a PendingOperation object
         * which enables user to cancel (potentially lengthy) operation.
         * @param {Function}successCB function to call after successful operation
         * @param {Function}errorCB function to call after error
         * @param {String}encoding encoding of file. Right now only two values are accepted as valid: "UTF-8" and "raw".
         * @returns {PendingOperation} a PendingOperation object
         */
        WACFile.prototype.readAsText = function (successCB, errorCB, encoding) {
            var pendingOperation = null, ref = this;

            // this strange behaviour is required by the spec. Sigh.
            if (typeof successCB !== "function" || (encoding && typeof encoding !== "string")) {
                throw ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.TYPE_MISMATCH_ERR);
            }

            pendingOperation = PendingOperationFactory.get(successCB, errorCB);
            setTimeout(function () {
                readAsText(pendingOperation, ref, encoding);
            }, 0);
            return pendingOperation;
        };

        // ***************************** sync methods *****************************
        // **************************** private stuff *****************************

        /**Internal function used by WACFile's two methods: createFile and createDirectory.
         * @param {String}path path to file or directory to be created
         * @param {String}type type of object to create ("F"ile or "D"irectory)
         * @param {WACFile}fileObj parent folder
         * @param {String}mode mode of virtual root (r/rw; in this case it must be rw)
         * @returns {WACFile} a handle to created file system object
         * @throws {IO_ERR}
         * @throws {SECURITY_ERR}
         * @throws {UNKNOWN_ERR}
         * @private
         */
        function create(path, type, fileObj, mode) {
            var EMPReturn = null,
                virtualRoot = parsePath(fileObj.fullPath)[0],
                relativePath = parsePath(fileObj.fullPath)[1];

            EMPReturn = RequestManager.sendRequest("Create", [virtualRoot, relativePath + "/" + path, type]);
            if (!EMPReturn.ErrorNumber) {
                EMPReturn = RequestManager.sendRequest("FileStat", [virtualRoot, relativePath + "/" + path]);
                return new WACFile(EMPReturn, fileObj, mode);
            } else {
                throw new WACError("Error: " + EMPReturn.ErrorDescription, empErrorCodes[EMPReturn.ErrorNumber]);
                return false;
            }
        }

        /**Sends a FileStat command to EMP.
         * @param {String}fpath path to file or directory to be resolved
         * @param {WACFile}fileObj parent folder
         * @param {String}mode mode of virtual root
         * @returns {Mixed} null or WACFile handle to resolved file system object
         * @throws {IO_ERR}
         * @throws {SECURITY_ERR}
         * @throws {UNKNOWN_ERR}
         * @private
         */
        function resolveSync(fpath, fileObj, mode) {
            var FileData = RequestManager.sendRequest("FileStat", [parsePath(fileObj.fullPath)[0], parsePath(fileObj.fullPath)[1] + "/" + fpath]);
            if (FileData.ErrorNumber) {
                alert("Error resolving path " + fileObj.fullPath + "/"+ fpath + ": " + FileData.ErrorDescription);
                return null;
            }
            return new WACFile(FileData, fileObj, mode);
        }

        // ***************************** public stuff *****************************

        /**Function returns URI to file system object. A URI is de facto an address
         * of object on device's file system. This way user does not have to read
         * contents of file if they just want to display an image; they can resolve image
         * to a WACFile handle, call .toURI method and use a plain, good, old <IMG/> tag
         * with its attribute "src" filled with the product of .toURI.
         *
         * Path is given relative where possible. In all other cases it is absolute.
         *
         * @returns {String} URI in a form of browser-understandable path to file
         */
        WACFile.prototype.toURI = function () {
            var virtualRoot = parsePath(this.fullPath)[0],
                relativePath = parsePath(this.fullPath)[1].replace(/^\//, ''),
                realPath = getRootRealPath(virtualRoot).replace(/^\//, '');

            if (virtualRoot === 'wgt-package') {
                return relativePath;
            }
            if (virtualRoot.match(/wgt-private|w3/i)) {
                return "RootFS/" + this.fullPath;
            }
            return "file:///" + escape(realPath + "/" + relativePath);
        };

        /**This function is not required by WAC File API spec, but I have found it useful.
         * It automatically generates a string describing shortly the WACFile object.
         * It is used by procedures such as <code>window.alert</code>, so the developers
         * will have convenient way to display WACFile objects.
         * @returns {String} description of WACFile object.
         */
        WACFile.prototype.toString = function () {
            return (this.isFile ? "File " : "Directory ") + this.fullPath + ", " + (this.fileSize ? "size: " + this.fileSize : "length: " + this.length);
        };

        return WACFile;
    }());

    /**Internal function that does webapis.filesystem.resolve's dirty work.
     * It initializes RequestManager (if not yet initialized) and resolves given
     * virtual root.
     * @param {Function}successCB function to call after successful operation
     * @param {Function}errorCB function to call after error
     * @param {String}location name of virtual root to resolve
     * @param {String}mode mode in which virtual root shall be resolved (r/rw)
     * @returns {PendingOperation} a PendingOperation object
     * @private
     */
    function resolve(successCB, errorCB, location, mode) {
        var pendingOperation = PendingOperationFactory.get(successCB, errorCB);
        setTimeout(function () {
            var EMPReturn, file;
            if (location  === 'wgt-package' && mode === "rw") {
                RequestManager.finalize(pendingOperation.id, false, [ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.SECURITY_ERR)]);
            }
            RequestManager.init();
            EMPReturn = RequestManager.sendRequest("FileStat", [location]);
            try {
                file = new WACFile(EMPReturn, null, mode);
                RequestManager.finalize(pendingOperation.id, true, [file]);
            } catch (e) {
                RequestManager.finalize(pendingOperation.id, false, [e]);
            }
        }, 0);
        return pendingOperation;
    }

    // return the final object. Expose only what is described by API.
    // Food for thought: should constructors (WACFile, WACError etc) be exposed?
    return {
        // this value should be given by EMP. It is not.
        maxPathLength: MAXINT,
        /**Main API entry point. This function begins each and every call.
         * It initializes the whole API (if not yet initialized) and resolves given
         * virtual root.
         * @param {Function}successCB function to call after successful operation
         * @param {Function}errorCB function to call after error
         * @param {String}location name of virtual root to resolve
         * @param {String}mode mode in which virtual root shall be resolved (r/rw)
         * @returns {PendingOperation} a PendingOperation object
         */
        resolve: function (successCB, errorCB, location, mode) {
            if (typeof successCB !== "function") {
                throw ErrorsHelper.createWebAPIException_byType('InvalidValuesError');
            }
            if (typeof errorCB === "string" && !mode) {
                // no error callback provided: let's shift up arguments
                mode = location;
                location = errorCB;
            }
            if (!location) {
                errorCB(ErrorsHelper.createWebAPIException_byType('InvalidValuesError'));
                return null; // normally it would return an object.
            }
            return resolve(successCB, errorCB, location, mode);
        },
        jasmine: {
            RequestManager: RequestManager,
            WACError: WACError,
            WACFile: WACFile,
            FileStream: FileStream,
            PendingOperation: PendingOperation
        }
    };

}());
// end

function CFILE(path, mode){
	var fs = new FileSystem();
	var fp = null;
	
	alert("path : " + path + ", mode : " + mode);
	
	/*if (path.slice(0, 7) == "$COMMON") {
		var tempPath = path.slice(8);
		var fp = fs.openCommonFile(path, pMode);
	}
	else {
		alert("Not supported currently.")
		return false;
	}*/
	
	//Write
	if (mode == 'w') {
		//File write from COMMON area
		if (path.slice(0, 7) == "$COMMON") {
			var tempPath = path.slice(8);
			
			/*var dirPath = getDirPath(path);
			var filePath = getFilePath(path);
			var fullPath = dirPath.toString() + "/" + filePath.toString();
			
			if (dirPath) {
				//Directory is not exist.
				if (fs.isValidCommonPath(dirPath) == false) {
					return false;
				}
				//Directory is exist.
				else {
					var fp = fs.openCommonFile(fullPath, "w");
				}
			}
			else {
				var fp = fs.openCommonFile(fullPath, "w");
			}*/
			var fp = fs.openCommonFile(tempPath, "w");
		}
		//File write on each app's area
		else {
			alert("Not supported currently.")
			return false;
		}
	}
	//Read
	else {
		//File read from COMMON area
		if (path.slice(0, 7) == "$COMMON") {
			var tempPath = path.slice(8);
			
			/*var dirPath = getDirPath(path);
			var filePath = getFilePath(path);
			var fullPath = dirPath.toString() + "/" + filePath.toString();
			
			if (dirPath) {
				//Directory is not exist.
				if (fs.isValidCommonPath(dirPath) == false) {
					alert("Directory is not exist");
					return false;
				}
				//Directory is exist.
				else {
					var fp = fs.openCommonFile(fullPath, "r");
					alert("Directory is not exist");
					var strResult = fp.readAll();
					alert("strResult : " + strResult);
				}
			}
			else {
				var fp = fs.openCommonFile(fullPath, "r");
				var strResult = fp.readAll();
				alert("strResult : " + strResult);
			}*/
			var fp = fs.openCommonFile(tempPath, "r");
			//var strResult = fp.readAll();
			//alert("strResult : " + strResult);
		}
		//File read on each app's area
		else {
			/*var dirPath = getDirPath(pPath);
			var filePath = getFilePath(pPath);
			var fullPath = dirPath.toString() + "/" + filePath.toString();
			
			if (dirPath) {
				//Directory is not exist.
				if (fs.isValidCommonPath(dirPath) == false) {
					return false;
				}
				//Directory is exist.
				else {
					var fp = fs.openFile(fullPath, "r");
				}
			}
			else {
				var fp = fs.openFile(fullPath, "r");
			}*/
			alert("Not supported currently.")
			return false;
		}
	}
	alert("CFILE fp : " + fp);
	
	if (!fp) {
		return false;
	}
	
	this.readAsText = function(){
		alert("readAsText");
		alert("fp : " + fp);
		var strResult = fp.readAll();
		alert("strResult : " + strResult);
		return strResult.toString();
	}
	
	this.readAll = function(){
		return fp.readAll();
	}
	
	this.readBytes = function(){
		return fp.readLine();
	}
	
	this.writeAll = function(str){
		alert("writeAll : " + str);
		fp.writeAll(str);
		fs.closeCommonFile(fp);
		return true;
	}
	
	this.writeBytes = function(str){
		alert("writeBytes : " + str);
		fp.writeLine(str);
		fs.closeCommonFile(fp);
		return true;
	}
}

function getDirPath ( pPath ) {
	alert("getDirPath("+pPath+")")
	if( pPath.indexOf('/') == 0 ) {
		var path = pPath.slice( 1 );
		var index = path.lastIndexOf('/');
		
		if( index >= 0 )	{
			var dirPath = path.slice( 0, index );
			alert("dirPath : " + dirPath);
			return dirPath;
		}
		else {
			return false;
		}
	}
	else {
		var index = pPath.lastIndexOf('/');
		if( index >= 0 )	{
			var dirPath = pPath.slice( 0, index );
			alert("dirPath : " + dirPath);
			return dirPath;
		}
		else {
			return false;
		}
	}
}

function getFilePath ( pPath ) {
	alert("getFilePath("+pPath+")")
	var index = pPath.lastIndexOf('/');
	if( index >= 0 ) {
		var filePath = pPath.slice( index + 1 );
		alert("filePath : " + filePath);
		return filePath;
	}
	else {
		return pPath;
	}
}

webapis.widgetevent = {};
webapis.widgetevent._eventListener = {};
webapis.widgetevent._listener = function(eventArgs){
    alert('[WidgetEvent] Type : ' + eventArgs.type);
    var fnListener = webapis.widgetevent._eventListener[eventArgs.type];
    if (typeof fnListener == 'function') {
        fnListener(eventArgs.data || null);
    }
    else {
        alert('[WidgetEvent] No event handler binded with this event : ' + eventArgs.type);
    }
}
webapis.widgetevent._registerListener = function(){
    curWidget.onWidgetEvent = webapis.widgetevent._listener;
}
webapis.widgetevent.setEventListener = function(type, callback){
    alert('[WidgetEvent] setEventListener(' + type + ')');
    webapis.widgetevent._eventListener[type] = callback;
	webapis.widgetevent._registerListener();
}


/*
 * Private
 * Function: _Plugin
 * 	Call the plugin function. This support Legacy(2010') and SEF(2011 or later) plugin.
 */
webapis._plugin = function(options) {	
	var bSEFAvailable = webapis._plugin.getSEFAvailable();	
	var args = Array.prototype.slice.call(arguments);	// make arguments to Array
	var pluginObj = null;
	alert('[webapis] _plugin('+args+')');	
	//////////////////////////////////////////////////////
	// 첫번째 Parameter: Plugin Name or Plugin Object
	// String: 플러그인 생성후 Method 호출	
	if (typeof arguments[0] == 'string') {
		var plugin = getPluginName(arguments[0]);
		if (!plugin) {
			alert("Plugin name is not defined : " + arguments[0]);
			return null;
		}
		var pluginObjPrefix = "_plugin_"
		pluginObj = document.getElementById(pluginObjPrefix + plugin);
		if(!pluginObj) {
			pluginObj = addPluginObject(plugin, bSEFAvailable);
			
		}
	}
	// Object: 해당 Plugin Object의 Method 호출
	else {
		pluginObj = arguments[0];
	}
	
	if (!pluginObj) {
		alert("CANNOT get the plugin object ("+plugin+")");
		return null;
	}
	
	//////////////////////////////////////////////////////
	// 두번째 Parameter: Method or undefined(get Plugin object)
	if(args.length == 1 && typeof arguments[0] == 'string') {	// Getting plugin object
		alert("Getting Plugin Object: " + pluginObj);
		return pluginObj;
	}
	var method = arguments[1];
	
	//////////////////////////////////////////////////////
	// Wrapped Method. (Legacy와 SEF 공용)
	if(webapis._plugin.wrappedMethod[method]) {
		return webapis._plugin.wrappedMethod[method](pluginObj, bSEFAvailable, args);
	}
	
	//////////////////////////////////////////////////////
	// Call the Method
	// Legacy plugin
	if(!bSEFAvailable) {
		var retValue = null;
		if (typeof pluginObj[method] == 'function') {
			args = args.slice(2);	// from first parameter
			retValue = pluginObj[method].apply(pluginObj, args);
		}
		else {
			retValue =  webapis._pluginDef.PLR_NOT_IMPLEMENT;
		}
		alert("\treturns " + retValue);
		return retValue;
	}
	// SEF plugin
	else {
		var retValue = null;
		args = args.slice(1);	// from Method name
		try {
			retValue = pluginObj.Execute.apply(pluginObj, args);
		}
		catch(e) {
			alert("EXCEPTION(Execute.apply): " + e);
		}
		alert("\treturns " + retValue);
		return retValue;
	}
	
	// SEF에서 Open을 위해 정확한 대소문자형태의 플러그인 이름을 얻어내는 역할. 리스트에 없는 경우엔 그대로 리턴한다.
	function getPluginName(name) {
		alert("getPluginName("+name+")");
		var SEFPluginName = ['TV', 'TVMW', 'NNavi', 'Audio', 'AppCommon', 'FrontPanel', 'ImageViewer', 'Player', 'AUI',
							 'Storage', 'Network', 'Download', 'Screen', 'Time', 'Video', 'Window',
							 'ExternalWidgetInterface', 'FileSystem', 'Gamepad', 'Michrophone',
							 'CustomDevice', 'MIDIDevice','RECOG', 'AllShare'];
		for(var i=0; i<SEFPluginName.length; i++) {
			if(SEFPluginName[i].toUpperCase() == name.toUpperCase()) {
				alert('\treturn '+SEFPluginName[i]);
				return SEFPluginName[i];
			}
		}
		alert('\treturn(as it is) '+name);
		return name;
	}
	
	function addPluginObject(plugin, bSEF) {
		alert("addPluginObject("+plugin+","+bSEF+")");
        var html = '';
		var containerID = '_pluginObject'+plugin+'Container_';
		var domNode = document.createElement('div');
			domNode.id = containerID;
			domNode.style.position = 'absolute';
			domNode.style.left = '0px';
			domNode.style.top = '0px';
			//domNode.style.zIndex = $.sf.define.zIndex.plugin;
			document.body.appendChild(domNode);
		
		var bVisible = isVisiblePlugin(plugin);
		var style = bVisible ? 'display:block;position:absolute;width:960px;height:540px;' : 'display:block;position:absolute;width:0px;height:0px;';
		
		if (!bSEF) {
			html = '<OBJECT id="' + pluginObjPrefix + plugin + '" classid="clsid:SAMSUNG-INFOLINK-' + plugin + '" style="'+style+'"></OBJECT>';
			alert("Create " + plugin + " Plugin : " + html);
			domNode.innerHTML += html;
        	return document.getElementById(pluginObjPrefix + plugin);
		}
		else {
			html = '<OBJECT id="' + pluginObjPrefix + plugin + '" classid="clsid:SAMSUNG-INFOLINK-SEF" style="'+style+'"></OBJECT>';
			alert("Create SEF Plugin for " + plugin + ": " + html);
			domNode.innerHTML += html;
			var opensuccess = document.getElementById(pluginObjPrefix + plugin).Open(plugin, "1.000", plugin);
			alert("SEF Open Returns " + opensuccess);
			if (parseInt(opensuccess, 10) == 1) {
				alert("SEF Open Succeded!!!");
				return document.getElementById(pluginObjPrefix + plugin);
			}
			else {
				alert("SEF Open Failed!!!");
        		return null;
			}
		}
	}
	
	function isVisiblePlugin(pluginName) {
		pluginName = pluginName.toUpperCase();
		return (pluginName == 'PLAYER' || pluginName == 'IMAGEVIEWER'); //|| pluginName == 'WINDOW');	// show()
	}
}
webapis._plugin.getSEFAvailable = function () {
	alert("webapis._plugin.getSEFAvailable()");
	
	var browserVersion = navigator.appVersion.toLowerCase();
	//if (browserVersion.indexOf('applewebkit') >= 0 && (browserVersion.indexOf('smarttv') >= 0 || browserVersion.indexOf('smarthub') >= 0)) {
	if(navigator.userAgent.toLowerCase().indexOf('applewebkit')>=0 && window.curWidget) {
		alert("2012's Webkit platform");
		return true; // 2012
	}
	else {
		var tStrArr = new Array();
		tStrArr = browserVersion.split("(");
		if (parseInt(tStrArr[0], 10) < 6.0) {
			alert("2010's or earlier platform");
			return false; // 2010 or lower
		}
		else {
			alert("2011's platform");
			return true; // 2011
		}
	}
}
webapis._plugin.wrappedMethod = {
	'GetVersion': function(plugin, isSEF, args){
		alert("[webapis] Wrapped Method GetVersion("+plugin+", "+isSEF+")");
		if (!isSEF) {
			var pluginVer = plugin.GetPluginInfo(webapis._pluginDef.PL_CMN_INFO_VERSION);
			alert("\tLegacy returns " + pluginVer);
			return {
				isSEF: false,
				ver: pluginVer
			};
		}
		else {
			var pluginVer = plugin.Execute("GetVersion");
			alert("\tSEF returns " + pluginVer);
			return {
				isSEF: true,
				ver: pluginVer
			};
		}
	}
}

webapis._pluginDef = {
    // Video Plugin >> Auto Motion
    PL_VIDEO_AUTO_MOTION_OFF: 1,
    PL_VIDEO_AUTO_MOTION_WEAK: 2,
    PL_VIDEO_AUTO_MOTION_MEDIUM: 3,
    PL_VIDEO_AUTO_MOTION_STRONG: 4,
    PL_VIDEO_AUTO_MOTION_CUSTOM: 5,
    PL_VIDEO_AUTO_MOTION_DEMO: 6,
    PL_VIDEO_AUTO_MOTION_RESET: 7, // RESET은 현재설정된 값을 그대로 사용합니다. 즉 변화가 없습니다.
    PL_VIDEO_AUTO_MOTION_CHECK: 8,
    PL_VIDEO_AUTO_MOTION_SUPPORT: 9,
    
    // PLUGIN_DISPLAY_PANEL_TYPE
    // Function : GetDisplayPanelType
    PL_DEVICE_DPT_LCD: 0,
    PL_DEVICE_DPT_PDP: 1,
    PL_DEVICE_DPT_DLP: 2,
    PL_DEVICE_DPT_LED: 3,
    PL_DEVICE_DPT_LED_EDGE: 4,
    PL_DEVICE_DPT_LED_DIRECT: 5,
    PL_DEVICE_DPT_CRT: 6,
    PL_DEVICE_DPT_UNKNOWN: 999,
    PL_DEVICE_DPT_MAX: 1000,
    
    // PLUGIN_NETWORK_TYPE
    // Function : GetNetworkType
    PL_NT_WIRED: 0,
    PL_NT_WIRELESS: 1,
    PL_NT_UNKNOWN: 2,
    
    // PL_SERVER_TYPE
    // Function : GetServerType
    PL_ST_SERVICE: 0,
    PL_ST_DEVELOPMENT: 1,
    PL_ST_DEVELOPING: 2,
    PL_ST_UNKNOWN: 3,
    
    // PL_TV_USE_TYPE
    // Function : GetTVUseMode
    PL_TVUT_HOME: 0,
    PL_TVUT_SHOP: 1,
    PL_TVUT_UNKNOWN: 2,
    
    // PL_PROFILE_ID
    // Function : GetProfile
    PL_PRFID_TICKER_ID: 0,
    PL_PRFID_CHILDLOCK_PIN: 1,
    PL_PRFID_HUB_TVID: 2,
    PL_PRFID_TICKER_AUTOBOOT: 3,
    PL_PRFID_TICKER_DURATION: 4,
    PL_PRFID_WIDGET_DPTIME: 5,
    PL_PRFID_CONTRACT: 6,
    PL_PRFID_TICKER_SAFE: 7,
    PL_PRFID_RESET: 8,
    PL_PRFID_PASSWD_RESET: 9,
    PL_PRFID_GEOIP_STATUS: 10,
    PL_PRFID_COUNTRY_CODE: 11,
    PL_PRFID_WLAN_DEFAULT_NETWORK: 12,
    PL_PRFID_AUTO_PROTECTION_TIME: 13,
    PL_PRFID_CHANNEL_BOUND_EXECUTE: 14,
    PL_PRFID_PASSWORD: 15,
    PL_PRFID_SOURCE_BOUND_WIDGET_ID: 16,
    PL_PRFID_SOURCE_BOUND_SOURCE_ID: 17,
    PL_PRFID_AUTOBOOT: 18,
    PL_PRFID_UNKNOWN: 19,
    PL_PRFID_MAX: 20,
    
    // 이시형 선임 지정값 : plugin manual 참고
    PL_TVMW_KEY_1: 0, // NUMBER
    PL_TVMW_KEY_2: 1,
    PL_TVMW_KEY_3: 2,
    PL_TVMW_KEY_4: 3,
    PL_TVMW_KEY_5: 4,
    PL_TVMW_KEY_6: 5,
    PL_TVMW_KEY_7: 6,
    PL_TVMW_KEY_8: 7,
    PL_TVMW_KEY_9: 8,
    PL_TVMW_KEY_0: 9,
    PL_TVMW_KEY_HYPHEN: 10,
    
    PL_TVMW_KEY_CH_UP: 100, // CHANNEL
    PL_TVMW_KEY_CH_DOWN: 101,
    PL_TVMW_KEY_CH_PREV: 102,
    PL_TVMW_KEY_CH_FAV: 103,
    PL_TVMW_KEY_CH_LIST: 104,
    PL_TVMW_KEY_CH_PANNEL_UP: 105,
    PL_TVMW_KEY_CH_PANNEL_DOWN: 106,
    
    PL_TVMW_KEY_VOL_UP: 200, // VOLUME
    PL_TVMW_KEY_VOL_DOWN: 201,
    PL_TVMW_KEY_MUTE: 202,
    PL_TVMW_KEY_VOL_PANNEL_UP: 203,
    PL_TVMW_KEY_VOL_PANNEL_DOWN: 204,
    
    PL_TVMW_KEY_ENTER: 300, // NAVI
    PL_TVMW_KEY_RETURN: 301,
    PL_TVMW_KEY_EXIT: 302,
    PL_TVMW_KEY_ARROW_UP: 303,
    PL_TVMW_KEY_ARROW_DOWN: 304,
    PL_TVMW_KEY_ARROW_LEFT: 305,
    PL_TVMW_KEY_ARROW_RIGHT: 306,
    PL_TVMW_KEY_WHEEL_LEFT: 307,
    PL_TVMW_KEY_WHEEL_RIGHT: 308,
    PL_TVMW_KEY_PANNEL_ENTER: 309,
    
    
    PL_TVMW_KEY_RED: 400, // COLOR
    PL_TVMW_KEY_GREEN: 401,
    PL_TVMW_KEY_YELLOW: 402,
    PL_TVMW_KEY_CYAN: 403,
    
    PL_TVMW_KEY_REWARD: 500, // PLAYBACK
    PL_TVMW_KEY_PAUSE: 501,
    PL_TVMW_KEY_FOWARD: 502,
    PL_TVMW_KEY_PLAY: 503,
    PL_TVMW_KEY_STOP: 504,
    PL_TVMW_KEY_REC: 505,
    
    PL_TVMW_KEY_INFO: 600, // ETC
    PL_TVMW_KEY_TOOLS: 601,
    PL_TVMW_KEY_INFOLINK: 602,
    PL_TVMW_KEY_EMODE: 603,
    PL_TVMW_KEY_DATA: 604,
    PL_TVMW_KEY_DMA: 605,
    PL_TVMW_KEY_CONTENTS: 606,
    PL_TVMW_KEY_MENU: 607,
    PL_TVMW_KEY_WISELINK: 608,
    PL_TVMW_KEY_SOURCE: 609,
    PL_TVMW_KEY_POWER: 610,
    PL_TVMW_KEY_TV: 611,
    PL_TVMW_KEY_PANNEL_SOURCE: 612,
    PL_TVMW_KEY_PANNEL_MENU: 613,
    PL_TVMW_KEY_PANNEL_POWER: 614,
    PL_TVMW_KEY_LOCK: 1119,
    PL_TVMW_KEY_HDMI: 1139,
    
    PL_TVMW_KEY_GRP_ALL: 0,
    PL_TVMW_KEY_GRP_NUMBER: 1,
    PL_TVMW_KEY_GRP_CHANNEL: 2,
    PL_TVMW_KEY_GRP_VOLUME: 3,
    PL_TVMW_KEY_GRP_NAVI: 4,
    PL_TVMW_KEY_GRP_COLOR: 5,
    PL_TVMW_KEY_GRP_PLAYBACK: 6,
    PL_TVMW_KEY_GRP_ETC: 7,
    
    // Added by ck1.seo@samsung.com
    // For all key regist
    // 3 April 2009
    PL_TVMW_KEY_TTX_MIX: 650,
    PL_TVMW_KEY_GUIDE: 651,
    PL_TVMW_KEY_SUBTITLE: 652,
    PL_TVMW_KEY_ASPECT: 653,
    PL_TVMW_KEY_DOLBY_SRR: 654,
    PL_TVMW_KEY_MTS: 655,
    
    // Added by jh.kim For BD
    PL_TVMW_KEY_11: 11,
    PL_TVMW_KEY_REPEAT: 656,
    
    // IME
    PL_TVMW_KEY_DISC_MENU: 1086,
    PL_TVMW_KEY_12: 1057,
    PL_TVMW_KEY_STEP: 1023, // REC PAUSE(BD)
    PL_TVMW_KEY_CALLER_ID: 1128, // FULL SCREEN(BD)
    PL_TVMW_KEY_3D: 1219,
    PL_TVMW_KEY_ZOOM1: 1083, // YAHOO
    PL_TVMW_KEY_ANTENA: 1054, // for CN
    PL_APPCOMMON_KEY_DVR: 1114,
    PL_APPCOMMON_KEY_HOME: 1118,
    PL_APPCOMMON_KEY_TV_SNS: 1251,
    PL_APPCOMMON_KEY_SEARCH: 1252,
    PL_APPCOMMON_KEY_PIP_SCAN: 1049, // BD vTuner HotKey
    PL_APPCOMMON_KEY_DEVICE_CONNECT: 1052, // BD Netflix HotKey
    PL_APPCOMMON_KEY_DOTCOM: 1253,
    
    // Country code
    // Function : GetCountry
    PL_TVMW_COUNTRY_USA: 0,
    PL_TVMW_COUNTRY_KOR: 1,
    PL_TVMW_COUNTRY_SPA: 2,
    PL_TVMW_COUNTRY_FRA: 3,
    PL_TVMW_COUNTRY_JPN: 4,
    PL_TVMW_COUNTRY_EU: 5,
    PL_TVMW_COUNTRY_UK: 6,
    PL_TVMW_COUNTRY_GERMANY: 7,
    PL_TVMW_COUNTRY_ITALY: 8,
    PL_TVMW_COUNTRY_SWEDEN: 9,
    PL_TVMW_COUNTRY_BULGARIA: 10, //10
    PL_TVMW_COUNTRY_CROATIA: 11,
    PL_TVMW_COUNTRY_CZECH: 12,
    PL_TVMW_COUNTRY_NETHERLANDS: 13,
    PL_TVMW_COUNTRY_GREECE: 14,
    PL_TVMW_COUNTRY_HUNGARY: 15,
    PL_TVMW_COUNTRY_POLAND: 16,
    PL_TVMW_COUNTRY_PORTUGAL: 17,
    PL_TVMW_COUNTRY_ROMANIA: 18,
    PL_TVMW_COUNTRY_RUSSIA: 19,
    PL_TVMW_COUNTRY_SWITZERLAND: 20, //20
    PL_TVMW_COUNTRY_TURKEY: 21,
    PL_TVMW_COUNTRY_AUSTRALIA: 22,
    PL_TVMW_COUNTRY_AUSTRIA: 23,
    PL_TVMW_COUNTRY_BELGIUM: 24,
    PL_TVMW_COUNTRY_DENMARK: 25,
    PL_TVMW_COUNTRY_FINLAND: 26,
    PL_TVMW_COUNTRY_NORWAY: 27,
    PL_TVMW_COUNTRY_CHINA: 28,
    PL_TVMW_COUNTRY_IRELAND: 29,
    PL_TVMW_COUNTRY_SERBIA: 30, //30
    PL_TVMW_COUNTRY_SAN_MARINO: 31,
    PL_TVMW_COUNTRY_MONACO: 32,
    PL_TVMW_COUNTRY_BRAZIL: 33,
    PL_TVMW_COUNTRY_HONGKONG: 34,
    PL_TVMW_COUNTRY_TAIWAN: 35,
    PL_TVMW_COUNTRY_NEWZEALAND: 36,
    PL_TVMW_COUNTRY_SLOVAKIA: 37,
    PL_TVMW_COUNTRY_SINGAPORE: 38,
    PL_TVMW_COUNTRY_GENERALCABLE: 39,
    PL_TVMW_COUNTRY_OTHER: 40,
    PL_TVMW_COUNTRY_ASIAWEUROPE_ANALOG: 41,
    PL_TVMW_COUNTRY_HONGKONG_UK_ANALOG: 42,
    PL_TVMW_COUNTRY_NZL_INDONESIA_ANALOG: 43,
    PL_TVMW_COUNTRY_SOUTH_AFRICA_ANALOG: 44,
    PL_TVMW_COUNTRY_AMERICA_ANALOG: 45,
    PL_TVMW_COUNTRY_CHINA_ANALOG: 46,
    
    // Country code
    // Function : GetCountry
    PL_TV_COUNTRY_USA: 0,
    PL_TV_COUNTRY_KOR: 1,
    PL_TV_COUNTRY_SPA: 2,
    PL_TV_COUNTRY_FRA: 3,
    PL_TV_COUNTRY_JPN: 4, 
    PL_TV_COUNTRY_EU: 5,  
    PL_TV_COUNTRY_UK: 6,
    PL_TV_COUNTRY_GERMANY: 7,
    PL_TV_COUNTRY_ITALY: 8, 
    PL_TV_COUNTRY_SWEDEN: 9, 
    PL_TV_COUNTRY_BULGARIA: 10, 
    PL_TV_COUNTRY_CROATIA: 11, 
    PL_TV_COUNTRY_CZECH: 12,  
    PL_TV_COUNTRY_NETHERLANDS: 13, 
    PL_TV_COUNTRY_GREECE: 14, 
    PL_TV_COUNTRY_HUNGARY: 15, 
    PL_TV_COUNTRY_POLAND: 16, 
    PL_TV_COUNTRY_PORTUGAL: 17, 
    PL_TV_COUNTRY_ROMANIA: 18, 
    PL_TV_COUNTRY_RUSSIA: 19, 
    PL_TV_COUNTRY_SWITZERLAND: 20, 
    PL_TV_COUNTRY_TURKEY: 21,  
    PL_TV_COUNTRY_AUSTRALIA: 22, 
    PL_TV_COUNTRY_AUSTRIA: 23, 
    PL_TV_COUNTRY_BELGIUM: 24, 
    PL_TV_COUNTRY_DENMARK: 25, 
    PL_TV_COUNTRY_FINLAND: 26, 
    PL_TV_COUNTRY_NORWAY: 27, 
    PL_TV_COUNTRY_CHINA: 28, 
    PL_TV_COUNTRY_IRELAND: 29, 
    PL_TV_COUNTRY_SERBIA: 30, 
    PL_TV_COUNTRY_SAN_MARINO: 31, 
    PL_TV_COUNTRY_MONACO: 32, 
    PL_TV_COUNTRY_BRAZIL: 33,  
    PL_TV_COUNTRY_HONGKONG: 34, 
    PL_TV_COUNTRY_TAIWAN: 35, 
    PL_TV_COUNTRY_NEWZEALAND: 36, 
    PL_TV_COUNTRY_SLOVAKIA: 37, 
    PL_TV_COUNTRY_SINGAPORE:38, 
    PL_TV_COUNTRY_GENERALCABLE: 39, 
    PL_TV_COUNTRY_NORTH_AFRICA: 40, 
    PL_TV_COUNTRY_BELGIUM_FRENCH: 41, 
    PL_TV_COUNTRY_BELGIUM_DUTCH: 42, 
    PL_TV_COUNTRY_SOUTH_AFRICA: 43,  
    PL_TV_COUNTRY_OTHER: 44, 
    PL_TV_COUNTRY_ASIAWEUROPE_ANALOG: 45, 
    PL_TV_COUNTRY_HONGKONG_UK_ANALOG: 46, 
    PL_TV_COUNTRY_NZL_INDONESIA_ANALOG: 47, 
    PL_TV_COUNTRY_SOUTH_AFRICA_ANALOG: 48, 
    PL_TV_COUNTRY_AMERICA_ANALOG: 49, 
    PL_TV_COUNTRY_CHINA_ANALOG: 50, 
    PL_TV_COUNTRY_EASTEUROPE_ANALOG: 51, 
    
    PL_TV_COUNTRY_CODE: new Array(),
    
    /////////////// Will be removed
    // Function : GetLanguage
    PL_TVMW_LANGUAGE_KOR: 0, // ko
    PL_TVMW_LANGUAGE_ENG_US: 1, // en
    PL_TVMW_LANGUAGE_SPA_US: 2, // en
    PL_TVMW_LANGUAGE_FRA_US: 3, // en
    PL_TVMW_LANGUAGE_POR_US: 4, // en
    PL_TVMW_LANGUAGE_BUL: 5, // bg
    PL_TVMW_LANGUAGE_CRO: 6, // hr
    PL_TVMW_LANGUAGE_CZE: 7, // cs
    PL_TVMW_LANGUAGE_DAN: 8, // da
    PL_TVMW_LANGUAGE_DUT: 9, // nl
    PL_TVMW_LANGUAGE_FIN: 10, // fi
    PL_TVMW_LANGUAGE_FRA: 11, // fr
    PL_TVMW_LANGUAGE_DEU: 12, // de
    PL_TVMW_LANGUAGE_GRE: 13, // el
    PL_TVMW_LANGUAGE_HUN: 14, // hu
    PL_TVMW_LANGUAGE_ITA: 15, // it
    PL_TVMW_LANGUAGE_NOR: 16, // no
    PL_TVMW_LANGUAGE_ENG: 17, // en
    PL_TVMW_LANGUAGE_POL: 18, // pl
    PL_TVMW_LANGUAGE_POR: 19, // pt
    PL_TVMW_LANGUAGE_ROM: 20, // ro
    PL_TVMW_LANGUAGE_RUS: 21, // ru
    PL_TVMW_LANGUAGE_SER: 22, // sr
    PL_TVMW_LANGUAGE_SLK: 23, // sk
    PL_TVMW_LANGUAGE_SPA: 24, // es
    PL_TVMW_LANGUAGE_SWE: 25, // sv
    PL_TVMW_LANGUAGE_TUR: 26, // tr
    PL_TVMW_LANGUAGE_CHI: 27,
    PL_TVMW_LANGUAGE_HKG: 28,
    PL_TVMW_LANGUAGE_TPE: 29,
    
    PL_TVMW_LANGUAGE_JPN: 30,
    PL_TVMW_LANGUAGE_MAO: 31,
    PL_TVMW_LANGUAGE_CMN: 32,
    PL_TVMW_LANGUAGE_YUE: 33,
    PL_TVMW_LANGUAGE_HIN: 34,
    PL_TVMW_LANGUAGE_EST: 35, // et
    PL_TVMW_LANGUAGE_LAT: 36, // lv
    PL_TVMW_LANGUAGE_LTU: 37, // lt
    PL_TVMW_LANGUAGE_ARA: 38,
    PL_TVMW_LANGUAGE_PER: 39,
    
    PL_TVMW_LANGUAGE_QAA: 40,
    PL_TVMW_LANGUAGE_AD: 41,
    PL_TVMW_LANGUAGE_CAT: 42,
    PL_TVMW_LANGUAGE_VAL: 43,
    PL_TVMW_LANGUAGE_HEB: 44,
    PL_TVMW_LANGUAGE_OTHER: 45,
    PL_TVMW_LANGUAGE_THA: 46,
    //////////////////////////////
    
    // Language code
    // Function : GetLanguage
    PL_TV_LANGUAGE_KOR: 0, // ko
    PL_TV_LANGUAGE_ENG_US: 1, // en
    PL_TV_LANGUAGE_SPA_US: 2, // es_US
    PL_TV_LANGUAGE_FRA_US: 3, // fr_US
    PL_TV_LANGUAGE_POR_US: 4, // pt_US
    PL_TV_LANGUAGE_BUL: 5, // bg
    PL_TV_LANGUAGE_CRO: 6, // hr
    PL_TV_LANGUAGE_CZE: 7, // cs
    PL_TV_LANGUAGE_DAN: 8, // da
    PL_TV_LANGUAGE_DUT: 9, // nl
    PL_TV_LANGUAGE_FIN: 10, // fi
    PL_TV_LANGUAGE_FRA: 11, // fr
    PL_TV_LANGUAGE_DEU: 12, // de
    PL_TV_LANGUAGE_GRE: 13, // el
    PL_TV_LANGUAGE_HUN: 14, // hu
    PL_TV_LANGUAGE_ITA: 15, // it
    PL_TV_LANGUAGE_NOR: 16, // no
    PL_TV_LANGUAGE_ENG: 17, // en-GB
    PL_TV_LANGUAGE_POL: 18, // pl
    PL_TV_LANGUAGE_POR: 19, // pt
    PL_TV_LANGUAGE_ROM: 20, // ro
    PL_TV_LANGUAGE_RUS: 21, // ru
    PL_TV_LANGUAGE_SER: 22, // sr
    PL_TV_LANGUAGE_SLK: 23, // sk
    PL_TV_LANGUAGE_SPA: 24, // es
    PL_TV_LANGUAGE_SWE: 25, // sv
    PL_TV_LANGUAGE_TUR: 26, // tr
    PL_TV_LANGUAGE_CHI: 27, // zh-CN
    PL_TV_LANGUAGE_HKG: 28, // zh-HK
    PL_TV_LANGUAGE_TPE: 29, // zh-TW
    PL_TV_LANGUAGE_JPN: 30,
    PL_TV_LANGUAGE_MAO: 31,
    PL_TV_LANGUAGE_CMN: 32,
    PL_TV_LANGUAGE_YUE: 33,
    PL_TV_LANGUAGE_HIN: 34, // hi 
    PL_TV_LANGUAGE_EST: 35, // et
    PL_TV_LANGUAGE_LAT: 36, // lv
    PL_TV_LANGUAGE_LTU: 37, // lt
    PL_TV_LANGUAGE_ARA: 38, // ar
    PL_TV_LANGUAGE_PER: 39, // fa
    PL_TV_LANGUAGE_QAA: 40,
    PL_TV_LANGUAGE_AD: 41,
    PL_TV_LANGUAGE_CAT: 42,
    PL_TV_LANGUAGE_VAL: 43,
    PL_TV_LANGUAGE_THA: 44, // th
    PL_TV_LANGUAGE_HEB: 45, // he
    PL_TV_LANGUAGE_IND: 46, // id
    PL_TV_LANGUAGE_VIE: 47, // vi
    PL_TV_LANGUAGE_URD: 48, // ur
    PL_TV_LANGUAGE_AFR: 49, // af
    PL_TV_LANGUAGE_ZUL: 50, // zu
    PL_TV_LANGUAGE_XHO: 51, // xh
    PL_TV_LANGUAGE_YOR: 52, // yo
    PL_TV_LANGUAGE_IGB: 53, // ig
    PL_TV_LANGUAGE_HAU: 54, // ha
    PL_TV_LANGUAGE_SWA: 55, // sw
    PL_TV_LANGUAGE_AMH: 56, // am
    PL_TV_LANGUAGE_OTHER: 57,
    PL_TV_LANGUAGE_TAM: 58, //ta
    PL_TV_LANGUAGE_IRA: 59,
    PL_TV_LANGUAGE_FIL: 60,
    PL_TV_LANGUAGE_LIT: 61, //lt
    PL_TV_LANGUAGE_LAV: 62, //lv
    PL_TV_LANGUAGE_SLV: 63, //sl
    PL_TV_LANGUAGE_ALB: 64, //sq               //!< Albania
    PL_TV_LANGUAGE_UKR: 65, //uk               //!< Ukraine
    PL_TV_LANGUAGE_KAZ: 66, //kk               //!< Kazakhstan
    PL_TV_LANGUAGE_MKD: 67, //mk               //!< Macedonia
    PL_TV_LANGUAGE_MAY: 68, //ms               //!< Malaysia
    PL_TV_LANGUAGE_WEL: 69, //cy               //!< Welsh
    PL_TV_LANGUAGE_GLA: 70, //gd               //!< Gaelic
    PL_TV_LANGUAGE_IRI: 71, //ga               //!< Irish 
    PL_TV_LANGUAGE_MAX: 72,
    
    PL_TV_LANGUAGE_CODE: new Array(),
    
    // PL_TV_USE_TYPE
    // Function : GetTVUseMode
    PL_TVMW_TVUT_HOME: 0,
    PL_TVMW_TVUT_SHOP: 1,
    PL_TVMW_TVUT_UNKNOWN: 2,
    
    // PL_PROFILE_ID
    // Function : GetProfile
    PL_TVMW_PRFID_TICKER_ID: 0,
    PL_TVMW_PRFID_CHILDLOCK_PIN: 1,
    PL_TVMW_PRFID_HUB_TVID: 2,
    PL_TVMW_PRFID_TICKER_AUTOBOOT: 3,
    PL_TVMW_PRFID_TICKER_DURATION: 4,
    PL_TVMW_PRFID_WIDGET_DPTIME: 5,
    PL_TVMW_PRFID_CONTRACT: 6,
    PL_TVMW_PRFID_TICKER_SAFE: 7,
    PL_TVMW_PRFID_RESET: 8,
    PL_TVMW_PRFID_PASSWD_RESET: 9,
    PL_TVMW_PRFID_GEOIP_STATUS: 10,
    PL_TVMW_PRFID_COUNTRY_CODE: 11,
    PL_TVMW_PRFID_WLAN_DEFAULT_NETWORK: 12,
    PL_TVMW_PRFID_AUTO_PROTECTION_TIME: 13,
    
    // PL_TVMW_PROFILE_TYPE
    // Function : GetProfile / SetProfile
    PL_TVMW_PROFILE_TYPE_STRING: 0,
    PL_TVMW_PROFILE_TYPE_INT: 1,
    PL_TVMW_PROFILE_TYPE_UNKNOWN: 999,
    PL_TVMW_PROFILE_TYPE_MAX: 1000,
    
    // PL_DTVAPP
    // Function : Deactivate
    PL_TVMW_DTVAPP_NONE: 0,
    PL_TVMW_DTVAPP_TVVIEWER: 1,
    PL_TVMW_DTVAPP_INFOLINK: 2,
    PL_TVMW_DTVAPP_MENU: 3,
    PL_TVMW_DTVAPP_UNKNOWN: 4,
    
    // Contents Home
    CH_DTVAPP_WIDGET: 11,
    CH_DTVAPP_FLASH: 12,
    CH_DTVAPP_GALLERY: 13,
    CH_DTVAPP_GAME: 14,
    CH_DTVAPP_YAHOO: 15,
    CH_DTVAPP_MOIP: 16,
    CH_DTVAPP_PHAROS: 17, // BD
    CH_DTVAPP_TOOL: 18, // Tool - 화면설정
    CH_DTVAPP_CLMOVIEPLAYER: 19, // Netflix 2.1
    CH_DTVAPP_FULLBROWSER: 20, // Full Browser
    CH_DTVAPP_MAPBROWSER: 21, // Map Browser
    CH_DTVAPP_MMPLAYER: 22, // AllShare
    CH_DTVAPP_PVR: 23, // PVR
    CH_DTVAPP_RCVIEWER: 24, // TC Main 화면
    CH_DTVAPP_FAVORITE: 25, // dummy
    CH_DTVAPP_CHANNEL: 26, // dummy
    CH_DTVAPP_GUIDE: 27, // dummy
    CH_DTVAPP_PVR_BROWSER: 28, // dummy
    CH_DTVAPP_BASIC_FULLBROWSER: 29, // dummy
    CH_DTVAPP_VIDEOS: 30, // dummy
    CH_DTVAPP_PHOTOS: 31, // dummy
    CH_DTVAPP_MUSIC: 32, // dummy
    CH_DTVAPP_SCHEDULE: 33, // dummy
    CH_DTVAPP_SOURCE: 34, // dummy
    CH_DTVAPP_SHOP_DEMO: 35, // dummy
    CH_DTVAPP_ALLSHARE: 36, // dummy
    CH_DTVAPP_NETWORK_SETUP: 37, // network settings
    CH_DTVAPP_CALENDAR: 38, // dummy
    // PL_TVMW_SOURCE
    PL_TVMW_SOURCE_TV: 0,
    PL_TVMW_SOURCE_ATV: 1,
    PL_TVMW_SOURCE_DTV: 2,
    PL_TVMW_SOURCE_CATV: 3,
    PL_TVMW_SOURCE_CDTV: 4,
    PL_TVMW_SOURCE_PATV: 5,
    PL_TVMW_SOURCE_PDTV: 6,
    PL_TVMW_SOURCE_SDTV: 7,
    PL_TVMW_SOURCE_BSDTV: 8,
    PL_TVMW_SOURCE_CS1DTV: 9,
    PL_TVMW_SOURCE_CS2DTV: 10,
    //
    PL_TVMW_SOURCE_ATV1: 11,
    PL_TVMW_SOURCE_ATV2: 12,
    PL_TVMW_SOURCE_DTV1: 13,
    PL_TVMW_SOURCE_DTV2: 14,
    // AV 1~4
    PL_TVMW_SOURCE_AV1: 15,
    PL_TVMW_SOURCE_AV2: 16,
    PL_TVMW_SOURCE_AV3: 17,
    PL_TVMW_SOURCE_AV4: 18,
    //S-Video 1~4
    PL_TVMW_SOURCE_SVIDEO1: 19,
    PL_TVMW_SOURCE_SVIDEO2: 20,
    PL_TVMW_SOURCE_SVIDEO3: 21,
    PL_TVMW_SOURCE_SVIDEO4: 22,
    //Component 1~3
    PL_TVMW_SOURCE_COMP1: 23,
    PL_TVMW_SOURCE_COMP2: 24,
    PL_TVMW_SOURCE_COMP3: 25,
    PL_TVMW_SOURCE_COMP4: 26,
    // PC 1~3
    PL_TVMW_SOURCE_PC1: 27,
    PL_TVMW_SOURCE_PC2: 28,
    PL_TVMW_SOURCE_PC3: 29,
    PL_TVMW_SOURCE_PC4: 30,
    //HDMI 1~3
    PL_TVMW_SOURCE_HDMI1: 31,
    PL_TVMW_SOURCE_HDMI2: 32,
    PL_TVMW_SOURCE_HDMI3: 33,
    PL_TVMW_SOURCE_HDMI4: 34,
    
    // SCART
    PL_TVMW_SOURCE_SCART1: 35,
    PL_TVMW_SOURCE_SCART2: 36,
    PL_TVMW_SOURCE_SCART3: 37,
    PL_TVMW_SOURCE_SCART4: 38,
    
    // DVI
    PL_TVMW_SOURCE_DVI1: 39,
    PL_TVMW_SOURCE_DVI2: 40,
    PL_TVMW_SOURCE_DVI3: 41,
    PL_TVMW_SOURCE_DVI4: 42,
    
    //! DNet, WiseLink, PVR         
    PL_TVMW_SOURCE_MEDIA: 43,
    
    // DCR HOMING
    PL_TVMW_SOURCE_HOMING: 44,
    PL_TVMW_SOURCE_NONE: 45,
    PL_TVMW_SOURCE_UNKNWON: 1000,
    PL_TVMW_SOURCE_MAX: 1001,
    
    // 추가! PL_WINDOB_SOURCE_ (getSource, setSource)
    PL_WINDOW_SOURCE_TV: 0,
	PL_WINDOW_SOURCE_ATV: 1,
	PL_WINDOW_SOURCE_DTV: 2,
	PL_WINDOW_SOURCE_CATV: 3,
	PL_WINDOW_SOURCE_CDTV: 4,
	PL_WINDOW_SOURCE_PATV: 5,
	PL_WINDOW_SOURCE_PDTV: 6,
	PL_WINDOW_SOURCE_SDTV: 7,
	PL_WINDOW_SOURCE_ATV1: 11,
	PL_WINDOW_SOURCE_ATV2: 12,
	PL_WINDOW_SOURCE_DTV1: 13,
	PL_WINDOW_SOURCE_DTV2: 14,
	PL_WINDOW_SOURCE_AV1: 15,
	PL_WINDOW_SOURCE_AV2: 16,
	PL_WINDOW_SOURCE_AV3: 17,
	PL_WINDOW_SOURCE_AV4: 18,
	PL_WINDOW_SOURCE_SVIDEO1: 19,
	PL_WINDOW_SOURCE_SVIDEO2: 20,
	PL_WINDOW_SOURCE_SVIDEO3: 21,
	PL_WINDOW_SOURCE_SVIDEO4: 22,
	PL_WINDOW_SOURCE_COMP1: 23,
	PL_WINDOW_SOURCE_COMP2: 24,
	PL_WINDOW_SOURCE_COMP3: 25,
	PL_WINDOW_SOURCE_COMP4: 26,
	PL_WINDOW_SOURCE_PC1: 27,
	PL_WINDOW_SOURCE_PC2: 28,
	PL_WINDOW_SOURCE_PC3: 29,
	PL_WINDOW_SOURCE_PC4: 30,
	PL_WINDOW_SOURCE_HDMI1: 31,
	PL_WINDOW_SOURCE_HDMI2: 32,
	PL_WINDOW_SOURCE_HDMI3: 33,
	PL_WINDOW_SOURCE_HDMI4: 34,
	PL_WINDOW_SOURCE_SCART1: 35,
	PL_WINDOW_SOURCE_SCART2: 36,
	PL_WINDOW_SOURCE_SCART3: 37,
	PL_WINDOW_SOURCE_SCART4: 38,
	PL_WINDOW_SOURCE_DVI1: 39,
	PL_WINDOW_SOURCE_DVI2: 40,
	PL_WINDOW_SOURCE_DVI3: 41,
	PL_WINDOW_SOURCE_DVI4: 42,
	PL_WINDOW_SOURCE_MEDIA: 43,
	PL_WINDOW_SOURCE_HOMING: 44,
	PL_WINDOW_SOURCE_IPTV: 45,      //!< IPTV. // 45 //20120920 박일우K 협의후 수정 및 소스 추가
        PL_WINDOW_SOURCE_RVU: 46,      //!< RVU. // 46
        PL_WINDOW_SOURCE_RUI: 47,      //!< RUI // 47
        PL_WINDOW_SOURCE_ISP: 48,      //!< ISP // 48
	PL_WINDOW_SOURCE_NONE: 100, 
	
	
	// Window_channel_type
	PL_WINDOW_CHANNEL_TYPE_UNKNOWN: 0,
	PL_WINDOW_CHANNEL_TYPE_TV: 1,
	PL_WINDOW_CHANNEL_TYPE_ATV: 2,
	PL_WINDOW_CHANNEL_TYPE_DTV: 3,
	PL_WINDOW_CHANNEL_TYPE_CATV: 4,
	PL_WINDOW_CHANNEL_TYPE_CDTV: 5,

	// Window_tv_mode
	PL_WINDOW_TV_MODE_AIR: 1, //!< Air TV
	PL_WINDOW_TV_MODE_CABLE: 2, //!< Cable TV

    // Audio Plugin Ver : Audio-0001
    // PL_AUDIO_MUTE
    PL_AUDIO_MUTE_ON: 0,
    PL_AUDIO_MUTE_OFF: 1,
    PL_AUDIO_INTERNAL_MUTE_ON: 2,
    PL_AUDIO_INTERNAL_MUTE_OFF: 3,
    PL_AUDIO_RECEIVER_CONNECTED: 4,
    PL_AUDIO_MUTE_UNKNOWN: 999,
    PL_AUDIO_MUTE_MAX: 1000,
    
    // Audio Plugin Ver : Audio-0002
    // PL_AUDIO_SET_MUTE
    PL_AUDIO_SET_MUTE_ON: 0,
    PL_AUDIO_SET_MUTE_OFF: 1,
    PL_AUDIO_SET_MUTE_TOGGLE: 2,
    PL_AUDIO_SET_MUTE_INTERNAL_ON: 3,
    PL_AUDIO_SET_MUTE_INTERNAL_OFF: 4,
    PL_AUDIO_SET_MUTE_UNKNOWN: 999,
    PL_AUDIO_SET_MUTE_MAX: 1000,
    
    // PL_AUDIO_OUTPUT_DEVICE
    PL_AUDIO_OUTPUT_DEVICE_MAIN_SPEAKER: 0,
    PL_AUDIO_OUTPUT_DEVICE_EARPHONE: 1,
    PL_AUDIO_OUTPUT_DEVICE_SUBWOOFER: 2,
    PL_AUDIO_OUTPUT_DEVICE_EXTERNAL: 3,
    PL_AUDIO_OUTPUT_DEVICE_RECEIVER: 4,
    PL_AUDIO_OUTPUT_DEVICE_UNKNOWN: 999,
    PL_AUDIO_OUTPUT_DEVICE_MAX: 1000,
    
    // PL_AUDIO_VOLUME_KEY
    PL_AUDIO_VOLUME_KEY_UP: 0,
    PL_AUDIO_VOLUME_KEY_DOWN: 1,
    PL_AUDIO_VOLUME_KEY_UNKNOWN: 999,
    PL_AUDIO_VOLUME_KEY_MAX: 1000,
    
    // PLUGIN_NETWORK_TYPE
    // Function : GetNetworkType
    PL_NT_WIRED: 0,
    PL_NT_WIRELESS: 1,
    PL_NT_UNKNOWN: 2,
    
    // PL_VIDEO_WIDGET_MODE
    PL_VIDEO_WIDGET_MODE_FULL: 0,
    PL_VIDEO_WIDGET_MODE_PART: 1,
    PL_VIDEO_WIDGET_MODE_UNKNOWN: 999,
    PL_VIDEO_WIDGET_MODE_MAX: 1000,
    
    
    // PL_VIDEO_SET_MUTE
    PL_VIDEO_SET_MUTE_ON: 0,
    PL_VIDEO_SET_MUTE_OFF: 1,
    PL_VIDEO_SET_MUTE_TOGGLE: 2,
    PL_VIDEO_SET_MUTE_UNKNOWN: 999,
    PL_VIDEO_SET_MUTE_MAX: 1000,
    
    
    // PL_NNAVI_PATH
    // Function : GetPath
    PL_NNAVI_PATH_WIDGET_MANAGER: 0,
    PL_NNAVI_PATH_WIDGET_NORMAL: 1,
    PL_NNAVI_PATH_UNKNOWN: 999,
    
    
    // PL_NNAVI_SYSTEM_VERSION
    // Function : GetSystemVersion
    PL_NNAVI_SYSTEM_VERSION_LEEUM: 0,
    PL_NNAVI_SYSTEM_VERSION_COMP: 1,
    PL_NNAVI_SYSTEM_UNKNOWN: 999,
    
    // PL_NNAVI_STATE_BANNER
    // Function : SetBannerState
    PL_NNAVI_STATE_BANNER_NONE: 0,
    PL_NNAVI_STATE_BANNER_VOL: 1,
    PL_NNAVI_STATE_BANNER_VOL_CH: 2,
    
    // Plugin Result
    PLR_TRUE: 1,
    PLR_FALSE: 0,
    PLR_FAIL: -1,
    PLR_NOT_IMPLEMENT: -2,
    PLR_NULL_ARG: -3,
    PLR_INVALID_ARG: -4,
    PLR_CANNOT_OPEN_FILE: -5,
    PLR_OUT_OF_RANGE: -6,
    
    
    // PL_CMN_INFO
    // Function : GetPluginInfo
    PL_CMN_INFO_VERSION: 0,
    PL_CMN_INFO_UNKNWON: 999,
    
    // Download Plugin 1000?XX
    DN_RES_ERR_UNKNOWN: 0, // (ERR) Unknown!!
    DN_RES_OK_FILE_DOWNLOADED: 1, // Complete to download
    DN_RES_OK_FILE_DOWN_CANCELED: 2, // When user cancel the download
    DN_RES_ERR_INVALID_URL: 3, // (ERR) URL doesn't keep the RULE
    DN_RES_ERR_NORMAL_SOCKET: 4, // (ERR) Normal Socket init error
    DN_RES_ERR_SSL_SOCKET: 5, // (ERR) SSL Socket init error (for hppts or AES Encrypt module)
    DN_RES_ERR_HOST: 6, // (ERR) including 'No response from Host, Firewall restricted, etc
    DN_RES_ERR_PERMISSION_DENY: 7, // (ERR) FILE_NOT_CREATED by PERMISSION_DENY
    DN_RES_ERR_NOT_ENOUGH_STORAGE: 8, // (ERR) Strorage has no enough space for New Download file (Needs space for at least [ZipFile+RawFile] check space with File IO Plugin)
    DN_RES_ERR_INVALID_DATA: 9, // (ERR) Received but no data or invalid data
    DN_RES_ERR_URL_HAS_NO_FILE: 10, // (ERR) Received but Server has no file
    // PL_TV_EVENT
    PL_TV_EVENT_NO_SIGNAL: 101,
    PL_TV_EVENT_TUNE_SUCCESS: 103,
    PL_TV_EVENT_CHANNEL_CHANGED: 113,
    PL_TV_EVENT_SOURCE_CHANGED: 114,
    PL_TV_EVENT_PROGRAM_CHANGED: 204,
    PL_TV_EVENT_RESOLUTION_CHANGED : 117,
    
    // PL_WINDOW_RESOLUTION
    PL_WINDOW_RESOLUTION_NOTSUPPORT: 0,
    PL_WINDOW_RESOLUTION_NOSIGNAL: 1,
    PL_WINDOW_RESOLUTION_NT: 2,
    PL_WINDOW_RESOLUTION_NT_N: 3,
    PL_WINDOW_RESOLUTION_PC: 4,
    PL_WINDOW_RESOLUTION_1080I: 5,
    PL_WINDOW_RESOLUTION_1080P: 6,
    PL_WINDOW_RESOLUTION_720P: 7,
    PL_WINDOW_RESOLUTION_480P: 8,
    PL_WINDOW_RESOLUTION_480I: 9,
    PL_WINDOW_RESOLUTION_640X480P: 10,
    PL_WINDOW_RESOLUTION_1440x480I: 11,
    PL_WINDOW_RESOLUTION_576P: 12,
    PL_WINDOW_RESOLUTION_576I: 13,
    PL_WINDOW_RESOLUTION_PAL: 14,
    PL_WINDOW_RESOLUTION_PAL_M: 15,
    PL_WINDOW_RESOLUTION_PAL_N: 16,
    PL_WINDOW_RESOLUTION_SECAM: 17,
    PL_WINDOW_RESOLUTION_YC_SECAM: 18,
    PL_WINDOW_RESOLUTION_NOVIDEO: 19,
    PL_WINDOW_RESOLUTION_UNKNOWN: 20,
    PL_WINDOW_RESOLUTION_UNSTABLE: 21,
    PL_WINDOW_RESOLUTION_288I: 22,
    PL_WINDOW_RESOLUTION_MAX: 23,
    
    //AppCommon
    PL_APPCOMMON_MESSAGE_INPUT_OCCUR: 23, // Occured when key is inputed.
    PL_WINDOW_POSITION_MODE_TOPLEFT: 0,
    PL_WINDOW_POSITION_MODE_TOPRIGHT: 1,
    PL_WINDOW_POSITION_MODE_TOPCENTER: 2,
    PL_WINDOW_POSITION_MODE_BOTTOMRIGHT: 3,
    PL_WINDOW_POSITION_MODE_BOTTOMLEFT: 4,
    PL_WINDOW_POSITION_MODE_MIDDLELEFT: 5,
    PL_WINDOW_POSITION_MODE_MIDDLECENTER: 6,
    PL_WINDOW_POSITION_MODE_CUSTOM: 7,
    PL_WINDOW_POSITION_MODE_DEFAULT: 8,
    PL_WINDOW_POSITION_MODE_MAX: 9,
    
    PL_WINDOW_RECT_SIZE_PIP_SMALL: 0,
    PL_WINDOW_RECT_SIZE_PIP_LARGE: 1,
    PL_WINDOW_RECT_SIZE_PIP_DOUBLE_SMALL: 2,
    PL_WINDOW_RECT_SIZE_PIP_DOUBLE_LARGE: 3,
    PL_WINDOW_RECT_SIZE_PIG: 4,
    PL_WINDOW_RECT_SIZE_DEFALUT: 5,
    PL_WINDOW_RECT_SIZE_CUSTOM: 6,
    PL_WINDOW_RECT_SIZE_WIDEPC: 7,
    PL_WINDOW_RECT_SIZE_PC_4_3: 8,
    PL_WINDOW_RECT_SIZE_MODE_MAX: 9,
    
    // WINDOW_SEEK
    // function : SetChannel_Seek
    PL_WINDOW_SEEK_UNKNOWN : 0, 
    PL_WINDOW_SEEK_FAVORITE: 1, 
    PL_WINDOW_SEEK_CURRENT: 2, 
    PL_WINDOW_SEEK_UP: 3, 
    PL_WINDOW_SEEK_DOWN: 4, 
    PL_WINDOW_SEEK_FIRST: 5, 
    PL_WINDOW_SEEK_LAST: 6, 
    PL_WINDOW_SEEK_NEXT: 7, 
    PL_WINDOW_SEEK_PREV: 8, 
    PL_WINDOW_SEEK_BACK: 9, 
    PL_WINDOW_SEEK_EXE: 10, 
    PL_WINDOW_SEEK_DIRECT: 11, 
    PL_WINDOW_SEEK_TEMPORAL:12,

    // PIP
    PL_TV_PIP_ON: 1,
    PL_TV_PIP_OFF: 0,
    
    // TaskManager
    PL_TASKMANAGER_DTV_APP_NONE: 0,
    ////////////////////////////////////
    // BP Application
    PL_TASKMANAGER_DTV_APP_TASKMANAGER: 1,
    PL_TASKMANAGER_DTV_APP_TVVIEWER: 2,
    PL_TASKMANAGER_DTV_APP_MENU: 3,
    PL_TASKMANAGER_DTV_APP_EPG: 4,
    PL_TASKMANAGER_DTV_APP_CM: 5,
    PL_TASKMANAGER_DTV_APP_CC: 6,
    PL_TASKMANAGER_DTV_APP_FAC: 7,
    PL_TASKMANAGER_DTV_APP_CHANNELSEARCH: 8,
    PL_TASKMANAGER_DTV_APP_ADDDEL: 9,
    PL_TASKMANAGER_DTV_APP_REMINDER: 10,
    PL_TASKMANAGER_DTV_APP_SOURCE: 11,
    PL_TASKMANAGER_DTV_APP_TVTOOLS: 12,
    PL_TASKMANAGER_DTV_APP_INTERTEST: 13,
    PL_TASKMANAGER_DTV_APP_INTERNALTEST: 14,
    PL_TASKMANAGER_DTV_APP_HOTEL: 15,
    // for UU
    PL_TASKMANAGER_DTV_APP_MINIFAVCH: 16,
    // for OCAP & ACAP
    PL_TASKMANAGER_DTV_APP_EAS: 17,
    PL_TASKMANAGER_DTV_APP_DV: 18,
    PL_TASKMANAGER_DTV_APP_HTML: 19,
    PL_TASKMANAGER_DTV_APP_APPLIST: 20,
    PL_TASKMANAGER_DTV_APP_JAVAMW: 21,
    PL_TASKMANAGER_DTV_APP_COMDOWNLOAD: 22,
    // for DV
    PL_TASKMANAGER_DTV_APP_TTX: 23,
    PL_TASKMANAGER_DTV_APP_SBT: 24,
    PL_TASKMANAGER_DTV_APP_CI: 25,
    PL_TASKMANAGER_DTV_APP_MHEG: 26,
    PL_TASKMANAGER_DTV_APP_RETURN_CHANNEL: 27,
    PL_TASKMANAGER_DTV_APP_CU: 28,
    PL_TASKMANAGER_DTV_APP_FAVCHLIST: 29,
    // Etc.
    PL_TASKMANAGER_DTV_APP_LOGOMANAGER: 30,
    PL_TASKMANAGER_DTV_APP_GEMSTAR: 31,
    PL_TASKMANAGER_DTV_APP_FMRADIO: 32,
    PL_TASKMANAGER_DTV_APP_HOME_MENU: 33,
    
    ////////////////////////////////////
    // AP Application
    // for WLink
    PL_TASKMANAGER_DTV_APP_WISELINK: 34,
    PL_TASKMANAGER_DTV_APP_MMBROWSER: 35,
    PL_TASKMANAGER_DTV_APP_MMPLAYER: 36,
    PL_TASKMANAGER_DTV_APP_MOVIE_PLAYER: 37,
    PL_TASKMANAGER_DTV_APP_MINT: 38,
    PL_TASKMANAGER_DTV_APP_WPRO: 39,
    PL_TASKMANAGER_DTV_APP_DLNA: 40,
    PL_TASKMANAGER_DTV_APP_DLNACENTER: 41,
    PL_TASKMANAGER_DTV_APP_DMR: 42,
    PL_TASKMANAGER_DTV_APP_PMR: 43,
    PL_TASKMANAGER_DTV_APP_RUIS: 44,
    PL_TASKMANAGER_DTV_APP_RUIC: 45,
    PL_TASKMANAGER_DTV_APP_USBLIST: 46,
    PL_TASKMANAGER_DTV_APP_STORY: 47,
    // for PVR
    PL_TASKMANAGER_DTV_APP_PVR: 48,
    PL_TASKMANAGER_DTV_APP_PVR_EDIT: 49,
    PL_TASKMANAGER_DTV_APP_PVR_BROWSER: 50,
    // for HdmiCEC
    PL_TASKMANAGER_DTV_APP_CEC: 51,
    PL_TASKMANAGER_DTV_APP_CEC_DEVICE: 52,
    // for BlueTooth
    PL_TASKMANAGER_DTV_APP_BLUETOOTH: 53,
    // for SWUpgrade
    PL_TASKMANAGER_DTV_APP_SWUPGRADE: 54,
    PL_TASKMANAGER_DTV_APP_OTA: 55,
    PL_TASKMANAGER_DTV_APP_SWUPGRADE_AIR: 56,
    PL_TASKMANAGER_DTV_APP_OAD: 57,
    PL_TASKMANAGER_DTV_APP_OAD_SAT: 58,
    PL_TASKMANAGER_DTV_APP_OTN: 59,
    PL_TASKMANAGER_DTV_APP_SWUCOMMON: 60,
    PL_TASKMANAGER_DTV_APP_USB_HOTEL_LOGOCLONE: 61,
    PL_TASKMANAGER_DTV_APP_CHMAP_TRANSFER: 62,
    // for InfoLink
    PL_TASKMANAGER_DTV_APP_RSS: 63,
    PL_TASKMANAGER_DTV_APP_INFOLINK2: 64,
    PL_TASKMANAGER_DTV_APP_MEDIALINK: 65,
    PL_TASKMANAGER_DTV_APP_CONTENTSHOME: 66,
    // for Contents Library & etc.
    PL_TASKMANAGER_DTV_APP_GPLAYER: 67,
    PL_TASKMANAGER_DTV_APP_FLASHPLAYER: 68,
    PL_TASKMANAGER_DTV_APP_TLIBBROWSER: 69,
    PL_TASKMANAGER_DTV_APP_PRODUCTGUIDE: 70,
    PL_TASKMANAGER_DTV_APP_USERMANUAL: 71,
    PL_TASKMANAGER_DTV_APP_GALLERYPLAYER: 72,
    PL_TASKMANAGER_DTV_APP_CLMOVIEPLAYER: 73,
    PL_TASKMANAGER_DTV_APP_CLPOP: 74,
    // for WLan
    PL_TASKMANAGER_DTV_APP_WLAN: 75,
    // for Yahoo
    PL_TASKMANAGER_DTV_APP_YAHOO: 76,
    PL_TASKMANAGER_DTV_APP_SHOPDEMO: 77,
    //for DualTV
    PL_TASKMANAGER_DTV_APP_MAINTVUPNPSERVER: 78,
    PL_TASKMANAGER_DTV_APP_DUALTV_READY: 79,
    // Etc
    PL_TASKMANAGER_DTV_APP_MOIP: 80,
    PL_TASKMANAGER_DTV_APP_DNET: 81,
    PL_TASKMANAGER_DTV_APP_POP: 82,
    PL_TASKMANAGER_DTV_APP_FRONTRUNNER: 83,
    PL_TASKMANAGER_DTV_APP_HOTEL_IPTV: 84,
    PL_TASKMANAGER_DTV_APP_IPTV_BROWSER: 85,
    // for BD
    PL_TASKMANAGER_DTV_APP_PHAROS_AGENT: 86,
    PL_TASKMANAGER_DTV_APP_FRONT_DISPLAY: 87,
    PL_TASKMANAGER_DTV_APP_BDP_TOOLS: 88,
    PL_TASKMANAGER_DTV_APP_BDHTS: 89,
    
    PL_TASKMANAGER_DTV_APP_FULLBROWSER: 90,
    PL_TASKMANAGER_DTV_APP_REMOTE: 91,
    PL_TASKMANAGER_DTV_APP_REMOTE_MSG: 92,
    PL_TASKMANAGER_DTV_APP_DLNADMS: 93,
    PL_TASKMANAGER_DTV_APP_DOCOMO_BROWSER: 94,
    PL_TASKMANAGER_DTV_APP_BML_BROWSER: 95,
    PL_TASKMANAGER_DTV_APP_RCT: 96,
    PL_TASKMANAGER_DTV_APP_DOWNLOAD_PLAYER: 97,
    PL_TASKMANAGER_DTV_APP_PSA: 98,
    PL_TASKMANAGER_DTV_APP_CALENDAR: 99,
    PL_TASKMANAGER_DTV_APP_BD_PLAYER: 100,
    PL_TASKMANAGER_DTV_APP_DVD_PLAYER: 101,
    PL_TASKMANAGER_DTV_APP_CDDA_PLAYER: 102,
    PL_TASKMANAGER_DTV_APP_BDRE_PLAYER: 103,
    PL_TASKMANAGER_DTV_APP_VCD_PLAYER: 104,
    PL_TASKMANAGER_DTV_APP_IPOD_PLAYER: 105,
    PL_TASKMANAGER_DTV_APP_BDP_SETTINGS: 106,
    PL_TASKMANAGER_DTV_APP_BDP_INITSET: 107,
    PL_TASKMANAGER_DTV_APP_HTS_FUNCTION_MODE: 108,
    PL_TASKMANAGER_DTV_APP_BDP_TEST_MODE: 109,
    PL_TASKMANAGER_DTV_APP_ATSC_MH: 110,
    PL_TASKMANAGER_DTV_APP_HBBTV: 111,
    PL_TASKMANAGER_DTV_TR_APP_RCVIEWER: 112,
    PL_TASKMANAGER_DTV_TR_APP_MYTV: 113,
    PL_TASKMANAGER_DTV_TR_APP_SETTINGS: 114,
    PL_TASKMANAGER_DTV_TR_APP_CHLIST: 115,
    PL_TASKMANAGER_DTV_TR_APP_ACTIVITY: 116,
    PL_TASKMANAGER_DTV_TR_APP_MBR: 117,
    PL_TASKMANAGER_DTV_TR_APP_SOURCELIST: 118,
    PL_TASKMANAGER_DTV_TR_APP_ALLSHARE: 119,
    PL_TASKMANAGER_DTV_TR_APP_FTU: 120,
    PL_TASKMANAGER_DTV_TR_APP_FACTORY: 121,
    PL_TASKMANAGER_DTV_TR_APP_PERSONALLISTENING: 122,
    PL_TASKMANAGER_DTV_TR_APP_FAVORITE: 123,
    PL_TASKMANAGER_DTV_TR_APP_SCHEDULEMANAGER: 124,
    PL_TASKMANAGER_DTV_TR_APP_VOIP: 125,
    PL_TASKMANAGER_DTV_TR_APP_OSK: 126,
    PL_TASKMANAGER_DTV_TR_APP_BROWSERCONTROL: 127,
    PL_TASKMANAGER_DTV_APP_MAP_BROWSER: 128,
    
    PL_TASKMANAGER_DTV_APP_MAX: 129,
    
    //Target Location
    PL_TV_TARGET_LOCATION_UNKNOWN: 0,
    PL_TV_TARGET_LOCATION_KOR: 1,
    PL_TV_TARGET_LOCATION_USA: 2,
    PL_TV_TARGET_LOCATION_BRA: 3,
    PL_TV_TARGET_LOCATION_PANEURO: 4,
    PL_TV_TARGET_LOCATION_CHI: 5,
    PL_TV_TARGET_LOCATION_HKG: 6,
    PL_TV_TARGET_LOCATION_ARB: 7,
    PL_TV_TARGET_LOCATION_PANNORDIG: 8,
    PL_TV_TARGET_LOCATION_SOUTHEASTASIA: 9,
    PL_TV_TARGET_LOCATION_ASIA_ATV: 10,
    PL_TV_TARGET_LOCATION_ASIA_DTV: 11,
    PL_TV_TARGET_LOCATION_TW: 12,
    PL_TV_TARGET_LOCATION_NORTHAFRICA: 13,
    PL_TV_TARGET_LOCATION_EA_DTV: 14,
    PL_TV_TARGET_LOCATION_CIS: 15,
    PL_TV_TARGET_LOCATION_PHI: 16,
    PL_TV_TARGET_LOCATION_S_AFR_DTV: 17,
    
    PL_TV_LOCATION_CODE: new Array(),
    
    PL_SCREEN_OPTION_BRIGHTNESS_SENSOR: 41,
    
    // GetProductType
    PL_TV_PRODUCT_TYPE_TV: 0,
    PL_TV_PRODUCT_TYPE_MONITOR: 1,
    PL_TV_PRODUCT_TYPE_BD: 2,
    
    // GetLanguageSet
    PL_TV_FACTORY_LANGUAGE_UNKNOWN: 0,
    PL_TV_FACTORY_LANGUAGE_EAST_ASIA: 1,
    PL_TV_FACTORY_LANGUAGE_IRAN: 2,
    PL_TV_FACTORY_LANGUAGE_ISRAEL: 3,
    PL_TV_FACTORY_LANGUAGE_MIDDLE_ASIA: 4,
    PL_TV_FACTORY_LANGUAGE_SOUTH_AMERICA: 5,
    PL_TV_FACTORY_LANGUAGE_TAIWAN: 6,
    PL_TV_FACTORY_LANGUAGE_AFRICA: 7,
    PL_TV_FACTORY_LANGUAGE_NORTH_AFRICA: 8,
    PL_TV_FACTORY_LANGUAGE_WEST_ASIA: 9,
    
    // PL_TV_REMOTE_INPUT_TYPE
    PL_TV_REMOTE_DEFAULT_TYPE: 0,
    PL_TV_REMOTE_4DIRECTION_TYPE: 1,
    PL_TV_REMOTE_NUMERIC_TYPE: 2,
    PL_TV_REMOTE_PLAYBACK_TYPE: 3,
    PL_TV_REMOTE_YAHOO_TYPE: 4,
    PL_TV_REMOTE_PSIZE_TYPE: 5,
    PL_TV_REMOTE_FULLBROWSER_TYPE: 6,
    PL_TV_REMOTE_INTERNET_TYPE: 7,
    PL_TV_REMOTE_DATASERVICE_TYPE: 8,
    PL_TV_REMOTE_ROOMEQ_TYPE: 9,
    PL_TV_REMOTE_TTX_TYPE: 10,
    PL_TV_REMOTE_MHP_TYPE: 11,
    PL_TV_REMOTE_FULLBROWSER2_TYPE: 12,
    PL_TV_REMOTE_FULLBROWSER3_TYPE: 13,
    PL_TV_REMOTE_FULLBROWSER4_TYPE: 14,
    PL_TV_REMOTE_FULLBROWSER5_TYPE: 15,
    PL_TV_REMOTE_FULLBROWSER6_TYPE: 16,
    PL_TV_REMOTE_FULLBROWSER7_TYPE: 17,
    PL_TV_REMOTE_FULLBROWSER8_TYPE: 18,
    PL_TV_REMOTE_HDMICEC_TYPE: 19,
    PL_TV_REMOTE_USBDLNA_TYPE: 20,
    PL_TV_REMOTE_INTERNET_4DIRECTION_TYPE: 21,
    PL_TV_REMOTE_COLOR_TYPE: 22,
    PL_TV_REMOTE_IR_TYPE: 23
}

with (webapis._pluginDef) {
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_KOR] = "ko";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ENG_US] = "en";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SPA_US] = "es-US";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_FRA_US] = "fr-US";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_POR_US] = "pt-US";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_BUL] = "bg";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_CRO] = "hr";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_CZE] = "cs";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_DAN] = "da";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_DUT] = "nl";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_FIN] = "fi";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_FRA] = "fr";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_DEU] = "de";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_GRE] = "el";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_HUN] = "hu";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ITA] = "it";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_NOR] = "no";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ENG] = "en-GB";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_POL] = "pl";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_POR] = "pt";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ROM] = "ro";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_RUS] = "ru";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SER] = "sr";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SLK] = "sk";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SPA] = "es";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SWE] = "sv";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_TUR] = "tr";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_CHI] = "zh-CN";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_HKG] = "zh-HK";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_TPE] = "zh-TW";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_JPN] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_MAO] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_CMN] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_YUE] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_HIN] = "hi";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_EST] = "et";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_LAT] = "lv";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_LTU] = "lt";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ARA] = "ar";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_PER] = "fa";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_QAA] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_AD] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_CAT] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_VAL] = null; // not support
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_THA] = "th";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_HEB] = "he";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_IND] = "id";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_VIE] = "vi";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_URD] = "ur";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_AFR] = "af";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ZUL] = "zu";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_XHO] = "xh";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_YOR] = "yo";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_IGB] = "ig";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_HAU] = "ha";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SWA] = "sw";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_AMH] = "am";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_OTHER] = null;
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_TAM] = "ta";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_IRA] = null;
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_FIL] = null;
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_LIT] = "lt";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_LAV] = "lv";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_SLV] = "sl";
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_ALB] = "sq"; //!< Albania
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_UKR] = "uk"; //!< Ukraine
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_KAZ] = "kk"; //!< Kazakhstan
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_MKD] = "mk"; //!< Macedonia
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_MAY] = "ms"; //!< Malaysia
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_WEL] = "cy"; //!< Welsh
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_GLA] = "gd"; //!< Gaelic
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_IRI] = "ga"; //!< Irish 
    PL_TV_LANGUAGE_CODE[PL_TV_LANGUAGE_MAX] = null;
    //reference = http=//en.wikipedia.org/wiki/List_of_ISO_639-1_codes
    
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_UNKNOWN] = "USA"; // default
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_KOR] = "KOR";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_USA] = "USA";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_BRA] = "BRA";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_PANEURO] = "PANEURO";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_CHI] = "CHI";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_HKG] = "HKG";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_ARB] = "ARB";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_PANNORDIG] = "PANNORDIG";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_SOUTHEASTASIA] = "SOUTHEASTASIA";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_ASIA_ATV] = "ASIA_ATV";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_ASIA_DTV] = "ASIA_DTV";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_TW] = "TW";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_NORTHAFRICA] = "NORTHAFRICA";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_EA_DTV] = "EA_DTV";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_CIS] = "CIS";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_PHI] = "PHI";
    PL_TV_LOCATION_CODE[PL_TV_TARGET_LOCATION_S_AFR_DTV] = "S_AFR_DTV";
    
    
    // Country Code 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_USA] = "USA";
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_KOR] = "KOR";
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SPA] = "SPA";
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_FRA] = "FRA";
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_JPN] = "JPN"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_EU] = "EU";  
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_UK] = "UK";
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_GERMANY] = "GERMANY";
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_ITALY] = "ITALY"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SWEDEN] = "SWEDEN"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_BULGARIA] = "BULGARIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_CROATIA] = "CROATIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_CZECH] = "CZECH";  
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_NETHERLANDS] = "NETHERLANDS"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_GREECE] = "GREECE"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_HUNGARY] = "HUNGARY"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_POLAND] = "POLAND"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_PORTUGAL] = "PORTUGAL"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_ROMANIA] = "ROMANIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_RUSSIA] = "RUSSIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SWITZERLAND] = "SWITZERLAND"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_TURKEY] = "TURKEY";  
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_AUSTRALIA] = "AUSTRALIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_AUSTRIA] = "AUSTRIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_BELGIUM] = "BELGIUM"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_DENMARK] = "DENMARK"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_FINLAND] = "FINLAND"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_NORWAY] = "NORWAY"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_CHINA] = "CHINA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_IRELAND] = "IRELAND"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SERBIA] = "SERBIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SAN_MARINO] = "SAN_MARINO"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_MONACO] = "MONACO"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_BRAZIL] = "BRAZIL";  
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_HONGKONG] = "HONGKONG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_TAIWAN] = "TAIWAN"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_NEWZEALAND] = "NEWZEALAND"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SLOVAKIA] = "SLOVAKIA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SINGAPORE] = "SINGAPORE"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_GENERALCABLE] = "GENERALCABLE"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_NORTH_AFRICA] = "NORTH_AFRICA"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_BELGIUM_FRENCH] = "BELGIUM_FRENCH"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_BELGIUM_DUTCH] = "BELGIUM_DUTCH"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SOUTH_AFRICA] = "SOUTH_AFRICA";  
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_OTHER] = "OTHER"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_ASIAWEUROPE_ANALOG] = "ASIAWEUROPE_ANALOG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_HONGKONG_UK_ANALOG] = "HONGKONG_UK_ANALOG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_NZL_INDONESIA_ANALOG] = "NZL_INDONESIA_ANALOG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_SOUTH_AFRICA_ANALOG] = "SOUTH_AFRICA_ANALOG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_AMERICA_ANALOG] = "AMERICA_ANALOG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_CHINA_ANALOG] = "CHINA_ANALOG"; 
    PL_TV_COUNTRY_CODE[PL_TV_COUNTRY_EASTEUROPE_ANALOG] = "EASTEUROPE_ANALOG";
}

/*
 * Private
 * Namespace: oci
 * 	oci module deals common parts of external devices. It will be used only internally and it shouldn't export to the 3rd party developers.
 * 
 * Available From:
 * 	0.1
 */
webapis.oci = {

    // public	
	EVENT_DEV_CONNECT: 11,
	EVENT_DEV_CONNECT_SUCCESS: 41,
	EVENT_DEV_CONNECT_FAIL: 42,	

	EVENT_DEV_DISCONNECT: 12,

	EVENT_DEV_STATUS: 13,

	// private
	OCI_OK: 0,
	OCI_NO_ERR: 0,
	OCI_ERR_INVALID_PARAM: 9901,
	OCI_ERR: 9999,
	OCI_EVENT_NOTHING: 0,
	DELIMITER_FUNC_PARAM: "|",
	DELIMITER_FUNC_RESULT: ",",
	DELIMITER_EVENT_PARAM: ",",
	COMMAND_OCI: "ExecuteOCI",
	OCI_PROFILE_PRINTER: 0x20,


	/*
     * Private
	 * Function: OCIDevInfo
	 *  structure that has device information
	 *
	 * Properties:
	 * UID - (String) unique ID of the device
	 * name - (String) name information of the device
	 * eventType - (Number) Type information of event such as Connect / Disconnect
	 * deviceType - (Number) device type information. This can be removed.
	 * isFree - (bool) it indicates the device is occupied or not.
	 */
	OCIDevInfo: function()
	{
	    var UID;
	    var name;
	    var eventType;
	    var deviceType;
	    var isFree;
	},
/*
	addPluginObject: function(plugin, bSEF)
	{
	    var pluginObjPrefix = "_plugin_";
        var html = '';
		var containerID = '_pluginObject'+plugin+'Container_';
		var domNode = document.createElement('div');
			domNode.id = containerID;
			domNode.style.position = 'absolute';
			domNode.style.left = '0px';
			domNode.style.top = '0px';
			//domNode.style.zIndex = $.sf.define.zIndex.plugin;
			document.body.appendChild(domNode);
		
		var bVisible = false;
		var style = bVisible ? 'position:absolute;display:block;left:0px;top:0px;width:0px;height:0px;' : 'opacity:0.0;width:0px;height:0px;';
		
		if (!bSEF) {
			html = '<OBJECT id="' + pluginObjPrefix + plugin + '" classid="clsid:SAMSUNG-INFOLINK-' + plugin + '" style="'+style+'"></OBJECT>';
			domNode.innerHTML += html;
		}
		else {
			html = '<OBJECT id="' + pluginObjPrefix + plugin + '" classid="clsid:SAMSUNG-INFOLINK-SEF" style="'+style+'"></OBJECT>';
			domNode.innerHTML += html;
			//document.getElementById(pluginObjPrefix + plugin).Open(plugin, "1.000", plugin);
		}
        return document.getElementById(pluginObjPrefix+plugin);
	},
/*/	
	addPluginObject: function(plugin, empName)
	{
		var pluginObjPrefix = "_plugin_"+empName+"_";
		var html = '';
		var containerID = '_pluginObject'+plugin+'Container_';
		var domNode = document.createElement('div');
			domNode.id = containerID;
			domNode.style.position = 'absolute';
			domNode.style.left = '0px';
			domNode.style.top = '0px';
			//domNode.style.zIndex = $.sf.define.zIndex.plugin;
			document.body.appendChild(domNode);
		var style = 'opacity:0.0;width:0px;height:0px;';
		
		html = '<OBJECT id="' + pluginObjPrefix + plugin + '" classid="clsid:SAMSUNG-INFOLINK-SEF" style="'+style+'"></OBJECT>';
		domNode.innerHTML += html;
        return document.getElementById(pluginObjPrefix+plugin);
	},
//*/
	/*
     * Private
	 * Function: create
	 *  This function create and initialize default setting of OCI Profiles.
	 *  If the profile's emp instance is already initialized, it ignores the request.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  empName - (String) name of emp process or thread of each OCI Profile.
	 *
	 * Returns
	 *  gSEF - (documentElement) If initialized successfully, it returns document element of SEF Plugin. Otherwise, it returns OCI_ERR.
	 */
	create: function(gSEF, empName)
	{
	    var ret = 0;
	    if (gSEF == null)
	    {
//            if(webapis._plugin.getSEFAvailable()==false)
            {
//                gSEF = webapis._plugin("SEF");
                gSEF = webapis.oci.addPluginObject("SEF", empName);
                if(gSEF!=null)
                {
                    // return value of Open is always 1, so we need to find different method to check EMP fail.
    	            ret = gSEF.Open(empName, "1.00", "None");
//	                if (ret == -1)
//	                {
//	                    return window.webapis.oci.OCI_ERR;
//	                }
                    if(this.getConnectedDeviceInfo(gSEF, 0)==window.webapis.oci.OCI_ERR)
                    {
                        return window.webapis.oci.OCI_ERR;
                    }
    	        }
    	        else
    	        {
                        return window.webapis.oci.OCI_ERR;
    	        }
            }
            
/*
            else
            {
                gSEF = webapis._plugin(empName);
                alert("gSEF = " + gSEF);
            }
*/            
	    }
        return gSEF;
	},

	/*
     * Private
	 * Function: isConnected
	 *  This function checks connection state of the device which has the unique id.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  sDevUID - (String) The unique id of device.
	 *
	 * Returns
	 *  true/false - (bool) If the device is connected, it returns true. Otherwise, it returns false.
	 */
	isConnected: function(gSEF, sDevUID)
	{
	    var ret = gSEF.Execute("IsConnected", "-1", sDevUID);
	    if (ret == 0)
	    {
	        return false;
	    }
	    
	    return true;
	},

	/*
     * Private
	 * Function: getConnectedDeviceInfo
	 *  This function gets the device information UID, name.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  nOrder - (Number) The order of device in total count, 1 to total count
	 *
	 * Returns
	 *  DeviceInfo - (OCIDevInfo) If it succeeds, it returns the structure of device information. Otherwise, it returns null.
	 */
	getConnectedDeviceInfo: function(gSEF, nOrder)
	{
	    if(gSEF==null||nOrder<0)
	    {
	        return null;
	    }
	    var nsRet = gSEF.Execute("GetConnectedDeviceInfo", "-1", String(nOrder));
	    if (nsRet == window.webapis.oci.OCI_ERR)
	    {
	        return null;
	    }
	    else if(nsRet == 0 || nsRet == 1)
	    {
	        return window.webapis.oci.OCI_ERR;
	    }
	    
	    var saDevInfo = nsRet.split(window.webapis.oci.DELIMITER_FUNC_RESULT);
		var devInfo = new window.webapis.oci.OCIDevInfo();
	    devInfo.UID = saDevInfo[0];
	    devInfo.name = saDevInfo[1];
	    devInfo.deviceType = Number(saDevInfo[2]);
	    devInfo.eventType = this.OCI_EVENT_NOTHING;
	    devInfo.isFree = saDevInfo[3];
		
	    return devInfo;
	},

	/*
     * Private
	 * Function: createDevice
	 *  This function creates a Device with device ID by taking device UID as parameter.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  sDevUID - (String) The unique id of device
	 *
	 * Returns
	 *  nDeviceID - (Number) If it succeeds, it returns the device ID. Otherwise, it returns null.
	 */
	createDevice: function(gSEF, sDevUID)
	{
	    var nDeviceID = gSEF.Execute("CreateDevice", "-1", sDevUID);
	    if (nDeviceID < 0)
	    {
	        return window.webapis.oci.OCI_ERR;
	    }
	    return nDeviceID;
	},

	/*
     * Private
	 * Function: destroyDevice
	 *  This function destroys the Device with the device ID.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  sDevUID - (String) The unique id of device
	 *
	 * Returns
	 */
	destroyDevice: function(gSEF, nDeviceID)
	{
	    if(gSEF!=null)
	    {
	        ret = gSEF.Execute("DestroyDevice", "-1", String(nDeviceID));
	        if (ret != window.webapis.oci.OCI_NO_ERR)
	        {
    //	        alert("[OCI]: ERROR! destroyDevice(" + nDeviceID + ") = " + ret);
	        }
	    }
	    return;
	},

    /*
     * Private
     * Function: parseDeviceEvent
     *  This is a helper function to parse the event from native codes. Each OCI devices can use this function.
     * 
     * Parameters:
     *  sParam - (String) String based device information
     *
     * Returns
     *  DeviceEvent - (OCIDevInfo) If it succeeds, it returns the structure of device event. Otherwise, it returns null.
     */
    parseDeviceEvent: function(sParam)
    {
        var event = sParam.split(window.webapis.oci.DELIMITER_EVENT_PARAM);
        if(event!=null)
        {
            var deviceEvent = new window.webapis.oci.OCIDevInfo();
            deviceEvent.eventType = Number(event[0]);
            deviceEvent.UID = String(event[1]);
            deviceEvent.name = String(event[2]);
            deviceEvent.deviceType = Number(event[5]);
        
            return deviceEvent;
        }
        return null;
	},

	/*
     * Private
	 * Function: isAvailable
	 *  This is a helper function to check the device is already exist or not.
	 * 
	 * Parameters:
	 *  uniqueID - (String) The unique id of device
	 *  arrayDevice - (array) array of existed deivces
	 *  arrayValid - (array) array of valid information
	 *
	 * Returns
	 *  nCount - (Number) If it succeeds, it returns the order of device array. Otherwise, it returns null.
	 */
	isAvailable: function(uniqueID, arrayDevice, arrayValid)
	{
		for(var nCount=0;nCount<arrayDevice.length;nCount++)
		{
//			alert("isAvailable : uniqueID = " + arrayDevice[nCount].getUniqueID());
			if(arrayDevice[nCount].getUniqueID() == uniqueID)
			{
				arrayValid[nCount] = true;
				return nCount;
			}
		}
		return null;
	},
	
	/*
     * Private
	 * Function: removeSpecificDevice
	 *  This is a helper function to remove the device which does not exist any more.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  uniqueID - (String) The unique id of device
	 *  arrayDevice - (array) array of existed deivces
	 *  arrayValid - (array) array of valid information
	 *
	 * Returns
	 */
	removeSpecificDevice: function(gSEF, uniqueID, arrayDevice, arrayValid)
	{
		var arrayID = this.isAvailable(uniqueID, arrayDevice, arrayValid);
		if(arrayID!=null)
		{
			var deviceID = arrayDevice[arrayID].getDeviceID();
			delete arrayDevice[arrayID];
			this.destroyDevice(gSEF, deviceID);
			arrayDevice.splice(arrayID, 1);
			arrayValid.splice(arrayID,1);
		}
	},
	
	/*
     * Private
	 * Function: removeDevices
	 *  This is a helper function to remove all devices.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  arrayDevice - (array) array of existed deivces
	 *  arrayValid - (array) array of valid information
	 *
	 * Returns
	 */
	removeDevices: function(gSEF, arrayDevice, arrayValid)
	{
		if(arrayValid.length)
		{
			for(var iCount=arrayValid.length-1;iCount>=0;iCount--)
			{
				if(arrayValid[iCount] == false)
				{
					this.destroyDevice(gSEF, arrayDevice[iCount].getDeviceID());
					arrayDevice.splice(iCount, 1);
					arrayValid.splice(iCount, 1);
				}
			}
		}
	},

	/*
     * Private
	 * Function: getConnectedDevices
	 *  This is a helper function to get connected devices
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  arrayDevice - (array) array of existing deivces
	 *  arrayValid - (array) array of valid information
	 *  classDevice - (class) class of each device
	 *
	 * Returns
	 */
	getConnectedDevices: function(gSEF, arrayDevice, arrayValid, successCallback, classDevice)
	{
		for(var iCount=0;iCount<arrayDevice.length;iCount++)
		{
			arrayValid[iCount] = false;
		}
		
		var iNum = 0;
		while(1)
		{
			var saDevInfo = null;
			saDevInfo = this.getConnectedDeviceInfo(gSEF, iNum++);
			if(saDevInfo == null)
			{
				break;
			}
			if(this.isAvailable(saDevInfo.UID, arrayDevice, arrayValid)==null)
			{
				var iDeviceID = this.createDevice(gSEF, saDevInfo.UID);
				if(iDeviceID!=this.OCI_ERR)
				{
					var newDevice = new classDevice(saDevInfo, iDeviceID);
					arrayDevice.push(newDevice);
				}
			}
		}
		
		this.removeDevices(gSEF, arrayDevice, arrayValid);
		successCallback(arrayDevice);
	},

	/*
	 * Private
	 * Function: setFilePath
	 *  This function sets the current directory with file name.
	 * 
	 * Parameters:
	 *  filepath - (String) filepath
	 *
	 * Returns
	 *  path - (String) path/filename
	 */
	setFilePath: function(filepath)
	{
		var smartHubFull = window.location.search.split('modelid=');
		var smartHubModel = smartHubFull[1].split('&');

		//check filepath is valid
		var filename = filepath.substring(7, filepath.length);
		
		var preFilename = filepath.substring(0, 7);
		preFilename = preFilename.toLowerCase();
		// alert(preFilename);
		if(preFilename != 'file://') 
		{
	        if(preFilename == 'http://')
	        {
	        	return filepath;
			}
			else if(smartHubModel[0] != 'SDK' && preFilename == 'usb://s')
			{
				filename = '/dtv/usb/' + filepath.substring(6, filepath.length);
				return filename;
			}
			return null;
		}

		if(filename[0] == '/' || filename.search(':') != -1  || filename.indexOf("..") != -1)	
		{
			alert("webapis :: absolute filepath is not supported");
			return null;
		}
		
		//get app. filepath
		var strLocalPath = window.location.pathname;

//		alert("strLocalPath = " + strLocalPath);

		var widgetPath;
		if(smartHubModel[0] == 'SDK')
		{
			var platformName = window.navigator.platform;
			if(platformName.indexOf("Linux")!=-1)
			{
				widgetPath = "Apps"+'/'+curWidget.id;
			}
			else
			{
				widgetPath = "apps"+'/'+curWidget.id;
			}
		}
		else
		{
			widgetPath = curWidget.id;
		}		
		var deleteIndex = strLocalPath.indexOf(widgetPath);
		if(deleteIndex!=-1)
		{
			var pFilePath = strLocalPath.substring(0, deleteIndex) + widgetPath;
			if(smartHubModel[0] != 'SDK')
			{
				if(webapis._plugin("FileSystem", "IsExistedPath", pFilePath+"_img"))
				{
					pFilePath = pFilePath+"_img";
				}
			}
//			alert("pFilePath = " + pFilePath);
			pFilePath = decodeURI(pFilePath+'/'+filename);
//			alert("pFilePath = " + pFilePath);
        
			return pFilePath;
		}
		return null;
	},

	/*
	 * Private
	 * Function: destroy
	 * destroy the sef module.
	 * this function will not be used normally.
	 * 
	 * Parameters:
	 *  gSEF - (Object) object of sef component.
	 */
	destroy: function(gSEF)
	{
	    if(gSEF!=null)
	    {
	    	gSEF.Close();
	    }
	}
}

/*
 * Namespace: gamepad
 * 	gamepad module deals with gamepad devices that can be connected to TV. It can check user's gamepad input event.
 * 
 * Available From:
 * 	0.1
 */
webapis.gamepad = {

    /*
     * Constant: GAMEPAD_MANAGER_EVENT
     * MGR_EVENT_DEV_CONNECT    - connection event
     * MGR_EVENT_DEV_DISCONNECT - disconnection event
     */
    MGR_EVENT_DEV_CONNECT : webapis.oci.EVENT_DEV_CONNECT,
    MGR_EVENT_DEV_DISCONNECT : webapis.oci.EVENT_DEV_DISCONNECT,
    
    // Event type : The input-event type of the gamepad device.
    /*
     * Constant: GAMEPAD_EVENT_TYPE
     * EV_KEY    - Key Event
     * EV_ABS    - ABS Event
     */    
    EV_KEY			:0x01,
    EV_ABS			:0x03,

    // ABS Code : The input-event code of the gamepad device.
    /*
     * Constant: GAMEPAD_ABS_CODE
     * ABS_X        - X axis
     * ABS_Y        - Y axis
     * ABS_Z        - Z axis
     * ABS_RX       - X rotation
     * ABS_RY		- Y rotation
     * ABS_RZ		- Z rotation
     * ABS_THROTTLE - Throttle
     * ABS_RUDDER	- Rudder
     * ABS_WHEEL	- Wheel
     * ABS_GAS		- Gas
     * ABS_BRAKE	- Brake
     * ABS_HAT0X	- HAT0 X axis
     * ABS_HAT0Y	- HAT0 Y axis
     * ABS_HAT1X	- HAT1 X axis
     * ABS_HAT1Y	- HAT1 Y axis
     * ABS_HAT2X	- HAT2 X axis
     * ABS_HAT2Y	- HAT2 Y axis
     * ABS_HAT3X	- HAT3 X axis
     * ABS_HAT3Y	- HAT3 Y axis
     */     
    ABS_X			:0x00,
    ABS_Y			:0x01,
    ABS_Z			:0x02,
    ABS_RX			:0x03,
    ABS_RY			:0x04,
    ABS_RZ			:0x05,
    ABS_THROTTLE	:0x06,
    ABS_RUDDER		:0x07,
    ABS_WHEEL		:0x08,
    ABS_GAS			:0x09,
    ABS_BRAKE		:0x0A,
    ABS_HAT0X		:0x10,
    ABS_HAT0Y		:0x11,
    ABS_HAT1X		:0x12,
    ABS_HAT1Y		:0x13,
    ABS_HAT2X		:0x14,
    ABS_HAT2Y		:0x15,
    ABS_HAT3X		:0x16,
    ABS_HAT3Y		:0x17,

    // Button(Key) Code
    /*
     * Constant: GAEPAD_BUTTON_CODE
     * BTN_1    - Button #1, A
     * BTN_2		- Button #2, B
     * BTN_3		- Button #3, X
     * BTN_4		- Button #4, Y
     * BTN_5		- Button #5, LB
     * BTN_6		- Button #6, RB
     * BTN_7		- Button #7, BACK
     * BTN_8		- Button #8, START
     * BTN_9		- Button #9, Left Stick
     * BTN_10		- Button #10, Right Stick
     * BTN_11		- Button #11
     * BTN_12		- Button #12
     * BTN_13		- Button #13
     * BTN_14		- Button #14
     * BTN_15		- Button #15
     * BTN_16		- Button #16
     */    
    BTN_1			:0x00,
    BTN_2			:0x01,
    BTN_3			:0x02,
    BTN_4			:0x03,
    BTN_5			:0x04,
    BTN_6			:0x05,
    BTN_7			:0x06,
    BTN_8			:0x07,
    BTN_9			:0x08,
    BTN_10			:0x09,
    BTN_11			:0x0A,
    BTN_12			:0x0B,
    BTN_13			:0x0C,
    BTN_14			:0x0D,
    BTN_15			:0x0E,
    BTN_16			:0x0F,

    // ABS Values : The input-event value of the gamepad device.
    /*
     * Constant: GAMEPAD_ABS_VALUE
     * MAX_ABS_VALUE    - Maximum ABS value
     * MIN_ABS_VALUE	- Minimum ABS value
     * XINPUT_MAX_ABS_VALUE     - Maximum ABS value of XInput
     * XINPUT_MIN_ABS_VALUE	    - Minimum ABS value of XInput 
     */
    MAX_ABS_VALUE   :255,
    MIN_ABS_VALUE   :0,
  
    XINPUT_MAX_ABS_VALUE	:32767,
    XINPUT_MIN_ABS_VALUE	:-32768,

    // key status value
    /*
     * Constant: GAMEPAD_KEY_STATUS
     * KEY_PRESSED          - Pressed status
     * KEY_RELEASED 	    - Released status
     * KEY_REPEATED         - Continous pressed status
     */    
    KEY_PRESSED     :1,
    KEY_RELEASED    :0,
    KEY_REPEATED    :2,

	GamepadArray: new Array(),
	
	GamepadValidArray: new Array(),
	
	ManagerCallback: null,
	
	SEFPlugin: null,
	
	/*
	 * Function: ManagerEvent
	 *  structure that has game pad management related event
	 *
	 * Properties:
	 * eventType - (Number) Type information of event such as Connect / Disconnect
	 * name - (Number) Gamepad's Name
	 * UID - (String) Gamepad's Unique ID
	 */
	ManagerEvent: function()
	{
		var eventType;
		var name;
		var UID;
	},
	
	/*
	 * Function: getGamepads
	 *  request to get gamepad instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns
	 */
	getGamepads: function(successCallback, errorCallback)
	{
		var timeout = 1;
		
		var returnValue = webapis.oci.create(this.SEFPlugin, "Gamepad");
		
		if(this.SEFPlugin == null)
		{
			timeout = 2500;
			
			if(returnValue != webapis.oci.OCI_ERR)
			{
        		this.SEFPlugin = returnValue;
        		
    			this.SEFPlugin.OnEvent = this.callbackGamepads;
    			
    			this.SEFPlugin.Execute("RegisterCallback", "-1");
			}
		}
				
	    if (typeof errorCallback == "undefined")
	    {
	        errorCallback = null;
	    }
		
	    if (typeof successCallback != 'function')
	    {
			alert("[gamepad.js : getGamepads] successCallback is not a function type");
			
			if(errorCallback!=null)
			{
			}
	    }
	    else
	    {
    		setTimeout(function(){webapis.oci.getConnectedDevices(webapis.gamepad.SEFPlugin, webapis.gamepad.GamepadArray, webapis.gamepad.GamepadValidArray, successCallback, webapis.gamepad.Gamepad);}, timeout);
	    }
	},

	/*
	 * Function: registerCallback
	 *  request to set callback function to receive gamepad's event such as gamepad insertion / removal event.
	 * 
	 * Parameters:
	 *  callback - (function) callback function that has DeviceEvent type parameter.
	 *
	 * Returns
	 */
	registerManagerCallback: function(callback)
	{
	    alert("[gamepad.js : registerManagerCallback]");
	    
	    if (typeof callback == 'function')
	    {
	    	this.ManagerCallback = callback;
	    }
	},

	callbackGamepads: function(eventType, sParam1, sParam2)
	{
	    var type = _checkNumberType(eventType);
	    
		switch(type)
		{
			case webapis.gamepad.MGR_EVENT_DEV_CONNECT:
			/* PASS THROUGH */
			case webapis.gamepad.MGR_EVENT_DEV_DISCONNECT:
			{
	        	var event = webapis.oci.parseDeviceEvent(sParam1);
	        	
	        	var evInfo = new webapis.gamepad.ManagerEvent();
	        	
	        	evInfo.eventType = event.eventType;
	        	
	        	evInfo.name = event.name;
	        	
	        	evInfo.UID = event.UID;
	        	
	        	if (webapis.gamepad.ManagerCallback != null)
	        	{
	        	webapis.gamepad.ManagerCallback(evInfo);
	        	}				
	        	
	        	if (type == webapis.gamepad.MGR_EVENT_DEV_DISCONNECT)
	        	{
	        	webapis.oci.removeSpecificDevice(webapis.gamepad.SEFPlugin, event.UID, webapis.gamepad.GamepadArray, webapis.gamepad.GamepadValidArray);
	        	}
	        	
	        	return;
        	}
        
        	default:
        	{
        		var temp = sParam1.split(webapis.oci.DELIMITER_EVENT_PARAM);
        		
        		for (var i = 0; i < webapis.gamepad.GamepadArray.length ; i++)
        		{
        			if ((webapis.gamepad.GamepadArray[i].deviceID == Number(temp[0]))
        			    && (webapis.gamepad.GamepadArray[i].deviceCallback != null))
        			{
        				webapis.gamepad.GamepadArray[i].gamepadEvent.time = Number(temp[1]);
        				webapis.gamepad.GamepadArray[i].gamepadEvent.type = Number(temp[2]);
        				webapis.gamepad.GamepadArray[i].gamepadEvent.code = Number(temp[3]);
        				webapis.gamepad.GamepadArray[i].gamepadEvent.value = Number(temp[4]);
        				
        				webapis.gamepad.GamepadArray[i].deviceCallback(i, webapis.gamepad.GamepadArray[i].gamepadEvent);
        			}
        		}
        		
        		return;
		    }
		}
	},

	/*
	 * Class: Gamepad
	 * 
	 * Properties:
	 *	devInfo	 - (OCIDevInfo) gamepad information structure
	 *	deviceID - (Number) gamepad's ID. It is managed internally
	 * 
	 * Private
	 * Function: Gamepad (constructor)
	 * constructor of Gamepad Class
	 * 
	 * Parameters:
	 * 	o devInfo	- (OCIDevInfo) device information structure
	 * 	o deviceID	- (Number) gamepad's ID. It is managed internally
	 */
    Gamepad: function(devInfo, deviceID)
	{
		this.uniqueID = devInfo.UID;
		this.name = devInfo.name;
		this.deviceID = deviceID;
		
		this.deviceCallback = null;
		
		/* 
		 * Function: getUniqueID
		 *  get unique ID of the gamepad
		 * 
		 * Returns
		 *  uniqueID - (String) unique ID of gamepad.
		 */
		this.getUniqueID = function() 
		{
			return this.uniqueID;
		}

		/*
		 * Private
		 * Function: getDeviceID
		 *  get device ID of the gamepad
		 * 
		 * Returns
		 *  deviceID - (String) device ID of gamepad.
		 */
		this.getDeviceID = function() 
		{
			return this.deviceID;
		}

		/* 
		 * Function: getName
		 *  get name of the gamepad
		 * 
		 * Returns
		 *  name - (String) name of gamepad.
		 */
		this.getName = function() 
		{
			return this.name;
		}

		/*
		 * Function: GamepadEvent
		 *  structure of the gamepad input event
		 * 
		 * Properties:
		 * time - (Number)
		 * type - (Number)
		 * code - (Number)
		 * value - (Number)
		 */
		this.GamepadEvent = function()
		{
			var time;
			var type;
			var code;
			var value;
		}
		this.gamepadEvent = new this.GamepadEvent();

		this.gamepadEventArray = new Array(11);
		
		this.gamepadEventArray[0] = 0;
		
        for (var i = 1; i < 11; i++)
	    {
			this.gamepadEventArray[i] = new this.GamepadEvent();
			
			this.gamepadEventArray[i].time = 0;
			this.gamepadEventArray[i].type = 0;
			this.gamepadEventArray[i].code = 0;
			this.gamepadEventArray[i].value = 0;
		}

		/*
		 * Function: GamepadABSValueRange
		 *  structure of the gamepad input range
		 * 
		 * Properties:
		 * maxValue - (Number) maximum value of the gamepad range
		 * minValue - (Number) minimum value of the gamepad range
		 */
		this.GamepadABSValueRange = function()
		{
			var maxValue;
			var minValue;
		}
		this.gamepadRange = new this.GamepadABSValueRange();

		/*
		 * Function: getInputEvent
		 *  get input event from the gamepad
		 * 
		 * Returns
		 *  GamepadEvent - (GamepadEvent) input event structure of the gamepad. If the value cannot acquired, it returns null.
		 */
		this.getInputEvent = function()
		{
//			alert("getInputEvent from " + this.deviceID);
		    var nsRetInputEvent = webapis.gamepad.SEFPlugin.Execute("GetInputEvent", String(this.deviceID));
		    
		    if (nsRetInputEvent == webapis.oci.OCI_ERR || nsRetInputEvent == webapis.oci.OCI_ERR_INVALID_PARAM)
		    {
//                alert("[OCI]: ERROR = ERR");
		        return null;
		    }
		    
		    var temp = nsRetInputEvent.split(webapis.oci.DELIMITER_FUNC_RESULT);
			this.gamepadEvent.time = Number(temp[0]);
			this.gamepadEvent.type = Number(temp[1]);
			this.gamepadEvent.code = Number(temp[2]);
			this.gamepadEvent.value = Number(temp[3]);

			return this.gamepadEvent;
		}
		
		/*
		 * Function: playForceFeedback
		 *  request to play forcefeedback effect of the the gamepad
		 * 
		 * Parameters:
		 *  duration - (Number) duration of forcefeeback effect. If the value is missed, it will be setted by 1
		 *  gain - (Number) gain of forcefeedback effect. If the value is missed, it will be setted by 100
		 *
		 * Returns
		 *  true/false - (bool) If forcefeedback is succeeded, it returns true. Otherwise, it returns false.
		 */
		this.playForceFeedback = function(duration, gain)
		{
		    alert("playForceFeedback");
		    if (typeof duration == "undefined")
		    {
		        duration = 1;
		    }
		    if (typeof gain == "undefined")
		    {
		        gain = 100;
		    }
		    var ret = webapis.gamepad.SEFPlugin.Execute("PlayForceFeedback", String(this.deviceID), String(duration), String(gain));
		    if (ret != webapis.oci.OCI_NO_ERR)
		    {
//		        alert("[OCI]: ERROR = " + ret);
		        return false;
		    }
		    return true;
		}
		
		/*
		 * Function: stopForceFeedback
		 *  request to stop forcefeedback effect of the the gamepad
		 * 
		 * Returns
		 *  true/false - (bool) If forcefeedback stops properly, it returns true. Otherwise, it returns false.
		 */
		this.stopForceFeedback = function()
		{
		    alert("stopForceFeedback");
		    var ret = webapis.gamepad.SEFPlugin.Execute("StopForceFeedback", String(this.deviceID));
		    if (ret != webapis.oci.OCI_NO_ERR)
		    {
//		        alert("[OCI]: ERROR = " + ret);
		        return false;
		    }
		    return true;
		}

		/*
		 * Function: isForceFeedbackSupported
		 *  check forcefeedback supportability of the the gamepad
		 * 
		 * Returns
		 *  true/false - (bool) If forcefeedback supported, it returns true. Otherwise, it returns false.
		 */
		this.isForceFeedbackSupported = function()
		{
		    alert("isForceFeedbackSupported");
		    var ret = webapis.gamepad.SEFPlugin.Execute("IsForceFeedbackSupported", String(this.deviceID));
		    if (ret == 0)
		    {
		        alert("[OCI]: ForceFeedback Not supported.");
		        return false;
		    }
		    alert("[OCI]: ForceFeedback Supported.");
		    return true;
		}

		/*
		 * Function: getABSValueRange
		 *  get ABS value range from the gamepad
		 *
         * Parameters:
		 *  duration - (Number) ABS Event Code to get the range.
		 * 
		 * Returns
		 *  GamepadABSValueRange - (GamepadABSValueRange) ABS value range structure of the gamepad. If the value cannot acquired, it returns null.
		 */
		this.getABSValueRange = function(code)
		{    
		    var nsRetRange = webapis.gamepad.SEFPlugin.Execute("GetABSValueRange", String(this.deviceID), String(code));
		    if (nsRetRange == webapis.oci.OCI_ERR)
		    {
		        //alert("[OCI]: ERROR = ERR");
		        return null;
		    }
		    var temp = nsRetRange.split(webapis.oci.DELIMITER_FUNC_RESULT);    
		    this.gamepadRange.maxValue = Number(temp[0]);
		    this.gamepadRange.minValue = Number(temp[1]);
		    return this.gamepadRange;
		}
		
		/*
		 * Function: setActive
		 *  set this ControllerDevice to work in active mode about input-events
		 * 
		 * Returns
		 *  true/false - (bool) If the setting active mode is successful, it returns 'true'. Otherwise, it will return 'false'
		 */
		this.setActive = function()
		{
		    alert("setActive");
		    
		    var ret = webapis.gamepad.SEFPlugin.Execute("SetActive", String(this.deviceID));
		    
		    if (ret == 0)
		    {
		        alert("[OCI]: Can't set to be active.");
		        return false;
		    }
		    alert("[OCI]: Set to be active.");
		    return true;
		}
		
		/*
		 * Function: setInactive
		 *  release this ControllerDevice from active mode
		 */
		this.setInactive = function()
		{
		    alert("setInActive");
		    
		    webapis.gamepad.SEFPlugin.Execute("SetInactive", String(this.deviceID));
		    
		    return;
		}				
		
		/*
		 * Function: getInputEventEx
		 *  get input event from the gamepad
		 * 
		 * Returns
		 *  GamepadEvent Array - (GamepadEvent Array) input event structures of the gamepad. If the value cannot acquired, it returns null.
		 *					     First element of this array is the count of events.
		 */		
		this.getInputEventEx = function(nCount)
		{
		    var nsRetInputEvent = webapis.gamepad.SEFPlugin.Execute("GetInputEventEx", String(this.deviceID), String(nCount));
		    
		    if (nsRetInputEvent == webapis.oci.OCI_ERR || nsRetInputEvent == webapis.oci.OCI_ERR_INVALID_PARAM)
		    {
		        return null;
		    }
		    
		    var temp = nsRetInputEvent.split(webapis.oci.DELIMITER_FUNC_RESULT);
		    
		    var evCnt = Number(temp[0]);
		    
		    this.gamepadEventArray[0] = evCnt;
		    
		    //alert("evCnt : " + evCnt);
		    
		    for (var i = 0; i < evCnt; i++)
		    {
				this.gamepadEventArray[i+1].time = Number(temp[i*4 + 1]);
				this.gamepadEventArray[i+1].type = Number(temp[i*4 + 2]);
				this.gamepadEventArray[i+1].code = Number(temp[i*4 + 3]);
				this.gamepadEventArray[i+1].value = Number(temp[i*4 + 4]);
				
				/*
				alert("time : " + this.gamepadEventArray[i+1].time);
				alert("type : " + this.gamepadEventArray[i+1].type);
				alert("code : " + this.gamepadEventArray[i+1].code);
				alert("value : " + this.gamepadEventArray[i+1].value);
				*/
    		}
			
			return this.gamepadEventArray;
		}
		
		/* Function: registerDeviceCallback
		 *  to register a device instance for receiving device event. If null is passed as callback function type, then callback
		 *  will be unregistered and event will no longer be received by device instance.
		 * Parameters:
		 *	callback - (Function) callbcak function, that is called when message arrives/Device joins or leaves group. 
		 * Returns:
		 *  None
		 * Note:
		 *	Function prototype
		 *-------------------------------------------------------------------------------------------------------------
  		 *	void callback(index, GamepadEvent)
		 */
		this.registerDeviceCallback = function(callback)
	    {
	        if (typeof callback == 'function')
	        {
	    	    this.deviceCallback = callback;
				webapis.gamepad.SEFPlugin.Execute("RegisterCallback", String(this.deviceID));
	        }
	        else
	        {
		        this.deviceCallback = null;
			    webapis.gamepad.SEFPlugin.Execute("UnregisterCallback", String(this.deviceID));
	        }
	        
	        return;
	    }
	},
	
	// this function will be removed, it is only temporary used.
	ends: function()
	{
		// set vaild array to false to check removed gamepads	
		for(var iCount=0;iCount<this.GamepadArray.length;iCount++)
		{
			this.GamepadValidArray[iCount] = false;
		}
		// remove gamepads which is not available.
		this.removeGamepads();
//		webapis.oci.destroy();
	}
}

/*
 * Namespace: customdevice
 * 	customdevice module deals with custom devices that can be connected to TV.
 * 
 * Available From:
 * 	0.1
 */
webapis.customdevice = {
	CustomDeviceArray: new Array(),
	CustomDeviceValidArray: new Array(),
	CustomDeviceCallback: null,
	SEFPlugin: null,
	DocEduDevice: null,
    EduDevicePlugin: 'EduDevice',


	/*
	 * Constant: CUSTOMDEVICE_ERROR_CODE
	 * ERROR_CODE_NO_ERR    - there was no error(success)
	 * ERROR_CODE_ERR - there was error 
	 */
	 ERROR_CODE_NO_ERR: 0,
	 ERROR_CODE_ERR: 9999,


	/*
	 * Constant: CUSTOMDEVICE_DEVICE_EVENTS
	 * DEV_EVENT_MESSAGE_RECEIVED    - message recieved from smart device event
	 * DEV_EVENT_JOINED_GROUP - smart device joined group event
	 * DEV_EVENT_LEFT_GROUP - smart device left group event
	 */
	DEV_EVENT_MESSAGE_RECEIVED: 150,
	DEV_EVENT_JOINED_GROUP: 151,
	DEV_EVENT_LEFT_GROUP: 152,

	/*
	 * Constant: CUSTOMDEVICE_MANAGER_EVENTS
	 * MGR_EVENT_DEV_CONNECT    - connection event
	 * MGR_EVENT_DEV_DISCONNECT - disconnection event
	 */
	MGR_EVENT_DEV_CONNECT: 11,
	MGR_EVENT_DEV_DISCONNECT: 12,
	MGR_EVENT_DEV_STATUS: 13,
	MGR_EVENT_DEV_SEARCHED : 160,   
	MGR_EVENT_DEV_SEARCH_FINISHED : 161,   
	MGR_EVENT_DEV_CONNECT_FAILED : 162,
	MGR_EVENT_DEV_PIN_REQUESTED : 171,

	/*
	 * Constant: CUSTOMDEVICE_DEVICE_TYPES
	 * DEV_EDU_DEVICE    - Edutainment device type
	 * DEV_SMART_DEVICE - Smart device type
	 */
	DEV_EDU_DEVICE: 0x05,
	DEV_SMART_DEVICE: 0x21,
	DEV_BTSPP_DEVICE: 0x22,

	/*
	 * Function: ManagerEvent
	 *  structure that has custom device connectivity information
	 *
	 * Properties:
	 * eventType - (unsigned short) Type information of event such as Connect / Disconnect 
	 * UID - (DOMString) Unique ID assigned by customdevice interface
	 * name - (String) Device name.
	 * deviceType - (unsigned short) Device type. One of DEVICE_TYPES types.
	 */
	ManagerEvent: function()
	{
		var eventType;
		var UID;
		var name;
		var deviceType;
	},


	/*
	 * Function: CustomDeviceInfo
	 *  structure that has information about event related each device instance
	 *
	 * Properties:
	 * infoType - (unsigned short) Type information of event. One of DEVICE_EVENTS types.
	 * data - (String) Event type specific data. This can assume either of CustomDeviceMessageInfo or CustomDeviceGroupInfo type depending on infoType.
	 */
	CustomDeviceInfo: function()
	{
		var infoType;
		var data;
	},


	/*
	 * Function: CustomDeviceMessageInfo
	 *  structure that has message sent by a device instance
	 *
	 * Properties:
	 * message1 - (String) Message 
	 * message2 - (String) Message
	 */
	CustomDeviceMessageInfo: function()
	{
		var message1;
		var message2;
	},


	/*
	 * Function: CustomDeviceCustomDataInfo
	 *  structure that has message sent by a device instance
	 *
	 * Properties:
	 * command 	- (Number) Message command 
	 * data 	- (String) Message data
	 */
	CustomDeviceCustomDataInfo: function()
	{
		var command;
		var data;
	},

	
	/*
	 * Function: CustomDeviceGroupInfo
	 *  structure that has group name sent by a device instance
	 *
	 * Properties:
	 * groupName - (String) group name 
	 */
	CustomDeviceGroupInfo: function()
	{
		var groupName;
	},

	/*
     * Private
	 * Function: initCustomDevices
	 *  request to initialize custom devices instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns
	 *  timeout - (int) timeout value of this function
	 */
	initCustomDevices: function()
	{
	    {
    		var returnValue = window.webapis.oci.create(this.SEFPlugin, "CustomDevice");
			if(returnValue != window.webapis.oci.OCI_ERR)
			{
        		if(this.SEFPlugin == null)
        		{
            		this.SEFPlugin = returnValue;
        			this.SEFPlugin.OnEvent = this.callbackCustomDevice;
        			this.SEFPlugin.Execute("RegisterCallback", "-1");

                    var pluginObjPrefix = "_plugin_";
                    var html = '';
                    var containerID = '_pluginObjectSEFContainer_';
                    var domNode = document.createElement('div');
                    domNode.id = containerID;
                    domNode.style.position = 'absolute';
                    domNode.style.left = '0px';
                    domNode.style.top = '0px';
                    document.body.appendChild(domNode);
                    var bVisible = true;
                    var style = bVisible ? 'position:absolute;display:block;width:0px;height:0px;' : 'opacity:0.0;width:0px;height:0px;';
                    html = '<OBJECT id="' + pluginObjPrefix + this.EduDevicePlugin + '" classid="clsid:SAMSUNG-INFOLINK-SEF" style="'+style+'"></OBJECT>';
//                  alert("Create SEF Plugin for " + this.EduDevicePlugin + ": " + html);
                    domNode.innerHTML += html;
                    this.DocEduDevice = document.getElementById(pluginObjPrefix + this.EduDevicePlugin);
        			
                    return 1000;
    			}
        		else
        		{
        		    return 1;
        		}
    		}
		    return window.webapis.oci.OCI_ERR;
    	}
	},
	
	/* 
	 * Function: getCustomDevices
	 *  request to get custom devices instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns
	 */
	getCustomDevices: function(successCallback, errorCallback)
	{
		var timeout = 1;
		timeout = webapis.customdevice.initCustomDevices();
				
	    if (typeof errorCallback == "undefined")
	    {
	        errorCallback = null;
	    }
		
	    if(timeout == window.webapis.oci.OCI_ERR)
	    {
	        if(errorCallback!=null)
	        {
    			errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR));
	        }
	        return;
	    }
		
	    if (typeof successCallback != 'function')
	    {
			alert("[custom.js : getCustoms] successCallback is not a function type");
			if(errorCallback!=null)
			{
			}
	    }
	    else
	    {
    		window.webapis.oci.getConnectedDevices(window.webapis.customdevice.SEFPlugin, window.webapis.customdevice.CustomDeviceArray, window.webapis.customdevice.CustomDeviceValidArray, successCallback, window.webapis.customdevice.CustomDevice);
	    }
	},

	/*
	 * Function: searchDevices
	 *  request to searched custom devices instances.
	 * 
	 * Parameters:
	 *
	 * Returns
	 */
    searchDevices: function()
	{
		if(webapis.customdevice.initCustomDevices()==window.webapis.oci.OCI_ERR)
		{
		    return false;
		}
		return webapis.customdevice.SEFPlugin.Execute("SearchDevice", "-1");
    },

	/*
	 * Function: stopSearchDevices
	 *  request to stop searching devices
	 * 
	 * Parameters:
	 *
	 * Returns
	 */
    stopSearchDevices: function()
	{
		if(webapis.customdevice.initCustomDevices()==window.webapis.oci.OCI_ERR)
		{
		    return false;
		}
		return webapis.customdevice.SEFPlugin.Execute("StopSearchDevice", "-1");
    },
    
	/*
	 * Function: connectDevice
	 *  request to connect bluetooth custom device.
	 * 
	 * Parameters:
	 *  strUID - (string) address/uniqueID of wanted device
	 * Returns
	 */
    connectDevice: function(strUID)
	{
	    var ret = false;
		ret = webapis.customdevice.SEFPlugin.Execute("ConnectDevice", "-1", strUID);
        return ret;
    },
    
	/*
	 * Function: disconnectDevice
	 *  request to disconnect custom device.
	 * 
	 * Parameters:
	 *  strUID - (string) address/uniqueID of wanted device
	 * Returns
	 */
    disconnectDevice: function(strUID)
	{
	    var ret = false;
		ret = webapis.customdevice.SEFPlugin.Execute("DisconnectDevice", "-1", strUID);
        return ret;
    },
    
	/*
	 * Function: setDevicePIN
	 *  set PIN of bluetooth custom device.
	 * 
	 * Parameters:
	 *  strUID - (string) address/uniqueID of wanted device
	 *  strPIN - (string) PIN
	 * Returns
	 */
    setDevicePIN: function(strUID, strPIN)
	{
	    var ret = false;
		ret = webapis.customdevice.SEFPlugin.Execute("SetDevicePIN", "-1", strUID, strPIN);
        return ret;
    },

	/*
	 * Function: registerManagerCallback
	 *  request to set callback function to receive custom device manager events of type MANAGER_EVENTS.
	 * 
	 * Parameters:
	 *  callback - (function) callback function that has prototype given below in Note section.
	 *
	 * Returns
	 *	None
	 *
	 * Note:
	 *	Function Prototype
	 *-------------------------------------------------------------------------------------------------------
	 *	void callbackFn(ManagerEvent)
	 */
	registerManagerCallback: function(callback)
	{
	    if (typeof callback == 'function' )
	    {
	    	CustomDeviceCallback = callback;
	    }
	},



	/* Private
	 * Function: callbackCustomDevice
	 *  This function is called when an event occur.
	 * 
	 * Parameters:
	 *  eventType - (String) Event type
	 *  sParam1 - (String) Parameter
	 *	sParam2	- (String) Parameter
	 *
	 * Returns
	 *	None.
	 */
	callbackCustomDevice: function(eventType, sParam1, sParam2)
	{
		alert("[callbackCustoms] Event type = "+eventType);
	    var type = Number(eventType);
		switch(type)
		{
			case window.webapis.customdevice.MGR_EVENT_DEV_STATUS:
			{
//			  	alert("#### ---- Launching edu device ----- ####");
	    			// Temporarily comment out by sihyung36.lee
				var temp = sParam1.split(",");
				window.webapis.customdevice.DocEduDevice.Open(window.webapis.customdevice.EduDevicePlugin, "1.000", "none");
				window.webapis.customdevice.DocEduDevice.Execute("Command","Param1",temp[2]);
				//*/
				break;
			}
			case window.webapis.customdevice.MGR_EVENT_DEV_DISCONNECT:
			case window.webapis.customdevice.MGR_EVENT_DEV_CONNECT:	
			{
//				alert("**[callbackCustomDevice] Device manager event**");
				var infoCustomDevice = new window.webapis.customdevice.ManagerEvent();
				var temp = sParam1.split(",");
				infoCustomDevice.eventType = Number(eventType);
				infoCustomDevice.UID = temp[1];
				infoCustomDevice.name = temp[2];
				infoCustomDevice.deviceType = Number(temp[5]);
				if(CustomDeviceCallback != null)
				{
					CustomDeviceCallback(infoCustomDevice);
				}
				if(type == window.webapis.customdevice.MGR_EVENT_DEV_DISCONNECT)
				{
					window.webapis.oci.removeSpecificDevice(window.webapis.customdevice.SEFPlugin, String(temp[1]), window.webapis.customdevice.CustomDeviceArray, window.webapis.customdevice.CustomDeviceValidArray);
				}
				break;
			}
			case window.webapis.customdevice.MGR_EVENT_DEV_CONNECT_FAILED:
			{
				if(CustomDeviceCallback!=null)
		        	{
					var event = window.webapis.oci.parseDeviceEvent(sParam1);
					var infoManager = new window.webapis.customdevice.ManagerEvent();
					infoManager.eventType = window.webapis.customdevice.MGR_EVENT_DEV_CONNECT_FAILED;
					infoManager.name = event.name;
					infoManager.UID = event.UID;
					infoManager.deviceType = event.deviceType;
					CustomDeviceCallback(infoManager);
				}
				break;
			}			
			case window.webapis.customdevice.MGR_EVENT_DEV_SEARCHED:
			case window.webapis.customdevice.MGR_EVENT_DEV_PIN_REQUESTED:
			{
				if(CustomDeviceCallback!=null)
				{
					var event = window.webapis.oci.parseDeviceEvent(sParam1);
					var infoManager = new window.webapis.customdevice.ManagerEvent();
					infoManager.eventType = Number(eventType);
					infoManager.name = event.name;
					infoManager.UID = event.UID;
					infoManager.deviceType = event.deviceType;
					CustomDeviceCallback(infoManager);
				}
				break;
			}
			case window.webapis.customdevice.MGR_EVENT_DEV_SEARCH_FINISHED:
			{
				if(CustomDeviceCallback!=null)
				{
					var infoManager = new window.webapis.customdevice.ManagerEvent();
					infoManager.eventType = Number(eventType);
					infoManager.name = null;
					infoManager.UID = null;
					infoManager.deviceType = 0;
					CustomDeviceCallback(infoManager);
				}
				break;
			}

			case window.webapis.customdevice.DEV_EVENT_JOINED_GROUP:
			case window.webapis.customdevice.DEV_EVENT_LEFT_GROUP:
			{
//				alert("[callbackCustomDevice] Group update event");
				var infoCustomDeviceInstanceEvent = new window.webapis.customdevice.CustomDeviceInfo();
				var infoCustomDeviceGroup = new window.webapis.customdevice.CustomDeviceGroupInfo();

				var temp = sParam1.split("2ZQ");
				
				infoCustomDeviceGroup.groupName = temp[1];
				infoCustomDeviceInstanceEvent.infoType = type;
				infoCustomDeviceInstanceEvent.data = infoCustomDeviceGroup;
				var i = 0;
				for (var i = 0; i < window.webapis.customdevice.CustomDeviceArray.length ; i++)
				{
					if((window.webapis.customdevice.CustomDeviceArray[i].deviceID == Number(temp[0])) && (window.webapis.customdevice.CustomDeviceArray[i].deviceCallback != null))
					{
						window.webapis.customdevice.CustomDeviceArray[i].deviceCallback(infoCustomDeviceInstanceEvent);
					}
				}

				break;
			}
			
			case window.webapis.customdevice.DEV_EVENT_MESSAGE_RECEIVED:
			{
//				alert("[callbackCustomDevice] Message received event");
				var infoCustomDeviceInstanceEvent = new window.webapis.customdevice.CustomDeviceInfo();
				var intoCustomDeviceMessage = new window.webapis.customdevice.CustomDeviceMessageInfo();
				var temp = sParam1.split("2ZQ");
				var sDeviID  = temp[0];
				intoCustomDeviceMessage.message1 = temp[1];
				infoCustomDeviceInstanceEvent.infoType = type;
				for (var i = 0; i < window.webapis.customdevice.CustomDeviceArray.length ; i++)
				{
					if(window.webapis.customdevice.CustomDeviceArray[i].deviceCallback != null)
					{
						if(window.webapis.customdevice.CustomDeviceArray[i].deviceID == Number(sDeviID))
						{
							if(window.webapis.customdevice.CustomDeviceArray[i].deviceType == window.webapis.customdevice.DEV_SMART_DEVICE)
							{
								// SmartDevice
								intoCustomDeviceMessage.message2 = temp[2];
							}
							else if(window.webapis.customdevice.CustomDeviceArray[i].deviceType == window.webapis.customdevice.DEV_BTSPP_DEVICE)
							{
								// BTDevice
								intoCustomDeviceMessage.message2 = Number(sDeviID);
							}
							infoCustomDeviceInstanceEvent.data = intoCustomDeviceMessage;
							window.webapis.customdevice.CustomDeviceArray[i].deviceCallback(infoCustomDeviceInstanceEvent);
						}
//						else if(window.webapis.customdevice.CustomDeviceArray[i].uniqueID == sDeviID)
//						{
//							// BTDevice
//							window.webapis.customdevice.CustomDeviceArray[i].deviceCallback(infoCustomDeviceInstanceEvent);
//						}
					}
				}
				break;
			}

			default:
//				alert("[callbackCustomDevice] unknown device event");
				break;
		}
	},

	/*
	 * Class : CustomDevice
	 *
	 * Properties: 
	 * 	devInfo	- (OCIDevInfo) device information structure
	 * 	deviceID	- (Number) customdevice's ID. It is managed internally
	 *
	 * Private
	 * Function: CustomDevice (constructor)
	 * constructor of CustomDevice Class
	 * 
	 * Parameters:
	 * 	o devInfo	- (OCIDevInfo) device information structure
	 * 	o deviceID	- (Number) customdevice's ID. It is managed internally
	 */
	CustomDevice: function(devInfo, deviceID)
	{
		this.uniqueID = devInfo.UID;
		this.name = devInfo.name;
		this.deviceID = deviceID;
		this.deviceType = devInfo.deviceType;
		this.deviceCallback = null;
		
		/* 
		 * Function: getUniqueID
		 *  get unique ID of the customdevice
		 * 
		 * Returns
		 *  uniqueID - (String) unique ID of custom device.
		 */
		this.getUniqueID = function() 
		{
			return this.uniqueID;
		}

		/* Private
		 * Function: 	getDeviceID
		 *  get device ID of the custom device
		 * 
		 * Returns
		 *  deviceID - (String) device ID of custom device.
		 */
		this.getDeviceID = function() 
		{
			return this.deviceID;
		}


		/* 
		 * Function: getName
		 *  get name of the customdevice
		 * 
		 * Returns
		 *  name - (String) name of custom device.
		 */
		this.getName = function() 
		{
			return this.name;
		}

		/* 
		 * Function: getType
		 *  get type of the custom device
		 * 
		 * Returns
		 *  type - (Int) type of custom device.
		 */
		this.getType = function() 
		{
			return this.deviceType;
		}
		

		/* Function: receiveMessage
		 *  receives message from interactive framework custom device.
		 * 
		 * Parameters: None
		 * Returns
		 *  customdeviceData - (CustomDeviceCustomDataInfo) message structure. If there is any error it will return null.
		 * Note
		 *	Supported for only Intercative Framework devices.
		 */
		this.receiveMessage = function()
		{
		    var nsRetInputMsg = window.webapis.customdevice.SEFPlugin.Execute("ReceiveCPMessage", String(this.deviceID));
		    if (nsRetInputMsg == null || nsRetInputMsg == window.webapis.customdevice.ERROR_CODE_ERR)
		    {
		        return null;
		    }
		    //index: value
		    //0: Command
		    //1: Message
		    var customdeviceData = new window.webapis.customdevice.CustomDeviceCustomDataInfo()
		    var temp = nsRetInputMsg.split("2ZQ");   
		    var customdeviceData = new Array(2);
		    customdeviceData.command = Number(temp[0]);
		    customdeviceData.data = String(temp[1]);
		    return customdeviceData;
		}

		/* Function: sendMessage
		 *  sends message from DTV widget to custom device.
		 * 
		 * Parameters:
		 *	messasge - (DOMString) message buffer
		 * Returns
		 *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
		 */
		this.sendMessage = function(messasge)
		{
		    var ret = window.webapis.customdevice.SEFPlugin.Execute("SendCPMessage", String(this.deviceID), messasge);

		    if (ret != window.webapis.customdevice.ERROR_CODE_NO_ERR)
		    {
		        alert("[sendMessage]: ERROR! = " + ret);
		        return window.webapis.customdevice.ERROR_CODE_ERR;
		    }
		    return window.webapis.customdevice.ERROR_CODE_NO_ERR;
		}

		/* Function: broadcastMessage
		 *  broadcasts message from DTV widget to custom devices.
		 * 
		 * Parameters:
		 *	message - (DOMString) message buffer
		 * Returns
		 *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
		 */
		this.broadcastMessage = function(message)
		{
		    var ret = window.webapis.customdevice.SEFPlugin.Execute("BroadcastCPMessage", String(this.deviceID), message );
		    
		    if (ret != window.webapis.customdevice.ERROR_CODE_NO_ERR)
		    {
		        alert("[broadcastMessage]: ERROR! = " + ret);
		        return window.webapis.customdevice.ERROR_CODE_ERR;
		    }
		    return window.webapis.customdevice.ERROR_CODE_NO_ERR;
		}

		/* Function: multicastMessage
		 *  multicasts message from DTV widget to custom devices in a group.
		 * 
		 * Parameters:
		 *	groupID - (String) Group ID.
		 *	message - (String) message buffer
		 * Returns
		 *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
		 */
		this.multicastMessage = function(groupID, message)
		{
		    var ret = window.webapis.customdevice.SEFPlugin.Execute("MulticastCPMessage", String(this.deviceID), groupID, message );
		    if (ret != window.webapis.customdevice.ERROR_CODE_NO_ERR)
		    {
		        alert("[multicastMessage]: ERROR! = " + ret);
		        return window.webapis.customdevice.ERROR_CODE_ERR;
		    }
		    return window.webapis.customdevice.ERROR_CODE_NO_ERR;
		}

		/* Function: disconnectDevice
		 *  disconnects custom device.
		 * 
		 * Parameters:
		 *	None
		 * Returns
		 *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
		 */
		this.disconnectDevice = function()
		{
		    var ret = window.webapis.customdevice.SEFPlugin.Execute("DisconnectCPDevice", String(nDeviceID));
		    
		    if (ret != window.webapis.customdevice.ERROR_CODE_NO_ERR)
		    {
		        alert("[disconnectDevice]: ERROR! = " + ret);
		        return window.webapis.customdevice.ERROR_CODE_ERR;
		    }
		    return window.webapis.customdevice.ERROR_CODE_NO_ERR;
		}
		
		/* Function: registerDeviceCallback
		 *  to register a device instance for receiving device event. If null is passed as callback function type, then callback
		 *  will be unregistered and event will no longer be received by device instance.
		 * Parameters:
		 *	callback - (Function) callbcak function, that is called when message arrives/Device joins or leaves group. 
		 * Returns:
		 *  None
		 * Note:
		 *	Function prototype
		 *-------------------------------------------------------------------------------------------------------------
  		 *	void callbackFn(CustomDeviceEventInfo)
		 */
		this.registerDeviceCallback = function(callback)
	    {
	        if (typeof callback == 'function')
	        {
	    	    this.deviceCallback = callback;
				window.webapis.customdevice.SEFPlugin.Execute("RegisterCallback", String(this.deviceID));
	        }
	        else
	        {
		        this.deviceCallback = null;
			    window.webapis.customdevice.SEFPlugin.Execute("UnregisterCallback", String(this.deviceID));
	        }
	    }
	},
}

/**
 * Namespace: mididevice
 *     mididevice module provides interface needed to develop applications that intend to use MIDI devices such as 
 *  MIDI keyboards connected to DTV over USB. mididevice module provides an inbuilt default synthesizer and 
 *  a default sountfont file. Application developers can choose their own soundfont file if needed or provide the
 *  end users with an option to choose their own soundfont file.
 * 
 * Available From:
 *     0.1
 */
webapis.mididevice = {
    MIDIDeviceArray: new Array(),
    MIDIDeviceValidArray: new Array(),
    MIDIDeviceCallback: null,
    SEFPlugin: null,


    /**
     * Constant: MIDI_MANAGER_EVENTS
     * MGR_EVENT_DEV_CONNECT    - connection event. Received when a MIDI device is connected to TV
     * MGR_EVENT_DEV_DISCONNECT - disconnection event. Received when a MIDI device is disconnected from TV
     */
    MGR_EVENT_DEV_CONNECT: 11,
    MGR_EVENT_DEV_DISCONNECT: 12,


    /**
    * Constant: MIDI_DEVICE_TYPE
    * MIDI_DEVICE_CONTROLLER    - Valid MIDI controller input device such as MIDI keyboard.
    * MIDI_DEVICE_SYNTHESIZER   - Valid MIDI output device.
    * MIDI_DEVICE_FILE          - Valid MIDI input/output device.
    * MIDI_DEVICE_UNKNOWN       - Invalid
    */
    MIDI_DEVICE_CONTROLLER: 0,
    MIDI_DEVICE_SYNTHESIZER: 1,
    MIDI_DEVICE_FILE: 2,             
    MIDI_DEVICE_UNKNOWN: 4,


    /**
    * Constant: MIDI_STREAM_STATUS
    MIDI_STREAM_STATUS_FAIL     -   Stream start failed.
    MIDI_STREAM_STATUS_SUCCESS  -   Stream start succeeded.
    MIDI_STREAM_STATUS_BUSY     -   Stream presently active or busy.
    */
    MIDI_STREAM_STATUS_FAIL: 0,
    MIDI_STREAM_STATUS_SUCCESS: 1,
    MIDI_STREAM_STATUS_BUSY: 2,


    /**
    * Constant: MIDI_EVENT_TYPE
    *  MIDI events that are sent or received to/from MIDI devices.
    */
    MIDI_EVENT_NOTE_ON: 6,
    MIDI_EVENT_NOTE_OFF: 7,
    MIDI_EVENT_CONTROL_CHANGE: 10,
    MIDI_EVENT_PROGRAM_CHANGE: 11,
    MIDI_EVENT_CHANNEL_PRESSURE: 12,
    MIDI_EVENT_PITCH_BEND: 13,


    /**
    * Constant: MIDI_CC_PARAM
    *     MIDI control change paramaters. These are sent or received as part of MIDIControlMessage, 
    * when MIDI_EVENT_TYPE is MIDI_EVENT_CONTROL_CHANGE along with values for parameters.
    */
    MIDI_CC_BANK_SELECT_MSB: 0,
    MIDI_CC_MODULATION: 1,
    MIDI_CC_MODULATION_MSB: 1,
    MIDI_CC_BREATH_CONTROLLER: 2,
    MIDI_CC_BREATH_CONTROLLER_MSB: 2,
    MIDI_CC_FOOT_CONTROLLER: 4,
    MIDI_CC_FOOT_CONTROLLER_MSB: 4,
    MIDI_CC_PORTAMENTO_TIME: 5,
    MIDI_CC_PORTAMENTO_TIME_MSB: 5,
    MIDI_CC_VOLUME: 7,
    MIDI_CC_CHANNEL_VOLUME_MSB: 7,
    MIDI_CC_BALANCE: 8,
    MIDI_CC_BALANCE_MSB: 8,
    MIDI_CC_PANNING: 10,
    MIDI_CC_PAN_MSB: 10,
    MIDI_CC_EXPRESS: 11,
    MIDI_CC_EXPRESSION_CONTROLLER_MSB: 11,
    
    MIDI_CC_BANK_SELECT: 32,
    MIDI_CC_BANK_SELECT_LSB: 32,
    MIDI_CC_MODULATION_LSB: 33,
    MIDI_CC_BREATH_CONTROLLER_LSB: 34,
    MIDI_CC_FOOT_CONTROLLER_LSB: 36,
    MIDI_CC_PORTAMENTO_TIME_LSB: 37,
    MIDI_CC_CHANNEL_VOLUME_LSB: 39,
    MIDI_CC_BALANCE_LSB: 40,
    MIDI_CC_PAN_LSB: 42,
    MIDI_CC_EXPRESSION_CONTROLLER_LSB: 43,

    MIDI_CC_SUSTAIN: 64,
    MIDI_CC_PORTAMENTO: 65,
    MIDI_CC_SOSTENUTO: 66,
    MIDI_CC_SOFT_PEDAL: 67,
    MIDI_CC_HOLD_2: 69,
    
    MIDI_CC_EXTERNAL_EFFECT_DEPTH: 91,
    MIDI_CC_TREMOLO_DEPTH: 92,
    MIDI_CC_CHORUS_DEPTH: 93,
    MIDI_CC_CELESTE_DEPTH: 94,
    MIDI_CC_PHASER_DEPTH: 95,
    
    MIDI_CC_RESET_CONTROLLER: 121,
    MIDI_CC_ALL_NOTE_OFF: 123,


    /**
     * Function: ManagerEvent
     *  MIDI device connectivity information. 
     *
     * Properties:
     * eventType      - (Integer) Type information of event such as Connect/Disconnect
     * uniqueID            - (String) Unique ID assigned by mididevice interface
     * deviceName           - (String) Device name.
     * deviceType     - (Integer) Device type. One of MIDI_DEVICE_TYPE types.
     */
    ManagerEvent: function()
    {
        var eventType;
        var uniqueID;
	var UID;
        var deviceName;
	var name;
        var deviceType;
    },


    /**
     * Function: MIDIDeviceInfo
     *  MIDI device information structure used for stream management such as start, stop or pause.
     *
     * Properties:
     * name           - (String) Device name. Limited to max 256 bytes
     * deviceID       - (Integer) Device ID. Assign 0 if not used.
     * deviceType     - (Integer) Device type. One of MIDI_DEVICE_TYPE types.
     */
    MIDIDeviceInfo: function()
    {
        var deviceName;
        var deviceID;
        var deviceType;
    },


    /**
     * Function: MIDIControlMessage
     *  MIDI channel control message type sent or received to/from MIDI devices.
     *
     * Properties:
     * channel    - (Integer) Channel number of interested control parameters.
     * parameter  - (Integer) Channel control parameter. Refer to MIDI_CC_PARAM
     * value      - (Integer) Value for the channel control parameter. Assign 0 if not used.
     * extra      - (String) Currently not used. Assign empty string.
     */
    MIDIControlMessage: function()
    {
        var channel;
        var parameter;
        var value;
        var extra;
    },


    /**
     * Function: MIDIVoiceMessage
     *  voice message sent from DTV app to MIDI synthesizer.
     *  Used for implementing simulated keyboard keys.
     *
     * Properties:
     * channel    - (Integer) Channel on which voice is played.
     * note       - (Integer) Note to be played.
     * velocity   - (Integer) Velocity with which Note is pressed.
     * duration   - (Integer) Duration, not used currently. Assign 0 if not used.
     */
    MIDIVoiceMessage: function()
    {
        var channel;
        var note;
        var velocity;
        var duration;
    },


    /**
     * Function: MIDIDeviceEventInfo
     *  represents events from MIDI controller devices.
     *
     * Properties:
     * eventType    - (Integer) Event type. Refer MIDI_EVENT_TYPE
     * message      - (MIDIControlMessage/MIDIVoiceMessage) Message corresponding to event type received.
     */
    MIDIDeviceEventInfo: function()
    {
        var eventType;
	var message;
        var eventData;
    },    


    /** 
     * Function: getMIDIDevices
     *  request to get midi devices instances.
     * 
     * Parameters:
     *  successCallback - (function) 
     *  errorCallback - (function) 
     *
     * Returns
     */
    getMIDIDevices: function(successCallback, errorCallback)
    {
        var timeout = 1;
        var returnValue = window.webapis.oci.create(this.SEFPlugin, "MIDIDevice");
        if(this.SEFPlugin == null)
        {
            timeout = 1000;
            if(returnValue != window.webapis.oci.OCI_ERR)
            {
                this.SEFPlugin = returnValue;
                this.SEFPlugin.OnEvent = this.callbackMIDIDevice;
                this.SEFPlugin.Execute("RegisterCallback", "-1");
            }
        }

        if (typeof errorCallback == "undefined")
        {
            errorCallback = null;
        }

        if (typeof successCallback != 'function')
        {
            alert("[midi.js : getMIDIs] successCallback is not a function type");
            if(errorCallback!=null)
            {
            }
        }
        else
        {
            window.webapis.oci.getConnectedDevices(window.webapis.mididevice.SEFPlugin, window.webapis.mididevice.MIDIDeviceArray, window.webapis.mididevice.MIDIDeviceValidArray, successCallback, window.webapis.mididevice.MIDIDevice);
        }
    },

    /** 
     * Function: startStream
     *  starts streaming MIDI events(or messages) from source device to destination device.
     *  MIDI keyboard live performance:
     *      Hearing key pressed on MIDI keyboard directly from the TV.
     *                  sourceDevice = MIDI keyboard info, destinationDevice = Synthesizer device info
     *  MIDI file play:
     *      Playing standard MIDI file on TV.
     *                  sourceDevice = Standard MIDI file info(must specify deviceID == 0), destinationDevice = Synthesizer device info
     *  MIDI keyboard record to file: 
     *      Recording MIDI keyvoards events in the form of standard MIDI file. This feature is not yet supported.
     *                  sourceDevice = MIDI keyboard info, destinationDevice = MIDI file info to record to.
     *                  
     * Parameters:
     *  sourceDevice        - (MIDIDeviceInfo) valid midi input device
     *  destinationDevice   - (MIDIDeviceInfo) valid midi output device
     *
     * Returns
     *      MIDI_STREAM_STATUS
     */
    startStream: function(sourceDevice, destinationDevice)
    {
        if(sourceDevice.deviceName == undefined)
        {
            sourceDevice.deviceName = "";
        }
        if(destinationDevice.deviceName == undefined)
        {
            destinationDevice.deviceName = "";
        }
        if(sourceDevice.deviceID == undefined)
        {
            sourceDevice.deviceID = 0;
        }
        if(destinationDevice.deviceID == undefined)
        {
            destinationDevice.deviceID = 0;
        }
        if(sourceDevice.deviceType == window.webapis.mididevice.MIDI_DEVICE_FILE)
        {
               sourceDevice.deviceName = webapis.oci.setFilePath(sourceDevice.deviceName);
        }
        var strSource = String(sourceDevice.deviceName)+","+String(sourceDevice.deviceID)+","+String(sourceDevice.deviceType);
        var strDestination = String(destinationDevice.deviceName)+","+String(destinationDevice.deviceID)+","+String(destinationDevice.deviceType);
        var nRetValue =  window.webapis.mididevice.SEFPlugin.Execute("StartStream", String("-1"), strSource, strDestination) 
        return nRetValue;
    },

    /** 
     * Function: stopStream
     *  stops streaming MIDI events(or messages) from source device to destination device.
     * 
     * Parameters:
     *  sourceDevice        - (MIDIDeviceInfo) valid midi input device
     *  destinationDevice   - (MIDIDeviceInfo) valid midi output device
     *
     * Returns
     *      true is success, false otherwise
     */
    stopStream: function(sourceDevice, destinationDevice)
    {
        var nRetValue = true;
        if(sourceDevice.deviceName == undefined)
        {
            sourceDevice.deviceName = "";
        }
        if(destinationDevice.deviceName == undefined)
        {
            destinationDevice.deviceName = "";
        }
        if(sourceDevice.deviceID == undefined)
        {
            sourceDevice.deviceID = 0;
        }
        if(destinationDevice.deviceID == undefined)
        {
            destinationDevice.deviceID = 0;
        }
        if (sourceDevice.deviceType == window.webapis.mididevice.MIDI_DEVICE_FILE)
        {
            sourceDevice.deviceName = webapis.oci.setFilePath(sourceDevice.deviceName);
        }
        var strSource = String(sourceDevice.deviceName)+","+String(sourceDevice.deviceID)+","+String(sourceDevice.deviceType);
        var strDestination = String(destinationDevice.deviceName)+","+String(destinationDevice.deviceID)+","+String(destinationDevice.deviceType);
        if( window.webapis.mididevice.SEFPlugin.Execute("StopStream", String("-1"), strSource, strDestination) != 0 )
        {
            nRetValue = false;
        }
        return nRetValue;
    },

    /** 
     * Function: pauseStream
     *  pauses streaming MIDI events(or messages) from source device to destination device. In case of MIDI control devices this is
     * same as stopStream, but in case of standard file this will make next startStream to start from position where it was stopped earlier.
     * 
     * Parameters:
     *  sourceDevice        - (MIDIDeviceInfo) valid midi input device
     *  destinationDevice   - (MIDIDeviceInfo) valid midi output device
     *
     * Returns
     *      true is success, false otherwise
     */
    pauseStream: function(sourceDevice, destinationDevice)
    {
        var nRetValue = true;
        if(sourceDevice.deviceName == undefined)
        {
            sourceDevice.deviceName = "";
        }
        if(destinationDevice.deviceName == undefined)
        {
            destinationDevice.deviceName = "";
        }
        if(sourceDevice.deviceID == undefined)
        {
            sourceDevice.deviceID = 0;
        }
        if(destinationDevice.deviceID == undefined)
        {
            destinationDevice.deviceID = 0;
        }
        if (sourceDevice.deviceType == window.webapis.mididevice.MIDI_DEVICE_FILE)
        {
            sourceDevice.deviceName = webapis.oci.setFilePath(sourceDevice.deviceName);
        }
        var strSource = String(sourceDevice.deviceName)+","+String(sourceDevice.deviceID)+","+String(sourceDevice.deviceType);
        var strDestination = String(destinationDevice.deviceName)+","+String(destinationDevice.deviceID)+","+String(destinationDevice.deviceType);
        if( window.webapis.mididevice.SEFPlugin.Execute("PauseStream", String("-1"), strSource, strDestination) != 0 )
        {
            nRetValue = false;
        }
        return nRetValue;
    },

    /**
     * Function: getFilePlayStatus
     *  provides file playback status.
     * 
     * Parameters:
     *  None.
     *
     * Returns
     *      MIDI_STREAM_STATUS_SUCCESS if no file is getting played, MIDI_STREAM_STATUS_BUSY if a file is still getting played.
     */
    getFilePlayStatus: function()
    {
        var nRetValue = window.webapis.mididevice.SEFPlugin.Execute("GetFilePlayStatus", String("-1"), String(""), String(""))
        return nRetValue;
    },


    /**
     * Function: registerManagerCallback
     *  request to set callback function to receive midi device manager events of type MANAGER_EVENTS.
     * 
     * Parameters:
     *  callback - (function) callback function that has prototype given below in Note section.
     *
     * Returns
     *    None
     *
     * Note:
     *    Function Prototype
     *-------------------------------------------------------------------------------------------------------
     *    void callbackFn(ManagerEvent)
     */
    registerManagerCallback: function(callback)
    {
        if (typeof callback == 'function' )
        {
            MIDIDeviceCallback = callback;
        }
    },


    /* Private
     * Function: callbackMIDIDevice
     *  This function is called when an event occurs.
     * 
     * Parameters:
     *  eventType - (String) Event type
     *  sParam1 - (String) Parameter
     *    sParam2    - (String) Parameter
     *
     * Returns
     *    None.
     */
    callbackMIDIDevice: function(eventType, sParam1, sParam2)
    {
        var type = Number(eventType);
        switch(type)
        {
            case window.webapis.mididevice.MGR_EVENT_DEV_DISCONNECT:
            case window.webapis.mididevice.MGR_EVENT_DEV_CONNECT:    
            {
                var infoMIDIDevice = new window.webapis.mididevice.ManagerEvent();
                var temp = sParam1.split(",");
                infoMIDIDevice.eventType = Number(eventType);
                infoMIDIDevice.uniqueID = temp[1];
		infoMIDIDevice.UID = temp[1];
                infoMIDIDevice.deviceName = temp[2];
		infoMIDIDevice.name = temp[2];
                infoMIDIDevice.deviceType = Number(temp[5]);
                if(MIDIDeviceCallback != null)
                {
                    MIDIDeviceCallback(infoMIDIDevice);
                }
                if(type == window.webapis.mididevice.MGR_EVENT_DEV_DISCONNECT)
                {
                    window.webapis.oci.removeSpecificDevice(window.webapis.mididevice.SEFPlugin, String(temp[1]), window.webapis.mididevice.MIDIDeviceArray, window.webapis.mididevice.MIDIDeviceValidArray);
                }
            }
            break;
            case (window.webapis.mididevice.MIDI_EVENT_NOTE_ON+10):
            case (window.webapis.mididevice.MIDI_EVENT_NOTE_OFF+10):
            {
                var infoMIDIDeviceMessage = new window.webapis.mididevice.MIDIDeviceEventInfo();
                var infoMIDIVoiceMessage = new window.webapis.mididevice.MIDIVoiceMessage();
                var voiceData = sParam2.split(",");

                infoMIDIVoiceMessage.channel = voiceData[0];
                infoMIDIVoiceMessage.note = voiceData[1];
                infoMIDIVoiceMessage.velocity = voiceData[2];
                infoMIDIVoiceMessage.duration = voiceData[3];
                infoMIDIDeviceMessage.eventType = (type-10);
                //alert("^v^v^v^v^ 6 ^v^v^v^v^");

                infoMIDIDeviceMessage.eventData = infoMIDIVoiceMessage;   
		infoMIDIDeviceMessage.message = infoMIDIVoiceMessage; 
                var i = 0;
                for ( i = 0; i < window.webapis.mididevice.MIDIDeviceArray.length ; i++)
                {
                    if((window.webapis.mididevice.MIDIDeviceArray[i].deviceID == Number(sParam1)) && (window.webapis.mididevice.MIDIDeviceArray[i].deviceCallback != null))
                    {
                        window.webapis.mididevice.MIDIDeviceArray[i].deviceCallback(infoMIDIDeviceMessage);
                    }
                }
            }
            break;

            case (window.webapis.mididevice.MIDI_EVENT_CONTROL_CHANGE+10):
            case (window.webapis.mididevice.MIDI_EVENT_PITCH_BEND+10):
            case (window.webapis.mididevice.MIDI_EVENT_PROGRAM_CHANGE+10):
            case (window.webapis.mididevice.MIDI_EVENT_CHANNEL_PRESSURE+10):
            case (window.webapis.mididevice.MIDI_EVENT_PITCH_BEND+10):
            {
                var infoMIDIDeviceMessage = new window.webapis.mididevice.MIDIDeviceEventInfo();
                var intoMIDIControlMessage = new window.webapis.mididevice.MIDIControlMessage();
                var controlData = sParam2.split(",");

                intoMIDIControlMessage.channel = controlData[0];
                intoMIDIControlMessage.parameter = controlData[1];
                intoMIDIControlMessage.value = controlData[2];
                intoMIDIControlMessage.extra = controlData[3];

                infoMIDIDeviceMessage.eventType = (type-10);
                infoMIDIDeviceMessage.eventData = intoMIDIControlMessage; 
		infoMIDIDeviceMessage.message = intoMIDIControlMessage;
                var i = 0;
                for ( i = 0; i < window.webapis.mididevice.MIDIDeviceArray.length ; i++)
                {
                    if((window.webapis.mididevice.MIDIDeviceArray[i].deviceID == Number(sParam1)) && (window.webapis.mididevice.MIDIDeviceArray[i].deviceCallback != null))
                    {
                        window.webapis.mididevice.MIDIDeviceArray[i].deviceCallback(infoMIDIDeviceMessage);
                    }
                }
            }
            break;
            default:
                alert("[callbackMIDIDevice] unknown device event");
                break;
        }
    },


    /*
     * Class : MIDIDevice
     *  MIDI class type for MIDI device instances. Each of the device instances represent each of the currently connected MIDI devices.
     * Properties: 
     *     devInfo        - (OCIDevInfo) device information structure
     *     deviceID    - (Number) mididevice's ID. It is managed internally
     *
     * Private
     * Function: MIDIDevice (constructor)
     * constructor of MIDIDevice Class
     * 
     * Parameters:
     *     o devInfo    - (OCIDevInfo) device information structure
     *     o deviceID    - (Number) mididevice's ID. It is managed internally
     */
    MIDIDevice: function(devInfo, deviceID)
    {
        this.uniqueID = devInfo.UID;
        this.name = devInfo.name;
        this.deviceID = deviceID;
        this.deviceType = devInfo.deviceType;
        this.deviceCallback = null;


        /** 
         * Function: getUniqueID
         *  get unique ID of the mididevice
         * 
         * Returns
         *  uniqueID - (String) unique ID of midi device.
         */
        this.getUniqueID = function() 
        {
            return this.uniqueID;
        }


        /** 
         * Function:     getDeviceID
         *  get device ID of the midi device
         * 
         * Returns
         *  deviceID - (String) device ID of midi device.
         */
        this.getDeviceID = function() 
        {
            return this.deviceID;
        }


        /** 
         * Function: getName
         *  get name of the mididevice
         * 
         * Returns
         *  name - (String) name of midi device.
         */
        this.getName = function() 
        {
            return this.name;
        }
    
    
        /** 
         * Function: getType
         *  get type of the midi device
         * 
         * Returns
         *  type - (Int) type of midi device.
         */
        this.getType = function() 
        {
            return this.deviceType;
        }


        /**
         * Function: getInformation
         *  used for getting MIDI parameters such volume, pitch bend, program, velocity... for specified MIDI channel.
         *
         * Parameters:
         * eventType    - (Integer) Refer MIDI_EVENT_TYPE
         * message      - (MIDIControlMessage) control message with channel and parameter of interest
         * Returns:
         *  (String) string representation of parameter requested, if successful. null otherwise.
         */
        this.getInformation = function(eventType, message)
        {
            if(message.extra == undefined)
            {
                message.extra = "";
            }
            if(message.value == undefined)
            {
                message.value = 0;
            }
            var strMessage = String(message.channel)+","+String(message.parameter)+","+String(message.value)+","+String(message.extra);
            var nsRetInputMsg = window.webapis.mididevice.SEFPlugin.Execute("GetInformation", String(this.deviceID), String(eventType), strMessage);
            return nsRetInputMsg;
        }


        /** 
         * Function: sendMessage
         *  sends message from DTV widget to midi device. This is basically used to implement software MIDI controller
         * such as simulated MIDI keyboard.
         *
         * Parameters:
         *  eventType   - (Integer) refer MIDI_EVENT_TYPE
         *  messasge    - (MIDIControlMessage/MIDIVoiceMessage) message structure
         * Returns:
         *  true if call is successful, false otherwise.
         */
        this.sendMessage = function(eventType, message)
        {
            var strMessage = null;
            if( (eventType == window.webapis.mididevice.MIDI_EVENT_NOTE_ON) || (eventType == window.webapis.mididevice.MIDI_EVENT_NOTE_OFF) )
            {
                if(message.duration == undefined)
                {
                    message.duration = 0;
                }
                strMessage = String(message.channel)+","+String(message.note)+","+String(message.velocity)+","+String(message.duration);
            }
            else
            {
                if(message.extra == undefined)
                {
                    message.extra = "";
                }
                strMessage = String(message.channel)+","+String(message.parameter)+","+String(message.value)+","+String(message.extra);
            }

            var ret = window.webapis.mididevice.SEFPlugin.Execute("SendMessage", String(this.deviceID), String(eventType), strMessage);

            if (ret != false)
            {
                alert("[sendMessage]: ERROR! = " + ret);
                return false;
            }
            return true;
        }


        /**
        * Function: loadSoundFontFile
        *  loads soundfont file used for synthesizing voice corresponding to MIDI note events. 
        * Due to limited CPU and memory recources on TV it is suggested to use smaller(<10MB) soundfont file and unload previously loaded soundfont 
        * file before loading new file.
        * Parameters:
        * sfFileName    - (String) specifies file path to load. If default soundfont file need to be loaded, specify "Default" as the sfFileName.
        * Returns:
        *  file ID if call was successful, otherwise -1.
        */
        this.loadSoundFontFile = function(sfFileName)
        {
            //alert("[loadSoundFontFile]");
            sfFileName = webapis.oci.setFilePath(sfFileName);
            var ret = window.webapis.mididevice.SEFPlugin.Execute("LoadSoundFontFile", String(this.deviceID), String(sfFileName), String(0));
            return ret;
        }


        /**
        * Function: unloadSoundFontFile
        *  unloads soundfont file used for synthesizing voice corresponding to midi note events. 
        * Parameters:
        * fileID    - (Integer) specifies file ID returned during file load operation. 
        * Returns:
        *  file ID if call was successful, otherwise -1.
        */
       this.unloadSoundFontFile = function(fileID)
       {
            //alert("[unloadSoundFontFile]");
            var ret = window.webapis.mididevice.SEFPlugin.Execute("UnloadSoundFontFile", String(this.deviceID), String(fileID), String(0));
            return ret;
       }

        /**
        * Function: getSoundFontInstruments
        *  gets all the instruments/programs/presets in the soundfont file loaded.
        * Parameters:
        * fileID            - (Integer) specifies file ID returned during file load operation.
        * instrumentOffset  - (Integer) specifies instruments offset, if number of programs/intruments in a soundfont file is more than 64, then
        *               this function need to be called multiple times with instrumentOffset starting from 0, then every iteration instrumentOffset incremented by
        *               64 till null is returned.
        * numberOfInstruments - (Integer) Number of soundfont files intruments to receive in one call.
        * Returns:
        *  array of Strings, with pattern "xxx:yyy ProgramName", where xxx is bank number, yyy is program number, ProgramName user readable name of instrument.
        * null is returned if call failed or no more intruments available.
        * Example is given below
        =====================================================
        |Bank       |Program Number     |Program Name       |
        =====================================================
        |000        |000                |Piano 1            |
        ----------------------------------------------------
        |000        |001                |Piano 2            |       
        ----------------------------------------------------
        |001        |007                |Harpsichord        |       
        ----------------------------------------------------
        |002        |119                |Synth Drum         |
        ----------------------------------------------------
        */
        this.getSoundFontInstruments = function(fileID, instrumentOffset, numberOfInstruments)
        {
            //alert("[getSFInstruments] ["+String(fileID)+"]");
            var ret = window.webapis.mididevice.SEFPlugin.Execute("GetSoundFontIntruments", String(this.deviceID), String(fileID), String(instrumentOffset), String(numberOfInstruments));
            if( ret != null )
            {
                var arr = ret.split(",");
                arr.pop();
                return arr;
            }
            return ret;
        }


        /** 
         * Function: registerDeviceCallback
         *  to register a device instance for receiving device event. If null is passed as callback function type, then callback
         * will be unregistered and event will no longer be received by device instance.
         * Parameters:
         *  callback      - (Function) callbcak function, that is called when an event arrives from subscribed device.
         * Returns:
         *  None
         * Note:
         *  Function prototype
         *-------------------------------------------------------------------------------------------------------------
         *  void callbackFn(MIDIDeviceEventInfo)
         */
        this.registerDeviceCallback = function(callback)
        {
            if (typeof callback == 'function')
            {
                this.deviceCallback = callback;
                window.webapis.mididevice.SEFPlugin.Execute("RegisterCallback", String(this.deviceID));
            }
            else
            {
                this.deviceCallback = null;
                window.webapis.mididevice.SEFPlugin.Execute("UnregisterCallback", String(this.deviceID));
            }
        }
    },
}


/*
 * Namespace: nservice
 *     nservice module deals with providing interface to HHP devices that are connected to TV over wifi network.
 * 
 * Available From:
 *     0.1
 */
webapis.nservice = {
    NServiceDeviceArray: new Array(),
    NServiceDeviceValidArray: new Array(),
    NServiceDeviceOnlyArray: new Array(),
    NServiceDeviceCallback: null,
    SEFPlugin: null,

    /*
     * Constant: NSERVICE_ERROR_CODE
     * ERROR_CODE_NO_ERR    - there was no error(success)
     * ERROR_CODE_ERR - there was error 
     */
     ERROR_CODE_NO_ERR: 0,
     ERROR_CODE_ERR: 9999,


    /*
     * Constant:  NSERVICE_DEVICE_EVENTS
     * DEV_EVENT_MESSAGE_RECEIVED    - message recieved from smart device event
     * DEV_EVENT_JOINED_GROUP - smart device joined group event
     * DEV_EVENT_LEFT_GROUP - smart device left group event
     */
    DEV_EVENT_MESSAGE_RECEIVED: 150,
    DEV_EVENT_JOINED_GROUP: 151,
    DEV_EVENT_LEFT_GROUP: 152,

    /*
     * Constant:  NSERVICE_DEVICE_MANAGER_EVENTS
     * MGR_EVENT_DEV_CONNECT    - connection event
     * MGR_EVENT_DEV_DISCONNECT - disconnection event
     */
    MGR_EVENT_DEV_CONNECT: 11,
    MGR_EVENT_DEV_DISCONNECT: 12,
    MGR_EVENT_DEV_STATUS: 13,

    /*Private
     * Constant: NSERVICE_DEVICE_DEVICE_TYPES
     * DEV_SMART_DEVICE - Smart device type
     */
    DEV_SMART_DEVICE: 0x21,

    /*
     * Function: ManagerEvent
     *  NService device connectivity information
     *
     * Properties:
     * eventType - (unsigned short) Type information of event such as Connect / Disconnect 
     * uniqueID - (DOMString) Unique ID assigned by NService interface
     * deviceName - (String) Device name.
     * deviceType - (unsigned short) Device type. One of DEVICE_TYPES types.
     */
    ManagerEvent: function()
    {
        var eventType;
        var uniqueID;
        var deviceName;
        var deviceType;
    },


    /*
     * Function: NServiceDeviceEventInfo
     *  structure that has information about event related each device instance
     *
     * Properties:
     * infoType - (unsigned short) Type information of event. One of DEVICE_EVENTS types.
     * data - (String) Event type specific data. This can assume either of NServiceDeviceMessageInfo or NServiceDeviceGroupInfo type depending on infoType.
     */
    NServiceDeviceEventInfo: function()
    {
        var eventType;
        var eventData;
    },


    /*
     * Function: NServiceDeviceMessageInfo
     *  structure that has message sent by a device instance
     *
     * Properties:
     * message - (String) Message 
     * context - (String) Context
     */
    NServiceDeviceMessageInfo: function()
    {
        var message;
        var context;
    },


    /*
     * Function: NServiceDeviceGroupInfo
     *  structure that has group name sent by a device instance
     *
     * Properties:
     * groupName - (String) group name 
     */
    NServiceDeviceGroupInfo: function()
    {
        var groupName;

		/*
    	 * Function: getMembers
    	 *
    	 * Returns
    	 *  NServiceDeviceArray
    	 *
    	 * Note:
    	 *  This API is provided in the client side only. Host throws 'NotSupportedError'
		 *
		 */
		this.getMembers = function() 
        {
            throw ErrorsHelper.createWebAPIException_byType('NotSupportedError');
			return new Array();
        }, 

		/*
    	 * Function: leave
    	 *
         * Parameters:
         *  onsuccess - (function) 
         *  onerror - (function) 
		 *
    	 * Returns
    	 *  None.
    	 *
    	 * Note:
    	 *  This API is provided in the client side only. Host throws 'NotSupportedError'
		 *
		 */
		this.leave = function(onsuccess, onerror) 
        {
            throw ErrorsHelper.createWebAPIException_byType('NotSupportedError');
        }

    },
    
    
    /* 
     * Function: getNServiceDevices
     *  Gets device information of HHP devices connected to Smart TV.
     * 
     * Parameters:
     *  successCallback - (function) 
     *  errorCallback - (function) 
     *
     * Returns
     */
    getNServiceDevices: function(successCallback, errorCallback)
    {
        var timeout = 1;
        var returnValue = window.webapis.oci.create(this.SEFPlugin, "CustomDevice");
        if(this.SEFPlugin == null)
        {
            timeout = 1000;
            if(returnValue != window.webapis.oci.OCI_ERR)
            {
                this.SEFPlugin = returnValue;
                this.SEFPlugin.OnEvent = this.callbackNServiceDevice;
                this.SEFPlugin.Execute("RegisterCallback", "-1");
            }
        }
                
        if (typeof errorCallback == "undefined")
        {
            errorCallback = null;
        }
        
        if (typeof successCallback != 'function')
        {
            alert("[getNServiceDevices] successCallback is not a function type");
            if(errorCallback!=null)
            {
            }
        }
        else
        {
            window.webapis.oci.getConnectedDevices(window.webapis.nservice.SEFPlugin, window.webapis.nservice.NServiceDeviceArray, window.webapis.nservice.NServiceDeviceValidArray, window.webapis.nservice.getNServiceDevicesOnly, window.webapis.nservice.NServiceDevice);
            successCallback(window.webapis.nservice.NServiceDeviceOnlyArray);
        }
    },

    
    /* Private
     * Function: getNServiceDevicesOnly
     *  Gets device information of HHP devices connected to Smart TV.
     * 
     * Parameters:
     *  deviceArray - (function) device array containing all types of custom devices.
     * Returns
     */    
    getNServiceDevicesOnly: function(deviceArray)
    {
        var numberOfDev = window.webapis.nservice.NServiceDeviceOnlyArray.length ;
        for(var i = 0 ; i < numberOfDev ; i++ )
        {
            window.webapis.nservice.NServiceDeviceOnlyArray.pop();
        }
        for(var i = 0; i < deviceArray.length; i++ )
        {
            if (deviceArray[i] != null && deviceArray[i].getType() == window.webapis.nservice.DEV_SMART_DEVICE) 
            {
                window.webapis.nservice.NServiceDeviceOnlyArray.push(deviceArray[i]);
            }
        }
    },
    
    
    /* 
     * Function: broadcastMessage
     *  broadcasts message from DTV widget to HHP devices.
     * 
     * Parameters:
     *    message - (DOMString) message buffer
     * Returns
     *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
     */
    broadcastMessage: function(message)
    {
        var retValue = window.webapis.nservice.ERROR_CODE_NO_ERR;

        //alert("Manager broadcast");
        
        var i = 0;
        if( window.webapis.nservice.NServiceDeviceArray.length > 0)
        {
            if( window.webapis.nservice.NServiceDeviceArray[i].broadcastMessage(message) != window.webapis.nservice.ERROR_CODE_NO_ERR)
            {
                retValue = window.webapis.nservice.ERROR_CODE_ERR;
            }
        }
        return retValue;
    },


	/* 
	 * Function: multicastMessage
	 *  multicasts message from DTV widget to HHP devices in a group.
	 * 
	 * Parameters:
	 *    groupID - (String) Group ID.
	 *    message - (String) message buffer
	 * Returns
	 *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
	 */
    multicastMessage: function(groupID, message)
    {
        //alert("Manager multicast");

        var i = 0;
        for ( i = 0; i < window.webapis.nservice.NServiceDeviceArray.length ; i++)
        {
            if(window.webapis.nservice.NServiceDeviceArray[i].groupID == groupID)
            {
                if( window.webapis.nservice.NServiceDeviceArray[i].multicastMessage(groupID, message) != window.webapis.nservice.ERROR_CODE_NO_ERR)
                {
                    return window.webapis.nservice.ERROR_CODE_ERR;
                }
                return window.webapis.nservice.ERROR_CODE_NO_ERR;
            }
        }
        return window.webapis.nservice.ERROR_CODE_ERR;
    },

	
    /*
     * Function: registerManagerCallback
     *  request to set callback function to receive NService device manager events of type MANAGER_EVENTS.
     * 
     * Parameters:
     *  callback - (function) callback function that has prototype given below in Note section.
     *
     * Returns
     *    None
     *
     * Note:
     *    Function Prototype
     *-------------------------------------------------------------------------------------------------------
     *    void callbackFn(ManagerEvent)
     */
    registerManagerCallback: function(callback)
    {
        if (typeof callback == 'function' )
        {
            NServiceDeviceCallback = callback;
        }
    },


    /*
    	 * Function: setOwnDeviceInfo
    	 *
    	 * Parameters:
    	 *  info - an object which contains NServiceOwnDeviceInfo
    	 *
    	 * Returns
    	 *  boolean
    	 *
    	 * Note:
    	 *  This API is provided in the client side only. TV throws 'NotSupportedError'
    	 */
    	setOwnDeviceInfo: function (info)
    	{
    		throw ErrorsHelper.createWebAPIException_byType('NotSupportedError');
    	},

    	/*
    	 * Function:  getOwnDeviceInfo
    	 *
    	 * Parameters:
    	 *  None
    	 *
    	 * Returns
    	 *  NServiceOwnDeviceInfo object
    	 *
    	 * Note:
    	 *  This API is provided in the client side only. TV throws 'NotSupportedError'
    	 */
    	getOwnDeviceInfo: function ()
    	{
    		throw ErrorsHelper.createWebAPIException_byType('NotSupportedError');
    	},

    	/*
    	 * Function: connectNServiceHost
    	 *
    	 * Parameters:
    	 *  hostInfo - an object which contains NServiceHostInfo
    	 *  onsuccess - (function)
    	 *  onerror - (function)
    	 *
    	 * Returns
    	 *  None.
    	 *
    	 * Note:
    	 *  This API is provided in the client side only. TV throw 'NotSupportedError'
    	 */
    	connectNServiceHost: function (hostInfo, onsuccess, onerror)
    	{
    		throw ErrorsHelper.createWebAPIException_byType('NotSupportedError');
    	},

    	/*
    	 * Function: joinGroup
    	 *
    	 * Parameters:
    	 *  info - an object which contains NServiceOwnDeviceInfo
    	 *  onsuccess - (function)
    	 *  onerror - (function)
    	 *
    	 * Returns
    	 *  boolean
    	 *
    	 * Note:
    	 *  This API is provided in the client side only. TV throw 'NotSupportedError'
    	 */
    	joinGroup: function (groupName, onsuccess, onerror)
    	{
    		throw ErrorsHelper.createWebAPIException_byType('NotSupportedError');
    	},

    /* Private
     * Function: callbackNServiceDevice
     *  This function is called when an event occur.
     * 
     * Parameters:
     *  eventType - (String) Event type
     *  sParam1 - (String) Parameter
     *    sParam2    - (String) Parameter
     *
     * Returns
     *    None.
     */
    callbackNServiceDevice: function(eventType, sParam1, sParam2)
    {
        //alert("[callbackNServiceDevice] Event type = "+eventType);
        var type = Number(eventType);
        switch(type)
        {
            case window.webapis.nservice.MGR_EVENT_DEV_STATUS:
                {
                }
                break
            case window.webapis.nservice.MGR_EVENT_DEV_DISCONNECT:
            case window.webapis.nservice.MGR_EVENT_DEV_CONNECT:    
            {
                //alert("**[callbackNServiceDevice] Device manager event**");
                var infoNServiceDevice = new window.webapis.nservice.ManagerEvent();
                var temp = sParam1.split(",");
                infoNServiceDevice.eventType = Number(eventType);
                infoNServiceDevice.uniqueID = temp[1];
                infoNServiceDevice.deviceName = temp[2];
                infoNServiceDevice.deviceType = Number(temp[5]);
                if(type == window.webapis.nservice.MGR_EVENT_DEV_DISCONNECT)
                {
                    window.webapis.oci.removeSpecificDevice(window.webapis.nservice.SEFPlugin, String(temp[1]), window.webapis.nservice.NServiceDeviceArray, window.webapis.nservice.NServiceDeviceValidArray);
                }
                if(NServiceDeviceCallback != null)
                {
                    NServiceDeviceCallback(infoNServiceDevice);
                }
                break;
            }

            case window.webapis.nservice.DEV_EVENT_JOINED_GROUP:
            case window.webapis.nservice.DEV_EVENT_LEFT_GROUP:
            {
                //alert("[callbackNServiceDevice] Group update event");
                var infoNServiceDeviceInstanceEvent = new window.webapis.nservice.NServiceDeviceEventInfo();
                var infoNServiceDeviceGroup = new window.webapis.nservice.NServiceDeviceGroupInfo();
                var temp = sParam1.split("2ZQ");
                
                infoNServiceDeviceGroup.groupName = temp[1];
                infoNServiceDeviceInstanceEvent.eventType = type;
                infoNServiceDeviceInstanceEvent.eventData = infoNServiceDeviceGroup;
                var i = 0;
                for ( i = 0; i < window.webapis.nservice.NServiceDeviceArray.length ; i++)
                {
                    if((window.webapis.nservice.NServiceDeviceArray[i].deviceID == Number(temp[0])) && (window.webapis.nservice.NServiceDeviceArray[i].deviceCallback != null))
                    {
                        window.webapis.nservice.NServiceDeviceArray[i].deviceCallback(infoNServiceDeviceInstanceEvent);
						if( window.webapis.nservice.DEV_EVENT_JOINED_GROUP == type )
						{
							window.webapis.nservice.NServiceDeviceArray[i].groupID = infoNServiceDeviceGroup.groupName;
						}
						else
						{
							window.webapis.nservice.NServiceDeviceArray[i].groupID = null;
						}
                    }
                }

                break;
            }
            
            case window.webapis.nservice.DEV_EVENT_MESSAGE_RECEIVED:
            {
                //alert("[callbackNServiceDevice] Message received event");
                var infoNServiceDeviceInstanceEvent = new window.webapis.nservice.NServiceDeviceEventInfo();
                var infoNServiceDeviceMessage = new window.webapis.nservice.NServiceDeviceMessageInfo();
                var temp = sParam1.split("2ZQ");
                var sDeviID  = temp[0];
                infoNServiceDeviceMessage.message = temp[1];
                infoNServiceDeviceMessage.context = temp[2];
                infoNServiceDeviceInstanceEvent.eventType = type;
                infoNServiceDeviceInstanceEvent.eventData = infoNServiceDeviceMessage;
                var i = 0;
                for ( i = 0; i < window.webapis.nservice.NServiceDeviceArray.length ; i++)
                {
                    if((window.webapis.nservice.NServiceDeviceArray[i].deviceID == Number(sDeviID)) && (window.webapis.nservice.NServiceDeviceArray[i].deviceCallback != null))
                    {
                        window.webapis.nservice.NServiceDeviceArray[i].deviceCallback(infoNServiceDeviceInstanceEvent);
                    }
                }
                break;
            }
            default:
                alert("[callbackNServiceDevice] unknown device event");
                break;
        }
    },

    /*
     * Class : NServiceDevice
     *
     * Properties: 
     *     devInfo    - (OCIDevInfo) device information structure
     *     deviceID    - (Number) device's ID. It is managed internally
     *
     * Private
     * Function: NServiceDevice (constructor)
     * constructor of NServiceDevice Class
     * 
     * Parameters:
     *     o devInfo    - (OCIDevInfo) device information structure
     *     o deviceID    - (Number) device's ID. It is managed internally
     */
    NServiceDevice: function(devInfo, deviceID)
    {
        this.uniqueID = devInfo.UID;
        this.name = devInfo.name;
        this.deviceID = deviceID;
        this.deviceType = devInfo.deviceType;
        this.deviceCallback = null;
	    this.groupID = curWidget.id;
        
        /* 
         * Function: getUniqueID
         *  get unique ID of the HHP device
         * 
         * Returns
         *  uniqueID - (String) unique ID of device.
         */
        this.getUniqueID = function() 
        {
            return this.uniqueID;
        }

        /* Private
         * Function:     getDeviceID
         *  get device ID of the device
         * 
         * Returns
         *  deviceID - (String) device ID of device.
         */
        this.getDeviceID = function() 
        {
            return this.deviceID;
        }


        /* 
         * Function: getName
         *  get name of the device
         * 
         * Returns
         *  name - (String) name of device.
         */
        this.getName = function() 
        {
            return this.name;
        }

        /* 
         * Function: getType
         *  get type of the device
         * 
         * Returns
         *  type - (Int) type of device.
         */
        this.getType = function() 
        {
            return this.deviceType;
        }
        

        /* 
         * Function: sendMessage
         *  sends message from DTV widget to HHP device.
         * 
         * Parameters:
         *    messasge - (DOMString) message buffer
         * Returns
         *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
         */
        this.sendMessage = function(messasge)
        {
            //alert("Send Message is called");
            var ret = window.webapis.nservice.SEFPlugin.Execute("SendCPMessage", String(this.deviceID), messasge);

            if (ret != window.webapis.nservice.ERROR_CODE_NO_ERR)
            {
                alert("[sendMessage]: ERROR! = " + ret);
                return window.webapis.nservice.ERROR_CODE_ERR;
            }
            return window.webapis.nservice.ERROR_CODE_NO_ERR;
        }

        /* Private
         * Function: broadcastMessage
         *  broadcasts message from DTV widget to devices.
         * 
         * Parameters:
         *    message - (DOMString) message buffer
         * Returns
         *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
         */
        this.broadcastMessage = function(message)
        {
            var ret = window.webapis.nservice.SEFPlugin.Execute("BroadcastCPMessage", String(this.deviceID), message );
            
            if (ret != window.webapis.nservice.ERROR_CODE_NO_ERR)
            {
                alert("[broadcastMessage]: ERROR! = " + ret);
                return window.webapis.nservice.ERROR_CODE_ERR;
            }
            return window.webapis.nservice.ERROR_CODE_NO_ERR;
        }

        /* Private
         * Function: multicastMessage
         *  multicasts message from DTV widget to devices in a group.
         * 
         * Parameters:
         *    groupID - (String) Group ID.
         *    message - (String) message buffer
         * Returns
         *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
         */
        this.multicastMessage = function(groupID, message)
        {
        	if( groupID == this.groupID)
    		{
	            var ret = window.webapis.nservice.SEFPlugin.Execute("MulticastCPMessage", String(this.deviceID), groupID, message );
				
	            if (ret != window.webapis.nservice.ERROR_CODE_NO_ERR)
	            {
	                alert("[multicastMessage]: ERROR! = " + ret);
	                return window.webapis.nservice.ERROR_CODE_ERR;
	            }
        	}	
            return window.webapis.nservice.ERROR_CODE_NO_ERR;
        }

        /*Private
         * Function: disconnectDevice
         *  disconnects device. This API is depricated.
         * 
         * Parameters:
         *    None
         * Returns
         *  ERROR_CODE_NO_ERR - if call is successful, ERROR_CODE_ERR otherwise.
         */
        this.disconnectDevice = function()
        {
	    alert("This API is depricated");
            return window.webapis.nservice.ERROR_CODE_NO_ERR;
        }
        
        
        /*Private
         * Function: registerDeviceCb
         *  to register a device instance for receiving device event. If null is passed as callback function type, then callback
         *  will be unregistered and event will no longer be received by device instance.
         * Parameters:
         *    callback - (Function) callbcak function, that is called when message arrives/Device joins or leaves group. 
         * Returns:
         *  None
         * Note:
         *    Function prototype
         *-------------------------------------------------------------------------------------------------------------
         *    void callbackFn(NServiceDeviceEventInfo)
         */
        this.registerDeviceCb = function(callback)
        {
            if (typeof callback == 'function')
            {
                this.deviceCallback = callback;
                window.webapis.nservice.SEFPlugin.Execute("RegisterCallback", String(this.deviceID));
            }
            else
            {
                this.deviceCallback = null;
                window.webapis.nservice.SEFPlugin.Execute("UnregisterCallback", String(this.deviceID));
            }
        }
        
        
        /*
         * Function: registerDeviceCallback
         *  to register a device instance for receiving device event. 
         * Parameters:
         *    callback - (Function) callbcak function, that is called when message arrives/Device joins or leaves group. 
         * Returns:
         *  None
         * Note:
         *    Function prototype
         *-------------------------------------------------------------------------------------------------------------
         *    void callbackFn(NServiceDeviceEventInfo)
         */
        this.registerDeviceCallback = function(callback)
        {
            this.registerDeviceCb(callback);
        }
        
        
        /*
         * Function: unregisterDeviceCallback
         *  to unregister a device instance from receiving device event. 
         * Parameters:
         *    callback - (Function) callbcak function, that is called when message arrives/Device joins or leaves group. 
         * Returns:
         *  None
         * Note:
         *    Function prototype
         *-------------------------------------------------------------------------------------------------------------
         *    void callbackFn(NServiceDeviceEventInfo)
         */
        this.unregisterDeviceCallback = function()
        {
            this.registerDeviceCb(null);
        }
    },
}

/*
 * Namespace: healthcaredevice
 * 	healthcaredevice module deals with healthcare devices that can be connected to TV.
 * 
 * Available From:
 * 	0.1
 */
webapis.healthcaredevice = {

    /*
     * Constant: HEALTHCARE_MANAGER_EVENT
     * Definitions for the event of device connect / disconnect
     * 
     * MGR_EVENT_DEV_CONNECT    - connection event
     * MGR_EVENT_DEV_DISCONNECT - disconnection event
     * MGR_EVENT_DEV_SEARCHED - disconnection event
     */
    MGR_EVENT_DEV_CONNECT : webapis.oci.EVENT_DEV_CONNECT,
    MGR_EVENT_DEV_DISCONNECT : webapis.oci.EVENT_DEV_DISCONNECT,
    MGR_EVENT_DEV_SEARCHED : 210,   
    MGR_EVENT_DEV_SEARCH_FINISHED : 211,   
    MGR_EVENT_DEV_CONNECT_FAILED : 212,
    MGR_EVENT_DEV_PIN_REQUESTED : 221,

    /*
     * Private
     * Constant: DEVICE_EVENT
     * Definitions for the event of device connect / disconnect
     * 
     * MGR_EVENT_GETDATA    - data from each healthcare device
     */
    MGR_EVENT_GETDATA : 200,    // this event will not passed to app. please don't make comment for this.
    // ===begin === add by scrc
    MGR_EVENT_DEV_ASSOCIATED : 202,
    MGR_EVENT_DEV_UNASSOCIATED : 203,
    MGR_EVENT_DEV_DISCONNECT_FAILED : 52,
    // ===End === add by scrc

    
    /*
     * Constant: HEALTHCAREDEVICE_DEVICE_TYPE
     * Definitions for the type of Healthcare Device
     * 
     * DEV_PULSE_OXIMETER    - pulse oximeter, not implemented yet
     * DEV_BLOOD_PRESSURE_MONITOR           - Blood pressure monitor 
     * DEV_TEMPERATURE         - temp, not implemented yet
     * DEV_WEIGHING_SCALE        - weighing scale 
     * DEV_GLUCOSE_METER      - glucose meter, not implemented yet
     * DEV_PEDOMETER        - pedometer, not implemented yet
     */    
    DEV_PULSE_OXIMETER           :4100,
    DEV_BLOOD_PRESSURE_MONITOR   :4103,
    DEV_TEMPERATURE              :4104,
    DEV_WEIGHING_SCALE           :4111,
    DEV_GLUCOSE_METER            :4113,
    DEV_CARDIOVASCULAR           :4137,
    DEV_PEDOMETER                :14113,
    SPEC_PROFILE_HF_STRENGTH     :4138,
    SPEC_PROFILE_AI_ACTIVITY_HUB :4167,
    SPEC_PROFILE_AI_MED_MINDER   :4168,

    /* 
     * Constant: HEALTHCAREDEVICE_INFO_TYPE
     * Definitions for Information Type
     * 
     * DEV_INFO_MEASURE_DATA  - measure data (ex. systolic, disatolic, MAP, weight, height and so on)
     * DEV_INFO_SYSTEM_INFO   - System Info (ex. company name, model name, etc.) 
     * DEV_INFO_PMSTORE_DATA  - pmstore data (for the device which use PM-store) 
     * DEV_INFO_UNKNOWN       - Unknown Infomation
     */  
    DEV_INFO_MEASURE_DATA :1,
    DEV_INFO_SYSTEM_INFO  :2,
    DEV_INFO_PMSTORE_DATA :5,
    // ===begin=== add by scrc
    DEV_INFO_SPP_DATA : 6,
    // ===end=== add by scrc
    DEV_INFO_UNKNOWN :0,

    /* Constant: HEALTHCAREDEVICE_SYSTEM_INFO_ELEMENT_TYPE
     * Definitions for the type of system information
     * 
     * DEV_MANUFACTURER  - manufacturer
     * DEV_MODEL_NUMBER  - model number
     */  
    DEV_MANUFACTURER        :80,
    DEV_MODEL_NUMBER        :81,

    /*
     * Constant: HEALTHCAREDEVICE_MEASURE_DATA_TYPE
     * Definitions for the type of measured data.
     * The definition of these types are from the specification,'IEEE 11073-20601', 'IEEE 11073-10407' and 'IEEE 11073-10415'
     * A health device could send vendor specific extended datas to app. It is out of scope of those specifications. So, we couldn't know what these data types mean. In this case, we just send you 'UNKNOWN_TYPE' as their data type
     *
     * MEASURE_DATA_ABS_TIMESTAMP   - AbsoluteTime 
     * MEASURE_DATA_BODY_WEIGHT - body weight 
     * MEASURE_DATA_BODY_HEIGHT - body height 
     * MEASURE_DATA_BODY_MASS   - body mass 
     * MEASURE_DATA_BODY_FAT    - body fat 
     * MEASURE_DATA_PULSE_RATE  - pulse rate 
     * MEASURE_DATA_NIBP        - NIBP 
     * MEASURE_DATA_SYSTOLIC    - systolic 
     * MEASURE_DATA_DIASTOLIC   - diastolic 
     * MEASURE_DATA_MAP - MAP
     * MEASURE_DATA_UNKNOWN          - unknown data type which is out of scope of referring specification 
     */
    MEASURE_DATA_ABS_TIMESTAMP    :2448,
    // For weighing scale 
    MEASURE_DATA_BODY_WEIGHT     :57664,
    MEASURE_DATA_BODY_HEIGHT     :57668,
    MEASURE_DATA_BODY_MASS       :57680,
    MEASURE_DATA_BODY_FAT        :57676,
    // For blood Pressure monitor    
    MEASURE_DATA_PULSE_RATE      :18474,
    MEASURE_DATA_NIBP            :18948,
    MEASURE_DATA_SYSTOLIC        :18949, 
    MEASURE_DATA_DIASTOLIC       :18950,
    MEASURE_DATA_MAP             :18951,
    // For pedometer
    MEASURE_DATA_DISTANCE        :103,
    MEASURE_DATA_ENERGY          :119,
    MEASURE_DATA_UNKNOWN         :0,
    // For glucosemeter
    MEASURE_DATA_CAPILLARY_WHOLEBLOOD       :29112,

    // For  Cardiovascular fitness and activity monitor 

    /*
     * Constants: HEALTHCAREDEVICE_UNIT
     * Definitions for the unit of data
     * 
     * DEV_UNIT_DIMENSIONLESS       - dimensionless
     * DEV_UNIT_PERCENT       - % 
     * DEV_UNIT_BPM  - bpm 
     * DEV_UNIT_KPA   - kPa 
     * DEV_UNIT_MMHG          - mmHg 
     * DEV_UNIT_KG        - kg 
     * DEV_UNIT_CM       - cm 
     * DEV_UNIT_IN          - in 
     * DEV_UNIT_LB            - lb 
     * DEV_UNIT_KGPM_SQ   - kg m-2 
     * DEV_UNIT_UNKNOWN      - unknown data unit which is out of scope of referring specification
     */

    DEV_UNIT_DIMENSIONLESS  :512,
    DEV_UNIT_PERCENT        :544,
    DEV_UNIT_BPM            :2720,
    DEV_UNIT_KPA            :3843,
    DEV_UNIT_MMHG           :3872,
    DEV_UNIT_KG             :1731,
    DEV_UNIT_CM             :1297,
    DEV_UNIT_IN             :1376,
    DEV_UNIT_LB             :1760,
    DEV_UNIT_KGPM_SQ        :1952,
    DEV_UNIT_STEP           :6656,
    DEV_UNIT_CAL            :6784,
    DEV_UNIT_M              :1280,
    DEV_UNIT_G              :1728,
    DEV_UNIT_MGPDL          :2130,
    DEV_UNIT_UNKNOWN        :0,

    // private definitions
    // GET_POSITION ------------------------------------------------------------//
    // split_var[0] :Profile ID
    // split_var[1] :OCI Event Type
    // split_var[2] :Healthcare Device Type (ex. blood pressure monitor, weighing scale)
    // split_var[3] :Healthcare Data SubType     ( System Info, Measure Data 
    GET_PROFILE_ID        :0, // position of 'profile ID' 
    GET_EVENT_TYPE        :1, // position of 'OCI event type' 
    GET_DATA_SUBTYPE      :2, // position of 'data sub type' 
    GET_DEVICE_TYPE       :3, // position of 'device type' 
    GET_PERSONID          :4, // position of 'total number of data sets' 
    GET_TOTAL_NUM_OF_DATA :5, // position of 'total number of data sets' 
    GET_START_OF_DATA     :6, // position of 'starting of data' 

    GET_PMSTORECOUNT      :4, // position of 'total number of data sets' 
    GET_PMSEGMENTCOUNT    :5, // position of 'total number of data sets' 

    OCI_PROFILE_HEALTHCARE: 0x02,

	HealthcareDeviceArray: new Array(),
	HealthcareDeviceValidArray: new Array(),
	HealthcareDeviceCallback: null,
	SEFPlugin: null,
	
	// for Emulation
	EMULStarted: 0,
	EMULSearched: 0,
	EMULConnected: 0,
	EMULData :new Array(),
	EMULSearch :new Array(),
    EMULBP1 :"AND BP",
    EMULBP2 :"HEM-7081-IT",
    EMULWS1 :"HBF-206IT",
    EMULGM1 :"AGM-3000",
    EMULRepeatTime :5000,


	/*
	 * Function: HealthcareDeviceInfo
	 *  structure that has a pieces of information which are came from Healthcare device
	 *
	 * Properties:
	 * infoType - (Number) type of the system data. DEV_INFO_SYSTEM_INFO or DEV_INFO_MEASURE_DATA type can be used.
	 * deviceType - (Number) This is an array that contains type of the device. HEALTHCAREDEVICE_DEVICE_TYPE's definitions can be used in each item.
	 *                       The definition of these types are from the specification,'IEEE 11073-20601'
	 * data - (HealthcareDeviceData) internal data array that has the information which are came from the device
	 */
    HealthcareDeviceInfo: function()
    {
		    var infoType;
        var deivceType;
		    var data;
    },

	/*
	 * Function: HealthcareDeviceData
	 *  structure that has the data of the healthcare device
	 *
	 * Properties:
	 * elementType - (Number) type of the element - it depends on infoType. if infoType is DEV_INFO_SYSTEM_INFO, SYSTEM_INFO_ELEMENT_TYPE's definitions can be used.
	 *                                              if infoType is DEV_INFO_MEASURE_DATA, MEASURE_DATA_TYPE's definitions can be used.
	 * element - (DOMString) when SystemInfo such as DEV_MANUFACTURER or DEV_MODEL_NUMBER type is used. \n
	 *           (HealthcareDeviceDataTimeInfo) when MEASURE_DATA_ABS_TIMESTAMP dataType is used. \n
	 *           (HealthcareDeviceDataMeasuredInfo) when the other dataType is used. \n
	 */
    HealthcareDeviceData: function()
    {
        var elementType;
        var element;
    },

	/*
	 * Function: HealthcareDeviceDataTimeInfo
	 *  structure that has time information of the data of the healthcare device
	 *  (Time information effects on measuring datas followed after the time stamp before meeting new time stamp.)
	 *
	 * Properties:
	 * year - (Number) year information
	 * month - (Number) month information (from 01 to 12)
	 * day - (Number) day information (from 01 to 31)
	 * time - (Number) time information (from 01 to 24)
	 * minute - (Number) minute information (from 01 to 60)
	 * second - (Number) second information (from 01 to 60)
	 */
    HealthcareDeviceDataTimeInfo: function()
    {
        var year;
        var month;
        var day;
        var time;
        var minute;
        var second;    
    },

	/*
	 * Function: HealthcareDeviceDataMeasuredInfo
	 *  structure that has healthcare device's measured data information except time related infomation.
	 *
	 * Properties:
	 * value - (Number) value of this data like 100, 50 and so on.
	 * unit - (Number) unit of this data. The definition of these types are from the specification,'IEEE 11073-20601', 'IEEE 11073-10407' and 'IEEE 11073-10415'
	 *                 A health device could send the unit which is out of scope of those specifications. So, we couldn't know what these units mean. In this case, we just send you 'UNKNOWN_UNIT' as their data unit.
	 */
    HealthcareDeviceDataMeasuredInfo: function()
    {
        var value;
        var unit;
    },
    
	/*
	 * Function: ManagerEvent
	 *  structure that has healthcare device information
	 *
	 * Properties:
	 * eventType - (Number) Type information of event such as Connect / Disconnect
	 * name - (DOMString) name of the device
	 */
    ManagerEvent: function()
	{
		var eventType;
		var name;
		var UID;
		var deviceType;
		var PIN;
	},

	/*
   * Private
	 * Function: initHealthcareDevices
	 *  request to initialize healthcare devices instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns
	 *  timeout - (int) timeout value of this function
	 */
	initHealthcareDevices: function()
	{
	    var smartHubFull = window.location.search.split('modelid=');
	    var smartHubModel = smartHubFull[1].split('&');
	    
	    var platformName = window.navigator.platform;
	    if (smartHubModel[0] == 'SDK'
	        && platformName.indexOf("Linux") == -1) {
                webapis.healthcaredevice.EMULStarted++;
                return 1000;
	    }
	    else
	    {
    		//var returnValue = window.webapis.oci.create(this.SEFPlugin, "HealthcareDevice");
		  var returnValue = window.webapis.oci.create(this.SEFPlugin, "HealthcareSensor");
			if(returnValue != window.webapis.oci.OCI_ERR)
			{
        		if(this.SEFPlugin == null)
        		{
            		this.SEFPlugin = returnValue;
        			this.SEFPlugin.OnEvent = this.callbackHealthcareDevice;
        			this.SEFPlugin.Execute("RegisterCallback", "-1");
                    return 1000;
    			}
        		else
        		{
        		    return 1;
        		}
    		}
		    return window.webapis.oci.OCI_ERR;
    	}
	},
	
	/*
	 * Function: getHealthcareDevices
	 *  request to get healthcare devices instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns
	 */
	getHealthcareDevices: function(successCallback, errorCallback)
	{
		var timeout = 1;
		timeout = webapis.healthcaredevice.initHealthcareDevices();
		
		if(webapis.healthcaredevice.EMULStarted)
		{
            if(webapis.healthcaredevice.EMULConnected!=1)
            {
                webapis.healthcaredevice.EMULConnected = 1;
                webapis.healthcaredevice.EMULSetData();
            }
		}
	    if (typeof errorCallback == "undefined")
	    {
	        errorCallback = null;
	    }
	    
	    if(timeout == window.webapis.oci.OCI_ERR)
	    {
	        if(errorCallback!=null)
	        {
    			errorCallback(ErrorsHelper.createWebAPIException_byLegacyCode(WebAPIException.NOT_FOUND_ERR));
	        }
	        return;
	    }
		
	    if (typeof successCallback != 'function')
	    {
			if(errorCallback!=null)
			{
			}
	    }
	    else
	    {
	        if(webapis.healthcaredevice.EMULStarted)
	        {
    		    setTimeout(function(){webapis.healthcaredevice.EMULsuccessCallback(webapis.healthcaredevice.HealthcareDeviceArray, successCallback);}, timeout);
    		}
    		else
    		{
        		setTimeout(function(){webapis.oci.getConnectedDevices(webapis.healthcaredevice.SEFPlugin, webapis.healthcaredevice.HealthcareDeviceArray, webapis.healthcaredevice.HealthcareDeviceValidArray, successCallback, webapis.healthcaredevice.HealthcareDevice);}, timeout);
    		}
	    }
	},

	/*
	 * Function: searchDevices
	 *  request to searched healthcare devices instances.
	 * 
	 * Parameters:
	 *
	 * Returns
	 */
    searchDevices: function(timeout)
	{
		if(webapis.healthcaredevice.initHealthcareDevices()==window.webapis.oci.OCI_ERR)
		{
		    return false;
		}
        if(webapis.healthcaredevice.EMULStarted)
        {
            if(!webapis.healthcaredevice.EMULSearched)
            {
                for(var iCount = 0; iCount<3; iCount++)
                {
//                    var randomValue = Math.floor(Math.random()*3);
                    var devInfo = new window.webapis.oci.OCIDevInfo();
                    switch(iCount)
                    {
                        case 0:
                        {
                            devInfo.UID = "00348F76B3C2";
                            devInfo.name = this.EMULBP2;
                            devInfo.deviceType = this.DEV_BLOOD_PRESSURE_MONITOR;
                            break;
                        }
                        case 1:
                        {
                            devInfo.UID = "2F425EBD6622";
                            devInfo.name = this.EMULWS1;
                            devInfo.deviceType = this.DEV_WEIGHING_SCALE;
                            break;
                        }
/*                        
                        case 2:
                        {
                            devInfo.UID = "224466FE5A7B";
                            devInfo.name = this.EMULBP1;
                            devInfo.deviceType = this.DEV_BLOOD_PRESSURE_MONITOR;
                            break;
                        }
*/                        
                        case 2:
                        {
                            devInfo.UID = "FF76F876F77F";
                            devInfo.name = this.EMULGM1;
                            devInfo.deviceType = this.DEV_GLUCOSE_METER;
                            break;
                        }
                    }
                    webapis.healthcaredevice.EMULSearch.push(devInfo);
                }
                webapis.healthcaredevice.EMULSearched = 1;
            }
		    setTimeout(function(){webapis.healthcaredevice.EMULSearchCallback();}, 1000);
            return true;
		}
		else
		{
			// === begin === modify by scrc
			// return webapis.healthcaredevice.SEFPlugin.Execute("SearchDevice", "-1");
			if (typeof timeout != "undefined") {
				return webapis.healthcaredevice.SEFPlugin.Execute("SearchDevice", "-1", timeout);
			} else {
				return webapis.healthcaredevice.SEFPlugin.Execute("SearchDevice", "-1");
			}
			// === end === modify by scrc
		}
    },
	// add by scrc
	stopSearchDevices : function() 
	{
		if (webapis.healthcaredevice.initHealthcareDevices() == window.webapis.oci.OCI_ERR) 
		{
			return false
		}
		return webapis.healthcaredevice.SEFPlugin.Execute("StopSearchDevice", "-1")
	},
	// add by scrc
    
	/*
	 * Function: connectDevice
	 *  request to connect healthcare device.
	 * 
	 * Parameters:
	 *  strUID - (string) address/uniqueID of wanted device
	 * Returns
	 */
    connectDevice: function(strUID)
	{
	    var ret = false;
        if(webapis.healthcaredevice.EMULStarted)
        {
            var iCount = 0;
            for(iCount = 0;iCount<webapis.healthcaredevice.HealthcareDeviceArray.length;iCount++)
            {
                if(webapis.healthcaredevice.HealthcareDeviceArray[iCount].getUniqueID() == strUID)
                {
                    return true;
                }
            }
            for(var iCount = 0; iCount<this.EMULSearch.length; iCount++)
            {
                if(webapis.healthcaredevice.EMULSearch[iCount].UID == strUID)
                {
                    var newDevice = new webapis.healthcaredevice.HealthcareDevice(webapis.healthcaredevice.EMULSearch[iCount], webapis.healthcaredevice.HealthcareDeviceArray.length);
                    webapis.healthcaredevice.HealthcareDeviceArray.push(newDevice);
        		    setTimeout(function(){webapis.healthcaredevice.EMULConnectionCallback(newDevice, window.webapis.healthcaredevice.MGR_EVENT_DEV_CONNECT);}, 1000);
                    return true;
                }
            }
		}
		else
		{
			ret = webapis.healthcaredevice.SEFPlugin.Execute("ConnectDevice", "-1", strUID);
		}
        return ret;
    },
    
	/*
	 * Function: disconnectDevice
	 *  request to disconnect healthcare device.
	 * 
	 * Parameters:
	 *  strUID - (string) address/uniqueID of wanted device
	 * Returns
	 */
    disconnectDevice: function(strUID)
	{
	    var ret = false;
        if(this.EMULStarted)
        {
            var iCount = 0;
            for(iCount = 0;iCount<this.HealthcareDeviceArray.length;iCount++)
            {
                if(webapis.healthcaredevice.HealthcareDeviceArray[iCount].getUniqueID() == strUID)
                {
                    var newDevice = webapis.healthcaredevice.HealthcareDeviceArray[iCount];
        			webapis.healthcaredevice.HealthcareDeviceArray.splice(iCount, 1);
        		    setTimeout(function(){webapis.healthcaredevice.EMULConnectionCallback(newDevice, window.webapis.healthcaredevice.MGR_EVENT_DEV_DISCONNECT);}, 1000);
                    return true;
                }
            }
		}
		else
		{
			ret = webapis.healthcaredevice.SEFPlugin.Execute("DisconnectDevice", "-1", strUID);
		}
        return ret;
    },
    
	/*
	 * Function: setDevicePIN
	 *  set PIN of healthcare device.
	 * 
	 * Parameters:
	 *  strUID - (string) address/uniqueID of wanted device
	 *  strPIN - (string) PIN
	 * Returns
	 */
    setDevicePIN: function(strUID, strPIN)
	{
		var ret = false;
		if(this.EMULStarted)
		{
		    ;
		}
		else
		{
			ret = webapis.healthcaredevice.SEFPlugin.Execute("SetDevicePIN", "-1", strUID, strPIN);
		}
		return ret;
	},
	// ===begin === Add by scrc
	requestAssociation : function(a) 
	{
		return webapis.healthcaredevice.SEFPlugin.Execute("ConnectChannel", "-1", a);
	},

	sendBluetoothData : function(a, b) 
	{
		return webapis.healthcaredevice.SEFPlugin.Execute("SendBluetoothData", "0", a, b)
	},
	// ===End ===Add by scrc

	/*
	 * Function: registerManagerCallback
	 *  request to set callback function to receive healthcare device manager event such as MGR_EVENT_DEV_CONNECT / MGR_EVENT_DEV_DISCONNECT
	 * 
	 * Parameters:
	 *  callback - (function) callback function that has prototype given below in Note section.
	 *
	 * Returns
	 *	None
	 *
	 * Note:
	 *	Function Prototype
	 *------------------------------------------------------------------------------------------------------------------------------
	 *	void callbackFn(ManagerEvent)
	 */
	registerManagerCallback: function(callback)
	{
	    if (typeof callback == 'function' )
	    {
	    	HealthcareDeviceCallback = callback;
	    }
	    else
	    {
	        HealthcareDeviceCallback = null;
	    }
	},
    
	/*
     * Private
	 */
    EMULsuccessCallback: function(arrayDevice, successCallback)
    {
        successCallback(arrayDevice);
    },

	/*
     * Private
	 */
    EMULStruct: function()
    {
        var name;
        var data;
    },

	/*
     * Private
	 */
    EMULSetData: function()
    {
/*        
        var data1 = new webapis.healthcaredevice.EMULStruct();
        data1.name = this.EMULBP1;
        var AND_BP1 = new Array();        
        AND_BP1.push("1,200,2,1,4103,2,80,A&D Medical,81,UA-767PBT-C");
        AND_BP1.push("1,200,1,4103,4,2448,2011,9,28,17,41,30,18949,101.00,3872,18950,66.00,3872,18951,82.00,3872");
        AND_BP1.push("1,200,1,4103,2,2448,2011,9,28,17,41,30,18474,64.00,2720");
        data1.data = AND_BP1;
        webapis.healthcaredevice.EMULData.push(data1);

        var data2 = new webapis.healthcaredevice.EMULStruct();
        data2.name = this.EMULBP1;
        var AND_BP2 = new Array();        
        AND_BP2.push("1,200,1,4103,8,2448,2011,9,28,17,45,49,18949,97.00,3872,18950,66.00,3872,18951,79.00,3872,2448,2011,9,28,17,46,31,18949,96.00,3872,18950,70.00,3872,18951,82.00,3872");
        AND_BP2.push("1,200,1,4103,4,2448,2011,9,28,17,45,49,18474,62.00,2720,2448,2011,9,28,17,46,31,18474,64.00,2720");
        AND_BP2.push("1,200,2,1,4103,2,80,A&D Medical,81,UA-767PBT-C");
        AND_BP2.push("1,200,1,4103,4,2448,2011,9,28,18,8,10,18949,94.00,3872,18950,73.00,3872,18951,81.00,3872");
        AND_BP2.push("1,200,1,4103,2,2448,2011,9,28,18,8,10,18474,71.00,2720");
        data2.data = AND_BP2;
        webapis.healthcaredevice.EMULData.push(data2);

        var data3 = new webapis.healthcaredevice.EMULStruct();
        data3.name = this.EMULBP1;
        var AND_BP3 = new Array();        
        AND_BP3.push("1,200,1,4103,16,2448,2011,9,28,18,14,6,18949,98.00,3872,18950,69.00,3872,18951,80.00,3872,2448,2011,9,28,18,14,57,18949,98.00,3872,18950,69.00,3872,18951,76.00,3872,2448,2011,9,28,18,15,59,18949,94.00,3872,18950,62.00,3872,18951,71.00,3872,2448,2011,9,28,18,17,2,18949,102.00,3872,18950,59.00,3872,18951,74.00,3872");
        AND_BP3.push("1,200,1,4103,8,2448,2011,9,28,18,14,6,18474,62.00,2720,2448,2011,9,28,18,14,57,18474,62.00,2720,2448,2011,9,28,18,15,59,18474,64.00,2720,2448,2011,9,28,18,17,2,18474,65.00,2720");
        AND_BP3.push("1,200,2,1,4103,2,80,A&D Medical,81,UA-767PBT-C");
        AND_BP3.push("1,200,1,4103,4,2448,2011,9,28,18,17,42,18949,100.00,3872,18950,66.00,3872,18951,82.00,3872");
        AND_BP3.push("1,200,1,4103,2,2448,2011,9,28,18,17,42,18474,61.00,2720");
        data3.data = AND_BP3;
        webapis.healthcaredevice.EMULData.push(data3);
*/
        var data4 = new webapis.healthcaredevice.EMULStruct();
        data4.name = this.EMULBP2;
        var OMRON_BP1 = new Array();        
        OMRON_BP1.push("1,200,2,1,4103,2,80,OMRON HEALTHCARE,81,HEM-7081-IT");
        OMRON_BP1.push("1,200,1,0,65,6,2677,18948,3:18949_116:18950_78:18951_90:,3872,2448,-9999,2010-01-01 00:13:15,-9999,2636,18474,58,2720,2448,-9999,2010-01-01 00:13:15,-9999,2636,61458,0,512,2448,-9999,2010-01-01 00:13:15,-9999,");
        OMRON_BP1.push("1,200,1,0,65,6,2677,18948,3:18949_112:18950_66:18951_81:,3872,2448,-9999,2011-11-01 10:21:00,-9999,2636,18474,85,2720,2448,-9999,2011-11-01 10:21:00,-9999,2636,61458,0,512,2448,-9999,2011-11-01 10:21:00,-9999,");
        data4.data = OMRON_BP1;
        webapis.healthcaredevice.EMULData.push(data4);

        var data5 = new webapis.healthcaredevice.EMULStruct();
        data5.name = this.EMULBP2;
        var OMRON_BP2 = new Array();        
        OMRON_BP2.push("3,200,2,1,4103,2,80,OMRON HEALTHCARE,81,HEM-7081-IT");
        OMRON_BP2.push("3,200,1,0,65,6,2677,18948,3:18949_102:18950_67:18951_78:,3872,2448,-9999,2011-10-05 15:47:50,-9999,2636,18474,62,2720,2448,-9999,2011-10-05 15:47:50,-9999,2636,61458,0,512,2448,-9999,2011-10-05 15:47:50,-9999,");
        data5.data = OMRON_BP2;
        webapis.healthcaredevice.EMULData.push(data5);

        var data6 = new webapis.healthcaredevice.EMULStruct();
        data6.name = this.EMULBP2;
        var OMRON_BP3 = new Array();        
        OMRON_BP3.push("3,200,2,1,4103,2,80,OMRON HEALTHCARE,81,HEM-7081-IT");
        OMRON_BP3.push("3,200,1,0,65,6,2677,18948,3:18949_102:18950_67:18951_78:,3872,2448,-9999,2011-10-05 15:47:50,-9999,2636,18474,62,2720,2448,-9999,2011-10-05 15:47:50,-9999,2636,61458,0,512,2448,-9999,2011-10-05 15:47:50,-9999,");
        OMRON_BP3.push("3,200,1,0,65,6,2677,18948,3:18949_105:18950_66:18951_79:,3872,2448,-9999,2011-10-05 15:50:39,-9999,2636,18474,62,2720,2448,-9999,2011-10-05 15:50:39,-9999,2636,61458,0,512,2448,-9999,2011-10-05 15:50:39,-9999,");
        data6.data = OMRON_BP3;
        webapis.healthcaredevice.EMULData.push(data6);

        var data7 = new webapis.healthcaredevice.EMULStruct();
        data7.name = this.EMULWS1;
        var OMRON_WS1 = new Array();        
        OMRON_WS1.push("2,200,2,1,4111,2,80,OMRON HEALTHCARE,81,HBF-206IT");
        OMRON_WS1.push("2,200,1,0,49,20,2646,57664,74,1731,2448,-9999,2010-01-01 00:00:00,-9999,2646,57668,150,1297,2448,-9999,2010-01-01 00:00:00,-9999,2646,57680,32.9,1952,2448,-9999,2010-01-01 00:00:00,-9999,2646,57676,37.1,544,2448,-9999,2010-01-01 00:00:00,-9999,2646,61441,1.575e+06,6784,2448,-9999,2010-01-01 00:00:00,-9999,2636,61442,27,544,2448,-9999,2010-01-01 00:00:00,-9999,2636,61443,76,2368,2448,-9999,2010-01-01 00:00:00,-9999,2677,61449,4:61450_24:61451_0:61452_0:61453_0:,544,2448,-9999,2010-01-01 00:00:00,-9999,2636,61454,2,512,2448,-9999,2010-01-01 00:00:00,-9999,2677,61455,2:61456_0:61457_70:,512,2448,-9999,2010-01-01 00:00:00,-9999,");
//        OMRON_WS1.push("2,200,1,4111,24,2448,2010,4,16,22,13,0,57664,56.40,1731,2448,2010,4,16,22,13,0,57668,150.00,1297,2448,2010,4,16,22,13,0,57680,25.10,1952,2448,2010,4,16,22,13,0,57676,37.50,544,2448,2010,4,16,22,13,0,61441,1191000.00,6784,2448,2010,4,16,22,13,0,61442, 8.00,544,2448,2010,4,16,22,13,0,61443,69.00,2368,2448,2010,4,16,22,13,0,61450,23.90,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2010,4,16,22,13,0,61454, 1.00,512,2448,2010,4,16,22,13,0,61456, 1.00,512,61457,69.00,512");
        data7.data = OMRON_WS1;
        webapis.healthcaredevice.EMULData.push(data7);

/*
        var data8 = new webapis.healthcaredevice.EMULStruct();
        data8.name = this.EMULWS1;
        var OMRON_WS2 = new Array();        
        OMRON_WS2.push("2,200,2,1,4111,2,80,OMRON HEALTHCARE,81,HBF-206IT");
        OMRON_WS2.push("2,200,1,4111,24,2448,2010,4,16,22,13,0,57664,56.40,1731,2448,2010,4,16,22,13,0,57668,150.00,1297,2448,2010,4,16,22,13,0,57680,25.10,1952,2448,2010,4,16,22,13,0,57676,37.50,544,2448,2010,4,16,22,13,0,61441,1191000.00,6784,2448,2010,4,16,22,13,0,61442, 8.00,544,2448,2010,4,16,22,13,0,61443,69.00,2368,2448,2010,4,16,22,13,0,61450,23.90,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2010,4,16,22,13,0,61454, 1.00,512,2448,2010,4,16,22,13,0,61456, 1.00,512,61457,69.00,512");
        OMRON_WS2.push("2,200,1,4111,24,2448,2011,10,5,15,30,0,57664,17.50,1731,2448,2011,10,5,15,30,0,57668,150.00,1297,2448,2011,10,5,15,30,0,57680, 7.80,1952,2448,2011,10,5,15,30,0,57676, 0.00,544,2448,2011,10,5,15,30,0,61441, 0.00,6784,2448,2011,10,5,15,30,0,61442, 0.00,544,2448,2011,10,5,15,30,0,61443, 0.00,2368,2448,2011,10,5,15,30,0,61450, 0.00,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2011,10,5,15,30,0,61454, 1.00,512,2448,2011,10,5,15,30,0,61456, 1.00,512,61457,70.00,512");
        OMRON_WS2.push("2,200,1,4111,24,2448,2011,10,5,15,31,0,57664,22.60,1731,2448,2011,10,5,15,31,0,57668,150.00,1297,2448,2011,10,5,15,31,0,57680,10.00,1952,2448,2011,10,5,15,31,0,57676, 0.00,544,2448,2011,10,5,15,31,0,61441, 0.00,6784,2448,2011,10,5,15,31,0,61442, 0.00,544,2448,2011,10,5,15,31,0,61443, 0.00,2368,2448,2011,10,5,15,31,0,61450, 0.00,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2011,10,5,15,31,0,61454, 1.00,512,2448,2011,10,5,15,31,0,61456, 1.00,512,61457,70.00,512");
        OMRON_WS2.push("2,200,1,4111,24,2448,2011,10,5,15,32,0,57664,24.20,1731,2448,2011,10,5,15,32,0,57668,150.00,1297,2448,2011,10,5,15,32,0,57680,10.80,1952,2448,2011,10,5,15,32,0,57676, 0.00,544,2448,2011,10,5,15,32,0,61441, 0.00,6784,2448,2011,10,5,15,32,0,61442, 0.00,544,2448,2011,10,5,15,32,0,61443, 0.00,2368,2448,2011,10,5,15,32,0,61450, 0.00,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2011,10,5,15,32,0,61454, 1.00,512,2448,2011,10,5,15,32,0,61456, 1.00,512,61457,70.00,512");
//        OMRON_WS2.push("2,200,1,4111,24,2448,2011,10,5,15,32,0,57664,22.70,1731,2448,2011,10,5,15,32,0,57668,150.00,1297,2448,2011,10,5,15,32,0,57680,10.10,1952,2448,2011,10,5,15,32,0,57676, 0.00,544,2448,2011,10,5,15,32,0,61441, 0.00,6784,2448,2011,10,5,15,32,0,61442, 0.00,544,2448,2011,10,5,15,32,0,61443, 0.00,2368,2448,2011,10,5,15,32,0,61450, 0.00,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2011,10,5,15,32,0,61454, 1.00,512,2448,2011,10,5,15,32,0,61456, 1.00,512,61457,70.00,512");
        data8.data = OMRON_WS2;
        webapis.healthcaredevice.EMULData.push(data8);

        var data9 = new webapis.healthcaredevice.EMULStruct();
        data9.name = this.EMULWS1;
        var OMRON_WS3 = new Array();        
        OMRON_WS3.push("2,200,2,1,4111,2,80,OMRON HEALTHCARE,81,HBF-206IT");
        OMRON_WS3.push("2,200,1,4111,24,2448,2011,10,5,15,36,0,57664,29.30,1731,2448,2011,10,5,15,36,0,57668,150.00,1297,2448,2011,10,5,15,36,0,57680,13.00,1952,2448,2011,10,5,15,36,0,57676, 8.10,544,2448,2011,10,5,15,36,0,61441,848000.00,6784,2448,2011,10,5,15,36,0,61442, 1.00,544,2448,2011,10,5,15,36,0,61443,19.00,2368,2448,2011,10,5,15,36,0,61450,35.20,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2011,10,5,15,36,0,61454, 1.00,512,2448,2011,10,5,15,36,0,61456, 1.00,512,61457,70.00,512");
        OMRON_WS3.push("2,200,1,4111,24,2448,2011,10,5,15,37,0,57664,56.20,1731,2448,2011,10,5,15,37,0,57668,150.00,1297,2448,2011,10,5,15,37,0,57680,25.00,1952,2448,2011,10,5,15,37,0,57676,37.60,544,2448,2011,10,5,15,37,0,61441,1188000.00,6784,2448,2011,10,5,15,37,0,61442, 8.00,544,2448,2011,10,5,15,37,0,61443,70.00,2368,2448,2011,10,5,15,37,0,61450,23.80,544,61451, 0.00,544,61452, 0.00,544,61453, 0.00,544,2448,2011,10,5,15,37,0,61454, 1.00,512,2448,2011,10,5,15,37,0,61456, 1.00,512,61457,70.00,512");
        data9.data = OMRON_WS3;
        webapis.healthcaredevice.EMULData.push(data9);
*/
        var data11 = new webapis.healthcaredevice.EMULStruct();
        data11.name = this.EMULGM1;
        var ALLMEDICUS_GM2 = new Array();
        ALLMEDICUS_GM2.push("1,200,2,1,4113,2,80,Allmedicus,81,AP001");
        ALLMEDICUS_GM2.push("0,200,1,0,1,2,2636,29112,311,2130,2448,-9999,2012-04-23 16:31:00,-9999");
        data11.data = ALLMEDICUS_GM2;
        webapis.healthcaredevice.EMULData.push(data11);

        setTimeout(function(){window.webapis.healthcaredevice.EMULDataCallback();}, this.EMULRepeatTime);
    },

	/*
     * Private
	 */
    EMULDataCallback: function()
    {
        var randomValue = Math.floor(Math.random()*webapis.healthcaredevice.EMULData.length);
        for(var iArray=0;iArray<webapis.healthcaredevice.HealthcareDeviceArray.length;iArray++)
        {
            if(webapis.healthcaredevice.HealthcareDeviceArray[iArray].getName()==webapis.healthcaredevice.EMULData[randomValue].name)
            {
                if(webapis.healthcaredevice.HealthcareDeviceArray[iArray].deviceCallback!=null)
                {
                    for(var iCount=0;iCount<webapis.healthcaredevice.EMULData[randomValue].data.length;iCount++)
                    {
                        var returndata = webapis.healthcaredevice.parseCallbackData(webapis.healthcaredevice.EMULData[randomValue].data[iCount]);
                        webapis.healthcaredevice.HealthcareDeviceArray[iArray].deviceCallback(returndata);
                    }
                }
            }
        }
        setTimeout(function(){window.webapis.healthcaredevice.EMULDataCallback();}, this.EMULRepeatTime);
    },

	/*
     * Private
	 */
    EMULSearchCallback: function()
    {
        if(HealthcareDeviceCallback!=null)
        {
            for(var iCount = 0; iCount<webapis.healthcaredevice.EMULSearch.length; iCount++)
            {
                var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
                infoManager.eventType = this.MGR_EVENT_DEV_SEARCHED;
                infoManager.name = this.EMULSearch[iCount].name;
                infoManager.UID = this.EMULSearch[iCount].UID;
                infoManager.deviceType = this.EMULSearch[iCount].deviceType;
                HealthcareDeviceCallback(infoManager);
            }
            var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
            infoManager.eventType = this.MGR_EVENT_DEV_SEARCH_FINISHED;
            infoManager.name = null;
            infoManager.UID = null;
            infoManager.deviceType = 0;
            HealthcareDeviceCallback(infoManager);
        }
    },

	/*
     * Private
	 */
    EMULConnectionCallback: function(newDevice, eventType)
    {
        if(HealthcareDeviceCallback!=null)
        {
            var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
            infoManager.eventType = eventType;
            infoManager.name = newDevice.getName();
            infoManager.UID = newDevice.getUniqueID();
            infoManager.deviceType = newDevice.getType();
            HealthcareDeviceCallback(infoManager);
        }
    },

	/*
     * Private
	 */
	callbackHealthcareDevice: function(eventType, sParam1, sParam2)
	{
	    var type = Number(eventType);
		switch(type)
		{
		    case window.webapis.healthcaredevice.MGR_EVENT_DEV_SEARCHED:
// ===begin===add by scrc
		    //case window.webapis.healthcaredevice.MGR_EVENT_DEV_PIN_REQUESTED:
		    case window.webapis.healthcaredevice.MGR_EVENT_DEV_ASSOCIATED :
		    case window.webapis.healthcaredevice.MGR_EVENT_DEV_UNASSOCIATED :
// ===end===add by scrc
		    {
		        if(HealthcareDeviceCallback!=null)
		        {
				var event = window.webapis.oci.parseDeviceEvent(sParam1);
				var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
				infoManager.eventType = Number(eventType);
				infoManager.name = event.name;
				infoManager.UID = event.UID;
				infoManager.deviceType = event.deviceType;
				HealthcareDeviceCallback(infoManager);
			}
		        break;
		    }
// ===begin===modify by scrc
		    case window.webapis.healthcaredevice.MGR_EVENT_DEV_PIN_REQUESTED :
		    {
			if (HealthcareDeviceCallback != null) {
				var event = sParam1.split(window.webapis.oci.DELIMITER_EVENT_PARAM);
				if (event != null) 
				{
					var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
					infoManager.eventType = Number(event[0]);
					infoManager.UID = String(event[1]);
					infoManager.name = String(event[2]);
					infoManager.PIN = event[4];
					HealthcareDeviceCallback(infoManager);
				}
			}
			break;
		    }
// ===end=== modifu by scrc
		    case window.webapis.healthcaredevice.MGR_EVENT_DEV_SEARCH_FINISHED:
		    {
		        if(HealthcareDeviceCallback!=null)
		        {
				var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
				infoManager.eventType = Number(eventType);
				infoManager.name = null;
				infoManager.UID = null;
				infoManager.deviceType = 0;
				HealthcareDeviceCallback(infoManager);
			}
		        break;
		    }
			case window.webapis.healthcaredevice.MGR_EVENT_DEV_CONNECT:
			{
		        if(HealthcareDeviceCallback!=null)
		        {
				var event = window.webapis.oci.parseDeviceEvent(sParam1);
				var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
				infoManager.eventType = Number(eventType);
				infoManager.name = null;
				infoManager.UID = event.UID;
				infoManager.deviceType = 0;
				HealthcareDeviceCallback(infoManager);
			}
			    break;
			}
			case window.webapis.healthcaredevice.MGR_EVENT_DEV_CONNECT_FAILED:
			{
				if(HealthcareDeviceCallback!=null)
				{
					var event = window.webapis.oci.parseDeviceEvent(sParam1);
					var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
					infoManager.eventType = window.webapis.healthcaredevice.MGR_EVENT_DEV_CONNECT_FAILED;
					infoManager.name = event.name;
					infoManager.UID = event.UID;
					infoManager.deviceType = event.deviceType;
					HealthcareDeviceCallback(infoManager);
				}
				break;
			}
			// ===begin=== add by scrc
			case window.webapis.healthcaredevice.MGR_EVENT_DEV_DISCONNECT_FAILED:
			// ===end=== add by scrc
			case window.webapis.healthcaredevice.MGR_EVENT_DEV_DISCONNECT:
			{
				var event = window.webapis.oci.parseDeviceEvent(sParam1);
				if(HealthcareDeviceCallback!=null)
				{
					var infoManager = new window.webapis.healthcaredevice.ManagerEvent();
					infoManager.eventType = Number(eventType);
					infoManager.name = event.name;
					infoManager.UID = event.UID;
					infoManager.deviceType = event.deviceType;
					HealthcareDeviceCallback(infoManager);
				}
				window.webapis.oci.removeSpecificDevice(window.webapis.healthcaredevice.SEFPlugin, event.UID, window.webapis.healthcaredevice.HealthcareDeviceArray, window.webapis.healthcaredevice.HealthcareDeviceValidArray);
				break;
			}
			case window.webapis.healthcaredevice.MGR_EVENT_GETDATA:
			{
				var split_var  = sParam1.split(window.webapis.oci.DELIMITER_EVENT_PARAM);
				var sDeviceID = split_var[0];
				var iCount = 0;
				for (iCount = 0; iCount < window.webapis.healthcaredevice.HealthcareDeviceArray.length ; iCount++)
				{
					if((window.webapis.healthcaredevice.HealthcareDeviceArray[iCount].deviceID == Number(sDeviceID)) && (window.webapis.healthcaredevice.HealthcareDeviceArray[iCount].deviceCallback != null))
					{
						var returndata = window.webapis.healthcaredevice.parseCallbackData(sParam1);
						window.webapis.healthcaredevice.HealthcareDeviceArray[iCount].deviceCallback(returndata);
					}
				}
				break;
			}
			default:
				break;
		}
	},

	/*
     * Private
	 */
    parseSystemInfoData: function(value)
    {
        var systemInfo = new webapis.healthcaredevice.HealthcareDeviceInfo();
        systemInfo.infoType = webapis.healthcaredevice.DEV_INFO_SYSTEM_INFO;
        systemInfo.deviceType = new Array();
        systemInfo.data = new Array();
        
        var j = webapis.healthcaredevice.GET_DEVICE_TYPE;
	    var typeCount  = Number(value[j++]);//deviceTypeCount
	    for(var i = 0 ; i < typeCount ; i++)
	    {
    		systemInfo.deviceType.push(Number(value[j++]));
	    }
    	
	    var infoCount = Number(value[j++]);
	    for(var i = 0 ; i < infoCount ; i++)
	    {
	        var systemData = new webapis.healthcaredevice.HealthcareDeviceData();
	        systemData.elementType = Number(value[j++]);
	        systemData.element = value[j++];
    	    systemInfo.data.push(systemData);
	    }
	    return systemInfo;
    },

	/*
     * Private
	 */
    parseAbsoluteTimestamp: function(measuredInfo, nomenclatureCode, typeID, sValue, unitCode)
    {
        var healthcaredeviceData = new webapis.healthcaredevice.HealthcareDeviceData();
	    healthcaredeviceData.elementType = Number(nomenclatureCode);
        var healthcareElement = new webapis.healthcaredevice.HealthcareDeviceDataTimeInfo();
        var j = 0;
        var k = 0;
        var l = 0;
	    var value  = sValue.split("-");
        healthcareElement.year = value[j++];
        healthcareElement.month = value[j++];
        var value2 = value[j++].split(" ");
        healthcareElement.day = value2[k++];
        var value3 = value2[k++].split(":");
        healthcareElement.time = value3[l++];
        healthcareElement.minute = value3[l++];
        healthcareElement.second = value3[l++];
        healthcaredeviceData.element = healthcareElement;
        measuredInfo.data.push(healthcaredeviceData);
    },
    
	/*
     * Private
	 */
    parseDefaultValue: function(measuredInfo, nomenclatureCode, typeID, sValue, unitCode)
    {
	    var value  = sValue.split(":");
	    if(value[1] != undefined)
	    {
	        for(var i=0;i<Number(value[0]);i++)
	        {
			var healthcaredeviceData = new webapis.healthcaredevice.HealthcareDeviceData();
			var element = value[i+1].split("_");
			var healthcareElement = new webapis.healthcaredevice.HealthcareDeviceDataMeasuredInfo();
			var j = 0;
			healthcaredeviceData.elementType = Number(element[j++]);
			healthcareElement.value = element[j++];
			healthcareElement.unit = Number(unitCode);
			healthcaredeviceData.element = healthcareElement;
			measuredInfo.data.push(healthcaredeviceData);
	        }
	    }
	    else
	    {
		var healthcaredeviceData = new webapis.healthcaredevice.HealthcareDeviceData();
		var healthcareElement = new webapis.healthcaredevice.HealthcareDeviceDataMeasuredInfo();
		healthcaredeviceData.elementType = Number(typeID);
		healthcareElement.value = Number(value[0]);
		healthcareElement.unit = Number(unitCode);
		healthcaredeviceData.element = healthcareElement;
		measuredInfo.data.push(healthcaredeviceData);
	    }
    },    
    
    /*
     * Private
     */
    parseHealthData: function(value)
    {
        var measuredInfo = new webapis.healthcaredevice.HealthcareDeviceInfo();
        measuredInfo.infoType = webapis.healthcaredevice.DEV_INFO_MEASURE_DATA;
        measuredInfo.deviceType = new Array();
	    measuredInfo.data = new Array();

        measuredInfo.deviceType.push(Number(value[webapis.healthcaredevice.GET_DEVICE_TYPE]));
        
	    var total = Number(value[webapis.healthcaredevice.GET_TOTAL_NUM_OF_DATA]);
	    var j = webapis.healthcaredevice.GET_START_OF_DATA;
	    for(var i = 0 ; i < total ; i++)
	    {
	        var nomenclatureCode = Number(value[j++]);
	        var typeId = Number(value[j++]);
	        var sValue = value[j++];
	        var unitCode = value[j++];
		    switch(nomenclatureCode)
		    {
			case webapis.healthcaredevice.MEASURE_DATA_ABS_TIMESTAMP: // time stamp
			{
				webapis.healthcaredevice.parseAbsoluteTimestamp(measuredInfo, nomenclatureCode, typeId, sValue, unitCode);
				break;
			}
			default:
			{
				webapis.healthcaredevice.parseDefaultValue(measuredInfo, nomenclatureCode, typeId, sValue, unitCode);
				break;
			}   
  		    }
		}
		return measuredInfo;
    },

	/*
     * Private
	 */
    parsePmstoreData: function(value)
    {
        var measuredInfo = new webapis.healthcaredevice.HealthcareDeviceInfo();
        measuredInfo.infoType = webapis.healthcaredevice.DEV_INFO_MEASURE_DATA;
        measuredInfo.deviceType = new Array();
	    measuredInfo.data = new Array();

        measuredInfo.deviceType.push(Number(value[webapis.healthcaredevice.GET_DEVICE_TYPE]));
        
        var pmstorecount = Number(value[webapis.healthcaredevice.GET_PMSTORECOUNT]);
        var currentLocation = Number(webapis.healthcaredevice.GET_PMSTORECOUNT)+1;
        
        for(var storeCount = 0; storeCount < pmstorecount; storeCount++)
        {
            var pmsegmentcount = Number(value[currentLocation++]);
            for(var segCount = 0; segCount < pmsegmentcount; segCount++)
            {
                var personId = Number(value[currentLocation++]);
                var segentrycount = Number(value[currentLocation++]);
                for(var entryCount = 0; entryCount < segentrycount; entryCount++)
                {
                    value[currentLocation++]; // segment entry header(type)
                    value[currentLocation++]; // segment entry header(value)
                    dataCount = Number(value[currentLocation++]);
                    for(iCount = 0; iCount < dataCount; iCount++)
                    {
            	        var nomenclatureCode = Number(value[currentLocation++]);
            	        var typeId = Number(value[currentLocation++]);
            	        var sValue = value[currentLocation++];
            	        var unitCode = value[currentLocation++];
            		    switch(nomenclatureCode)
            		    {
            			    case webapis.healthcaredevice.MEASURE_DATA_ABS_TIMESTAMP: // time stamp
            			    {
            			        webapis.healthcaredevice.parseAbsoluteTimestamp(measuredInfo, nomenclatureCode, typeId, sValue, unitCode);
            				    break;
            			    }
            				default:
            				{
            			        webapis.healthcaredevice.parseDefaultValue(measuredInfo, nomenclatureCode, typeId, sValue, unitCode);
            				    break;
            				}   
              		    }
                    }
                }
            }
        }
		return measuredInfo;
    },

	/*
     * Private
	 */
    parseCallbackData: function(value)
    {
	    var split_var  = value.split(webapis.oci.DELIMITER_EVENT_PARAM);
	    var dataSubType = Number(split_var[webapis.healthcaredevice.GET_DATA_SUBTYPE]);
//	    alert("parseCallbackData = " + dataSubType);
	    if(dataSubType == webapis.healthcaredevice.DEV_INFO_SYSTEM_INFO)
	    {
	        return webapis.healthcaredevice.parseSystemInfoData(split_var);
	    }
	    else if(dataSubType == webapis.healthcaredevice.DEV_INFO_MEASURE_DATA)
	    {
		    return webapis.healthcaredevice.parseHealthData(split_var);
	    }
	    else if(dataSubType == webapis.healthcaredevice.DEV_INFO_PMSTORE_DATA)
	    {
		    return webapis.healthcaredevice.parsePmstoreData(split_var);
	    }
// ===begin=== add by scrc
	    else if (dataSubType == webapis.healthcaredevice.DEV_INFO_SPP_DATA) 
	    {
		    return value;
	    }
// ===end=== add by scrc
    },

	/*
	 * Class: HealthcareDevice
	 * 
	 * Properties:
	 *	devInfo	- (OCIDevInfo) device information structure
	 *	deviceID	- (Number) healthcaredevice's ID. It is managed internally
	 * 
	 * Private
	 * Function: HealthcareDevice (constructor)
	 * constructor of HealthcareDevice Class
	 * 
	 * Parameters:
	 * 	o devInfo	- (OCIDevInfo) device information structure
	 * 	o deviceID	- (Number) healthcaredevice's ID. It is managed internally
	 */
	HealthcareDevice: function(devInfo, deviceID)
	{
		this.uniqueID = devInfo.UID;
		this.name = devInfo.name;
		this.deviceID = deviceID;
		this.deviceType = devInfo.deviceType;
		this.callback = null;

		/* 
		 * Function: getUniqueID
		 *  get unique ID of the healthcaredevice
		 * 
		 * Returns
		 *  uniqueID - (String) unique ID of healthcaredevice.
		 */
		this.getUniqueID = function() 
		{
			return this.uniqueID;
		}

		/*
		 * Private
		 * Function: getDeviceID
		 *  get device ID of the healthcaredevice
		 * 
		 * Returns
		 *  deviceID - (String) device ID of healthcaredevice.
		 */
		this.getDeviceID = function() 
		{
			return this.deviceID;
		}

		/* 
		 * Function: getName
		 *  get name of the healthcare device
		 * 
		 * Returns
		 *  name - (String) name of healthcare device.
		 */
		this.getName = function() 
		{
			return this.name;
		}

		/* 
		 * Function: getType
		 *  get type of the healthcare device
		 * 
		 * Returns
		 *  type - (Int) type of healthcare device.
		 */
		this.getType = function() 
		{
			return this.deviceType;
		}

	    /*
	     * Function: registerDeviceCallback
		 *  to register a device instance for receiving device event. If null is passed as callback function type, then callback
		 *  will be unregistered and event will no longer be received by device instance.
	     * 
	     * Parameters:
	     *  callback - (function) callback function that has HealthcareDeviceInfo type parameter.
	     *
	     * Returns
	     */
		this.registerDeviceCallback = function(callback)
	    {
	        if (typeof callback == 'function')
	        {
	    	    this.deviceCallback = callback;
	    	    if(window.webapis.healthcaredevice.SEFPlugin != null)
                {
				    window.webapis.healthcaredevice.SEFPlugin.Execute("RegisterCallback", String(this.deviceID));
				}
	        }
	        else
	        {
		        this.deviceCallback = null;
	    	    if(window.webapis.healthcaredevice.SEFPlugin != null)
                {
    			    window.webapis.healthcaredevice.SEFPlugin.Execute("UnregisterCallback", String(this.deviceID));
    			}
	        }
	    }
	    
	}
}

/*
 * Namespace: microphone
 * 	microphone module deals with microphone devices that can be connected to TV.
 * 
 * Available From:
 * 	0.1
 */
webapis.microphone = {

    
	/*
     * Constant: MICROPHONE_MANAGER_EVENT
     * MGR_EVENT_DEV_CONNECT    - connection event
     * MGR_EVENT_DEV_DISCONNECT - disconnection event
     */
    MGR_EVENT_DEV_CONNECT : webapis.oci.EVENT_DEV_CONNECT,
    MGR_EVENT_DEV_DISCONNECT : webapis.oci.EVENT_DEV_DISCONNECT,
    
    /*
     * Private
     * Constant: DEVICE_PLAY_ERROR_EVENT
     * Definitions for the event to send the error status.
     * 
     * MGR_EVENT_PLAY_FAIL    - data from each microphone device
     */
    MGR_EVENT_PLAY_FAIL : 400,    // this event will not passed to app. please don't make comment for this.    
    MGR_EVENT_RECORD_FAIL : 402,  // this event will not passed to app. please don't make comment for this.    
    MGR_EVENT_RECORD_STOP : 403,  // this event will not passed to app. please don't make comment for this.    
    MGR_EVENT_FILTER_VOICE_DETECETD : 404,  // this event will be passed to app. 
    MGR_EVENT_FILTER_PLAY_STARTED : 405,  // this event will be passed to app. 
    MGR_EVENT_FILTER_PLAY_STOPPED : 406,  // this event will be passed to app. 
    MGR_EVENT_FILTER_PLAY_VOLUME  : 450,  // this event will be passed to app. 
    /*
     * Constant: MICROPHONE_FORMAT
     * Definitions for the supported Audio Format for Microphone
     * (Currently, MICROPHONE_FORMAT_SIGNED_16BIT_LITTLE_ENDIAN is just supported.)
     * 
     * MICROPHONE_FORMAT_SIGNED_16BIT_LITTLE_ENDIAN    - Default format of Audio Input Device
     */   
	MICROPHONE_FORMAT_SIGNED_16BIT_LITTLE_ENDIAN : 0,	///< Default format of Audio Input Device

    /*
     * Constant: MICROPHONE_FRAMERATE
     * Definitions for the frame rates of Microphone
     * (Currently, MICROPHONE_FRAMERATE_48000 is just supported.)
     * 
     * MICROPHONE_FRAMERATE_48000             - sample rate is 48k. CD quality. Default format of Audio Input Device
     */  
	MICROPHONE_FRAMERATE_48000 : 48000,		///< sample rate = 48k. CD quality. Default format of Audio Input Device

    /*
     * Constant: MICROPHONE_EFFECT
     * Definitions for Microphone effects
     * (Currently, MICROPHONE_EFFECT_REVERB is just supported. In the future, other effects would be added.)
     * 
     * MICROPHONE_EFFECT_REVERB             - reverb effect
     */  
	MICROPHONE_EFFECT_NONE : 0x00,	///< no effect
	MICROPHONE_EFFECT_REVERB : 0x01,	///< reverb effect
	MICROPHONE_EFFECT_FILTER : 0x10,	///< reverb effect
	
    // private
	OCI_PROFILE_AUDIOINPUT: 0x10,
	
	MicrophoneArray: new Array(),
	MicrophoneValidArray: new Array(),
	MicrophoneCallback: null,
	SEFPlugin: null,
	
	MicrophoneStateStop: 0x00000000,
	MicrophoneStatePlay: 0x00000001,
	MicrophoneStateRecord: 0x00000010,
	MicrophoneStateFilter: 0x00000100,
    
	IsVoiceRecognition: true,
	MicrophonePlayOption: 0x00000001,
	MicrophoneRecordOption: 0x00000010,


	/*
	 * Function: ManagerEvent
	 *  structure that has microhpone connectivity information
	 *
	 * Properties:
	 * eventType - (String) Type information of event such as Connect / Disconnect 
	 * deviceName - (String) Device name.
	 * UID - (DOMString) Deviec UniqueID
	 */
	ManagerEvent: function()
	{
	    var eventType;
	    var name;
		var UID;
	    var value;
	},

	isSupported: function()
	{
        	var smartHubFull = window.location.search.split('modelid=');
        	var smartHubModel = smartHubFull[1].split('&');
		if (smartHubModel[0] == 'SDK') {
			return true;
		}
		else
		{
			var nProduct = window.webapis.tv.info.getProduct();
			if(nProduct==0)
			{
				return true;
			}
			else if(nProduct==2)
			{
				if(window.webapis._plugin("Microphone", "IsSupported", "-1")==1)
				{
					return true;
				}
			}
		}
		return false;
	},
	
	/*
	 * Function: getMicrophones
	 *  request to get microphone devices instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns 
	 */
	getMicrophones: function(successCallback, errorCallback)
	{
		var timeout = 1;
		
		var returnValue = window.webapis.oci.create(this.SEFPlugin, "Microphone");
		
		if(this.SEFPlugin == null)
		{
			timeout = 2500;
			
			if(returnValue != window.webapis.oci.OCI_ERR)
			{
				this.SEFPlugin = returnValue;
				this.SEFPlugin.OnEvent = this.callbackMicrophones;
				this.SEFPlugin.Execute("RegisterCallback", "-1");
//				this.SEFPlugin.Execute("EnableInternalMicrophone", "-1", "InternalMicrophone");
			}
		}
				
	    if (typeof errorCallback == "undefined")
	    {
	        errorCallback = null;
	    }
		
	    if (typeof successCallback != 'function')
	    {
			alert("[microphone.js : getMicrophones] successCallback is not a function type");
			
			if(errorCallback!=null)
			{
			}
	    }
	    else
	    {
            setTimeout(function(){webapis.oci.getConnectedDevices(webapis.microphone.SEFPlugin, webapis.microphone.MicrophoneArray, webapis.microphone.MicrophoneValidArray, successCallback, webapis.microphone.Microphone);}, timeout);
	    }
	},

	/*
	 * Function: registerManagerCallback
	 *  request to set callback function to receive microphone device manager event such as MGR_EVENT_DEV_CONNECT / MGR_EVENT_DEV_DISCONNECT
	 * 
	 * Parameters:
	 *  callback - (function) callback function that has prototype given below in Note section.
	 *
	 * Returns
	 *	None
	 *
	 * Note:
	 *	Function Prototype
	 *------------------------------------------------------------------------------------------------------------------------------
	 *	void callbackFn(ManagerEvent)
	 */
	registerManagerCallback: function(callback)
	{
	    if (typeof callback == 'function' )
	    {
	    	this.MicrophoneCallback = callback;
	    }
	},  

	/*
         * Private
	 */
	enableInternalMicrophones: function(enable)
	{
		var enableFlag = 1;
		if(enable == true)
		{
			enableFlag = 1;
		}
		else
		{
			enableFlag = 0;
		}
		this.SEFPlugin.Execute("EnableInternalMicrophone", "-1", "InternalMicrophone", String(enableFlag));
	},  

	/*
         * Private
	 */
	callbackMicrophones: function(sEventType, sParam1, sParam2)
	{
		var nCount;
		var nDevice;
		var uniqueID;
		
	    var type = Number(sEventType);
		var subType;
		var errType;
		var devEvent;
		var infoMicrophone;


		//if(webapis.microphone.MicrophoneCallback!=null)
		//{
		//	webapis.microphone.MicrophoneCallback(infoMicrophone);
		//}
		//alert("[callbackMicrophones] ***************************");
		//alert("[callbackMicrophones] type=" + type);
		//alert("[callbackMicrophones] ***************************");
		
		switch(type)
		{
			case window.webapis.microphone.MGR_EVENT_PLAY_FAIL:
			{
				for(nCount = 0; nCount < window.webapis.microphone.MicrophoneArray.length ; nCount++)
				{
					nDevice = window.webapis.microphone.MicrophoneArray[nCount];
					var split_var  = sParam1.split(window.webapis.oci.DELIMITER_EVENT_PARAM);					
					if((nDevice.getDeviceID() == Number(split_var[1])) && (window.webapis.microphone.MicrophoneArray[nCount].deviceCallback != null))
					{
						window.webapis.microphone.MicrophoneArray[nCount].deviceCallback(type);
					}
				}
				break;
			}

			case window.webapis.microphone.MGR_EVENT_RECORD_FAIL:
			case window.webapis.microphone.MGR_EVENT_RECORD_STOP:
			case window.webapis.microphone.MGR_EVENT_FILTER_VOICE_DETECETD:
			case window.webapis.microphone.MGR_EVENT_FILTER_PLAY_STARTED:
			case window.webapis.microphone.MGR_EVENT_FILTER_PLAY_STOPPED:
			case window.webapis.microphone.MGR_EVENT_FILTER_PLAY_VOLUME:
			{
				for(nCount = 0; nCount < window.webapis.microphone.MicrophoneArray.length ; nCount++)
				{
					nDevice = window.webapis.microphone.MicrophoneArray[nCount];
					if((nDevice.getDeviceID() == Number(sParam1)) && (webapis.microphone.MicrophoneCallback!=null))
					{
						infoMicrophone = new window.webapis.microphone.ManagerEvent();
						infoMicrophone.eventType = type;
						infoMicrophone.name = nDevice.getName();
						infoMicrophone.UID = nDevice.getUniqueID();
						infoMicrophone.value = Number(sParam2);
						webapis.microphone.MicrophoneCallback(infoMicrophone);
					}
				}
			    break;
			}

			case window.webapis.microphone.MGR_EVENT_DEV_CONNECT:
			{
				alert("[callbackMicrophones] MGR_EVENT_DEV_CONNECT");

				if(webapis.microphone.MicrophoneCallback!=null)
				{
					devEvent = window.webapis.oci.parseDeviceEvent(sParam1);
					infoMicrophone = new window.webapis.microphone.ManagerEvent();
					infoMicrophone.eventType = devEvent.eventType;
					infoMicrophone.name = devEvent.name;
					infoMicrophone.UID = devEvent.UID;

					webapis.microphone.MicrophoneCallback(infoMicrophone);
				}	
				break;
			}

			case window.webapis.microphone.MGR_EVENT_DEV_DISCONNECT:
			{
				alert("[callbackMicrophones] MGR_EVENT_DEV_DISCONNECT");
				devEvent = window.webapis.oci.parseDeviceEvent(sParam1);
							
				if(webapis.microphone.MicrophoneCallback!=null)
				{
					infoMicrophone = new window.webapis.microphone.ManagerEvent();
					infoMicrophone.eventType = devEvent.eventType;
					infoMicrophone.name = devEvent.name;
					infoMicrophone.UID = devEvent.UID;

					webapis.microphone.MicrophoneCallback(infoMicrophone);
				}
	
				for(nCount = 0; nCount < window.webapis.microphone.MicrophoneArray.length; nCount++)
				{
					nDevice = window.webapis.microphone.MicrophoneArray[nCount];
					uniqueID = nDevice.getUniqueID();
					if(uniqueID == devEvent.UID)
					{
						//alert("disconnected UID = " + uniqueID);
						//alert("disconnected name = " + nDevice.getName());
						nDevice.stop(window.webapis.microphone.MicrophoneStatePlay | window.webapis.microphone.MicrophoneStateRecord);
						nDevice.disableDevice();
					}
				}

			    window.webapis.oci.removeSpecificDevice(window.webapis.microphone.SEFPlugin, devEvent.UID, window.webapis.microphone.MicrophoneArray, window.webapis.microphone.MicrophoneValidArray);
				break;
			}
			
			default:
//				alert("[callbackMicrophones] eveType="+type+"_");
				break;
		}
	},
	

	/*
	 * Class : Microphone
	 *
	 * Properties:
	 * devInfo - (OCIDevInfo) device information structure
	 * deviceID - (Number) microphone's ID. It is managed by OCI
	 *
	 * Function: Microphone (constructor)
	 *
	 * Parameters:
	 *  devInfo - (OCIDevInfo) device information structure
	 *  deviceID - (Number) microphone's ID. It is managed by OCI
	 */
	Microphone: function(devInfo, deviceID)
	{
		INTERFACE_COMMAND = "Microphone";
		this.uniqueID = devInfo.UID;
		this.name = devInfo.name;
		this.deviceID = deviceID;
		this.deviceCallback = null;
		this.deviceType = devInfo.deviceType;
//		this.deviceState = webapis.microphone.MicrophoneStateStop;
//		this.devicePrevState = webapis.microphone.MicrophoneStateStop;
		
		/* Function: getUniqueID
		 *  get unique ID of the microphone
		 * 
		 * Returns
		 *  uniqueID - (String) unique ID of microphone.
		 */
		this.getUniqueID = function() 
		{
			return this.uniqueID;
		}

		/*
		 * private
		 * Function: getDeviceID
		 *  get device ID of the microphone
		 * 
		 * Returns
		 * 	deviceID - (String) device ID of microphone.
		 */
		this.getDeviceID = function() 
		{
			return this.deviceID;
		}

		/*
		 * Function: getName
		 *  get name of the microphone device
		 * 
		 * Returns
		 *  name - (String) name of microphone device.
		 */
		this.getName = function() 
		{
			return this.name;
		}
		
		/*
		 * Function: enableDevice
		 *  enable the microphone device
		 *
		 * Parameters:
		 * 	o format	- (Number) audio data format from audio input device. ex) MICROPHONE_FORMAT_SIGNED_16BIT_LITTLE_ENDIAN
		 * 	o framerate - (Number) framerate of retrieved audio data. ex) MICROPHONE_FRAMERATE_48000
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */		
		this.enableDevice = function(format, framerate) 
		{
			var ret = webapis.microphone.SEFPlugin.Execute("EnableAudioIn", String(this.deviceID), String(format), String(framerate));
			if (ret != webapis.oci.OCI_NO_ERR)
			{
				alert("[OCI_microphone_enable]: ERROR = " + ret);
			    return false;
			}
			return true;
		}

		/*
		 * Function: disableDevice
		 *  disable the microphone device
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.disableDevice = function() 
		{
			var ret = webapis.microphone.SEFPlugin.Execute("DisableAudioIn", String(this.deviceID));
			if (ret != webapis.oci.OCI_NO_ERR)
			{
				alert("[OCI_microphone_disable]: ERROR = " + ret);
			    return false;
			}
			return true;
		}

		/*
		 * Private
		 * Function: playrecord
		 *  start to play/record the microphone device. 
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.playrecord = function(nState, strFilename, strLockState) 
		{
			if (typeof nState == "undefined")
			{
				nState = webapis.microphone.MicrophoneStatePlay;
			}
			if (typeof strLockState == "undefined")
			{
				strLockState = "";
			}
			if (typeof strFilename == "undefined")
			{
				strFilename = "";
			}
			var ret = false;
			if(webapis.microphone.SEFPlugin.Execute("PlayAudioIn", String(this.deviceID), strLockState, String(nState), strFilename)== webapis.oci.OCI_NO_ERR)
			{
				ret = true;
			}
/*			
			if(ret==true && webapis.recognition.IsVoiceRecognitionEnabled())
			{
		                webapis.recognition.HideVoiceHelpbar();
				webapis.recognition.StopVoiceRecognition();
				IsVoiceRecognition = true;
	            	}
*/            
			return ret;
		}

		/*
		 * Private
		 * Function: stop
		 *  stop playing the microphone device, it means users can't hear their voice from the TV speaker any more. 
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.stop = function(nState) 
		{
			if (typeof nState == "undefined")
			{
				nState = webapis.microphone.MicrophoneStatePlay;
			}
			var ret = false;
			if(webapis.microphone.SEFPlugin.Execute("StopAudioIn", String(this.deviceID), String(nState)) == webapis.oci.OCI_NO_ERR)
			{
			    ret = true;
			}
/*			
			if(IsVoiceRecognition == true)
			{
				webapis.recognition.StartVoiceRecognition();
				IsVoiceRecognition = false;
			}
*/            
			return ret;
		}

		/*
		 * Function: play
		 *  start to play the microphone device, it means users start to hear their voice from the TV speaker. 
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.play = function(strLockState) 
		{
			if (typeof strLockState == "undefined")
			{
				strLockState = "";
			}
			webapis.microphone.SEFPlugin.Execute("SetMicAppID", "-1", "77", curWidget.id);
			var ret = this.playrecord(webapis.microphone.MicrophoneStatePlay, "", strLockState);
			if(ret == false)
			{
				alert("play ERROR = " + ret);
			}
			return ret;
		}
		
		/*
		 * Function: stopPlay
		 *  stop playing the microphone device, it means users can't hear their voice from the TV speaker any more. 
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.stopPlay = function() 
		{
			var ret = this.stop(webapis.microphone.MicrophoneStatePlay);
			if(ret == false)
			{
				alert("stopPlay ERROR = " + ret);
			}
			return ret;
		}

		/*
		 * Private
		 * Function: record
		 *  start to record the microphone data, it will recorded as a file. 
		 * 
		 * Parameters:
		 * 	o filename - (String) filename which will be stored.
		 *		 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.record = function(filename)
		{
			var ret = false;
			if (typeof filename != "undefined")
			{
//				alert("version = " + Number(webapis.microphone.SEFPlugin.Execute("GetVersion", "", "", "", "")));
				if(Number(webapis.microphone.SEFPlugin.Execute("GetVersion", "", "", "", ""))>= 2.0)
				{
					filename = webapis.oci.setFilePath(filename);
					if(filename != null)
					{
						ret = this.playrecord(webapis.microphone.MicrophoneStateRecord, filename, "");
					}
				}
				if(ret == false)
				{
					alert("record ERROR = " + ret);
				}
			}
			return ret;
			
		}

		/*
		 * Function: stopRecord
		 *  stop record the microphone. 
		 * 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.stopRecord = function()
		{
			var ret = false;
//			alert("version = " + Number(webapis.microphone.SEFPlugin.Execute("GetVersion", "", "", "", "")));
			if(Number(webapis.microphone.SEFPlugin.Execute("GetVersion", "", "", "", ""))>= 2.0)
			{
				ret = this.stop(webapis.microphone.MicrophoneStateRecord);
			}
			if(ret == false)
			{
				alert("stopRecord ERROR = " + ret);
			}
			return ret;
		}


		/*
		 * Function: getVolumeLevel
		 *  get the level of the microphone's volume
		 * 
		 * Returns
		 *  volule level- (Number) volume(from 0 to 100) is returned.
		 */	
		this.getVolumeLevel = function() 
		{
			var ret = webapis.microphone.SEFPlugin.Execute("GetAudioInVolumeLevel", String(this.deviceID));
			return ret;			
		}

		/*
		 * Function: setVolumeLevel
		 *  set the level of the microphone's volume. The range of level is from 0 to 100.
		 * 
		 * Parameters:
		 * 	o volume - (Number) volume level (Minimum : 0 to Miximum : 100).
		 *		 
		 * Returns
		 *  true/false - (bool) return true if call is successful, false otherwise.
		 */	
		this.setVolumeLevel = function(volume) 
		{
			var ret = webapis.microphone.SEFPlugin.Execute("SetAudioInVolumeLevel", String(this.deviceID),String(volume));
			if (ret != webapis.oci.OCI_NO_ERR)
			{
				alert("setVolumeLevel function only work while playing");
			    return false;
			}
			return true;
		}

		/*
		 * Function: getSupportedEffects
		 *  get all effects that can be supported by the microphone
		 * 
		 * Returns
		 *  effects- (Number) all effects that can be supported by the microphone
		 */	
		this.getSupportedEffects = function() 
		{
			var ret = webapis.microphone.SEFPlugin.Execute("GetSupportedAudioInEffects", String(this.deviceID));
//			alert("getSuppoertedEffects = " + ret);
			return ret;
		}

		/*
		 * Function: getEnabledEffects
		 *  get all enabled effects of microphone
		 * 
		 * Returns
		 *  effects- (Number) all enabled effects of microphone
		 */	
		this.getEnabledEffects = function() 
		{
   			var ret = webapis.microphone.SEFPlugin.Execute("GetEnabledAudioInEffects", String(this.deviceID));
			return ret;
		}

		/*
		 * Function: setEffect
		 *  set special effects with on/off options. If 
		 * 
		 * Parameters:
		 * 	o effect	- (Number) audio effect, ex)MICROPHONE_EFFECT_REVERB
		 * 	o enable	- (bool) true if you want to set the effect, false if you want to release the effect.
		 *
		 * Returns
		 *   true/false - (bool) return true if call is successful, false otherwise.
		 */			
		this.setEffect = function(effect, enable, param1, param2, param3, param4) 
		{
//			alert("Mic setEffect("+effect+","+enable+")");
			
			var enableFlag = 0;
			if(enable == true)
			{
				enableFlag = 1;
			}
			else
			{
				enableFlag = 0;
			}
			if (typeof param1 == "undefined")
			{
				param1 = 0;
			}
			if (typeof param2 == "undefined")
			{
				param2 = 0;
			}
			if (typeof param3 == "undefined")
			{
				param3 = 0;
			}
			if (typeof param4 == "undefined")
			{
				param4 = "";
			}
			var ret = webapis.microphone.SEFPlugin.Execute("SetAudioInEffect", String(this.deviceID), String(effect), String(enableFlag), String(param1)+','+String(param2)+','+String(param3)+','+String(param4));
			if (ret == webapis.oci.OCI_NO_ERR)
			{
	    			return true;		
			}
			alert("[setEffect]: ERROR = " + ret);
			return false;
		}
 		
	    /*
	     * Function: registerDeviceCallback
		 *  to register a device instance for receiving device event. If null is passed as callback function type, then callback
		 *  will be unregistered and event will no longer be received by device instance.
	     * 
	     * Parameters:
	     *  callback - (function) callback function that has ???????????? parameter.
	     *
	     * Returns
	     */
		this.registerDeviceCallback = function(callback)
		{
//			alert("[microphone_registerDeviceCallback] ");
			
			if (typeof callback == 'function')
			{
//				alert("[microphone_registerDeviceCallback]_RegisterCallback. ");
				this.deviceCallback = callback;
				if(window.webapis.microphone.SEFPlugin != null)
				{
//					alert("[microphone_registerDeviceCallback]_RegisterCallback_CALL. ");
					
					window.webapis.microphone.SEFPlugin.Execute("RegisterCallback", String(this.deviceID));
				}
			}
			else
			{
//				alert("[microphone_registerDeviceCallback]_UnregisterCallback ");
				this.deviceCallback = null;
			
				if(window.webapis.microphone.SEFPlugin != null)
				{
					window.webapis.microphone.SEFPlugin.Execute("UnregisterCallback", String(this.deviceID));
				}
			}
		}
 		
		/*
		 * private:
		 * Function: getFilterVolume
		 *  get the volume of filter voice.
		 * 
		 * Returns
		 *  volume- (Number) volume of voice
		 */	
		this.getFilterVolume = function() 
		{
   			var ret = webapis.microphone.SEFPlugin.Execute("GetFilterVolume", String(this.deviceID));
			return ret;
		}
	}
}

/*
 * Namespace: download
 * 	download module
 * 
 * Available From:
 * 	0.1
 */

webapis.download = {

	cbDownloadCallback : null,
	filepath_tmp : null,

	downloadCallback: function (type, param1, param2) {
		alert('[Subtitle] _downloadCallback('+type+','+param1+','+param2+')');
				
		var aResult = param1.split('?');
		switch (aResult[0]) {
			case '1000': // download is complete
				if (typeof cbDownloadCallback == 'function'){
					if (aResult[1] == 1) {	// success
						alert('[Subtitle] Download Success!');
						if (typeof cbDownloadCallback == 'function') {
							cbDownloadCallback(filepath_tmp);
						       
						}
					}
					else {	// fail
						alert('[Subtitle] Download Failed!');
						if (typeof cbDownloadCallback == 'function') {
							cbDownloadCallback(false);
						}
					}
				}
				break;
						
			default:
				break;
		}
	},
	
	requestDownload: function(sUrl, filename, onevent)
	{
		var result = 0;

		var downloadPlugin = webapis._plugin("Download");
		filepath_tmp = "$TEMP" + "/" + filename;
		cbDownloadCallback = onevent;

		if (navigator.userAgent.toLowerCase().indexOf('applewebkit') < 0 && navigator.userAgent.toLowerCase().indexOf('maple') >= 0) { // maple
			downloadPlugin.OnEvent = 'webapis.download.downloadCallback';					
		}
		else {
			downloadPlugin.OnEvent = window.webapis.download.downloadCallback;
		}

		result = webapis._plugin(downloadPlugin , "StartDownFile",sUrl, filepath_tmp);
	     
		return result;

	},

	removeFile: function( filename )
	{
		var filepath_tmp = "$TEMP" + "/" + filename;
		var fileSystemObj = new FileSystem();
		var result = fileSystemObj.deleteCommonFile( filepath_tmp );
		return result;
	},

}

/*
 * Namespace: audiorecorder
 * 	audiorecorder module deals with recording the TV-out sound
 * 
 * Available From:
 * 	0.1
 */
webapis.audiorecorder = {
    
	/*
	 * Constant: AUDIORECORDER EVENT
	 * Definitions for AudioRecorder event
	 * 
	 */  
	EVENT_RECORDING_ERROR : 0,
	EVENT_RECORDING_START : 1,
	EVENT_RECORDING_STOP : 2,


	callbackFnc : null,
	SEFPlugin: null,
	isStarted: false,

	/*
	 * Function: FileInfo
	 *  structure that has a pieces of information which are came from file list
	 *
	 * Properties:
	 * fileName - (String) name of the file
	 * nChannel - (int) number of channels
	 * nSamplesPerSec - (int) number of samples per second
	 * nBitsPerSamples - (int) number of bits per samples
	 * nSize - (int) size of the total data
	 */
	FileInfo: function()
	{
		var fileName;
		var nChannel;
		var nSamplesPerSec;
		var nBitsPerSamples;
		var nSize;
	},

	/*
	 * Private
	 * Function: isAudioRecoderSupported
	 *  Determines whether audio recorder is supported.
	 * 
	 * Returns
	 *   true/false - (boolean) return true if there is recording module, otherwise return false.
	 */
	isAudioRecorderSupported: function()
	{
		var smartHubFull = window.location.search.split('modelid=');
		var smartHubModel = smartHubFull[1].split('&');
        
		if(smartHubModel[0] == 'SDK')
		{
		    return false;
		}
		else
		{
//			var returnValue = window.webapis.oci.create(this.SEFPlugin, "AudioRecorder");
			if(this.SEFPlugin == null)
			{
			        this.SEFPlugin = webapis.oci.addPluginObject("SEF", "AudioRecorder");
			        if(this.SEFPlugin!=null)
			        {
					// return value of Open is always 1, so we need to find different method to check EMP fail.
					var ret = this.SEFPlugin.Open("AudioRecorder", "1.00", "None");
					if (ret == -1)
					{
								this.SEFPlugin = null;
					}				
				}
			}
			if(this.SEFPlugin != null)
			{
				alert('Support AudioRecorder');
				return true;
			}
			else
			{
				alert('Not Support AudioRecorder');
				return false;
			}		
		}
	},
    
	/*
	 * Private
	 * Function: record
	 *  start to record the audio out from TV. it will recorded as a file. 
	 * 
	 * Parameters:
	 * 	o filename - (String) filename which will be stored.
	 * 	o callback - (function) callback function.
	 * 	o settings - (String) settings parameter
	 *		 
	 * Returns
	 *  true/false - (bool) return true if call is successful, false otherwise.
	 */	
	record : function(filename, callback, settings)
	{
		var ret = false;
		
		if (typeof filename != "undefined")
		{
			filename = webapis.oci.setFilePath(filename);
			if(filename != null)
			{
				//alert("[AudioRecorder#record]_filename="+filename);
				if(this.isAudioRecorderSupported() == false)
				{
					return false;
				}
				
				if (typeof callback == 'function' )
				{
					webapis.audiorecorder.callbackFnc = callback;
				}
				else
				{
					alert("[AudioRecorder#record]_Error! callback is not a function.");
				}
				
				if (typeof settings == "undefined")
				{
					settings = "";
				}
				
				this.SEFPlugin.OnEvent = webapis.audiorecorder.callbackAudioRecorder;
				
				if(this.SEFPlugin.Execute("Start", filename, settings) == webapis._pluginDef.PLR_FALSE)
				{
					alert("AudioRecorder#record ERROR = " + ret);
					ret = false;
				}
				else
				{
					ret = true;
				}
			}
		}		
		return ret;
		
	},

	/*
	 * Private
	 * Function: stopRecord
	 *  stop record the audio out from TV. 
	 * 
	 * Returns
	 *  true/false - (bool) return true if call is successful, false otherwise.
	 */	
	stopRecord : function()
	{
		var ret = true;
		
		if(this.isAudioRecorderSupported() == false)
		{
			return false;
		}
		
		if(this.SEFPlugin.Execute("Stop") == webapis._pluginDef.PLR_FALSE)
		{
			alert("AudioRecorder#stopRecord ERROR = " + ret);
			ret = false;
		}
		
		//var recorderPlugin = webapis._plugin("AudioRecorder");
		//recorderPlugin.OnEvent = null;
				
		//webapis.audiorecorder.callbackFnc = null;
			
		return true;
	},

	/*
	 * Private
	 * Function: callbackAudioRecorder
	 *  callback function from the empAudioRecorder
	 *  it will deliever the event to application if registered.
	 *
	 * Parameters:
	 * 	o sEventType - (String) event type
	 * 	o sParam1 - (String) parameter 
	 * 	o sParam2 - (String) parameter
	 */	
	callbackAudioRecorder: function(sEventType, sParam1, sParam2)
	{
	    var type = Number(sEventType);


		alert("[callbackAudioRecorder] ***************************");
		alert("[callbackAudioRecorder] type=" + type);
		alert("[callbackAudioRecorder] ***************************");
		
		switch(type)
		{
			case webapis.audiorecorder.EVENT_RECORDING_ERROR:
			{
				alert("[callbackAudioRecorder] EVENT_RECORDING_ERROR__");
				//webapis.audiorecorder.stopRecord();
				if(webapis.audiorecorder.callbackFnc != null)
				{
					webapis.audiorecorder.callbackFnc(type);
				}
				webapis.audiorecorder.isStarted = false;
			    break;
			}

			case webapis.audiorecorder.EVENT_RECORDING_START:
			{
				alert("[callbackAudioRecorder] EVENT_RECORDING_START__");
				if(webapis.audiorecorder.callbackFnc != null)
				{
					webapis.audiorecorder.callbackFnc(type);
				}
				webapis.audiorecorder.isStarted = true;
				//TO DO
				break;
			}

			case webapis.audiorecorder.EVENT_RECORDING_STOP:
			{
				alert("[callbackAudioRecorder] EVENT_RECORDING_STOP__");
				if(webapis.audiorecorder.callbackFnc != null)
				{
					webapis.audiorecorder.callbackFnc(type);
				}
				webapis.audiorecorder.isStarted = false;
				//var recorderPlugin = webapis._plugin("AudioRecorder");
				//recorderPlugin.OnEvent = null;
				
				//webapis.audiorecorder.callbackFnc = null;
				break;
			}
			default:
				alert("[callbackAudioRecorder] eveType="+type+"_");
				break;
		}
	},

	/*
	 * Private
	 * Function: getRecordedSize
	 *  get the information about recorded size 
	 * 
	 * Returns
	 *  size - (unsigned int) return size of recorded file.
	 */	
	getRecordedSize: function()
	{
		var ret = -1;
		if((this.isAudioRecorderSupported() == true) && webapis.audiorecorder.isStarted == true)
		{
			ret = this.SEFPlugin.Execute("GetRecordedSize");			
		}
		return ret;
	},

	/*
	 * Private
	 * Function: getAvailableDiskSpace
	 *  get the information about available space from disk 
	 * 
	 * Parameters:
	 * 	o strDirectory - (String) directory path
	 * 
	 * Returns
	 *  size - (unsigned int) return size of available space.
	 */	
	getAvailableDiskSpace : function(strDirectory)
	{
		var ret = -1;
		if (typeof strDirectory == "undefined")
		{
			strDirectory = "file://";
		}
		strDirectory = webapis.oci.setFilePath(strDirectory);
		if(strDirectory != null)
		{
			if(this.isAudioRecorderSupported() == true)
			{
				ret = this.SEFPlugin.Execute("GetAvailableDiskSpace", strDirectory);			
			}
		}	
		return ret;
	},

	/*
	 * Private
	 * Function: deleteFile
	 *  delete recorded file from disk 
	 *  it will delete recorded audio file only.
	 * 
	 * Parameters:
	 * 	o strFilename - (String) file name
	 * 
	 * Returns
	 *  void
	 */	
	deleteFile : function(strFilename)
	{
		if (typeof strFilename != "undefined")
		{
			strFilename = webapis.oci.setFilePath(strFilename);
			if(strFilename != null)
			{
				if(this.isAudioRecorderSupported() == true)
				{
					this.SEFPlugin.Execute("Delete", strFilename);			
				}
			}
		}
			
		return;
	},

	/*
	 * Private
	 * Function: list
	 *  get file list from directory
	 * 
	 * Parameters:
	 * 	o strDirectory - (String) directory path
	 * 
	 * Returns
	 *  list - (array) list of file.
	 */	
	list : function(strDirectory)
	{
		if (typeof strDirectory == "undefined")
		{
			strDirectory = "file://";
		}
		strDirectory = webapis.oci.setFilePath(strDirectory);
		if(strDirectory != null)
		{
		    var arrayList = new Array();		
			if(this.isAudioRecorderSupported() == true)
			{
				var value = this.SEFPlugin.Execute("List", strDirectory);
			    var split_var  = value.split(webapis.oci.DELIMITER_EVENT_PARAM);
			    var nCount = Number(split_var[0]);
			    for(iCount = 0;iCount<nCount;iCount++)
			    {
			        var fileInfo = new webapis.audiorecorder.FileInfo();
			        fileInfo.fileName = split_var[1+iCount*5];
			        fileInfo.nChannel = Number(split_var[1+iCount*5+1]);
			        fileInfo.nSamplesPerSec = Number(split_var[1+iCount*5+2]);
			        fileInfo.nBitsPerSamples = Number(split_var[1+iCount*5+3]);
			        fileInfo.nSize = Number(split_var[1+iCount*5+4]);
			        arrayList.push(fileInfo);
				}
			}
		}
		return arrayList;
	},

	/*
	 * Function: USBInfo
	 *  structure that has information about usb memory
	 *
	 * Properties:
	 * path - (String) path of the usb memory
	 * name - (String) name of the usb memory
	 */
	USBInfo: function()
	{
		var path;
		var name;
	},

	/*
	 * Private
	 * Function: getUSBList
	 *  get the information about usb memory
	 *
	 * Returns
	 *  list - (array) list of USB memory device.
	 */
	getUSBList: function()
	{
		var arrayList = new Array();
		var nUSBSize = webapis._plugin("Storage", "GetUSBListSize");
		for(iCount=0;iCount<nUSBSize;iCount++)
		{
			var usbInfo = new webapis.oci.USBInfo();
			var nUSBID = webapis._plugin("Storage", "GetUSBDeviceID", iCount);
			usbInfo.name = webapis._plugin("Storage", "GetUSBModelName", nUSBID);
			var nUSBPartition = webapis._plugin("Storage", "GetUSBDevicePartitionNum", nUSBID);
			usbInfo.path = webapis._plugin("Storage", "GetUSBMountPath", nUSBID, nUSBPartition);
			arrayList.push(usbInfo);
		}
		return arrayList;
	},
	
	/*
	 * Private
	 * Function: getPlayer
	 *  get the audio tag element for specific wav file.
	 * 
	 * Parameters:
	 * 	o filePath - (String) file path
	 *
	 * Returns
	 *  element - (object) audio tag element
	 */
	getPlayer: function(filepath)
	{
		var aelement = document.createElement("audio");
//		aelement.type = "audio/wav";
		aelement.src = window.webapis.oci.setFilePath(filepath);
		return aelement;
	},
	
	/*
	 * Private
	 * Function: upload
	 *  upload recorded file to server
	 * 
	 * Parameters:
	 * 	o filePath - (String) file path
	 * 	o onevent - (Object) event function callback
	 * 	o sUrl - (String) url
	 * 	o ratio - (String) event ratio
	 * 	o serverType - (String) server type
	 * 	o sHeader - (String) name for version 2, header for version 1
	 * 	o sBody - (String) body
	 * 	o sPort - (String) port number
	 * 	o version - (String) version information - default value is 2
	 *
	 * Returns
	 *  element - (object) audio tag element
	 */
	upload: function(filepath, onevent, sUrl, ratio, serverType, sHeader, sBody, sPort, version)
	{
		if (typeof version == "undefined")
		{
			version = '2';
		}
		var result = 0;
		filepath = window.webapis.oci.setFilePath(filepath);
		if(filepath!=null)
		{
			var downloadPlugin = webapis._plugin("Download");
			downloadPlugin.OnEvent = onevent;
			switch(version)
			{
				case '1':
					{
						result = webapis._plugin("Download", "StartUpload", sUrl, sPort, sHeader, sBody, filepath, ratio, serverType);
					}
					break;
				case '2':
					{
						result = webapis._plugin("Download", "StartUpload2", sUrl, filepath, ratio, serverType, sHeader, sBody);
					}
					break;
				default :
					break;
			}	
		}
		return result;		
	}	
}

/*
 * Namespace: printer
 *  printer module deals with printer devices that can be connected to TV.
 * 
 * Available From:
 *  
 */
webapis.printer = {
    
    SCREEN_LAYER_ALL : 0,
    SCREEN_LAYER_OSD : 1,
    
    SEFPlugin: null,
    
    /*
     * Function: isPrintingServiceSupported
     *  Determines whether printing is supported.
     * 
     * Returns
     *   true/false - (boolean) return true if there is printing module, otherwise return false.
    */
    isPrintingServiceSupported: function()
    {
        var smartHubFull = window.location.search.split('modelid=');
        var smartHubModel = smartHubFull[1].split('&');
        
        if(smartHubModel[0] == 'SDK')
        {
            return true;
        }
        else
        {
            if(webapis._plugin('Device', 'SupportPrinter') == webapis._pluginDef.PLR_TRUE)
            {
                alert('SupportPrinter');
                return true;
            }
            else
            {
                alert('Not SupportPrinter');
                return false;
            }
        }
    },
            
    /*
     * Function: runFilePrinting
     *  Activates file printing.
     *  After activating the file printing, user can select printing option and print the file.
     *  Printing module supports printing of sequential encoded image file. (jepg, bmp, gif, png) 
     * 
     * Parameters:
     *  o filePath - (DOMString) file path for printing (local file, URL)
     *    ex1. runFilePrinting('file://image/testImage.jpeg')
     *    ex2. runFilePrinting('http://test.co.kr/testImage.jpeg')
     */
    runFilePrinting: function(filePath)
    {
        var smartHubFull = window.location.search.split('modelid=');
        var smartHubModel = smartHubFull[1].split('&');

        var pFilePath = webapis.oci.setFilePath(filePath);
        
	if(pFilePath == null)
	{
		alert('runFilePrinting(\'file://filename\')');
	}
        
        if(smartHubModel[0] == 'SDK')
        {
       	    var returnValue = webapis.oci.create(this.SEFPlugin, "Printer");   
  
            if(this.SEFPlugin == null)
            {
                if(returnValue != webapis.oci.OCI_ERR)
                {
                    this.SEFPlugin = returnValue;
                }
            }
                        
						window.webapis.printer.SEFPlugin.Execute("RunFilePrinting", 0, pFilePath);

        }
        else //TV
        {            
            webapis._plugin('NNavi', 'ActivateWithData', '41', pFilePath, '', '', '');
        }
    },
    
    /*
     * Function: runScreenPrinting
     *  Activates screen printing.
     *  After activating screen printing module, user can select printing option and print the screen. 
     * 
     * Parameters:
     *  o screen - (unsigned short) SCREEN_LAYER_ALL or SCREEN_LAYER_OSD
     *  o rect     - (SRect) screen area
     */
    runScreenPrinting: function(screen, rect)
    {
        var smartHubFull = window.location.search.split('modelid=');
        var smartHubModel = smartHubFull[1].split('&');
        
        if(rect == null)
        {
		rect = new SRect(0,0,curWidget.width,curWidget.height);
	}
        
        if(smartHubModel[0] == 'SDK')
        {
       	    var returnValue = webapis.oci.create(this.SEFPlugin, "Printer");   
  
            if(this.SEFPlugin == null)
            {
                if(returnValue != webapis.oci.OCI_ERR)
                {
                    this.SEFPlugin = returnValue;
                }
            }
            
            window.webapis.printer.SEFPlugin.Execute("RunScreenPrinting", 0, "./capturePrinter.jpg");
 
            //alert('_ captured screen: '+rect.left+','+rect.top+','+rect.width+','+rect.height);
            //alert('_ captured screen is printed!');
        }
        else
        {
            var nLeft  = rect.left;
            var nTop   = rect.top;
            var nRight = rect.left + rect.width;
            var nDown  = rect.top + rect.height;
            var sScreen = null;
            
            if(screen == window.webapis.printer.SCREEN_LAYER_ALL)
            {
                sScreen = 'ALL';
            }
            else if(screen == window.webapis.printer.SCREEN_LAYER_OSD)
            {
                sScreen = 'OSD';
            }
            else
            {
                alert('Error!, Undefined screen type.');
                return;
            }
            
            webapis._plugin('NNavi', 'ActivateWithData', 41, 'screen://' + sScreen + ',' + nLeft + ',' + nTop + ',' + nRight + ',' + nDown + ',' + curWidget.width + ',' + curWidget.height, '', '', '');
        }        
    },
}

/*
 * Namespace: networkcamera
 * 	networkcamera module deals with networkcamera devices that can be connected to TV. It can check user's networkcamera input event.
 * 
 * Available From:
 * 	0.1
 */
webapis.networkcamera = {
    /*
     * Constant: NETWORKCAMERA_MANAGER_EVENT
     * MGR_EVENT_DEV_CONNECT    - connection event
     * MGR_EVENT_DEV_DISCONNECT - disconnection event
     * MGR_EVENT_NWCAMERA_DETECT_MOTION
     * MGR_EVENT_NWCAMERA_DETECT_AUDIO
     */
    MGR_EVENT_DEV_CONNECT : webapis.oci.EVENT_DEV_CONNECT,
    MGR_EVENT_DEV_DISCONNECT : webapis.oci.EVENT_DEV_DISCONNECT,
    MGR_EVENT_NWCAMERA_DETECT_MOTION : 1101,
    MGR_EVENT_NWCAMERA_DETECT_AUDIO  : 1102,

    /*
     * Constant: NETWORKCAMERA_FLIP_VALUES
     * FLIP_DISABLE
     * FLIP_MIRROR
     * FLIP_VERT_FLIP
     * FLIP_ROT_180
     */
    FLIP_DISABLE : 0,
    FLIP_MIRROR  : 1,
    FLIP_VERT_FLIP  : 2,
	FLIP_ROT_180 : 3,

    /*
     * Constant: NETWORKCAMERA_FLICKERLESS_VALUES
     * FLICKERLESS_AUTO
     * FLICKERLESS_50HZ
     * FLICKERLESS_60HZ
     */
	FLICKERLESS_AUTO : 0,
	FLICKERLESS_50HZ : 1,
	FLICKERLESS_60HZ : 2,
	    
	NetworkCameraArray: new Array(),
	
	NetworkCameraValidArray: new Array(),
	
	ManagerCallback: null,
	
	SEFPlugin: null,
	
	/*
	 * Function: ManagerEvent
	 *  structure that has game pad management related event
	 *
	 * Properties:
	 * eventType - (Number) Type information of event such as Connect / Disconnect
	 * name - (Number) Gamepad's Name
	 * UID - (String) Gamepad's Unique ID
	 * registeredName - (String) Registered device name.
	 * liveStreamURLFormat - (String) Live Stream URL Format.
	 *                       "ex) rtsp://admin:[private_key]@192.168.0.11/profile5/media.smp"
	 *                       you have to change [private_key] to real key submitted by user.
	 */
	ManagerEvent: function()
	{
		var eventType;
		var name;
		var UID;

        var registeredName;
		var liveStreamURLFormat;        
	},

	/*
	 * Function: NetworkCameraInfoEx
	 *  structure that has network camera information
	 *
	 * Properties:
	 * UID - (String) unique ID of the device
	 * name - (String) name information of the device
	 * deviceType - (Number) device type information.
	 * bRegistered - (Bool) Is registered or not. 
	 * bDefault - (Bool) Is default device or not.
	 * ipaddr - (String) IP Address.
	 * model - (String) Model name.
	 * vendor - (String) Vendor name.
	 * registeredName - (String) Registered device name.
	 */
	NetworkCameraInfoEx: function()
	{
	    var UID;
	    var name;
	    var deviceType;

        var bRegistered;
        var bDefault;
        var ipaddr;
        var model;
        var vendor;
        var registeredName;
	},

	/*
	 * Function: getNetworkCameras
	 *  request to get networkcamera instances.
	 * 
	 * Parameters:
	 *  successCallback - (function) 
	 *  errorCallback - (function) 
	 *
	 * Returns
	 */
	getNetworkCameras: function(successCallback, errorCallback)
	{
		var timeout = 1;
		
		var returnValue = webapis.oci.create(this.SEFPlugin, "NetworkCamera");
		
		if (this.SEFPlugin == null)
		{
			timeout = 100;
			
			if (returnValue != webapis.oci.OCI_ERR)
			{
        		this.SEFPlugin = returnValue;
        		
    			this.SEFPlugin.OnEvent = this.callbackNetworkCameras;
    			
    			this.SEFPlugin.Execute("RegisterCallback", "-1");
			}
		}
				
	    if (typeof errorCallback == "undefined")
	    {
	        errorCallback = null;
	    }
		
	    if (typeof successCallback != 'function')
	    {
			alert("[getGamepads] successCallback is not a function type");
			
			if (errorCallback != null)
			{
			}
	    }
	    else
	    {
    		setTimeout(function(){webapis.networkcamera.getRegisteredDevices(webapis.networkcamera.SEFPlugin, webapis.networkcamera.NetworkCameraArray, webapis.networkcamera.NetworkCameraValidArray, successCallback, webapis.networkcamera.NetworkCamera);}, timeout);
	    }
	},

	/*
     * Private
	 * Function: getRegisteredDevices
	 *  This is a helper function to get connected devices
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  arrayDevice - (array) array of existing deivces
	 *  arrayValid - (array) array of valid information
	 *  classDevice - (class) class of each device
	 *
	 * Returns
	 */	
	getRegisteredDevices: function(gSEF, arrayDevice, arrayValid, successCallback, classDevice)
	{
		for (var iCount = 0; iCount < arrayDevice.length; iCount++)
		{
			arrayValid[iCount] = false;
		}
		
		var iNum = 0;
        
		while (1)
		{
			var saDevInfoEx = null;
            
			saDevInfoEx = this.getRegisteredDeviceInfoEx(gSEF, iNum++);
            
			if (saDevInfoEx == null)
			{
				break;
			}
            
			if (webapis.oci.isAvailable(saDevInfoEx.UID, arrayDevice, arrayValid) == null)
			{
				var iDeviceID = webapis.oci.createDevice(gSEF, saDevInfoEx.UID);
                
				if (iDeviceID != webapis.oci.OCI_ERR)
				{
					var newDevice = new classDevice(saDevInfoEx, iDeviceID);
                    
					arrayDevice.push(newDevice);
				}
			}
		}
		
		webapis.oci.removeDevices(gSEF, arrayDevice, arrayValid);
        
		successCallback(arrayDevice);
	},

	/*
     * Private
	 * Function: getRegisteredDeviceInfoEx
	 *  This function gets the device information UID, name.
	 * 
	 * Parameters:
	 *  gSEF - (documentElement) document element of SEF Plugin
	 *  nOrder - (Number) The order of device in total count, 1 to total count
	 *
	 * Returns
	 *  NetworkCameraInfoEx - (NetworkCameraInfoEx) If it succeeds, it returns the structure of device information. Otherwise, it returns null.
	 */
	getRegisteredDeviceInfoEx: function(gSEF, nOrder)
	{
	    if ((gSEF == null) || (nOrder < 0))
	    {
	        return null;
	    }
        
	    var nsRet = gSEF.Execute("GetRegisteredDeviceInfoEx", "-1", String(nOrder));
        
	    if (nsRet == webapis.oci.OCI_ERR)
	    {
	        return null;
	    }
	    else if ((nsRet == 0) || (nsRet == 1))
	    {
	        return webapis.oci.OCI_ERR;
	    }
	    
	    var saDevInfoEx = nsRet.split(webapis.oci.DELIMITER_FUNC_RESULT);
        
		var devInfoEx = new this.NetworkCameraInfoEx();
        
	    devInfoEx.UID = saDevInfoEx[0];
	    devInfoEx.name = saDevInfoEx[1];
	    devInfoEx.deviceType = Number(saDevInfoEx[2]);

        devInfoEx.bRegistered = true;
        
        if (Number(saDevInfoEx[4]) == 0)
        {
            devInfoEx.bDefault = false;
        }
        else
        {
            devInfoEx.bDefault = true;        
        }
        
        devInfoEx.ipaddr = saDevInfoEx[5];
        devInfoEx.model = saDevInfoEx[6];
        devInfoEx.vendor = saDevInfoEx[7];
        devInfoEx.registeredName = saDevInfoEx[8];

	    return devInfoEx;
	},

	/*
	 * Function: registerManagerCallback
	 *  request to set callback function to receive events related with network camera. connect/disconnect/detection events.
	 * 
	 * Parameters:
	 *  callback - (function) callback function that has event parameter.
	 *
	 * Returns
	 */
	registerManagerCallback: function(callback)
	{
	    alert("[NetworkCamera:registerManagerCallback]");
	    
	    if (typeof callback == 'function')
	    {
	    	this.ManagerCallback = callback;
	    }
	},

	callbackNetworkCameras: function(eventType, sParam1, sParam2)
	{
	    var type = Number(eventType);
	    
		switch(type)
		{
			case webapis.networkcamera.MGR_EVENT_DEV_CONNECT:
			    /* PASS THROUGH */
			case webapis.networkcamera.MGR_EVENT_DEV_DISCONNECT:
			{
	        	var event = webapis.oci.parseDeviceEvent(sParam1);
	        	
	        	var evInfo = new webapis.networkcamera.ManagerEvent();
	        	
	        	evInfo.eventType = event.eventType;
	        	evInfo.name = event.name;
	        	evInfo.UID = event.UID;
                evInfo.registeredName = null;
                evInfo.liveStreamURLFormat = null;
	        	
	        	if (webapis.networkcamera.ManagerCallback != null)
	        	{
	        	    webapis.networkcamera.ManagerCallback(evInfo);
	        	}				
	        	
	        	if (type == webapis.networkcamera.MGR_EVENT_DEV_DISCONNECT)
	        	{
	        	    webapis.oci.removeSpecificDevice(webapis.networkcamera.SEFPlugin, event.UID, webapis.networkcamera.NetworkCameraArray, webapis.networkcamera.NetworkCameraValidArray);
	        	}
	        	
	        	return;
        	}

            case webapis.networkcamera.MGR_EVENT_NWCAMERA_DETECT_MOTION:
                /* PASS THROUGH */
            case webapis.networkcamera.MGR_EVENT_NWCAMERA_DETECT_AUDIO:
            {
        		var event = sParam1.split(webapis.oci.DELIMITER_EVENT_PARAM);

                var evInfo = new webapis.networkcamera.ManagerEvent();

                evInfo.eventType = type;
    	        evInfo.UID = event[0];
    	        evInfo.name = event[1];
    	        evInfo.registeredName = event[3];
                evInfo.liveStreamURLFormat = event[4];

	        	if (webapis.networkcamera.ManagerCallback != null)
	        	{
	        	    webapis.networkcamera.ManagerCallback(evInfo);
	        	}				
                
	        	return;                
            }
            
        	default:
        	{
        		return;
		    }
		}
	},

    /*
     * Function: runNetworkCameraSettings
     *  Activates NetworkCamera Settings Menu.
     */
    runNetworkCameraSettings: function()
    {
        webapis._plugin('NNavi', 'ActivateWithData', '47', '', '', '', '');
    },

    /*
     * Function: setDefaultDevice
     *  Set as default device.
     */
    setDefaultDevice: function(UniqueID)
    {
    	var nRet = this.SEFPlugin.Execute("SetDefaultDevice", "-1", UniqueID);
    	
        return nRet;
    },

    /*
     * Function: setNotificationEvent
     *  Set to be enable or disable notification event.
     *
     * Parameters:
     *  bEnableEvent - (Boolean) true/false
     *
     * Returns
     *  OCI_NO_ERR or Error Code - (Number)     
     */
    setNotificationEvent: function(bEnableEvent)
    {
    	var nParam = 0;
    	
    	if (bEnableEvent == true)
    	{
    		nParam = 1;
    	}
    	else
    	{
    		nParam = 0;
    	}
    	
	    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetNotificationEvent", "-1", String(nParam));

        return nRet;
    },


	/*
	 * Class: NetworkCamera
	 * 
	 * Properties:
	 *	devInfoEx - (NetworkCameraInfoEx) network camera information
	 *	deviceID - (Number) network camera's ID. It is managed internally
	 * 
	 * Private
	 * Function: NetworkCamera (constructor)
	 * constructor of NetworkCamera Class
	 * 
	 * Parameters:
	 * 	o devInfoEx	- (NetworkCameraInfoEx) device information structure
	 * 	o deviceID	- (Number) networkcamera's ID. It is managed internally
	 */
    NetworkCamera: function(devInfoEx, deviceID)
	{
		this.deviceID = deviceID;
        
		this.uniqueID = devInfoEx.UID;
		this.name = devInfoEx.name;

		/*
		 * Function: DeviceExtraInfo
		 *  structure of the extra information of network camera
		 * 
		 * Properties:
		 * bDefault - (Boolean)
		 * ipaddr - (String)
		 * model - (String)
		 * vendor - (String)
		 * registeredName - (String)
		 */
		this.DeviceExtraInfo = function()
		{
			var bDefault;		//bool
			var ipaddr;			//string
			var model;          //string
			var vendor;         //string
			var registeredName; //string
		}
        this.extraInfo = new this.DeviceExtraInfo();

        this.extraInfo.bDefault = devInfoEx.bDefault;
        this.extraInfo.ipaddr = devInfoEx.ipaddr;
        this.extraInfo.model = devInfoEx.model;
        this.extraInfo.vendor = devInfoEx.vendor;
        this.extraInfo.registeredName = devInfoEx.registeredName;

		/*
		 * Function: DetectionOptions
		 *  structure of the Detection Options
		 * 
		 * Properties:
		 * motionSensitivity - (Number)
		 * audioSensitivity - (Number)
		 */
		this.DetectionOptions = function()
		{
			var motionSensitivity;	//0~5, 0 : OFF
			var audioSensitivity;   //0~5, 0 : OFF
		}
        this.detectionOpts = new this.DetectionOptions();

		/*
		 * Function: VideoOptions
		 *  structure of the Video Options
		 * 
		 * Properties:
		 * flip - (Number)
		 * brightness - (Number)
		 * flickerless - (Number)
		 * bDayNight - (Boolean)
		 */
		this.VideoOptions = function()
		{
            var flip;           //Diable, Horizontally mirrors, Vertically flip, Rotate 180
            var brightness;     //1~10
            var flickerless;    //Auto, 50Hz, 60Hz
            var bDayNight;      //true/false = ON/OFF
		}
        this.videoOpts = new this.VideoOptions();

		/*
		 * Function: AudioOptions
		 *  structure of the Audio Options
		 * 
		 * Properties:
		 * micVolume - (Number)
		 * speakerVolume - (Number) NOT USED
		 */
		this.AudioOptions = function()
		{
            var micVolume;         //1-3
            var speakerVolume;     //NOT USED
		}
        this.audioOpts = new this.AudioOptions();

        this.liveSteamURL = null;

		this.deviceCallback = null;
		
		/* 
		 * Function: getUniqueID
		 *  get unique ID of the network camera
		 * 
		 * Returns
		 *  uniqueID - (String) unique ID of network camera.
		 */
		this.getUniqueID = function() 
		{
			return this.uniqueID;
		}

		/*
		 * Private
		 * Function: getDeviceID
		 *  get device ID of the network camera
		 * 
		 * Returns
		 *  deviceID - (String) device ID of network camera.
		 */
		this.getDeviceID = function() 
		{
			return this.deviceID;
		}

		/* 
		 * Function: getName
		 *  get name of the network camera
		 * 
		 * Returns
		 *  name - (String) name of network camera.
		 */
		this.getName = function() 
		{
			return this.name;
		}


		/*
		 * Function: getDeviceExInfo
		 *  get device ID of the networkcamera
		 * 
		 * Returns
		 *  extraInfo - (DeviceExtraInfo) extra information of network camera.
		 */
        this.getDeviceExInfo = function()
        {
            return this.extraInfo;
        }

		/* 
		 * Function: isConnected
		 *  check a connection state of network camera
		 * 
		 * Returns
		 *  true/false - (bool)
		 */
        this.isConnected = function()
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("IsConnected", String(this.deviceID));
		    
		    if (nRet == 0)
		    {
		        return false;
		    }
            else
            {
                return true;
            }
        }

		/* 
		 * Function: isDefault
		 *  check whether the network camera is a default device or not
		 * 
		 * Returns
		 *  true/false - (bool)
		 */
        this.isDefault = function()
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("IsDefault", String(this.deviceID));
		    
		    if (nRet == 0)
		    {
		        return false;
		    }
            else
            {
                return true;
            }
        }

		/* 
		 * Function: isDefault
		 *  check whether the network camera is a default device or not
		 * 
		 * Returns
		 *  true/false - (bool)
		 */
        this.submitPrivateKey = function(key)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SubmitPrivateKey", String(this.deviceID), String(key));
		    
		    if (nRet == 0)
		    {
		        return false;
		    }
            else
            {
                return true;
            }
        }
        
		/* 
		 * Function: setMotionDetection
		 *  set the options for detecting motion
		 *
		 * Parameters:
		 *  sensitivity - (Number) detection sensitivity 0 ~ 5, is OFF.
		 * 
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */
        this.setMotionDetection = function(sensitivity)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetMotionDetection", String(this.deviceID), String(sensitivity));

            return nRet;
        }

		/* 
		 * Function: setAudioDetection
		 *  set the options for detecting audio
		 *
		 * Parameters:
		 *  sensitivity - (Number) detection sensitivity 0 ~ 5, is OFF.		 
		 * 
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */
        this.setAudioDetection = function(sensitivity)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetAudioDetection", String(this.deviceID), String(sensitivity));

            return nRet;
        }

		/* 
		 * Function: getDetectionOptions
		 *  get the options for detection from network camera.
		 * 
		 * Returns
		 *  detectionOpts or null - (DetectionOptions)
		 */
        this.getDetectionOptions = function()
        {
		    var nsRetOptions = webapis.networkcamera.SEFPlugin.Execute("GetDetectionOptions", String(this.deviceID));

		    if (nsRetOptions == webapis.oci.OCI_ERR)
		    {
		        return null;
		    }
		    
		    var temp = nsRetOptions.split(webapis.oci.DELIMITER_FUNC_RESULT);
            
			this.detectionOpts.motionSensitivity = Number(temp[0]);
			this.detectionOpts.audioSensitivity = Number(temp[1]);

			return this.detectionOpts;
        }

		/* 
		 * Function: setVideoFlip
		 *  set the options for video flip
		 *
		 * Parameters:
		 *  flip - (Number) flip value
		 * 
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */
        this.setVideoFlip = function(flip)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetVideoFlip", String(this.deviceID), String(flip));

            return nRet;
        }

		/* 
		 * Function: setVideoBrightness
		 *  set the options for video brightness
		 *
		 * Parameters:
		 *  brightness - (Number) brightness value, 1 ~ 10
		 *
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */
        this.setVideoBrightness = function(brightness)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetVideoBrightness", String(this.deviceID), String(brightness));

            return nRet;
        }

		/* 
		 * Function: setVideoFlickerless
		 *  set the options for video flickerless
		 *
		 * Parameters:
		 *  flickerless - (Number) flickerless value
		 *
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */
        this.setVideoFlickerless = function(flickerless)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetVideoFlickerless", String(this.deviceID), String(flickerless));

            return nRet;
        }

		/* 
		 * Function: setVideoDayNight
		 *  set the options for video DayNight
		 *
		 * Parameters:
		 *  bDayNight - (Boolean) true/false
		 *
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */        
        this.setVideoDayNight = function(bDayNight)
        {
        	var nParam = 0;
        	
        	if (bDayNight == true)
        	{
        		nParam = 1;
        	}
        	else
        	{
        		nParam = 0;
        	}
        	
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetVideoDayNight", String(this.deviceID), String(nParam));

            return nRet;
        }

		/* 
		 * Function: getVideoOptions
		 *  get the options for video from network camera.
		 * 
		 * Returns
		 *  videoOpts or null - (VideoOptions)
		 */
        this.getVideoOptions = function()
        {
		    var nsRetOptions = webapis.networkcamera.SEFPlugin.Execute("GetVideoOptions", String(this.deviceID));

		    if (nsRetOptions == webapis.oci.OCI_ERR)
		    {
		        return null;
		    }

		    var temp = nsRetOptions.split(webapis.oci.DELIMITER_FUNC_RESULT);
            
			this.videoOpts.flip = Number(temp[0]);
			this.videoOpts.brightness = Number(temp[1]);
            this.videoOpts.flickerless = Number(temp[2]);

            if (Number(temp[3]) == 0)
            {
                this.videoOpts.bDayNight = false;
            }
            else
            {
                this.videoOpts.bDayNight = true;
            }

			return this.videoOpts;
        }

		/* 
		 * Function: setMicVolume
		 *  set the options for mic volume
		 *
		 * Parameters:
		 *  volume - (Number) mic volume, 1 ~ 3
		 *
		 * Returns
		 *  OCI_NO_ERR or Error Code - (Number)
		 */
        this.setMicVolume = function(volume)
        {
		    var nRet = webapis.networkcamera.SEFPlugin.Execute("SetMicVolume", String(this.deviceID), String(volume));

            return nRet;
        }

		/* 
		 * Function: getAudioOptions
		 *  get the options for audio from network camera.
		 * 
		 * Returns
		 *  audioOpts or null - (AudioOptions)
		 */
        this.getAudioOptions = function()
        {
		    var nsRetOptions = webapis.networkcamera.SEFPlugin.Execute("GetAudioOptions", String(this.deviceID));

		    if (nsRetOptions == webapis.oci.OCI_ERR)
		    {
		        return null;
		    }

		    var temp = nsRetOptions.split(webapis.oci.DELIMITER_FUNC_RESULT);

			this.audioOpts.micVolume = Number(temp[0]);
			this.audioOpts.speakerVolume = 0;

			return this.audioOpts;
        }

		/* 
		 * Function: getLiveStreamURL
		 *  get the URL for receiving live stream.
		 * 
		 * Returns
		 *  liveSteamURL or null - (String)
		 */
        this.getLiveStreamURL = function(key)
        {
		    var nsRetURL = webapis.networkcamera.SEFPlugin.Execute("GetLiveStreamURL", String(this.deviceID), String(key));

		    if (nsRetURL == webapis.oci.OCI_ERR)
		    {
		        return null;
		    }

            this.liveSteamURL = nsRetURL;

			return this.liveSteamURL;
        }
 	},
}

/*
 * Private
 * Namespace: recognition
 */

webapis.recognition = {

	PL_RECOGNITION_TYPE_VOICE: 0,
	PL_RECOGNITION_TYPE_GESTURE: 1,
	PL_RECOGNITION_TYPE_FACE: 2,
	
	MESSAGE_RECOGNITION_VOICE_EMP : 6510,
	MESSAGE_RECOGNITION_GESTURE_EMP : 6511,
	MESSAGE_RECOGNITION_FACE_EMP : 6512,
		
	oCallback : [],	
	oCallback_name : [],
	blinitialized : false,	
	plRecog : null,
	is3rd : false,
	isSubscribeEvent : false,
	
	
	setRetValue : function(ret){
		alert('[webapis.js] recognition.setRetValue() called : ' +ret);
		if(ret == 1)
			return true;
		else
			return false;
	},
	
	isSEFSupported : function(){
		alert('[webapis.js] recognition.isSEFSupported() called');
		
		firmwareVer = webapis._plugin('NNavi', 'GetFirmware');
		splitString = firmwareVer.split('-');
		var tmp = splitString[1];		
		alert('[webapis.js] recognition.isSEFSupported() platform : ' + tmp.substr(8,11));
		var year = parseInt(tmp.substr(8,11),10);
		if(year >= 2012 && webapis._plugin("RECOG") != null)
			return true;
		else {
			alert('[webapis.js] recognition not supported.');
			return false;
		}
	},	
	
	
	isSDK : function(){
		alert('[webapis.js] recognition.isSDK() called');
		var uri = decodeURI(window.location.search);
		splitString = uri.split('&');
		var tmp = splitString[3];
		splitString = tmp.split('=');
		var modelID = splitString[1];
	
		alert('[webapis.js] recognition.isSDK() tmp: ' +tmp+ '  modelID: '+modelID);
		
		if (modelID =='SDK')
			return true;
		else 
			return false;		
	},
	
	
	initialize : function(){
		alert('[webapis.js] recognition.initialize() called');
		
		webapis.recognition.plRecog = webapis._plugin("RECOG");
		if(webapis.recognition.plRecog == null){
			alert('[webapis.js] recognition not supported');
			return false;
		}
				
		if(webapis.recognition.blinitialized) 
			return true;
		
		webapis.recognition.blinitialized = true;	
		alert("[webapis.js] recognition.initialize() : plRecog = " +webapis.recognition.plRecog);
				
		webapis.recognition.plRecog.OnEvent = webapis.recognition._handleEvent;
		
		return true;
	},
	
	
	setCallback : function(type, name, callback){		
		alert('[webapis.js] recognition.setCallback() called');
		
		webapis.recognition.oCallback[type] = callback;
		webapis.recognition.oCallback_name[type] = name;		
	},
	
	
	unsetCallback : function(type){
		alert('[webapis.js] recognition.unsetCallback() called');
		
		webapis.recognition.oCallback[type] = null;
		webapis.recognition.oCallback_name[type] = null;
	},


	_handleEvent : function(recognitiontype, eventtype, result){
		alert('[webapis.js] recognition.handleEvent() called');
		alert('[webapis.js] recognition.handleEvent() ' + recognitiontype +', '+eventtype+', '+result);
		
		var type = null;
		switch(recognitiontype){
			case webapis.recognition.MESSAGE_RECOGNITION_VOICE_EMP : // voice
				type = webapis.recognition.PL_RECOGNITION_TYPE_VOICE;
				break;
				
			case webapis.recognition.MESSAGE_RECOGNITION_GESTURE_EMP :
				type = webapis.recognition.PL_RECOGNITION_TYPE_GESTURE;
				break;
				
			case webapis.recognition.MESSAGE_RECOGNITION_FACE_EMP :
				type = webapis.recognition.PL_RECOGNITION_TYPE_FACE;
				break;
			
			default :
				alert("unknown recognition type");
				return false;
				break;	
		}
		
		splitString = eventtype.split('-');
		var event = splitString[0];
		var name = 	splitString[1];		
						
		if(webapis.recognition.oCallback[type] && webapis.recognition.oCallback[type] instanceof Function){					
			if(!webapis.recognition.isSubscribeEvent){
				var resultInfo = {
						recognitiontype: type,
						eventtype: event,
						name: name,
						result: result
				};
				
				webapis.recognition.oCallback[type](resultInfo);
			}
					
			else if(webapis.recognition.isSubscribeEvent  && type == webapis.recognition.PL_RECOGNITION_TYPE_VOICE){		
				switch(event){
					case 'EVENT_VOICE_BEGIN_MONITOR':
					case 'EVENT_VOICE_BTSOUND_START':
						var guideHelpbarInfo = '{"helpbarType":"HELPBAR_TYPE_VOICE_SERVER_GUIDE","guideText":"Say the word or phrase you wish to type"}';
						
						webapis._plugin("RECOG","SetVoiceHelpbarInfo", guideHelpbarInfo);
												
						break;
					
					case 'EVENT_VOICE_RECOG_RESULT':							
						var tmp = event+'-' +webapis.recognition.oCallback_name[type]+'-'+result;
						webapis.recognition.oCallback[type](tmp);
						break;
					
					case 'EVENT_VOICE_LANGUAGE_CHANGED':
						break;
						
					default :
						break;						
				}								
			}	
		}
	},	
	
	//Open for SDK
	SDKTest_GenerateExpectedResult : function(number, resultList, interval){
		alert('[webapis.js] recognition.SDKTest_GenerateExpectedResult() called');
		if(!webapis.recognition.isSDK() && !webapis.recognition.isSEFSupported()) 
			return false;		
				
		splitString = resultList.split('&&');
		var list = splitString;
		
		alert('[webapis.js] recognition.SDKTest_GenerateExpectedResult() list: '+list);

		count = 0;		

		timer = setInterval('setEvent()', interval*1000);
		
		setEvent = function(){
			if(count < number){
				webapis.recognition._handleEvent(webapis.recognition.MESSAGE_RECOGNITION_VOICE_EMP,'EVENT_VOICE_RECOG_RESULT-'+webapis.recognition.oCallback_name[0],list[count]);
				count++;
			}
			else
				clearInterval(timer);
		}
		return true;
	},	
	
	
	//Open for SDK, only Severguide mode available
	SubscribeEvent : function(type, name, callback){		
		alert("[webapis.js] recognition.SubscribeEvent() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		if(webapis.recognition.initialize()){
			webapis.recognition.is3rd = true;
			webapis.recognition.isSubscribeEvent = true;
			
			webapis.recognition.setCallback(type, name, callback);

			var message_type = null;

			switch(type){	
				case webapis.recognition.PL_RECOGNITION_TYPE_VOICE :
					 message_type = 'MESSAGE_RECOGNITION_VOICE';
					 break;
					 
				case webapis.recognition.PL_RECOGNITION_TYPE_GESTURE :
					 message_type = 'MESSAGE_RECOGNITION_GESTURE';
					 break;		
					 
				case webapis.recognition.PL_RECOGNITION_TYPE_FACE :
					 message_type = 'MESSAGE_RECOGNITION_FACE';
					 break;		 
			}

			return webapis.recognition.setRetValue(webapis._plugin("RECOG","SubscribeEvent", message_type, name));				
		}
		else
			return false;
	},
	
	
	//Open for SDK
	UnsubscribeEvent : function(type, name){	
		alert("[webapis.js] recognition.UnsubscribeEvent() called ");
		
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		//is3rd, isSubscribeEvent change "false" from "true"		
		webapis.recognition.is3rd = false;
		webapis.recognition.isSubscribeEvent = false;
		
		webapis.recognition.unsetCallback(type);
		
		var message_type = null;
			
		switch(type){
			case webapis.recognition.PL_RECOGNITION_TYPE_VOICE :
				 message_type = 'MESSAGE_RECOGNITION_VOICE';
				 break;
				 
			case webapis.recognition.PL_RECOGNITION_TYPE_GESTURE :
				 message_type = 'MESSAGE_RECOGNITION_GESTURE';
				 break;		
				 
			case webapis.recognition.PL_RECOGNITION_TYPE_FACE :
				 message_type = 'MESSAGE_RECOGNITION_FACE';
				 break;		 
		}
	
		return webapis.recognition.setRetValue(webapis._plugin("RECOG",'UnsubscribeEvent', message_type, name));
	
	},
	
	
	//Open for SDK
	//change comment by eunjin.yoo(120227) 
	SubscribeExEvent : function(type, name, callback){		
		alert("[webapis.js] recognition.SubscribeExEvent() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		if(webapis.recognition.initialize()){
			webapis.recognition.is3rd = true;
			webapis.recognition.isSubscribeEvent = false;
		
			webapis.recognition.setCallback(type, name, callback);
		
				var message_type = null;			
				switch(type){	
					case webapis.recognition.PL_RECOGNITION_TYPE_VOICE :
						 message_type = 'MESSAGE_RECOGNITION_VOICE';
						 break;
						 
					case webapis.recognition.PL_RECOGNITION_TYPE_GESTURE :
						 message_type = 'MESSAGE_RECOGNITION_GESTURE';
						 break;		
						 
					case webapis.recognition.PL_RECOGNITION_TYPE_FACE :
						 message_type = 'MESSAGE_RECOGNITION_FACE';
						 break;		 
				}
			
			return webapis.recognition.setRetValue(webapis._plugin("RECOG","SubscribeEvent", message_type, name));	
	
		}
		else
			return false;
	},
	
	
	//Open for SDK
	UnsubscribeExEvent : function(type, name){	
		alert("[webapis.js] recognition.UnsubscribeExEvent() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		//is3rd change "false" from "true"	
		webapis.recognition.is3rd = false;
		webapis.recognition.unsetCallback(type);

		var message_type = null;
		
		switch(type){
			case webapis.recognition.PL_RECOGNITION_TYPE_VOICE :
				 message_type = 'MESSAGE_RECOGNITION_VOICE';
				 break;
				 
			case webapis.recognition.PL_RECOGNITION_TYPE_GESTURE :
				 message_type = 'MESSAGE_RECOGNITION_GESTURE';
				 break;		
				 
			case webapis.recognition.PL_RECOGNITION_TYPE_FACE :
				 message_type = 'MESSAGE_RECOGNITION_FACE';
				 break;		 
		}
	
		return webapis.recognition.setRetValue(webapis._plugin("RECOG",'UnsubscribeEvent', message_type, name));	
		
	},	
	
	
	//Open for SDK
	IsRecognitionSupported : function(){
		alert("[webapis.js] recognition.IsRecognitionSupported() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		return webapis.recognition.setRetValue(webapis._plugin("RECOG", 'RecogEMPUsingStatus'));
	},
	
	
	IsRecognitionAppAvailable : function(){
		alert("[webapis.js] recognition.IsRecognitionAppAvailable() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		return webapis.recognition.setRetValue(webapis._plugin("RECOG", 'RecogEMPUsingStatus'));
	},
	
	
	RegisterRecognition: function(type, name, callback){
		alert("[webapis.js] recognition.RegisterRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		if(webapis.recognition.initialize()){
			webapis.recognition.setCallback(type, name, callback);
			webapis.recognition.is3rd = false;
			webapis.recognition.isSubscribeEvent = false;
			
			if(!webapis.recognition.isSDK()){
				var message_type = null;
				
				switch(type){	
					case webapis.recognition.PL_RECOGNITION_TYPE_VOICE :
						 message_type = 'MESSAGE_RECOGNITION_VOICE';
						 break;
						 
					case webapis.recognition.PL_RECOGNITION_TYPE_GESTURE :
						 message_type = 'MESSAGE_RECOGNITION_GESTURE';
						 break;		
						 
					case webapis.recognition.PL_RECOGNITION_TYPE_FACE :
						 message_type = 'MESSAGE_RECOGNITION_FACE';
						 break;		 
				}
				
				return webapis.recognition.setRetValue(webapis._plugin("RECOG","SubscribeEvent", message_type, name)); 
			}
		}		
		else 
				return false;
	},
	
	
	UnregisterRecognition: function(type, name)	{	
		alert("[webapis.js] recognition.UnregisterRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		webapis.recognition.unsetCallback(type);
		
		if(!webapis.recognition.isSDK()){
			var message_type = null;
			
			switch(type){
				case webapis.recognition.PL_RECOGNITION_TYPE_VOICE :
					 message_type = 'MESSAGE_RECOGNITION_VOICE';
					 break;
					 
				case webapis.recognition.PL_RECOGNITION_TYPE_GESTURE :
					 message_type = 'MESSAGE_RECOGNITION_GESTURE';
					 break;		
					 
				case webapis.recognition.PL_RECOGNITION_TYPE_FACE :
					 message_type = 'MESSAGE_RECOGNITION_FACE';
					 break;		 
			}
		
			return webapis.recognition.setRetValue(webapis._plugin("RECOG",'UnsubscribeEvent', message_type, name));
		}
		
	},
	
	
	//Open for SDK
	SetVoiceHelpbarInfo: function(helpbarInfo){
		alert("[webapis.js] recognition.SetVoiceHelpbarInfo() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		return webapis.recognition.setRetValue(webapis._plugin("RECOG","SetVoiceHelpbarInfo", helpbarInfo));
	},
	
	
	SetVoiceCandidateList: function(list){
		alert("[webapis.js] recognition.SetVoiceCandidateList() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","SetVoiceCandidateList", list));
	},


	SetVoiceHelpbarItemsList: function(list){		
		alert("[webapis.js] recognition.SetVoiceHelpbarItemsList() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","SetVoiceHelpbarItemsList", list));
	},

	
	SetVoiceHelpbarType: function(type, userData1, userData2, bKeepCurrentInfo){
		alert("[webapis.js] recognition.SetVoiceHelpbarType() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","SetVoiceHelpbarType", type, userData1, userData2, bKeepCurrentInfo));
	},
	
	
	//Open for SDK
	//2012-04-19 change a return value 'arb' -> 'ar' by eunjin.yoo  
	GetCurrentVoiceLanguage : function(){
		alert("[webapis.js] recognition.GetCurrentVoiceLanguage() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		//return webapis._plugin("RECOG","GetCurrentVoiceLanguage");
		
 		var retVal = webapis._plugin("RECOG","GetCurrentVoiceLanguage");
 		
 		if(retVal == 'arb')
 			return 'ar';
 		else
 			return retVal;
	},
	
	
	GetVoiceRecognitionStatus : function(){
		alert("[webapis.js] recognition.GetVoiceRecognitionStatus() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		return webapis._plugin("RECOG","GetVoiceRecognitionStatus");
	},
	
	
	EnableVoiceRecognition : function(){
		alert("[webapis.js] recognition.EnableVoiceRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;
		
		if(!webapis.recognition.isSDK() && !webapis.recognition.is3rd)
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","EnableVoiceRecognition"));
	},
	
	
	DisableVoiceRecognition : function(){
		alert("[webapis.js] recognition.DisableVoiceRecognition() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK() && !webapis.recognition.is3rd)
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","DisableVoiceRecognition"));
	},
	
	
	//Open for SDK
	IsVoiceRecognitionEnabled: function(){	
		alert("[webapis.js] recognition.IsVoiceRecognitionEnabled() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		return webapis.recognition.setRetValue(webapis._plugin("RECOG","IsVoiceRecognitionEnabled"));
	},
	
	
	StartVoiceRecognition : function () {
		alert("[webapis.js] recognition.StartGestureRecognition() called ");	
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","StartGestureRecognition"));
		else
			return false;
	},
	
		
	StopVoiceRecognition : function () {
		alert("[webapis.js] recognition.StopVoiceRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;	
		
		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","StopVoiceRecognition"));
		else
			return false;
	},	
	
	//Open for SDK, 120924
	SetGestureHelpbarInfo : function (helpbarInfo){
		alert("[webapis.js] recognition.SetGestureHelpbarInfo() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","SetGestureHelpbarInfo", helpbarInfo));
	},
	
	
	GetGestureRecognitionStatus : function (){
		alert("[webapis.js] recognition.GetGestureRecognitionStatus() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis._plugin("RECOG","GetGestureRecognitionStatus");
		else
			return false;
	},
	
	
	EnableGestureRecognition : function(){		
		alert("[webapis.js] recognition.EnableGestureRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK() && !webapis.recognition.is3rd)
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","EnableGestureRecognition"));
		else
			return false;
	},
	
	
	DisableGestureRecognition : function(){		
		alert("[webapis.js] recognition.DisableGestureRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK() && !webapis.recognition.is3rd)
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","EnableGestureRecognition"));
		else
			return false;
	},
	
	//Open for SDK, 120924
	IsGestureRecognitionEnabled : function(){		
		alert("[webapis.js] recognition.IsGestureRecognitionEnabled() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","IsGestureRecognitionEnabled"));
	},
	
	
	StartGestureRecognition : function(){		
		alert("[webapis.js] recognition.StartGestureRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","StartGestureRecognition"));
		else
			return false;
	},
	
	
	StopGestureRecognition : function(){		
		alert("[webapis.js] recognition.StopGestureRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","StopGestureRecognition"));
		else
	 		return false;
	},
	
	
	//Open for SDK
	ShowVoiceHelpbar: function(){		
		alert("[webapis.js] recognition.ShowVoiceHelpbar() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		return webapis.recognition.setRetValue(webapis._plugin("RECOG","ShowVoiceHelpbar"));
	},	
	
	
	//Open for SDK
	HideVoiceHelpbar: function(){		
		alert("[webapis.js] recognition.HideVoiceHelpbar() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		return webapis.recognition.setRetValue(webapis._plugin("RECOG","HideVoiceHelpbar"));
	},	
	
	
	//Open for SDK
	SetVoiceTimeout: function(msTime){		
		alert("[webapis.js] recognition.SetVoiceTimeout() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		return webapis.recognition.setRetValue(webapis._plugin("RECOG","SetVoiceTimeout", msTime));
	},
	
	
	//2012-04-19 change a return value 'arb' -> 'ar' by eunjin.yoo  
	GetVoiceServerLanguage: function(){		
		alert("[webapis.js] recognition.GetVoiceServerLanguage() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		//if(!webapis.recognition.isSDK())
	 	//	return webapis._plugin("RECOG","GetVoiceServerLanguage");
	 		
	 	if(!webapis.recognition.isSDK()){
		 	var retVal = webapis._plugin("RECOG","GetCurrentVoiceLanguage");
	 		if(retVal == 'arb')
	 			return 'ar';
	 		else
	 			return retVal;
	 	}
	},
	
	
	//Open for SDK
	IsVoiceServerLanguageSupported: function(){		
		alert("[webapis.js] recognition.IsVoiceServerLanguageSupported() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","IsVoiceServerLanguageSupported"));
	},
	
	
	IsActivatedVoiceRecognition: function(){		
		alert("[webapis.js] recognition.IsActivatedVoiceRecognition() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","IsActivatedVoiceRecognition"));
	},
	
	
	EnableGestureHint: function(){
		alert("[webapis.js] recognition.EnableGestureHint() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","EnableGestureHint"));
		else
			return false;
	},
	
	DisableGestureHint: function(){
		alert("[webapis.js] recognition.DisableGestureHint() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","DisableGestureHint"));
		else
			return false;
	},
	
	EnableReturnMotion: function(){
		alert("[webapis.js] recognition.EnableReturnMotion() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","EnableReturnMotion"));
		else
			return false;	
	},
	
	DisableReturnMotion: function(){
		alert("[webapis.js] recognition.DisableReturnMotion() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","DisableReturnMotion"));
		else
			return false;	
	},
	
	EnableNativeTwoCursorTheme: function(){
		alert("[webapis.js] recognition.EnableNativeTwoCursorTheme() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","EnableNativeTwoCursorTheme"));
		else
			return false;	
	},
	
	DisableNativeTwoCursorTheme: function(){
		alert("[webapis.js] recognition.DisableNativeTwoCursorTheme() called ");
		if(!webapis.recognition.isSEFSupported())
			return false;

		if(!webapis.recognition.isSDK())
	 		return webapis.recognition.setRetValue(webapis._plugin("RECOG","DisableNativeTwoCursorTheme"));
		else
			return false;	
	}				
}


var ZindexSet = 0;
var SaveProperty = 0;
// Usage of initialize() function is modified, 2012.02.21
//
webapis.camera = {
	PL_CAMERA_EVENT_DISCONNECTED: 0,
	PL_CAMERA_EVENT_CONNECTING: 1,
	PL_CAMERA_EVENT_CONNECTED: 2,
	PL_CAMERA_EVENT_JPEG_CAPTURED: 3,

	PL_CAMERA_STATE_DISCONNECTED: 0,
	PL_CAMERA_STATE_CONNECTING: 1,
	PL_CAMERA_STATE_READY: 2,
	PL_CAMERA_STATE_PLAYING: 3,

	PL_CAMERA_QUALITY_LOW: 1,
	PL_CAMERA_QUALITY_MID: 2,
	PL_CAMERA_QUALITY_HIGH: 3,

	PL_CAMERA_RESOLUTION_VGA: 1,
	PL_CAMERA_RESOLUTION_HD: 2,
	
	PL_CAMERA_WINDOW_MAIN: 1,
	PL_CAMERA_WINDOW_SUB: 2,
	
	oCameraPluginCallback : null,	
	bCameraPluginInitialiezed : false,	
	bCameraStarted : false,	
	plCamera : null,
	
	
	isSEFSupported : function(){
		alert('[webapis.js] camera.isSEFSupported() called');
		
		firmwareVer = webapis._plugin('NNavi', 'GetFirmware');
		splitString = firmwareVer.split('-');
		var tmp = splitString[1];		
		alert('[webapis.js] camera.isSEFSupported() platform : ' + tmp.substr(8,11));
		var year = parseInt(tmp.substr(8,11),10);
		if(year >= 2012 && webapis._plugin("Camera") != null)
			return true;
		else {
			alert('[webapis.js] camera not supported.');
			return false;
		}
	},	
	
	
	isSDK : function(){
		alert('[webapis.js] camera.isSDK() called');
		var uri = decodeURI(window.location.search);
		splitString = uri.split('&');
		var tmp = splitString[3];
		splitString = tmp.split('=');
		var modelID = splitString[1];
	
		alert('[webapis.js] camera.isSDK() tmp: ' +tmp+ '  modelID: '+modelID);
		
		if (modelID =='SDK')
			return true;
		else 
			return false;		
	},
	
	
	initialize : function(){
		if(webapis.camera.bCameraPluginInitialiezed){
			return;
		}

		alert('[webapis.js] camera.initialize() called');

		webapis.camera.bCameraPluginInitialiezed = true;	
		webapis.camera.plCamera = webapis._plugin("Camera");
		webapis.camera.plCamera.OnEvent = webapis.camera._handleEvent;
	},
	
	_handleEvent : function(eventType, data1, data2){
		alert('[webapis.js] camera.handleEvent() called');
		alert('[webapis.js] camera.handleEvent() ' + eventType +', '+data1+', '+data2);

		if( webapis.camera.oCameraPluginCallback != null)
			webapis.camera.oCameraPluginCallback(eventType);
	},	
       //Open for SDK
	RegisterEventCallback : function(callback){	
		if(!webapis.camera.isSEFSupported())
			return false;
				
		webapis.camera.initialize();
		alert('[webapis.js] camera.setCallback() called');
		webapis.camera.oCameraPluginCallback = callback;
	},
	
	
	UnregisterEventCallback : function(){
		if(!webapis.camera.isSEFSupported())
			return false;
		
		webapis.camera.initialize();
		alert('[webapis.js] camera.unsetCallback() called');
		
		webapis.camera.oCameraPluginCallback = null;
	},

      //Open for SDK 
	GetCameraState: function(){	
		if(!webapis.camera.isSEFSupported())
			return false;
			
		webapis.camera.initialize();
		alert("[webapis.js] camera.GetCameraState() called ");
		
// commented by dongyoung,2012-05-11		
		if(webapis.camera.isSDK()) {
        	return webapis._plugin("Camera","GetCameraState");
        } else { // 2012.5.11 convert string value to enum
            return parseInt(webapis._plugin("Camera","GetCameraState"));
        }
	},	

	//Do not open for SDK
	StartCamVideo: function(x, y, w, h, resolution, quality){	
		if(!webapis.camera.isSEFSupported())
			return false;
			
		webapis.camera.initialize();
		alert("[webapis.js] camera.StartCamVideo() called ");
		alert("[webapis.js] x:"+x+' y:'+y+' w:'+w+' h:'+h+' res:'+resolution+' q:'+quality);

		if(webapis.camera.bCameraStarted)
		{
			alert("[webapis.js] camera.StartCamVideo() already camera started");
			return;
		}
		
		var rate = "5000000";
		var res = webapis.camera.PL_CAMERA_RESOLUTION_HD;
		var qov = webapis.camera.PL_CAMERA_QUALITY_HIGH;

		if(resolution) res = resolution;
		if(quality) qov = quality;

		webapis._plugin("Camera","SetUsecase","1");
		webapis._plugin("Camera","SetCamVideoDisplaySize",String(x),String(y),String(w),String(h));
		switch(res){
			case webapis.camera.PL_CAMERA_RESOLUTION_VGA:
				webapis._plugin("Camera","SetCamProperty","3","640","480");
				switch(qov){
					case webapis.camera.PL_CAMERA_QUALITY_LOW:
						rate = "100000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_MID:
						rate = "500000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_HIGH:
					default:
						rate = "2000000";
						break;
				}
				break;
			case webapis.camera.PL_CAMERA_RESOLUTION_HD:
			default:
				webapis._plugin("Camera","SetCamProperty","3","1280","720");
				switch(qov){
					case webapis.camera.PL_CAMERA_QUALITY_LOW:
						rate = "1000000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_MID:
						rate = "2000000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_HIGH:
					default:
						rate = "5000000";
						break;
				}
				break;
		}
		webapis._plugin("Camera","SetCamProperty","0",rate); //bitrate
		webapis._plugin("Camera","SetCamProperty","1","30"); //framerate
		var ret = webapis._plugin("Camera","StartCamVideo","1","2"); //main scaler, H264
		webapis._plugin("Camera","SetCamProperty","2","1"); //new gop
		webapis.camera.bCameraStarted = true;
		return ret;
	},
	//Do not open for SDK
	StopCamVideo: function(){	
		if(!webapis.camera.isSEFSupported())
			return false;
			
		webapis.camera.initialize();
		alert("[webapis.js] camera.StopCamVideo() called ");
		var ret;

	 		ret = webapis._plugin("Camera","StopCamVideo");

		webapis.camera.bCameraStarted = false;
		return ret;
	},	
	
	//Do not open for SDK
	ShowCamVideo: function(x, y, w, h, options) {	
		if(!webapis.camera.isSEFSupported())
			return false;
		
		var mode = options && options.mode || 1;	// default mode: main(1)
		var resolution = options && options.resolution || webapis.camera.PL_CAMERA_RESOLUTION_HD;	// default resolution: HD
		var quality = options && options.quality || webapis.camera.PL_CAMERA_QUALITY_HIGH;			// default quality: HIGH
		var container = options && options.container || null;	// default container: null(create container and append to body)
		

		webapis.camera.initialize();
		alert("[webapis.js] camera.StartCamVideo() called ");
		alert('[webapis.js] x:'+x+' y:'+y+' w:'+w+' h:'+h+' res:'+resolution+' q:'+quality +' container: ' + (container?'defined':'notdefined'));

		if(webapis.camera.bCameraStarted)
		{
			alert("[webapis.js] camera.ShowCamVideo() already camera started");
			return;
		}
		
		var rate = "5000000";
		var res = webapis.camera.PL_CAMERA_RESOLUTION_HD;
		var qov = webapis.camera.PL_CAMERA_QUALITY_HIGH;

		res = resolution;
		if(quality) qov = quality;

		webapis._plugin("Camera","SetUsecase","1");
		//webapis._plugin("TV", "GetPanelResolution"); << 패널해상도
		var jsonString = webapis._plugin("TV", "GetPanelResolution");
		var resolution = eval('('+jsonString+')');
		alert("PanelVertical : " + resolution.vertical);

		webapis._plugin("Camera","SetCamVideoDisplaySize",String(this._convRes(x, resolution.vertical)),String(this._convRes(y, resolution.vertical)),String(this._convRes(w, resolution.vertical)),String(this._convRes(h, resolution.vertical)));
		switch(res){
			case webapis.camera.PL_CAMERA_RESOLUTION_VGA:
				webapis._plugin("Camera","SetCamProperty","3","640","480");
				switch(qov){
					case webapis.camera.PL_CAMERA_QUALITY_LOW:
						rate = "100000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_MID:
						rate = "500000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_HIGH:
					default:
						rate = "2000000";
						break;
				}
				break;
			case webapis.camera.PL_CAMERA_RESOLUTION_HD:
			default:
				webapis._plugin("Camera","SetCamProperty","3","1280","720");
				switch(qov){
					case webapis.camera.PL_CAMERA_QUALITY_LOW:
						rate = "1000000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_MID:
						rate = "2000000";
						break;
					case webapis.camera.PL_CAMERA_QUALITY_HIGH:
					default:
						rate = "5000000";
						break;
				}
				break;
		}

		this.SetMirrorMode(SaveProperty);
		webapis._plugin("Camera","SetCamProperty","0",rate); //bitrate
		webapis._plugin("Camera","SetCamProperty","1","30"); //framerate
		var ret = webapis._plugin("Camera","StartCamVideo",String(mode),"2"); //main scaler, H264
		webapis._plugin("Camera","SetCamProperty","2","1"); //new gop


		if (!this._windowObj || container) {
			this.ShowCamWindow(x,y,w,h, container||null);
		}

		webapis.camera.bCameraStarted = true;

		return ret;
	},

	//Do not open for SDK
	HideCamVideo: function(){	
		if(!webapis.camera.isSEFSupported())
			return false;
			
		webapis.camera.initialize();
		alert("[webapis.js] camera.StopCamVideo() called ");
		
		if (this._windowObj) {
			this.HideCamWindow();
		}

		var ret;
		
	 	ret = webapis._plugin("Camera","StopCamVideo");

		webapis.camera.bCameraStarted = false;
		return ret;
	},

	//Do not open for SDK
	MoveCamVideo: function(x, y, w, h){	
		if(!webapis.camera.isSEFSupported())
			return false;
			
		webapis.camera.initialize();
		alert("[webapis.js] camera.MoveCamVideo() called ");
		
		if (this._windowObj) {
			this.HideCamWindow();
		}

		var ret;
		var jsonString = webapis._plugin("TV", "GetPanelResolution");
		var resolution = eval('('+jsonString+')');
		ret = webapis._plugin("Camera","SetCamVideoDisplaySize",String(this._convRes(x, resolution.vertical)),String(this._convRes(y, resolution.vertical)),String(this._convRes(w, resolution.vertical)),String(this._convRes(h, resolution.vertical)));
		
		if (!this._windowObj) {
			this.ShowCamWindow(x,y,w,h);
		}
		return ret;
	},

	SetZIndexWindowObject: function(data){
		alert("SetZIndexWindowObject()");
		alert("ZindexSet = " + ZindexSet);
		if(!this.windowObj){
			ZindexSet = data;
			alert("ZindexSet = " + ZindexSet);
		}
	
		if(!data) data = 0;

		var obj = document.getElementById("pluginObjectWindowForCam");
		obj && (obj.parentElement.style.zIndex = data);

		return data;
	},
	
	GetZIndexWindowObject: function(){
		alert("SetZIndexWindowObject()");
		//if(!this.windowObj) return false;

		var obj = document.getElementById("pluginObjectWindowForCam").parentNode;
		var style = window.getComputedStyle(obj,null);
		var data = style.zIndex;
		if(data == "auto") data = "0";
		alert("zindex : " + data);

		return data;
	},

	SetMirrorMode: function(property){
		if(!property) property = 0;
		if(!webapis.camera.bCameraStarted){ //Not true (Camera isn't Started)
			SaveProperty = property;
			alert("Mirror Mode = " + SaveProperty);
		}
		webapis._plugin("Camera","SetCamProperty","12",String(property)); //Mirror mode --> 1 : Mirror mode
		alert("property = " + property);
		if(property == 0){
			var pluspro = 1;
		} else if(property == 1){
			var pluspro = 0;
		}
		webapis._plugin("Camera","SetCamProperty","43",String(pluspro));
		alert("pluspro = " + pluspro);
		return property;
	},

	//Do not open for SDK
	ShowCamWindow: function (x,y,w,h, container) {
		alert("ShowCamWindow()");
		if(this._windowObj && !container)	return false;

		var domNode = document.getElementById(container) || document.createElement('div');
		if(!container) {
			domNode.id = '_pluginObjectWindowForCamContainer_';
			//domNode.style.zIndex = $.sf.define.zIndex.plugin;
			document.body.appendChild(domNode);
		}
		domNode.style.position = 'absolute';
		domNode.style.left = x + 'px';
		domNode.style.top = y + 'px';

		var style = 'display:block;position:absolute;width:'+w+'px;height:'+h+'px;';


		html = '<OBJECT id="pluginObjectWindowForCam" classid="clsid:SAMSUNG-INFOLINK-SEF" style="'+style+'"></OBJECT>';
		domNode.innerHTML += html;
		
		this._windowObj = document.getElementById('pluginObjectWindowForCam');
		alert("ShowCamWindow() END: " + this._windowObj);
		alert("ZindexSet = " + ZindexSet);

		var obj = document.getElementById("pluginObjectWindowForCam");
		obj.parentNode.style.zIndex = ZindexSet;
		ZindexSet = 0;
		alert("ZindexSet = " + ZindexSet);
	
		return this._windowObj;
	},

	//Do not open for SDK	
	HideCamWindow: function(){
		if(!webapis.camera.isSEFSupported())
			return false;
		
		//this._windowObj = null;
		var obj = document.getElementById("pluginObjectWindowForCam");
		var div = document.getElementById("_pluginObjectWindowForCamContainer_");

		if(this._windowObj) {
			this._windowObj.parentElement.removeChild(this._windowObj);
		}
		if(div) {
			document.body.removeChild(div);
		}
		this._windowObj = null;

		return 1;
	},

	//Do not open for SDK	
	_convRes: function (val, res) {
		var ret = parseInt(val * (res / curWidget.height), 10);
		alert("_convRes("+val+", " + res + "): " + ret);
		return ret;
	},

	CaptureCamVideo: function(width, height, cropArea){	
		if(!webapis.camera.isSEFSupported())
			return false;
		
		
			var scaleW = ((width == 0) ? String(0) : width) || null;	
			var scaleH = ((height == 0) ? String(0) : height) || null;
		if(cropArea != null){
			var cropX = ((cropArea && cropArea.x == 0) ? String(0) : (cropArea && cropArea.x)) || null;
			var cropY = ((cropArea && cropArea.y == 0) ? String(0) : (cropArea && cropArea.y)) || null;
			var cropW = ((cropArea && cropArea.w == 0) ? String(0) : (cropArea && cropArea.w)) || null;
			var cropH = ((cropArea && cropArea.h == 0) ? String(0) : (cropArea && cropArea.h)) || null;
		} else {
			var cropX = cropY = cropW = cropH = null;
		}

		alert("Check Parameter!===================================================");
		alert("CropX : " + cropX);
		alert("CropY : " + cropY);
		alert("CropW : " + cropW);
		alert("CropH : " + cropH);
		alert("ScaleW : " + scaleW);
		alert("ScaleH : " + scaleH);

		webapis.camera.initialize();
		alert("[webapis.js] camera.CapturCamVideo() called ");
		var ret;

		
		if(scaleW == null && scaleH == null && cropX == null && cropY == null && cropW == null && cropH == null){ // no parameter
			//do nothing
			if(!webapis.camera.isSDK()){
				ret = webapis._plugin("Camera","CaptureCamVideo");
			}
		} else {
			if(scaleW < 0 || scaleH < 0 || cropX < 0 || cropY < 0 || cropW < 0 || cropH < 0){
				alert("Error!!! Parameter < 0");
				return false;
			}
	
			if(scaleW == null || scaleH == null){
				alert("Parameter error");
				return false;
			} else {
				if(cropX == null && cropY == null && cropW == null && cropH == null){
					//scaled
					alert("Check : Scaled!!")
					if(!webapis.camera.isSDK()){
						var scaledSize = scaleW + "," + scaleH;
						ret = webapis._plugin("Camera","CaptureCamVideo",scaledSize);
					}
				} else { //crop
					if(cropX == null || cropX == null || cropW == null || cropH == null) {
						alert("Parameter error");
						return false;
					} else {
						if(((cropX + cropW) > scaleW ) || ((cropY + cropH) > scaleH)){
							alert("Check : Crop!!")
							alert("CropX : " + cropX);
							alert("CropY : " + cropY);
							alert("CropW : " + cropW);
							alert("CropH : " + cropH);
							alert("ScaleW : " + scaleW);
							alert("ScaleH : " + scaleH);
							alert("Crop size is bigger than Image");
							return false;
						}
						var scaledSize = scaleW + "," + scaleH;
						var cropSize = cropX + "," + cropY + "," + cropW + "," + cropH;
						if(!webapis.camera.isSDK()){
							ret = webapis._plugin("Camera","CaptureCamVideo",scaledSize, cropSize);
						}
					}
				}
			}
		}
		return true;
	}
}


//121203 PVR 팀 API 김재석(jaeseok22.kim)
webapis.pvr = {
	bPVRPluginInitialiezed : false,
	plPVR : null,

	initialize: function(widgetName){
		alert('[webapis.js] pvr.initialize() called');		

		webapis.pvr.bPVRPluginInitialiezed = true;	
		webapis.pvr.plPVR = webapis._plugin("PVR");		

		var ret = webapis._plugin("PVR","OpenPVRWidget",widgetName);

		return ret;	
	},	

	registerEventCallback: function(callback){
		alert('[webapis.js] pvr.registerEventCallback() called');		
		
		if (typeof callback == 'function' && webapis.pvr.bPVRPluginInitialiezed == true )
		{
			webapis.pvr.plPVR.OnEvent = callback;
		}	
	},

	

	openPVRWidget: function(widgetName){
		alert('[webapis.js] pvr.openPVRWidget() called');		

		var ret = webapis._plugin("PVR","OpenPVRWidget",widgetName); 

		return ret;
	},
	
	closePVRWidget: function(widgetName){
		alert('[webapis.js] pvr.closePVRWidget() called');	

		var ret = webapis._plugin("PVR","ClosePVRWidget",widgetName); 

		return ret;
	},
	
	getMaxRecordNum: function(){
		alert('[webapis.js] pvr.getMaxRecordNum() called');

		var ret = webapis._plugin("PVR","GetMaxRecordNum"); 

		return ret;		
	},
	
	getHandle: function(handleType){
		alert('[webapis.js] pvr.GetHandle() called');	

		var ret = webapis._plugin("PVR","GetHandle",handleType); 

		return ret;		
	},
	
	recordStart: function(handle,PTC,major,minor){
		alert('[webapis.js] pvr.recordStart() called');	

		var ret = webapis._plugin("PVR","RecordStart",handle,PTC,major,minor); 

		return ret;		
	},
	
	recordStop: function(handle){
		alert('[webapis.js] pvr.recordStop() called');	

		var ret = webapis._plugin("PVR","RecordStop",handle); 

		return ret;		
	},
	
	timeshiftPlayStart: function(handle,PTC,major,minor){
		alert('[webapis.js] pvr.timeshiftPlayStart() called');	

		var ret = webapis._plugin("PVR","TimeshiftPlayStart",handle,PTC,major,minor); 

		return ret;				
	},
	
	timeshiftPauseStart: function(handle,PTC,major,minor){
		alert('[webapis.js] pvr.timeshiftPauseStart() called');	

		var ret = webapis._plugin("PVR","TimeshiftPauseStart",handle,PTC,major,minor); 

		return ret;		
	},
	
	timeshiftStop: function(handle){
		alert('[webapis.js] pvr.timeshiftStop() called');	

		var ret = webapis._plugin("PVR","TimeshiftStop",handle); 

		return ret;		
	},
	
	playStart: function(handle,uri){
		alert('[webapis.js] pvr.playStart() called');	

		var ret = webapis._plugin("PVR","PlayStart",handle,uri); 

		return ret;		
	},
	
	playPause: function(handle,uri){
		alert('[webapis.js] pvr.playPause() called');	

		var ret = webapis._plugin("PVR","PlayPause",handle,uri); 

		return ret;		
	},
	
	playResume: function(handle){
		alert('[webapis.js] pvr.playResume() called');	

		var ret = webapis._plugin("PVR","PlayResume",handle); 

		return ret;		
	},
	
	playForward: function(handle,forwardType){
		alert('[webapis.js] pvr.playForward() called');	

		var ret = webapis._plugin("PVR","PlayForward",handle,forwardType); 

		return ret;		
	},
	
	playRewind: function(handle,uri,rewindType){
		alert('[webapis.js] pvr.playRewind() called');	

		var ret = webapis._plugin("PVR","PlayRewind",handle,uri,rewindType); 

		return ret;		
	},
	
	playSkip: function(handle,uri,skipType,skipPos){
		alert('[webapis.js] pvr.playSkip() called');	

		var ret = webapis._plugin("PVR","PlaySkip",handle,uri,skipType,skipPos); 

		return ret;		
	},
	
	playStop: function(handle){
		alert('[webapis.js] pvr.playStop() called');	

		var ret = webapis._plugin("PVR","PlayStop",handle); 

		return ret;		
	},
	
	playRecordStop: function(handle){
		alert('[webapis.js] pvr.playRecordStop() called');	

		var ret = webapis._plugin("PVR","PlayRecordStop",handle); 

		return ret;		
	},
	
	getRecordState: function(handle){
		alert('[webapis.js] pvr.getRecordState() called');	

		var ret = webapis._plugin("PVR","GetRecordState",handle); 

		return ret;		
	},
	
	getPlayState: function(handle){
		alert('[webapis.js] pvr.GetPlayState() called');	

		var ret = webapis._plugin("PVR","GetPlayState",handle); 

		return ret;		
	},	
	
	getRecordProgress: function(handle){
		alert('[webapis.js] pvr.GetRecordProgress() called');	

		var ret = webapis._plugin("PVR","GetRecordProgress",handle); 

		return ret;		
	},
	
	getPlayProgress: function(handle){
		alert('[webapis.js] pvr.GetPlayProgress() called');	

		var ret = webapis._plugin("PVR","GetPlayProgress",handle); 

		return ret;		
	},

	deleteRecoredFile: function(uri){
		alert('[webapis.js] pvr.deleteRecoredFile() called');	

		var ret = webapis._plugin("PVR","DeleteRecoredFile",uri); 

		return ret;		
	},

	getRecDevCapacity: function(totalSpace, freeSpace){
		alert('[webapis.js] pvr.getRecDevCapacity() called');	

		var ret = webapis._plugin("PVR","GetRecDevCapacity",totalSpace,freeSpace); 

		return ret;		
	},
}



//=============================================================================
// AllShare WebAPI implementation   BEGIN
//=============================================================================


/** 
 * Title: serviceapis.js
 * version : 1.3.0.9
 * date : 2012-11-28
 * 
 * Copyright Samsung Electronics 2011-2012
 *
 * Author of original code : Yoojeong Jo <yoojeong1.jo@samsung.com>
 * Modification: Yevgen Poltavets <yevgen.poltavets@samsung.com>
 *
 */

 
//alert("[serviceapis.js] loading begin");
//alert("[serviceapis.js] Implementation of AllShare WebAPI 1.3 version");


// window.webapis.allshare.VERSION_ALLSHARE_WEBAPI version information defined below


//=============================================================================

function allshare_helper(){}


allshare_helper.setReadOnlyAttribute = function(obj, attrName, value)
{
    obj.__defineGetter__(attrName, function(){ return value; });
}


// set default flags for debug functions
allshare_helper.log = 
{
    flags :
        {
            print : true, // Special messages (versions info etc)
            nonimplemented : true, // marks for non implemented places
            trace : false, // information about entering to/from functions (huge volume of debug information)
            info : false, // more info(huge volume)
            debug : false, // debug info, important
            error : true // errors, exceptions
        }
}


allshare_helper.log.print = function(msg)
{
    if (this.flags.print)
        alert("Print: " + msg);
}

allshare_helper.log.nonimplemented = function(msg)
{
    if (this.flags.nonimplemented)
        alert("Not implemented: " + msg);
}

allshare_helper.log.trace = function(msg)
{
    if (this.flags.trace)
        alert("Trace: " + msg);
}

allshare_helper.log.info = function(msg)
{
    if (this.flags.info)
        alert("Info: " + msg);
}

allshare_helper.log.debug = function(msg)
{
    if (this.flags.debug)
        alert("Debug: " + msg);
}

allshare_helper.log.error = function(msg)
{
    if (this.flags.error)
        alert("Error: " + msg);
}


//-----------------------------------------------------------------------------


allshare_helper.convertStrDurationToSec = function(str)
{
//  allshare_helper.log.trace("allshare_helper.convertStrDurationToSec() begin");
//  allshare_helper.log.trace("allshare_helper.convertStrDurationToSec(str:" + str + ")");

    // result true/false
    this.isNumberChar = function(s)
    {
      var charCode = s.charCodeAt(0);
    //alert("s:" +s+ "  code:"+charCode);
      if (charCode >= 48 && charCode <= 57)
        return true;
      else
        return false;
    }


    // {rez_num:, rez_ok:, rez_str:}
    this.getNumber = function(str)
    {
      var num = 0;  
      var ok = false;
      for (var i = 0; i < str.length; i++)
      {
         var ch = str.charAt(i);
         if (this.isNumberChar(ch))
         {
            num *= 10;
            num += parseInt(ch);
            ok = true;
         }
         else
            break;
      }
      return {rez_num: num, rez_ok:ok , rez_str:  str.substring(i)};
    }


    // {rez_ok:, rez_str:}
    this.getDelimiter = function(str, ch)
    {
      var rez = { rez_ok: false, rez_str: str};
      if (str.length > 0)
      {
        if (str.charAt(0) == ch)
        {
          rez.rez_ok = true;
          rez.rez_str = str.substring(1);
        }
      }
      return rez;
    }


    var hourRez = this.getNumber(str);
    if (! hourRez.rez_ok)
    {
        return 0;
    }

    var delimResult1 = this.getDelimiter(hourRez.rez_str, ":");
    if (! delimResult1.rez_ok)
    {
        return 0;
    }

    var minuteRez = this.getNumber(delimResult1.rez_str);
    if (! minuteRez.rez_ok)
    {
        return 0;
    }

    var delimResult2 = this.getDelimiter(minuteRez.rez_str, ":");
    if (! delimResult2.rez_ok)
    {
        return 0;
    }

    var secondRez = this.getNumber(delimResult2.rez_str);
    if (! secondRez.rez_ok)
    {
        return 0;
    }

    if (secondRez.rez_str.length  != 0)
    {
        var delimResult1 = this.getDelimiter(minuteRez.rez_str, ".");
        if (! delimResult1.rez_ok)
        {
            return 0;
        }
    }

//   allshare_helper.log.debug("str:" + str + "     " + hourRez.rez_num + "-" + minuteRez.rez_num +"-"+ secondRez.rez_num);

//    allshare_helper.log.trace("allshare_helper.convertStrDurationToSec() end");

    return hourRez.rez_num * 3600 + minuteRez.rez_num * 60 + secondRez.rez_num;
}


//-----------------------------------------------------------------------------


allshare_helper.createWebAPIError = function(name, message)
{
//    var webAPIError = new WebAPIError;
    // ??? better to use common function object for error
    var webAPIError = {};

    // readonly attribute DOMString name;
    // readonly attribute DOMString message;

    allshare_helper.setReadOnlyAttribute(webAPIError, "name", name || "UnknownError");
    allshare_helper.setReadOnlyAttribute(webAPIError, "message", message || "An unknown error has occurred");
//    allshare_helper.log.error("WebAPIError name:" + webAPIError.name + "  message:" + webAPIError.message);
    return webAPIError;
}


allshare_helper.createWebAPIException = function(name, message)
{
//    var webAPIException = new WebAPIException;
    // ??? better to use common function object for exception
    var webAPIException = {};

    var code = 0; // default
    name = name || "UnknownError";
    message = message || "An unknown error has occurred";
    
    // webAPIException.code
    // webAPIException.name
    // webAPIException.message

    // set code attribute depends error 'name'
    switch(name)
    {
        case "IndexSizeError": code = 1; break; // INDEX_SIZE_ERR
//        case "": code = 2; break; // DOMSTRING_SIZE_ERR 
        case "HierarchyRequestError": code = 3; break; // HIERARCHY_REQUEST_ERR 
        case "WrongDocumentError": code = 4; break; // WRONG_DOCUMENT_ERR 
        case "InvalidCharacterError": code = 5; break; // INVALID_CHARACTER_ERR 
//        case "": code = 6; break; // NO_DATA_ALLOWED_ERR 
        case "NoModificationAllowedError": code = 7; break; // NO_MODIFICATION_ALLOWED_ERR 
        case "NotFoundError": code = 8; break; // NOT_FOUND_ERR 
        case "NotSupportedError": code = 9; break; // NOT_SUPPORTED_ERR 
//        case "": code = 10; break; // INUSE_ATTRIBUTE_ERR 
        case "InvalidStateError": code = 11; break; // INVALID_STATE_ERR 
        case "SyntaxError": code = 12; break; // SYNTAX_ERR 
        case "InvalidModificationError": code = 13; break; // INVALID_MODIFICATION_ERR 
        case "NamespaceError": code = 14; break; // NAMESPACE_ERR 
        case "InvalidAccessError": code = 15; break; // INVALID_ACCESS_ERR 
//        case "": code = 16; break; // VALIDATION_ERR 
        case "TypeMismatchError": code = 17; break; // TYPE_MISMATCH_ERR 
        case "SecurityError": code = 18; break; // SECURITY_ERR 
        case "NetworkError": code = 19; break; // NETWORK_ERR 
        case "AbortError": code = 20; break; // ABORT_ERR 
        case "URLMismatchError": code = 21; break; // URL_MISMATCH_ERR 
        case "QuotaExceededError": code = 22; break; // QUOTA_EXCEEDED_ERR 
        case "TimeoutError": code = 23; break; // TIMEOUT_ERR 
        case "InvalidNodeTypeError": code = 24; break; // INVALID_NODE_TYPE_ERR 
        case "DataCloneError": code = 25; break; // DATA_CLONE_ERR 
        default: break;
    }
    
    
    allshare_helper.setReadOnlyAttribute(webAPIException, "code", code);
    allshare_helper.setReadOnlyAttribute(webAPIException, "name", name);
    allshare_helper.setReadOnlyAttribute(webAPIException, "message", message);
//    allshare_helper.log.error("WebAPIError name:" + webAPIException.name + "  message:" + webAPIException.message);
    
    return webAPIException;
}



//=============================================================================
// Instantiation of 'WebAPIs webapis' object
if (window.webapis == null)
{
    allshare_helper.setReadOnlyAttribute(window, "webapis", {});
    allshare_helper.log.debug("added webapis object into window");
}



//=============================================================================
// Instantiation of  'AllShare allshare' object
if (window.webapis.allshare == null)
{
    allshare_helper.setReadOnlyAttribute(window.webapis, "allshare", {});
    allshare_helper.log.debug("added allshare object into webapis");
}


window.webapis.allshare.VERSION_ALLSHARE_WEBAPI = "1.3.0.9"; // non standard
if(webapis.pcmode) {
	console.log("AllShare Web API  JS: " + window.webapis.allshare.VERSION_ALLSHARE_WEBAPI);
}else {
	allshare_helper.log.print("AllShare Web API  JS: " + window.webapis.allshare.VERSION_ALLSHARE_WEBAPI);
}


//=============================================================================



//=============================================================================
// RequestsHelper
//=============================================================================

allshare_helper.RequestsHelper = function()
{
}


allshare_helper.RequestsHelper._requestsList = [];
allshare_helper.RequestsHelper._requestCounter = 0;

// This settings specify use uniqRequestId or not
// Current proxy support uniqRequestId for browse/search request responces but not for errors
allshare_helper.RequestsHelper._uniqRequestIdSupported = false;

allshare_helper.RequestsHelper.RequestInfo = function(id)
{
    this.id = id;

// fields added for Search
//    requestInfo.keyword = keyword;
//    requestInfo.startIndex = startIndex;
//    requestInfo.requestCount = requestCount;
//    requestInfo.successCallback = successCallback;
//    requestInfo.errorCallback = errorCallback;
//    requestInfo.provider = this;
//    requestInfo.callbackHandlers = SearchCallbackHandlers;

            
// fields added for Browse
//    requestInfo.parentFolderItem = paretnFolderItem;
//    requestInfo.startIndex = startIndex;
//    requestInfo.requestCount = requestCount;
//    requestInfo.successCallback = successCallback;
//    requestInfo.errorCallback = errorCallback;
//    requestInfo.provider = this;
//    requestInfo.callbackHandlers = BrowseCallbackHandlers;
            
}


// return RequestsHelper.RequestInfo object
allshare_helper.RequestsHelper.registerRequest = function()
{
    allshare_helper.log.trace("RequestsHelper.registerRequest() begin");

    var requestId = "reqId_" + this._requestCounter;
    var requestInfo = new allshare_helper.RequestsHelper.RequestInfo(requestId);
    allshare_helper.log.debug("RequestsHelper.registerRequest() requestCounter: " + this._requestCounter + "  requestId:" + requestId);
    this._requestCounter++;

    // needed if requestId is not supported
    if (this._uniqRequestIdSupported == false)
    {
        if (this._requestsList.length > 0) // means 1    
        {
            return null;
        }
    }
    
    this._requestsList.push(requestInfo);

    allshare_helper.log.trace("RequestsHelper.registerRequest() end");

    return requestInfo;
}


// return RequestsHelper.SearchRequestInfo object
allshare_helper.RequestsHelper.getRequest = function(requestId)
{
    allshare_helper.log.trace("RequestsHelper.getRequest()");
    allshare_helper.log.debug("RequestsHelper.getRequest(requestId:" + requestId + ")");

    // needed if requestId is not supported
    if (this._uniqRequestIdSupported == false)
    {
        if (this._requestsList.length > 0) // means 1    
        {
            return this._requestsList[0];
        }
        else
        {
            return null;
        }
    }

    for (var i = 0; i < this._requestsList.length; i++)
    {
        var requestInfo = this._requestsList[i];
        if (requestInfo.id == requestId)
        {
            return requestInfo;
        }
    }

    allshare_helper.log.debug("RequestsHelper.getRequest() Warning! Can't find search request with id:" + requestId);
    return null;
}


allshare_helper.RequestsHelper.removeRequest = function(requestId)
{
    allshare_helper.log.trace("RequestsHelper.removeRequest() begin");
    allshare_helper.log.debug("RequestsHelper.removeRequest(requestId:" + requestId + ")");

    // needed if requestId is not supported
    if (this._uniqRequestIdSupported == false)
    {
        if (this._requestsList.length > 0) // means 1    
        {
            this._requestsList.splice(0, 1);
        }
        return;
    }

    for (var i = 0; i < this._requestsList.length; )
    {
        var requestInfo = this._requestsList[i];
        if (requestInfo.id == requestId)
        {
            allshare_helper.log.debug("removed requestId:" + requestId);
            this._requestsList.splice(i, 1);
            continue;
        }
        i++;
    }
    allshare_helper.log.trace("RequestsHelper.removeRequest() end");
}

 

//=============================================================================
// Instantiation of 'ServiceConnector serviceconnector' object
allshare_helper.setReadOnlyAttribute(window.webapis.allshare, "serviceconnector", {});

window.webapis.allshare.serviceconnector._ServiceState = 
{
    DISABLED: "DISABLED",
    ENABLED: "ENABLED",
    UNKNOWN: "UNKNOWN"
};


window.webapis.allshare.serviceconnector._serviceProvider = null;


//=============================================================================
window.webapis.allshare.serviceconnector._ServiceProvider = {};


// prototype
// DeviceFinder getDeviceFinder() raises(WebAPIException);
window.webapis.allshare.serviceconnector._ServiceProvider.getDeviceFinder = function()
{
    allshare_helper.log.trace("window.webapis.allshare.serviceconnector._ServiceProvider.getDeviceFinder()");
    return window.webapis.allshare.serviceconnector._DeviceFinder;
}


// prototype
// ServiceState getServiceState() raises(WebAPIException); 
window.webapis.allshare.serviceconnector._ServiceProvider.getServiceState = function()
{
    allshare_helper.log.trace("window.webapis.allshare.serviceconnector._ServiceProvider.getServiceState()");

    var serviceState = "UNKNOWN";

    // if plugin object available then ServiceState ENABLED
    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(allsharePlugin)
    {
        serviceState = "ENABLED";
    } 

    return serviceState;
}


//=============================================================================


// prototype
// void createServiceProvider(in ServiceProviderCreateSuccessCallback successCallback, 
//                            in optional ServiceProviderErrorCallback errorCallback) raises(WebAPIException);
window.webapis.allshare.serviceconnector.createServiceProvider = function(successCallback, errorCallback)
{
    allshare_helper.log.trace("window.webapis.allshare.serviceconnector.createServiceProvider() begin");

    if (typeof successCallback != 'function')
    {
        allshare_helper.log.error("Type of successCallback is not function");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of successCallback is not function");
    }

    if (errorCallback != null)
    {
        if (typeof errorCallback != 'function')
        {
            allshare_helper.log.error("Type of errorCallback is not function");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of errorCallback is not function");
        }
    }

    if (this._serviceprovider)
    {
        allshare_helper.log.error("connection with the allshare service alreday exists");
        throw allshare_helper.createWebAPIException("AlreadyConnectedError", "connection with the allshare service alreday exists");
    }
    else
    {
        var allsharePlugin = window.webapis.allshare._plugin("AllShare");
        if(!allsharePlugin)
        {
            allshare_helper.log.error("AllShare plugin is not exist.");
            throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
        } 
        allsharePlugin.OnEvent = window.webapis.allshare._onAllsharePluginEvent; // set event receiver object
        

        this._serviceprovider = this._ServiceProvider; // like a static object

        try
        {
            successCallback(this._serviceprovider);
        }
        catch(e)
        {
            allshare_helper.log.error("exception during success callback");
            allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
        }
    }

    allshare_helper.log.trace("window.webapis.allshare.serviceconnector.createServiceProvider() end");
}


// prototype
// void deleteServiceProvider(in SuccessCallback successCallback,
//                            in optional ServiceProviderErrorCallback errorCallback) raises(WebAPIException);
window.webapis.allshare.serviceconnector.deleteServiceProvider = function(successCallback, errorCallback)
{
    allshare_helper.log.trace("window.webapis.allshare.serviceconnector.deleteServiceProvider() begin");

    if (typeof successCallback != 'function')
    {
        allshare_helper.log.error("Type of successCallback is not function");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of successCallback is not function");
    }

    if (errorCallback != null)
    {
        if (typeof errorCallback != 'function')
        {
            allshare_helper.log.error("Type of errorCallback is not function");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of errorCallback is not function");
        }
    }

    if (this._serviceprovider)
    {
        var serviceProvider = this._serviceprovider;
        this._serviceprovider = null;
        try
        {
            successCallback(serviceProvider);
        }
        catch(e)
        {
            allshare_helper.log.error("exception during success callback");
            allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
        }
    }
    else
    {
        try
        {
            var webAPIError = allshare_helper.createWebAPIError("NotFoundError", "serviceProvider does not exist.");
//            var serviceState = window.webapis.allshare.serviceconnector._ServiceProvider.getServiceState();
            var serviceState = this._ServiceState.UNKNOWN;  // ??? ServiceState value?
            if(errorCallback)
            {
                errorCallback(webAPIError, serviceState);
            }
        }
        catch(e)
        {
            allshare_helper.log.error("exception during error callback");
            allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
        }
    }

    allshare_helper.log.trace("window.webapis.allshare.serviceconnector.deleteServiceProvider() end");
}


// prototype
// ServiceProvider getServiceProvider() raises(WebAPIException);
window.webapis.allshare.serviceconnector.getServiceProvider = function()
{
    allshare_helper.log.trace("window.webapis.allshare.serviceconnector.getServiceProvider()");
    return this._serviceprovider;
}


//=============================================================================

window.webapis.allshare.serviceconnector._DeviceFinder = function(){}


window.webapis.allshare.serviceconnector._DeviceFinder._listeners = [];
window.webapis.allshare.serviceconnector._DeviceFinder._listenersCounter = 0;

window.webapis.allshare.serviceconnector._DeviceFinder._ListenerInfo = function(discoveryCallback, listenerId)
{
    allshare_helper.log.debug("DeviceFinder._ListenerInfo(discoveryCallback, listenerId:" + listenerId + ")");
    this.discoveryCallback = discoveryCallback;
    this.listenerId = listenerId;
}


window.webapis.allshare.serviceconnector._DeviceFinder._addedDevice = function(device)
{
    allshare_helper.log.trace("DeviceFinder._addedDevice() begin");
    allshare_helper.log.debug("DeviceFinder._addedDevice(device.name:" + device.name + ") begin");

    for (var i = 0; i < this._listeners.length; i++)
    {
        allshare_helper.log.debug("DeviceFinder._addedDevice() call[" + i + "] listener id : " + this._listeners[i].listenerId);

        try
        {
            this._listeners[i].discoveryCallback.ondeviceadded(device);
        }
        catch(e)
        {
            allshare_helper.log.error("exception during ondeviceadded callback");
            allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
        }
    }
    
    allshare_helper.log.trace("DeviceFinder._addedDevice() end");
}


window.webapis.allshare.serviceconnector._DeviceFinder._removedDevice = function(device)
{
    allshare_helper.log.trace("DeviceFinder._removedDevice() begin");
    allshare_helper.log.debug("DeviceFinder._removedDevice(device.name:" + device.name + ") begin");

    for (var i = 0; i < this._listeners.length; i++)
    {
        try
        {
            this._listeners[i].discoveryCallback.ondeviceremoved(device);
        }
        catch(e)
        {
            allshare_helper.log.error("exception during ondeviceremoved callback");
            allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
        }
    }

    allshare_helper.log.trace("DeviceFinder._removedDevice() end");
}


// prototype
// Device getDevice(in DeviceType deviceType,
//                          in DeviceId id) raises(WebAPIException);
window.webapis.allshare.serviceconnector._DeviceFinder.getDevice = function(deviceType, id)
{
    allshare_helper.log.trace("DeviceFinder.getDevice() begin");
    // id is UDN
    allshare_helper.log.debug("DeviceFinder.getDevice(deviceType:" + deviceType + ", id:" + id + ")");

    if(typeof deviceType != 'string')
    {
        allshare_helper.log.error("Type of deviceType is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of deviceType is not string");
    }

    if(deviceType != "MEDIAPROVIDER")
    {
        allshare_helper.log.print("Supported only deviceType == 'MEDIAPROVIDER'");
    }

    if(typeof id == 'object')
    {
        if (typeof id.ToString == 'function')
        {
            id = id.ToString();
        }
        else
        {
            allshare_helper.log.error("Type casting is not available for id argument");
            throw allshare_helper.createWebAPIException("InvalidValuesError", "Type casting is not available for id argument");
        }
    }

    if(typeof id != 'string')
    {
        allshare_helper.log.error("Type of id is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of id is not string");
    }


    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    } 

    var retValue = null;
    var json = window.webapis.allshare._plugin(allsharePlugin, "GetDeviceList", 0);    
    if(json && json != -1) // handle errors???
    {
        var obj = null;
        try
        {
            obj = eval("(" + json + ")");

            if(obj && obj.length)
            {
                for(var i=0; i<obj.length; i++)
                {
                    var device = allshare_helper.createDeviceFactory(obj[i]);
                    if (device && ((device.deviceType == deviceType) && (device.id == id)))
                    {
                        retValue = device;
                        break;
                    }
                }
            }
        }
        catch(e)
        {
            allshare_helper.log.error("eval failed");
            allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
        }

    }
    else
    {
        allshare_helper.log.error("allshare plugin return error from GetDeviceList()");
    }

    allshare_helper.log.trace("DeviceFinder.getDevice() end");

    return retValue;
}


// prototype
//  DeviceArray getDeviceList(in DeviceType deviceType) raises(WebAPIException)
window.webapis.allshare.serviceconnector._DeviceFinder.getDeviceList = function(deviceType)
{
    allshare_helper.log.trace("DeviceFinder.getDeviceList() begin");
    allshare_helper.log.debug("DeviceFinder.getDeviceList(deviceType:" + deviceType + ")");

    if(typeof deviceType != 'string')
    {
        allshare_helper.log.error("Type of deviceType is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of deviceType is not string");
    }

    if(deviceType != "MEDIAPROVIDER")
    {
        allshare_helper.log.print("Supported only deviceType == 'MEDIAPROVIDER'");
    }


    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    } 

    var retValue = [];
    var json = window.webapis.allshare._plugin(allsharePlugin, "GetDeviceList", 0);    
    if(json && json != -1) // handle errors???
    {
        var obj = null;
        try
        {
            obj = eval("(" + json + ")");

            if(obj && obj.length)
            {
                for(var i=0; i<obj.length; i++)
                {
                    var device = allshare_helper.createDeviceFactory(obj[i]);
                    if (device && (device.deviceType == deviceType))
                    {
                        retValue.push(device);
                    }
                }
            }
        }
        catch(e)
        {
            allshare_helper.log.error("eval failed");
            allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
        }
    }
    else
    {
        allshare_helper.log.error("allshare plugin return error from GetDeviceList()");
    }

    allshare_helper.log.trace("DeviceFinder.getDeviceList() end");

    return retValue;
}


// prototype
// DeviceArray getDeviceListByDomain(in DeviceType deviceType,
//                               in DeviceDomain domain) raises(WebAPIException);
window.webapis.allshare.serviceconnector._DeviceFinder.getDeviceListByDomain = function(deviceType, domain)
{
    allshare_helper.log.trace("DeviceFinder.getDeviceListByDomain() begin");
    allshare_helper.log.debug("DeviceFinder.getDeviceListByDomain(deviceType:" + deviceType + ", domain: " + domain + ")");

    if(typeof deviceType != 'string')
    {
        allshare_helper.log.error("Type of deviceType is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of deviceType is not string");
    }

    if(deviceType != "MEDIAPROVIDER")
    {
        allshare_helper.log.print("Supported only deviceType == 'MEDIAPROVIDER'");
    }

    if(typeof domain != 'string')
    {
        allshare_helper.log.error("Type of domain is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of domain is not string");
    }


    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    } 

    var retValue = [];
    var json = window.webapis.allshare._plugin(allsharePlugin, "GetDeviceList", 0);    
    if(json && json != -1) // handle errors???
    {
        var obj = null;
        try
        {
            obj = eval("(" + json + ")");

            if(obj && obj.length)
            {
                for(var i=0; i<obj.length; i++)
                {
                    var device = allshare_helper.createDeviceFactory(obj[i]);
                    if (device && (device.deviceDomain == domain))
                    {
                        retValue.push(device);
                    }
                }
            }
        }
        catch(e)
        {
            allshare_helper.log.error("eval failed");
            allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
        }
    }
    else
    {
        allshare_helper.log.error("allshare plugin return error from GetDeviceList()");
    }

    allshare_helper.log.trace("DeviceFinder.getDeviceListByDomain() end");

    return retValue;
}


// prototype
// DeviceArray getDeviceListByNIC(in DeviceType deviceType, in DOMString nic) raises(WebAPIException);
window.webapis.allshare.serviceconnector._DeviceFinder.getDeviceListByNIC = function(deviceType, nic)
{
    allshare_helper.log.trace("DeviceFinder.getDeviceListByNIC() begin");
    allshare_helper.log.debug("DeviceFinder.getDeviceListByNIC(deviceType:" + deviceType + ", nic: " + nic + ")");

    if(typeof deviceType != 'string')
    {
        allshare_helper.log.error("Type of deviceType is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of deviceType is not string");
    }

    if(deviceType != "MEDIAPROVIDER")
    {
        allshare_helper.log.print("Supported only deviceType == 'MEDIAPROVIDER'");
    }

    if(typeof nic != 'string')
    {
        allshare_helper.log.error("Type of nic is not string");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of nic is not string");
    }


    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    } 

    var retValue = [];
    var json = window.webapis.allshare._plugin(allsharePlugin, "GetDeviceList", 0);    
    if(json && json != -1) // handle errors???
    {
        var obj = null;
        try
        {
            obj = eval("(" + json + ")");

            if(obj && obj.length)
            {
                for(var i=0; i<obj.length; i++)
                {
                    var device = allshare_helper.createDeviceFactory(obj[i]);
                    if (device && (device.nic == nic))
                    {
                        retValue.push(device);
                    }
                }
            }
        }
        catch(e)
        {
            allshare_helper.log.error("eval failed");
            allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
        }
    }
    else
    {
        allshare_helper.log.error("allshare plugin return error from GetDeviceList()");
    }

    allshare_helper.log.trace("DeviceFinder.getDeviceListByNIC() end");

    return retValue;
}


// prototype
// void refresh() raises(WebAPIException);
window.webapis.allshare.serviceconnector._DeviceFinder.refresh = function()
{
    allshare_helper.log.trace("DeviceFinder.refresh()");

    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    }

    // return boolean type, result ignored
    window.webapis.allshare._plugin(allsharePlugin, "RefreshDMS");    
}


// prototype
// long addDeviceDiscoveryListener(in DeviceDiscoveryCallback deviceDiscoveryCallback) raises(WebAPIException);
window.webapis.allshare.serviceconnector._DeviceFinder.addDeviceDiscoveryListener = function(deviceDiscoveryCallback)
{
    allshare_helper.log.trace("DeviceFinder.addDeviceDiscoveryListener() begin");

    // check argument
    if (deviceDiscoveryCallback == null || (typeof deviceDiscoveryCallback != 'object'))
    {
        allshare_helper.log.error("Type check for deviceDiscoveryCallback is failed: (deviceDiscoveryCallback == null || (typeof deviceDiscoveryCallback != 'object')");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type check for deviceDiscoveryCallback is failed: (deviceDiscoveryCallback == null || (typeof deviceDiscoveryCallback != 'object')");
    }

    if (typeof deviceDiscoveryCallback.ondeviceadded != 'function')
    {
        allshare_helper.log.error("deviceDiscoveryCallback.ondeviceadded != 'function'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "deviceDiscoveryCallback.ondeviceadded != 'function'");
    }

    if (typeof deviceDiscoveryCallback.ondeviceremoved != 'function')
    {
        allshare_helper.log.error("typeof deviceDiscoveryCallback.ondeviceremoved != 'function'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof deviceDiscoveryCallback.ondeviceremoved != 'function'");
    }    

    // processing
    this._listenersCounter++;
    this._listeners.push(new this._ListenerInfo(deviceDiscoveryCallback, this._listenersCounter));
    allshare_helper.log.debug("DeviceFinder.addDeviceDiscoveryListener() added listenersId:" + this._listenersCounter);

    allshare_helper.log.trace("DeviceFinder.addDeviceDiscoveryListener() end");
    
    return this._listenersCounter;
}


// prototype
// void removeDeviceDiscoveryListener(in long deviceDiscoveryListener) raises(WebAPIException);
window.webapis.allshare.serviceconnector._DeviceFinder.removeDeviceDiscoveryListener = function(deviceDiscoveryListenerId)
{
    allshare_helper.log.trace("DeviceFinder.removeDeviceDiscoveryListener() begin");

    // check argument
    if (deviceDiscoveryListenerId == null)
    {
        allshare_helper.log.error("deviceDiscoveryListenerId == null");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "deviceDiscoveryListenerId == null");
    }    

    if (typeof deviceDiscoveryListenerId == 'boolean')
    {
        allshare_helper.log.error("typeof deviceDiscoveryListenerId == 'boolean'");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "typeof deviceDiscoveryListenerId != 'boolean'");
    }    

    if (typeof deviceDiscoveryListenerId != 'number')
    {
        allshare_helper.log.error("typeof deviceDiscoveryListenerId != 'number'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof deviceDiscoveryListenerId != 'number'");
    }    

    // processing
    var fRemoved = false;
    for (var i = 0; i < this._listeners.length; i++)
    {
        if (this._listeners[i].listenerId == deviceDiscoveryListenerId)
        {
            // remove element from array
            allshare_helper.log.debug("DeviceFinder.removeDeviceDiscoveryListener() remove listenerId:" + this._listeners[i].listenerId);
            this._listeners.splice(i, 1);
            fRemoved = true;
            break;
        }
    }

    if (! fRemoved)
    {
        // no found deviceDiscoveryListener - 'id'
        allshare_helper.log.error("deviceDiscoveryListenerId:" + deviceDiscoveryListenerId + " unknown");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "deviceDiscoveryListenerId:" + deviceDiscoveryListenerId + " unknown");
    }

    allshare_helper.log.trace("DeviceFinder.removeDeviceDiscoveryListener() end");
}


 
//=============================================================================
// 
//=============================================================================

//=============================================================================
// AbstractFilter
window.webapis.AbstractFilter = function()
{}


//=============================================================================
// [Constructor(CompositeFilterType type, optional AbstractFilter[] filters)]
window.webapis.CompositeFilter = function(type, filters)
{
    this.setType = function(type)
    {
        if(typeof type != 'string')
        {
            allshare_helper.log.error("Type of 'type' argument is not string");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of 'type' argument is not string");
        }

        switch(type)
        {
            case "UNION": break;
            case "INTERSECTION": break;
            default: 
                allshare_helper.log.error("Only 'UNION' and 'INTERSECTION' values of 'type' argument is supported");
                throw allshare_helper.createWebAPIException("TypeMismatchError", "Only 'UNION' and 'INTERSECTION' values of 'type' argument is supported");
                break;
        }
        this.type_ = type;
    }

    // attribute CompositeFilterType type;
    this.__defineGetter__("type", function(){ return this.type_; });
    this.__defineSetter__("type", function(type){ this.setType(type); });

    this.setType(type);

    this.filters = filters; // could be null or array
}


//=============================================================================

// [Constructor(DOMString attributeName, optional FilterMatchFlag matchFlag, optional any matchValue)]
window.webapis.AttributeFilter = function(attributeName, matchFlag, matchValue)
{
    this.setAttributeName = function(attributeName)
    {
        if(typeof attributeName != 'string')
        {
            allshare_helper.log.error("Type of 'attributeName' argument is not string");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of 'attributeName' argument is not string");
        }

/*
        switch(attributeName)
        {
            case "albumTitle": break;
            case "artist": break;
            case "date": break;
            case "duration": break;
            case "extension": break;
            case "fileSize": break;
            case "genre": break;
            case "location": break;
            case "mimeType": break;
            case "width": break;
            case "height": break;
            case "subtitleUri": break;
            case "thumbnailUri": break;
            case "title": break;
            case "itemType": break;
            case "itemUri": break;
            case "isRootFolder": break;
            case "contentBuildType": break;

            default: 
                allshare_helper.log.error("Value of 'atributeName' argument is not supported");
                throw allshare_helper.createWebAPIException("TypeMismatchError", "Value of 'atributeName' argument is not supported");
                break;
        }
*/
        this.attributeName_ = attributeName;
    }

  
    // attribute DOMString attributeName;
    this.__defineGetter__("attributeName", function(){ return this.attributeName_; });
    this.__defineSetter__("attributeName", function(attributeName){ this.setAttributeName(attributeName); });


    this.setMatchFlag = function(matchFlag)
    {
        if (matchFlag)
        {
            if(typeof matchFlag != 'string')
            {
                allshare_helper.log.error("Type of 'matchFlag' argument is not string");
                throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of 'matchFlag' argument is not string");
            }

            switch(matchFlag)
            {
                case "EXACTLY": break;
                case "FULLSTRING": break;
                case "CONTAINS": break;
                case "STARTSWITH": break;
                case "ENDSWITH": break;
                case "EXISTS": break;

                default: 
                    allshare_helper.log.error("Value of 'matchFlag' argument is not supported");
                    throw allshare_helper.createWebAPIException("TypeMismatchError", "Value of 'matchFlag' argument is not supported");
                    break;
            }
        }
        else
        {
            matchFlag = "EXACTLY";
        }

        this.matchFlag_ = matchFlag;
    }
    
    // attribute FilterMatchFlag matchFlag setraises(WebAPIException);
    this.__defineGetter__("matchFlag", function(){ return this.matchFlag_; });
    this.__defineSetter__("matchFlag", function(matchFlag){ this.setMatchFlag(matchFlag); });


    this.setAttributeName(attributeName);     
    this.setMatchFlag(matchFlag);

    // attribute any matchValue;
    this.matchValue = matchValue;
}


//=============================================================================

// [Constructor(DOMString attributeName, optional any initialValue, optional any endValue)]
window.webapis.AttributeRangeFilter = function(attributeName, initialValue, endValue)
{
    this.setAttributeName = function(attributeName)
    {
        if(typeof attributeName != 'string')
        {
            allshare_helper.log.error("Type of 'attributeName' argument is not string");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of 'attributeName' argument is not string");
        }

        this.attributeName_ = attributeName;
    }


    // attribute DOMString attributeName;
    this.__defineGetter__("attributeName", function(){ return this.attributeName_; });
    this.__defineSetter__("attributeName", function(attributeName){ this.setAttributeName(attributeName); });

    this.setAttributeName(attributeName); 

    // attribute any initialValue;
    this.initialValue = initialValue;

    // attribute any endValue;
    this.endValue = endValue;
    
}


 //=============================================================================

// [Constructor(DOMString attributeName, optional SortModeOrder order)] 
window.webapis.SortMode = function(attributeName, order)
{
    this.setAttributeName = function(attributeName)
    {
        if(typeof attributeName != 'string')
        {
            allshare_helper.log.error("Type of 'attributeName' argument is not string");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of 'attributeName' argument is not string");
        }

        this.attributeName_ = attributeName;
    }


    this.setOrder = function(order)
    {
        if (order != null)
        {
            if(typeof order != 'string')
            {
                allshare_helper.log.error("Type of 'order' argument is not string");
                throw allshare_helper.createWebAPIException("TypeMismatchError", "Type of 'order' argument is not string");
            }

            switch(order)
            {
                case "ASC": break;
                case "DESC": break;

                default: 
                    allshare_helper.log.error("Value of 'order' argument is not supported, expected ASC or DESC");
                    throw allshare_helper.createWebAPIException("TypeMismatchError", "Value of 'order' argument is not supported, expected ASC or DESC");
                    break;
            }
        }
        else
        {
            order = "ASC";
        }
        
        this.order_ = order;
    }    


    // attribute DOMString attributeName;
    this.__defineGetter__("attributeName", function(){ return this.attributeName_; });
    this.__defineSetter__("attributeName", function(attributeName){ this.setAttributeName(attributeName); });


    // attribute SortModeOrder order setraises(WebAPIException);
    this.__defineGetter__("order", function(){ return this.order_; });
    this.__defineSetter__("order", function(order){ this.setOrder(order); });


    this.setAttributeName(attributeName);
    this.setOrder(order);

}


// return boolean 
allshare_helper.doCompositeFiltering = function(item, filter)
{
    allshare_helper.log.trace("doCompositeFiltering()");

    if (filter.filters)
    {
        for (var i = 0; i < filter.filters.length; i++)
        {
            var result = allshare_helper.doFiltering(item, filter.filters[i]);

            if (filter.type == "UNION")
            {
                if (result)
                    return true;
            }
            else
            if (filter.type == "INTERSECTION")
            {
                if (! result)
                    return false;
            }
            else
            {
                allshare_helper.log.error("Value of CompositeFilter 'type' is not supported, expected UNION or INTERSECTION");
            }
        }

        if (filter.filters.length > 0)
        {
            if (filter.type == "UNION")
            {
                return false;
            }
            else
            if (filter.type == "INTERSECTION")
            {
                return true;
            }
            else
            {
                allshare_helper.log.error("Value of CompositeFilter 'type' is not supported, expected UNION or INTERSECTION");
            }
        }
    }
    return true;
}


// return boolean 
allshare_helper.doAttributeFiltering = function(item, filter)
{
    allshare_helper.log.trace("doAttributeFiltering()");

    var itemAttrValue = item[filter.attributeName];
    switch(filter.matchFlag)
    {
        case "EXACTLY":
            if (itemAttrValue === filter.matchValue)
                return true;
            else
                return false;
        break;

        case "FULLSTRING":
            if (typeof itemAttrValue == 'string' && typeof filter.matchValue == 'string')
            {
                if (itemAttrValue.toLowerCase() === filter.matchValue.toLowerCase())
                    return true;
            }
            return false;
        break;

        case "CONTAINS":
            if (typeof itemAttrValue == 'string' && typeof filter.matchValue == 'string')
            {
                if (itemAttrValue.toLowerCase().indexOf(filter.matchValue.toLowerCase()) >= 0)
                    return true;
            }
            return false;
        break;

        case "STARTSWITH":
            if (typeof itemAttrValue == 'string' && typeof filter.matchValue == 'string')
            {
                if (itemAttrValue.toLowerCase().indexOf(filter.matchValue.toLowerCase()) == 0)
                    return true;
            }
            return false;
        break;

        case "ENDSWITH":
            if (typeof itemAttrValue == 'string' && typeof filter.matchValue == 'string')
            {
                var str1 = itemAttrValue.toLowerCase();
                var str2 = filter.matchValue.toLowerCase();
                if (str1.lastIndexOf(str2) == -1)
                    return false;
                if (str1.lastIndexOf(str2) + str2.length == str1.length)
                    return true;
            }
            return false;
        break;

        case "EXISTS":
            if (itemAttrValue != null)
                return true
        break;

        default:
            allshare_helper.log.error("Value of AttributeFilter 'type' is not supported, expected 'EXACTLY', 'FULLSTRING', 'CONTAINS', 'STARTSWITH', 'ENDSWITH', 'EXISTS'");
            break;

    }

    return false;
}


// return boolean 
allshare_helper.doAttributeRangeFiltering = function(item, filter)
{
    allshare_helper.log.trace("doAttributeRangeFiltering()");

    var itemAttrValue = item[filter.attributeName];
    if (itemAttrValue)
    {
        if (filter.initialValue)
        {
            if (typeof itemAttrValue == 'number' && typeof filter.initialValue == 'number')
            {
                if (filter.initialValue > itemAttrValue)
                    return false;
            }
            else
            if (itemAttrValue instanceof Date && filter.initialValue instanceof Date)
            {
                if (filter.initialValue.valueOf() > itemAttrValue.valueOf())
                    return false;
            }
            else
            {
                allshare_helper.log.error("AttributeRangeFilter supported only number and Date attribute");
                return false;
            }
        }

        if (filter.endValue)
        {
            if (typeof itemAttrValue == 'number' && typeof filter.endValue == 'number')
            {
                if (filter.endValue <= itemAttrValue)
                    return false;
            }
            else
            if (itemAttrValue instanceof Date && filter.endValue instanceof Date)
            {
                if (filter.endValue.valueOf() > itemAttrValue.valueOf())
                    return false;
            }
            else
            {
                allshare_helper.log.error("AttributeRangeFilter supported only number and Date attribute");
                return false;
            }
        }
        return true;
    }

    return false;
}


// return boolean 
allshare_helper.doFiltering = function(item, filter)
{
    allshare_helper.log.trace("doFiltering()");

    if (filter == null)
    {
    }
    else
    if (filter instanceof window.webapis.CompositeFilter)
    {
        return allshare_helper.doCompositeFiltering(item, filter);
    }
    else
    if (filter instanceof window.webapis.AttributeFilter)
    {
        return allshare_helper.doAttributeFiltering(item, filter);
    }
    else
    if (filter instanceof window.webapis.AttributeRangeFilter)
    {
        return allshare_helper.doAttributeRangeFiltering(item, filter);
    }
    else
    if (filter instanceof window.webapis.AbstractFilter)
    {
    }
    else
    {
        allshare_helper.log.error("Find filter of unknown type - ignored");
    }

    return true;
}


//=============================================================================
// function Icon
window.webapis.allshare.Icon = function()
{
}


allshare_helper.initIcon = function(iconJson, icon)
{
    allshare_helper.log.trace("initIcon() begin");

    if (iconJson == null)
    {
        allshare_helper.log.debug("iconJson == null");
        return null;
    }

    if (icon == null)
    {
        allshare_helper.log.debug("icon == null");
        return null;
    }

    // readonly attribute long depth;
    var depth = 0;
    if (typeof iconJson.IconDepth == 'number')
    {
        depth = iconJson.IconDepth;
    }
    allshare_helper.setReadOnlyAttribute(icon, "depth", depth);
    allshare_helper.log.info("added icon.depth:" + depth);


    // readonly attribute long height;
    var height = 0;
    if (typeof iconJson.IconHeight == 'number')
    {
        height = iconJson.IconHeight;
    }
    allshare_helper.setReadOnlyAttribute(icon, "height", height);
    allshare_helper.log.info("added icon.height:" + height);


    // readonly attribute long width;
    var width = 0;
    if (typeof iconJson.IconWidth == 'number')
    {
        width = iconJson.IconWidth;
    }
    allshare_helper.setReadOnlyAttribute(icon, "width", width);
    allshare_helper.log.info("added icon.width:" + width);


    // readonly attribute DOMString mimeType;
    var mimeType = "";
    if (typeof iconJson.IconMimeType == 'string')
    {
        mimeType = iconJson.IconMimeType;
    }
    allshare_helper.setReadOnlyAttribute(icon, "mimeType", mimeType);
    allshare_helper.log.info("added icon.mimeType:" + mimeType);


    // readonly attribute DOMString iconUri;
    var iconUri = "";
    if (typeof iconJson.IconURL == 'string')
    {
        iconUri = iconJson.IconURL;
    }
    allshare_helper.setReadOnlyAttribute(icon, "iconUri", iconUri);
    allshare_helper.log.info("added icon.iconUri:" + iconUri);

    allshare_helper.log.trace("initIcon() end");

    return icon;
}

 
//=============================================================================
// Device 
window.webapis.allshare.Device = function()
{
}


allshare_helper.initDevice = function(deviceJson, deviceType, device)
{
    allshare_helper.log.trace("initDevice() begin");

    if (deviceJson == null)
    {
        allshare_helper.log.debug("deviceJson == null");
        return null;
    }

    if (device == null)
    {
        allshare_helper.log.debug("device == null");
        return null;
    }


    //readonly attribute DeviceId id;  // string
    var id = null;
    if (typeof deviceJson.deviceID == 'number')
    {
        device._deviceID = deviceJson.deviceID; // keeps deviceID in form of below level
        allshare_helper.log.info("added device._deviceID:" + device._deviceID);
        id = deviceJson.UDN || ("" + deviceJson.deviceID); //'id' should be UDN
    }
    allshare_helper.setReadOnlyAttribute(device, "id", id);
    allshare_helper.log.info("added device.id:" + id);


    // readonly attribute DeviceDomain deviceDomain;
    var deviceDomain = "";
    if (typeof deviceJson.Domain == 'string')
    {
        deviceDomain = deviceJson.Domain;
    }
    allshare_helper.setReadOnlyAttribute(device, "deviceDomain", deviceDomain);
    allshare_helper.log.info("added device.deviceDomain:" + deviceDomain);


    //readonly attribute DeviceType deviceType;

    // plugin may return info only about MediaServers 
    // deviceType == 'AllShare' - samsung DMS devices
    // deviceType == 'Network' - other DMS devices
/*
    var deviceType = "UNKNOWN";
    if (typeof deviceJson.deviceType == 'string')
    {
        switch(deviceJson.deviceType)
        {
            case "AllShare": ;
            case "Network": deviceType = "MEDIAPROVIDER"; break;

            default: deviceType = "UNKNOWN"; break;
        }
    }
*/    
    allshare_helper.setReadOnlyAttribute(device, "deviceType", deviceType);
    allshare_helper.log.info("added device.deviceType:" + deviceType);


    // readonly attribute IconArray iconArray;
    var iconArray = [];
    if (deviceJson.Icons != null && (typeof deviceJson.Icons == 'object'))
    {
        for (var i = 0; i < deviceJson.Icons.length; i++)
        {
            var icon = allshare_helper.initIcon(deviceJson.Icons[i], new window.webapis.allshare.Icon);
            iconArray.push(icon);
        }
    }
    allshare_helper.setReadOnlyAttribute(device, "iconArray", iconArray);
    allshare_helper.log.info("added device.iconArray: number:" + iconArray.length);


    // readonly attribute DOMString ipAddress;
    var ipAddress = "";
    if (typeof deviceJson.IPAddress == 'string')
    {
        ipAddress = deviceJson.IPAddress;
    }
    allshare_helper.setReadOnlyAttribute(device, "ipAddress", ipAddress);
    allshare_helper.log.info("added device.ipAddress:" + ipAddress);


    // readonly attribute DOMString modelName;
    var modelName = "";
    if (typeof deviceJson.ModelName == 'string')
    {
        modelName = deviceJson.ModelName;
    }
    allshare_helper.setReadOnlyAttribute(device, "modelName", modelName);
    allshare_helper.log.info("added device.modelName:" + modelName);


    // readonly attribute DOMString name;
    var name = "";
    if (typeof deviceJson.deviceName == 'string')
    {
        name = deviceJson.deviceName;
    }
    allshare_helper.setReadOnlyAttribute(device, "name", name);
    allshare_helper.log.info("added device.name:" + name);


    // readonly attribute DOMString nic;
    var nic = deviceJson.nic;
    if (typeof nic != 'string')
    {
        nic = "";
    }
    allshare_helper.setReadOnlyAttribute(device, "nic", nic);
    allshare_helper.log.info("added device.nic:" + nic);


    allshare_helper.log.trace("initDevice() end");

    return device;
}


//=============================================================================


window.webapis.allshare.MediaProvider = function()
{
    allshare_helper.log.trace("function MediaProvider()");
    window.webapis.allshare.Device.call(this);
}


// ??? is this right hierarcy?
window.webapis.allshare.MediaProvider.prototype = new window.webapis.allshare.Device();
window.webapis.allshare.MediaProvider.prototype.constructor = window.webapis.allshare.MediaProvider;


allshare_helper.initMediaProvider = function(mediaProviderJson, mediaProvider)
{
    allshare_helper.log.trace("initMediaProvider() begin");

    if (mediaProviderJson == null)
    {
        allshare_helper.log.debug("mediaProviderJson == null");
        return null;
    }

    if (mediaProvider == null)
    {
        allshare_helper.log.debug("mediaProvider == null");
        return null;
    }

    var deviceType = "MEDIAPROVIDER";
    allshare_helper.initDevice(mediaProviderJson, deviceType, mediaProvider);
 
    allshare_helper.setReadOnlyAttribute(mediaProvider, "rootFolder", mediaProvider.getRootFolder());


    // readonly attribute boolean isSearchable;
    var isSearchable = false;
    if (typeof mediaProviderJson.isSearchable == 'boolean')
    {
        isSearchable = mediaProviderJson.isSearchable;
    }
    allshare_helper.setReadOnlyAttribute(mediaProvider, "isSearchable", isSearchable);
    allshare_helper.log.info("added mediaProvider.isSearchable:" + isSearchable);

    allshare_helper.log.trace("initMediaProvider() end");

    return mediaProvider;
}


allshare_helper.createDeviceFactory = function(deviceJson)
{
    allshare_helper.log.trace("allshare_helper.createDevices() begin");

    if (deviceJson == null)
    {
        allshare_helper.log.debug("deviceJson == null");
        return null;
    }


    // plugin may return info only about MediaServers 
    // deviceType == 'AllShare' - samsung DMS devices
    // deviceType == 'Network' - other DMS devices
    var deviceType = "UNKNOWN";
    if (typeof deviceJson.deviceType == 'string')
    {
        switch(deviceJson.deviceType)
        {
            case "AllShare": ;
            case "Network": deviceType = "MEDIAPROVIDER"; break;

            default: deviceType = "UNKNOWN"; break;
        }
    }
    
    var device = null;
    
    switch(deviceType)
    {
        case "MEDIAPROVIDER":
            device = allshare_helper.initMediaProvider(deviceJson, new window.webapis.allshare.MediaProvider);
        break;

        default:
        break;
    }

    allshare_helper.log.trace("allshare_helper.createDevices() end");

    return device;
}


window.webapis.allshare.MediaProvider.prototype.getMediaReceiver = function()
{
    allshare_helper.log.trace("MediaProvider.prototype.getMediaReceiver()");

    allshare_helper.log.error("This feature is not supported");
    throw allshare_helper.createWebAPIException("NotSupportedError", "This feature is not supported");
}


window.webapis.allshare.MediaProvider.prototype.getRootFolder = function()
{
    allshare_helper.log.trace("MediaProvider.prototype.getRootFolder() begin");
    allshare_helper.log.debug("MediaProvider.prototype.getRootFolder() for device id:" + this._deviceID);

    var retValue = null;
    var json = window.webapis.allshare._plugin("AllShare", "GetRootFolder", this._deviceID, 0) || '';
    
    if(json)
    {
        var obj = null;
        try
        {
            obj = eval("(" + json + ")");

            obj.type = "folder";
            retValue = allshare_helper.initItem(obj, new window.webapis.allshare.Item(null, null, obj.title));
        }
        catch(e)
        {
            allshare_helper.log.error("eval failed");
            allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
        }
    }

    allshare_helper.log.trace("MediaProvider.prototype.getRootFolder() end");

    return retValue;
}


// prototype
// long addMediaProviderEventListener(MediaProviderEventCallback eventCallback)
window.webapis.allshare.MediaProvider.prototype.addMediaProviderEventListener = function(eventCallback)
{
    allshare_helper.log.trace("MediaProvider.prototype.addMediaProviderEventListener()");
    
    allshare_helper.log.nonimplemented("MediaProvider.prototype.addMediaProviderEventListener()");

    throw allshare_helper.createWebAPIException("NotSupportedError", "this feature is not supported");
    return 0;
}


// prototype
// void removeMediaProviderEventListener(long eventCallbackId)
window.webapis.allshare.MediaProvider.prototype.removeMediaProviderEventListener = function(eventCallbackId)
{
    allshare_helper.log.trace("MediaProvider.prototype.removeMediaProviderEventListener()");

    allshare_helper.log.nonimplemented("MediaProvider.prototype.removeMediaProviderEventListener()");

    if (typeof eventCallbackId != 'number')
    {
        allshare_helper.log.error("typeof eventCallbackId != 'number'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof eventCallbackId != 'number'");
    }

    throw allshare_helper.createWebAPIException("NotSupportedError", "this feature is not supported");
}


// prototype
// void browse(Item folderItem, unsigned long startIndex, unsigned long requestCount,
//                      MediaProviderSuccessCallback browseCallback, MediaProviderErrorCallback errorCallback,
//                      AbstractFilter browseFilter, SortMode sortMode)
window.webapis.allshare.MediaProvider.prototype.browse = function(parentFolderItem, startIndex, requestCount, successCallback, errorCallback, browseFilter, sortMode)
{
    allshare_helper.log.trace("MediaProvider.prototype.browse() begin");
    allshare_helper.log.debug("  startIndex:" + startIndex + "   requestCount:" + requestCount);
    
    // check parentFolderItem
    if (parentFolderItem == null || (typeof parentFolderItem != 'object'))
    {
        allshare_helper.log.error("Type check for parentFolderItem failed: (parentFolderItem == null || (typeof parentFolderItem != 'object'))");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "Type check for parentFolderItem failed: (parentFolderItem == null || (typeof parentFolderItem != 'object'))");
    }
    

    // check startIndex
    if (typeof startIndex != 'number')
    {
        allshare_helper.log.error("typeof startIndex != 'number'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof startIndex != 'number'");
    }

    if (startIndex < 0)
    {
        allshare_helper.log.error("startIndex is out of range.");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "startIndex is out of range.");
    }

    
    // check requestCount
    if (typeof requestCount != 'number')
    {
        allshare_helper.log.error("typeof requestCount != 'number'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof requestCount != 'number'");
    }

    if (requestCount < 0)
    {
        allshare_helper.log.error("requestCount is out of range.");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "requestCount is out of range.");
    }


    // check successCallback
    if (typeof successCallback != 'function')
    {
        allshare_helper.log.error("typeof successCallback != 'function'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof successCallback != 'function'");
    }

    
    // check errorCallback
    if (typeof errorCallback != 'function')
    {
        allshare_helper.log.error("typeof errorCallback != 'function'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof errorCallback != 'function'");
    }


    // check searchFilter
    if (browseFilter != null)
    {
        if (typeof browseFilter != 'object')
        {
            allshare_helper.log.error("typeof browseFilter != 'object'");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof browseFilter != 'object'");
        }
        else
        {
            allshare_helper.log.error("browseFilter parameter is not supported - ignored");
//            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof errorCallback != 'function'");
        }
    }

    // check sortMode
    if (sortMode != null)
    {
        if (typeof sortMode != 'object')
        {
            allshare_helper.log.error("typeof sortMode != 'object'");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof sortMode != 'object'");
        }

        if ((sortMode.attributeName == null) ||(sortMode.order == null))
        {
            allshare_helper.log.error("Contents of sortMode is wrong");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "Contents of sortMode is wrong");
        }
    }


    var deviceId = this._deviceID;
    if (deviceId == null)
    {
        allshare_helper.log.error("can't found this._deviceID");
        throw allshare_helper.createWebAPIException("InvalidStateError", "can't found this._deviceID");
    }

    
    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    } 

    var requestInfo = allshare_helper.RequestsHelper.registerRequest();
    if (requestInfo == null)
    {
        allshare_helper.log.error("Internal error SEF: request in processing already.");
        throw allshare_helper.createWebAPIException("UnknownError", "Internal error SEF: request in processing already.");
    }

    requestInfo.description = "BrowseItems request info";
    requestInfo.parentFolderItem = parentFolderItem;
    requestInfo.startIndex = startIndex;
    requestInfo.requestCount = requestCount;
    requestInfo.successCallback = successCallback;
    requestInfo.errorCallback = errorCallback;
    requestInfo.browseFilter = browseFilter;
    requestInfo.sortMode = sortMode;
    requestInfo.provider = this;
    requestInfo.callbackHandlers = window.webapis.allshare.MediaProvider.BrowseCallbackHandlers;

    // no support for sort mode ???
    // 'BrowseItems2' new API for 2013 with support of requestID, EndOfItems, sortMode

    // no arguments description
    // such "a" - ??

    var result;

    if (sortMode)
    {
        // ???
        result = window.webapis.allshare._plugin(allsharePlugin, "BrowseItemsTitleSort", "a", deviceId, parentFolderItem._objectID, startIndex, requestCount, requestInfo.id);
    }
    else
    {
        result = window.webapis.allshare._plugin(allsharePlugin, "BrowseItems2", "a", deviceId, parentFolderItem._objectID, startIndex, requestCount, requestInfo.id);
    }

    if (! result)
    {
        // if error, clean requestinfo
        allshare_helper.RequestsHelper.removeRequest(requestInfo.id);
    }

    allshare_helper.log.trace("MediaProvider.prototype.browse() end");
}



//=============================================================================
// 
//=============================================================================


// prototype
// void search(DOMString keyword, unsigned long startIndex, unsigned long requestCount,
//                    MediaProviderSuccessCallback successCallback, MediaProviderErrorCallback errorCallback,
//                    AbstractFilter searchFilter)
window.webapis.allshare.MediaProvider.prototype.search = function(keyword, startIndex, requestCount, successCallback, errorCallback, searchFilter)
{
    allshare_helper.log.trace("MediaProvider.prototype.search() begin");
    allshare_helper.log.debug("  keyword:" + keyword + "   startIndex:" + startIndex + "   requestCount:" + requestCount);

    // check keyword
    if (typeof keyword != 'string')
    {
        allshare_helper.log.error("typeof keyword != 'string'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof keyword != 'string'");
    }


    // check startIndex
    if (typeof startIndex != 'number')
    {
        allshare_helper.log.error("typeof startIndex != 'number'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof startIndex != 'number'");
    }

    if (startIndex < 0)
    {
        allshare_helper.log.error("startIndex is out of range.");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "startIndex is out of range.");
    }

    
    // check requestCount
    if (typeof requestCount != 'number')
    {
        allshare_helper.log.error("typeof requestCount != 'number'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof requestCount != 'number'");
    }

    if (requestCount < 0)
    {
        allshare_helper.log.error("requestCount is out of range.");
        throw allshare_helper.createWebAPIException("InvalidValuesError", "requestCount is out of range.");
    }

    
    // check successCallback
    if (typeof successCallback != 'function')
    {
        allshare_helper.log.error("typeof successCallback != 'function'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof successCallback != 'function'");
    }


    // check errorCallback
    if (typeof errorCallback != 'function')
    {
        allshare_helper.log.error("typeof errorCallback != 'function'");
        throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof errorCallback != 'function'");
    }

    // check searchFilter
    if (searchFilter != null)
    {
        if (typeof searchFilter != 'object')
        {
            allshare_helper.log.error("typeof searchFilter != 'object'");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof searchFilter != 'object'");
        }
        else
        {
            allshare_helper.log.error("searchFilter parameter is not supported - ignored");
//            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof errorCallback != 'function'");
        }
    }

    var deviceId = this._deviceID;
    if (deviceId == null)
    {
        allshare_helper.log.error("can't found this._deviceID");
        throw allshare_helper.createWebAPIException("InvalidStateError", "can't found this._deviceID");
    }

    var allsharePlugin = window.webapis.allshare._plugin("AllShare");
    if(!allsharePlugin)
    {
        allshare_helper.log.error("AllShare plugin is not exist.");
        throw allshare_helper.createWebAPIException("InvalidStateError", "AllShare plugin is not exist.");
    } 
    
    var requestInfo = allshare_helper.RequestsHelper.registerRequest();
    if (requestInfo == null)
    {
        allshare_helper.log.error("Internal error SEF: request in processing already.");
        throw allshare_helper.createWebAPIException("UnknownError", "Internal error SEF: request in processing already.");
    }

    requestInfo.description = "SearchItems request info";
    requestInfo.keyword = keyword;
    requestInfo.startIndex = startIndex;
    requestInfo.requestCount = requestCount;
    requestInfo.successCallback = successCallback;
    requestInfo.errorCallback = errorCallback;
    requestInfo.searchFilter = searchFilter;
    requestInfo.provider = this;
    requestInfo.callbackHandlers = window.webapis.allshare.MediaProvider.SearchCallbackHandlers;
    
    // 'SearchItems2' new API for 2013 with support of requestID, EndOfItems
    
    // no arguments description
    // such 3 - ??,  "a" - ??
    var result = window.webapis.allshare._plugin(allsharePlugin, "SearchItems2", "a", deviceId, keyword, 3, startIndex, requestCount, requestInfo.id);

    if (! result)
    {
        // if error, clean requestinfo
        allshare_helper.RequestsHelper.removeRequest(requestInfo.id);
    }
    
    allshare_helper.log.trace("MediaProvider.prototype.search() end");
}


//=============================================================================
// BrowseCallbackHandlers
//=============================================================================
window.webapis.allshare.MediaProvider.BrowseCallbackHandlers = {}


window.webapis.allshare.MediaProvider.BrowseCallbackHandlers.successCallback = function(requestInfo, obj)
{
    allshare_helper.log.trace("BrowseCallbackHandlers.successCallback() begin");

    var itemList = [];
    var EndOfItem = true;

    try
    {
        allshare_helper.log.debug("obj.deviceID:" + obj.deviceID);
    //            obj.deviceID = ;

        allshare_helper.log.debug("obj.EndOfItem:" + obj.EndOfItem);
        if (typeof obj.EndOfItem == 'boolean')
        {
            EndOfItem = obj.EndOfItem;
        }

        if(obj.Items && (obj.Items.length > 0))
        {
            for(var i=0; i<obj.Items.length; i++)
            {
                var item = allshare_helper.initItem(obj.Items[i], new window.webapis.allshare.Item);
                if (allshare_helper.doFiltering(item, requestInfo.browseFilter))
                {
                    itemList.push(item);
                }
            }
        }

        requestInfo.successCallback(itemList, EndOfItem, requestInfo.provider.id);
    }
    catch(e)
    {
        allshare_helper.log.error("exception inside browse success callback handler");
        allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
    }
    
    allshare_helper.log.trace("BrowseCallbackHandlers.successCallback() end");

}


window.webapis.allshare.MediaProvider.BrowseCallbackHandlers.errorCallback = function(requestInfo, webAPIError)
{
    allshare_helper.log.trace("BrowseCallbackHandlers.errorCallback()");

    try
    {
        requestInfo.errorCallback(webAPIError, requestInfo.provider.id);
    }
    catch(e)
    {
        allshare_helper.log.error("exception during browse error callback");
        allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
    }
    
//    allshare_helper.RequestsHelper.removeRequest(requestInfo.id); // located in _onAllsharePluginEvent()
}


//=============================================================================
// SearchCallbackHandlers
//=============================================================================

 
window.webapis.allshare.MediaProvider.SearchCallbackHandlers = {}


window.webapis.allshare.MediaProvider.SearchCallbackHandlers.successCallback = function(requestInfo, obj)
{
    allshare_helper.log.trace("SearchCallbackHandlers.successCallback() begin");
    
    var itemList = [];
    var EndOfItem = true;
    
    try
    {
        allshare_helper.log.debug("obj.deviceID:" + obj.deviceID);
//            obj.deviceID = ;

        allshare_helper.log.debug("obj.EndOfItem:" + obj.EndOfItem);
        if (typeof obj.EndOfItem == 'boolean')
        {
            EndOfItem = obj.EndOfItem;
        }

        if(obj.Items && (obj.Items.length > 0))
        {
            for(var i=0; i<obj.Items.length; i++)
            {
                var item = allshare_helper.initItem(obj.Items[i], new window.webapis.allshare.Item);
                if (allshare_helper.doFiltering(item, requestInfo.searchFilter))
                {
                    itemList.push(item);
                }
            }
        }

        requestInfo.successCallback(itemList, EndOfItem, requestInfo.provider.id);
    }
    catch(e)
    {
        allshare_helper.log.error("exception inside search callback handler");
        allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
    }

    allshare_helper.log.trace("SearchCallbackHandlers.successCallback() end");
}


window.webapis.allshare.MediaProvider.SearchCallbackHandlers.errorCallback = function(requestInfo, webAPIError)
{
    allshare_helper.log.trace("SearchCallbackHandlers.errorCallback()");
    
    try
    {
        requestInfo.errorCallback(webAPIError, requestInfo.provider.id);
    }
    catch(e)
    {
        allshare_helper.log.error("exception during search error callback");
        allshare_helper.log.error("e name:" + (e && (e.name || "")) + "   message:" + (e && (e.message || "")));
    }

//    allshare_helper.RequestsHelper.removeRequest(requestInfo.id); // located in _onAllsharePluginEvent()
}


//=============================================================================
// 
//=============================================================================
// prototype
// [Constructor(double latitude, double longitude)]
window.webapis.SimpleCoordinates = function(latitude_, longitude_)
{
    this.setLatitude(latitude_);
    this.setLongitude(longitude_);
}


window.webapis.SimpleCoordinates.prototype = 
{
    setLatitude : function(latitude_)
    {
        if (typeof latitude_ != 'number')
        {
            allshare_helper.log.error("typeof latitude != 'number'");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof latitude != 'number'");
        }
        this.latitude_ = latitude_;
    },
    setLongitude : function(longitude_)
    {
        if (typeof longitude_ != 'number')
        {
            allshare_helper.log.error("typeof longitude != 'number'");
            throw allshare_helper.createWebAPIException("TypeMismatchError", "typeof longitude != 'number'");
        }
        this.longitude_ = longitude_;
    },
    get latitude(){ return this.latitude_; },
    set latitude(latitude_){ this.setLatitude(latitude_); },
    get longitude(){ return this.longitude_; },
    set longitude(longitude_){ this.setLongitude(longitude_); }

};


//=============================================================================
// New declaration: Item
// Item
window.webapis.allshare.Item = function(uri, mimeType, title)
{
    allshare_helper.log.trace("Item() begin");

    allshare_helper.setReadOnlyAttribute(this, "itemUri", uri);
    allshare_helper.log.info("added item.itemUri:" + uri);

    allshare_helper.setReadOnlyAttribute(this, "mimeType", mimeType);
    allshare_helper.log.info("added item.mimeType:" + mimeType);

    allshare_helper.setReadOnlyAttribute(this, "title", title);
    allshare_helper.log.info("added item.title:" + title);


// attribute DOMString albumTitle
// attribute DOMString artist
// attribute Date date;
// attribute unsigned long long duration
// attribute DOMString extension
// attribute double fileSize; 
// attribute DOMString genre;
// attribute SimpleCoordinates? location;
// attribute DOMString mimeType;
// attribute unsigned long width; 
// attribute unsigned long height;
// attribute DOMString subtitleUri;
// attribute DOMString thumbnailUri;
// attribute DOMString title;
// attribute ItemType itemType;
// attribute DOMString itemUri;
// attribute boolean isRootFolder;
// attribute ItemContentBuildType contentBuildType;

    allshare_helper.log.trace("Item() end");
}


allshare_helper.initItem = function(itemJson, item)
{
    allshare_helper.log.trace("allshare_helper.initItem() begin");

    // attribute ItemType itemType;
    var itemTypeJson = itemJson.type; // 'folder'/'file'
    var itemType = null;
    switch(itemTypeJson)
    {
        case "folder": itemType = "FOLDER"; break;
        case "file":
        {
            var itemMediaTypeJson = itemJson.mediaType;
            switch(itemMediaTypeJson)
            {
                case "audio": itemType = "AUDIO"; break;
                case "video": itemType = "VIDEO"; break;
                case "image": itemType = "IMAGE"; break;
                default: itemType = "UNKNOWN"; break;
            }
        }
        break;
        default: itemType = "UNKNOWN"; break;
    }
    allshare_helper.setReadOnlyAttribute(item, "itemType", itemType);
    allshare_helper.log.info("added item.itemType:" + itemType);


    // attribute DOMString albumTitle
    var albumTitle = undefined;
    if (itemType == "AUDIO" || itemType == "VIDEO" || itemType == "IMAGE" || itemType == "UNKNOWN")
    {
        albumTitle = itemJson.album;
    }
    allshare_helper.setReadOnlyAttribute(item, "albumTitle", albumTitle);
    allshare_helper.log.info("added item.albumTitle:" + albumTitle);


    // attribute DOMString artist
    var artist = undefined;
    if (itemType == "AUDIO")
    {
        artist = null;
        if (typeof itemJson.artist == 'string')
        {
            artist = itemJson.artist;
        }
    }
    allshare_helper.setReadOnlyAttribute(item, "artist", artist);
    allshare_helper.log.info("added item.artist:" + artist);


    // attribute Date date;
    var date = undefined;
    if (itemType == "AUDIO" || itemType == "VIDEO" || itemType == "IMAGE" || itemType == "UNKNOWN")
    {
        date = itemJson.date;

        if (typeof date == 'string')
        {
            date = new Date(date);
        }
    }
    allshare_helper.setReadOnlyAttribute(item, "date", date);
    allshare_helper.log.info("added item.date:" + date);


    // attribute unsigned long long duration
    var duration = itemJson.totalplaytime;
    if (itemType != "AUDIO" || itemType != "VIDEO")
    {
        //convert from totalplaytime : "01:53:05" into seconds
        if (typeof duration == 'string')
        {
            duration = allshare_helper.convertStrDurationToSec(duration); // from string to number
        }
    }
    allshare_helper.setReadOnlyAttribute(item, "duration", duration);
    allshare_helper.log.info("added item.duration:" + duration);


    // attribute DOMString extension
    var extension = undefined;
    if (itemType == "AUDIO" || itemType == "VIDEO" || itemType == "IMAGE" || itemType == "UNKNOWN")
    {    
        extension = itemJson.extension;
    }
    allshare_helper.setReadOnlyAttribute(item, "extension", extension);
    allshare_helper.log.info("added item.extension:" + extension);


    // attribute double fileSize; 
    var fileSize = undefined;
    if (itemType == "AUDIO" || itemType == "VIDEO" || itemType == "IMAGE" || itemType == "UNKNOWN")
    {
        fileSize = itemJson.filesize;
    }
    allshare_helper.setReadOnlyAttribute(item, "fileSize", fileSize);
    allshare_helper.log.info("added item.fileSize:" + fileSize);


    // attribute DOMString genre;
    var genre = undefined;
    if (itemType == "AUDIO")
    {
        genre = itemJson.genre;
    }
    allshare_helper.setReadOnlyAttribute(item, "genre", genre);
    allshare_helper.log.info("added item.genre:" + genre);


    // attribute SimpleCoordinates location;
//    var location = itemJson.location; // ???
    var location = null; // not implemented
    if (itemType == "FOLDER")
    {
        location = undefined;
    }
    allshare_helper.setReadOnlyAttribute(item, "location", location);
    allshare_helper.log.info("added item.location:" + location);


    // attribute DOMString mimeType;
    var mimeType = itemJson.mimeType;
    allshare_helper.setReadOnlyAttribute(item, "mimeType", mimeType);
    allshare_helper.log.info("added item.mimeType:" + mimeType);


    // attribute unsigned long width;
    var width = undefined;
    if (itemType == "IMAGE" || itemType == "VIDEO")
    {
        width = itemJson.Width;
    }
    allshare_helper.setReadOnlyAttribute(item, "width", width);
    allshare_helper.log.info("added item.width:" + width);


    // attribute unsigned long height;
    var height = undefined;
    if (itemType == "IMAGE" || itemType == "VIDEO")
    {
        height = itemJson.Height;
    }
    allshare_helper.setReadOnlyAttribute(item, "height", height);
    allshare_helper.log.info("added item.height:" + height);


    // attribute DOMString subtitleUri;
    var subtitleUri = undefined;
    if (itemType == "VIDEO")
    {
        subtitleUri = null;
        var captions = itemJson.Captions;
        if (captions && captions.length)
        {
            subtitleUri = captions[0].subtitle;
        }
    }
    allshare_helper.setReadOnlyAttribute(item, "subtitleUri", subtitleUri);
    allshare_helper.log.info("added item.subtitleUri:" + subtitleUri);


    // attribute DOMString thumbnailUri;
    var thumbnailUri = itemJson.thumbnail;
    allshare_helper.setReadOnlyAttribute(item, "thumbnailUri", thumbnailUri);
    allshare_helper.log.info("added item.thumbnailUri:" + thumbnailUri);


    // attribute DOMString title;
    var title = itemJson.title;
    allshare_helper.setReadOnlyAttribute(item, "title", title);
    allshare_helper.log.info("added item.title:" + title);


    // attribute DOMString itemUri;
    var itemUri = itemJson.url;
    allshare_helper.setReadOnlyAttribute(item, "itemUri", itemUri);
    allshare_helper.log.info("added item.itemUri:" + itemUri);


    // attribute boolean isRootFolder;
    var objectIdJson = itemJson.objectID;
    item._objectID = objectIdJson;
    allshare_helper.log.info("added item._objectID:" + item._objectID);
    var isRootFolder = (objectIdJson == "0");
    allshare_helper.setReadOnlyAttribute(item, "isRootFolder", isRootFolder);
    allshare_helper.log.info("added item.isRootFolder:" + isRootFolder);


    // attribute ItemContentBuildType contentBuildType;
    var contentBuildType = itemJson.contentBuildType;
    if (typeof contentBuildType != 'string')
    {
        // LOCAL: the content is from the local device
        // MEDIAPROVIDER: the content is from a remote media provider
        // UNKNOWN: the content is from nowhere
        // WEB: the content is from internet
        contentBuildType = "UNKNOWN";
    }
    allshare_helper.setReadOnlyAttribute(item, "contentBuildType", contentBuildType);
    allshare_helper.log.info("added item.contentBuildType:" + contentBuildType);


    allshare_helper.log.trace("allshare_helper.initItem() end");

    return item;
}


//=============================================================================


window.webapis.allshare._onAllsharePluginEvent = function(event, data1, data2)
{
    allshare_helper.log.trace("window.webapis.allshare._onAllsharePluginEvent() begin");

    var EDMSCP_MSG_BROWSEFOLDER_RESULT     = 200;          // result of BrowseItem 
    var EDMSCP_MSG_SEARCH_RESULT         = 201;          // result of SearchItem
    var EDMSCP_MSG_GETITEMINFO_RESULT     = 212;

    // Device add/delete
    var DMSCP_UPNP_DEVICEADDED    = 0;
    var DMSCP_UPNP_DEVICEDELETE    = 1;

    // Error Event
    var EDMSCP_MSG_NO_DATA             = -9;
    var EDMSCP_MSG_CANCEL             = -8;
    var EDMSCP_MSG_UNKNOWN             = -1;
    var EDMSCP_MSG_INVALID_SVC        = -17;    // added [Not searchable error]


    allshare_helper.log.print("_onAllsharePluginEvent()");
    allshare_helper.log.print("Event : " + event);
    allshare_helper.log.print("typeof data1:" + (typeof data1));

    allshare_helper.log.print("BT_Retrun : " + data1 + " => " + data2);  // data1: object name  data2: JSON format

    // 'data1' - includes trash on 2012
    
    switch(event)
    {
        case EDMSCP_MSG_BROWSEFOLDER_RESULT:
            var obj = null;
            var requestId = null;
            try
            {
                // data = data.replace(/mime-type/g, "mime_type");
                // allshare_helper.log.debug("data fixed");

                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - browse responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                requestInfo.callbackHandlers.successCallback(requestInfo, obj);
            }

            break;
            
        case EDMSCP_MSG_SEARCH_RESULT:
            var obj = null;
            var requestId = null;
            try
            {
                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - search responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                requestInfo.callbackHandlers.successCallback(requestInfo, obj);
            }

            break;
            
        case DMSCP_UPNP_DEVICEADDED:    // Device ADD
            allshare_helper.log.debug("DMSCP_UPNP_DEVICEADDED");
            var addedDevice = null;
            if(data2)
            {
                var deviceJson = null;
                try
                {
                    deviceJson = eval("(" + data2 + ")");
                }
                catch(e)
                {
                    allshare_helper.log.error("eval failed");
                    allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
                }

                if (deviceJson)
                {
                    var device = allshare_helper.createDeviceFactory(deviceJson);
                    if (device)
                    {
                        window.webapis.allshare.serviceconnector._DeviceFinder._addedDevice(device);
                    }
                }
            }        
            break;
            
        case DMSCP_UPNP_DEVICEDELETE:    // Device DELETE
            allshare_helper.log.debug("DMSCP_UPNP_DEVICEDELETE");
            var deletedDevice = null;
            if(data2)
            {
                var deviceJson = null;
                try
                {
                    deviceJson = eval("(" + data2 + ")");
                }
                catch(e)
                {
                    allshare_helper.log.error("eval failed");
                    allshare_helper.log.error("exception e.name:" + e.name + "   e.message:" + e.message);
                }

                if (deviceJson)
                {
                    var device = allshare_helper.createDeviceFactory(deviceJson);
                    if (device)
                    {
                        window.webapis.allshare.serviceconnector._DeviceFinder._removedDevice(device);
                    }
                }
            }
            break;
            
        case EDMSCP_MSG_NO_DATA:
            allshare_helper.log.debug("EDMSCP_MSG_NO_DATA");
            // it is not error, just empty set?

            var requestId = null;
            try
            {
                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                var webAPIError = allshare_helper.createWebAPIError("NotFoundError", "No data");
                requestInfo.callbackHandlers.errorCallback(requestInfo, webAPIError);
            }

            break;
            
        case EDMSCP_MSG_CANCEL:
            allshare_helper.log.debug("EDMSCP_MSG_CANCEL");

            var requestId = null;
            try
            {
                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                var webAPIError = allshare_helper.createWebAPIError("UnknownError", "EDMSCP_MSG_CANCEL");
                requestInfo.callbackHandlers.errorCallback(requestInfo, webAPIError);
            }

            break;
            
        case EDMSCP_MSG_UNKNOWN:
            allshare_helper.log.debug("EDMSCP_MSG_UNKNOWN");

            var requestId = null;
            try
            {
                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                var webAPIError = allshare_helper.createWebAPIError("UnknownError", "EDMSCP_MSG_UNKNOWN");
                requestInfo.callbackHandlers.errorCallback(requestInfo, webAPIError);
            }

            break;
            
        case EDMSCP_MSG_INVALID_SVC:
            allshare_helper.log.debug("EDMSCP_MSG_INVALID_SVC");

            var requestId = null;
            try
            {
                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                var webAPIError = allshare_helper.createWebAPIError("NotSupportedError", "The operation is not supported.");
                requestInfo.callbackHandlers.errorCallback(requestInfo, webAPIError);
            }

            break;
            
        default:
            allshare_helper.log.debug("default");

            var requestId = null;
            try
            {
                obj = eval("(" + data2 + ")");

                allshare_helper.log.debug("obj.UniqueRequestID:"+ obj.UniqueRequestID);
                requestId = obj.UniqueRequestID;
            }
            catch(e)
            {
                allshare_helper.log.error("Eval failed - responce return broken JSON");
                allshare_helper.log.error("Exception e.name:" + e.name + "   e.message:" + e.message);
            }

            var requestInfo = allshare_helper.RequestsHelper.getRequest(requestId);
            if (requestInfo)
            {
                allshare_helper.RequestsHelper.removeRequest(requestId);
                var webAPIError = allshare_helper.createWebAPIError("UnknownError", "An unknown error has occurred.");
                requestInfo.callbackHandlers.errorCallback(requestInfo, webAPIError);
            }

            break;
    }

    allshare_helper.log.trace("window.webapis.allshare._onAllsharePluginEvent() end");
}


//=============================================================================


window.webapis.allshare._plugin = function()
{
    var args = Array.prototype.slice.call(arguments);    // make arguments to Array
    allshare_helper.log.print('[window.webapis.allshare] _plugin('+args+')');

    // calling common part to access SEF plugin
    return webapis._plugin.apply(webapis._plugin, args)
}


//alert("[serviceapis.js] loading end");


//=============================================================================
// AllShare WebAPI implementation   END
//=============================================================================



