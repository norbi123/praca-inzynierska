// Chrome: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.220 Safari/535.1
// Safari: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50
// TV Webkit: Mozilla/5.0 (SmartHub; SMART-TV; U; Linux/SmartTV) AppleWebKit/531.2+ (KHTML, like Gecko) WebBrowser/1.0 SmartTV Safari/531.2+
alert("navigator.userAgent: " + navigator.userAgent);
//if(navigator.userAgent.toLowerCase().indexOf('smarttv')>=0 || navigator.userAgent.toLowerCase().indexOf('smarthub')>=0) {

if (navigator.userAgent.toLowerCase().indexOf("applewebkit") >= 0 && navigator.userAgent.toLowerCase().indexOf("maple") >= 0) {
	window._browser = 'tv_webkit';
}
else if (navigator.userAgent.toLowerCase().indexOf('chrome/')>=0) {
	window._browser = 'chrome';
}
else if (navigator.userAgent.toLowerCase().indexOf('mozilla/')>=0) {
	window._browser = 'mozilla';
}
else if (navigator.userAgent.toLowerCase().indexOf('safari/')>=0 && !window.curWidget) {
	window._browser = 'safari';
}
else if (navigator.userAgent.toLowerCase().indexOf('msie')>=0) {
	window._browser = 'internetexplorer';
}
else if (navigator.userAgent.toLowerCase().indexOf('maple')>=0) {
	window._browser = 'tv_maple';
}
alert("Detected Browser : " + window._browser);
if(window._browser == 'chrome' || window._browser == 'safari' || window._browser == 'internetexplorer' || window._browser == 'mozilla') {
	(function () {
		alert("[AF] PC Mode");
		/*
         if(window.location.search.indexOf('modelid') < 0) {
			var url = document.location.href;
			url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
			window.location.href = url + '?country='+ chromeWrapper.country +'&language='+ chromeWrapper.language +'&lang='+ chromeWrapper.lang +'&modelid='+ chromeWrapper.modelId +'&server='+ chromeWrapper.server +'&firmware='+ chromeWrapper.firmware +'&remocon='+ chromeWrapper.remocon +'&area='+ chromeWrapper.area +'&product='+ chromeWrapper.product +'&mgrver='+ chromeWrapper.mgrver +'&res='+ chromeWrapper.res;
            return;
        }
        */
		//window._logwindow = window.open ('','aflogwindow','location=no, directories=no,resizable=yes,status=no,toolbar=no,menubar=no, width=600,height=700,left=0, top=0, scrollbars=no');

        window.curWidget = {
			id: '1234567',
			width: 1280,
			height: 720,
			setPreference: function (key, value) {return null;}
		}

		if (!window.console) console = {};
		console.log = console.log || function(){};
		console.warn = console.warn || function(){};
		console.error = console.error || function(){};
		console.info = console.info || function(){};

		var alertCount = 0;
		window.alert = function (message) {
		//	window._logwindow.document.writeln((alertCount++) + " : " + message+'<br>');
		//	window._logwindow.scrollTo(0, 1000000);
			console.log(message);
		}

		window.FileSystem = function () {
			this.storage = window.localStorage || null;
			var storage = {};
			if(!this.storage) {
				this.storage = {
					getItem: function (key) {
						return storage[key] || null;
					},
					setItem: function (key, value) {
						storage[key] = value;
					},
					removeItem: function (key, value) {
						delete storage[key];
					},
					clear: function (key, value) {
						storage = {};
					}
				}
			}

			var oFile = {};
			var oCommonFile = {};
			this.openFile = function (path, mode) {
				return new FilePointer(path);
			}
			this.openCommonFile = function (path, mode) {
				alert("[FS] openCommonFile(" + path + "," + mode + ")");
				return new CommonFilePointer(path);
			}
			this.isValidCommonPath = function (path) {
				alert("[FS] isValidCommonPath("+path+")");
				var pathObj = null;
				try {
					pathObj = eval("oCommonFile."+path.replace('/', '.'));
				}
				catch(e){
					alert("\tEXCEPTION: " + e);
				}
				return pathObj?1:0;
			}
			this.closeFile = function (fp) {
				alert("[FS] closeFile("+fp+")");
			}

			function FilePointer(path){
				this.readAll = function () {
					var text = $.ajax({url: path, async: false}).responseText;
					alert("[FilePointer] readAll("+path+") returns : " + text);
					return text;
				}
				this.toString = function () {
					return '[Object FilePointer]';
				}
			}

			function CommonFilePointer (path) {
				this.readAll = function () {
					return oCommonFile[path];
				}
				this.toString = function () {
					return '[Object CommonFilePointer]';
				}
			}
		}
	})();
}

/*
 * Title: Core API
 * 	Definition of Apps Framework Base
 */
var sf = sf || {};
sf.version = '2.0.0';
alert("############# Samsung TV Apps Framework : " + sf.version + " #############");

(function (sf) {
	if(sf.core)	return;

	var _afRootPath = window._AFRootPath || '$MANAGER_WIDGET/Common/af';

	function inArray(find, list){
		alert("inArray(" + find + ", " + list + ")");
		for (var i = 0; i < list.length; i++) {
			if (list[i] == find)
				return true;
		}
		return false;
	}

	/*
	   Namespace: sf.core
	   	Apps Framework basic functions
	*/
	sf.core = {
		_afPath: {
			root: _afRootPath,
			core: _afRootPath+'/'+sf.version,
			resources: _afRootPath+'/res',
			images: _afRootPath+'/res',
			lang: _afRootPath+'/res/lang'
		},
		/*
			Function: exit
				terminate the application

			Parameters:
				o (boolean)bEsc -  Whether return to TV Screen or not.
					true - return to TV screen
					false - return to Application Manager

			Returns:
				nothing

			Description:
				Samsung TV specification : EXIT key brings to TV(or previous source) and RETURN key brings to Smart Hub

			Example 1:
				This example exit your app
				(start code)
				handleKeyDown = function (keyCode) {
				 if(keyCode == sf.key.GREEN) { // When green key pressed, return to Smart Hub.
				  sf.core.exit(false);
				 }
				}
				(end)

			Available From:
				2.0.0

			History:
		*/
		exit: function(bEsc){
			bEsc = (bEsc === undefined) ? false : bEsc;		// default == false
			alert('[AF core] sf.core.exit('+bEsc+')');
			if (!bEsc) {
				// send return event
				curWidget.setPreference('return', 'true');
			}
			else {
				// send exit event
				curWidget.setPreference('exit', 'true');
			}
		},

		/*
			Function: version
				returns Apps Framework version

			Parameters:

			Returns:
				String - Apps Framework version. (e.g., "2.0.0")

			Available From:
				2.0.0

			History:
		*/
		version: function(){
			alert("[AF core] AF Version: "+sf.version);
			return sf.version;
		},

		/*
			Function: loadJS
				include javascript file(s)

			Parameters:
				o (Array or String) files	- javascript file path(s)
				o (Function) callback 		- callback function to be called when including files are completed.

			Returns:
				nothing

			Description:
				This function works asynchronously. So, until the callback is called we can't gurantee the file is loaded.

			Available From:
				2.0.0

			History:
		*/
		loadJS : function (files, callback) {
			alert('[AF core] sf.core.loadJS()');
			if(!(files instanceof Array)) {
				files = [files];
			}

			// '' string을 array에서 제거 -> attachJS에서 src를 비워두면 오동작함.
			var temp = [];
			for(var i=0; i<files.length; i++) {
				if(files[i] != '') {
					temp.push(files[i]);
				}
			}
			files = temp;

			new jsLoader (files, callback);

			function jsLoader (files, callback) {
				alert('[AF core] jsLoader() : ' + files.length + ' files: ' + files);
				var nLoaded = 0, script = null, js = null, head = document.getElementsByTagName('head')[0];
				for (var i = 0, len = files.length; i < len; i++) {
					js = files[i];
					if (js.indexOf('/mtd_down') == 0) {	// URL 처음이 mtd_down이면 FILE_PROTOCOL을 붙여줌
						js = FILE_PROTOCOL + ((js.charAt(0) != '/') ? '/' : '') + js;
					}
					script = document.createElement('script');
					script.type = 'text/javascript';
					script.src = js; //+ '?t='+(new Date()).getTime();
					script.onload = onload;
					head.appendChild(script);
				}
				function onload () {
					alert('[AF core] jsLoader (' + files[nLoaded] + ')');
					nLoaded++;
					alert('[AF core] jsLoader ('+nLoaded+'/'+files.length+')');
					if (nLoaded >= files.length) {
						if (typeof callback == 'function') {
							callback();
						}
					}
				}
			}
		},

		/*
			Function: loadCSS
				include CSS file(s)

			Parameters:
				o (Array or String) files	- CSS file path(s)

			Returns:
				nothing

			Description:
				This function works asynchronously.

			Available From:
				2.0.0

			History:
		*/
		loadCSS : function (files) {
			alert('[AF core] sf.core.loadCSS()');
			if(!(files instanceof Array)) {
				files = new Array (files);
			}
			var link = null, css = null;
			for(var i=0; i<files.length; i++) {
				css = files[i];
				if (css.indexOf('/mtd_down') == 0) {	// URL 처음이 mtd_down이면 FILE_PROTOCOL을 붙여줌
					css = FILE_PROTOCOL + ((css.charAt(0) != '/') ? '/' : '') + css;
				}
				link = document.createElement('link');
				link.type = 'text/css';
				link.rel = 'stylesheet';
				link.href = css + '?t=' + (new Date()).getTime();
				document.getElementsByTagName('head')[0].appendChild(link);
				alert('[AF core] ' + css);
			};
		},
		/*
			Function: readFile
				Read the file contents and returns string

			Parameters:
				o (String) path	- file path to read

			Returns:
				String - file contents

			Available From:
				2.0.0

			History:
		*/
		readFile : function (path, root) {
			alert('[AF core] sf.core.readFile('+path+')');
	        if(FileSystem) {//&& location.href.indexOf('file://') >= 0) {
				var fs = new FileSystem();
				var fp = fs.openFile(path, 'r');
				var result = '';
				if (fp) {
					result = fp.readAll();
					fs.closeFile(fp);
				}
				else {
					alert('[AF core] File open failed.');
				}
				return result;
			}
			else {
				alert('[AF core] File open failed.');
			}
	    },

		/*
			Function: getEnvValue
				Getting TV evironment values that is passed to this widget by GET URL parameter.
				The Widget Manager(Smart Hub) passes the environmental parameters via GET REST parameter. (eg, index.html?country=us&lang=en&....)
				This function parse the parameter from Widget Manager and return it.

			Parameters:
				o (String) key	- key of reqested value (country, lang, id, pw)

			Returns:
				String - returns value of the key.

			Description:
				This function parses 'window.location.search' and returns corresponded value.

			Available From:
				2.0.0

			History:
		*/
		getEnvValue: function (key) {
			var retValue = null;
			if (!oEnvParams) {
				oEnvParams = parseEnvParams();
			}
			if (key && typeof key === 'string') {
				retValue = oEnvParams[key];
			}
			var msg = retValue;		// id, pw의 경우 보여주지 않는다.
			if (key == 'id' || key == 'pw') {
				msg = '******';
			}
			alert('[AF core] getInfo(' + key + ') returns ' + msg);
			return retValue;
		},

		_changeSource : function () {
		    alert('[AF core] sf.core._changeSource()');
			var fs = new FileSystem();

			// 파일이 존재하면 source change 하지 않음
			var tFilePath = "WidgetMgr/Widget2Widget.txt";
			if (fs.isValidCommonPath( tFilePath ) != 0) {
				alert("File is existed.. skip setOnFullScreen()");

				var fp = fs.openCommonFile(tFilePath, "r");

				if(fp){
					var temp = fp.readAll();
					temp = parseInt(temp);
					alert("prevSource : "+temp);
					fs.closeFile(fp);
				}
			}else{
				var plugin = sf.core.plugin('TVMW');
				originalSource = plugin.GetSource();

				if( sf.core.PL_TVMW_SOURCE_MEDIA == originalSource ){
					return;
				}
				else{
					originalSource = plugin.GetSource();
					plugin.SetMediaSource();
				}
			}
		},

		_restoreSource : function () {
		    alert('[AF core] sf.core._restoreSource()');
			var fs = new FileSystem();

			// 파일이 존재하면 source change (복구) 하지 않음
			var tFilePath = "WidgetMgr/Widget2Widget.txt";
			if (fs.isValidCommonPath( tFilePath ) != 0) {
				alert("File is existed.. skip setOffFullScreen()");
			}
			else{
				var plugin = sf.core.plugin('TVMW');

				if (originalSource != null) {
					plugin.SetSource(originalSource);
					originalSource = null;
				}
			}
		},

		// Plugin object를 얻어내는 함수
		// options 를 통해 원하는 div에 삽입할 수 있음
		// options : {
		//     'container': 'div element id to insert plguin object', (must be exist)
		//     'z-index': 'a number to set to container div'
		// }
		/*
			Function: plugin
				Get TV device plugin object

			Parameters:
				o (String) name	- name of reqested plugin (TV, TVMW, NNAVI, AUDIO, APPCOMMON, FRONTPANEL, IMAGEVIEWER, PLAYER, etc)

			Returns:
				(Object) - return device plugin object.

			Example:
				(start code)
				var pluginNNAVI = sf.core.plugin('NNAVI');
				pluginNNAVI.GetDeviceID();
				(end)

			Available From:
				2.0.0

			History:
		*/
		plugin : function (name, options) {
			name = name.toUpperCase();
			var plugin = null;
			if (inArray(name, arrPluginList) != -1) {
				plugin = this._getPlugin(name);
				if (!plugin) {
					// if not check mode, insert plugin
					var zIndex = options && options['z-index'] ? 'z-index:'+options['z-index']+';' : '';
					var html = '<OBJECT id="'+pluginIdPrefix+name+'" classid="clsid:SAMSUNG-INFOLINK-'+name+'" style="position:absolute;visibility:hidden;display:block;width:0px;height:0px;'+zIndex+'"></OBJECT>';

					if (options && options.container) {
						var e = document.getElementById(options.container);
						if (e) e.innerHTML += html;
						else alert('[AF core] cannot get ' + options.container + ' element.');
					}
					else {
						var domNode = document.createElement('div');
						domNode.id = '_pluginObjectContainer_'+name;
						domNode.style.position = 'absolute';
						domNode.style.left = '0px';
						domNode.style.top = '0px';
						domNode.style.display = 'block';
						domNode.innerHTML = html;
						getContainer().appendChild(domNode);
					}
					plugin = document.getElementById(pluginIdPrefix+name);
				}
			}
			else {
				alert('[AF core] Cannot find the plugin : ' + name);
			}
			alert('[AF core] sf.core.plugin('+name+') returns ' + plugin);
			return plugin;

			function getContainer () {
				var container = document.getElementById('_pluginObjectContainer_');
				if (!container) {
					var domNode = document.createElement('div');
					domNode.id = '_pluginObjectContainer_';
					domNode.style.position = 'absolute';
					domNode.style.left = '0px';
					domNode.style.top = '0px';
					domNode.style.display = 'block';
					document.body.appendChild(domNode);
					container = document.getElementById('_pluginObjectContainer_');
				}
				return container;
			}
		},

		_getPlugin: function (name) {
			return document.getElementById(pluginIdPrefix+name);
		},

		// SEF (Samsung Extension Framework) Plugin object를 얻어내는 함수
		// options 를 통해 원하는 div에 삽입할 수 있음
		// options : {
		//     'container': 'div element id to insert plguin object', (must be exist)
		//     'z-index': 'a number to set to container div'
		// }
		/*
			Function: sefplugin
				Get TV device SEF plugin object

			Parameters:
				o (String) name	- name of reqested plugin (TV, TVMW, NNAVI, AUDIO, APPCOMMON, FRONTPANEL, IMAGEVIEWER, PLAYER, etc)

			Returns:
				Object - return device plugin object.

			Example:
				(start code)
				var pluginNNAVI = sf.core.sefplugin('NNAVI');
				pluginNNAVI.Execute('GetDeviceID');
				(end)

			Description:
				This function creates SEF plugin and call "open" to intialize this plugin.

			Available From:
				2.0.0

			History:
		*/
		sefplugin : function (name, options) {
			var plugin = null;
			if (true) {  //SEF 는 어떤 이름이 올지 알 수 없으므로 check 하지 않는다.
				plugin = this._getSEFPlugin(name);
				if (!plugin) {
					// if not check mode, insert plugin
					var zIndex = options && options['z-index'] ? 'z-index:'+options['z-index']+';' : '';
					var html = '<OBJECT id="'+sefIdPrefix+name+'" classid="clsid:SAMSUNG-INFOLINK-SEF" style="position:absolute;visibility:hidden;display:block;width:0px;height:0px;'+zIndex+'"></OBJECT>';
					if (options && options.container) {
						var e = document.getElementById(options.container);
						if (e) e.innerHTML += html;
						else alert('[AF core] cannot get ' + options.container + ' element.');
					}
					else {
						var domNode = document.createElement('div');
						domNode.id = '_sefObjectContainer_'+name;
						domNode.style.position = 'absolute';
						domNode.style.left = '0px';
						domNode.style.top = '0px';
						domNode.style.display = 'block';
						getContainer().appendChild(domNode);
						document.getElementById('_sefObjectContainer_'+name).innerHTML = html;
					}
					document.getElementById(sefIdPrefix+name).Open(name, "1.000",name);
					plugin = this._getSEFPlugin(name);
				}
			}
			else {
				alert('[AF core] Cannot find the SEF plugin : ' + name);
			}
			alert('[AF core] sf.core.sefplugin('+name+') returns ' + plugin);
			return plugin;

			function getContainer () {
				var container = document.getElementById('_sefObjectContainer_');
				if (!container) {
					var domNode = document.createElement('div');
					domNode.id = '_sefObjectContainer_';
					domNode.style.position = 'absolute';
					domNode.style.left = '0px';
					domNode.style.top = '0px';
					document.body.appendChild(domNode);
					container = document.getElementById('_sefObjectContainer_');
				}
				return container;
			}
		},
		_getSEFPlugin: function (name) {
			return document.getElementById(sefIdPrefix+name);
		},

		/*
		 * Function: mapAliasedKeys
		 * 	Convert keycode to aliased key's key code. There's some keys to be aliased. For example, the PANEL_VOL_UP and PANEL_VOL_DOWN are to be worked same as RIGHT/LEFT in the Samsung TV's specification. If you use the Scene based architecture, you don't have to care about this. You can use the keycode passed to "handleKeyDown" method.
		 *
		 * Parameters:
		 * 	o keyCode	- (Number) the key code user entered.
		 *
		 * Returns:
		 * 	(Number) aliased key code. if the key code doesn't have to be aliased, the keycode parameter will be returns as it is.
		 */
		mapAliasedKeys: function (keyCode) {
			switch (keyCode) {
				case sf.key.PANEL_VOL_DOWN:
					keyCode = sf.key.LEFT;
					break;
				case sf.key.PANEL_VOL_UP:
					keyCode = sf.key.RIGHT;
					break;
				case sf.key.WHEELUP:
				case sf.key.PANEL_CH_UP:
					keyCode = sf.key.UP;
					break;
				case sf.key.WHEELDOWN:
				case sf.key.PANEL_CH_DOWN:
					keyCode = sf.key.DOWN;
					break;
				case sf.key.PANEL_ENTER:
				case sf.key.PANEL_SOURCE:
					keyCode = sf.key.ENTER;
					break;
				case sf.key.PANEL_MENU:
					keyCode = sf.key.RETURN;
					break;
			}
			return keyCode;
		},

		// Defines
		PL_AUDIO_OUTPUT_DEVICE_EXTERNAL: 3,
		PL_AUDIO_VOLUME_KEY_UP: 0,
		PL_AUDIO_VOLUME_KEY_DOWN: 1,
		PL_AUDIO_OUTPUT_DEVICE_RECEIVER: 4,
		PLR_FALSE: 0,
		PLR_TRUE: 1,
		PL_AUDIO_MUTE_OFF: 1,
		PL_AUDIO_MUTE_ON: 0,
		PL_TV_PRODUCT_TYPE_TV: 0,
		PL_TV_PRODUCT_TYPE_MONITOR: 1,
		PL_TV_PRODUCT_TYPE_BD: 2,
		// Plugin common
		PL_CMN_INFO_VERSION: 0,
		// setVolControl
		PL_NNAVI_STATE_BANNER_NONE: 0,
		PL_NNAVI_STATE_BANNER_VOL: 1,
		PL_NNAVI_STATE_BANNER_VOL_CH: 2,
		// setScreenSaver
		PL_PRFID_AUTO_PROTECTION_TIME: 13,
		EVENT_TO_DEVICE_SCREEN_SAVER_ON: 3,
		EVENT_TO_DEVICE_SCREEN_SAVER_OFF: 4,
		EVENT_TO_DEVICE_HIDE_TOOLS: 6,
		// showSetting
		PICTURE_SETTING: 1,
		SOUND_SETTING: 0,
		MANAGER_EVT_RETURN_WIDGETID: '100',		// TOOL 표시하는 위젯ID 저장을 위한 이벤트
		MANAGER_EVT_RETURN_TO_WIDGET: '101',	// TOOL -> Widget 복귀시 전달되는 이벤트
		MANAGER_EVT_GET_SNS_LIST: "32",
		MANAGER_EVT_GET_APP_ALL_LIST: "36",
		MANAGER_EVT_GET_APP_PATH: "33",
		MANAGER_EVT_MOVE_APP_INFO: "34",

		PL_DTVAPP_TOOL: 18,			// Tool 관련  apptype
		//
		// sfScene.loadMode() > dynamic load
		PL_TVMW_SOURCE_MEDIA: 43
	}

	// private
	// loadJS()
	var FILE_PROTOCOL = 'file://localhost';

	// changeSource(), restoreSource()
	var originalSource = null;

	// plugin()
	var pluginIdPrefix = '_plugin_id_';
	var arrPluginList = [
		'TV', 'TVMW', 'NNAVI', 'AUDIO', 'APPCOMMON',
		'FRONTPANEL', 'IMAGEVIEWER', 'PLAYER', 'STORAGE',
		'NETWORK', 'DOWNLOAD', 'SCREEN', 'TIME', 'VIDEO', 'WINDOW'
	];
	// sef()
	var sefIdPrefix = '_sef_id_';

	// getInfo()
	var oEnvParams = null;
	function parseEnvParams () {
		alert('[AF core] parseInfo()');
		var str = window.location.search;
		var pattern = /(\w*=[\w!@#\$%\*\^\(\)\-\+\|\.=\ \']*)/g;
		var retValue = {};

		// test string
//		str = '?country=US&language=1&lang=en&modelid=Valencia&server=operation&firmware=T-SPHAKRC-1000&remocon=2_35_259_0&area=USA&product=0&mgrver=2.31&name=삼성&good=#@8*!-_=\\&id=ori590e@empal.com'
		for (var i=0, v=str.match(pattern), l=v.length, tmp; i < l; i++) {
			tmp = v[i].split('=');
			retValue[tmp[0]] = decodeURIComponent(tmp[1]);
		}
		return retValue;
	}

	/*
	sf._module = {
		list: ['ui.button', 'ui.checkbox', 'ui.datepicker', 'ui.image', 'ui.keyhelp', 'ui.label', 'ui.list', 'ui.loading', 'ui.popup', 'ui.scroll', 'ui.maps',
			'service.avsetting', 'service.imagecore', 'service.imageviewer', 'service.videocore', 'service.video', 'service.mediabrowser', 'service.smartremote', 'service.usb', 'service.videoplayer']
	}
	*/
})(sf);

// application data management functions
(function (sf) {
	var data = null;
	var FILE = '_sfdata.json';
	var DIR = curWidget.id;
	var localDataLoaded = false;

	/*
	 * Function: localData
	 * 	Save values to local flash memory and gets saved data.
	 *
	 * Parameters:
	 * 	o key	- (String) name of the config
	 * 	o value	- (Any types) (Optional) If this is defined, set this "value" as value of the key.
	 *
	 * Description:
	 * 	This function uses common area
	 */
	sf.core.localData = function (key, value){
		if(!localDataLoaded) {
			_load();
		}

		if (value !== undefined) {
			return set(key, value);
		}
		else {
			return get(key);
		}

		function get (key) {
			key = (key !== undefined) ? key : 0;
			var retValue = (data) ? data[key] : null;
			alert('[AF core] sf.core.localData.get(' +key+ ') returns : ' + retValue);
			return retValue;
		}
		function set (key, value) {
			if (data == null) data = {};
			alert('[AF core] sf.core.localData.set(' + key + ',' + value + ')');
			data[key] = value;
			return value;
		}

		// Common/DIR/FILE 에 저장된 data 로드 (JSON)
		function _load () {
			alert('[AF core] sf.core.localData._load()');
			localDataLoaded = true;
			var FS = new FileSystem();
			if (FS.isValidCommonPath(DIR) != 1) {
				alert('[AF core] directory is not found : ' + DIR);
				data = null;
				return;
			}
			var FP = FS.openCommonFile(DIR + '/' + FILE, 'r');
			if (!FP) {
				alert('[AF core] file is not found : ' + FILE);
				data = null;
				return;
			}
			var json = FP.readAll();
			alert('[AF core] loaded App data : ' + json);
			data = (json == '') ? {} : $.parseJSON(json);
			FS.closeFile(FP);
		}
	}
	sf.core._saveLocalData = function () {
		alert('[AF core] sf.core._saveLocalData()');
		var FS = new FileSystem();
		if (FS.isValidCommonPath(DIR) != 1) {
			alert('[AF core] create data directory..');
			FS.createCommonDir(DIR);
		}
		if (data && data != '') {
			var FP = FS.openCommonFile(DIR + '/' + FILE, 'w');
			if (!FP) {
				alert('[AF core] cannot open App data file..');
				return;
			}
			var json = $.toJSON(data);

			/*
			 * 저장할 data 용량 제한 필요
			 */

			alert('[AF core] save App data : ' + json);
			FP.writeAll(json);
			FS.closeFile(FP);
		}
		else {
			alert('[AF core] no need to save file.');
		}
	}
})(sf);

// app.json의 내용을 가져오는 함수
// 여러 곳에서 중복 파싱을 안하기 위한 방안임.
(function (sf) {
	var conf = null;
	function parseJSON(text){
		if (/^[\],:{}\s]*$/.test(
			text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
				.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
				.replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
			return eval('(' + text + ')');
		}
		return null;
	}
	sf.core.getAppConf = function (key) {
		if(!conf) {
			sf.core._readAppConf();
		}
		if (!conf) {
			return null;
		}
		if (key) {
			alert('[AF core] getConf(' + key + ') returns ' + conf[key]);
			return conf[key];
		}
		else {
			return conf;
		}
	};

	sf.core._readAppConf = function () {
		alert('[AF core] readConf()');
		try {
			var filePath = 'app.json';
			conf = parseJSON(sf.core.readFile(filePath));
		}
		catch (e) {
			alert('[AF core] ' + e.toString());
		}
	};
})(sf);

// curWidget.onWidgetEvent를 여러 곳에서 사용할 때의 문제 해결을 위함
// curWidget.onWidgetEvent로 이벤트를 받고 싶으면
// setEventHandler() 함수를 통해 등록해야 함.
(function (sf) {
	var handler = {};
	sf.core.setWMEventListener = function (event, callback) {
		alert("[AF core] setWMEventListener("+event+")");
		if (typeof callback == "function") {
			handler[event] = callback;
		}
		else {
			alert("[AF core] second param must be a function.");
		}
	}
	sf.core.registerEventListener = function () {
		alert("[AF core] registerEventListener()");
		curWidget.onWidgetEvent = function (eventArgs) {
			alert("[AF core] !!! curWidget.onWidgetEvent !!!");
			alert("[AF core] !!! type : " + eventArgs.type);
			var eh = handler[eventArgs.type];
			if (typeof eh == "function") {
				eh(eventArgs.data || null);
			}
			else {
				alert("[AF core] no event handler binded on this event : " + eventArgs.type);
			}
		}
	}
})(sf);

(function (sf) {
	var firmwareVersion = null;
	var firmwareYear = null;
	var firmwareVer = null;
	var productType = sf.core.getEnvValue('product');
	var modelID = sf.core.getEnvValue('modelid');

	sf.env = {
		//////////////////////////////////
		// Product Type
		//////////////////////////////////
		PRODUCTTYPE_TV: 0,
		PRODUCTTYPE_MONITOR: 1,
		PRODUCTTYPE_BD: 2,
		getProductType: function () {
			alert("[AF env] getProductType() : " + productType);
			return productType;
		},

		//////////////////////////////////
		// Model ID
		//////////////////////////////////
		getModelID: function () {
			alert("[AF env] getModelID() : " + modelID);
			return modelID;
		},

		//////////////////////////////////
		// 3D Supported
		//////////////////////////////////
		SUPPORT3D_FIRMWARE_UPDATE_NEEDED: -1,
		SUPPORT3D_NOTSUPPORTED: 0,
		SUPPORT3D_SUPPORTED: 1,
		get3DSupported: function () {
			alert("[AF env] get3DSupported():");
			var plScreen = sf.core.plugin('SCREEN');
			var firmware = this.getFirmwareVer();
			var productType = this.getProductType();
			var modelID = this.getModelID();

			try {
				if (plScreen && plScreen.Flag3DEffectSupport() == 1) { //3D를 지원하는 기기인지 (1: 지원 0: 미지원)
					alert("\t3D Supported device");
					if (firmware.year == 2010) { // 2010년도
						alert("\t2010 device : Supported");
						return sf.env.SUPPORT3D_SUPPORTED;
					}
					else if (firmware.year == 2011) { // 2011년도
						alert("\t2011 device");
						if (productType == 2) { // PL_TV_PRODUCT_TYPE_BD : 2
							if (plScreen && plScreen.Flag3DTVConnect() == 1) { //BD인 경우 Flag3DTVConnect()로 다시한번 확인 (1: 3D지원 기기, 0:미지원 기기)
								alert("\tBD-Flag3DTVConnect: true");
								retValue = sf.env.SUPPORT3D_SUPPORTED; // 3D지원기기 & BD & Flag3DTVConnect : 3D 지원
							}
							else {
								alert("\tBD-Flag3DTVConnect: false - This method can not guarantee the supporting 3D in this case. TV connected to this BD Player may support 3D.");
								retValue = sf.env.SUPPORT3D_NOTSUPPORTED; // 3D지원기기 & BD & !Flag3DTVConnect : 3D 미지원 - 3DTV에서 HDMI 1.3 스펙을 제대로 지원하지 않아서 지원여부를 잘못 리턴할수도 있음.
							}
						}
						else { // TV/MFM인 경우
							if (modelID.indexOf('X6') >= 0) { // X6 모델 인경우 firmware version이 1003 미만인지 확인
								alert("\tX6 device");
								if (firmware.num < 1003) {
									retValue = sf.env.SUPPORT3D_FIRMWARE_UPDATE_NEEDED; // 3D지원기기 & X6모델 & 1003 미만: firmware update 필요
									alert("\tFirmware update needed(lower than 1003)");
								}
								else {
									retValue = sf.env.SUPPORT3D_SUPPORTED; // 3D지원기기 & X6모델 & 1003 이상: 3D 지원
									alert("\tSupported device");
								}
							}
							else { // 그밖의 모델(Genoa, Novatek 등)인경우, 1001 미만인지 확인
								alert("\tNot X6 device");
								if (firmware.num < 1001) {
									retValue = sf.env.SUPPORT3D_FIRMWARE_UPDATE_NEEDED; // 3D지원기기 & X6모델아님 & 1001 미만: firmware update 필요
									alert("\tFirmware Update Needed(lower than 1001)");
								}
								else {
									retValue = sf.env.SUPPORT3D_SUPPORTED; // 3D지원기기 & X6모델아님 & 1001 이상: 3D 지원
									alert("\tSupported Device");
								}
							}
						}
					}
					else if (firmware.year > 2011) { // 2012년도부터는 일단 지원한다고 리턴
						alert("\tSupported Device (2012 or later)");
						retValue = sf.env.SUPPORT3D_SUPPORTED;
					}
					return retValue;
				}
				else {
					alert("\tNot Suppported device");
					return sf.env.SUPPORT3D_NOTSUPPORTED;
				}
			} catch (e) {
				alert("EXCEPTION: " + e);
			}
		},

		getFirmwareVer: function () {
			// Firmware version (year & ver)
			if (!firmwareVersion || !firmwareYear || !firmwareVer) {
				firmwareVersion = sf.core.plugin('NNAVI').GetFirmware();
				var tmp = firmwareVersion.split('-');
	            firmwareYear = parseInt(tmp[1].replace("INFOLINK", ""), 10);
	            firmwareVer = parseInt(tmp[2], 10);
			}
			alert("[AF env] getFirmwareVer() : " + firmwareVersion);
			return {
				version: firmwareVersion,
				year: firmwareYear,
				num: firmwareVer
			}
		}
	};

	//////////////////////////////////
	// Browser
	//////////////////////////////////
	var browser = window._browser;
	sf.env.getBrowser = function(){
		return browser;
	}

	//////////////////////////////////
	// Resolution
	//////////////////////////////////
	var resolution = parseInt(curWidget.height, 10);
	if(resolution == 540 || resolution == 720 || resolution == 1080) {
		resolution += 'p';
	}
	else {
		resolution = '540p';
	}
	sf.env.getResolution = function(){
		alert("[AF env] getResolution: " + resolution);
		return resolution;
	}
	sf.env.getScreenSize = function () {
		if (curWidget && curWidget.width && curWidget.height) {
			alert("[AF env] getScreenSize: " + curWidget.width + "x" + curWidget.height);
			return {
				width: curWidget.width,
				height: curWidget.height
			};
		}
		else {
			var width = $(window).width();
			var height = $(window).height();
			alert("[AF env] getScreenSize: " + width + "x" + height);
			return {
				width: width,
				height: height
			};
		}
	}
	alert("[AF Core] Resolution: " + sf.env.getResolution() + " - " + sf.env.getScreenSize().width + "x" + sf.env.getScreenSize().height);

	//////////////////////////////////
	// Language
	//////////////////////////////////
	sf.env.getLanguageCode = function(){
		var lang = sf.core.getEnvValue('lang');
		alert("[AF env] getLanguageCode: " + lang);
		return lang;
	}

	//////////////////////////////////
	// Country
	//////////////////////////////////
	sf.env.getCountryCode = function(){
		var country = sf.core.getEnvValue('country');
		alert("[AF env] getCountryCode: " + country);
		return country;
	}
})(sf);

(function(sf) {
	var modulePath = {
		"sso": {
			js : [
				"$MANAGER_WIDGET/Common/API/Widget.js",
				"$MANAGER_WIDGET/Common/API/TVKeyValue.js",
				"$MANAGER_WIDGET/Common/service.sso.js",
				"$MANAGER_WIDGET/Common/Util/sha1.js"
			],
			css : []
		}
	}

	// Private
	// Load the files to be included and initialize the scene manager if the "scenes" is defined in "app.json"
	sf.core.init = function() {
		alert("[AF Core] init()");
		// read application config file
		var conf = sf.core.getAppConf();

		var theme = sf.core.getAppConf('theme');
		sf.core._afPath.images = sf.core._afPath.resources + '/images/' + theme + '/' + sf.env.getResolution();
		//alert("[AF Core] Set Image Path : " + sf.core._afPath.images);

		////////////////////////////////////////
		// Basic files
		var fileList = [];//sf.core._afPath + '/deviceapis.js'];

		////////////////////////////////////////
		// Framework files
		var lang = sf.env.getLanguageCode();
		alert('[AF core] lang before : ' + lang);
		if (conf && conf.languages) {
			if (!sf.util.inArray(lang, conf.languages)) {
				lang = conf.languages[0] || 'en'; // 첫번째 것 혹은 en
			}
		}
		alert('[AF core] lang after : ' + lang);

		// Framework language resource
		fileList.push(sf.core._afPath.lang + '/' + lang + '.js');

		// load base CSS
		alert('[AF ui] load base theme.');

		// Load Module
		/*
		if(conf && conf.modules) {
			//fileList = fileList.concat(sf._module.getModulePaths(conf.module));
			var modules = conf.modules;
			if(!(modules instanceof Array)) {
				modules = [modules];
			}
			var oModuleList = {};
			for(var i=0; i<modules.length; i++) {
				var tmp = modules[i].toLowerCase().split('.');
				if (tmp.length > 1) {
					var cate = tmp[0];
					var mod = tmp[1];
					if(tmp[1] == '*') {	// 해당 카테고리의 모든 모듈
						for(var j=0; j<sf._module.list.length; j++) {
							if (sf._module.list[j].indexOf(cate + '.') == 0) {
								oModuleList[cate] = oModuleList[cate] || {};
								oModuleList[cate][sf._module.list[j].substr(sf._module.list[j].indexOf(cate)+cate.length+1)] = true;
							}
						}
					}
					else if (sf.util.inArray(modules[i].toLowerCase(), sf._module.list)){
						oModuleList[cate] = oModuleList[cate] || {};
						oModuleList[cate][mod] = true;
					}
				}
			}

			var aPathList = [];
			for(var cate in oModuleList) {
				aPathList.push(sf.core._afPath.core + '/' + cate + '/' + cate + '.js');
				sf.core.loadCSS(sf.core._afPath.core + '/themes/' + 'base' + '/' + sf.env.getResolution() + '/' + cate + '.css');
				if (sf.core.getAppConf('theme') != 'base') {
					sf.core.loadCSS(sf.core._afPath.core + '/' + sf.core.getAppConf('theme') + '/' + sf.env.getResolution() + '/' + cate + '.css');
				}
				for (var mod in oModuleList[cate]) {
					aPathList.push(sf.core._afPath.core + '/' + cate + '/' + mod + '.js');
					sf.core.loadCSS(sf.core._afPath.core + '/themes/' + 'base' + '/' + sf.env.getResolution() + '/' + cate + '.' + mod + '.css');
					if (sf.core.getAppConf('theme') != 'base') {
						sf.core.loadCSS(sf.core._afPath.core + '/' + sf.core.getAppConf('theme') + '/' + sf.env.getResolution() + '/' + cate + '.' + mod + '.css');
					}
				}
			}

			fileList = fileList.concat(aPathList);
		}
		*/

		sf.core.loadCSS(sf.core._afPath.core + '/' + 'base_' + sf.env.getResolution() + '.css');
		var conftheme = sf.core.getAppConf('theme');
		if (conftheme && conftheme != 'base') {
			sf.core.loadCSS(sf.core._afPath.core + '/' + sf.core.getAppConf('theme') + '_' + sf.env.getResolution() + '.css');
		}

		/*
		 * Include Module files
		 */
		if (conf && conf.modules && conf.modules.length) {
			for (var i = 0; i < conf.modules.length; i++) {
				if (modulePath[conf.modules[i].toLowerCase()]) {
					var modpath = modulePath[conf.modules[i].toLowerCase()];
					if (modpath.css && modpath.css.length) {
						for (var j = 0; j < modpath.css.length; j++) {
							sf.core.loadCSS(modpath.css[j]);
						}
					}
					if (modpath.js && modpath.js.length) {
						for (var j = 0; j < modpath.js.length; j++) {
							fileList.push(modpath.js[j]);
						}
					}
				}
				else {
					alert("[AF core] No such a module : " + conf.modules[i]);
				}
			}
		}

		////////////////////////////////////////
		// Application files
		// App's lanaguage resource
		if (conf && conf.languages) {
			fileList.push('lang/' + lang + '.js');
		}

		// Scene 구조를 사용할때만 init.js를 로드한다
		if (conf && conf.scenes && conf.scenes.length) {
			fileList.push('app/init.js');
		}

		if (conf && conf.files && conf.files.length) {
			fileList = fileList.concat(conf.files);
		}

		// load framework files
		sf.core.loadJS(fileList, function(){
			// Scene Architecture 사용
			if (conf && conf.scenes && conf.scenes.length) {
				$(document).ready(function(){
					alert("DOCUMENT READY");
					var splash = sf.core.getAppConf('splashimage');
					if(splash && false) {//splash.url) {
						alert("Insert Splash Image");
						splash.width = splash.width || curWidget.width;
						splash.height = splash.height || curWidget.height;
						var left = parseInt((curWidget.width-parseInt(splash.width,10))/2, 10);
						var top = parseInt((curWidget.height-parseInt(splash.height,10))/2, 10);

						var splashdiv = document.createElement('div');
						splashdiv.id = "_af_splash";
						splashdiv.style = "position:absolute;left:0px;top:0px;width:"+curWidget.width+"px;height:"+curWidget.height+"px;background-color:#000000;z-index:100;opacity:0.5;";
						splashdiv.innerHTML = "<div style='position:absolute;left:"+left+"px;top:"+top+"px;width:"+splash.width+"px;height:"+splash.height+"px;'><img src='"+splash.url+"'/></div>";
						if (sf.core.getAppConf('changeSource')) {
							sf.core._changeSource();	// 여기서 에러남
						}
						document.body.appendChild(splashdiv);
						curWidget.setPreference('ready', 'true');
					}
					else {
						splash = null;
					}

					sf.scene.init(function(){
						if (conf.changeSource) {
							sf.core._changeSource();
						}
						// send ready event
						if (splash) {
							document.body.removeChild(splashdiv);
						}
                    else {
							curWidget.setPreference('ready', 'true');
						}
                    if (typeof onStart == 'function') {
							onStart();
						}
					});
				});
			}
			// Scene Architecture 미사용
			else {
				if (conf && conf.changeSource) {
					sf.core._changeSource();
				}
				// send ready event
				curWidget.setPreference('ready', 'true');
			}
		});

		$(window).unload(function(){
			alert("[AF core] unload handler");
	        if (typeof onDestroy == 'function') {
	            onDestroy();
	        }
	        else {
	            alert('[AF core] onDestroy is not defined..');
	        }

			sf.service.PIG.hide();

			// [111121] deviceapis -> imageview, avplay destroy
			if (deviceapis) {
				deviceapis.imageview._destroyAll();
				deviceapis.avplay._destroyAll();
			}

			var ePlayer = sf.core._getPlugin('PLAYER');
			if (ePlayer) ePlayer.Stop();

			var eImageViewer = sf.core._getPlugin('IMAGEVIEWER');
			if (eImageViewer) eImageViewer.Stop();

			var eSEFPlayer = sf.core._getSEFPlugin('Player');
			if (eSEFPlayer) eSEFPlayer.Execute("Stop");

	        sf.core._saveLocalData();
	        sf.core._restoreSource();
	    });

		if (jQuery) {
			jQuery.ajaxSetup({
				type: 'GET',
				timeout: 60000,
				beforeSend: function(xhrobj){
					// watchdog off
					sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_TRUE);
				},
				complete: function(xhrobj, status){
					// watchdog on
					sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_FALSE);
					alert('[AF core] AJAX complete : ' + status);
					alert('[AF core] xhrobj.status : ' + xhrobj.status);
				},
				error: function(xhrobj, status, errorThrown){
					alert('[AF core] AJAX error : ' + status);
					alert('[AF core] xhrobj.status : ' + xhrobj.status);
					alert('[AF core] errorThrown : ' + errorThrown);
				}
			});
		}
	}
})(sf);

(function (sf) {
	/*
		Namespace: sf.key
			Key event ENUM values and functions

		Enums:
			UP		- up key
			DOWN	- down key
			LEFT	-	left key
			RIGHT	-	right key
			ENTER	-	enter key
			TOOLS	-	tools key
			RETURN	-	return key
			EXIT	-	exit key
			INFO	-	info key
			RED		-	red key
			GREEN	-	green key
			YELLOW	-	yellow key
			BLUE	-	blue key
			N0 ~ N9 - number key 0~9
			PRECH	-	PRE-CH key
			VOL_UP	-	volume up
			VOL_DOWN	-	volume down
			MUTE	-	volume mute
			CH_UP	-	channel up
			CH_DOWN	-	channel down
			REW		-	rewind key
			PAUSE	-	pause key
			FF		-	fast forward key
			REC		-	record key
			PLAY	-	play key
			STOP	-	stop key
			THREE_D -	3D key
	*/

	sf.key = {
		//POWER : 76,
		//TV : 77,

		N1 : 101,
		N2 : 98,
		N3 : 6,
		N4 : 8,
		N5 : 9,
		N6 : 10,
		N7 : 12,
		N8 : 13,
		N9 : 14,
		N0 : 17,

		PRECH : 259,

		VOL_UP : 7,
		VOL_DOWN : 11,
		MUTE : 27,
		CH_UP : 68,
		CH_DOWN : 65,

//		SOURCE : 222,
//		CHLIST : 84,
//		MENU : 262,
//		WLINK : 115,
		TOOLS : 75,
		ENTER : 29443,
		RETURN : 88,
	    INFO : 31,
	    EXIT : 45,

	    UP : 29460,
		DOWN : 29461,
		LEFT : 4,
		RIGHT : 5,

	    RED : 108,
		GREEN : 20,
		YELLOW : 21,
		BLUE : 22,

//		INFOLINK : 147,
//		DMA : 260,
//	    EMODE : 148,
//		CONTENTS : 261,
//	    FAVCH : 256,

	    REW : 69,
		REW_ : 1080,
		PAUSE : 74,
		FF : 72,
		FF_ : 1078,
	    REC : 192,
		PLAY : 71,
		STOP : 70,

		THREE_D : 1219,

		/* 아래는 자동으로 처리해주므로 공개하지 않음 */
	    PANEL_CH_UP: 105,
	    PANEL_CH_DOWN : 106,
	    PANEL_VOL_UP : 203,
	    PANEL_VOL_DOWN : 204,
	    PANEL_ENTER : 309,
	    PANEL_SOURCE : 612,
	    PANEL_MENU : 613,
	    PANEL_POWER : 614

//	    EMPTY : 0,
//	    CC : 118,
//		TTX_MIX : 650,
//		GUIDE : 651,
//		SUBTITLE : 652,
//		ASPECT : 653,
//		DOLBY_SRR : 654,
//		MTS : 655,

//		WHEELDOWN : 29469,
//		WHEELUP : 29468
	};

	/*
    * Function: preventDefault
    *	prevent default key action.
    *
    * Parameters:
    * 	N/A
    *
    * Returns:
    * 	nothing
    *
    * Default actions of KEYs:
    * 	EXIT, INFOLINK	- return to TV (or previous source)
    * 	RETURN			- retrun to Smart Hub(or Widget Manager)
    */
	sf.key.preventDefault = function() {
		alert("[AF Key] preventDefault()");
		event.preventDefault();
	}

	/*
    * Function: registerKey
    *	Register key for getting key events of the key. (keydown, keyup, etc.)
    * 	Each keys are having the default actions for that. For example, VOL_UP key raise the volume level.
    * 	We need to register a key for receiving key events instead of default actions. If we register the VOL_UP key, we can receive "keydown" event for the key and it's not work for rasing the volume.
    * 	Almost all keys are registered by Widget Manager when the application is launched. So *you don't need to call this function in most cases.*
    * 	If you need to get the event for not registered key by default, you can call this function.
    *
    * Parameters:
    * 	o (Number) keyCode	- keycode to register
    *
    * Returns:
    * 	nothing
    *
    * Description:
    * 	Keys generally used are already registered when the app is initialized. If you need to get key event except that, you need to call this function.
    */
	sf.key.registerKey = function (keyCode) {
		/*
		var p = sf.core.plugin('APPCOMMON');
		if (p) {
			alert('[AF core] sf.key.registerKey('+keyCode+' -> ['+appCommonMap[keyCode]+'])');
			p.RegisterKey(appCommonMap[keyCode]);
		}
		*/
		var p = sf.core.plugin('TVMW');
		if (p) {
			alert('[AF core] sf.key.registerKey('+keyCode+' -> ['+TVMWMap[keyCode]+'])');
			if (TVMWMap[keyCode]) {
				p.RegisterKey(TVMWMap[keyCode]);
			}
		}
	}

	/*
    * Function: unregisterKey
    *	Unregister key. please refer to <registerKey> for more details about register/unregister keys.
    *
    * Parameters:
    * 	o (Number) keyCode	- keycode to register
    *
    * Returns:
    * 	nothing
    */
	sf.key.unregisterKey = function (keyCode) {
		/*
		var p = sf.core.plugin('APPCOMMON');
		if (p) {
			alert('[AF core] sf.key.unregisterKey('+keyCode+' -> ['+appCommonMap[keyCode]+'])');
			p.UnregisterKey(appCommonMap[keyCode]);
		}
		*/
		var p = sf.core.plugin('TVMW');
		if (p) {
			alert('[AF core] sf.key.unregisterKey('+keyCode+' -> ['+TVMWMap[keyCode]+'])');
			if (TVMWMap[keyCode]) {
				p.UnregisterKey(TVMWMap[keyCode]);
			}
		}
	}

	// plAppCommon_scriptToPluginAPI.h
	var APPCOMMON_KEY = {
		PL_APPCOMMON_KEY_JOYSTICK_OK			: 29443,
		PL_APPCOMMON_KEY_MENU					: 262,
		PL_APPCOMMON_KEY_JOYSTICK_UP			: 29460,
		PL_APPCOMMON_KEY_JOYSTICK_DOWN			: 29461,
		PL_APPCOMMON_KEY_JOYSTICK_LEFT			: 4,
		PL_APPCOMMON_KEY_JOYSTICK_RIGHT			: 5,
		PL_APPCOMMON_KEY_3						: 6,
		PL_APPCOMMON_KEY_VOLUP					: 7,
		PL_APPCOMMON_KEY_4						: 8,
		PL_APPCOMMON_KEY_5						: 9,
		PL_APPCOMMON_KEY_6						: 10,
		PL_APPCOMMON_KEY_VOLDOWN				: 11,
		PL_APPCOMMON_KEY_7						: 12,
		PL_APPCOMMON_KEY_8						: 13,
		PL_APPCOMMON_KEY_9						: 14,
		PL_APPCOMMON_KEY_MUTE					: 27,
		PL_APPCOMMON_KEY_CHDOWN					: 65,
		PL_APPCOMMON_KEY_0						: 17,
		PL_APPCOMMON_KEY_CHUP					: 68,
		PL_APPCOMMON_KEY_PRECH					: 259,
		PL_APPCOMMON_KEY_GREEN					: 20,
		PL_APPCOMMON_KEY_YELLOW					: 21,
		PL_APPCOMMON_KEY_CYAN					: 22,
//		PL_APPCOMMON_KEY_STEP					: 1023,
//		PL_APPCOMMON_KEY_DEL					: 1024,
//		PL_APPCOMMON_KEY_ADDDEL					: 1025,
		PL_APPCOMMON_KEY_SOURCE					: 222,
		PL_APPCOMMON_KEY_TV						: 77,
//		PL_APPCOMMON_KEY_AUTO					: 1028,
//		PL_APPCOMMON_KEY_MOIP					: 1029,
//		PL_APPCOMMON_KEY_PMENU					: 1030,
		PL_APPCOMMON_KEY_INFO					: 31,
//		PL_APPCOMMON_KEY_PIP_ONOFF				: 1032,
//		PL_APPCOMMON_KEY_PIP_SWAP				: 1033,
//		PL_APPCOMMON_KEY_PIP_ROTATE				: 1034,
//		PL_APPCOMMON_KEY_PLUS100				: 35,
//		PL_APPCOMMON_KEY_PIP_INPUT				: 1036,
//		PL_APPCOMMON_KEY_CAPTION				: 652,
//		PL_APPCOMMON_KEY_PIP_STILL				: 1038,
//		PL_APPCOMMON_KEY_AD						: 1039,
//		PL_APPCOMMON_KEY_PMODE					: 1040,
//		PL_APPCOMMON_KEY_SOUND_MODE				: 1041,
//		PL_APPCOMMON_KEY_NR						: 1042,
//		PL_APPCOMMON_KEY_SMODE					: 1043,
//		PL_APPCOMMON_KEY_TTX_MIX				: 650,
		PL_APPCOMMON_KEY_EXIT					: 45,
//		PL_APPCOMMON_KEY_ENTER					: 1046,
//		PL_APPCOMMON_KEY_PIP_SIZE				: 1047,
//		PL_APPCOMMON_KEY_MAGIC_CHANNEL			: 1048,
//		PL_APPCOMMON_KEY_PIP_SCAN				: 1049,
//		PL_APPCOMMON_KEY_PIP_CHUP				: 1050,
//		PL_APPCOMMON_KEY_PIP_CHDOWN				: 1051,
//		PL_APPCOMMON_KEY_DEVICE_CONNECT			: 1052,
//		PL_APPCOMMON_KEY_HELP					: 1053,
//		PL_APPCOMMON_KEY_ANTENA					: 1054,
//		PL_APPCOMMON_KEY_CONVERGENCE			: 1055,
//		PL_APPCOMMON_KEY_11						: 73,
//		PL_APPCOMMON_KEY_12						: 1057,
//		PL_APPCOMMON_KEY_AUTO_PROGRAM			: 1058,
//		PL_APPCOMMON_KEY_FACTORY				: 1059,
//		PL_APPCOMMON_KEY_3SPEED					: 1060,
//		PL_APPCOMMON_KEY_RSURF					: 1061,
//		PL_APPCOMMON_KEY_ASPECT					: 653,
//		PL_APPCOMMON_KEY_EMANUAL				: 1063,
//		PL_APPCOMMON_KEY_GAME					: 1064,
//		PL_APPCOMMON_KEY_QUICK_REPLAY			: 1065,
//		PL_APPCOMMON_KEY_STILL_PICTURE			: 1066,
//		PL_APPCOMMON_KEY_DTV					: 1067,
		PL_APPCOMMON_KEY_FAVCH					: 256,
		PL_APPCOMMON_KEY_REWIND					: 69,
		PL_APPCOMMON_KEY_STOP					: 70,
		PL_APPCOMMON_KEY_PLAY					: 71,
		PL_APPCOMMON_KEY_FF						: 72,
		PL_APPCOMMON_KEY_REC					: 192,
		PL_APPCOMMON_KEY_PAUSE					: 74,
		PL_APPCOMMON_KEY_TOOLS					: 115,
//		PL_APPCOMMON_KEY_INSTANT_REPLAY			: 1076,
//		PL_APPCOMMON_KEY_LINK					: 1077,
		PL_APPCOMMON_KEY_FF_					: 1078,
//		PL_APPCOMMON_KEY_GUIDE					: 651,
		PL_APPCOMMON_KEY_REWIND_				: 1080,
//		PL_APPCOMMON_KEY_ANGLE					: 1081,
//		PL_APPCOMMON_KEY_RESERVED1				: 1082,
//		PL_APPCOMMON_KEY_ZOOM1					: 1083,
//		PL_APPCOMMON_KEY_PROGRAM				: 1084,
//		PL_APPCOMMON_KEY_BOOKMARK				: 1085,
//		PL_APPCOMMON_KEY_DISC_MENU				: 1086,
//		PL_APPCOMMON_KEY_PRINT					: 1087,
		PL_APPCOMMON_KEY_RETURN					: 88,
//		PL_APPCOMMON_KEY_SUB_TITLE				: 1089,
//		PL_APPCOMMON_KEY_CLEAR					: 1090,
//		PL_APPCOMMON_KEY_VCHIP					: 1091,
//		PL_APPCOMMON_KEY_REPEAT					: 78,
//		PL_APPCOMMON_KEY_DOOR					: 1093,
//		PL_APPCOMMON_KEY_OPEN					: 1094,
//		PL_APPCOMMON_KEY_WHEEL_LEFT				: 29468,
//		PL_APPCOMMON_KEY_POWER					: 76,
//		PL_APPCOMMON_KEY_SLEEP					: 1097,
		PL_APPCOMMON_KEY_2						: 98,
		PL_APPCOMMON_KEY_DMA					: 1099,
//		PL_APPCOMMON_KEY_TURBO					: 1100,
		PL_APPCOMMON_KEY_1						: 101,
//		PL_APPCOMMON_KEY_FM_RADIO				: 1102,
//		PL_APPCOMMON_KEY_DVR_MENU				: 1103,
//		PL_APPCOMMON_KEY_MTS					: 655,
//		PL_APPCOMMON_KEY_PCMODE					: 1105,
//		PL_APPCOMMON_KEY_TTX_SUBFACE			: 1106,
		PL_APPCOMMON_KEY_CH_LIST				: 84,
		PL_APPCOMMON_KEY_RED					: 108,
//		PL_APPCOMMON_KEY_DNIe					: 1109,
//		PL_APPCOMMON_KEY_SRS					: 654,
//		PL_APPCOMMON_KEY_CONVERT_AUDIO_MAINSUB	: 1111,
//		PL_APPCOMMON_KEY_MDC					: 1112,
//		PL_APPCOMMON_KEY_SEFFECT				: 1113,
//		PL_APPCOMMON_KEY_DVR					: 1114,
//		PL_APPCOMMON_KEY_DTV_SIGNAL				: 1115,
//		PL_APPCOMMON_KEY_LIVE					: 1116,
//		PL_APPCOMMON_KEY_PERPECT_FOCUS			: 1117,
//		PL_APPCOMMON_KEY_HOME					: 1118,
//		PL_APPCOMMON_KEY_ESAVING				: 1119,
//		PL_APPCOMMON_KEY_WHEEL_RIGHT			: 29469,
		PL_APPCOMMON_KEY_CONTENTS				: 261,
//		PL_APPCOMMON_KEY_VCR_MODE				: 1122,
//		PL_APPCOMMON_KEY_CATV_MODE				: 1123,
//		PL_APPCOMMON_KEY_DSS_MODE				: 1124,
//		PL_APPCOMMON_KEY_TV_MODE				: 1125,
//		PL_APPCOMMON_KEY_DVD_MODE				: 1126,
//		PL_APPCOMMON_KEY_STB_MODE				: 1127,
//		PL_APPCOMMON_KEY_CALLER_ID				: 1128,
//		PL_APPCOMMON_KEY_SCALE					: 1129,
//		PL_APPCOMMON_KEY_ZOOM_MOVE				: 1130,
//		PL_APPCOMMON_KEY_CLOCK_DISPLAY			: 1131,
//		PL_APPCOMMON_KEY_AV1					: 1132,
//		PL_APPCOMMON_KEY_SVIDEO1				: 1133,
//		PL_APPCOMMON_KEY_COMPONENT1				: 1134,
//		PL_APPCOMMON_KEY_SETUP_CLOCK_TIMER		: 1135,
//		PL_APPCOMMON_KEY_COMPONENT2				: 1136,
//		PL_APPCOMMON_KEY_MAGIC_BRIGHT			: 1137,
//		PL_APPCOMMON_KEY_DVI					: 1138,
//		PL_APPCOMMON_KEY_HDMI					: 1139,
		PL_APPCOMMON_KEY_W_LINK					: 115,
//		PL_APPCOMMON_KEY_DTV_LINK				: 1141,
//		PL_APPCOMMON_KEY_RESERVED5				: 1142,
//		PL_APPCOMMON_KEY_APP_LIST				: 118,
//		PL_APPCOMMON_KEY_BACK_MHP				: 1144,
//		PL_APPCOMMON_KEY_ALT_MHP				: 1145,
//		PL_APPCOMMON_KEY_DNSe					: 1146,
		PL_APPCOMMON_KEY_RSS					: 147,
		PL_APPCOMMON_KEY_ENTERTAINMENT			: 653,
//		PL_APPCOMMON_KEY_ID_INPUT				: 1149,
//		PL_APPCOMMON_KEY_ID_SETUP				: 1150,
//		PL_APPCOMMON_KEY_ANYNET					: 1151,
//		PL_APPCOMMON_KEY_POWEROFF				: 1152,
//		PL_APPCOMMON_KEY_POWERON				: 1153,
//		PL_APPCOMMON_KEY_ANYVIEW				: 1154,
//		PL_APPCOMMON_KEY_MS						: 1155,
//		PL_APPCOMMON_KEY_MORE					: 1156,
		PL_APPCOMMON_KEY_PANNEL_POWER			: 614,
		PL_APPCOMMON_KEY_PANNEL_CHUP			: 105,
		PL_APPCOMMON_KEY_PANNEL_CHDOWN			: 106,
		PL_APPCOMMON_KEY_PANNEL_VOLUP			: 203,
		PL_APPCOMMON_KEY_PANNEL_VOLDOWN			: 204,
		PL_APPCOMMON_KEY_PANNEL_ENTER			: 309,
		PL_APPCOMMON_KEY_PANNEL_MENU			: 613,
		PL_APPCOMMON_KEY_PANNEL_SOURCE			: 612,
//		PL_APPCOMMON_KEY_AV2					: 1165,
//		PL_APPCOMMON_KEY_AV3					: 1166,
//		PL_APPCOMMON_KEY_SVIDEO2				: 1167,
//		PL_APPCOMMON_KEY_SVIDEO3				: 1168,
//		PL_APPCOMMON_KEY_ZOOM2					: 1169,
//		PL_APPCOMMON_KEY_PANORAMA				: 1170,
//		PL_APPCOMMON_KEY_4_3					: 1171,
//		PL_APPCOMMON_KEY_16_9					: 1172,
//		PL_APPCOMMON_KEY_DYNAMIC				: 1173,
//		PL_APPCOMMON_KEY_STANDARD				: 1174,
//		PL_APPCOMMON_KEY_MOVIE1					: 1175,
//		PL_APPCOMMON_KEY_CUSTOM					: 1176,
//		PL_APPCOMMON_KEY_AUTO_ARC_RESET			: 1177,
//		PL_APPCOMMON_KEY_AUTO_ARC_LNA_ON		: 1178,
//		PL_APPCOMMON_KEY_AUTO_ARC_LNA_OFF		: 1179,
//		PL_APPCOMMON_KEY_AUTO_ARC_ANYNET_MODE_OK: 1180,
//		PL_APPCOMMON_KEY_AUTO_ARC_ANYNET_AUTO_START: 1181,
//		PL_APPCOMMON_KEY_AUTO_FORMAT			: 1182,
//		PL_APPCOMMON_KEY_DNET					: 1183,
//		PL_APPCOMMON_KEY_HDMI1					: 1184,
//		PL_APPCOMMON_KEY_AUTO_ARC_CAPTION_ON	: 1185,
//		PL_APPCOMMON_KEY_AUTO_ARC_CAPTION_OFF	: 1186,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_DOUBLE	: 1187,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_LARGE		: 1188,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_SMALL		: 1189,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_WIDE		: 1190,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_LEFT_TOP	: 1191,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_RIGHT_TOP	: 1192,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_LEFT_BOTTOM: 1193,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_RIGHT_BOTTOM: 1194,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_CH_CHANGE	: 1195,
//		PL_APPCOMMON_KEY_AUTO_ARC_AUTOCOLOR_SUCCESS: 1196,
//		PL_APPCOMMON_KEY_AUTO_ARC_AUTOCOLOR_FAIL: 1197,
//		PL_APPCOMMON_KEY_AUTO_ARC_C_FORCE_AGING	: 1198,
//		PL_APPCOMMON_KEY_AUTO_ARC_USBJACK_INSPECT: 1199,
//		PL_APPCOMMON_KEY_AUTO_ARC_JACK_IDENT	: 1200,
//		PL_APPCOMMON_KEY_NINE_SEPERATE			: 1201,
//		PL_APPCOMMON_KEY_ZOOM_IN				: 1202,
//		PL_APPCOMMON_KEY_ZOOM_OUT				: 1203,
//		PL_APPCOMMON_KEY_MIC					: 1204,
//		PL_APPCOMMON_KEY_HDMI2					: 1205,
//		PL_APPCOMMON_KEY_HDMI3					: 1206,
//		PL_APPCOMMON_KEY_AUTO_ARC_CAPTION_KOR	: 1207,
//		PL_APPCOMMON_KEY_AUTO_ARC_CAPTION_ENG	: 1208,
//		PL_APPCOMMON_KEY_AUTO_ARC_PIP_SOURCE_CHANGE: 1209,
//		PL_APPCOMMON_KEY_HDMI4					: 1210,
//		PL_APPCOMMON_KEY_AUTO_ARC_ANTENNA_AIR	: 1211,
//		PL_APPCOMMON_KEY_AUTO_ARC_ANTENNA_CABLE	: 1212,
//		PL_APPCOMMON_KEY_AUTO_ARC_ANTENNA_SATELLITE: 1213,
//		PL_APPCOMMON_KEY_AUTO_ARC_CIP_TEST		: 1214,
//		PL_APPCOMMON_KEY_AUTO_ARC_CH_CHANGE		: 1215,
//		PL_APPCOMMON_KEY_AUTO_ARC_START_MBR_TEST: 1216,
//
//		PL_APPCOMMON_KEY_TILT     				: 1217,
//		PL_APPCOMMON_KEY_EZ_VIEW         		: 1218,
		PL_APPCOMMON_KEY_3D            			: 1219
//
//		PL_APPCOMMON_KEY_AUTO_ARC_PVR_RECORDING_TEST	: 1220,
//		PL_APPCOMMON_KEY_AUTO_ARC_PVR_PLAY_TEST     	: 1221,
//		PL_APPCOMMON_KEY_AUTO_ARC_PVR_DELETE_ALL        : 1222,
//
//		PL_APPCOMMON_KEY_AUTO_ARC_HOTEL_INTERACTIVE: 1223,
//
//		PL_APPCOMMON_KEY_D_LIST_UP 				: 1224,
//		PL_APPCOMMON_KEY_D_LIST_DOWN			: 1225,
//
//		PL_APPCOMMON_KEY_D_ONDEMAND				: 1226,
//
//		PL_APPCOMMON_KEY_D_PANNEL_VIRTUAL_ENTER : 1227,
//		PL_APPCOMMON_KEY_D_PANNEL_VIRTUAL_SOURCE : 1228,

		//for BD Application
//		PL_APPCOMMON_KEY_D_REC_PAUSE 			: 1229,
//		PL_APPCOMMON_KEY_D_CONTINUED_FF 		: 1230,
//		PL_APPCOMMON_KEY_D_CONTINUED_RW 		: 1231,
//		PL_APPCOMMON_KEY_D_CONTINUED_OFF 		: 1232,
//		PL_APPCOMMON_KEY_D_CANCEL 				: 1233,
//		PL_APPCOMMON_KEY_D_TITLE_MENU 			: 1234,
//		PL_APPCOMMON_KEY_D_REPEAT_AB 			: 1235,
//		PL_APPCOMMON_KEY_D_AUDIO 				: 1236,
//		PL_APPCOMMON_KEY_D_REC_MODE 			: 1237,
//		PL_APPCOMMON_KEY_D_USB 					: 1238,
//		PL_APPCOMMON_KEY_D_HDD 					: 1239,
//		PL_APPCOMMON_KEY_D_DISC 				: 1240,
//		PL_APPCOMMON_KEY_D_TEST 				: 1241,
//		PL_APPCOMMON_KEY_D_COPY 				: 1242,
//		PL_APPCOMMON_KEY_D_FRONT_PLAY 			: 1243,
//		PL_APPCOMMON_KEY_D_FRONT_STOP 			: 1244,
//		PL_APPCOMMON_KEY_D_FRONT_FF 			: 1245,
//		PL_APPCOMMON_KEY_D_FRONT_RW 			: 1246,
//		PL_APPCOMMON_KEY_D_FRONT_REC 			: 1247,
//		PL_APPCOMMON_KEY_D_VERSION_DISPLAY_KEU 	: 1248,
//		PL_APPCOMMON_KEY_D_VIEW_MODE 			: 1249,
//		PL_APPCOMMON_KEY_D_COLD_START 			: 1250
	};

	// convert sfKey to _PL_APPCOMMON_KEY
	var appCommonMap = {};
	appCommonMap[sf.key.N1]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_1;
	appCommonMap[sf.key.N2]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_2;
	appCommonMap[sf.key.N3]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_3;
	appCommonMap[sf.key.N4]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_4;
	appCommonMap[sf.key.N5]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_5;
	appCommonMap[sf.key.N6]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_6;
	appCommonMap[sf.key.N7]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_7;
	appCommonMap[sf.key.N8]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_8;
	appCommonMap[sf.key.N9]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_9;
	appCommonMap[sf.key.N0]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_0;
	appCommonMap[sf.key.PRECH]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PRECH;
	appCommonMap[sf.key.VOL_UP]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_VOLUP;
	appCommonMap[sf.key.VOL_DOWN]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_VOLDOWN;
	appCommonMap[sf.key.CH_UP]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_CHUP;
	appCommonMap[sf.key.CH_DOWN]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_CHDOWN;
	appCommonMap[sf.key.MUTE]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_MUTE;
	appCommonMap[sf.key.SOURCE]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_SOURCE;
	appCommonMap[sf.key.CHLIST]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_CH_LIST;
	appCommonMap[sf.key.MENU]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_MENU;
	appCommonMap[sf.key.WLINK]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_W_LINK;
	appCommonMap[sf.key.ENTER]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_JOYSTICK_OK;
	appCommonMap[sf.key.RETURN]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_RETURN;
	appCommonMap[sf.key.INFO]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_INFO;
	appCommonMap[sf.key.EXIT]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_EXIT;
	appCommonMap[sf.key.UP]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_JOYSTICK_UP;
	appCommonMap[sf.key.DOWN]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_JOYSTICK_DOWN;
	appCommonMap[sf.key.LEFT]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_JOYSTICK_LEFT;
	appCommonMap[sf.key.RIGHT]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_JOYSTICK_RIGHT;
	appCommonMap[sf.key.RED]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_RED;
	appCommonMap[sf.key.GREEN]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_GREEN;
	appCommonMap[sf.key.YELLOW]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_YELLOW;
	appCommonMap[sf.key.BLUE]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_CYAN;
	appCommonMap[sf.key.INFOLINK]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_RSS;
	appCommonMap[sf.key.DMA]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_DMA;
	appCommonMap[sf.key.EMODE]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_ENTERTAINMENT;
	appCommonMap[sf.key.CONTENTS]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_CONTENTS;
	appCommonMap[sf.key.FAVCH]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_FAVCH;
	appCommonMap[sf.key.REW]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_REWIND;
	appCommonMap[sf.key.PAUSE]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PAUSE;
	appCommonMap[sf.key.FF]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_FF;
	appCommonMap[sf.key.REC]			=	APPCOMMON_KEY.PL_APPCOMMON_KEY_REC;
	appCommonMap[sf.key.PLAY]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PLAY;
	appCommonMap[sf.key.STOP]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_STOP;
	appCommonMap[sf.key.PANEL_CH_UP]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_CHUP;
	appCommonMap[sf.key.PANEL_CH_DOWN]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_CHDOWN;
	appCommonMap[sf.key.PANEL_VOL_UP]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_VOLUP;
	appCommonMap[sf.key.PANEL_VOL_DOWN]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_VOLDOWN;
	appCommonMap[sf.key.PANEL_ENTER]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_ENTER;
	appCommonMap[sf.key.PANEL_SOURCE]	=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_SOURCE;
	appCommonMap[sf.key.PANEL_MENU]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_MENU;
	appCommonMap[sf.key.PANEL_POWER]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_PANNEL_POWER;
	appCommonMap[sf.key.THREE_D]		=	APPCOMMON_KEY.PL_APPCOMMON_KEY_3D;

	var TVMW_KEY = {
		PL_TVMW_KEY_1: 0, // Number 1
		PL_TVMW_KEY_2: 1, // Number 2
		PL_TVMW_KEY_3: 2, // Number 3
		PL_TVMW_KEY_4: 3, // Number 4
		PL_TVMW_KEY_5: 4, // Number 5
		PL_TVMW_KEY_6: 5, // Number 6
		PL_TVMW_KEY_7: 6, // Number 7
		PL_TVMW_KEY_8: 7, // Number 8
		PL_TVMW_KEY_9: 8, // Number 9
		PL_TVMW_KEY_0: 9, // Number 0
		PL_TVMW_KEY_HYPHEN: 10, // Hyphen(-)
		PL_TVMW_KEY_11: 11, // Number 11
		PL_TVMW_KEY_CH_UP: 100, // Channel up
		PL_TVMW_KEY_CH_DOWN: 101, // Channel down
		PL_TVMW_KEY_CH_PREV: 102, // Previous channel
		PL_TVMW_KEY_CH_FAV: 103, // Favorite channel
		PL_TVMW_KEY_CH_LIST: 104, // Channel list
		PL_TVMW_KEY_CH_PANNEL_UP: 105, // Pannel channel up
		PL_TVMW_KEY_CH_PANNEL_DOWN: 106, // Pannel channel down
		PL_TVMW_KEY_VOL_UP: 200, // Volume up
		PL_TVMW_KEY_VOL_DOWN: 201, // Volume down
		PL_TVMW_KEY_MUTE: 202, // Mute
		PL_TVMW_KEY_VOL_PANNEL_UP: 203, // Pannel volume up
		PL_TVMW_KEY_VOL_PANNEL_DOWN: 204, // Pannel volume down
		PL_TVMW_KEY_ENTER: 300, // Enter
		PL_TVMW_KEY_RETURN: 301, // Return
		PL_TVMW_KEY_EXIT: 302, // Exit
		PL_TVMW_KEY_ARROW_UP: 303, // Arrow up
		PL_TVMW_KEY_ARROW_DOWN: 304, // Arrow down
		PL_TVMW_KEY_ARROW_LEFT: 305, // Arrow left
		PL_TVMW_KEY_ARROW_RIGHT: 306, // Arrow right
		PL_TVMW_KEY_WHEEL_LEFT: 307, // Wheel left
		PL_TVMW_KEY_WHEEL_RIGHT: 308, // Wheel right
		PL_TVMW_KEY_PANNEL_ENTER: 309, // Pannel enter
		PL_TVMW_KEY_RED: 400, // Color key red
		PL_TVMW_KEY_GREEN: 401, // Color key green
		PL_TVMW_KEY_YELLOW: 402, // Color key yellow
		PL_TVMW_KEY_CYAN: 403, // Color key cyan
		PL_TVMW_KEY_REWARD: 500, // Color key reward
		PL_TVMW_KEY_PAUSE: 501, // Playback key pause
		PL_TVMW_KEY_FOWARD: 502, // Playback key foward
		PL_TVMW_KEY_PLAY: 503, // Playback key play
		PL_TVMW_KEY_STOP: 504, // Playback key stop
		PL_TVMW_KEY_REC: 505, // Playback key rec
		PL_TVMW_KEY_INFO: 600, // Info
		PL_TVMW_KEY_TOOLS: 601, // Tools
		PL_TVMW_KEY_INFOLINK: 602, // Infolink (Info.L)
		PL_TVMW_KEY_EMODE: 603, // Entertainment mode
		PL_TVMW_KEY_DATA: 604, // Data
		PL_TVMW_KEY_DMA: 605, // DMA
		PL_TVMW_KEY_CONTENTS: 606, // Contents
		PL_TVMW_KEY_MENU: 607, // Menu
		PL_TVMW_KEY_WISELINK: 608, // Wiselink (W.Link)
		PL_TVMW_KEY_SOURCE: 609, // Source
		PL_TVMW_KEY_POWER: 610, // Power
		PL_TVMW_KEY_TV: 611, // TV
		PL_TVMW_KEY_PANNEL_SOURCE: 612, // Pannel source
		PL_TVMW_KEY_PANNEL_MENU: 613, // Pannel menu
		PL_TVMW_KEY_PANNEL_POWER: 614, // Pannel power
		PL_TVMW_KEY_TTX_MIX: 650, // TTX MIX
		PL_TVMW_KEY_GUIDE: 651, // Key guide
		PL_TVMW_KEY_SUBTITLE: 652, // Subtitle
		PL_TVMW_KEY_ASPECT: 653, // Aspect
		PL_TVMW_KEY_DOLBY_SRR: 654, // Dolby surround
		PL_TVMW_KEY_MTS: 655, // MTS
		PL_TVMW_KEY_REPEAT: 656, // Repeat
		PL_TVMW_KEY_STEP: 1023, // STEP
		PL_TVMW_KEY_PIP_ONOFF: 1032, // PIP_ONOFF
		PL_TVMW_KEY_AD: 1039, // AD
		PL_TVMW_KEY_PMODE: 1040, // PMODE
		PL_TVMW_KEY_SMODE: 1043, // SMODE
		PL_TVMW_KEY_PIP_CHUP: 1050, // PIP_CHUP
		PL_TVMW_KEY_PIP_CHDOWN: 1051, // PIP_CHDOWN
		PL_TVMW_KEY_FF_: 1078, // _FF_
		PL_TVMW_KEY_REWIND_: 1080, // REWIND
		PL_TVMW_KEY_DISC_MENU: 1086, // DISC_MENU
		PL_TVMW_KEY_SUB_TITLE: 1089, // SUB_TITLE
		PL_TVMW_KEY_SLEEP: 1097, // SLEEP
		PL_TVMW_KEY_PCMODE: 1105, // PCMODE
		PL_TVMW_KEY_AV1: 1132, // AV1
		PL_TVMW_KEY_SVIDEO1: 1133, // SVIDEO1
		PL_TVMW_KEY_COMPONENT1: 1134, // COMPONENT1
		PL_TVMW_KEY_COMPONENT2: 1136, // COMPONENT2
		PL_TVMW_KEY_HDMI: 1139, // HDMI
		PL_TVMW_KEY_AV2: 1165, // AV2
		PL_TVMW_KEY_AV3: 1166, // AV3
		PL_TVMW_KEY_SVIDEO2: 1167, // SVIDEO2
		PL_TVMW_KEY_SVIDEO3: 1168, // SVIDEO3
		PL_TVMW_KEY_ZOOM2: 1169, // ZOOM2
		PL_TVMW_KEY_PANORAMA: 1170, // PANORAMA
		PL_TVMW_KEY_4_3: 1171, // 4_3
		PL_TVMW_KEY_16_9: 1172, // 16_9
		PL_TVMW_KEY_DYNAMIC: 1173, // DYNAMIC
		PL_TVMW_KEY_STANDARD: 1174, // STANDARD
		PL_TVMW_KEY_MOVIE1: 1175, // MOVIE1
		PL_TVMW_KEY_CUSTOM: 1176, // CUSTOM
		PL_TVMW_KEY_HDMI1: 1184, // HDMI1
		PL_TVMW_KEY_HDMI2: 1205, // HDMI2
		PL_TVMW_KEY_HDMI3: 1206, // HDMI3
		PL_TVMW_KEY_HDMI4: 1210, // HDMI4
		PL_TVMW_KEY_TILT: 1217, // TILT
		PL_TVMW_KEY_EZ_VIEW: 1218, // EZ_VIEW
		PL_TVMW_KEY_3D: 1219, // 3D
		PL_TVMW_KEY_AUTO_ARC_PVR_RECORDING_TEST: 1220, // AUTO_ARC_PVR_RECORDING_TEST
		PL_TVMW_KEY_AUTO_ARC_PVR_PLAY_TEST: 1221, // AUTO_ARC_PVR_PLAY_TEST
		PL_TVMW_KEY_AUTO_ARC_PVR_DELETE_ALL: 1222, // AUTO_ARC_PVR_DELETE_ALL
		PL_TVMW_KEY_AUTO_ARC_HOTEL_INTERACTIVE: 1223, // AUTO_ARC_HOTEL_INTERACTIVE
		PL_TVMW_KEY_D_LIST_UP: 1224, // D_LIST_UP
		PL_TVMW_KEY_D_LIST_DOWN: 1225, // D_LIST_DOWN
		PL_TVMW_KEY_D_ONDEMAND: 1226, // D_ONDEMAND
		PL_TVMW_KEY_D_PANNEL_VIRTUAL_ENTER: 1227, // D_PANNEL_VIRTUAL_ENTER
		PL_TVMW_KEY_D_PANNEL_VIRTUAL_SOURCE: 1228, // D_PANNEL_VIRTUAL_SOURCE
		PL_TVMW_KEY_D_REC_PAUSE: 1229, // D_REC_PAUSE
		PL_TVMW_KEY_D_CONTINUED_FF: 1230, // D_CONTINUED_FF
		PL_TVMW_KEY_D_CONTINUED_RW: 1231, // D_CONTINUED_RW
		PL_TVMW_KEY_D_CONTINUED_OFF: 1232, // D_CONTINUED_OFF
		PL_TVMW_KEY_D_CANCEL: 1233, // D_CANCEL
		PL_TVMW_KEY_D_TITLE_MENU: 1234, // D_TITLE_MENU
		PL_TVMW_KEY_D_REPEAT_AB: 1235, // D_REPEAT_AB
		PL_TVMW_KEY_D_AUDIO: 1236, // _D_AUDIO
		PL_TVMW_KEY_D_REC_MODE: 1237, // D_REC_MODE
		PL_TVMW_KEY_D_USB: 1238, // D_USB
		PL_TVMW_KEY_D_HDD: 1239, // D_HDD
		PL_TVMW_KEY_D_DISC: 1240, // D_DISC
		PL_TVMW_KEY_D_TEST: 1241, // D_TEST
		PL_TVMW_KEY_D_COPY: 1242, // D_COPY
		PL_TVMW_KEY_D_FRONT_PLAY: 1243, // D_FRONT_PLAY
		PL_TVMW_KEY_D_FRONT_STOP: 1244, // D_FRONT_STOP
		PL_TVMW_KEY_D_FRONT_FF: 1245, // D_FRONT_FF
		PL_TVMW_KEY_D_FRONT_RW: 1246, // D_FRONT_RW
		PL_TVMW_KEY_D_FRONT_REC: 1247, // D_FRONT_REC
		PL_TVMW_KEY_D_VERSION_DISPLAY_KEU: 1248, // D_VERSION_DISPLAY_KEU
		PL_TVMW_KEY_D_VIEW_MODE: 1249, // D_VIEW_MODE
		PL_TVMW_KEY_D_COLD_START: 1250 // D_COLD_START
	};

	var TVMWMap = {};
	TVMWMap[sf.key.N1]			=	TVMW_KEY.PL_TVMW_KEY_1;
	TVMWMap[sf.key.N2]			=	TVMW_KEY.PL_TVMW_KEY_2;
	TVMWMap[sf.key.N3]			=	TVMW_KEY.PL_TVMW_KEY_3;
	TVMWMap[sf.key.N4]			=	TVMW_KEY.PL_TVMW_KEY_4;
	TVMWMap[sf.key.N5]			=	TVMW_KEY.PL_TVMW_KEY_5;
	TVMWMap[sf.key.N6]			=	TVMW_KEY.PL_TVMW_KEY_6;
	TVMWMap[sf.key.N7]			=	TVMW_KEY.PL_TVMW_KEY_7;
	TVMWMap[sf.key.N8]			=	TVMW_KEY.PL_TVMW_KEY_8;
	TVMWMap[sf.key.N9]			=	TVMW_KEY.PL_TVMW_KEY_9;
	TVMWMap[sf.key.N0]			=	TVMW_KEY.PL_TVMW_KEY_0;
	TVMWMap[sf.key.PRECH]		=	TVMW_KEY.PL_TVMW_KEY_CH_PREV;
	TVMWMap[sf.key.VOL_UP]		=	TVMW_KEY.PL_TVMW_KEY_VOL_UP;
	TVMWMap[sf.key.VOL_DOWN]	=	TVMW_KEY.PL_TVMW_KEY_VOL_DOWN;
	TVMWMap[sf.key.CH_UP]		=	TVMW_KEY.PL_TVMW_KEY_CH_UP;
	TVMWMap[sf.key.CH_DOWN]		=	TVMW_KEY.PL_TVMW_KEY_CH_DOWN;
	TVMWMap[sf.key.MUTE]		=	TVMW_KEY.PL_TVMW_KEY_MUTE;
	TVMWMap[sf.key.SOURCE]		=	TVMW_KEY.PL_TVMW_KEY_SOURCE;
	TVMWMap[sf.key.CHLIST]		=	TVMW_KEY.PL_TVMW_KEY_CH_LIST;
	TVMWMap[sf.key.MENU]		=	TVMW_KEY.PL_TVMW_KEY_MENU;
	TVMWMap[sf.key.WLINK]		=	TVMW_KEY.PL_TVMW_KEY_WISELINK;
	TVMWMap[sf.key.ENTER]		=	TVMW_KEY.PL_TVMW_KEY_ENTER;
	TVMWMap[sf.key.RETURN]		=	TVMW_KEY.PL_TVMW_KEY_RETURN;
	TVMWMap[sf.key.INFO]		=	TVMW_KEY.PL_TVMW_KEY_INFO;
	TVMWMap[sf.key.EXIT]		=	TVMW_KEY.PL_TVMW_KEY_EXIT;
	TVMWMap[sf.key.UP]			=	TVMW_KEY.PL_TVMW_KEY_ARROW_UP;
	TVMWMap[sf.key.DOWN]		=	TVMW_KEY.PL_TVMW_KEY_ARROW_DOWN;
	TVMWMap[sf.key.LEFT]		=	TVMW_KEY.PL_TVMW_KEY_ARROW_LEFT;
	TVMWMap[sf.key.RIGHT]		=	TVMW_KEY.PL_TVMW_KEY_ARROW_RIGHT;
	TVMWMap[sf.key.RED]			=	TVMW_KEY.PL_TVMW_KEY_RED;
	TVMWMap[sf.key.GREEN]		=	TVMW_KEY.PL_TVMW_KEY_GREEN;
	TVMWMap[sf.key.YELLOW]		=	TVMW_KEY.PL_TVMW_KEY_YELLOW;
	TVMWMap[sf.key.BLUE]		=	TVMW_KEY.PL_TVMW_KEY_CYAN;
	TVMWMap[sf.key.INFOLINK]	=	TVMW_KEY.PL_TVMW_KEY_INFOLINK;
	TVMWMap[sf.key.DMA]			=	TVMW_KEY.PL_TVMW_KEY_DMA;
	TVMWMap[sf.key.EMODE]		=	TVMW_KEY.PL_TVMW_KEY_EMODE;
	TVMWMap[sf.key.CONTENTS]	=	TVMW_KEY.PL_TVMW_KEY_CONTENTS;
	TVMWMap[sf.key.FAVCH]		=	TVMW_KEY.PL_TVMW_KEY_CH_FAV;
	TVMWMap[sf.key.REW]			=	TVMW_KEY.PL_TVMW_KEY_REWARD;
	TVMWMap[sf.key.PAUSE]		=	TVMW_KEY.PL_TVMW_KEY_PAUSE;
	TVMWMap[sf.key.FF]			=	TVMW_KEY.PL_TVMW_KEY_FOWARD;
	TVMWMap[sf.key.REC]			=	TVMW_KEY.PL_TVMW_KEY_REC;
	TVMWMap[sf.key.PLAY]		=	TVMW_KEY.PL_TVMW_KEY_PLAY;
	TVMWMap[sf.key.STOP]		=	TVMW_KEY.PL_TVMW_KEY_STOP;
	TVMWMap[sf.key.PANEL_CH_UP]		=	TVMW_KEY.PL_TVMW_KEY_CH_PANNEL_UP;
	TVMWMap[sf.key.PANEL_CH_DOWN]	=	TVMW_KEY.PL_TVMW_KEY_CH_PANNEL_DOWN;
	TVMWMap[sf.key.PANEL_VOL_UP]	=	TVMW_KEY.PL_TVMW_KEY_VOL_PANNEL_UP;
	TVMWMap[sf.key.PANEL_VOL_DOWN]	=	TVMW_KEY.PL_TVMW_KEY_VOL_DOWN;
	TVMWMap[sf.key.PANEL_ENTER]		=	TVMW_KEY.PL_TVMW_KEY_PANNEL_ENTER;
	TVMWMap[sf.key.PANEL_SOURCE]	=	TVMW_KEY.PL_TVMW_KEY_PANNEL_SOURCE;
	TVMWMap[sf.key.PANEL_MENU]		=	TVMW_KEY.PL_TVMW_KEY_PANNEL_MENU;
	TVMWMap[sf.key.PANEL_POWER]		=	TVMW_KEY.PL_TVMW_KEY_PANNEL_POWER;
	TVMWMap[sf.key.THREE_D]		=	TVMW_KEY.PL_TVMW_KEY_3D;
})(sf);

(function (sf) {
	sf.core._PCMode = (sf.env.getBrowser() == 'chrome' || sf.env.getBrowser() == 'safari');

	var dummyPlugins = {
		'AUDIO': {
			'GetPluginInfo': function () {return null;},
			'CheckExternalOutMode': function() {return null},
			'GetExternalOutMode': function() {return null},
			'GetOutputDevice': function() {return null},
			'GetSystemMute': function() {return null},
			'GetUserMute': function() {return null},
			'GetVolume': function() {return null},
			'IsActiveSourceOnCEC': function() {return null},
			'SetExternalOutMode': function() {return null},
			'SetSystemMute': function() {return null},
			'SetTVSourceOnCEC': function() {return null},
			'SetUserMute': function() {return null},
			'SetVolumeWithKey': function() {return null}
		},
		'APPCOMMON': {
			'GetPluginInfo': function () {return null;},
			'CheckReservedKey': function(){return null},
			'IsKeyRegister': function(){return null},
			'RegisterAllKey': function(){return null},
			'RegisterColorKey': function(){return null},
			'RegisterKey': function(){return null},
			'RegisterNaviKey': function(){return null},
			'RegisterNumKey': function(){return null},
			'RegisterPlaybackKey': function(){return null},
			'SendEvent_IME': function(){return null},
			'SendEvent_IME_Sync': function(){return null},
			'SendKeyToTVViewer': function(){return null},
			'SubscribeEvent': function(){return null},
			'UnregisterAllKey': function(){return null},
			'UnregisterColorKey': function(){return null},
			'UnregisterKey': function(){return null},
			'UnregisterNaviKey': function(){return null},
			'UnregisterNumKey': function(){return null},
			'UnregisterPlaybackKey': function(){return null},
			'UnsubscribeEvent': function(){return null},
			'SendDualTVWidgetViewInfo': function(){return null}
		},
		'DEVICE': {
			'GetPluginInfo': function () {return null;},
			'GetDisplayPanelType': function(){
				return 0;//PL_DEVICE_DPT_LCD
			},
			'GetModel': function(){
				return 'UNXXD8000_USA'
			},
			'GetRealModel ': function(){
				return 'UN46D8000_USA'
			}
		},
		'FRONTPANEL': {
			'GetPluginInfo': function () {return null;},
			'DisplayVFD_Show': function(){return null},
			'DisplayVFD_Time': function(){return null}
		},
		'IMAGEVIEWER': {
			'GetPluginInfo': function () {return null;},
			'ClearScreen': function(){return null},
			'GetPlayerVersion': function(){return null},
			'GetVideoHeight': function(){return null},
			'GetVideoWidth': function(){return null},
			'InitPlayer': function(){return null},
			'Play': function(){return null},
			'SetDisplayArea': function(){return null},
			'SetDisplayLock': function(){return null},
			'SetTransitionEffect': function(){return null},
			'ShowImage': function(){return null},
			'Stop': function(){return null},
			'OnBufferingComplete': null,
			'OnBufferingStart': null,
			'OnConnectionFailed': null,
			'OnNetworkDisconnected': null,
			'OnRenderError': null,
			'OnRenderingComplete': null,
			'OnStreamInfoReady': null,
			'OnStreamNotFound': null
		},
		'NETWORK': {
			'GetPluginInfo': function () {return null;},
			'CheckCableConnection': function(){return null},
			'CheckDNS': function(){return null},
			'CheckGateway': function(){return null},
			'CreatePlugin': function(){return null},
			'GetActiveType': function(){return null},
			'GetDNS': function(){return null},
			'GetGateway': function(){return null},
			'GetHostAddr': function(){return null},
			'GetHWaddr': function(){return null},
			'GetIpType': function(){return null},
			'GetNetMask': function(){return null},
			'GetStatus': function(){return null},
			'HttpTest': function(){return null},
			'IPConflictTest': function(){return null}
		},
		'NNAVI': {
			'GetPluginInfo': function () {return null;},
			'ActivateReady': function(){return null},
			'ActivateWithData': function(){return null},
			'ChangeWidgetManager': function(){return null},
			'GetAppKey': function(){return null},
			'GetDUID': function(){return null},
			'GetFirmware': function(){return null},
			'GetModelCode': function(){return null},
			'GetPath': function(){return null},
			'GetRemoconType': function(){return null},
			'GetServerType': function(){return null},
			'GetSupportPIG': function(){return null},
			'GetSystemVersion': function(){return null},
			'GetToken': function(){return null},
			'NeedThisRemoconKey': function(){return null},
			'ResetWidgetData': function(){return null},
			'SendEventToDevice': function(){return null},
			'SetBannerState': function(){return null}
		},
		'PLAYER': {
			'GetPluginInfo': function () {return null;},
			'ClearScreen': function(){return null},
			'GetAvailableBitrates': function(){return null},
			'GetCurrentBitrates': function(){return null},
			'GetDuration': function(){return null},
			'GetLiveDuration': function(){return null},
			'GetPlayerVersion': function(){return null},
			'GetVideoHeight': function(){return null},
			'GetVideoWidth': function(){return null},
			'InitPlayer': function(){return null},
			'JumpBackward': function(){return null},
			'JumpForward': function(){return null},
			'Pause': function(){return null},
			'Play': function(){return null},
			'Resume': function(){return null},
			'ResumePlay': function(){return null},
			'SetCropArea': function(){return null},
			'SetDisplayArea': function(){return null},
			'SetICT': function(){return null},
			'SetInitialBuffer': function(){return null},
			'SetInitialTimeOut': function(){return null},
			'SetMacrovision': function(){return null},
			'SetPendingBuffer': function(){return null},
			'SetPlaybackSpeed': function(){return null},
			'SetPlayerProperty': function(){return null},
			'SetTotalBufferSize': function(){return null},
			'SetVBIData': function(){return null},
			'StartPlayback': function(){return null},
			'Stop': function(){return null},
			'OnAdEnd': null,
			'OnAdStart': null,
			'OnAuthenticationFailed': null,
			'OnBufferingComplete': null,
			'OnBufferingProgress': null,
			'OnBufferingStart': null,
			'OnConnectionFailed': null,
			'OnCurrentPlayTime': null,
			'OnNetworkDisconnected': null,
			'OnRenderError': null,
			'OnRenderingComplete': null,
			'OnResolutionChanged': null,
			'OnStreamInfoReady': null,
			'OnStreamNotFound': null
		},
		'SCREEN': {
			'GetPluginInfo': function () {return null;},
			'Check3DEffectMode': function(){return null},
			'Flag3DEffectSupport': function(){return null},
			'Flag3DTVConnect': function(){return null},
			'Get3DEffectMode': function(){return null},
			'GetOption': function(){return null},
			'Set3DEffectMode': function(){return null},
			'SetOption': function(){return null}
		},
		'TV': {
			'GetPluginInfo': function () {return null;},
			'CheckPIP': function(){return null},
			'FlagStreamDST': function(){return null},
			'FlagStreamDST': function(){return null},
			'GetBDProductType': function(){return null},
			'GetCountry': function(){return null},
			'GetDisplayPanelType': function(){return null},
			'GetDST': function(){return null},
			'GetLanguage': function(){return null},
			'GetLanguageSet': function(){return null},
			'GetPIP': function(){return null},
			'GetPresentProgram_Duration': function(){return null},
			'GetPresentProgram_EndTime': function(){return null},
			'GetPresentProgram_StartTime': function(){return null},
			'GetPresentProgram_Title': function(){return null},
			'GetProductCode': function(){return null},
			'GetProductType': function(){return null},
			'GetProgram_Duration': function(){return null},
			'GetProgram_EndTime': function(){return null},
			'GetProgram_StartTime': function(){return null},
			'GetProgram_Title': function(){return null},
			'GetProgramList': function(){return null},
			'GetProgramList_Size': function(){return null},
			'GetTargetLocation': function(){return null},
			'GetTimeZone': function(){return null},
			'GetTimeZone_Offset': function(){return null},
			'SetCountry': function(){return null},
			'SetDST': function(){return null},
			'SetEvent': function(){return null},
			'SetLanguage': function(){return null},
			'SetPIP': function(){return null},
			'SetTimeZone': function(){return null},
			'SetWatchDog': function(){return null},
			'UnsetEvent': function(){return null},
			'OnEvent': null
		},
		'TVMW': {
			'GetPluginInfo': function () {return null;},
			'CheckReservedKey': function(){return null},
			'Deactivate': function(){return null},
			'GetActiveApp': function(){return null},
			'GetBGApp': function(){return null},
			'GetCountry': function(){return null},
			'GetLanguage': function(){return null},
			'GetProfile': function(){return null},
			'GetSource': function(){return null},
			'GetTVUseMode': function(){return null},
			'IsRegisteredKey': function(){return null},
			'RegForBGApp': function(){return null},
			'RegisterKey': function(){return null},
			'RegisterKeyGroup': function(){return null},
			'SendKeyToTVViewer': function(){return null},
			'SetProfile': function(){return null},
			'SetSource': function(){return null},
			'SetMediaSource': function () {return null},
			'SetWatchDog': function(){return null},
			'UnregForBGApp': function(){return null},
			'UnregisterKey': function(){return null},
			'UnregisterKeyGroup': function(){return null}
		},
		'WINDOW': {
			'GetPluginInfo': function () {return null;},
			'CheckScreenRect_PosSizeMode': function(){return null},
			'GetCurrentChannel_Major': function(){return null},
			'GetCurrentChannel_Minor': function(){return null},
			'GetCurrentChannel_Name': function(){return null},
			'GetCurrentChannel_OriginNetID': function(){return null},
			'GetCurrentChannel_ProgramNumber': function(){return null},
			'GetCurrentChannel_PTC': function(){return null},
			'GetCurrentChannel_ServiceName': function(){return null},
			'GetCurrentChannel_Type': function(){return null},
			'GetResolution': function(){return null},
			'GetScreenRect': function(){return null},
			'GetScreenRect_PosMode': function(){return null},
			'GetScreenRect_SizeMode': function(){return null},
			'GetSource': function(){return null},
			'GetState_Show': function(){return null},
			'SetChannel': function(){return null},
			'SetChannel_PTC': function(){return null},
			'SetChannel_Seek': function(){return null},
			'SetScreenRect': function(){return null},
			'SetScreenRect_PosSizeMode': function(){return null},
			'SetSource': function(){return null},
			'Show': function(){return null}
		}
	}

	if(sf.core._PCMode) {
		sf.core.plugin = sf.core.sefplugin = function (name) {
			return dummyPlugins[name.toUpperCase()];
		}

		sf.key.LEFT = 37;
		sf.key.RIGHT = 39;
		sf.key.UP = 38;
		sf.key.DOWN = 40;
		sf.key.ENTER = 13;
		sf.key.N0 = 48;
		sf.key.N1 = 49;
		sf.key.N2 = 50;
		sf.key.N3 = 51;
		sf.key.N4 = 52;
		sf.key.N5 = 53;
		sf.key.N6 = 54;
		sf.key.N7 = 55;
		sf.key.N8 = 56;
		sf.key.N9 = 57;
		sf.key.RETURN = 8;
		sf.key.RED = 65;
		sf.key.GREEN = 66;
		sf.key.YELLOW = 67;
		sf.key.BLUE = 68;
	}
})(sf);

alert('[AF core] core.js included');/**
 * Additions to AF Core API
 *
 * @author Maciej Stankiewicz <<a href='mailto:m.stankiewic@samsung.com'>m.stankiewic@samsung.com</a>>
 * @date 2011-11-17
 *
 *
 *******************************************************************************
 *
 * Copyright (c) 2011 Samsung Electronics, Visual Display Division.
 * All Rights Reserved.
 *
 ******************************************************************************/

/**
 * @Namespace AF Core API
 */
var sf = sf || {};/*
 * Title: Scene Manager
 * Definition of Apps Framework Scene Manager
 */

(function (sf) {
/*
   Class: sf.scene
   Scene Manager functions
*/
	var focusInterval = null;

	sf.scene = {
		_masterAnchorID: '_sf_master_anchor_',
		_insertMasterAnchor: function () {
			$('body').append('<a href="javascript:void(0)" id="' + sf.scene._masterAnchorID + '"></a>');
		},
		_setMasterAnchorHandler: function () {
			$('#' + sf.scene._masterAnchorID).keydown(function(){
				sf.scene._handleKeyDown();
			});
			/*
			$(document).bind('click', function () {
				sf.scene.returnFocus();
			});
			*/
		},
		/*
     	* Function: returnFocus
     	*	Returns focus to the framework scene manager.
     	*
     	* Parameters:
     	* 	N/A
     	*
     	* Returns:
     	* 	nothing
     	*
     	* Description:
     	* 	When other anchor is focused, call this function to return the focus to Scene Manager.
    	*/
		returnFocus : function () {
		    alert('[AF core] sf.scene.returnFocus()');
			var anchor = document.getElementById(sf.scene._masterAnchorID);
			if (anchor) {
				anchor.focus();
			}

			//if (sf.core._PCMode && !focusInterval) {
			//	this._setFocusTimer();
			//}
		},

		_blur: function () {
			alert('[AF core] sf.scene._blur()');
			//this._removeFocusTimer();
		},

		_setFocusTimer: function () {
			alert("[AF scene] _setFocusTimer()");
			focusInterval = setInterval(function () {
				sf.scene.returnFocus();
			}, 2000);
		},
		_removeFocusTimer: function () {
			alert("[AF scene] _removeFocusTimer()");
			if(focusInterval) {
				clearInterval(focusInterval);
				focusInterval = null;
			}
		},

		/*
		 * Function: get
		 *	Gets the Scene instance.
		 *
		 * Parameters:
		 * 	o (String) key	- Scene id
		 *
		 * Returns:
		 * 	Object - Scene instance
		 */
		get : function(key) {
			var retValue = storage.get(key);
			alert('[AF scene] sf.scene.get('+key+') returns ' + (retValue ? key : null));
			return retValue;
		},

		/*
		 * Function: show
		 *	Shows the Scene.
		 *
		 * Parameters:
		 * 	o (String) key		- Scene id
		 * 	o (Any types) data	- Data to pass to the Scene. use array or javascript object to pass multiple values. (Optional)
		 *
		 * Returns:
		 * 	nothing
		 */
		show : function (sceneID, data) {
			alert('[AF scene] sf.scene.show('+sceneID+')');

			var scene = this.get(sceneID);
			if (!scene) {
				alert('[AF scene] Invalid scene Id : ' + sceneID);
				return;
			}

			var el = document.getElementById('Scene'+sceneID);
			if(!el) {
				var filePath = 'app/htmls/' + sceneID + '.html';
				var sceneHTML = sf.core.readFile(filePath);

				var scenediv = document.createElement('DIV');
                scenediv.id = 'Scene'+ sceneID;
				scenediv.innerHTML = sceneHTML;

				document.body.appendChild(scenediv);
				scene.initialize();
				el = document.getElementById('Scene'+sceneID);
			}
			el.style.display = 'block';

			scene.handleShow(data);
		},

		/*
		 * Function: hide
		 *	Hides the Scene.
		 *
		 * Parameters:
		 * 	o (String) key		- Scene id
		 * 	o (Any type) data	- Data to pass to the Scene. use array or javascript object to pass multiple values (optional)
		 *
		 * Returns:
		 * 	nothing
		 */
		hide : function (sceneID, data) {
			alert('[AF scene] sf.scene.hide('+sceneID+')');
			var scene = this.get(sceneID);
			if (!scene) {
				alert('[AF scene] Invalid scene Id : ' + sceneID);
				return;
			}

			var id = 'Scene'+sceneID;
			var el = document.getElementById(id);
			if(el) {
				el.style.display = 'none';
			}
			else {
				alert('[AF scene] cannot find div : ' + id);
			}
			scene.handleHide(data);
		},

		/*
		 * Function: focus
		 *	Binds the framework’s keyHandler to the Scene keyHandler. The focused Scene keyHandler is used when remote key pressed.
		 *
		 * Parameters:
		 * 	o (String) key		- (string) Scene id
		 * 	o (anytype) data	- (any type) data to pass to the Scene. Use array or javascript object to pass multiple values (optional)
		 *
		 * Returns:
		 * 	nothing
		 */
		focus : function (sceneID, data) {
			alert('[AF scene] sf.scene.focus('+sceneID+')');
			if(!this.getVisible(sceneID)) {
				alert('\t'+sceneID+': This scene is not shown yet. Please show this scene first.');
				return;
			}

			var scene = this.get(sceneID);
			if (!scene) {
				alert('[AF scene] Invalid scene Id : ' + sceneID);
				return;
			}

			var oldScene = curFocused;
			if (oldScene) {
				oldScene.handleBlur();
			}
			curFocused = scene;
			scene.handleFocus(data);
		},

		init: function (callback) {
			alert('[AF scene] sf.scene.init()');
			sf.scene.loadScenes(function(){
				// insert & focus anchor tag
				sf.scene._insertMasterAnchor();
				sf.scene._setMasterAnchorHandler();
				sf.scene.returnFocus();
				if(callback && callback instanceof Function) {
					callback();
				}
			});
		},

		loadScenes: function (callback) {
			alert('[AF scene] sf.scene.loadScenes()');
			var scenes, files;
			var conf = sf.core.getAppConf();
			scenes = conf.scenes || [];

			// load js, css files
			var arrFiles = [];

			// Resolutions 옵션의 처리
			var resolutions = conf.resolutions;
			var resolutionDir = '';

			alert("[AF scene] current resolution : " + sf.env.getResolution());
			alert("[AF scene] supported resolutions : " + resolutions);

			if(resolutions && resolutions instanceof Array && resolutions.length) {	// Resolution 설정이 있는경우
				if(sf.util.inArray(sf.env.getResolution(), resolutions)) {	// 지원하는 resolution이 있을경우
					resolutionDir = sf.env.getResolution() + '/';
				}
				else {	// 지원하지 않으면 가장 작은 Resolution을 로드. (적어도 화면에 다 보일수는 있도록.)
					resolutionDir = sf.util.inArray('540p', resolutions)?'540p/' : sf.util.inArray('720p', resolutions)?'720p/' : sf.util.inArray('1080p', resolutions)?'1080p/' : '';
				}
			}
			else {	// Resolution 설정이 없는경우는 그냥 stylesheets에서 로드함
				resolutionDir = '';
			}
			alert("[AF scene] applied resolution : " + resolutionDir);

			// Push the Scene's JS files and load the Scene's CSS files
			$.each (scenes, function (index, value) {
				arrFiles.push ('app/scenes/' + value + '.js');
				sf.core.loadCSS ('app/stylesheets/' + resolutionDir + value + '.css');
			});
			// Scene JS파일 Load.
			sf.core.loadJS(arrFiles, function () {
				// create scene instance
				$.each (scenes, function (index, SceneID) {
					alert("SCENE: " + SceneID);
					var sceneClass = 'Scene' + SceneID;
					try {
						var instance = eval('new ' + sceneClass + '()');
						storage.set(SceneID, instance);
					}
					catch (e) {
						alert('[AF scene] !!!!! cannot create scene instance !!!!!');
						alert('[AF scene] ' + e.toString());
					}
				});

				if (typeof callback == 'function') {
					callback();
				}
			});
		},

		/*
		 * Function: getFocused
		 *	Get current focused Scene ID. If the Scene ID is specified, Get whether the Scene is focused .
		 *
		 * Parameters:
		 * 	o sceneid		- (String) (Optional) If this "sceneid" is specified, Get whether the Scene is focused or not.
		 *
		 * Returns:
		 * 	(String) - Current focused scene ID. (if "sceneid" is not passed)
		 * 	(Boolean) - Whether the scene is focused or not. (if "sceneid" is passed)
		 */
		getFocused: function (sceneid) {
			if (sceneid) {
				return curFocused.id == sceneid;
			}
			else {
				return curFocused.id;
			}
		},

		/*
		 * Function: getVisible
		 *	Get whether the Scene is visible or not.
		 *
		 * Parameters:
		 * 	o sceneID		- (String) Scene ID
		 *
		 * Returns:
		 * 	(Boolean) - Whether the scene is visible or not.
		 */
		getVisible: function (sceneID) {
			alert("[AF scene] getVisible("+sceneID+")")
			if (!sceneID) {
				return false;
			}
			var el = document.getElementById('Scene'+sceneID);
			return el && el.style.display == 'block';
		},

		getState: function (sceneID) {
			alert("[AF scene] getState("+sceneID+")")
			if(this.getFocused(sceneID)) {
				return 'focused';
			}
			else {
				return this.getVisible(sceneID) ? 'shown' : 'hidden';
			}
		},

		setKeyHandler: function (keyCode, handler) {
			alert("[AF scene] setKeyHandler("+keyCode+","+handler+")");
			if(!keyCode || !handler) {
				alert("\t" + "ERROR: keyCode or handler is not defined: " + keyCode, handler);
				return;
			}
			if(typeof handler != 'function') {
				alert("\t" + "ERROR: handler must be Function type: " + (typeof handler));
				return;
			}
			oKeyHandler[keyCode] = handler;
		},
		getKeyHandler: function (keyCode) {
			return oKeyHandler[keyCode] || null;
		},
		unsetKeyHandler: function (keyCode) {
			alert("[AF scene] unsetKeyHandler("+keyCode+")");
			if(!keyCode) {
				alert("\t"+"Remove all handlers");
				oKeyHandler = {};
			}
			else if (typeof keyCode != 'number') {
				// ERROR
				alert("\t"+"ERROR: keyCode must be Number type");
				return;
			}
			else if(oKeyHandler[keyCode]){
				alert("\t"+"Remove handler for key code "+ keyCode);
				delete oKeyHandler[keyCode];
			}
			else {
				// ERROR
				alert("\t"+"ERROR: No binded handler for key code "+ keyCode);
				return;
			}
		},

		// 내부 관리용 함수

		_handleKeyDown : function (specifiedKeyCode) {	// keyCode is optional
			var keycode = specifiedKeyCode || event.keyCode;
			alert('[AF scene] sf.scene._handleKeyDown('+keycode+')');
			keycode = sf.core.mapAliasedKeys(keycode);

			// Custom 핸들러가 있다면 해당 핸들러를 호출
			var customHandler = this.getKeyHandler(keycode);
			if(customHandler && customHandler instanceof Function) {
				customHandler(keycode);
				return;
			}

			// 현재 포커스된 Scene의 handleKeyDown 호출
			var scene = curFocused;
			if (scene && typeof scene.handleKeyDown == 'function') {
				scene.handleKeyDown(keycode);
			}
			else {
				alert('[AF scene] cannot run current focused scene\'s key handler.');
				alert('[AF scene] scene : ' + scene);
				if (scene) alert('[AF scene] key handler : ' + scene.handleKeyDown);
			}
		}
	};

	/////////////////
	///  Private  ///
	/////////////////
	var oKeyHandler = {};

	var storage = new Storage();	// app의 scene instance를 저장
	var curFocused = '';		// 현재 focus를 가지고 있는 scene을 저장

	function Storage () {
		var data = {};
		this.get = function (key) {
			var retValue = null;
			if (data[key]) {
				retValue = data[key];
			}
			alert('[AF scene] storage.get('+key+') returns ' + retValue);
			return retValue;
		}

		this.set = function (key, value) {
			data[key] = value;
			alert('[AF scene] Storage.set('+key+')');
		}

		this.add = function (key, prop, value) {
			if (data[key]) {
				data[key][prop] = value;
			}
			else {
				alert('[AF scene] cannot add the property.')
			}
		}
		// for debugging
		this._getData = function () {
			return data;
		}
	}
})(sf);

alert('[AF scene] Scene.js included');/*
 * Title: Util API
 * 	Definition of utilities for application.
 */
sf = sf || {};
(function (sf) {
	function debugLib(msg) {
		return;
		var tab = '';
		for(var i=0; i<depth; i++)	tab += '\t';
		alert(tab+msg);
	}
	var depth = 0;

	/*
	   Namespace: sf.util
	   	Apps Framework utility functions
	*/
	sf.util = {
		/*
		 * Function: xml2json
		 * 	Convert XML DOM object to json string.
		 *
		 * Parameters:
		 * 	o dom	- (Object) XML Dom object to be converted.
		 *
		 * Returns:
		 * 	(String) Converted JSON string
		 */
		xml2json: function(dom){
			var obj = this.xml2obj(dom);
			return this.obj2json(obj);
		},

		/*
		 * Function: xml2obj
		 * 	Convert XML DOM object to Object.
		 *
		 * Parameters:
		 * 	o dom	- (Object) XML Dom object to be converted.
		 *
		 * Returns:
		 * 	(Object) Converted object
		 *
		 * Description:
		 * 	All attributes are added as property of the converted object. And the text will added with property named "text".
		 *
		 *  *XML*
		 *  (start code)
		 *  <?xml version="1.0" encoding="UTF-8"?>
		 *  <title><![CDATA[TEST TITLE]]></title>
		 *  <link>http://www.samsung.com</link>
		 *  <description><![CDATA[Samsung]]></description>
		 *  <items>
		 *  	<item id="1">ITEM1</item>
		 *  	<item id="2">ITEM2</item>
		 *  </items>
		 *  (end)
		 *
		 *  *JSON of converted object*
		 *  (start code)
		 *  {
		 *  	"title" : "TEST TITLE",
		 *  	"link" : "http://www.samsung.com",
		 *  	"description" : "Samsung",
		 *  	"items" : {
		 *  		"item" : [{
		 *  			"id" : "1",
		 *  			"text" : "ITEM1"
		 *  		}, {
		 *  			"id" : "2",
		 *  			"text" : "ITEM2"
		 *  		}]
		 *  	}
		 *  }
		 *  (end)
		 */
		xml2obj: function(dom){
			var obj = {};
			var bNothing = true;
			depth++;
			debugLib("xml2obj : " + dom.tagName + "(" + dom.nodeType + ")");
			if(dom.nodeType == 9) {	// Document Element
				depth--;
				return this.xml2obj(dom.documentElement);
			}
			else if(dom.nodeType == 1) {	// Element
				var valueCount = 0;
				for (var node = dom.firstChild; node; node = node.nextSibling) {
					if ((node.nodeType == 3 && node.nodeValue.match(/[^ \f\n\r\t\v]/)) || node.nodeType == 4) {
						valueCount++;
					}
				}
				debugLib("valueCount = " + valueCount);
				for (var i = 0; i < dom.attributes.length; i++) {
					debugLib("attribute " + dom.attributes[i].nodeName + " = " + dom.attributes[i].nodeValue);
					obj[dom.attributes[i].nodeName] = dom.attributes[i].nodeValue;
					bNothing = false;
				}
				for (var node = dom.firstChild; node; node = node.nextSibling) {
					debugLib("Child node : " + node.nodeType);
					if (node.nodeType == 3 && node.nodeValue.match(/[^ \f\n\r\t\v]/)) { // text
						if (valueCount == 1 && dom.attributes.length == 0) {
							obj = node.nodeValue;
						}
						else {
							obj['text'] = node.nodeValue;
						}
						bNothing = false;
					}
					else if(node.nodeType == 4) { // CDATA
						if (valueCount == 1 && dom.attributes.length == 0) {
							obj = node.nodeValue;
						}
						else {
							obj['cdata'] = node.nodeValue;
						}
						bNothing = false;
					}
					else if (node.nodeType != 3) {
						var subobj = this.xml2obj(node);
						if (subobj) {
							if (obj[node.tagName]) {
								if (obj[node.tagName] instanceof Array) {
									obj[node.tagName].push(subobj);
								}
								else {
									obj[node.tagName] = [obj[node.tagName]];
									obj[node.tagName].push(subobj);
								}
							}
							else {
								obj[node.tagName] = subobj;
							}
							bNothing = false;
						}
						else {
							debugLib("this is nothing")
						}
					}
				}
			}
			depth--;
			return bNothing ? null : obj;
		},

		/*
		 * Function: obj2json
		 * 	Convert JavaScript Object object to JSON string.
		 *
		 * Parameters:
		 * 	o obj	- (Object) Object to be converted.
		 *
		 * Returns:
		 * 	(String) Converted JSON string
		 */
		obj2json: function(obj){
			var str = '';
			if (obj.constructor == Array) {
				str += '[';
				for (var i = 0; i < obj.length; i++) {
					str += i != 0 ? ', ' : '';
					str += this.obj2json(obj[i]);
				}
				str += ']';
			}
			else
				if (obj.constructor == String || obj.constructor == Date) {
					str += '"' + obj + '"';
				}
				else
					if (obj.constructor == Object) {
						str += '{';
						var cnt = 0;
						for (var elmt in obj) {
							str += cnt != 0 ? ', ' : '';
							str += '"' + elmt + '" : ' + this.obj2json(obj[elmt]);
							cnt++;
						}
						str += '}';
					}
					else
						if (obj.constructor == Number) {
							str += obj;
						}
			return str;
		},

		/*
		 * Function: dateFormat
		 * 	Convert JavaScript Data instance to formatted string.
		 *
		 * Paramters:
		 * 	o date	- (Date) Date instance to convert
		 * 	o formatStr	- (String) Format string. Please refer to Description section below.
		 *
		 * Returns:
		 * 	(String) Formatted string
		 *
		 * Description:
		 * 	Format string consists of these characters.
		 *
		 * 	*Day*
		 *
		 * 	d	- Day of the month, 2 digits with leading zeros.	01 through 31
		 * 	D	- A string of a day with three letters.	"Mon" through "Sun"
		 * 	j	- Day of the month without zero.	1 through 31
		 * 	l	- (lower case of L) Full string of the day of the week.	"Sunday" through "Saturday"
		 * 	S	- The day of the month with st, nd, rd or th suffix. 2 characters.	1st, 2nd, 3rd through 31st
		 * 	w	- Number of the day of the week.	0(Sunday) through 6(Saturday)
		 *
		 * 	*Month*
		 *
		 * 	F	- A string of a month, such as "January" or "March".	"January" through "December"
		 * 	m	- Month of 2 digits with zero.	01 through 12
		 * 	M	- Abbreviated string of a month, three letters.	Jan through Dec
		 * 	n	- Numeric month, without zero.	1 through 12
		 * 	t	- Number of days in the month.	28 through 31
		 *
		 * 	*Year*
		 *
		 * 	Y	- Numeric year, 4 digits.	Examples: 1999 through 2011
		 * 	y	- Numeric year, 2 digits.	Examples: 99 through 11
		 *
		 * 	*Time*
		 *
		 * 	a	- Lowercase AM and PM.	am or pm
		 * 	A	- Uppercase AM and PM.	AM or PM
		 * 	g	- 12-hour format of an hour without zero.	1 through 12
		 * 	G	- 24-hour format of an hour without zero.	0 through 23
		 * 	h	- 12-hour format of an hour with zero.	01 through 12
		 * 	H	- 24-hour format of an hour with zero.	00 through 23
		 * 	i	- Minutes with zero.	00 through 59
		 * 	s	- Seconds with zero.	00 through 59
		 * 	u	- Milliseconds	Example: 123456
		 *
		 * 	*Time zone*
		 *
		 * 	T	- Time difference.	Example: +09:00 for Seoul
		 */
		dateFormat: function (date, formatStr) {
			var _DATE_ = date;
			var aMonthNM = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			var aMonthNM_abbr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			var aDaysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
			var aDayNM = ['Sunday', 'Monday', 'Thuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			var aDayNM_abbr = ['Sun', 'Mony', 'Thu', 'Wed', 'Thr', 'Fri', 'Sat'];
			// Day
			var str = formatStr.replace(/(d|D|j|l|N|S|w|z)/g, function($1){
				switch($1) {
					case 'd':	return addZero(_DATE_.getDate(), 2);
					case 'D':	return aDayNM_abbr[_DATE_.getDay()];
					case 'j':	return _DATE_.getDate();
					case 'l':	return aDayNM[_DATE_.getDay()];
					case 'S':	return addTh(_DATE_.getDate());
					case 'w':	return _DATE_.getDay();
				}
				return '';
			});

			// Month
			str = str.replace(/(F|m|M|n|t)/g, function($1){
				switch($1) {
					case 'F':	return aMonthNM[_DATE_.getMonth()];
					case 'm':	return addZero(_DATE_.getMonth()+1, 2);
					case 'M':	return aMonthNM_abbr[_DATE_.getMonth()];
					case 'n':	return (_DATE_.getMonth()+1);
					case 't':	return aDaysInMonth[_DATE_.getMonth()] + (_DATE_.getFullYear()%4==0 && _DATE_.getFullYear()%100!=0 || _DATE_.getFullYear()%400==0)&&_DATE_.getMonth()==1?1:0;
				}
				return '';
			});

			// Year
			str = str.replace(/(Y|y)/g, function($1){
				switch($1) {
					case 'Y':	return _DATE_.getFullYear();
					case 'y':	return (_DATE_.getFullYear()+'').substr(2,2);
				}
				return '';
			});

			// Time
			str = str.replace(/(a|A|g|G|h|H|i|s|u)/g, function($1){
				switch($1) {
					case 'a':	return _DATE_.getHours()<12?'am':'pm';
					case 'A':	return _DATE_.getHours()<12?'AM':'PM';
					case 'g':	return _DATE_.getHours()%12;
					case 'G':	return _DATE_.getHours();
					case 'h':	return addZero(_DATE_.getHours()%12, 2);
					case 'H':	return addZero(_DATE_.getHours(), 2);
					case 'i':	return addZero(_DATE_.getMinutes(), 2);
					case 's':	return addZero(_DATE_.getSeconds(), 2);
					case 'u':	return _DATE_.getMilliseconds();
				}
				return '';
			});

			// Time Zone
			str = str.replace(/(T)/g, function($1){
				switch($1) {
					case 'T':
						var diff = _DATE_.getTimezoneOffset()/60;	// if the timezone is -5, diff is 5
						var plus = diff<=0?'+':'-';
						diff = Math.abs(diff);
						var diffHour = Math.floor(diff);
						var diffMin = (diff-diffHour)*60;
						diffHour = (diffHour<10?'0':'') + diffHour;
						diffMin = (diffMin<10?'0':'') + diffMin;
						return plus+diffHour+':'+diffMin;
				}
				return '';
			});

			return str;

			function addZero (str, length) {
				str += '';
				for(var i=0;str.length<length; i++)	str = '0'+str;
				return str;
			}

			function addTh(num) {
				var endnum = num-parseInt(num/10, 10)*10;
				var str = num + '' + (endnum<=1?'st':endnum==2?'nd':endnum==3?'rd':'th');
				return str;
			}
		},
		inArray: function(find, list) {
			alert("inArray("+find+", "+list+")");
			for(var i=0; i<list.length; i++) {
				if(list[i] == find) return true;
			}
			return false;
		}
	}
})(sf);

alert('[AF Util] util.js included');
/*
 * Title: Service API (Core)
 * Definition of Apps Framework Service API
 */

(function (sf) {
	/*
	 *	Namespace: sf.service
	 *		TV control
	 *
	 */

	sf.service = {
		/*
		 * Function: setVolumeControl
		 *   enable/disable TV volume control
		 *
		 * Parameters:
		 * 	o state - (Boolean) enable or disable controlling TV volume <default : disabled>
		 */
		setVolumeControl: function (state) {
			alert('[AF service] sf.service.setVolumeControl('+state+')');
			if (state === true) {
				sf.key.unregisterKey(sf.key.VOL_UP);
				sf.key.unregisterKey(sf.key.VOL_DOWN);
				sf.key.unregisterKey(sf.key.MUTE);
				var p = sf.core.plugin('NNAVI');
				if (p) {
					p.SetBannerState(sf.core.PL_NNAVI_STATE_BANNER_VOL);
				}
			}
			else if (state === false) {
				sf.key.registerKey(sf.key.VOL_UP);
				sf.key.registerKey(sf.key.VOL_DOWN);
				sf.key.registerKey(sf.key.MUTE);
				var p = sf.core.plugin('NNAVI');
				if (p) {
					p.SetBannerState(sf.core.PL_NNAVI_STATE_BANNER_NONE);
				}
			}
			else {
				alert('[AF service] parameter must be true or false : ' + state);
			}
		},
		/*
		 * Function: setChannelControl
		 *   enable/disable TV channel control. Source must be the TV.(not Media, PC etc.)
		 *
		 * Parameters:
		 * 	o state - (Boolean) enable or disable controlling TV channel <default : disabled>
		 */
		setChannelControl: function (state) {
			alert('[AF service] sf.service.setVolControl('+state+')');
			if (state === true) {
				sf.key.unregisterKey(sf.key.CH_UP);
				sf.key.unregisterKey(sf.key.CH_DOWN);
				var p = sf.core.plugin('NNAVI');
				if (p) {
					p.SetBannerState(sf.core.PL_NNAVI_STATE_BANNER_VOL_CH);
				}
			}
			else if (state === false) {
				sf.key.registerKey(sf.key.CH_UP);
				sf.key.registerKey(sf.key.CH_DOWN);
				var p = sf.core.plugin('NNAVI');
				if (p) {
					p.SetBannerState(sf.core.PL_NNAVI_STATE_BANNER_NONE);
				}
			}
			else {
				alert('[AF service] parameter must be true or false : ' + state);
			}
		},

		/*
		 * Function: setScreenSaver
		 *   enable/disable screen saver
		 *
		 * Parameters:
		 * 	enable/diable	- (boolean) enable or disable controlling TV volume <default : enabled>
		 * 						screen saver time is different by product type (LCD/PDP/BDP etc)
		 */
		//second	- (number) optional. 지정하지 않을 경우 시스템에 저장된 값으로 설정함. (미공개)
		setScreenSaver: function (state, second) {
			alert('[AF service] sf.service.setScreenSaver('+state+','+(second?second:'')+')');
			var np = sf.core.plugin('NNAVI');
			if (!np || np.GetPluginInfo(sf.core.PL_CMN_INFO_VERSION) < 'NNAVI-0004') {
				alert('[AF service] invalid nnavi plugin version : ' + np.GetPluginInfo(sf.core.PL_CMN_INFO_VERSION));
				return;
			}

			if (state === true) {	// on
				if (!second) {
					var tp = sf.core.plugin('TVMW');
					if (!tp) return;
					var pf = tp.GetProfile(sf.core.PL_PRFID_AUTO_PROTECTION_TIME);
					switch (parseInt(pf)) {
						case 0: //PROFILE_DURATION_10MIN :
							second = 10 * 60;
							break;
						case 1: //PROFILE_DURATION_20MIN :
							second = 20 * 60;
							break;
						case 2: //PROFILE_DURATION_40MIN :
							second = 40 * 60;
							break;
						case 3: //PROFILE_DURATION_1HOUR :
							second = 1 * 60 * 60;
							break;
						case 4: //PROFILE_DURATION_2HOUR :
							second = 2 * 60 * 60;
							break;
						case 5: //PROFILE_DURATION_4HOUR :
							second = 4 * 60 * 60;
							break;
						case 6: //PROFILE_DURATION_8HOUR :
							second = 8 * 60 * 60;
							break;
						case 7: //PROFILE_DURATION_10HOUR :
							second = 10 * 60 * 60;
							break;
						case 8: //PROFILE_DURATION_ALWAYS :
							// do nothing
							alert("[AF service] PROFILE_DURATION_ALWAYS is set. do nothing..");
							return;
						default:
							second = 1 * 60 * 60;
							break;
					}
				}
				alert('[AF service] sf.service.setScreenSaver('+state+') -> ' + second + ' sec');
				np.SendEventToDevice(sf.core.EVENT_TO_DEVICE_SCREEN_SAVER_ON, second);
			}
			else if (state === false) {	// off
				// param이 두개여야 정상 동작함.
				np.SendEventToDevice(sf.core.EVENT_TO_DEVICE_SCREEN_SAVER_OFF, 0);
			}
			else {
				alert('[AF service] parameter must be true or false : ' + state);
			}
		},
		/*
		 * Function: showSetting
		 *   Show Picture/Sound setting popup
		 *
		 * Parameters:
		 * 	mode	- (number) 0 : Sound setting, 1 : Picture setting
		 *   callback - (function) function to be called when the popup is closed.
		 */
		showSetting: function (mode, callback) {
			alert('[AF service] sf.service.showSetting('+mode+')');
			var np = sf.core.plugin('NNAVI');
			if (!np || np.GetPluginInfo(sf.core.PL_CMN_INFO_VERSION) < 'NNAVI-0004') {
				alert('[AF service] invalid nnavi plugin version : ' + np.GetPluginInfo(sf.core.PL_CMN_INFO_VERSION));
				return;
			}
			// 돌아갈 위젯 ID 저장
			var type = sf.core.MANAGER_EVT_RETURN_WIDGETID;
			var data = curWidget.id;
			var widgetEvent = new WidgetEvent(type, data);
			sendWidgetEvent("", widgetEvent, false);		// async call

			// setEventHandler
			deviceapis.widgetevent.setEventListener(type, callback);

//			sf.core.registerEventListener();			// curWidget.onWidgetEvent 가 덮어써졌을 경우를 위함
//			sf.core.setWMEventListener(type, callback);

			np.ActivateWithData(sf.core.PL_DTVAPP_TOOL, mode);
		},
		/*
		 * Function: hideSetting
		 *   Hide Picture/Sound setting popup
		 */
		hideSetting: function () {
			alert('[AF service] sf.service.hideSetting()');
			var np = sf.core.plugin('NNAVI');
			if (!np || np.GetPluginInfo(sf.core.PL_CMN_INFO_VERSION) < 'NNAVI-0003') {
				alert('[AF service] invalid nnavi plugin version : ' + np.GetPluginInfo(sf.core.PL_CMN_INFO_VERSION));
				return;
			}
//			curWidget.onWidgetEvent = null;		// 강제 종료시에는 callback을 실행하지 않음
			var type = sf.core.MANAGER_EVT_RETURN_WIDGETID;
			deviceapis.widgetevent.setEventListener(type, null);

//			sf.core.setWMEventListener(type, null);	// 강제 종료시에는 callback을 실행하지 않음
			// SendEventToDevice 는 2개의 인자를 가져야 함. 2nd 파리미터 의미없음.
			np.SendEventToDevice(sf.core.EVENT_TO_DEVICE_HIDE_TOOLS, 0);
		},
		// 비공개
		getSNSAppList: function (callback) {
			alert('[AF service] sf.service.getSNSAppList()');
			var type = sf.core.MANAGER_EVT_GET_SNS_LIST ;
			var data = curWidget.id;
			var widgetEvent = new WidgetEvent(type, data);	// async call

			sendWidgetEvent("", widgetEvent, false);

			// setEventHandler
			deviceapis.widgetevent.setEventListener(type, callback);

//			sf.core.registerEventListener();	// curWidget.onWidgetEvent 가 덮어써졌을 경우를 위함
//			sf.core.setWMEventListener(type, callback);
		},
		// 비공개
		getAvailAppList: function (callback) {
			alert('[AF service] sf.service.getAvailAppList()');
			var type = sf.core.MANAGER_EVT_GET_APP_ALL_LIST ;
			var data = curWidget.id;
			var widgetEvent = new WidgetEvent(type, data);  // async call

			sendWidgetEvent("", widgetEvent, false);

			// setEventHandler
			deviceapis.widgetevent.setEventListener(type, callback);

//			sf.core.registerEventListener();           // curWidget.onWidgetEvent 가 덮어써졌을 경우를 위함
//			sf.core.setWMEventListener(type, callback);
		},

		/*
		 * Class: PIG
		 *   Control PIG(Picture In Graphics) area.
		 *   TV has only one PIG area at the same time.
		 */
		PIG : {
			'bOn' : false,
			'sDivID' : null,
			'elPIG' : null,
			'bMediaSource' : false,
			'oWindowPlugin' : {},
			'nPluginStyle' : 0,

			// Define
			'MEDIA_SOURCE' : 43,
			'SEF_PLUGIN' : 10,
			'LEGACY_PLUGIN' : 11,

			/*
			 * Function: PIG.show
			 *   Show TV screen in given div area
			 *
			 * Parameters:
			 * 	sDivID	- (string) Div id to show TV screen
			 */
			'show' : function(sDivID) {
				alert("[sf.service.PIG] show()");

				PIGOn.apply(this, arguments);

				function PIGOn(sDivID) {
					alert("[sf.service.PIG] PIGOn("+sDivID+")");
                    this.elPIG = $("#"+sDivID);

					if(this.elPIG.length == 0) {
						alert("[sf.service.PIG.show] Fail to find given " + sDivID + " DIV.");
						return;
					}

					if (this.bOn) {
						this.hide();
					}

					this.bOn = true;
					this.sDivID = sDivID;
					this.nPluginStyle = this.LEGACY_PLUGIN;

					this.elPIG.show();
					var nLeft = this.elPIG.offset().left;
					var nTop = this.elPIG.offset().top;
					var nWidth = this.elPIG.width();
					var nHeight = this.elPIG.height();
					alert("PIG Rect : " + nLeft + ", " + nTop + ", " + nWidth + ", " + nHeight);

					if (!this.oWindowPlugin[sDivID]) {
						var nExpando = (new Date()).getTime();
						var sWindowObjectID = 'SEF-Window-Plugin-' + nExpando;
						document.getElementById(sDivID).innerHTML += getPluginHTML.call(this, sWindowObjectID);
						this.oWindowPlugin[sDivID] = document.getElementById(sWindowObjectID);
						if (this.nPluginStyle == this.SEF_PLUGIN) {
							this.oWindowPlugin[sDivID].Open("Window", "1.000", "Window");
						}
					}

					// Set plugin object's position
					this.oWindowPlugin[sDivID].style.left = "0px";
					this.oWindowPlugin[sDivID].style.top = "0px";
					this.oWindowPlugin[sDivID].style.width = nWidth + "px";
					this.oWindowPlugin[sDivID].style.height = nHeight + "px";

					var oTVMWPlugin = sf.core.plugin('TVMW');

					if (oTVMWPlugin.GetSource() == this.MEDIA_SOURCE) {
						this.bMediaSource = true;

						if (this.nPluginStyle == this.SEF_PLUGIN) {
							this.oWindowPlugin[sDivID].Execute("SetPreviousSource");
						}
						else {
							this.oWindowPlugin[sDivID].SetPreviousSource();
						}
					}
					sf.service.setVolumeControl(true);
					sf.service.setChannelControl(true);

					// Set PIG mode with given area
					var nScrLeft = convCoord(nLeft, 540);
					var nScrTop = convCoord(nTop, 540);
					var nScrWidth = convCoord(nWidth, 540);
					var nScrHeight = convCoord(nHeight, 540);

					if (this.nPluginStyle == this.SEF_PLUGIN) {
						this.oWindowPlugin[sDivID].Execute("SetScreenRect", nScrLeft, nScrTop, nScrWidth, nScrHeight);
					} else {
						this.oWindowPlugin[sDivID].SetScreenRect(nScrLeft, nScrTop, nScrWidth, nScrHeight);
					}

					function getPluginHTML(sPluginID) {
						var sClassID = (this.nPluginStyle == this.SEF_PLUGIN) ? "SAMSUNG-INFOLINK-SEF" : "SAMSUNG-INFOLINK-WINDOW";
						return '<OBJECT id="'+sPluginID+'" classid="clsid:'+sClassID+'" style="position:absolute;width:0px;height:0px;display:block;"></OBJECT>';
					}

					function convCoord(val, convHeight) {
						var rate = convHeight/curWidget.height;
						return parseInt(val*rate, 10);
					}
					alert("[sf.service.PIG] PIGOn() end");
				}
			},

			/*
			 * Function: PIG.hide
			 *   Hide TV screen
			 */
			'hide' : function() {
				alert("[sf.service.PIG] hide()");

				PIGOff.apply(this, arguments);

				function PIGOff() {
					alert("[sf.service.PIG] PIGOff()");
					if (!this.bOn) {
						alert("[sf.service.PIG.hide] No PIG Mode.");
						return;
					}

					this.bOn = false;
					var oTVMWPlugin = sf.core.plugin('TVMW');

					if(this.oWindowPlugin[this.sDivID]) {
						if (this.bMediaSource) {
							oTVMWPlugin.SetSource(this.MEDIA_SOURCE);
						}
						sf.service.setVolumeControl(false);
						sf.service.setChannelControl(false);
						if (this.nPluginStyle == this.SEF_PLUGIN) {
							this.oWindowPlugin[this.sDivID].Execute("SetScreenRect", -1, 0, 0, 0);
						} else {
							this.oWindowPlugin[this.sDivID].SetScreenRect(-1, 0, 0, 0);
						}
						this.elPIG.hide();
					} else {
						alert("[sf.service.PIG.hide] Plugin is not defined.");
					}
					alert("[sf.service.PIG] PIGOff() end");
				}
			}
		}
	}
})(sf);
alert('[AF Service] service.js included');/*
 * Title: Service API(AVSetting)
 * 	Definition of AVSetting Service API
 */

(function (sf) {
	var init = false;
	var onReturn = null;
	var itemIdx = 0;
	var bSetting = false;		// whether Setting popup on or not
	var view = null;

	/*
	 * Namespace: AVSetting
	 * 	show the AV setting popup. This can be useful for VOD applications.
	 */
	sf.service.AVSetting = {
		/*
		 * Function: show
		 * 	Show the AV setting popup.
		 *
		 * Parameters:
		 * 	o callback	- (Function) function to be called when the AV setting popup is closed by user or "hide" method.
		 *
		 * Returns:
		 * 	nothing
		 */
		show: function(callback){
			// product : TV(0), Monitor(1), BD(2) 제품 구분자 전달
			if (sf.core.getEnvValue('product') == sf.core.PL_TV_PRODUCT_TYPE_BD) {
				alert('[AF service.AVSetting]  Not available in BD.');
				return;
			}
			if (!init) {
				init = true;
				initialize();
			}
			onReturn = callback;
			view.show();
			view.move(0);
			setCloseTimer();
		},

		/*
		 * Function: hide
		 * 	Hide the AV setting popup. The callback function passed to "show" method will be called.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		hide: function(){
			if (bSetting) {
				sf.service.hideSetting();
			}
			view.hide();
			clearCloseTimer();
			sf.scene.returnFocus();
			if (typeof onReturn == 'function') { // 종료시에 항상 callback을 실행함.
				onReturn();
			}
			initEnv();
		}
	}

	var bBlock = false;
	sf.service.AVSetting.handleKeydown = function () {
		var keyCode = event.keyCode;
		alert('[AF service.AVSetting] handleKeydown()');
		if(bBlock) {
			alert('[AF service.AVSetting] Blocking...');
			sf.key.preventDefault();
			return;
		}
		setCloseTimer();
		switch(keyCode) {
			case sf.key.UP:
			case sf.key.DOWN:
				// items are only 2
				itemIdx = (itemIdx+1)%2;
				view.move(itemIdx);
				break;

			case sf.key.ENTER:
				view.hide();
				clearCloseTimer();
				bSetting = true;
				var mode = itemIdx == 0 ? sf.core.PICTURE_SETTING : sf.core.SOUND_SETTING;
				bBlock = true;
				sf.service.showSetting (mode, function(){
					bBlock = false;
					view.show();
					setCloseTimer();
					bSetting = false;
				});
				break;

			case sf.key.RETURN:
			case sf.key.EXIT:
				sf.key.preventDefault();
				this.hide();
				break;
		}
	}

	////////////////////////
	///  avsetting view  ///
	////////////////////////
	function View () {
		// insert divs
		$('<div id="sf-service-avsetting"></div>').html(
			// bg
			'<div class="sf-service-avsetting-bg alpha">'+
			'<div class="sf-service-avsetting-bg-top"><div class="sf-service-avsetting-bg-top-l"></div><div class="sf-service-avsetting-bg-top-c"></div><div class="sf-service-avsetting-bg-top-r"></div></div>'+
			'<div class="sf-service-avsetting-bg-mid"><div class="sf-service-avsetting-bg-mid-l"></div><div class="sf-service-avsetting-bg-mid-c"></div><div class="sf-service-avsetting-bg-mid-r"></div></div>'+
			'<div class="sf-service-avsetting-bg-btm"><div class="sf-service-avsetting-bg-btm-l"></div><div class="sf-service-avsetting-bg-btm-c"></div><div class="sf-service-avsetting-bg-btm-r"></div></div>'+
			'</div>'+
			'<div class="sf-service-avsetting-bg">'+
			'<div class="sf-service-avsetting-bg-top"><div class="sf-service-avsetting-bg-top-l"></div><div class="sf-service-avsetting-bg-top-c"></div><div class="sf-service-avsetting-bg-top-r"></div></div>'+
			'<div class="sf-service-avsetting-bg-mid"><div class="sf-service-avsetting-bg-mid-l"></div><div class="sf-service-avsetting-bg-mid-c"></div><div class="sf-service-avsetting-bg-mid-r"></div></div>'+
			'<div class="sf-service-avsetting-bg-btm"><div class="sf-service-avsetting-bg-btm-l"></div><div class="sf-service-avsetting-bg-btm-c"></div><div class="sf-service-avsetting-bg-btm-r"></div></div>'+
			'</div>'+
			// title
			'<div id="sf-service-avsetting-title">'+sf.lang.SID_TOOLS+'</div>'+
			// items
			'<div id="sf-service-avsetting-items">'+
			'</div>'+
			// help
			'<div id="sf-service-avsetting-keyhelp"></div>'+
			'<a href="javascript:void(0)" id="sf-service-avsetting-anchor"></a>'
		).appendTo('body');

		$('#sf-service-avsetting-items').sfList({
			data: [sf.lang.SID_PICTURE_SETTING, sf.lang.SID_SOUND_SETTING],
			index: 0,
			itemsPerPage: 2
		});

		$('#sf-service-avsetting-keyhelp').sfKeyHelp({
			'iconset' : 'WHITE',
			'return' : sf.lang.SID_RETURN
		});

		alert(document.getElementById("sf-service-avsetting").innerHTML);

		this.show = function () {
			alert('[AF service.AVSetting] view : show()');
			$('#sf-service-avsetting').show();
			$('#sf-service-avsetting-items').sfList('focus');
			$('#sf-service-avsetting-anchor').focus();
		}
		this.hide = function (options) {
			alert('[AF service.AVSetting] view : hide()');
			$('#sf-service-avsetting').hide();
		}
		this.move = function (idx) {
			alert('[AF service.AVSetting] view : move('+idx+')');
			$('#sf-service-avsetting-items').sfList('move', idx);
		}
	}

	// initialize
	function initEnv () {
		onReturn = null;
		itemIdx = 0;
		view.move(itemIdx);
		bSetting = false;
	}
	function initialize () {
		alert('[AF service.AVSetting] initialize()');
		setTimeout (function(){
			$('#sf-service-avsetting-anchor').keydown(function(){
				sf.service.AVSetting.handleKeydown();
			});
		}, 0);
		view = new View();
	}
	// close timer
	var timer = null;
	function setCloseTimer () {
		clearCloseTimer();
		timer = setTimeout (function(){
			sf.service.AVSetting.hide();
		}, 60000);	// 60 sec
	}
	function clearCloseTimer () {
		if (timer)  {
			clearTimeout(timer);
			timer = null;
		}
	}
})(sf);
alert('[AF AVSetting] avsetting.js included');/*
 * Title:  Service API (VideoPlayer)
 * Definition of Video Service API
 */
(function (sf) {
	/*
	 *	Namespace: sf.service.VideoPlayer
	 *		Video Player service module.
	 *
	 */
	sf.service.VideoPlayer = new VideoPlayer();

	/*
	 * # 3D 관련 처리
	 * - 부분화면에서는 3D 불가 -> 3D 키 막기
	 * - 전체화면에서 3D 재생할 경우, TV 자체 Volume UI 못그림 -> 자체 Volume UI 그림
	 */
	function VideoPlayer () {

		////////////////////////
		///  Public methods  ///
		////////////////////////
		/*
		 * Function: init
		 * 	Initialize the VideoPlayer with given option.
		 *
		 * Parameters:
		 * 	o option - (Object)(Optional) Contains event handler
		 * (start code)
		 * {
		 *     onstatechange: function(state){},	// (Function) called when VideoPlayer state changes. "state" can be one of sf.service.VideoPlayer.STATE_PLAYING, STATE_STOPPED, STATE_PAUSED, STATE_BUFFERING, STATE_SCANNIG
		 *     onend: function(){},	// (Function) called when video play ends.
		 *     onerror: function(error){}	// (Function) called when error occurs.
		 * }
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.init = function (option) {
			alert('[VideoPlayer] init('+option+')');

			oInitOption = option || {};
			if (!bInitialize) initialize();

			if (!bSetAVPlayInstance) {	// AVPlay 삽입 후 설정
				setAVPlayInstance();

				// 최초 AVPlay init 시에 container를 줘서 container에 <object>가 삽입되도록 함.
				oInitOption.containerID = VIDEOPLAYER_CONTAINER_DIV_ID;
				oInitOption.zIndex = PLAYER_OBJECT_ZINDEX;

				initAVPlay(oInitOption);
			}
			else {	// AVPlay 설정
				initAVPlay(oInitOption);
			}
		}
		/*
		 * Function: show
		 * 	Shows the VideoPlayer.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.show = function () {
			alert('[VideoPlayer] show()');
			if (!bInitialize) initialize();
			// <object> 상위 div가 display:none 되지 않도록 view, object 구분하여 show/hide.
			$('#'+VIEW_CONTAINER_DIV_ID).show();
			iAVPlay.show();
		}
		/*
		 * Function: hide
		 * 	Hides the VideoPlayer.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.hide = function () {
			alert('[VideoPlayer] hide()');
			if (!bInitialize) initialize();
			$('#'+VIEW_CONTAINER_DIV_ID).hide();
			iAVPlay.hide();
		}
		/*
		 * Function: focus
		 * 	Focuses to the VideoPlayer. This API works only under full-Screen mode.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.focus = function () {
			alert('[VideoPlayer] focus()');
			if (!bInitialize) initialize();
			if (!bFullMode) {
				alert('[VideoPlayer] This API works only under FullScreen.');
				return;
			}
			$('#'+ANCHOR_ID).focus();
		}
		/*
		 * Function: play
		 * 	Plays the video with given option.
		 *
		 * Parameters:
		 * 	o option - (Object) Contains video information.
		 * (start code)
		 * {
		 *     url: 'http://www.samsung.com/samsung.mkv',	// video URL
		 *     title: 'Samsung movie'	// video title. This will be shown on full screen infomation bar.
		 *     startTime: 5	// (Number) Start time of the video. (seconds)
		 * }
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.play = function (option) {
			alert('[VideoPlayer] play('+option+')');
			if (!bInitialize) initialize();
			if (!bSetAVPlayInstance) {
				alert('[VideoPlayer] Execute setPosition() first to play video on partial-screen, or do setFullScreen(true) for full-screen.');
				return;
			}

			if (nStatus != sf.service.VideoPlayer.STATE_STOPPED) {
				alert('[VideoPlayer] !ERROR! play() should be called on "STATE_STOPPED". Current Status : ' + State2String[nStatus]);
            	return false;
			}

			oCurPlayItem = $.extend({}, option);

			runViewCleaner();

			if (oCurPlayItem.title) {
				iFullView.refresh('title', oCurPlayItem.title);
			}

			iMiniView.showLoading();
			iFullView.showLoading();
			this.show();

			if (oCurPlayItem.is3D && iCCheck3DEffect.is3DEffectSupport()) {	// 3D 지원
				if (bFullMode) {	// 전체 화면 -> 3D 동작하도록 함
					sf.key.unregisterKey(sf.key.THREE_D);	// 3D 키 해제
					deviceapis._plugin('Screen', 'Set3DEffectFunction', deviceapis._pluginDef.PLR_TRUE);	// 3D on
					sf.service.setVolumeControl(false);	// -> 볼륨 UI를 직접 그려줘야 함
				}
				else {	// 부분 화면	-> 3D 기능 막음
					sf.key.registerKey(sf.key.THREE_D);	// 3D 키 등록, 3D 키를 사용하지 못하도록 함
					deviceapis._plugin('Screen', 'Set3DEffectFunction', deviceapis._pluginDef.PLR_FALSE);	// 3D off
					sf.service.setVolumeControl(true);
				}
			}
			else {	// 3D 미지원
				sf.service.setVolumeControl(true);
			}

			if (oCurPlayItem.subtitle) { // Subtitle이 있다!
				if (iAVPlay.getSubtitleAvailable()) {	// 지원하는 Firmware! -> 다운로드 받은 후 재생 시작
					this.Subtitle._downloadFile(oCurPlayItem.subtitle.url, function (downloadedSubtitleFilePath) {
						if (downloadedSubtitleFilePath) {
							oCurPlayItem.subtitle = {
								path: downloadedSubtitleFilePath,
								streamID: 0,
								sync: 0,
								callback: onSubtitle
							}

							self.Subtitle.show();
						}
						else {
							alert('[VideoPlayer] Download failed. "subtitle" option is deleted.');
							delete oCurPlayItem.subtitle;
						}
						play();
					});
				}
				else { // 지원하지 않는 Firmware! -> 그냥 재생
					alert('[VideoPlayer] Not Support Subtitle function. "subtitle" option is deleted.');
					delete oCurPlayItem.subtitle;
					play();
				}
			}
			else { // Subtitle이 없다!
				play();
			}

			if (bFullMode) { // full-screen 모드면 focus를 가져온다.
				this.focus();
			}

			function play () {
				iAVPlay.open(oCurPlayItem.url, oCurPlayItem);
				iAVPlay.play(
					onStreamInfoReady,
					function(error){
						alert('[VideoPlayer] ' + error);
						iAVPlay.stop();
					},
					oCurPlayItem.startTime || 0
				);
			}
		}
		/*
		 * Function: stop
		 * 	Stops playing video.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.stop = function () {
			alert('[VideoPlayer] stop()');
			if (!bInitialize) initialize();
			if (!bSetAVPlayInstance) {
				alert('[VideoPlayer] Check the usage.');
				return;
			}

            if (nStatus != sf.service.VideoPlayer.STATE_PLAYING &&
            nStatus != sf.service.VideoPlayer.STATE_PAUSED &&
            nStatus != sf.service.VideoPlayer.STATE_STOPPED &&
            nStatus != sf.service.VideoPlayer.STATE_ERROR) {
                alert('[VideoPlayer] !ERROR! stop() should be called on among "STATE_PLAYING", "STATE_PAUSED" and "STATE_ERROR". Current Status : ' + State2String[nStatus]);
                return false;
            }

			iAVPlay.stop();
			setStatus(sf.service.VideoPlayer.STATE_STOPPED);

			oCurPlayItem = null;

			runViewCleaner();

			iMiniView.hideTime();
			iMiniView.refresh('curtime', new PlayTime(0));
			iMiniView.refresh('totaltime', 0);
			iMiniView.Subtitle.clear();
			iMiniView.Subtitle.hide();
			iMiniView.hideLoading();

			iFullView.refresh('curtime', new PlayTime(0));
			iFullView.refresh('totaltime', 0);
			iFullView.refresh('title', '');
			iFullView.Subtitle.clear();
			iFullView.Subtitle.hide();
			iFullView.hideLoading();

			// Volume, 3D 키 설정 원복
			sf.key.unregisterKey(sf.key.THREE_D);
			sf.service.setVolumeControl(false);

			if (bFullMode) {	// 전체화면일 경우, 화면을 내림
//				this.hide();
				iFullView.hide();
				iFullView.hideBars();
				iAVPlay.hide();
				sf.scene.returnFocus();	// sf.scene에 focus 돌려줌
			}
		}
		/*
		 * Function: pause
		 * 	Pauses the video.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.pause = function () {
			alert('[VideoPlayer] pause()');
			if (!bInitialize) initialize();
			if (!bSetAVPlayInstance) {
				alert('[VideoPlayer] Check the usage.');
				return;
			}

//			var sSkipState = iSkip.getStatus();
//			if (sSkipState == 'FF' || sSkipState == 'REW') {
			if (self.Skip.isInProgress()) {
				alert('[VideoPlayer] Skipping now.. pause skipping..');
				iSkip.pause();
				return;
			}

			if (nStatus != sf.service.VideoPlayer.STATE_PLAYING) {
                alert('[VideoPlayer] !ERROR! pause() should be called on "STATE_PLAYING". Current Status : ' + State2String[nStatus]);
                return false;
            }

			iAVPlay.pause();
			setStatus(sf.service.VideoPlayer.STATE_PAUSED);
		}
		/*
		 * Function: resume
		 * 	Resumes the video.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.resume = function () {
			alert('[VideoPlayer] resume()');
			if (!bInitialize) initialize();
			if (!bSetAVPlayInstance) {
				alert('[VideoPlayer] Check the usage.');
				return;
			}

//			var sSkipState = iSkip.getStatus();
//			if (sSkipState == 'PAUSE') {
			if (self.Skip.isInProgress()) {
				alert('[VideoPlayer] Skipping now.. resume skipping..');
				iSkip.resume();
			}

			if (nStatus != sf.service.VideoPlayer.STATE_PAUSED) {
                alert('[VideoPlayer] !ERROR! resume() should be called on "STATE_PAUSED". Current Status : ' + State2String[nStatus]);
                return false;
            }

			iAVPlay.resume();
		}
		/*
		 * Function: setPosition
		 * 	Sets the VideoPlayer position to given rect. VideoPlayer information panel will be shown inside that rect. It takes 73px of the rect base.
		 *
		 * Parameters:
		 * 	o rect - (Object) Position information.
		 * (start code)
		 * {
		 *     left: 100,	// left-coordinate of VideoPlayer area.
		 *     top: 100,	// top-coordinate of VideoPlayer area.
		 *     width: 500,	// width of VideoPlayer area.
		 *     height: 400	// height of VideoPlayer area. VideoPlayer takes 73px of the area base to show information panel.
		 * }
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setPosition = function (rect) {
			alert('[VideoPlayer] setPosition('+rect+')');

			if (!bInitialize) initialize();

			iMiniModeDisplayRect = new SRect(rect.left, rect.top, rect.width, rect.height - iMiniView.getInfobarHeight());

			if (!bSetAVPlayInstance) {
				setAVPlayInstance();
				initAVPlay({
					displayRect: iMiniModeDisplayRect
				});
			}
			else {
				iAVPlay.setDisplayRect(iMiniModeDisplayRect);
			}

			iMiniView.setPosition(rect);
			iMiniView.show();	// setPosition()을 사용하는 건 Mini mode를 의미 하므로, iMiniView.show() 호출.
		}
		/*
		 * Function: setFullScreen
		 * 	Sets or unsets full-screen mode. In full-screen mode, VideoPlayer takes key handling from scene. You can add your key handler by setKeyHandler().
		 *
		 * Parameters:
		 * 	o bOn - (Boolean) true: sets full-screen, false: unsets full-screen.
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setFullScreen = function (bOn) {
			alert('[VideoPlayer] setFullScreen('+bOn+')');
			if (!bInitialize) initialize();
			if (!bSetAVPlayInstance) {
				setAVPlayInstance();

				var rect = new SRect(0, 0, curWidget.width / 2, curWidget.height / 2);
				iMiniModeDisplayRect = new SRect(rect.left, rect.top, rect.width, rect.height - iMiniView.getInfobarHeight());
				initAVPlay({
					displayRect: iMiniModeDisplayRect
				});
				iMiniView.setPosition(rect);
			}

			bFullMode = bOn;

			if (bOn) {
                iAVPlay.setDisplayRect(new SRect(0, 0, curWidget.width, curWidget.height));

				iMiniView.hide();
				iFullView.show();
				iFullView.showBars();

				this.focus();	// 전체화면에서는 키를 직접 받음.
			}
			else {
				iAVPlay.setDisplayRect(iMiniModeDisplayRect);

				iFullView.hide();
				iFullView.hideBars();
				iMiniView.show();

				sf.scene.returnFocus();	// 부분화면으로 전환시 sf.scene에 focus 돌려줌
			}

			if (oCurPlayItem && oCurPlayItem.is3D && iCCheck3DEffect.is3DEffectSupport()) {	// 3D 지원
				if (bFullMode) {	// 전체 화면 -> 3D 동작하도록 함
					sf.key.unregisterKey(sf.key.THREE_D);	// 3D 키 해제
					deviceapis._plugin('Screen', 'Set3DEffectFunction', deviceapis._pluginDef.PLR_TRUE);	// 3D on
					sf.service.setVolumeControl(false);	// -> 볼륨 UI를 직접 그려줘야 함
				}
				else {	// 부분 화면	-> 3D 기능 막음
					sf.key.registerKey(sf.key.THREE_D);	// 3D 키 등록, 3D 키를 사용하지 못하도록 함
					deviceapis._plugin('Screen', 'Set3DEffectFunction', deviceapis._pluginDef.PLR_FALSE);	// 3D off
					sf.service.setVolumeControl(true);
				}
			}
			else {	// 3D 미지원
				sf.service.setVolumeControl(true);
			}
		}
		/*
		 * Function: setKeyHandler
		 * 	Sets key handler. This API only need in full-screen mode.
		 *
		 * Parameters:
		 * 	o key - (Number) sf.key value.
		 *  o handler - (Function) key handler function. If the given key pressed, this function will be called.
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setKeyHandler = function (key, handler) {
			alert('[VideoPlayer] setKeyHandler('+key+','+(handler?handler:'')+')');
			if (!bInitialize) initialize();

			if (typeof key == 'object') {	// {key:handler,...} 형태일 경우
				for (var prop in key) {
					oUserKeyHandlerMap[prop] = key[prop];
				}
			}
			else {
				oUserKeyHandlerMap[key] = handler;
			}
		}
		/*
		 * Function: setKeyHelp
		 * 	Sets key help description to Key Help Bar on full-screen mode.
		 *
		 * Parameters:
		 * 	o key - (Number) sf.key value.
		 *  o desc - (String) key description.
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setKeyHelp = function (key, desc) {
			alert('[VideoPlayer] setKeyHelp('+key+','+(desc?desc:'')+')');
			if (!bInitialize) initialize();

			if (typeof key == 'object') {	// {key:desc,...} 형태일 경우
				for (var prop in key) {
					oUserKeyHelpMap[prop] = key[prop];
				}
			}
			else {
				oUserKeyHelpMap[key] = desc;
			}
			iFullView.refresh('helpbar', getKeyHelpMap(oUserKeyHelpMap));
		}

		/*
		 *	Namespace: sf.service.VideoPlayer.Skip
		 *		Video Player Skip mode control.
		 */
		this.Skip = {};
		/*
		 * Function: start
		 * 	Starts skipping. VideoPlayer pauses playback and moves skip poiner as per given offset.
		 *
		 * Parameters:
		 * 	o offset - (Number) Seconds. If you want to skip backward, set offset to negative number.
		 *
		 * Returns:
		 * 	nothing
		 */
		this.Skip.start = function (offset) {
			alert('[VideoPlayer] Skip.start('+offset+')');
			if (!bInitialize) initialize();
			if (nStatus != sf.service.VideoPlayer.STATE_PLAYING && nStatus != sf.service.VideoPlayer.STATE_PAUSED) {
				alert('[VideoPlayer] Cannot do Skip. State -> ' +nStatus+ '. Skip calling "Skip.start" method.');
				return;
			}
			iSkip.start(offset);
		}
		/*
		 * Function: stop
		 * 	Stops skipping and resume playback from last position of skip pointer.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.Skip.stop = function () {
			alert('[VideoPlayer] Skip.stop()');
			if (!bInitialize) initialize();
			if (iSkip.getStatus() == 'NONE') {
				alert('[VideoPlayer] Skip is not in progress.. Skip calling "Skip.stop" method.');
				return;
			}
			iSkip.stop();
		}
		/*
		 * Function: cancel
		 * 	Cancels skipping and resume playback from skipping starts.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.Skip.cancel = function () {
			alert('[VideoPlayer] Skip.cancel()');
			if (!bInitialize) initialize();
			if (iSkip.getStatus() == 'NONE') {
				alert('[VideoPlayer] Skip is not in progress.. Skip calling "Skip.cancel" method.');
				return;
			}
			iSkip.cancel();
		}
		/*
		 * Function: isInProgress
		 * 	Returns whether Skip is in progress or not.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	(Boolean) whether Skip is in progress or not.
		 * 		true	- Skip is in progress.
		 * 		false	- Skip is not in progress.
		 */
		this.Skip.isInProgress = function () {
			if (!bInitialize) initialize();
			var bInProgress = iSkip.getStatus() != 'NONE';	// status = FF | REW | NONE | PAUSE
			alert('[VideoPlayer] Skip.isInProgress() returns ' + bInProgress);
			return bInProgress;
		}

		/////////////////////////////////////
		///  Private variables & methods  ///
		/////////////////////////////////////

		/*
		 * HTML 구조
		 * <Div: VideoPlayer Container>	// z-index: 30
		 *     <Div: View Container>			// z-index: 20
		 *         <Div: Mini View>					// z-index:
		 *         <Div: Full View>					// z-index:
		 *
		 *     <Object: Player Plugin Object>	// z-index: 10
		 */

		var VIDEOPLAYER_CONTAINER_DIV_ID = 'sf-service-videoplayer-container';
		var VIDEOPLAYER_CONTAINER_DIV_ZINDEX = 30;
		var VIEW_CONTAINER_DIV_ID = 'sf-service-videoplayer-view-container';
		var VIEW_CONTAINER_DIV_ZINDEX = 20;
		var PLAYER_OBJECT_ZINDEX = 10;

		var ANCHOR_ID = 'sf-service-videoplayer-anchor';

		// State
		this.STATE_PLAYING		= 1;
		this.STATE_STOPPED		= 2;
		this.STATE_PAUSED		= 3;
		this.STATE_BUFFERING	= 4;
		this.STATE_SCANNING		= 5;
		this.STATE_ERROR		= 6;

		var self = this;
		var bInitialize = false;
		var bSetAVPlayInstance = false;
		var iMiniView = null;
		var iFullView = null;
		var iAVPlay = null;
		var iSkip = new Skip();
		this.Subtitle = null;

		var oInitOption = {};
		var oCurPlayItem = null;
		var iMiniModeDisplayRect = null	// 부분 화면 영역, 전체 화면에서 돌아올 때 사용됨
		var bFullMode = false;
		var nStatus = self.STATE_STOPPED;	// STATE_PLAYING, STATE_STOPPED, STATE_PAUSED, STATE_BUFFERING, STATE_SCANNIG
		var oUserKeyHandlerMap = {};
		var oUserKeyHelpMap = {};
		var fnViewCleaner = null;

		var nCurrentPlayTime = 0;	// onCurrentPlayTime 에서 설정됨

		var State2String = {};
		State2String[self.STATE_PLAYING] = 'STATE_PLAYING';
		State2String[self.STATE_STOPPED] = 'STATE_STOPPED';
		State2String[self.STATE_PAUSED] = 'STATE_PAUSED';
		State2String[self.STATE_BUFFERING] = 'STATE_BUFFERING';
		State2String[self.STATE_SCANNING] = 'STATE_SCANNING';

		function initialize () {
			$('<div id="'+VIDEOPLAYER_CONTAINER_DIV_ID+'" style="z-index:'+VIDEOPLAYER_CONTAINER_DIV_ZINDEX+';"></div>')
				.appendTo('body');	// container 삽입
			$('<div id="'+VIEW_CONTAINER_DIV_ID+'" style="z-index:'+VIEW_CONTAINER_DIV_ZINDEX+';"></div>')
				.appendTo($('#'+VIDEOPLAYER_CONTAINER_DIV_ID));	// view container 삽입

			$('<a href="javascript:void(0)" id="'+ANCHOR_ID+'"></a>').appendTo('body');	// anchor 삽입
			setTimeout (function(){
				$('#'+ANCHOR_ID).keydown(handleKeydown);
			}, 0);

			iMiniView = new MiniView(VIEW_CONTAINER_DIV_ID);
			iMiniView.init();
//			iMiniView.hide();
			iMiniView.hideTime();

			iFullView = new FullView(VIEW_CONTAINER_DIV_ID);
			iFullView.init();
//			iFullView.hide();

			self.Subtitle = new Subtitle();

			bInitialize = true;
		}
		function setAVPlayInstance () {
			deviceapis.avplay.getAVPlay(function(instance){	// sync로 실행된다..
				iAVPlay = instance;
				self.AVPlay = iAVPlay;	// 외부 참조용
			});
			bSetAVPlayInstance = true;
		}
		function initAVPlay (option) {
			// VideoPlayer 구동에 꼭 필요한 event handler를 덮어 씌움
			option.bufferingCallback = {
				onbufferingstart: onBufferingStart,
				onbufferingcomplete: onBufferingComplete
			};
			option.playCallback = {
				oncurrentplaytime: onCurrentPlayTime,
				onstreamcompleted: onStreamCompleted,
				onerror: onError
			};
			iAVPlay.init(option);
		}
		function setStatus (state) {
//			alert('[VideoPlayer] setStatus('+state+')');

			var nOldStatus = nStatus;
			nStatus = state;

			iMiniView.refresh('status', nStatus);
			iFullView.refresh('status', nStatus);

			if (nOldStatus != state) {	// status 변경시 호출
				if (typeof oInitOption.onstatechange == 'function') {
					oInitOption.onstatechange(nStatus);
				}
			}
		}
		function getKeyHelpMap (userKeyHelpMap) {

			var keyMap = $.extend({}, userKeyHelpMap);

			// default
			var defaultMap = {};
			defaultMap['REWFF'] = sf.lang.SID_SKIP;
			defaultMap['RETURN'] = sf.lang.SID_RETURN;

			for (var prop in defaultMap) {	// userKeyHelpMap에 없는 것만 붙임
				if (keyMap[prop] === undefined) {
					keyMap[prop] = defaultMap[prop];
				}
			}

			for (var prop in keyMap) {	// user가 null로 설정한 property 제거
				if (keyMap[prop] === null) {
					delete keyMap[prop];
				}
			}

			return keyMap;
		}
		function setViewCleaner (viewCleaner) {
			alert('[VideoPlayer] setViewCleaner()');
			fnViewCleaner = viewCleaner;
		}
		function runViewCleaner () {
			alert('[VideoPlayer] runViewCleaner()');
			if (typeof fnViewCleaner == 'function') {
				fnViewCleaner();
				fnViewCleaner = null;
			}
		}
		function ms2str(ms){
            var hr = 3600, min = 60; // sec
            var floor = Math.floor; // optimize
            var t = floor(parseInt(ms, 10) / 1000);
            var h = floor(t / hr);
            var m = floor(t % hr / min);
            var s = (t % hr % min);
            var retValue = toFixedWidth(h, 2) + ':' + toFixedWidth(m, 2) + ':' + toFixedWidth(s, 2);
//            alert('[VideoPlayer] ms2str(' + ms + ') returns ' + retValue);
            return retValue;

            function toFixedWidth(value, length, fill){
                if (!fill) {
                    fill = '0';
                }
                var result = value.toString();
                var padding = length - result.length;
                if (padding < 0) {
                    result = result.substr(-padding);
                }
                else {
                    for (var n = 0; n < padding; n++) {
                        result = fill + result;
                    }
                }
                return result;
            }
        }
		function turnVolume (option) {	// full mode 전용 (전체화면으로 3D content 재생시에만 동작함)
			alert('[VideoPlayer] turnVolume('+option+')');
			var outputDevice = deviceapis._plugin('Audio', 'GetOutputDevice');

			// External device 처리
			if (outputDevice == deviceapis._pluginDef.PL_AUDIO_OUTPUT_DEVICE_EXTERNAL) {
				alert('[VideoPlayer] Receiver is external..');
				iFullView.showPopup({
					text: sf.lang.SID_NOTAVAILABLE_POPUP,
					buttons: 'OK',	// buttons
					callback: function(){
						self.focus();
					}
				});
				return;
			}

			// Volume 조절
			if (option.toUpperCase() == 'UP') {
				deviceapis._plugin('Audio', 'SetVolumeWithKey', deviceapis._pluginDef.PL_AUDIO_VOLUME_KEY_UP);	// [[deviceapis.audiocontrol.setVolumeUp,Down 함수로 교체 필요]]
			}
			else {	// 'DOWN'
				deviceapis._plugin('Audio', 'SetVolumeWithKey', deviceapis._pluginDef.PL_AUDIO_VOLUME_KEY_DOWN);
			}

			// Receiver 처리
			if (outputDevice == deviceapis._pluginDef.PL_AUDIO_OUTPUT_DEVICE_RECEIVER) {
				alert('[VideoPlayer] Receiver is connected. do nothing..');
				return;
			}

			// Volume UI 처리
			if (deviceapis.audiocontrol.getMute()){
				iFullView.showVolume(0);
			}
			else {
				iFullView.showVolume(deviceapis.audiocontrol.getVolume());
			}
		}
		function toggleMute () {
			alert('[VideoPlayer] toggleMute()');

			var outputDevice = deviceapis._plugin('Audio', 'GetOutputDevice');

			// External device 처리
			if (outputDevice == deviceapis._pluginDef.PL_AUDIO_OUTPUT_DEVICE_EXTERNAL) {
				alert('[VideoPlayer] Receiver is external..');
				iFullView.showPopup({
					text: sf.lang.SID_NOTAVAILABLE_POPUP,
					buttons: 'OK',	// buttons
					callback: function(){
						self.focus();
					}
				});
				return;
			}

			var bMuted = deviceapis.audiocontrol.getMute();

			if (bMuted){	// Mute On -> Off
				deviceapis.audiocontrol.setMute(false);
				bMuted = false;
			}
			else {	// Mute Off -> On
				deviceapis.audiocontrol.setMute(true);
				bMuted = true;
			}

			// Receiver 처리
			if (outputDevice == deviceapis._pluginDef.PL_AUDIO_OUTPUT_DEVICE_RECEIVER) {
				alert('[VideoPlayer] Receiver is connected. do nothing..');
				return;
			}

			if (bMuted) {
				iFullView.showVolume(0);
			}
			else {
				iFullView.showVolume(deviceapis.audiocontrol.getVolume());
			}
		}

		/*
		 * Skip controller
		 */
		function Skip () {

			var self = this;
			var INTERVAL = 300;	// ms
			var hTimer = null;
			var sStatus = 'NONE';	// REW | FF | NONE | PAUSE
			var nSpeed = 0;	// 3 단계 (0-based)
			var aTimeOffset = [
				10000,	// 10 sec
				30000,
				60000,
			];
			var nTimeOffset = 0;
			var nCurSkipTime = 0;	// ms

			function setPointerMoveTimer () {
				hTimer = setInterval (function(){
					nCurSkipTime = Number(nCurSkipTime) + Number(nTimeOffset);
					if (nCurSkipTime < 0) {
						nCurSkipTime = 0;
						clearInterval(hTimer);
						hTimer = null;
					}
					else if (nCurSkipTime > iAVPlay.duration) {
						nCurSkipTime = iAVPlay.duration;
						clearInterval(hTimer);
                        hTimer = null;
					}

					iMiniView.refresh('skiptime', nCurSkipTime);
					iFullView.refresh('skiptime', nCurSkipTime);
				}, INTERVAL);
			}
			function clearPointerMoveTimer () {
				clearInterval(hTimer);
				hTimer = null;
			}

			this.start = function (offset) {
				alert('[Skip] start('+offset+')');	// sec
				if (nStatus == sf.service.VideoPlayer.STATE_PLAYING) {	// 재생 중이었으면 pause
					sf.service.VideoPlayer.pause();	// videoPlayer.pause();
				}

				clearPointerMoveTimer();

				if (sStatus == 'NONE') {
					nCurSkipTime = nCurrentPlayTime;
				}

				if (offset > 0) {
					sStatus = 'FF';
				}
				else {
					sStatus = 'REW';
				}

				nTimeOffset = offset * 1000;	// ms

				setPointerMoveTimer();
			}
			this.stop = function () {
				alert('[Skip] stop()');
				clearPointerMoveTimer();
				sStatus = 'NONE';

				var nJumpSec = parseInt((nCurSkipTime - nCurrentPlayTime) / 1000, 10);
				if (nJumpSec >= 0) {
					iAVPlay.jumpForward(nJumpSec);
				}
				else {
					iAVPlay.jumpBackward(-nJumpSec);	// 양수
				}
				sf.service.VideoPlayer.resume();

				setTimeout(function(){
					iMiniView.refresh('skiptime', 'hide');
					iFullView.refresh('skiptime', 'hide');
				}, 100);

				return nCurSkipTime;
			}
			this.pause = function () {
				alert('[Skip] pause()');
				clearPointerMoveTimer();
				sStatus = 'PAUSE';
			}
			this.resume = function () {
				alert('[Skip] resume()');

				if (nTimeOffset > 0) {
					sStatus = 'FF';
				}
				else {
					sStatus = 'REW';
				}

				setPointerMoveTimer();
			}
			this.cancel = function () {
				alert('[Skip] cancel()');
				clearPointerMoveTimer();
				sStatus = 'NONE';
				nCurSkipTime = 0;
				nSpeed = 0;

				iMiniView.refresh('skiptime', 'hide');
				iFullView.refresh('skiptime', 'hide');

				sf.service.VideoPlayer.resume();
			}
			this.getStatus = function () {
				alert('[Skip] getStatus() returns ' + sStatus);
				return sStatus;
			}

			this.doREW = function () {
				alert('[Skip] doREW()');

				if (sStatus == 'PAUSE' || sStatus == 'NONE' || sStatus == 'FF') {	// PAUSE, NONE, FF 상태였으면 초기 offset 으로 시작
					nSpeed = 0;
				}
				else {
					nSpeed = (nSpeed < aTimeOffset.length - 1) ? nSpeed + 1 : 0;	// REW 였으면 offset 증가 or 초기값으로..
				}
				nTimeOffset = -aTimeOffset[nSpeed];	// 음수 ==  REW

				self.start (parseInt(nTimeOffset / 1000));
			}
			this.doFF = function () {
				alert('[Skip] doFF()');

				if (sStatus == 'PAUSE' || sStatus == 'NONE' || sStatus == 'REW') {	// PAUSE, NONE, REW 상태였으면 초기 offset 으로 시작
					nSpeed = 0;
				}
				else {
					nSpeed = (nSpeed < aTimeOffset.length - 1) ? nSpeed + 1 : 0;	// FF 였으면 offset 증가 or 초기값으로..
				}
				nTimeOffset = aTimeOffset[nSpeed];	// 양수 == FF

				self.start (parseInt(nTimeOffset / 1000));
			}
		}

		function Subtitle () {
			this.VPOS_TOP = 1;
			this.VPOS_BOTTOM = 2;

			var DOWNLOADED_SUBTITLE_FILE = '$TEMP/videoplayer.smi'; // $TEMP -> /dtv/temp/
			var cbDownloadCallback = null;

			this.show = function () {
				alert('[Subtitle] show()');
				iMiniView.Subtitle.show();
				iFullView.Subtitle.show();
			}
			this.hide = function () {
				alert('[Subtitle] hide()');
				iMiniView.Subtitle.hide();
				iFullView.Subtitle.hide();
			}
			this.setSyncTime = function (offset) {
				alert('[Subtitle] setSyncTime('+offset+')');
				iAVPlay.setSubtitleSync(offset);
			}
			this.setVerticalPosition = function (base, offset) {
				alert('[Subtitle] setVerticalPosition('+base+','+offset+')');
				iMiniView.Subtitle.setVerticalPosition(base, offset);
				iFullView.Subtitle.setVerticalPosition(base, offset);
			}
			this.setFontSize = function (size) {
				alert('[Subtitle] setFontSize('+size+')');
				iMiniView.Subtitle.setFontSize(size);
				iFullView.Subtitle.setFontSize(size);
			}
			this.setFontColor = function (color) {
				alert('[Subtitle] setFontColor('+color+')');
				iMiniView.Subtitle.setFontColor(color);
				iFullView.Subtitle.setFontColor(color);
			}
			this._downloadFile = function (url, callback) {
				alert('[Subtitle] _downloadFile('+url+','+callback+')');
				cbDownloadCallback = callback;
				var eDownloadPlugin = deviceapis._plugin('Download');
				eDownloadPlugin.OnEvent = 'sf.service.VideoPlayer.Subtitle._downloadCallback';
				deviceapis._plugin(eDownloadPlugin, 'StartDownFile', url, DOWNLOADED_SUBTITLE_FILE);
			}
			this._downloadCallback = function (type, param1, param2) {
				alert('[Subtitle] _downloadCallback('+type+','+param1+','+param2+')');

				var aResult = param1.split('?');
				switch (aResult[0]) {
					case '1000': // download is complete
						if (typeof cbDownloadCallback == 'function'){
							if (aResult[1] == 1) {	// success
								alert('[Subtitle] Download Success!');
								if (typeof cbDownloadCallback == 'function') {
									cbDownloadCallback(DOWNLOADED_SUBTITLE_FILE);
								}
							}
							else {	// fail
								alert('[Subtitle] Download Failed!');
								if (typeof cbDownloadCallback == 'function') {
									cbDownloadCallback(false);
								}
							}
						}
						break;

					case '1001': // percentage
						// do nothing
						break;

					case '1002': // download speed
						// do nothing
						break;

					default:
						break;
				}
			}
		}

		/*
		 * Event handlers
		 */
		function onBufferingStart () {
			setStatus(sf.service.VideoPlayer.STATE_BUFFERING);

//			iMiniView.showLoading();
//			iFullView.showLoading();
		}
		function onBufferingComplete () {
			iMiniView.hideLoading();
			iFullView.hideLoading();
		}
		function onStreamInfoReady () {
			nCurrentPlayTime = 0;

			iMiniView.refresh('curtime', new PlayTime(0));
			iMiniView.refresh('totaltime', iAVPlay.duration);
			iMiniView.showTime();
//			iMiniView.refresh('speed', 'x 1');

			iFullView.refresh('curtime', new PlayTime(0));
			iFullView.refresh('totaltime', iAVPlay.duration);
//			iFullView.refresh('speed', 'x 1');
		}
		function onCurrentPlayTime (playTime) {
			nCurrentPlayTime = playTime.millisecond;

			iMiniView.refresh('curtime', playTime);
			iFullView.refresh('curtime', playTime);

			// RENDERING_START는 Valencia 에 없어서 CURRENT_PLAYTIME 에서 설정
			if (!false) {	// [[scan 기능 구현 후 수정 필요]]
				setStatus(sf.service.VideoPlayer.STATE_PLAYING);
			}
			else {
				setStatus(sf.service.VideoPlayer.STATE_SCANNING);
			}

//			// for test - defect 임시 회피 [DMS01083886] bufferingstart, bufferingcomplete 순서 바뀌는 문제
//			iMiniView.hideLoading();
//			iFullView.hideLoading();
		}
		function onStreamCompleted () {
			self.stop();
			iMiniView.refresh('status', sf.service.VideoPlayer.STATE_STOPPED);
			iFullView.refresh('status', sf.service.VideoPlayer.STATE_STOPPED);
			iFullView.showBars();

			if (typeof oInitOption.onend == 'function') {
				oInitOption.onend();
			}
		}
		function onSubtitle (syncTime, string) {
			iMiniView.Subtitle.setString(string);
			iFullView.Subtitle.setString(string);
		}
		function onError (error) {
			alert('[VideoPlayer] onError('+error.code+')');

			runViewCleaner();
			iMiniView.hideLoading();
			iFullView.hideLoading();
			setStatus(sf.service.VideoPlayer.STATE_ERROR);

			var msg = '';
			switch (Number(error.code)) {
				case SDeviceAPIError.AVPLAY_UNSUPPORTED_CONTAINER_ERR:
					msg = sf.lang.SID_PLAYER_UNSPPORT_ABBR_10;
					iMiniView.showNotSupport();
					iFullView.showNotSupport();
					setViewCleaner(function(){
						iMiniView.hideNotSupport();
						iFullView.hideNotSupport();
					});
					break;
				case SDeviceAPIError.AVPLAY_UNSUPPORTED_AUDIO_FORMAT_ERR:
					msg = sf.lang.SID_PLAYER_NOTSUPPORT_AUDIO;
					iMiniView.showNotSupport();
					iFullView.showNotSupport();
					setViewCleaner(function(){
						iMiniView.hideNotSupport();
						iFullView.hideNotSupport();
					});
					break;
				case SDeviceAPIError.AVPLAY_UNSUPPORTED_VIDEO_FORMAT_ERR:
					msg = sf.lang.SID_PLAYER_NOTSUPPORT_VIDEO;
					iMiniView.showNotSupport();
					iFullView.showNotSupport();
					setViewCleaner(function(){
						iMiniView.hideNotSupport();
						iFullView.hideNotSupport();
					});
					break;
				case SDeviceAPIError.AVPLAY_UNSUPPORTED_VIDEO_RESOLUTION_ERR:
					msg = sf.lang.SID_PLAYER_RENDERERROR;
					iMiniView.showNotSupport();
					iFullView.showNotSupport();
					setViewCleaner(function(){
						iMiniView.hideNotSupport();
						iFullView.hideNotSupport();
					});
					break;
				case SDeviceAPIError.AVPLAY_UNSUPPORTED_VIDEO_FRAMERATE_ERR:	// 구 service.video.js 에서 unknown 처리 하고 있음.
				case SDeviceAPIError.AVPLAY_CURRUPTED_STREAM_ERR:
				case SDeviceAPIError.UNKNOWN_ERR:
					msg = sf.lang.SID_PLAYER_RENDERERROR;
					iMiniView.showNotSupport();
					iFullView.showNotSupport();
					setViewCleaner(function(){
						iMiniView.hideNotSupport();
						iFullView.hideNotSupport();
					});
					break;

				case SDeviceAPIError.NOT_FOUND_ERR:
				case SDeviceAPIError.NETWORK_ERR:
				case SDeviceAPIError.NETWORK_SLOW_ERR:
				case SDeviceAPIError.SECURITY_ERR:
					msg = sf.lang.SID_CLOSE_BY_SERVER_NETWORK_ERROR_MSG;
					iMiniView.showNetworkError();
					iFullView.showNetworkError();
					setViewCleaner(function(){
						iMiniView.hideNetworkError();
						iFullView.hideNetworkError();
					});
					break;

				default:
					msg = 'Unknown problem!';
					iMiniView.showNotSupport();
					iFullView.showNotSupport();
					setViewCleaner(function(){
						iMiniView.hideNotSupport();
						iFullView.hideNotSupport();
					});
					break;
			}

			if (iFullView.isShow()) {
				iFullView.showPopup({
					text: msg,
					buttons: 'OK',	// buttons
					callback: function(){
						self.focus();
					}
				});
			}

			if (typeof oInitOption.onerror == 'function') {
				oInitOption.onerror(error);
			}
		}
		function handleKeydown(){
			var keyCode = event.keyCode;
			alert('[VideoPlayer] handleKeydown(' + keyCode + ')');

			switch (keyCode) {
				case sf.key.ENTER:
					if (runUserKeyHandler(keyCode)) break;

//					if (iSkip.getStatus() != 'NONE') {
					if (self.Skip.isInProgress()) {
						iSkip.stop();
						iFullView.setBarHideTimer();
					}
					else {
						iFullView.showBars();
						switch (nStatus) {
							case sf.service.VideoPlayer.STATE_PLAYING:
								self.pause();
								break;
							case sf.service.VideoPlayer.STATE_STOPPED:
								// do nothing
								break;
							case sf.service.VideoPlayer.STATE_PAUSED:
								self.resume();
								break;
						}
					}
					break;

				case sf.key.RETURN:	// Skip 중이면 cancel, 아니면 user key handler 실행
					sf.key.preventDefault();
//					if (iSkip.getStatus() != 'NONE') {
					if (self.Skip.isInProgress()) {
						iSkip.cancel();
					}
					else {
						runUserKeyHandler(keyCode);
					}
					break;

				case sf.key.EXIT:
					sf.key.preventDefault();
					if (runUserKeyHandler(keyCode)) break;
					break;

				case sf.key.PLAY:
					if (runUserKeyHandler(keyCode)) break;

//					if (iSkip.getStatus() != 'NONE') {
					if (self.Skip.isInProgress()) {
						iSkip.stop();
						iFullView.setBarHideTimer();
					}
					else {
						iFullView.showBars();
						switch (nStatus) {
							case sf.service.VideoPlayer.STATE_PLAYING:
								// do nothing
								break;
							case sf.service.VideoPlayer.STATE_STOPPED:
								// do nothing
								break;
							case sf.service.VideoPlayer.STATE_PAUSED:
								self.resume();
								break;
						}
					}
					break;

				case sf.key.STOP:
					if (runUserKeyHandler(keyCode)) break;

					self.stop();
					break;

				case sf.key.PAUSE:
					if (runUserKeyHandler(keyCode)) break;

					if (self.Skip.isInProgress()) {	// Skip 중이면 pause() 시킴
						self.pause();
					}
					else {
						iFullView.showBars();
						switch (nStatus) {
							case sf.service.VideoPlayer.STATE_PLAYING:
								self.pause();
								break;
							case sf.service.VideoPlayer.STATE_STOPPED:
								// do nothing
								break;
							case sf.service.VideoPlayer.STATE_PAUSED:
								// do nothing
								break;
						}
					}
					break;

				case sf.key.REW:
					if (runUserKeyHandler(keyCode)) break;

					if (nStatus != sf.service.VideoPlayer.STATE_STOPPED) {
						iFullView.showBars();
						iFullView.clearBarHideTimer();
						iSkip.doREW();
					}
					break;

				case sf.key.FF:
					if (runUserKeyHandler(keyCode)) break;

					if (nStatus != sf.service.VideoPlayer.STATE_STOPPED) {
						iFullView.showBars();
						iFullView.clearBarHideTimer();
						iSkip.doFF();
					}
					break;

				case sf.key.INFO:
					if (runUserKeyHandler(keyCode)) break;

					if (iSkip.getStatus() == 'NONE') {	// skip 중에는 bar가 항상 보인다.
						if (iFullView.isShowBars()) {
							iFullView.hideBars();
						}
						else {
							iFullView.showBars();
						}
					}
					break;

				case sf.key.TOOLS:
					sf.service.AVSetting.show(function(){
						self.focus();
					});
					break;

				case sf.key.VOL_UP:
					turnVolume('up');
					iFullView.showBars();
					break;

				case sf.key.VOL_DOWN:
					turnVolume('down');
					iFullView.showBars();
					break;

				case sf.key.MUTE:
					toggleMute();
					iFullView.showBars();
					break;

				case sf.key.BLUE:
//					window.reload();
					break;
				default:
					runUserKeyHandler(keyCode);
					break;
			}

			/*
			 * 등록되어 있을 경우 실행한 뒤 true 리턴, 없으면 false를 리턴
			 */
			function runUserKeyHandler(keyCode) {
				if (typeof oUserKeyHandlerMap[keyCode] == 'function') {
					oUserKeyHandlerMap[keyCode]();
					return true;
				}
				return false;
			}
		}

		///////////////////
		///  Mini View  ///
		///////////////////
		function MiniView (containerID) {

			var nTotalTime = 0;	// ms
			this.Subtitle = new Subtitle();

			$('<div id="sf-service-videoplayer-mini"></div>').html([
				'<div id="sf-service-videoplayer-mini-infobar">',
					'<div id="sf-service-videoplayer-mini-infobar-status"></div>',
					'<div id="sf-service-videoplayer-mini-infobar-speed"></div>',
					'<div id="sf-service-videoplayer-mini-infobar-time">',
						'<div id="sf-service-videoplayer-mini-infobar-time-0"></div>',
						'<div id="sf-service-videoplayer-mini-infobar-time-1">/</div>',
						'<div id="sf-service-videoplayer-mini-infobar-time-2"></div>',
					'</div>',
					'<div id="sf-service-videoplayer-mini-infobar-progress-bg">',
						'<div id="sf-service-videoplayer-mini-infobar-progress-bg-left"></div>',
						'<div id="sf-service-videoplayer-mini-infobar-progress-bg-right"></div>',
						'<div id="sf-service-videoplayer-mini-infobar-progress-bg-middle"></div>',
					'</div>',
					'<div id="sf-service-videoplayer-mini-infobar-progress-play">',
						'<div id="sf-service-videoplayer-mini-infobar-progress-play-left"></div>',
						'<div id="sf-service-videoplayer-mini-infobar-progress-play-right"></div>',
						'<div id="sf-service-videoplayer-mini-infobar-progress-play-middle"></div>',
					'</div>',
//					'<div id="sf-service-videoplayer-mini-infobar-skip">',
						'<div id="sf-service-videoplayer-mini-infobar-skip-pointer"></div>',
//					'</div>',
				'</div>',
				'<div id="sf-service-videoplayer-mini-loading"></div>',
				'<div id="sf-service-videoplayer-mini-neterr-icon"></div>',
				'<div id="sf-service-videoplayer-mini-neterr-text">' + sf.lang.SID_ALERT_NETWORKERROR_MSG + '</div>',
				'<div id="sf-service-videoplayer-mini-notsup-icon"></div>',
				'<div id="sf-service-videoplayer-mini-notsup-text">' + sf.lang.SID_NOT_SUPPORT_FORMAT + '</div>',

				'<div id="sf-service-videoplayer-mini-subtitle">',
					'<table cellpadding="0px" cellspacing="0px">',
						'<tr>',
							'<td id="sf-service-videoplayer-mini-subtitle-table-td">',
							'</td>',
						'</tr>',
					'</table>',
				'</div>',
			].join('')).appendTo($('#'+containerID));

//			$('#sf-service-videoplayer-mini-loading').sfLoading();

			var oPlayBar = $('#sf-service-videoplayer-mini-infobar-progress-play');
			var nInfoBarHeight = $('#sf-service-videoplayer-mini-infobar').height();
			var nMinBarWidth = oPlayBar.width();
			var nMaxBarWidth = 0; // setPosition()에서 설정됨
			var nSkipPointerBase = parseInt(oPlayBar.css('left')) + nMinBarWidth - $('#sf-service-videoplayer-mini-infobar-skip-pointer').width(); // bar start position + nMinBarWidth - skip pointer width
			var nBarRightMargin = parseInt(oPlayBar.css('left')) +  parseInt($('#sf-service-videoplayer-mini-infobar-time').css('right'));

            this.init = function(){
                alert('[MiniView] init()');

                this.hide();

                this.refresh('status', sf.service.VideoPlayer.STATE_STOPPED);
                this.refresh('speed', '');
                this.refresh('totaltime', 0);
                this.refresh('curtime', new PlayTime(0));
            }
            this.show = function(){
				alert('[MiniView] show()');
            	$('#sf-service-videoplayer-mini').show();
            }
            this.hide = function(){
				alert('[MiniView] hide()');
            	$('#sf-service-videoplayer-mini').hide();
            }
			this.showLoading = function () {
				alert('[MiniView] showLoading()');
				$('#sf-service-videoplayer-mini-loading').sfLoading('show');
			}
			this.hideLoading = function () {
				alert('[MiniView] hideLoading()');
				$('#sf-service-videoplayer-mini-loading').sfLoading('hide');
			}
			this.showTime = function () {
				alert('[MiniView] showTime()');
				$('#sf-service-videoplayer-mini-infobar-time').show();
			}
			this.hideTime = function () {
				alert('[MiniView] hideTime()');
				$('#sf-service-videoplayer-mini-infobar-time').hide();
			}
			this.showNetworkError = function () {
				alert('[MiniView] showNetworkError()');
				$('#sf-service-videoplayer-mini-neterr-icon').show();
				$('#sf-service-videoplayer-mini-neterr-text').show();
			}
			this.hideNetworkError = function () {
				alert('[MiniView] hideNetworkError()');
				$('#sf-service-videoplayer-mini-neterr-icon').hide();
				$('#sf-service-videoplayer-mini-neterr-text').hide();
			}
			this.showNotSupport = function () {
				alert('[MiniView] showNotSupport()');
				$('#sf-service-videoplayer-mini-notsup-icon').show();
				$('#sf-service-videoplayer-mini-notsup-text').show();
			}
			this.hideNotSupport = function () {
				alert('[MiniView] hideNotSupport()');
				$('#sf-service-videoplayer-mini-notsup-icon').hide();
				$('#sf-service-videoplayer-mini-notsup-text').hide();
			}
			this.setPosition = function (rect) {
				alert('[MiniView] setPosition('+rect+')');
				$('#sf-service-videoplayer-mini').css({
					left: rect.left,
					top: rect.top,
					width: rect.width,
					height: rect.height
				});

				/*
				 * Progress bar 설정
				 */
				nMaxBarWidth = rect.width - nBarRightMargin;	// left, right margin 13 + 12
				$('#sf-service-videoplayer-mini-infobar-progress-bg').width(nMaxBarWidth);

				/*
				 * Subtitle 영역 설정
				 */
				$('#sf-service-videoplayer-mini-subtitle').height(rect.height - this.getInfobarHeight());
			}
			this.getInfobarHeight = function () {
				alert('[MiniView] getInfobarHeight() returns ' + nInfoBarHeight);
				return nInfoBarHeight;
			}
			this.refresh = function (key, value) {
//				alert('[MiniView] refresh('+key+','+value+')');
				switch (key.toUpperCase()) {
					case 'STATUS':
						$('#sf-service-videoplayer-mini-infobar-status')
							.attr('class', 'sf-service-videoplayer-mini-infobar-state-' + state2classname[value]);
						break;
					case 'SPEED':
						$('#sf-service-videoplayer-mini-infobar-speed').html(value);
						break;
					case 'CURTIME':
						/*
						 * value {
						 *   millisecond: 1000
						 *   timeString: '00:00:00'
						 * }
						 */
						// update current time string
						$('#sf-service-videoplayer-mini-infobar-time-0').html(value.timeString);

						// adjust progress bar
                        if (nTotalTime) {
							var nBarWidth = parseInt((nMaxBarWidth - nMinBarWidth) * value.millisecond / nTotalTime) + nMinBarWidth;
                            $('#sf-service-videoplayer-mini-infobar-progress-play').width(nBarWidth);
                        }
						break;
					case 'TOTALTIME':
						// update total time string
						$('#sf-service-videoplayer-mini-infobar-time-2').html(ms2str(value));
						nTotalTime = value;
						break;
					case 'SKIPTIME':
						var eSkipPointer = $('#sf-service-videoplayer-mini-infobar-skip-pointer');
						if (value == 'hide') {
							eSkipPointer.hide();
						}
						else {
							var nSkipPointerPos = nSkipPointerBase + parseInt((nMaxBarWidth - nMinBarWidth) * value / nTotalTime);
							eSkipPointer.css('left', nSkipPointerPos);
							if (eSkipPointer.css('display') == 'none') eSkipPointer.show();
						}
						break;
					default :
						break;
				}
			}

			function Subtitle () {
				this.show = function () {
					alert('[MiniView][Subtitle] show()');
					$('#sf-service-videoplayer-mini-subtitle').show();
				}
				this.hide = function () {
					alert('[MiniView][Subtitle] hide()');
					$('#sf-service-videoplayer-mini-subtitle').hide();
				}
				this.clear = function () {
					alert('[MiniView][Subtitle] clear()');
					$('#sf-service-videoplayer-mini-subtitle-table-td').html('');
				}
				this.setVerticalPosition = function (base, offset){
					alert('[MiniView][Subtitle] setVerticalPosition('+base+','+offset+')');
					var eSubtitleTd = $('#sf-service-videoplayer-mini-subtitle-table-td');

					if (base == 1) { // VPOS_TOP = 1;
                        eSubtitleTd.css({
                            'vertical-align': 'top',
							'padding-top': offset,
							'padding-bottom': 0
                        });
					}
					else if (base == 2) { // VPOS_BOTTOM = 2;
						eSubtitleTd.css({
                            'vertical-align': 'bottom',
							'padding-top': 0,
							'padding-bottom': offset
                        });
					}
				}
				this.setFontSize = function (px) {
					alert('[MiniView][Subtitle] setFontSize('+px+')');
					$('#sf-service-videoplayer-mini-subtitle-table-td').css({
						'font-size': px
					});
				}
				this.setFontColor = function (color) {
					alert('[MiniView][Subtitle] setFontColor('+color+')');
					$('#sf-service-videoplayer-mini-subtitle-table-td').css({
						'color': color
					});
				}
				this.setString = function (string) {
					alert('[MiniView][Subtitle] setString('+string+')');
					$('#sf-service-videoplayer-mini-subtitle-table-td').html(string);
				}
			}
		}

		///////////////////
		///  Full View  ///
		///////////////////
		function FullView (containerID) {

			var self = this;
			var nTotalTime = 0;	// ms

			var hBarHideTimer = null;
			var hVolumeHideTimer = null;
			var BAR_HIDE_TIME = 5000;	// 5 sec
			var VOLUME_HIDE_TIME = 1000;	// 1 sec

			this.Subtitle = new Subtitle();

			$('<div id="sf-service-videoplayer-full"></div>').html([
				'<div id="sf-service-videoplayer-full-infobar">',
					'<div id="sf-service-videoplayer-full-infobar-bg0"></div>',
					'<div id="sf-service-videoplayer-full-infobar-bg1"></div>',
					'<div id="sf-service-videoplayer-full-infobar-status"></div>',
					'<div id="sf-service-videoplayer-full-infobar-speed"></div>',
					'<div id="sf-service-videoplayer-full-infobar-time">',
						'<div id="sf-service-videoplayer-full-infobar-time-0"></div>',
						'<div id="sf-service-videoplayer-full-infobar-time-1">/</div>',
						'<div id="sf-service-videoplayer-full-infobar-time-2"></div>',
					'</div>',
					'<div id="sf-service-videoplayer-full-infobar-progress-bg">',
						'<div id="sf-service-videoplayer-full-infobar-progress-bg-left"></div>',
						'<div id="sf-service-videoplayer-full-infobar-progress-bg-right"></div>',
						'<div id="sf-service-videoplayer-full-infobar-progress-bg-middle"></div>',
					'</div>',
					'<div id="sf-service-videoplayer-full-infobar-progress-play">',
						'<div id="sf-service-videoplayer-full-infobar-progress-play-left"></div>',
						'<div id="sf-service-videoplayer-full-infobar-progress-play-right"></div>',
						'<div id="sf-service-videoplayer-full-infobar-progress-play-middle"></div>',
					'</div>',
//					'<div id="sf-service-videoplayer-full-infobar-skip">',
						'<div id="sf-service-videoplayer-full-infobar-skip-pointer"></div>',
//					'</div>',
					'<div id="sf-service-videoplayer-full-infobar-title"></div>',
					'<div id="sf-service-videoplayer-full-infobar-volume">',
						'<div id="sf-service-videoplayer-full-infobar-volume-icon"></div>',
						'<div id="sf-service-videoplayer-full-infobar-volume-text"></div>',
					'</div>',
				'</div>',
				'<div id="sf-service-videoplayer-full-helpbar"></div>',
				'<div id="sf-service-videoplayer-full-loading"></div>',
				'<div id="sf-service-videoplayer-full-popup"></div>',
				'<div id="sf-service-videoplayer-full-neterr-icon"></div>',
				'<div id="sf-service-videoplayer-full-neterr-text">' + sf.lang.SID_ALERT_NETWORKERROR_MSG + '</div>',
				'<div id="sf-service-videoplayer-full-notsup-icon"></div>',
				'<div id="sf-service-videoplayer-full-notsup-text">' + sf.lang.SID_NOT_SUPPORT_FORMAT + '</div>',

				'<div id="sf-service-videoplayer-full-subtitle">',
					'<table cellpadding="0px" cellspacing="0px">',
						'<tr>',
							'<td id="sf-service-videoplayer-full-subtitle-table-td">',
							'</td>',
						'</tr>',
					'</table>',
				'</div>',
			].join('')).appendTo($('#'+containerID));

			$('#sf-service-videoplayer-full-helpbar').sfKeyHelp({
				REWFF: sf.lang.SID_SKIP,
				RETURN: sf.lang.SID_RETURN
			});
//			$('#sf-service-videoplayer-full-loading').sfLoading();

			var oPlayBar = $('#sf-service-videoplayer-full-infobar-progress-play');
			var nMinBarWidth = oPlayBar.width();
			var nMaxBarWidth = $('#sf-service-videoplayer-full-infobar-progress-bg').width();
			var nSkipPointerBase = parseInt(oPlayBar.css('left')) + nMinBarWidth - $('#sf-service-videoplayer-full-infobar-skip-pointer').width(); // bar start position + nMinBarWidth - skip pointer width

            this.init = function(){
                alert('[FullView] init()');

                this.hide();

                this.refresh('status', sf.service.VideoPlayer.STATE_STOPPED);
                this.refresh('speed', '');
                this.refresh('totaltime', 0);
                this.refresh('curtime', new PlayTime(0));
                this.refresh('title', '');
            }
            this.show = function(){
				alert('[FullView] show()');
            	$('#sf-service-videoplayer-full').show();
				self.showBars();
            }
            this.hide = function(){
				alert('[FullView] hide()');
            	$('#sf-service-videoplayer-full').hide();
            }
			this.isShow = function(){
				var bShow = $('#sf-service-videoplayer-full').css('display') != 'none';
				alert('[FullView] isShow() returns ' + bShow);
				return bShow;
            }
			this.showBars = function () {
				alert('[FullView] showBars()');
				$('#sf-service-videoplayer-full-infobar').show();
				$('#sf-service-videoplayer-full-helpbar').show();

				self.setBarHideTimer();
			}
			this.hideBars = function () {
				alert('[FullView] hideBars()');
				$('#sf-service-videoplayer-full-infobar').hide();
				$('#sf-service-videoplayer-full-helpbar').hide();

				self.clearBarHideTimer();
			}
			this.isShowBars = function () {
				if (hBarHideTimer != null) {
					return true;
				}
				return false;
			}
			this.showPopup = function (option) {
				alert('[FullView] showPopup('+option+')');
				$('#sf-service-videoplayer-full-popup')
					.sfPopup(option)
					.sfPopup('show');
			}
			this.showLoading = function () {
				alert('[FullView] showLoading()');
				$('#sf-service-videoplayer-full-loading').sfLoading('show');
			}
			this.hideLoading = function () {
				alert('[FullView] hideLoading()');
				$('#sf-service-videoplayer-full-loading').sfLoading('hide');
			}
			this.showNetworkError = function () {
				alert('[FullView] showNetworkError()');
				$('#sf-service-videoplayer-full-neterr-icon').show();
				$('#sf-service-videoplayer-full-neterr-text').show();
			}
			this.hideNetworkError = function () {
				alert('[FullView] hideNetworkError()');
				$('#sf-service-videoplayer-full-neterr-icon').hide();
				$('#sf-service-videoplayer-full-neterr-text').hide();
			}
			this.showNotSupport = function () {
				alert('[FullView] showNotSupport()');
				$('#sf-service-videoplayer-full-notsup-icon').show();
				$('#sf-service-videoplayer-full-notsup-text').show();
			}
			this.hideNotSupport = function () {
				alert('[FullView] hideNotSupport()');
				$('#sf-service-videoplayer-full-notsup-icon').hide();
				$('#sf-service-videoplayer-full-notsup-text').hide();
			}
            this.setBarHideTimer = function(){
                alert('[FullView] setBarHideTimer()');
                self.clearBarHideTimer();
                hBarHideTimer = setTimeout(function(){
                    self.hideBars();
                    hBarHideTimer = null;
                }, BAR_HIDE_TIME);
            }
			this.clearBarHideTimer = function () {
				alert('[FullView] clearBarHideTimer()');
				if (hBarHideTimer) {
					clearTimeout(hBarHideTimer);
					hBarHideTimer = null;
				}
			}

			this.showVolume = function (volume) {
				alert('[FullView] showVolume('+volume+')');
				if (volume == 0) {
					$('#sf-service-videoplayer-full-infobar-volume')
						.addClass('sf-service-videoplayer-full-infobar-volume-icon-mute');
				}
				else {
					$('#sf-service-videoplayer-full-infobar-volume')
						.removeClass('sf-service-videoplayer-full-infobar-volume-icon-mute');
					$('#sf-service-videoplayer-full-infobar-volume-text')
						.html(volume);

				}
				$('#sf-service-videoplayer-full-infobar-volume').show();
				self.setVolumeHideTimer();
			}
			this.hideVolume = function () {
				alert('[FullView] hideVolume()');
				$('#sf-service-videoplayer-full-infobar-volume').hide();
			}
			this.setVolumeHideTimer = function(){
                alert('[FullView] setVolumeHideTimer()');
                self.clearVolumeHideTimer();
                hVolumeHideTimer = setTimeout(function(){
                    self.hideVolume();
                    hVolumeHideTimer = null;
                }, VOLUME_HIDE_TIME);
            }
			this.clearVolumeHideTimer = function () {
				alert('[FullView] clearVolumeHideTimer()');
				if (hVolumeHideTimer) {
					clearTimeout(hVolumeHideTimer);
					hVolumeHideTimer = null;
				}
			}
			this.refresh = function (key, value) {
//				alert('[FullView] refresh('+key+','+value+')');
				switch (key.toUpperCase()) {
					case 'STATUS':
						$('#sf-service-videoplayer-full-infobar-status')
							.attr('class', 'sf-service-videoplayer-full-infobar-state-' + state2classname[value]);
						break;
					case 'SPEED':
						$('#sf-service-videoplayer-full-infobar-speed').html(value);
						break;
					case 'CURTIME':
						/*
						 * value {
						 *   millisecond: 1000
						 *   timeString: '00:00:00'
						 * }
						 */
						// update current time string
						$('#sf-service-videoplayer-full-infobar-time-0').html(value.timeString);

						// adjust progress bar
                        if (nTotalTime) {
							var nBarWidth = parseInt((nMaxBarWidth - nMinBarWidth) * value.millisecond / nTotalTime) + nMinBarWidth;
                            $('#sf-service-videoplayer-full-infobar-progress-play').width(nBarWidth);
                        }
						break;
					case 'TOTALTIME':
						// update total time string
						$('#sf-service-videoplayer-full-infobar-time-2').html(ms2str(value));
						nTotalTime = value;
						break;
					case 'SKIPTIME':
						var eSkipPointer = $('#sf-service-videoplayer-full-infobar-skip-pointer');
						if (value == 'hide') {
							eSkipPointer.hide();
						}
						else {
							var nSkipPointerPos = nSkipPointerBase + parseInt((nMaxBarWidth - nMinBarWidth) * value / nTotalTime);
							eSkipPointer.css('left', nSkipPointerPos);
							if (eSkipPointer.css('display') == 'none') eSkipPointer.show();
						}
						break;
					case 'TITLE':
						$('#sf-service-videoplayer-full-infobar-title').html(value);
						break;
					case 'HELPBAR':
						$('#sf-service-videoplayer-full-helpbar').sfKeyHelp(value);
						break;
					default :
						break;
				}
			}
			function Subtitle () {
				this.show = function () {
					alert('[FullView][Subtitle] show()');
					$('#sf-service-videoplayer-full-subtitle').show();
				}
				this.hide = function () {
					alert('[FullView][Subtitle] hide()');
					$('#sf-service-videoplayer-full-subtitle').hide();
				}
				this.clear = function () {
					alert('[FullView][Subtitle] clear()');
					$('#sf-service-videoplayer-full-subtitle-table-td').html('');
				}
				this.setVerticalPosition = function (base, offset){
					alert('[FullView][Subtitle] setVerticalPosition('+base+','+offset+')');
					var eSubtitleTd = $('#sf-service-videoplayer-full-subtitle-table-td');

					if (base == 1) { // VPOS_TOP = 1;
                        eSubtitleTd.css({
                            'vertical-align': 'top',
							'padding-top': offset,
							'padding-bottom': 0
                        });
					}
					else if (base == 2) { // VPOS_BOTTOM = 2;
						eSubtitleTd.css({
                            'vertical-align': 'bottom',
							'padding-top': 0,
							'padding-bottom': offset
                        });
					}
				}
				this.setFontSize = function (px) {
					alert('[FullView][Subtitle] setFontSize('+px+')');
					$('#sf-service-videoplayer-full-subtitle-table-td').css({
						'font-size': px
					});
				}
				this.setFontColor = function (color) {
					alert('[FullView][Subtitle] setFontColor('+color+')');
					$('#sf-service-videoplayer-full-subtitle-table-td').css({
						'color': color
					});
				}
				this.setString = function (string) {
					alert('[FullView][Subtitle] setString('+string+')');
					$('#sf-service-videoplayer-full-subtitle-table-td').html(string);
				}
			}
		}

		var state2classname = {};	// for selecting state image class
		state2classname[self.STATE_PLAYING] = 'play';
		state2classname[self.STATE_PAUSED] = 'pause';
		state2classname[self.STATE_STOPPED] = 'stop';
		state2classname[self.STATE_BUFFERING] = 'play';
		state2classname[self.STATE_SCANNING] = 'play';
	}
})(sf);

var iCCheck3DEffect = new CCheck3DEffect();
function CCheck3DEffect(){
        var modelID = "";
        var strName = "Check3DEffect";          // Instance name
        var version = '1.0';                    // CCheck3DEffect object version for management.

        var eScreen3D = null;                           //3D effect
        var nnaviPlugin = null;
        var bInitialized = false;

        /*
         * Function : is3DEffectSupport
         *              해당 기기가 3D를 지원하는지 체크
         *
         *  return :
         *      1 - 3d 지원
         *      0 - 3d 미지원 모델
         *     -1 - 지원하지만 firmware update 필요
         */
        this.is3DEffectSupport = function(){
                alert("Check3DEffectSupport:");
                if(!bInitialized) {
                        init();
                }
                if(!eScreen3D || !nnaviPlugin) {
                        alert("\t"+strName+ " fail to initialize : can not find plugin object!");
                        return 0;
                }
                // Firmware version (year & ver)
                var firmwareVersion = firmwareVersion = nnaviPlugin.GetFirmware();
                var tmp = firmwareVersion.split('-');
                var firmwareYear = parseInt(tmp[1].replace("INFOLINK", ""), 10);
                var firmwareVer = parseInt(tmp[2], 10);

                // Model ID & product
                var retValue = -2;
                var paramModelID = window.location.search.match("modelid=([^\&]+)");
                var paramProduct = window.location.search.match("product=([^\&]+)");
                var modelID = (paramModelID && paramModelID.length==2) ? paramModelID[1] : '';
                var productType = (paramProduct && paramProduct.length==2) ? parseInt(paramProduct[1],10) : -1;
                alert("\tfirmware year: " + firmwareYear);
                alert("\tfirmware version: " + firmwareVer);
                alert("\tmodelid: " + modelID);
                alert("\tproductType: " + productType);
                alert("\tScreen Plugin: " + eScreen3D);
                alert("\tNNavi Plugin: " + nnaviPlugin);
                if(eScreen3D && eScreen3D.Flag3DEffectSupport() == 1){          //3D를 지원하는 기기인지 (1: 지원 0: 미지원)
                        if(firmwareYear == 2010) {      // 2010년도
                                return 1;
                        }
                        else if(firmwareYear == 2011) { // 2011년도
                                if(productType == 2){           // PL_TV_PRODUCT_TYPE_BD : 2
                                        if(eScreen3D && eScreen3D.Flag3DTVConnect() == 1){              //BD인 경우 Flag3DTVConnect()로 다시한번 확인 (1: 3D지원 기기, 0:미지원 기기)
                                                alert("\tBD-Flag3DTVConnect: true");
                                                retValue = 1;   // 3D지원기기 & BD & Flag3DTVConnect : 3D 지원
                                        }
                                        else{
                                                alert("\tBD-Flag3DTVConnect: false - This method can not guarantee the supporting 3D in this case. TV connected to this BD Player may support 3D.");
                                                retValue = 0;   // 3D지원기기 & BD & !Flag3DTVConnect : 3D 미지원 - 3DTV에서 HDMI 1.3 스펙을 제대로 지원하지 않아서 지원여부를 잘못 리턴할수도 있음.
                                        }
                                }
                                else{                                                                   // TV/MFM인 경우
                                        if(modelID.indexOf('X6') >= 0) {        // X6 모델 인경우 firmware version이 1003 미만인지 확인
                                                if(firmwareVer < 1003){
                                                        retValue = -1;                          // 3D지원기기 & X6모델 & 1003 미만: firmware update 필요
                                                }
                                                else {
                                                        retValue = 1;                           // 3D지원기기 & X6모델 & 1003 이상: 3D 지원
                                                }
                                        }
                                        else{                                                           // 그밖의 모델(Genoa, Novatek 등)인경우, 1001 미만인지 확인
                                                if(firmwareVer < 1001){
                                                        retValue = -1;                          // 3D지원기기 & X6모델아님 & 1001 미만: firmware update 필요
                                                }
                                                else {
                                                        retValue = 1;                           // 3D지원기기 & X6모델아님 & 1001 이상: 3D 지원
                                                }
                                        }
                                }
                        }
                        else if(firmwareYear > 2011) {  // 2012년도부터는 일단 지원한다고 리턴
                                retValue = 1;
                        }
                }
                else{
                        retValue = 0;   // 3D지원기기 아님: 3D 미지원
                }
                alert("\tCheck3DEffectSupport returns: "+((retValue==1)?"3D supported":(retValue==0)?"3D not supported":(retValue==-1)?"Firmware update needed":"Unknown"));
                return retValue;
        }


        function init() {
                alert(strName+ " init() version [ "+version+" ]");

                eScreen3D = document.getElementById('pluginObjectScreen3D');
                nnaviPlugin = document.getElementById('pluginObjectNNavi');

                if(!eScreen3D || !nnaviPlugin) {
                        var divID = "plugin_Object_for_3DEffect";
                        var divNode = document.createElement("div");

                        divNode.id = divID;
                        divNode.style.position="absolute";
                        divNode.style.top="0px";
                        divNode.style.left="0px";
                        document.body.appendChild(divNode);

                        if (!eScreen3D) {
                                document.getElementById(divID).innerHTML += "<object id='pluginObjectScreen3D' border=0 width=0 height =0 classid='clsid:SAMSUNG-INFOLINK-SCREEN' style='opacity:0.0;background:#000000;' ></object>";
                        }
                        if(!nnaviPlugin) {
                                document.getElementById(divID).innerHTML += "<object id='pluginObjectNNavi' border=0 width=0 height =0 classid='clsid:SAMSUNG-INFOLINK-NNAVI' style='opacity:0.0;background:#000000;' ></object>";
                        }
						eScreen3D = document.getElementById('pluginObjectScreen3D');
						nnaviPlugin = document.getElementById('pluginObjectNNavi');
                }

                if(!eScreen3D || !nnaviPlugin) {
                        alert(strName+ " fail to initialize : can not find plugin object!");
                        return;
                }
                bInitialized = true;
        }
 }/*
 * Title:  Service API (ImageViewer)
 * Definition of ImageViewer Service API
 */
(function (sf) {
	/*
	 *	Namespace: sf.service.ImageViewer
	 *		ImageViewer service module.
	 *
	 */
	sf.service.ImageViewer = new ImageViewer();

	sf.service.ImageViewer.NOERROR = 		0;
	sf.service.ImageViewer.ERR_NETWORK = 	1;
	sf.service.ImageViewer.ERR_RENDER = 	2;

	function ImageViewer () {

		////////////////////////
		///  Public methods  ///
		////////////////////////

		/*
		 * Function: draw
		 * 	Draws the given image.
		 *
		 * Parameters:
		 * 	o item - (Object) Contains image information.
		 * (start code)
		 * {
		 *     url: 'http://www.samsung.com/samsung.jpg',	// image URL
		 *     width: '1920',	// image width
		 *     height: '1080'	// image height
		 * }
		 * (end)
		 * 	o option - (Object)(Optional) Contains event handler
		 * (start code)
		 * {
		 *     oncomplete: onComplete,	// (Function) called when drawing completes.
		 *     onerror: onError			// (Function) called with SDeviceAPIError when error occurs
		 * }
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.draw = function (item, option) {
			alert('[ImageViewer] draw('+item.url+','+item.width+','+item.height+')');
			if (!bInitialize) initialize ();
			if (!bSetImageViewInstance) {
				alert('[ImageViewer] Execute setPosition() first.');
				return;
			}

			if (!isValidItem(item)) {
				alert('[ImageViewer] draw() : Invalid item..');
				return false;
			}

			runViewCleaner();
			iView.show();
			iView.showLoading();
			iImageView.prepare(	// (successCallback, errorCallback, url, option)
				function(){
					iView.hideLoading();
					if (option)
						iImageView.draw(option.oncomplete, option.onerror);
					else
						iImageView.draw(null, null);
				},
				onError,
				item.url,
				{
					width: item.width,
					height: item.height
				}
			);

			function onError (error) {
				alert('[ImageViewer] onError('+error+')');
				iView.hideLoading();
				switch (error.code) {
					case SDeviceAPIError.NOT_FOUND_ERR:
					case SDeviceAPIError.NETWORK_ERR:
					case SDeviceAPIError.NETWORK_SLOW_ERR:
					case SDeviceAPIError.SECURITY_ERR:
						iView.showNetworkError();
						setViewCleaner(iView.hideNetworkError);
						if (option && typeof option.onerror == 'function') {
							option.onerror(sf.service.ImageViewer.ERR_NETWORK);
						}
						break;

					case SDeviceAPIError.RENDER_ERR:
						iView.showNotSupport();
						setViewCleaner(iView.hideNotSupport);
						if (option && typeof option.onerror == 'function') {
							option.onerror(sf.service.ImageViewer.ERR_RENDER);
						}
						break;
					default:
						break;
				}
			}
		};
		/*
		 * Function: clear
		 * 	Clears the ImageViewer.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.clear = function () {
			alert('[ImageViewer] clear()');
			// Stop Image
			if (!bInitialize) initialize();
			if (!bSetImageViewInstance) {
				alert('[ImageViewer] Check the usage.');
				return;
			}
			iImageView.clear();
			runViewCleaner();
			iView.hideLoading();
		};
		/*
		 * Function: setPosition
		 * 	Sets the ImageViewer position to given rect.
		 *
		 * Parameters:
		 * 	o rect - (Object) Position information.
		 * (start code)
		 * {
		 *     left: 100,	// left-coordinate of ImageViewer area.
		 *     top: 100,	// top-coordinate of ImageViewer area.
		 *     width: 500,	// width of ImageViewer area.
		 *     height: 400	// height of ImageViewer area. ImageViewer takes 73px of the area base to show information panel.
		 * }
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setPosition = function (rect) {
			alert('[ImageViewer] setPosition('+rect+')');
			if (!bInitialize) initialize ();
			if (!bSetImageViewInstance) {
				setImageViewInstance(new SRect(rect.left, rect.top, rect.width, rect.height));
			}
			else {
				iImageView.setDisplayRect(new SRect(rect.left, rect.top, rect.width, rect.height));
			}
			iView.setPosition(rect);
		};
		/*
		 * Function: show
		 * 	Shows the ImageViewer.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.show = function () {
			alert('[ImageViewer] show()');
			if (!bInitialize) initialize ();
			iView.show();
			iImageView.show();
		}
		/*
		 * Function: hide
		 * 	Hides the ImageViewer.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.hide = function () {
			alert('[ImageViewer] hide()');
			if (!bInitialize) initialize ();
			this.clear();	// hide 시에는 이미지 재생을 중지시킴
			iView.hide();
			iImageView.hide();
		};

		// for test
		this.stop = function () {
			alert('[ImageViewer] stop()');
			iImageView.stop();
		}

		///////////////////
		///  Variables  ///
		///////////////////

		/*
		 * HTML 구조
		 * <Div: ImageViewer Container>		// z-index: 30
		 *     <Div: Partial Container>					// z-index: 10
		 *         <Object: ImageViewer Plugin Object for Partial>	// z-index: 10
		 *         <Div: Partial View>								// z-index: 15
		 *
		 *     <Div: SlideShow Container>				// z-index: 20
		 *         <Object: Imageviewer Plugin Object for SlideShow>	// z-index: 10
		 *         <Div: SlideShow View>								// z-index: 15
		 */

		var IMAGEVIEWER_CONTAINER_DIV_ID = 'sf-service-imageviewer-container';
		var IMAGEVIEWER_CONTAINER_DIV_ZINDEX = 30;
		var IMAGEVIEWER_OBJECT_ZINDEX = 10;
		var IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ID = 'sf-service-imageviewer-partial-container';	// <object>는 0,0 기준이어야 하기 때문에,
		var IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ZINDEX = 10;

		var self = this;
		var bInitialize = false;
		var bSetImageViewInstance = false;
		var iImageView = null;	// ImageView instance
		var iView = null;
		var fnViewCleaner = null;

		function initialize () {
			alert('[ImageViewer] initialize()');
			if (!document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID)) {
				$('<div id="'+IMAGEVIEWER_CONTAINER_DIV_ID+'" style="z-index:'+IMAGEVIEWER_CONTAINER_DIV_ZINDEX+';"></div>').appendTo('body');	// container 삽입
			}

			iView = new View(IMAGEVIEWER_CONTAINER_DIV_ID);

			bInitialize = true;

			// for test
			self.View = iView;
		}
        function setImageViewInstance(rect){
			alert('[ImageViewer] setImageViewInstance('+rect+')');
        	deviceapis.imageview.getImageView (function(instance){
				iImageView = instance;
				self.ImageView = instance;

				var oOption = {
					containerID: IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ID,
					zIndex: IMAGEVIEWER_OBJECT_ZINDEX,
					displayRect: new SRect(rect.left, rect.top, rect.width, rect.height)
				}
				iImageView.init(oOption);
			});

			bSetImageViewInstance = true;
        }
		function isValidItem (item) {
			var bValidate = false;
			if (item && item.url && item.width && item.height) {
				bValidate = true;
			}
			alert('[ImageViewer] isValidItem('+item+') returns ' + bValidate);
			return bValidate;
		}
		function setViewCleaner (viewCleaner) {
			alert('[ImageViewer] setViewCleaner()');
			if (typeof fnViewCleaner == 'function') fnViewCleaner();
			fnViewCleaner = viewCleaner;
		}
		function runViewCleaner () {
			alert('[ImageViewer] runViewCleaner()');
			if (typeof fnViewCleaner == 'function') {
				fnViewCleaner();
				fnViewCleaner = null;
			}
		}

		//////////////
		///  View  ///
		//////////////
		function View (containerID) {

			$('<div id="'+IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ID+'" style="z-index:'+IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ZINDEX+';"></div>').appendTo($('#'+containerID));

			$('<div id="sf-service-imageviewer-partial"></div>').html([
				'<div id="sf-service-imageviewer-partial-neterr-icon"></div>',
				'<div id="sf-service-imageviewer-partial-neterr-text">' + sf.lang.SID_ALERT_NETWORKERROR_MSG + '</div>',
				'<div id="sf-service-imageviewer-partial-notsup-icon"></div>',
				'<div id="sf-service-imageviewer-partial-notsup-text">' + sf.lang.SID_NOT_SUPPORT_FORMAT + '</div>',
				'<div id="sf-service-imageviewer-partial-loading"></div>',
			].join('')).appendTo($('#'+IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ID));

            this.init = function(){
				alert('[ImageViewer][view] init()');
            }
            this.show = function(){
				alert('[ImageViewer][view] show()');
				// <object> 상위 div가 display:none 되지 않도록 view, object 구분하여 show/hide.
            	$('#sf-service-imageviewer-partial').show();
//            	$('#'+IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ID).show();
            }
            this.hide = function(){
				alert('[ImageViewer][view] hide()');
            	$('#sf-service-imageviewer-partial').hide();
//            	$('#'+IMAGEVIEWER_PARTIAL_CONTAINER_DIV_ID).hide();
				this.hideLoading();
            }
			this.setPosition = function (rect) {
				alert('[ImageViewer][view] setPosition('+rect+')');
				$('#sf-service-imageviewer-partial').css({
					left: rect.left,
					top: rect.top,
					width: rect.width,
					height: rect.height
				});
			}
			this.showNetworkError = function () {
				alert('[ImageViewer][view] showNetworkError()');
				$('#sf-service-imageviewer-partial-neterr-icon').show();
				$('#sf-service-imageviewer-partial-neterr-text').show();
			}
			this.hideNetworkError = function () {
				alert('[ImageViewer][view] hideNetworkError()');
				$('#sf-service-imageviewer-partial-neterr-icon').hide();
				$('#sf-service-imageviewer-partial-neterr-text').hide();
			}
			this.showNotSupport = function () {
				alert('[ImageViewer][view] showNotSupport()');
				$('#sf-service-imageviewer-partial-notsup-icon').show();
				$('#sf-service-imageviewer-partial-notsup-text').show();
			}
			this.hideNotSupport = function () {
				alert('[ImageViewer][view] hideNotSupport()');
				$('#sf-service-imageviewer-partial-notsup-icon').hide();
				$('#sf-service-imageviewer-partial-notsup-text').hide();
			}
			this.showLoading = function () {
				alert('[ImageViewer][view] showLoading()');
				$('#sf-service-imageviewer-partial-loading').sfLoading('show');
			}
			this.hideLoading = function () {
				alert('[ImageViewer][view] hideLoading()');
				$('#sf-service-imageviewer-partial-loading').sfLoading('hide');
			}
		}
	}
})(sf);

/*
 * Slideshow functions
 */
(function (sf) {
	/*
	 *	Namespace: sf.service.ImageViewer.SlideShow
	 *		SlideShow service module.
	 *
	 */
	sf.service.ImageViewer.SlideShow = new SlideShow();

	function SlideShow () {
		/*
		 * Function: init
		 * 	Initializes the SlideShow with given option.
		 *
		 * Parameters:
		 * 	o option - (Object)
		 * (start code)
		 * {
		 *     timegap: {,			// (Object)(Optional) the gap of time between images.
		 *         slow: 15000,		// (Number) when the slideshow speed is slow. (ms)
		 *         normal: 10000,	// (Number) normal
		 *         fast: 5000		// (Number) fast
		 *     }
		 *     cyclic: true,		// (Boolean)(Optional) whether to repeat or not.
		 *     onlastitem: function(idx){}	// (Function)(Optional) called when SlideShow reaches the last third, second, first items. You can add items on this function. 'idx' argument is the index from last item.
		 *     onerror: function(){}	// (Function)(Optional) called when error occurs.
		 * }
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.init = function (option) {
			alert('[SlideShow] init()');
			if (option) {
				if (option.cyclic !== undefined) {
					bCyclic = option.cyclic;
				}
				if (option.timegap !== undefined) {
					oTimegap = option.timegap;
				}
				if (option.onlastitem !== undefined) {
					cbOnLastItem = option.onlastitem;
				}
				if (option.onerror !== undefined) {
					cbOnError = option.onerror;
				}
				if (option.onimagechange !== undefined) {
					cbOnImageChange = option.onimagechange;
				}
			}
			if (!bInitialize) initialize();
		}
		/*
		 * Function: start
		 * 	Starts the SlideShow.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.start = function () {
			alert('[SlideShow] start()');
			if (!bInitialize) initialize();

			if (bSlideshowInProgress) {
				alert('[SlideShow] SlideShow is already started.');
				return false;
			}

			sf.service.ImageViewer.clear();

			initEnvVars();

			// control ImageView
			iImageView.show();
			iImageView.setSlideShow(true);

			drawImage(nCurItemIdx);
			playShow();

			iView.showBars();
			iView.show();

			bSlideshowInProgress = true;

			this.focus();
		}
		/*
		 * Function: stop
		 * 	Stops the SlideShow. In the normal cases, you should call this function in RETURN key handler.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.stop = function () {
			alert('[SlideShow] stop()');
			if (!bInitialize) initialize();

			if (!bSlideshowInProgress) {
				alert('[SlideShow] SlideShow is not started.');
				return false;
			}

			// control ImageView
			iImageView.setSlideShow(false);
			iImageView.clear();
			iImageView.hide();

			// View update -> View hide 전 ImageView 가 Stop 되도록 해야 함. -> display:none 상태에서는 함수콜 오류
			iView.hide('LOADING');
			iView.hideBars();
			iView.hide();

			clearShowTimer();
			sf.scene.returnFocus();
			bSlideshowInProgress = false;
			sState = 'stop';
//			sf.service.setVolumeControl(false);
		}
		this.pause = function () {
			alert('[SlideShow] pause()');
			pauseShow();
		}
		this.resume = function () {
			alert('[SlideShow] resume()');
			playShow();
		}
		this.focus = function () {
			alert('[SlideShow] focus()');
			if (!bInitialize) initialize();
			$('#'+ANCHOR_ID).focus();
		}
		/*
		 * Function: setItem
		 * 	Sets the SlideShow item.
		 *
		 * Parameters:
		 * 	o items - (Array) image item array.
		 * (start code)
		 * [
		 *     {
		 *         url: 'http://www.samsung.com/sample/image.jpg',	// (String) image URL
		 *         width: 1920,	// (Number) image width
		 *         height: 1080	// (Number) image height
		 *     },
		 *     ...
		 * ]
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setItem = function (items) {
			alert('[ImageViewer][SlideShow] setItem('+items+')');
			if (!bInitialize) initialize();

			if (isValidItem(items)) {
				if (items.constructor != Array) items = [items];
				aItem = items;
				if (aItem.length > MAX_ITEM_LENGTH)
					aItem.splice(MAX_ITEM_LENGTH);

				if (bSlideshowInProgress) {	// slideshow 진행 중 들어온 요청이면
					nCurItemIdx = nNextItemIdx = 0;	// 설정된 items의 첫번째 item을 재생
					fnProceed = null;	// playShow()에서 새로 설정된 item을 prepare 하도록 함
					if (sState == 'play') {
						if (aItem.length <= 1) {
							pauseShow();
							return true;
						};
						setShowTimer();
						prepare(nNextItemIdx, nEffect);
					}
				}
				return true;
			}
			else {
				alert('[ImageViewer][SlideShow] setItem - Parameter should be the object that has "url" property, or its Array.');
				return false;
			}
		}
		/*
		 * Function: addItem
		 *  Adds the SlideShow item. The items you set by "setItem()" will be kept and the new items will be added to tail of it.
		 *
		 * Parameters:
		 * 	o items - (Array) image item array.
		 * (start code)
		 * [
		 *     {
		 *         url: 'http://www.samsung.com/sample/image.jpg',	// (String) image URL
		 *         width: 1920,	// (Number) image width
		 *         height: 1080	// (Number) image height
		 *     },
		 *     ...
		 * ]
		 * (end)
		 *
		 * Returns:
		 * 	nothing
		 */
		this.addItem = function (items) {
			alert('[ImageViewer][SlideShow] addItems('+items+')');
			if (!bInitialize) initialize();

			if (isValidItem(items)) {
				if (items.constructor != Array) items = [items];
				aItem = aItem.concat(items);
				if (aItem.length > MAX_ITEM_LENGTH)
					aItem = aItem.slice(aItem.length - MAX_ITEM_LENGTH); // 앞부분 아이템을 뺀다.
				return true;
			}
			else {
				alert('[ImageViewer][SlideShow] addItems - Parameter should be the object that has "url" property, or its Array.');
				return false;
			}
		}
		/*
		 * Function: getItemIdx
		 * 	Gets the index of the current drawn image item.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	(Number) the index of the current drawn image item.
		 */
		this.getItemIdx = function () {
			alert('[ImageViewer][SlideShow] getItemIdx() returns ' + nCurItemIdx);
			return nCurItemIdx;
		}
		/*
		 * Function: setItemIdx
		 * 	Sets the item index to draw next.
		 *
		 * Parameters:
		 * 	o N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setItemIdx = function (itemIdx) {
			alert('[ImageViewer][SlideShow] setItemIdx('+itemIdx+')');
			if (!bInitialize) initialize();

			if (itemIdx >= 0 && itemIdx <= aItem.length - 1) {
				nCurItemIdx = itemIdx;
				return true;
			}
			else {
				alert('[ImageViewer][SlideShow] invalid index : ' + itemIdx);
				return false;
			}
		}
		/*
		 * Function: setKeyHandler
		 * 	Sets key handler.
		 *
		 * Parameters:
		 * 	o key - (Number) sf.key value.
		 *  o handler - (Function) key handler function. If the given key pressed, this function will be called.
		 *
		 * Returns:
		 * 	nothing
		 */
		this.setKeyHandler = function (key, handler) {
			alert('[ImageViewer][SlideShow] setKeyHandler('+key+','+(handler?handler:'')+')');
			if (!bInitialize) initialize();

			if (typeof key == 'object') {	// key:handler map이 올 경우
				for (var prop in key) {
					oKeyHandlerMap[prop] = key[prop];
				}
			}
			else {
				oKeyHandlerMap[key] = handler;
			}
		}
		this.setEffect = function (effect) {
			alert('[ImageViewer][SlideShow] setEffect('+effect+')');
			nEffect = effect;
			if (sState == 'play') {
				fnProceed = null;
				setShowTimer();
				nNextItemIdx = getNextItemIdx(nCurItemIdx);
				prepare(nNextItemIdx, nEffect);
			}
			else if (sState == 'pause') {
				fnProceed = null;
				nNextItemIdx = getNextItemIdx(nCurItemIdx);
				prepare(nNextItemIdx, nEffect);
			}
		}
		this.getState = function () {
			alert('[ImageViewer][SlideShow] getState() returns ' + sState);
			return sState;
		}
		this.getTransitionEffectList = function () {
			alert('[ImageViewer][SlideShow] getTransitionEffectList()');
			if (!bInitialize) initialize(); // initialize 에서 this.ImageView 가 생성됨.
			return this.ImageView.getTransitionEffectList();
		}

		// Key handler
		function handleKeydown () {
			var keyCode = event.keyCode;
			alert('[ImageViewer][SlideShow] handleKeydown('+keyCode+')');

			if (bEffectProcess && keyCode != sf.key.RETURN && keyCode != sf.key.EXIT) {
				alert('[ImageViewer][SlideShow] key blocked. transition effect is going on.');
				return;
			}

			if (typeof oKeyHandlerMap[keyCode] == 'function') {
				// 사용자가 등록한 keyhandler가 false를 return 하면 ImageViewer keyhandler 실행 안함.
				if (oKeyHandlerMap[keyCode]() == false) {
					return;
				}
			}

			switch(keyCode) {
				case sf.key.LEFT:
				case sf.key.RIGHT:
					if (aItem && aItem.length == 1) return;
					if (sState == 'play'){
						clearShowTimer();
						iImageView.stop();
					}
					var getItemIdx = (keyCode == sf.key.LEFT) ? getPrevItemIdx : getNextItemIdx;
					nNextItemIdx = getItemIdx(nCurItemIdx);
					drawImage (nNextItemIdx);
					iView.refresh('infobar', getInfoMap());
					iView.showBars();
					break;

				case sf.key.UP:
				case sf.key.DOWN:
					break;

				case sf.key.ENTER:
					if (aItem && aItem.length == 1) return;
					sState == 'play' ? pauseShow() : playShow();
					iView.refresh('helpbar', getKeyHelpMap());
					iView.showBars();
					break;

				case sf.key.RETURN:
				case sf.key.EXIT:
					sf.key.preventDefault();
					break;

				case sf.key.REW:
					if (aItem && aItem.length == 1) return;
					if (sSpeed == 'fast') {
						sSpeed = 'normal';
					}
					else if (sSpeed == 'normal') {
						sSpeed = 'slow';
					}
					iView.refresh('infobar', getInfoMap());
					iView.showBars();
					if (sState == 'play') setShowTimer();	// refresh timer
					break;

				case sf.key.FF:
					if (aItem && aItem.length == 1) return;
					if (sSpeed == 'slow') {
						sSpeed = 'normal';
					}
					else if (sSpeed == 'normal') {
						sSpeed = 'fast';
					}
					iView.refresh('infobar', getInfoMap());
					iView.showBars();
					if (sState == 'play') setShowTimer();	// refresh timer
					break;

				case sf.key.INFO:
					iView.toggleBars();
					break;

				case sf.key.PLAY:
					if (aItem && aItem.length == 1) return;
					playShow();
					iView.refresh('helpbar', getKeyHelpMap());
					iView.showBars();
					break;

				case sf.key.PAUSE:
					if (aItem && aItem.length == 1) return;
					pauseShow();
					iView.refresh('helpbar', getKeyHelpMap());
					iView.showBars();
					break;

				case sf.key.TOOLS:
					// network error를 화면에 보여주고 있는 상태에서는 동작하지 않음.
//					var curStatus = sState;
//					pauseShow();
//					iView.refresh('helpbar', getKeyHelpMap());
//					$('#sf-service-slideshow-tool').sfToolPopup('show', function(){
//						$('#'+ANCHOR_ID).focus();
//						if (curStatus == 'play') {
//							playShow();
//						}
//					});
					break;

				case sf.key.RED:
					break;
				case sf.key.GREEN:
					break;
				case sf.key.YELLOW:
					break;
				case sf.key.BLUE:
					break;
			}

			if (typeof oKeyDownHandler[keyCode] == 'function') {
				alert('[ImageViewer][SlideShow] Run user\'s key handler ! ['+keyCode+']');
				oKeyDownHandler[keyCode]();
			}
		}

		//////////////////////////////
		///  environment settings  ///
		//////////////////////////////

		var IMAGEVIEWER_CONTAINER_DIV_ID = 'sf-service-imageviewer-container';
		var IMAGEVIEWER_CONTAINER_DIV_ZINDEX = 30;
		var SLIDESHOW_CONTAINER_DIV_ID = 'sf-service-slideshow-container';
		var SLIDESHOW_CONTAINER_DIV_ZINDEX = 20;
		var IMAGEVIEWER_OBJECT_ZINDEX = 10;
		var ANCHOR_ID = 'sf-service-slideshow-anchor';

		var MAX_ITEM_LENGTH = 100;	// item 제한
		var REAR_NOTIFIER_COUNT = 3;	// 데이터 끝을 알려주는 시점

		var self = this;
		var bInitialize = false;
		var iView = null;
		var iImageView = null;		// ImageView instance
		var aItem = null;			// Image data array, [{url:xxx}]
		var nCurItemIdx = 0;		// Current item index
		var nNextItemIdx = 0;		// Next item index prepared

		var cbOnError = null;		// error callback
		var cbOnLastItem = null;	// last item notifier callback

		var oSpeedText = {slow:'',normal:'',fast:''};	// text will be set initialize() for language file loading timing.
		var sSpeed = 'normal';
		var oStatusText = {play:'',pause:''};
		var sState = 'stop'; // play: SlideShow 진행 중, pause: SlideShow 일시정지, stop: stop()이 불린 상태로, 화면에 보이지 않는 상태.
		var bCyclic = true;
		var oTimegap = {		// ms
			slow: 10000,
			normal: 5000,
			fast: 3000
		};
		var oKeyHandlerMap = {};
		var nEffect = deviceapis.imageview.EFFECT_RANDOM;
		var oEffectText = null;

		// keydown handler
		var oKeyDownHandler = {};

		/////////////////////////////////////
		///  slideshow process variables  ///
		/////////////////////////////////////
		var bSlideshowInProgress = false;	// whether slideshow is in progress or not
		var bDrawImageProgress = false;	// whether being in progress of "drawImage()"
		var bPrepareProgress = false;	// whether being in progress of "prepare()"
		var bPrepared = false;	// whether "prepare()" complete

		var bEffectProcess = false;
		var hShowTimer = null;	// show timer
		var fnProceed = null;	// fnProceed function
		var fnViewCleaner = null;	// be executed before ImageView complete drawing
		var errorCounter = new ErrorCounter(5);

		function onDecodingComplete () {	// ImageView Buffering Complete
			alert('[ImageViewer][SlideShow] onDecodingComplete()');
			if (typeof fnProceed == 'function') {
				if (sState == 'play') {
					alert('[ImageViewer][SlideShow] run fnProceed function.');
					fnProceed();
					fnProceed = null;
				}
			}
			else {
				alert('[ImageViewer][SlideShow] set fnProceed function.');
				fnProceed = showPreparedImage;
			}
		}
		function onRenderingComplete () {	// ImageView Rendering Complete
			alert('[ImageViewer][SlideShow] onRenderingComplete()');
			bEffectProcess = false;
			errorCounter.init();

			if (typeof cbOnImageChange == 'function') {
				cbOnImageChange();
			}

			// rear notifier
			if (typeof cbOnLastItem == 'function') { // 마지막 notify 에서 addItem 할 수 있도록 위치 변경
				var whereAmI = aItem.length - nCurItemIdx;
				if (whereAmI <= REAR_NOTIFIER_COUNT) {
					cbOnLastItem(whereAmI);
				}
			}

			if (sState == 'play') {
				if (aItem.length <= 1 || (!bCyclic && nCurItemIdx == aItem.length -1)) {
					pauseShow();
					return;
				};
				fnProceed = null;
				setShowTimer();
				nNextItemIdx = getNextItemIdx(nCurItemIdx);
				prepare(nNextItemIdx, nEffect);
			}
		}
		function setShowTimer () {
			alert('[ImageViewer][SlideShow] setShowTimer()');
			clearShowTimer();
			hShowTimer = setTimeout(function(){
				alert('[ImageViewer][SlideShow] hShowTimer runs.');
				hShowTimer = null;
				if (sState != 'play') return;
				if (typeof fnProceed == 'function') {
					alert('[ImageViewer][SlideShow] run fnProceed function.');
					fnProceed();
					fnProceed = null;
				}
				else {
					alert('[ImageViewer][SlideShow] set fnProceed function.');
					fnProceed = showPreparedImage;
				}
			}, (oTimegap[sSpeed] || oTimegap['normal']));
			alert('[ImageViewer][SlideShow] Timer is set');
		}
		function clearShowTimer () {
			alert('[ImageViewer][SlideShow] clearShowTimer()');
			if (hShowTimer) {
				clearTimeout(hShowTimer);
				hShowTimer = null;
			}
		}
		function drawImage (itemIdx) {
			alert('[ImageViewer][SlideShow] drawImage('+itemIdx+')');
			runViewCleaner();
			iView.show('LOADING');
			nCurItemIdx = itemIdx;
			bDrawImageProgress = true;
			iImageView.prepare(	// (successCallback, errorCallback, url, option)
				function(){
					iView.hide('LOADING');
					iImageView.draw(function(){
						bDrawImageProgress = false;
						onRenderingComplete();
					}, onError);
				},
				function(error){
					iView.hide('LOADING');
					bDrawImageProgress = false;
					onError(error);
				},
				aItem[itemIdx].url,
				{
					width: aItem[itemIdx].width,
					height: aItem[itemIdx].height,
					effect: deviceapis.imageview.EFFECT_NONE
				}
			);

			function onError (error) {
				alert('[ImageViewer][SlideShow] drawImage() -> onError('+error+')');
				switch (error.code) {
					case SDeviceAPIError.NOT_FOUND_ERR:
					case SDeviceAPIError.NETWORK_ERR:
					case SDeviceAPIError.NETWORK_SLOW_ERR:
					case SDeviceAPIError.SECURITY_ERR:
						onNetworkError();
						if (typeof cbOnError == 'function') {
							cbOnError(sf.service.ImageViewer.SlideShow.ERR_NETWORK);
						}
						break;

					case SDeviceAPIError.RENDER_ERR:
						onRenderError();
						if (typeof cbOnError == 'function') {
							cbOnError(sf.service.ImageViewer.SlideShow.ERR_RENDER);
						}
						break;

					default:
						break;
				}
			}
			function onNetworkError () {
				alert('[ImageViewer][SlideShow] drawImage() -> onNetworkError()');
				// NETWORK_ERROR view 보여주고 다음 사진 준비
				// show network error
				iImageView.clear();
				iView.show('NETWORK_ERROR');
				setViewCleaner(function () {
					iView.hide('NETWORK_ERROR');
				});
				iView.refresh('helpbar', getKeyHelpMap());

				if (sState == 'play') {
					if (aItem.length <= 1 || (!bCyclic && nCurItemIdx == aItem.length -1)) {
						pauseShow();
					}
					else {
						fnProceed = null;
						setShowTimer();
						nNextItemIdx = getNextItemIdx(nCurItemIdx);
						prepare(nNextItemIdx, deviceapis.imageview.EFFECT_NONE);
					}
				}
			}
			function onRenderError () {
				alert('[ImageViewer][SlideShow] drawImage() -> onRenderError()');
				// NOT_SUPPORT view 보여주고 다음 사진 준비

				iView.hide('LOADING');

				// show network error
				iImageView.clear();
				iView.show('NOT_SUPPORT');
				setViewCleaner(function () {
					iView.hide('NOT_SUPPORT');
				});
				iView.refresh('infobar', getInfoMap());

				if (sState == 'play') {
					if (aItem.length <= 1 || (!bCyclic && nCurItemIdx == aItem.length - 1)) {
						pauseShow();
					}
					else {
						fnProceed = null;
						setShowTimer();
						nNextItemIdx = getNextItemIdx(nCurItemIdx);
						prepare(nNextItemIdx, deviceapis.imageview.EFFECT_NONE);
					}
				}
			}
		}
		function prepare (idx, effect) {
			alert('[ImageViewer][SlideShow] prepare('+idx+','+effect+')');
			var item = aItem[idx];
			bPrepareProgress = true;
			bPrepared = false;
			iImageView.prepare(	// (successCallback, errorCallback, url, option)
				function () {
					bPrepareProgress = false;
					bPrepared = true;
					onDecodingComplete();
				},
				function (error) {
					bPrepareProgress = false;
					bPrepared = false;
					onError(error);
				},
				item.url,
				{
					width: item.width,
					height: item.height,
					effect: effect
				}
			);
			function onError (error) {
				alert('[ImageViewer][SlideShow] prepare() -> onError('+error+')');
				switch (error.code) {
					case SDeviceAPIError.NOT_FOUND_ERR:
					case SDeviceAPIError.NETWORK_ERR:
					case SDeviceAPIError.NETWORK_SLOW_ERR:
					case SDeviceAPIError.SECURITY_ERR:
						onNetworkError();
						if (typeof cbOnError == 'function') {
							cbOnError(sf.service.ImageViewer.SlideShow.ERR_NETWORK);
						}
						break;

					case SDeviceAPIError.RENDER_ERR:
						onRenderError();
						if (typeof cbOnError == 'function') {
							cbOnError(sf.service.ImageViewer.SlideShow.ERR_RENDER);
						}
						break;

					default:
						break;
				}
			}
			function onNetworkError () {
				alert('[ImageViewer][SlideShow] prepare() -> onNetworkError()');
				// 5회 실패할 때까지 다음 사진 재생 시도
				// 연속 5회 실패할 경우 NETWORK_ERROR 보여주고 Pause

				if (errorCounter.inc()) {	// is count reached Max?
					// show network error
					iImageView.clear();
					iView.show('NETWORK_ERROR');
					setViewCleaner(function () {
						iView.hide('NETWORK_ERROR');
					});
					nCurItemIdx = nNextItemIdx;
					iView.refresh('helpbar', getKeyHelpMap());

					pauseShow();
				}
				else {
					if (sState == 'play') {
						// prepare next image
						if (aItem.length <= 1 || (!bCyclic && nCurItemIdx == aItem.length - 1)) {
							pauseShow();
							return;
						};
//						nCurItemIdx = nNextItemIdx;
						nNextItemIdx = getNextItemIdx(nNextItemIdx);
						prepare(nNextItemIdx, nEffect);
					}
				}
			}
			function onRenderError () {
				alert('[ImageViewer][SlideShow] prepare() -> onRenderError()');
				// ShowTimer 중 -> ShowTimer 끝나는 시점에 NOT_SUPPORT 보여주고 다음 사진 재생
				// ShowTimer 끝 -> NOT_SUPPORT 보여주고 다음 사진 재생

				if (sState == 'play') {
					if (fnProceed) {    // show timer가 먼저 끝나서 fnProceed가 등록되어 있으면
						alert('[ImageViewer][SlideShow] show Not Support and prepare next.');
						showAndNext();
						fnProceed = null;
					}
					else {
						alert('[ImageViewer][SlideShow] set fnProceed function. -> show Not Support)');
						fnProceed = showAndNext;
					}
				}

				function showAndNext () {
					iImageView.clear();
					iView.show('NOT_SUPPORT');
					setViewCleaner(function () {
						iView.hide('NOT_SUPPORT');
					});
					nCurItemIdx = nNextItemIdx;
					iView.refresh('infobar', getInfoMap());
					if (aItem.length <= 1 || (!bCyclic && nCurItemIdx == aItem.length -1)) {
						pauseShow();
						return;
					};
					fnProceed = null;
					setShowTimer();
					nNextItemIdx = getNextItemIdx(nCurItemIdx);
					prepare(nNextItemIdx, deviceapis.imageview.EFFECT_NONE);
				}
			}
		}
		function showPreparedImage () {
			alert('[ImageViewer][SlideShow] showPreparedImage()');

			nCurItemIdx = nNextItemIdx;
			iView.refresh('infobar', getInfoMap());
			runViewCleaner();
			bEffectProcess = true;
			iImageView.draw(function(){
				bPrepared = false;
				onRenderingComplete();
			}, null);
		}
		function getNextItemIdx (idx) {
			var next = idx + 1, total = aItem ? aItem.length : 0;
			if (next > total - 1) {
				next = 0;
			}
			alert('[ImageViewer][SlideShow] getNextItemIdx('+idx+') returns ' + next);
			return next;
		}
		function getPrevItemIdx (idx) {
			var next = --idx, total = aItem ? aItem.length : 0;
			if (next < 0) {
				next = total - 1;
			}
			alert('[ImageViewer][SlideShow] getPrevItemIdx('+idx+') returns ' + next);
			return next;
		}
		function playShow () {
			alert('[ImageViewer][SlideShow] playShow()');

			sState = 'play';

			if (aItem.length <= 1) {
				pauseShow();
				return;
			};

			if (!bDrawImageProgress) {	// drawImage()로 이미지를 그리고 있는 중이면 RENDERING_COMPLETE 에서 timer 설정됨
				setShowTimer();
				if (!bPrepared && !bPrepareProgress) {	// prepare 중이거나 prepare 완료 상태가 아니면 prepare() 실행
					nNextItemIdx = getNextItemIdx(nCurItemIdx);
					prepare(nNextItemIdx, nEffect);
				}
			}

			iView.refresh('infobar', getInfoMap());
			iView.refresh('helpbar', getKeyHelpMap());
			sf.service.setScreenSaver(false);
		}
		function pauseShow () {
			alert('[ImageViewer][SlideShow] pauseShow()');

			sState = 'pause';

			clearShowTimer();	// ShowTimer 제거
//			fnProceed = null;	// proceed를 null로 만들어 ImageView event를 무시하도록 함.

			iView.refresh('infobar', getInfoMap());
			iView.refresh('helpbar', getKeyHelpMap());
			sf.service.setScreenSaver(true);
		}
		function getKeyHelpMap () {
			var ret = {};
			ret['REWFF'] = sf.lang.SID_SLOW_FAST;
			ret['LEFTRIGHT'] = sf.lang.SID_PREV_NEXT;
			ret['ENTER'] = sState == 'play' ? sf.lang.SID_STOP_SLIDESHOW : sf.lang.SID_START_SLIDESHOW;
			ret['RETURN'] = sf.lang.SID_RETURN;
			return ret;
		}
		function getInfoMap () {
			var ret = {};
			ret['STATE'] = sState;
			ret['SPEED'] = oSpeedText[sSpeed];
			ret['INDEX'] = (nCurItemIdx+1) + ' / ' + aItem.length;

			var item = aItem[nCurItemIdx];
			ret['FILENAME'] = item.filename || '';
			ret['RESOLUTION'] = (item.width && item.height) ? item.width + ' x ' + item.height : '';
			ret['DATE'] = item.date || '';
			return ret;
		}
		// initialize
		function initialize () {
			alert('[ImageViewer][SlideShow] initialize()');

			// ImageViewer container div
			if (!document.getElementById(IMAGEVIEWER_CONTAINER_DIV_ID)) {
				$('<div id="'+IMAGEVIEWER_CONTAINER_DIV_ID+'" style="z-index:'+IMAGEVIEWER_CONTAINER_DIV_ZINDEX+';"></div>').appendTo('body');
			}

			iView = new View(IMAGEVIEWER_CONTAINER_DIV_ID);
			iView.init();

			// SlideShow anchor
			$('<a href="javascript:void(0)" id="'+ANCHOR_ID+'"></a>').appendTo($('#'+SLIDESHOW_CONTAINER_DIV_ID));
			setTimeout (function(){
				$('#'+ANCHOR_ID).keydown(handleKeydown);
			}, 0);

			deviceapis.imageview.getImageView (function(instance){
				iImageView = instance;
				self.ImageView = instance;

				var oInitOption = {
					containerID: SLIDESHOW_CONTAINER_DIV_ID,
					zIndex: IMAGEVIEWER_OBJECT_ZINDEX,
					displayRect: new SRect(0,0,curWidget.width,curWidget.height)
				}
				iImageView.init(oInitOption);
			});

			iView.hide();
			iImageView.hide();

			setTextResource();	// 언어 파일 로드 후 변수 할당되도록 함

			bInitialize = true;
		}
		function setViewCleaner (viewCleaner) {
			alert('[ImageViewer][SlideShow] setViewCleaner()');
			if (typeof fnViewCleaner == 'function') fnViewCleaner();
			fnViewCleaner = viewCleaner;
		}
		function runViewCleaner () {
			alert('[ImageViewer][SlideShow] runViewCleaner()');
			if (typeof fnViewCleaner == 'function') {
				fnViewCleaner();
				fnViewCleaner = null;
			}
		}
		function initEnvVars () {
			alert('[ImageViewer][SlideShow] initEnvVars()');

			bSlideshowInProgress = false;
			bDrawImageProgress = false;
			bPrepareProgress = false;
			bPrepared = false;
			bEffectProcess = false;
			hShowTimer = null;
			fnProceed = null;
			fnViewCleaner = null;
		}
		function setTextResource () {
			alert('[ImageViewer][SlideShow] setTextResource()');
			oEffectText = {
				FADE1       : sf.lang.SID_FADE1,
				FADE2       : sf.lang.SID_FADE2,
				BLIND       : sf.lang.SID_BLIND,
				SPIRAL      : sf.lang.SID_SPIRAL,
				CHECKER     : sf.lang.SID_CHECKER,
				LINEAR      : sf.lang.SID_LINEAR,
				STAIRS      : sf.lang.SID_STAIRS,
				WIPE        : sf.lang.SID_WIPE,
				RANDOM      : sf.lang.SID_RANDOM,
				NONE        : sf.lang.SID_NONE
			}
			oStatusText = {
				'play': sf.lang.SID_PLAY,
				'pause': sf.lang.SID_PAUSE
			};
			oSpeedText = {
				'slow': sf.lang.SID_SLOW,
				'normal': sf.lang.SID_NORMAL,
				'fast': sf.lang.SID_FAST
			};
		}

		// utilities
		function isValidItem (item) {
			var bValidate = false;
			if (item.constructor != Array) {
				item = [item];
			}
			var len = item.length;
			if (len > 0) {
				bValidate = true;
				for (var i=0; i < len; i++) {
					if (typeof item[i] != 'object' || item[i].url === undefined) {
						bValidate = false;
					}
				}
			}
			alert('[ImageViewer][SlideShow] isValidItem() returns ' + bValidate);
			return bValidate;
		}
		// network error counter while preparing
		function ErrorCounter (MAX) {
			var count = 0;
			this.inc = function () {
				var bRet = false;
				if (++count >= MAX) {
					bRet = true;
				}
				alert('[ImageViewer][SlideShow] ErrorCounter.inc() count -> ' + count + ', MAX : ' + MAX + ', returns ' + bRet);
				return bRet;
			}
			this.init = function () {
				count = 0;
				alert('[ImageViewer][SlideShow] ErrorCounter.init() count -> ' + count + ', MAX : ' + MAX);
			}
			this.get = function () {
				alert('[ImageViewer][SlideShow] ErrorCounter.get() returns ' + count);
				return count;
			}
		}

		////////////////////////
		///  SlideShow View  ///
		////////////////////////

		function View (containerID) {

			var self = this;
			var hBarHideTimer = null;
			var HIDE_TIME = 5000;	// 5 sec

			// SlideShow container div
			$('<div id="'+SLIDESHOW_CONTAINER_DIV_ID+'" style="z-index:'+SLIDESHOW_CONTAINER_DIV_ZINDEX+';"></div>').appendTo($('#'+containerID));

			$('<div id="sf-service-slideshow" class="sf-service-slideshow"></div>').html([
				'<div id="sf-service-slideshow-infobar">',
					'<div id="sf-service-slideshow-infobar-status"></div>',
					'<div id="sf-service-slideshow-infobar-speed"></div>',
					'<div id="sf-service-slideshow-infobar-filename"></div>',
					'<div id="sf-service-slideshow-infobar-resolution"></div>',
					'<div id="sf-service-slideshow-infobar-date"></div>',
					'<div id="sf-service-slideshow-infobar-index"></div>',
				'</div>',
				'<div id="sf-service-slideshow-helpbar"></div>',
				'<div id="sf-service-slideshow-neterr">',
					'<div id="sf-service-slideshow-neterr-icon"></div>',
					'<div id="sf-service-slideshow-neterr-text-0">'+sf.lang.SID_ALERT_NETWORKERROR_MSG+'</div>',
					'<div id="sf-service-slideshow-neterr-text-1"></div>',
				'</div>',
				'<div id="sf-service-slideshow-notsup">',
					'<div id="sf-service-slideshow-notsup-icon"></div>',
					'<div id="sf-service-slideshow-notsup-text">'+sf.lang.SID_NOT_SUPPORT_FORMAT+'</div>',
				'</div>',
				'<div id="sf-service-slideshow-loading"></div>',
				'<div id="sf-service-slideshow-tool"></div>'
			].join('')).appendTo($('#'+SLIDESHOW_CONTAINER_DIV_ID));
//			.hide()	// Tool popup 위치 계산 오류 발생

			$('#sf-service-slideshow-helpbar').sfKeyHelp({
				'return': sf.lang.SID_RETURN
			});
//			$('#sf-service-slideshow-loading').sfLoading();

			this.init = function(){
				alert('[ImageViewer][SlideShow][view] init()');
            }
			this.show = function (option) {
				alert('[ImageViewer][SlideShow][view] show('+(option?option:'')+')');

				if (typeof option == 'undefined') {
					$('#sf-service-slideshow').show();
					return;
				}

				switch (option.toUpperCase()) {
					case 'INFOBAR':
						$('#sf-service-slideshow-infobar').show();
						break;
					case 'HELPBAR':
						$('#sf-service-slideshow-helpbar').sfKeyHelp('show');
						break;
					case 'LOADING':
						$('#sf-service-slideshow-loading').sfLoading('show');
						break;
					case 'NETWORK_ERROR':
						$('#sf-service-slideshow-neterr').show();
						break;
					case 'NOT_SUPPORT':
						$('#sf-service-slideshow-notsup').show();
						break;
					default :
						break;
				}
			}
			this.hide = function (option) {
				alert('[ImageViewer][SlideShow][view] hide('+(option?option:'')+')');

				if (typeof option == 'undefined') {
					$('#sf-service-slideshow').hide();
					return;
				}
				switch (option.toUpperCase()) {
					case 'INFOBAR':
						$('#sf-service-slideshow-infobar').hide();
						break;
					case 'HELPBAR':
						$('#sf-service-slideshow-helpbar').sfKeyHelp('hide');
						break;
					case 'LOADING':
						$('#sf-service-slideshow-loading').sfLoading('hide');
						break;
					case 'NETWORK_ERROR':
						$('#sf-service-slideshow-neterr').hide();
						break;
					case 'NOT_SUPPORT':
						$('#sf-service-slideshow-notsup').hide();
						break;
					default :
						break;
				}
			}
			this.showBars = function () {
				alert('[ImageViewer][SlideShow][view] showBars()');
				this.show('infobar');
				this.show('helpbar');

				self.setHideTimer();
			}
			this.hideBars = function () {
				alert('[ImageViewer][SlideShow][view] hideBars()');
				this.hide('infobar');
				this.hide('helpbar');

				self.clearHideTimer();
			}
			this.toggleBars = function () {
				alert('[ImageViewer][SlideShow][view] toggleBars()');
				hBarHideTimer ? this.hideBars() : this.showBars();
			}
			this.setHideTimer = function(){
                alert('[ImageViewer][SlideShow][view] setHideTimer()');
                self.clearHideTimer();
                hBarHideTimer = setTimeout(function(){
                    self.hideBars();
                    hBarHideTimer = null;
                }, HIDE_TIME);
            }
			this.clearHideTimer = function () {
				alert('[ImageViewer][SlideShow][view] clearHideTimer()');
				if (hBarHideTimer) {
					clearTimeout(hBarHideTimer);
					hBarHideTimer = null;
				}
			}
			this.refresh = function (option, value) {
				alert('[ImageViewer][SlideShow][view] refresh('+(option?option:'')+','+(value?value:'')+')');
				switch (option.toUpperCase()) {
					case 'INFOBAR':
						$.each (value, function(key, value){
							switch (key.toUpperCase()) {
								case 'STATE':
									var statusDiv = $('#sf-service-slideshow-infobar-status');
									switch (value.toUpperCase()) {
										case 'PLAY':
											statusDiv
												.removeClass(pauseClass)
												.addClass(playClass);
											break;
										case 'PAUSE':
											statusDiv
												.removeClass(playClass)
												.addClass(pauseClass);
											break;
										default:
											break;
									}
									break;
								case 'SPEED':
									$('#sf-service-slideshow-infobar-speed').html(value);
									break;
								case 'FILENAME':
									$('#sf-service-slideshow-infobar-filename').html(value);
									break;
								case 'RESOLUTION':
									$('#sf-service-slideshow-infobar-resolution').html(value);
									break;
								case 'DATE':
									$('#sf-service-slideshow-infobar-date').html(value);
									break;
								case 'INDEX':
									$('#sf-service-slideshow-infobar-index').html(value);
									break;
							}
						});
						break;
					case 'HELPBAR':
						$('#sf-service-slideshow-helpbar').sfKeyHelp(value);
						break;
					default :
						break;
				}
			}
			var playClass = 'sf-service-slideshow-infobar-status-play';
			var pauseClass = 'sf-service-slideshow-infobar-status-pause';
		}
	}
})(sf);
/*
 * Title: Service API (SmartRemote)
 * Apps Framework Service API
 *
 * Version added:
 * 1.1.0
 */

/*
   Class : sf.service.Remote
   Apps Framework Class for Interactive Remote
*/

(function(sf) {
    //Interactive Plugin Variable
	var _g_tigger = {
		PL_APPCOMMON_MESSAGE_XML_INPUT : 46,
		pluginAppCommon : null
	};

	/*
	 *	Namespace: sf.service.SmartRemote
	 *		SmartRemote
	 */
	sf.service.SmartRemote = {

		/*
		 * Function: init
		 * 	Initialization for Interactive Remote
		 *
		 * Parameters:
		 * 	o callback - (Function) callback function to be called when the message from SmartRemote received.
		 *
		 * Returns:
		 * 	nothing
		 */
		//Initialization for Interactive Remote
		init: function (callback) {
			_g_tigger.pluginAppCommon = document.getElementById('pluginObjectAppCommon');
			_g_tigger.pluginAppCommon.OnMessage = callback;
			_g_tigger.pluginAppCommon.SubscribeEvent(_g_tigger.PL_APPCOMMON_MESSAGE_XML_INPUT);
		},

		/*
		 * Function: destroy
		 * 	Destroying for Interactive Remote
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		destroy: function () {
			_g_tigger.pluginAppCommon.UnsubscribeEvent(_g_tigger.PL_APPCOMMON_MESSAGE_XML_INPUT);
		},

		/*
		 * Function: parse
		 * 	Parsing received response from Smart Device
		 *
		 * Parameters:
		 * 	o str	- (String) string from smart remote to parse
		 *
		 * Returns:
		 * 	(Object) xml document object
		 */
		parse: function (str) {
			var rlt = str.split("/");
			var xmlData = rlt[1];
			for (i = 2; i < rlt.length; i++) {
				xmlData = xmlData + "/" + rlt[i];
			}
			if (window.DOMParser) {
				parser = new DOMParser();
				xmlDoc = parser.parseFromString(xmlData, "text/xml");
			} else // Internet Explorer
			{
				xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = "false";
				xmlDoc.loadXML(xmlData);
			}
			return xmlDoc;
		},

		/*
		 * Function: sendSourceItem
		 * 	Sending SourceItem to Smart Device
		 *
		 * Parameters:
		 * 	o items		- (Array<String>|String) items to be sent
		 * 	o menuID	- (String) Current source item’s Menu <identifier> value.
		 *
		 * Returns:
		 * 	nothing
		 */
		sendSourceItem: function (items, menuID) {
			var xmlBufferData = "";
			xmlBufferData = makeSourceHeader(xmlBufferData, menuID);
			if (items.constructor == Array) {
				for ( var i = 0; i < items.length; i++) {
					xmlBufferData = makeSourceItem(xmlBufferData,
							items[i].type, items[i].typeDefault,
							items[i].identifier, items[i].title,
							items[i].description, items[i].image,
							items[i].onClick, items[i].onTrue,
							items[i].onFalse, items[i].multiTitles,
							items[i].multiURLs, items[i].typeVoice,
							items[i].voiceSelect, items[i].typeItems,
							items[i].typeMode, items[i].titlelinebreak,
							items[i].desclinebreak, items[i].multiIcons,
							items[i].multiTitlesHead, items[i].multiURLsHead,
							items[i].multiSizeHead, items[i].multiTitlesRow,
							items[i].multiURLsRow, items[i].multiSizeRow,
							items[i].multiSelectFlag,
							items[i].selectType,
							items[i].selectflag);
				}
			} else {
					xmlBufferData = makeSourceItem(xmlBufferData, items.type,
							items.typeDefault, items.identifier,
							items.title, items.description,
							items.image, items.onClick,
							items.onTrue, items.onFalse,
							items.multiTitles, items.multiURLs,
							items.typeVoice, items.voiceSelect,
							items.typeItems, items.typeMode,
							items.titlelinebreak, items.desclinebreak,
							items.multiIcons, items.multiTitlesHead,
							items.multiURLsHead, items.multiSizeHead,
							items.multiTitlesRow, items.multiURLsRow,
							items.multiSizeRow, items.multiSelectFlag,
							items.selectType,items.selectflag);
			}
			xmlBufferData = makeSourceFooter(xmlBufferData);
			_g_tigger.pluginAppCommon.SendEvent_XML_Sync(xmlBufferData);
		},

		/*
		 * Function: sendMenuItem
		 * 	Sending MenuIterm to Smart Device
		 *
		 * Parameters:
		 * 	o items	- (Array<String>|String) items to be sent
		 *
		 * Returns:
		 * 	nothing
		 */
		sendMenuItem: function (items) {
			var xmlBufferData = "";
			xmlBufferData = makeMenuHeader(xmlBufferData);
			if (items.constructor == Array) {
				for ( var i = 0; i < items.length; i++) {
					xmlBufferData = makeMenuItem(xmlBufferData,
							items[i].identifier, items[i].type,
							items[i].title, items[i].name,
							items[i].icon, items[i].onClick,
							items[i].onEvent, items[i].onHistory,
							items[i].multiTitles, items[i].multiURLs,
							items[i].hiddenTitles, items[i].hiddenURLs);
				}
			} else {
					xmlBufferData = makeMenuItem(xmlBufferData,
							items.identifier, items.type,
							items.title, items.name, items.icon,
							items.onClick, items.onEvent,
							items.onHistory, items.multiTitles,
							items.multiURLs, items.hiddenTitles,
							items.hiddenURLs);
			}
			xmlBufferData = makeMenuFooter(xmlBufferData);
			_g_tigger.pluginAppCommon.SendEvent_XML_Sync(xmlBufferData);

		},

		/*
		 * Function: showPopup
		 * 	Creating POPUP Item to Smart Device
		 *
		 * Parameters:
		 * 	o options	- (Object) options for showing popup.
		 * 		title	- (String) Title of the popup
		 * 		descr	- (String) Description of the popup
		 * 		buttonNames - (Array<String>) Button texts for the popup.
		 * 		buttonURLs	- (Array<String>) Button Action URLs
		 *
		 * Returns:
		 * 	nothing
		 */
		//Creating POPUP Item to Smart Device
		showPopup: function(options) {
			var xmlBufferData = "";
			xmlBufferData = makePopupMessageHeader(xmlBufferData);
			xmlBufferData = makePopupItem(xmlBufferData, options.title,
					options.descr, options.buttonNames,
					options.buttonURLs);
			xmlBufferData = makePopupFooter(xmlBufferData);
			_g_tigger.pluginAppCommon.SendEvent_XML_Sync(xmlBufferData);
		},

		/*
		 * Function: hidePopup
		 * 	Hide POPUP from Smart Device
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		hidePopup: function () {
			var xmlBufferData = "";
			xmlBufferData = makePopupMessageHeader(xmlBufferData);
			xmlBufferData = makePopupOffItem(xmlBufferData);
			xmlBufferData = makePopupFooter(xmlBufferData);
			_g_tigger.pluginAppCommon.SendEvent_XML_Sync(xmlBufferData);
		},

		/*
		 * Function: showLoading
		 * 	Loading an image on a Smart Device
		 *
		 * Parameters:
		 * 	o options	- (Object) options for loading
		 * 		title	- (String) title for loading
		 */
		showLoading: function (options) {
			xmlBufferData = makeLoadingHeader(xmlBufferData);
			xmlBufferData = makeLoadingItem(xmlBufferData, options.title);
			xmlBufferData = makeLoadingFooter(xmlBufferData);
			_g_tigger.pluginAppCommon.SendEvent_XML_Sync(xmlBufferData);
		},

		/*
		 * Function: hideLoading
		 * 	Loading an image off on Smart Device
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 */
		hideLoading: function () {
			var xmlBufferData = "";
			xmlBufferData = makeLoadingHeader(xmlBufferData);
			xmlBufferData = makeLoadingOffItem(xmlBufferData);
			xmlBufferData = makeLoadingFooter(xmlBufferData);
			_g_tigger.pluginAppCommon.SendEvent_XML_Sync(xmlBufferData);
		}
	}

	/*
	 *
	Function: convertXMLSpecialChar
	 Convert some characters to special character code

	Parameters:
	 o str - string that need to replace some characters to special character code

	Returns:
	 string - string that replaced special character code

	Description:
	 This is function to convert some characters to special character code

	Example 1:
	 This example convert some characters to special character code
	 (start code)
	  sf.service.convertXMLSpecialChar(pStr);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function convertXMLSpecialChar(str) {
		if (str != null && str != "") {
			str = str.replace(/&/gi, "&amp;");
			str = str.replace(/</gi, "&lt;");
			str = str.replace(/>/gi, "&gt;");
			str = str.replace(/\n/gi, "\r\n");
			str = str.replace(/\f/gi, "");
			str = str.replace(/\r/gi, "");
			str = str.replace(/\t/gi, "");
			str = str.replace(/" "/gi, "");
			str = str.replace(//gi,"");
		}
		return str;
	}

	/*
	 *
	Function: makeSourceHeader
	 Create source item's header

	Parameters:
	 o XMLBuffer - Just initialized variable.
	 o menuID -  Current source item’s Menu <identifier> value

	Returns:
	 XMLBuffer - Created source item's header

	Description:
	 This is function to create source item's header

	Example 1:
	 This example create source item's header and return it
	 (start code)
	  sf.service.makeSourceHeader(xmlBufferData, menuID);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeSourceHeader(XMLBuffer, menuID) {
		XMLBuffer = "<source><menuID>" + menuID + "</menuID>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makeSourceItem
	 Create source items and attach to source item' header

	Parameters:
	 o XMLBuffer - XML data start with source item's header
	 o type - source item's type
	 o typeDefault - source item's typeDefault
	 o identifier - source item's identifier
	 o title - source item's title
	 o description - source item's description
	 o image - source item's image
	 o onClick - source item's onClick
	 o onTrue - source item's onTrue
	 o onFalse - source item's onFalse
	 o multiTitles - source item's multiTitles
	 o multiURLs - source item's multiURLs
	 o typeVoice - source item's typeVoice
	 o voiceSelect - source item's voiceSelect
	 o typeItems - source item's typeItems
	 o typeMode - source item's typeMode
	 o titleLineBreak - source item's titleLineBreak
	 o descLineBreak - source item's descLineBreak
	 o multiIcons - source item's multiIcons
	 o multiTitlesHead - source item's multiTitlesHead
	 o multiURLsHead - source item's multiURLsHead
	 o multiSizeHead - source item's multiSizeHead
	 o multiTitlesRow - source item's multiTitlesRow
	 o multiURLsRow - source item's multiURLsRow
	 o multiSizeRow - source item's multiSizeRow
	 o multiSelectFlag - source item's multiSelectFlag
	 o selectMode - source item's selectMode
	 o selectFlag - source item's selectFlag

	Returns:
	 XMLBuffer - XML data that attached source item

	Description:
	 This is function to create and attach source item

	Example 1:
	 This example create and attach source item and return it
	 (start code)
	  sf.service.makeSourceItem(xmlBufferData,
		itemObjArr[i].type, itemObjArr[i].typeDefault,
		itemObjArr[i].identifier, itemObjArr[i].title,
		itemObjArr[i].description, itemObjArr[i].image,
		itemObjArr[i].onClick, itemObjArr[i].onTrue,
		itemObjArr[i].onFalse, itemObjArr[i].multiTitles,
		itemObjArr[i].multiURLs, itemObjArr[i].typeVoice,
		itemObjArr[i].voiceSelect, itemObjArr[i].typeItems,
		itemObjArr[i].typeMode, itemObjArr[i].titlelinebreak,
		itemObjArr[i].desclinebreak, itemObjArr[i].multiIcons,
		itemObjArr[i].multiTitlesHead, itemObjArr[i].multiURLsHead,
		itemObjArr[i].multiSizeHead, itemObjArr[i].multiTitlesRow,
		itemObjArr[i].multiURLsRow, itemObjArr[i].multiSizeRow,
		itemObjArr[i].multiSelectFlag,
		itemObjArr[i].selectType,
		itemObjArr[i].selectflag);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeSourceItem(XMLBuffer, type, typeDefault, identifier, title,
			description, image, onClick, onTrue, onFalse, multiTitles,
			multiURLs, typeVoice, voiceSelect, typeItems, typeMode, titleLineBreak, descLineBreak,
			multiIcons, multiTitlesHead, multiURLsHead, multiSizeHead, multiTitlesRow, multiURLsRow, multiSizeRow, multiSelectFlag,
			selectMode,selectFlag) {
		title = convertXMLSpecialChar(title);
		description = convertXMLSpecialChar(description);
		image = convertXMLSpecialChar(image);
		onClick = convertXMLSpecialChar(onClick);
		onTrue = convertXMLSpecialChar(onTrue);
		onFalse = convertXMLSpecialChar(onFalse);
			if (type!= "table" && selectMode != null && selectMode  != ""){
			XMLBuffer = XMLBuffer +"<select>"+selectMode+"</select>";
		}
		XMLBuffer = XMLBuffer + "<sourceItem>";
		if (type != null && type != ""){
			if(typeDefault != null && typeDefault != ""){
				XMLBuffer = XMLBuffer + "<type default=\""+ typeDefault + "\">" + type + "</type>";
			}
            else if(type!="table" && selectFlag !=null && selectFlag !="")
                {
                    XMLBuffer = XMLBuffer + "<type selected=\""+selectFlag+"\">"+type+"</type>";
                }
			else if(typeVoice != null && typeVoice !=""){
				if(voiceSelect !=null && voiceSelect !=""){
					XMLBuffer = XMLBuffer + "<type voice=\""+ typeVoice + "\" voice_select=\""+ voiceSelect + "\">" + type + "</type>";
				}
				else{
					XMLBuffer = XMLBuffer + "<type voice=\""+ typeVoice + "\">" + type + "</type>";
				}
			}
			else if(typeItems != null && typeItems != ""){
				XMLBuffer = XMLBuffer + "<type items=\""+ typeItems + "\">" + type + "</type>";
			}
			else if(typeMode != null && typeMode != ""){
				XMLBuffer = XMLBuffer + "<type mode=\""+ typeMode + "\">" + type + "</type>";
			}
			else{
				XMLBuffer = XMLBuffer + "<type>" + type + "</type>";
			}
		}
		if (identifier != null && identifier != "")
			XMLBuffer = XMLBuffer + "<identifier>" + identifier
					+ "</identifier>";
		if (title != null && title != ""){
			if(titleLineBreak !=null && titleLineBreak !=""){
				XMLBuffer = XMLBuffer +"<title linebreak=\""+ titleLineBreak
						+ "\">" + title +"</title>";
			}
			else {
				XMLBuffer = XMLBuffer + "<title>" + title + "</title>";
			}
		}
		if (description != null && description != ""){
			if(descLineBreak !=null && descLineBreak !=""){
				XMLBuffer = XMLBuffer +"<description linebreak=\""+ descLineBreak + "\">" +description+"</description>";
			}
			else{
				XMLBuffer = XMLBuffer + "<description>" + description
					+ "</description>";
			}
		}
		if (image != null && image != "")
			XMLBuffer = XMLBuffer + "<image>" + image + "</image>";
		if (onClick != null && onClick != "")
			XMLBuffer = XMLBuffer + "<onClick>" + onClick + "</onClick>";
		if (onTrue != null && onTrue != "")
			XMLBuffer = XMLBuffer + "<onTrue>" + onTrue + "</onTrue>";
		if (onFalse != null && onFalse != "")
			XMLBuffer = XMLBuffer + "<onFalse>" + onFalse + "</onFalse>";
		if (multiTitles != null && multiTitles.length > 0) {
			XMLBuffer = XMLBuffer + "<multiTitles>";
			for (var i = 0; i < multiTitles.length; i++) {
				if (type == "combobox" && multiIcons && multiIcons[i] != undefined && multiIcons[i] != null) {
					XMLBuffer = XMLBuffer + "<title icon =\"" + multiIcons[i] + "\">" + multiTitles[i] + "</title>";
				}
				else {
					XMLBuffer = XMLBuffer + "<title>" + multiTitles[i] + "</title>";
				}
			}
			XMLBuffer = XMLBuffer + "</multiTitles>";
		}
		if (multiURLs != null && multiURLs.length > 0) {
			XMLBuffer = XMLBuffer + "<multiURLs>";
			for (var i = 0; i < multiURLs.length; i++) {
				XMLBuffer = XMLBuffer + "<url>" + multiURLs[i] + "</url>";
			}
			XMLBuffer = XMLBuffer + "</multiURLs>";
		}
		if(type == "table") {
			if(selectMode != null && selectMode  != "") {
				XMLBuffer = XMLBuffer +"<table select=\""+ selectMode + "\">";
			}
			else{
				XMLBuffer = XMLBuffer + "<table>";
			}
			if(multiTitlesHead != null && multiTitlesHead.length > 0){
				XMLBuffer = XMLBuffer + "<head>";
				for(var i = 0 ; i < multiTitlesHead.length ; i++){
					if (multiSizeHead && multiSizeHead[i] != undefined && multiSizeHead[i] != null) {
						XMLBuffer = XMLBuffer + "<col size=\"" + multiSizeHead[i] + "\">";
					}
					else{
						XMLBuffer = XMLBuffer + "<col>";
					}
					if (multiTitlesHead && multiTitlesHead[i] != undefined && multiTitlesHead[i] != null) {
                        multiTitlesHead[i]  = convertXMLSpecialChar(multiTitlesHead[i]);
						XMLBuffer = XMLBuffer + "<title>" + multiTitlesHead[i] + "</title>";
					}
					if (multiURLsHead && multiURLsHead[i] != undefined && multiURLsHead[i] != null) {
						XMLBuffer = XMLBuffer + "<url>" + multiURLsHead[i] + "</url>";
					}
					XMLBuffer = XMLBuffer + "</col>";
				}
				XMLBuffer = XMLBuffer + "</head>";
			}
			if(multiTitlesRow != null && multiTitlesRow.length >0){
				for (var i = 0; i < multiTitlesRow.length; i++) {
					XMLBuffer = XMLBuffer + "<row>";
					if(selectMode != null && selectMode  != ""){
						XMLBuffer = XMLBuffer + "<selected>";
						if (multiSelectFlag && multiSelectFlag[i] != undefined && multiSelectFlag[i] != null) {
							XMLBuffer = XMLBuffer + multiSelectFlag[i];
						}
						XMLBuffer = XMLBuffer + "</selected>";
					}
					for (var j = 0; j < multiTitlesRow[i].length; j++) {
						if (multiSizeRow && multiSizeRow[i] != undefined && multiSizeRow[i][j] != null) {
							XMLBuffer = XMLBuffer + "<col size=\"" + multiSizeRow[i][j] + "\">";
						}
						else{
							XMLBuffer = XMLBuffer + "<col>";
						}
						if (multiTitlesRow && multiTitlesRow[i] != undefined && multiTitlesRow[i][j] != null) {
                            multiTitlesRow[i][j]  = convertXMLSpecialChar(multiTitlesRow[i][j]);
							XMLBuffer = XMLBuffer + "<title>" + multiTitlesRow[i][j] + "</title>";
						}
						if (multiURLsRow && multiURLsRow[i] != undefined && multiURLsRow[i][j] != null) {
							XMLBuffer = XMLBuffer + "<url>" + multiURLsRow[i][j] + "</url>";
						}
						XMLBuffer = XMLBuffer + "</col>";
					}
					XMLBuffer = XMLBuffer + "</row>";
				}
			}
			XMLBuffer = XMLBuffer + "</table>";
		}
		XMLBuffer = XMLBuffer + "</sourceItem>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makeSourceFooter
	 Attach end tag to source item's XML

	Parameters:
	 o XMLBuffer - XML data without source end tag

	Returns:
	 XMLBuffer - whole source item's XML

	Description:
	 This is function to attach end tag to source item's XML and return whole source item's XML

	Example 1:
	 This example attach end tag to source item's XML and return whole source item's XML
	 (start code)
	  sf.service.makeSourceFooter(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeSourceFooter(XMLBuffer) {
		XMLBuffer = XMLBuffer + "</source>";

		return XMLBuffer;
	}

	/*
	 *
	Function: makeMenuHeader
	 Create menu item's header

	Parameters:
	 o XMLBuffer - Just initialized variable

	Returns:
	 XMLBuffer - Created menu item's header

	Description:
	 This is function to create menu item's header

	Example 1:
	 This example create menu item's header and return it
	 (start code)
	  sf.service.makeMenuHeader(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeMenuHeader(XMLBuffer) {
		XMLBuffer = "<menu>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makemenuItem
	 Create menu items and attach to menu item's header

	Parameters:
	 o XMLBuffer - XML data start with menu item's header
	 o identifier - menu item's identifier
	 o type - menu item's type
	 o title - menu item's title
	 o name - menu item's name
	 o icon - menu item's icon
	 o onClick - menu item's onClick
	 o onEvent - menu item's onEvent
	 o onHistory - menu item's onHistory
	 o multiTitles - menu item's multiTitles
	 o multiURLs - menu item's multiURLs
	 o hiddenTitles - menu item's hiddenTitles
	 o hiddenURLs - menu item's hiddenURLs

	Returns:
	 XMLBuffer - XML data that attached menu item

	Description:
	 This is function to create and attach menu item

	Example 1:
	 This example create and attach menu item and return it.
	 (start code)
	  sf.service.makemenuItem(xmlBufferData,
		itemObjArr.identifier, itemObjArr.type,
		itemObjArr.title, itemObjArr.name, itemObjArr.icon,
		itemObjArr.onClick, itemObjArr.onEvent,
		itemObjArr.onHistory, itemObjArr.multiTitles,
		itemObjArr.multiURLs, itemObjArr.hiddenTitles,
		itemObjArr.hiddenURLs);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeMenuItem(XMLBuffer, identifier, type, title, name, icon,
			onClick, onEvent, onHistory, multiTitles, multiURLs, hiddenTitles, hiddenURLs) {
		title = convertXMLSpecialChar(title);
		name = convertXMLSpecialChar(name);
		icon = convertXMLSpecialChar(icon);
		onClick = convertXMLSpecialChar(onClick);
		onEvent = convertXMLSpecialChar(onEvent);
		onHistory = convertXMLSpecialChar(onHistory);

		XMLBuffer = XMLBuffer + "<menuItem>";

		if (identifier != null && identifier != "")
			XMLBuffer = XMLBuffer + "<identifier>" + identifier
					+ "</identifier>";
		if (type != null && type != "")
			XMLBuffer = XMLBuffer + "<type>" + type + "</type>";
		if (title != null && title != "")
			XMLBuffer = XMLBuffer + "<title>" + title + "</title>";
		if (name != null && name != "")
			XMLBuffer = XMLBuffer + "<name>" + name + "</name>";
		if (icon != null && icon != "")
			XMLBuffer = XMLBuffer + "<icon>" + icon + "</icon>";
		if (onClick != null && onClick != "")
			XMLBuffer = XMLBuffer + "<onClick>" + onClick + "</onClick>";
		if (onEvent != null && onEvent != "")
			XMLBuffer = XMLBuffer + "<onEvent>" + onEvent + "</onEvent>";
		if (onHistory != null && onHistory != "")
			XMLBuffer = XMLBuffer + "<onHistory>" + onHistory + "</onHistory>";
		if (multiTitles != null && multiTitles.length > 0) {
			XMLBuffer = XMLBuffer + "<multiTitles>";
			for (i = 0; i < multiTitles.length; i++) {
				XMLBuffer = XMLBuffer + "<title>" + multiTitles[i] + "</title>";
			}
			XMLBuffer = XMLBuffer + "</multiTitles>";
		}
		if (multiURLs != null && multiURLs.length > 0) {
			XMLBuffer = XMLBuffer + "<multiURLs>";
			for (i = 0; i < multiURLs.length; i++) {
				XMLBuffer = XMLBuffer + "<url>" + multiURLs[i] + "</url>";
			}
			XMLBuffer = XMLBuffer + "</multiURLs>";
		}
		if(hiddenTitles != null && hiddenTitles.length > 0){
			XMLBuffer = XMLBuffer + "<hiddenTitles>";
			for(i=0;i<hiddenTitles.length;i++){
				XMLBuffer = XMLBuffer + "<title>" + hiddenTitles[i] + "</title>";
			}
			XMLBuffer = XMLBuffer + "</hiddenTitles>";
		}
		if(hiddenURLs != null && hiddenURLs.length > 0){
			XMLBuffer = XMLBuffer + "<hiddenURLs>";
			for(i=0;i<hiddenURLs.length;i++){
				XMLBuffer = XMLBuffer + "<url>" + hiddenURLs[i] + "</url>";
			}
			XMLBuffer = XMLBuffer + "</hiddenURLs>";
		}
		XMLBuffer = XMLBuffer + "</menuItem>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makeMenuFooter
	 Attach end tag to menu item's XML

	Parameters:
	 o XMLBuffer - XML data without menu end tag

	Returns:
	 XMLBuffer - whole menu item's XML

	Description:
	 This is function to attach end tag to menu item's XML and return whole menu item's XML

	Example 1:
	 This example attach end tag to menu item's XML and return whole menu item's XML
	 (start code)
	  sf.service.makeMenuFooter(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeMenuFooter(XMLBuffer) {
		XMLBuffer = XMLBuffer + "</menu>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makePopupMessageHeader
	 Create popup message's header

	Parameters:
	 o XMLBuffer - Just initialized variable.

	Returns:
	 XMLBuffer - Created popup message's header

	Description:
	 This is function to popup message's header

	Example 1:
	 This example create popup message's header and return it
	 (start code)
	  sf.service.makePopupMessageHeader(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makePopupMessageHeader(XMLBuffer) {
		XMLBuffer = "<message>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makePopupItem
	 Create popup items and attach to popup item's header

	Parameters:
	 o XMLBuffer - XML data start with popup item's header
	 o title - popup item's title
	 o descr - popup item's descr
	 o buttonNames - popup item's buttonNames
	 o buttonURLs - popup item's buttonNames

	Returns:
	 XMLBuffer - XML data that attached popup item

	Description:
	 This is function to create and attach popup item

	Example 1:
	 This example create and attach popup item and return it.
	 (start code)
	  sf.service.makePopupItem(xmlBufferData, itemObjArr.title,
		itemObjArr.descr, itemObjArr.buttonNames,
		itemObjArr.buttonURLs);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makePopupItem(XMLBuffer, title, descr, buttonNames, buttonURLs) {

		title = convertXMLSpecialChar(title);
		descr = convertXMLSpecialChar(descr);

		if (title != null && title != "")
			XMLBuffer = XMLBuffer + "<title>" + title + "</title>";
		if (descr != null && descr != "")
			XMLBuffer = XMLBuffer + "<text>" + descr + "</text>";
		if (buttonNames != null && buttonNames.length > 0) {
			XMLBuffer = XMLBuffer + "<buttonNames>";
			for (i = 0; i < buttonNames.length; i++) {
				XMLBuffer = XMLBuffer + "<name>" + buttonNames[i] + "</name>";
			}
			XMLBuffer = XMLBuffer + "</buttonNames>";
		}
		if (buttonURLs != null && buttonURLs.length > 0) {
			XMLBuffer = XMLBuffer + "<buttonURLs>";
			for (i = 0; i < buttonURLs.length; i++) {
				XMLBuffer = XMLBuffer + "<onClick>" + buttonURLs[i]
						+ "</onClick>";
			}
			XMLBuffer = XMLBuffer + "</buttonURLs>";
		}
		return XMLBuffer;
	}

	/*
	 *
	Function: makePopupFooter
	 Attach end tag to popup item's XML

	Parameters:
	 o XMLBuffer - XML data without popup end tag

	Returns:
	 XMLBuffer - whole popup item's XML

	Description:
	 This is function to attach end tag to popup item's XML and return whole popup item's XML

	Example 1:
	 This example attach end tag to popup item's XML and return whole popup item's XML
	 (start code)
	  sf.service.makePopupFooter(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makePopupFooter(XMLBuffer) {
		XMLBuffer = XMLBuffer + "</message>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makePopupOffItem
	 Create popupOff items and attach to popupOff item's header

	Parameters:
	 o XMLBuffer - XML data start with popupOff item's header

	Returns:
	 XMLBuffer - XML data that attached popupOff item

	Description:
	 This is function to create and attach popupOff item

	Example 1:
	 This example create and attach popupOff item and return it.
	 (start code)
	  sf.service.makePopupOffItem(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makePopupOffItem(XMLBuffer) {
		XMLBuffer = XMLBuffer
		XMLBuffer = XMLBuffer + "<action>"
		XMLBuffer = XMLBuffer + "close";
		XMLBuffer = XMLBuffer + "</action>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makeLoadingHeader
	 Create loading's header

	Parameters:
	 o XMLBuffer - Just initialized variable.

	Returns:
	 XMLBuffer - Created loading's header

	Description:
	 This is function to loading's header

	Example 1:
	 This example create loading's header and return it
	 (start code)
	  sf.service.makeLoadingHeader(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeLoadingHeader(XMLBuffer) {
		XMLBuffer = XMLBuffer + "<loading>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makeLoadingItem
	 Create loading items and attach to loading item's header

	Parameters:
	 o XMLBuffer - XML data start with loading item's header
	 o title - loading item's title

	Returns:
	 XMLBuffer - XML data that attached loading item

	Description:
	 This is function to create and attach loading item

	Example 1:
	 This example create and attach loading item and return it.
	 (start code)
	  sf.service.makeLoadingItem(xmlBufferData,itemObjArr.title);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeLoadingItem(XMLBuffer, title) {
		title = convertXMLSpecialChar(title);
		if (title != null && title != ""){
			XMLBuffer = XMLBuffer + "<title>" + title + "</title>";
			XMLBuffer = XMLBuffer + "<type>" + "open" + "</type>";
		}
		else{
			XMLBuffer = XMLBuffer + "<type>" + "openbg" + "</type>";
		}
		return XMLBuffer;
	}

	/*
	 *
	Function: makeLoadingFooter
	 Attach end tag to loading item's XML

	Parameters:
	 o XMLBuffer - XML data without loading end tag

	Returns:
	 XMLBuffer - whole loading item's XML

	Description:
	 This is function to attach end tag to loading item's XML and return whole loading item's XML

	Example 1:
	 This example attach end tag to loading item's XML and return whole loading item's XML
	 (start code)
	  sf.service.makeLoadingFooter(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeLoadingFooter(XMLBuffer) {
		XMLBuffer = XMLBuffer + "</loading>";
		return XMLBuffer;
	}

	/*
	 *
	Function: makeLoadingOffItem
	 Create loadingOff items and attach to loadingOff item's header

	Parameters:
	 o XMLBuffer - XML data start with loadingOff item's header

	Returns:
	 XMLBuffer - XML data that attached loadingOff item

	Description:
	 This is function to create and attach loadingOff item

	Example 1:
	 This example create and attach loadingOff item and return it.
	 (start code)
	  sf.service.makeLoadingOffItem(xmlBufferData);
	 (end)

	Available From:
	 1.1.0

	History:
	*/
	function makeLoadingOffItem(XMLBuffer) {
		XMLBuffer = XMLBuffer + "<type>"
		XMLBuffer = XMLBuffer + "close";
		XMLBuffer = XMLBuffer + "</type>";
		return XMLBuffer;
	}
})(sf);
/*
 *	Title: Service API (USB)
 *	Apps Framework USB Service API
 *
 *	Version added:
 *	1.1.0
 */
(function (sf) {
	/*
	 *	Namespace: sf.service.USB
	 *		Browse the files in the USB memory. And select some files from it.
	 */
	sf.service.USB = {
		/*
		 * Function: show
		 *	show the usb browser window
		 *
		 * Parameters:
		 *	o (Function)callback - callback function to be called when the USB popup is closed.
		 *	o (String)fileType - Filter. This would be applied with the file's extention.
		 *		'all' - All extention. Not filtered.
		 *		'image' - Image files (png, jpg, bmp, gif)
		 *		'text' - Text files (txt, gul)
		 *		'video' - Video files (avi, wma, mp4)
		 *		'music' - Music files (mp3)
		 *
		 * Returns:
		 * 	Array - Each items will be selected file's path. This array will be passed to callback.
		 *
		 * Available From:
		 *	1.1.0
		 */

		handleUSBEvent : function (message) { //usb plug
			refreshUsb();
		},
		show: function(options) {

			$.each (options, function (key, value) {
				if (typeof env[key] == 'function') {
					if (typeof value == 'string') {
						value = value.toLowerCase();
					}
					env[key](value); //callback function and fileType(image/video/all...)
				}
			});
			if(!bInit) {
				initialize();
			}

			readusb(); //초기 USB 정보 리드 (usb 유/무 , usb plugin )

			if (nSize == -1) { //삽입된 USB가 없을 경우
				view.selectedFile();
				view.initTextView();
				$('.sf-service-usb-focus').addClass('sf-service-usb-focus-position0');
				$('#sf-service-usb-item1-icon').attr('class','sf-service-usb-item-icon-usbNone');
				$('#sf-service-usb-item1-icon').show();
				$('#sf-service-usb-item1-label').html(''+sf.lang.SID_USB_IS_NOT_CONNECTED);
				$('#sf-service-usb-size').html('');
				$('#sf-service-usb-vendorName').html('');
				$("#sf-service-usb-scroll").hide();
				$("#sf-service-usb-text-arrowUp").hide();
				$("#sf-service-usb-text-arrowDown").hide();
			} else {
				alert("test..........................................................");
				alert("test1.........................................................."+aFilepath);
				alert("test2.........................................................."+nItemNum);
				readUsbList(aFilepath);
				view.setTextView(nItemNum);
				Refresh();
			}
			$('#'+ANCHOR_ID).focus();
			view.show();
		}
	}
	var bInit = false;
	var sPattern = null;
	var refunction = null;
	var aUserArr = null;
	var aImaArr = new Array('png','jpg','bmp','gif');
	var	aTextArr = new Array('txt','gul');
	var	aVideoArr = new Array('avi','wma','mp4','wmv');
	var	aMusicArr = new Array('mp3');
	var USB_UNPLUGED = 1;
	var USB_PLUGED = 0;
	var bSetUsb = true;
	var MAX_ITEM_NUM = 5;
	var nStateUsbNum = 0;
	var nScrollHeight = 0;
	var nScrollBody = 0;
	var nScrollMove = 0;
	var bSizeAvailable = false;

	var env = {
		callback : function (value) {
			if (typeof value == 'function')
				refunction = value;
			else
				alert ('[AF usb] value must be a Function.');
		},
		fileType: function(value) {
			if (typeof value == 'string') {
				if (value == 'image') {
					sPattern = 'image';
				} else if (value == 'text') {
					sPattern = 'text';
				} else if (value == 'video') {
					sPattern = 'video';
				} else if (value == 'music') {
					sPattern = 'music';
				} else if(value =='all') {
					sPattern = 'all';
				} else {
					alert('[AF usb] Not Supported type')
				}
			} else if(typeof value =='object') {
				sPattern = 'other';
				aUserArr=value;
			} else {
				alert('[AF usb] Not Supported string/Array type');
			}
		}
	}
	function initialize() {
		mountPath = null;
		dirPath = null;
		defaultDir = null;
		aOnReturn = new Array();
		fs = new FileSystem();
		object = null // usb_object
		aObjectList = new Array(); //aObjectList
		aUsbNum = new Array();
		aFilepath = new Array(); //attr
		view = new View();

		np = sf.core.plugin('NNavi');
		if(np) {
			var firmware = np.GetFirmware();
			if('T-INFOLINK2011-9999' < firmware) {
				bSizeAvailable = true;
			}
		}


		$('<a href="javascript:void(0);" id="'+ANCHOR_ID+'"></a>').appendTo('body');
		$('#'+ANCHOR_ID).keydown( function() {
			sf.service.USB.handleKeydown();
		});
		bInit = true;

		bSetUsb =true;
	}

	function readusb() {
		sp = sf.core.plugin('Storage');
		sp.OnMessage = sfUsbEvent;
		sp.SubscribeEvent(USB_UNPLUGED);
		sp.SubscribeEvent(USB_PLUGED);
		nSize = sp.GetUSBListSize();
		var nlistNum = 0
		alert('[AF usb] GetUSBListSize :'+nSize);

		for (var i=0; i<nSize; i++) {
			var defaultID = sp.GetUSBDeviceID(i);
			var partitionNum=sp.GetUSBPartitionNum(defaultID); //USB partition
			for(var j=0; j<partitionNum; j++) {
				var nPartition = j+1;
				aUsbNum[nlistNum] = {};
				if(partitionNum>=2){
					aUsbNum[nlistNum].vendorName = sp.GetUSBVendorName(defaultID)+'&nbsp;&nbsp;('+nPartition+')';
				}
				else {
					aUsbNum[nlistNum].vendorName = sp.GetUSBVendorName(defaultID);
				}
				aUsbNum[nlistNum].mountPath = sp.GetUSBMountPath(defaultID,j);
				aUsbNum[nlistNum].availSize = sp.GetUSBAvailSize(defaultID,j);
				aUsbNum[nlistNum].totalSize = sp.GetUSBTotalSize(defaultID,j);
				nlistNum++;
			}
		}

	}

	function refreshUsb() {
		readusb();
		if(nSize == -1) {
			nStateUsbNum = nSize;
			for(var i=1; i<=MAX_ITEM_NUM; i++) {
				$('#sf-service-usb-item' + i + '-label').html('');
				$('#sf-service-usb-item' + i + '-icon').hide();

			}
			$('#sf-service-usb-item1-label').html(''+sf.lang.SID_USB_IS_NOT_CONNECTED);
			$('#sf-service-usb-item1-icon').attr('class','sf-service-usb-item-icon-usbNone');
			$('#sf-service-usb-item1-icon').show();
			$('#sf-service-usb-label').html('');
			$('#sf-service-usb-size').html('');
			$('#sf-service-usb-vendorName').html('');
		} else if(nStateUsbNum != nSize) {
			if(nSize == -1) {
				for(var i=1; i<=MAX_ITEM_NUM; i++) {
					$('#sf-service-usb-item' + i + '-label').html('');
					$('#sf-service-usb-item' + i + '-icon').hide();
				}
				$('#sf-service-usb-item1-label').html(''+sf.lang.SID_USB_IS_NOT_CONNECTED);
				$('#sf-service-usb-item1-icon').attr('class','sf-service-usb-item-icon-usbNone');
				$('#sf-service-usb-item1-icon').show();
				$('#sf-service-usb-label').html('');
				$('#sf-service-usb-size').html('');
				$('#sf-service-usb-vendorName').html('');
			} else {
				for(var i=1; i<=MAX_ITEM_NUM; i++) {
					$('#sf-service-usb-item' + i + '-label').html('');
					$('#sf-service-usb-item' + i + '-icon').hide();
				}
				nStateUsbNum = nSize;
				aUsbNum = new Array();
				readusb();
				Refresh();
				view.setTextView(nListPageNum);
				var nTotalSize = aUsbNum[nItemNum-1].totalSize/1048576;
				var nAvailSize = aUsbNum[nItemNum-1].availSize/1048576;
				var nUsingSize = nTotalSize-nAvailSize;
				alert("bSizeAvailable.........................................."+bSizeAvailable);
				if(bSizeAvailable){
					$('#sf-service-usb-size').html(''+nUsingSize.toFixed(2)+'GB'+' / ' + nTotalSize.toFixed(2)+'GB');
				}
				else{
					$('#sf-service-usb-size').html('');
				}
			}
		}
	}
	function readUsbList(Path) { //USB 선택후 기본 주소를 통해 내부 list 확인
		nScrollBodyTop = 0;
		if(bSetUsb == true) { //scroll이 표현될 페이지 수 결정
			nScrollPage = Math.ceil(aUsbNum.length/MAX_ITEM_NUM);
			if(nScrollPage <= 1) {
				$("#sf-service-usb-scroll").hide();
				$("#sf-service-usb-text-arrowUp").hide();
				$("#sf-service-usb-text-arrowDown").hide();
			} else {
				$("#sf-service-usb-scroll").show();
				setScroll(nScrollPage);
			}
		}
		else {
			alert('[AF usb] Path:'+Path);
			var fullPath = Path.join('/');
			object = fs.readDir(fullPath);
			var nNumFolder=0;
			var nNumFile=0;

			for (var i=0; i<object.length; i++) { //directory가 우선 보여야 하므로 먼저 리드한다.
				if (object[i].isDir) {
					var list = new setFileList(object[i], 0);
					aObjectList[nNumFolder] = list; //aObjectList : 경로에 따른 usb 내부 file 및 directory 등을  list로 보관
					alert("aObjectList[nNumFolder]........................."+aObjectList[nNumFolder].name);
					nNumFolder++;
				}
			}
			nNumFile=nNumFolder; //folder 뒤로 file을 저장

			for (var i=0; i<object.length; i++) {
				if (!object[i].isDir) { //초기에 선언된 fileType(image,video,all..) 에 따라 패턴 검사
					var list =new setFileList(object[i],0);
					var sExtName = list.fileInfo.name.match(/\.(\w+)$/i);
					var nRlt=0;
					if(sExtName != null) {
						sExtName = sExtName[1].toLowerCase();
					}
					if(sPattern == 'image') {
						nRlt = $.inArray(sExtName, aImaArr);
					} else if(sPattern == 'text') {
						nRlt = $.inArray(sExtName, aTextArr);
					} else if(sPattern == 'video') {
						nRlt = $.inArray(sExtName, aVideoArr);
					} else if(sPattern == 'music') {
						nRlt = $.inArray(sExtName, aMusicArr);
					} else if(sPattern == 'other') {
						nRlt = $.inArray(sExtName, aUserArr);
					} else if(sPattern == 'all') {
						nRlt=1;
					}
					if(nRlt >= 0 ) {
						aObjectList[nNumFile] = list;
						alert("aObjectList[nNumFile]........................."+aObjectList[nNumFile].name);
						nNumFile++;
					}
				}
			}
			nScrollPage = Math.ceil((aObjectList.length-1)/MAX_ITEM_NUM);
			if(nScrollPage <= 1) {
				$("#sf-service-usb-scroll").hide();
				$("#sf-service-usb-text-arrowUp").hide();
				$("#sf-service-usb-text-arrowDown").hide();
			} else {
				$("#sf-service-usb-scroll").show();
				setScroll(nScrollPage);
			}

		}
	}

	function setScroll(nScrollPageNum) {
		nScrollHeight = parseFloat($('#sf-service-usb-scroll').css('height'),10);
		var nTopHeight = parseFloat($('#sf-service-usb-scroll-body-top').css('height'),10);
		var nBottomheight = parseFloat($('#sf-service-usb-scroll-body-bottom').css('height'),10);
		nScrollBody = nScrollHeight / nScrollPageNum;
		var nMiddleheight = nScrollBody-(nTopHeight+nBottomheight);
		if(nMiddleheight <= 14) {
			nMiddleheight = 14;
		}
		$('#sf-service-usb-scroll-body-middle').css({
			'height' : nMiddleheight
		});

		nScrollMove = (nScrollHeight-(nTopHeight+nBottomheight+nMiddleheight)) / (nScrollPageNum-1);
	}

	function setFileList(object,check) { //파일 정보 및 checkinfo(선택여부) 확인
		this.fileInfo=object;
		this.checkinfo=check;
	}

	var nItemNum = 1;
	var nListPageNum =1;
	var nCount=0;
	var nScrollPage =0;

	function View() {
		$('<div class="sf-service-usb"></div>').html([
		'<div class="sf-service-usb-bg">'+
		'<div id="sf-service-usb-bg-up-l-alpha"></div>'+
		'<div id="sf-service-usb-bg-up-c-alpha"></div>'+
		'<div id="sf-service-usb-bg-up-r-alpha"></div>'+
		'<div id="sf-service-usb-bg-mid-l-alpha"></div>'+
		'<div id="sf-service-usb-bg-mid-c-alpha"></div>'+
		'<div id="sf-service-usb-bg-mid-r-alpha"></div>'+
		'<div id="sf-service-usb-bg-bottom-l-alpha"></div>'+
		'<div id="sf-service-usb-bg-bottom-c-alpha"></div>'+
		'<div id="sf-service-usb-bg-bottom-r-alpha"></div>'+
		'<div id="sf-service-usb-bg-up-l"></div>'+
		'<div id="sf-service-usb-bg-up-c"></div>'+
		'<div id="sf-service-usb-bg-up-r"></div>'+
		'<div id="sf-service-usb-bg-mid-l"></div>'+
		'<div id="sf-service-usb-bg-mid-c"></div>'+
		'<div id="sf-service-usb-bg-mid-r"></div>'+
		'<div id="sf-service-usb-bg-bottom-l"></div>'+
		'<div id="sf-service-usb-bg-bottom-c"></div>'+
		'<div id="sf-service-usb-bg-bottom-r"></div>'+
		'<div id="sf-service-usb-title">'+sf.lang.SID_USB+'</div>'+
		'<div id="sf-service-usb-size"></div>'+
		'<div id="sf-service-usb-vendorName"></div>'+
		'<div id="sf-service-usb-bg-line"></div>'+
		'<div class="sf-service-usb-okbutton-l"></div>'+
		'<div class="sf-service-usb-okbutton-c">'+sf.lang.SID_OK+'</div>'+
		'<div class="sf-service-usb-okbutton-r"></div>'+
		'<div class="sf-service-usb-cancelbutton-l"></div>'+
		'<div class="sf-service-usb-cancelbutton-c">'+sf.lang.SID_CANCEL+'</div>'+
		'<div class="sf-service-usb-cancelbutton-r"></div>'+
		'<div id="sf-service-usb-keyhelp"></div>'+
		'<div id="sf-service-usb-label"></div>'+
		'<div class="sf-service-usb-text">'+
		'<div id="sf-service-usb-text-arrowUp"></div>'+
		'<div id="sf-service-usb-text-arrowDown"></div>'+
		'<div id="sf-service-usb-text-top-l"></div>'+
		'<div id="sf-service-usb-text-top-c"></div>'+
		'<div id="sf-service-usb-text-top-r"></div>'+
		'<div id="sf-service-usb-text-mid-l"></div>'+
		'<div id="sf-service-usb-text-mid-c"></div>'+
		'<div id="sf-service-usb-text-mid-r"></div>'+
		'<div id="sf-service-usb-text-bottom-l"></div>'+
		'<div id="sf-service-usb-text-bottom-c"></div>'+
		'<div id="sf-service-usb-text-bottom-r"></div>'+
		'<div class="sf-service-usb-focus">'+
		'<div id="sf-service-usb-focus-l"></div>'+
		'<div id="sf-service-usb-focus-c"></div>'+
		'<div id="sf-service-usb-focus-r"></div>'+
		'</div>'+
		'<div id="sf-service-usb-item1">'+
		'<div id="sf-service-usb-item1-checkbox"></div>'+
		'<div id="sf-service-usb-item1-icon"></div>'+
		'<div id="sf-service-usb-item1-label"></div>'+
		'</div>'+
		'<div id="sf-service-usb-item2">'+
		'<div id="sf-service-usb-item2-checkbox"></div>'+
		'<div id="sf-service-usb-item2-icon"></div>'+
		'<div id="sf-service-usb-item2-label"></div>'+
		'</div>'+
		'<div id="sf-service-usb-item3">'+
		'<div id="sf-service-usb-item3-checkbox"></div>'+
		'<div id="sf-service-usb-item3-icon"></div>'+
		'<div id="sf-service-usb-item3-label"></div>'+
		'</div>'+
		'<div id="sf-service-usb-item4">'+
		'<div id="sf-service-usb-item4-checkbox"></div>'+
		'<div id="sf-service-usb-item4-icon"></div>'+
		'<div id="sf-service-usb-item4-label"></div>'+
		'</div>'+
		'<div id="sf-service-usb-item5">'+
		'<div id="sf-service-usb-item5-checkbox"></div>'+
		'<div id="sf-service-usb-item5-icon"></div>'+
		'<div id="sf-service-usb-item5-label"></div>'+
		'</div>'+
		'<div id="sf-service-usb-scroll">'+
		'<div id="sf-service-usb-scroll-body">'+
		'<div id="sf-service-usb-scroll-body-top"></div>'+
		'<div id="sf-service-usb-scroll-body-middle"></div>'+
		'<div id="sf-service-usb-scroll-body-bottom"></div>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div>'
		].join('')).appendTo('body')
		$('#sf-service-usb-item1-label').sfLabel({
			text: ''
		});

		$('#sf-service-usb-item1-checkbox').sfCheckBox();

		$('#sf-service-usb-item2-label').sfLabel({
			text: ''
		});

		$('#sf-service-usb-item2-checkbox').sfCheckBox();

		$('#sf-service-usb-item3-label').sfLabel({
			text: ''
		});

		$('#sf-service-usb-item3-checkbox').sfCheckBox();

		$('#sf-service-usb-item4-label').sfLabel({
			text: ''
		});

		$('#sf-service-usb-item4-checkbox').sfCheckBox();

		$('#sf-service-usb-item5-label').sfLabel({
			text: ''
		});

		$('#sf-service-usb-item5-checkbox').sfCheckBox();

		$('#sf-service-usb-keyhelp').sfKeyHelp({
			'iconset':'GRAY',
			'move': sf.lang.SID_MOVE,
			'enter': sf.lang.SID_SELECT,
			'return': sf.lang.SID_RETURN
		})

		this.show = function() {
			$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
				'color' : '#ffba19'
			});
			alert("$(window).height()................................"+$(window).height());
			$('.sf-service-usb').css({
				'top' : ($(window).height()-$('.sf-service-usb').height())/2+'px',
				'left' : ($(window).width()-$('.sf-service-usb').width())/2+'px'
			});
			$('.sf-service-usb-focus').show();
			$('.sf-service-usb').show();

		}
		this.hide = function() {
			aFilepath=new Array();
			aUsbNum =new Array();
			bSetUsb=true;
			nCount =0;
			$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position'+nIndex);
			$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
				'color' : '#ffffff'
			});
			$('#sf-service-usb-size').html('');
			$('#sf-service-usb-vendorName').html('');
			$('.sf-service-usb-okbutton-l').removeClass('sf-service-usb-button-focus-1');
			$('.sf-service-usb-okbutton-c').removeClass('sf-service-usb-button-focus-c');
			$('.sf-service-usb-okbutton-r').removeClass('sf-service-usb-button-focus-r');
			$('.sf-service-usb-cancelbutton-l').removeClass('sf-service-usb-button-focus-1');
			$('.sf-service-usb-cancelbutton-c').removeClass('sf-service-usb-button-focus-c');
			$('.sf-service-usb-cancelbutton-r').removeClass('sf-service-usb-button-focus-r');
			for(var i=0; i<aObjectList.length; i++) {
				aObjectList[i].checkinfo = 0;
			}
			$('.sf-service-usb').hide();
		}
		this.setTextView = function(listpage) {
			view.selectedFile();
			var max_page=listpage+MAX_ITEM_NUM;
			var nItem;
			if (bSetUsb == true) {
				$('#sf-service-usb-label').html('');
				if (max_page > aUsbNum.length) {
					max_page=aUsbNum.length+1;
				}
				for (var i=(listpage-1); i<max_page-1; i++) { //usb 선택 화면
					nItem = (i % MAX_ITEM_NUM) + 1;
					$('#sf-service-usb-item' + nItem + '-icon').attr('class','sf-service-usb-item-icon-usb');
					$('#sf-service-usb-item' + nItem + '-icon').show();
					var nTotalSize = aUsbNum[nItemNum-1].totalSize/1048576;
					var nAvailSize = aUsbNum[nItemNum-1].availSize/1048576;
					var nUsingSize = nTotalSize-nAvailSize;
					alert("bSizeAvailable.........................................."+bSizeAvailable);
					if(bSizeAvailable){
						$('#sf-service-usb-size').html(''+nUsingSize.toFixed(2)+'GB'+' / ' + nTotalSize.toFixed(2)+'GB');
					}
					else{
						$('#sf-service-usb-size').html('');
					}
					$('#sf-service-usb-vendorName').html(''+aUsbNum[nItemNum-1].vendorName);
					$('#sf-service-usb-item' + nItem + '-label').html('' + aUsbNum[i].vendorName);
				}

			} else {
				if (max_page > aObjectList.length) {
					max_page=aObjectList.length;
				}
				for (var i=listpage; i<max_page; i++) {
					nItem = i%MAX_ITEM_NUM;
					if (nItem == 0) { //표시 할 수가 나누어 떨어질 경우 5로 설정 해서 5개씩 화면에 표시
						nItem = MAX_ITEM_NUM;
					}
					if (aObjectList[i].fileInfo.isDir) {
						$('#sf-service-usb-item' + nItem + '-checkbox').sfCheckBox('hide');
						if(aObjectList[i].fileInfo.name == '..') { //상위 폴더로 가기 일때
							$('#sf-service-usb-item' + nItem + '-icon').attr('class','sf-service-usb-item-icon-upFolder');
						} else {
							$('#sf-service-usb-item' + nItem + '-icon').attr('class','sf-service-usb-item-icon-folder');
						}
						$('#sf-service-usb-item' + nItem + '-icon').show();

					} else {
						$('#sf-service-usb-item' + nItem + '-checkbox').sfCheckBox('show');
						if (aObjectList[i].checkinfo == 1) {
							$('#sf-service-usb-item' + nItem + '-checkbox').sfCheckBox('check');
						} else {
							$('#sf-service-usb-item' + nItem + '-checkbox').sfCheckBox('uncheck');
						}
						view.setIcon(nItem, aObjectList[i].fileInfo.name);
						$('#sf-service-usb-item' + nItem + '-icon').show();
					}
					alert('fileInfo.name==================='+aObjectList[i].fileInfo.name);

					if(aObjectList[i].fileInfo.name == '..') { //상위 폴더로 가는 dir의 경우
						$('#sf-service-usb-item' + nItem + '-label').html(''+sf.lang.SID_UPPER_FOLDER);
					} else {
						$('#sf-service-usb-item' + nItem + '-label').html('' + aObjectList[i].fileInfo.name);
					}
				}
			}
			if(nPageNum == 1 && nScrollPage !=1) { //arrow 표시 여부
				$("#sf-service-usb-text-arrowUp").hide();
				$("#sf-service-usb-text-arrowDown").show();
			}
			else if(nPageNum == nScrollPage ){
				$("#sf-service-usb-text-arrowUp").show();
				$("#sf-service-usb-text-arrowDown").hide();
			}
			else{
				$("#sf-service-usb-text-arrowDown").show();
				$("#sf-service-usb-text-arrowUp").show();
			}
		}
		this.moveScroll = function(Index) {
			if(Index == 'prev') {
				nScrollBodyTop = nScrollBodyTop-nScrollMove;
				$('#sf-service-usb-scroll-body').css({
					'top' : nScrollBodyTop
				});
				if(nScrollBodyTop < 0 ) {
					$('#sf-service-usb-scroll-body').css({
						'top' : '0'
					});
				}
			} else if(Index == 'next') {
				nScrollBodyTop =nScrollBodyTop+nScrollMove;
				$('#sf-service-usb-scroll-body').css({
					'top' : nScrollBodyTop
				});
				if(nScrollBodyTop >= nScrollHeight) {
					nScrollBodyTop =nScrollBodyTop-nScrollMove;
				}
			}

		}
		this.setIcon= function(num,fileName) { //Icon 모양 결정
			var sIconType = fileName.match(/\.(\w+)$/i);
			alert('================'+sIconType);
			var nImaRlt=0;
			var nTextRlt=0;
			var nVideoRlt=0;
			var nMusicRlt=0;
			if(sIconType != null) { //확장자 없는 예외 상황일 경우 처리  (추가)
				sIconType = sIconType[1].toLowerCase();
				nImaRlt = $.inArray(sIconType, aImaArr);
				nTextRlt = $.inArray(sIconType, aTextArr);
				nVideoRlt = $.inArray(sIconType, aVideoArr);
				nMusicRlt = $.inArray(sIconType, aMusicArr);

				if(nImaRlt >= 0 ) {
					$('#sf-service-usb-item' + num + '-icon').attr('class','sf-service-usb-item-icon-image');
				} else if(nTextRlt >= 0) {
					$('#sf-service-usb-item' + num + '-icon').attr('class','sf-service-usb-item-icon-text');
				} else if(nVideoRlt >= 0) {
					$('#sf-service-usb-item' + num + '-icon').attr('class','sf-service-usb-item-icon-video');
				} else if(nMusicRlt >= 0) {
					$('#sf-service-usb-item' + num + '-icon').attr('class','sf-service-usb-item-icon-music');
				} else {
					alert('[AF usb] NOT found!!');
					$('#sf-service-usb-item' + num + '-icon').attr('class','sf-service-usb-item-icon-File');

				}
			} else {  //확장자가 없을 경우
				$('#sf-service-usb-item' + num + '-icon').attr('class','sf-service-usb-item-icon-File');
			}

		}
		this.selectedFile = function() { //checkinfo를 통해 선택된 파일에 따라 화면 표시 사항을 결정
			nCount=0;
			for(var i=0; i<aObjectList.length; i++) { //check된 것이 있으면 카운트 증가
				if (aObjectList[i].checkinfo == 1) {
					nCount++;
				}
			}
			if(nCount == 0) {
				$('#sf-service-usb-label').html('');
				$('#sf-service-usb-keyhelp').sfKeyHelp({
					'iconset':'GRAY',
					'move': sf.lang.SID_MOVE,
					'enter': sf.lang.SID_SELECT,
					'return': sf.lang.SID_RETURN
				})
			} else if(nCount != 0) {
				$('#sf-service-usb-keyhelp').sfKeyHelp({
					'iconset':'GRAY',
					'red' : sf.lang.SID_DESELECT_ALL,
					'move': sf.lang.SID_MOVE,
					'enter': sf.lang.SID_SELECT,
					'return': sf.lang.SID_RETURN
				})
				if(nCount == 1) {
					$('#sf-service-usb-label').html(sf.lang.SID_MIX_FILE_SELECTED.replace(/\<\<A\>\>/g,'1'));
				} else {
					$('#sf-service-usb-label').html(sf.lang.SID_MIX_FILES_SELECTED_MSG.replace(/\<\<A\>\>/g,nCount));
				}
			}
		}
		this.initTextView = function() { //화면 초기화
			bFocusBox = true;
			bFocusButton = true;
			$('#sf-service-usb-scroll-body').css({
				'top' : '0'
			});
			$('#sf-service-usb-label').html('');
			for(var i=1; i<=MAX_ITEM_NUM; i++) {
				$('#sf-service-usb-item' + i + '-label').html('');
				$('#sf-service-usb-item' + i + '-label').css({
					'color' : '#ffffff'
				});
				$('#sf-service-usb-item' + i + '-checkbox').sfCheckBox('hide');
				$('#sf-service-usb-item' + i + '-icon').hide();
				$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position' + (i-1));
			}
		}
		this.setCheckbox = function(state) {
			if(aObjectList[nItemNum].checkinfo==0) {
				$('#sf-service-usb-item' + (state+1) + '-checkbox').sfCheckBox('check');
				aObjectList[nItemNum].checkinfo=1;
			} else if(aObjectList[nItemNum].checkinfo==1) {
				$('#sf-service-usb-item' + (state+1) + '-checkbox').sfCheckBox('uncheck');
				aObjectList[nItemNum].checkinfo=0;
			}
		}
	}

	var ANCHOR_ID = 'sf-service-usb';

	var nIndex = 0;	//text box focus
	var bFocusBox = true; //text box or button focus
	var bFocusButton = true;
	var nPageNum = 1;
	var nScrollBodyTop = 0;

	sf.service.USB.handleKeydown = function () {
		var keyCode = event.keyCode;
		alert('[AF ui] sfPopup keyctl('+keyCode+')');
		switch (keyCode) {
			case sf.key.LEFT:
				if(bFocusBox == true) {
					$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-file');
					$('.sf-service-usb-focus').hide();
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffffff'
					});
					$('.sf-service-usb-okbutton-l').addClass('sf-service-usb-button-focus-1');
					$('.sf-service-usb-okbutton-c').addClass('sf-service-usb-button-focus-c');
					$('.sf-service-usb-okbutton-r').addClass('sf-service-usb-button-focus-r');
					bFocusBox=false;
				} else if(bFocusBox == false) {
					$('.sf-service-usb-okbutton-l').removeClass('sf-service-usb-button-focus-1');
					$('.sf-service-usb-okbutton-c').removeClass('sf-service-usb-button-focus-c');
					$('.sf-service-usb-okbutton-r').removeClass('sf-service-usb-button-focus-r');
					$('.sf-service-usb-cancelbutton-l').removeClass('sf-service-usb-button-focus-1');
					$('.sf-service-usb-cancelbutton-c').removeClass('sf-service-usb-button-focus-c');
					$('.sf-service-usb-cancelbutton-r').removeClass('sf-service-usb-button-focus-r');
					$('.sf-service-usb-focus').show();
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffba19'
					});
					bFocusButton=true;
					bFocusBox=true;
				}
				break;
			case sf.key.RIGHT:
				alert("bFocusBox............................................................"+bFocusBox);
				if(bFocusBox == true) {
					$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-file');
					$('.sf-service-usb-focus').hide();
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffffff'
					});
					$('.sf-service-usb-okbutton-l').addClass('sf-service-usb-button-focus-1');
					$('.sf-service-usb-okbutton-c').addClass('sf-service-usb-button-focus-c');
					$('.sf-service-usb-okbutton-r').addClass('sf-service-usb-button-focus-r');
					bFocusBox=false;
				} else if(bFocusBox == false) {
					$('.sf-service-usb-focus').show();
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffba19'
					});
					$('.sf-service-usb-okbutton-l').removeClass('sf-service-usb-button-focus-1');
					$('.sf-service-usb-okbutton-c').removeClass('sf-service-usb-button-focus-c');
					$('.sf-service-usb-okbutton-r').removeClass('sf-service-usb-button-focus-r');
					$('.sf-service-usb-cancelbutton-l').removeClass('sf-service-usb-button-focus-1');
					$('.sf-service-usb-cancelbutton-c').removeClass('sf-service-usb-button-focus-c');
					$('.sf-service-usb-cancelbutton-r').removeClass('sf-service-usb-button-focus-r');
					bFocusButton=true;
					bFocusBox=true;
				}
				alert("bFocusButton111............................................................"+bFocusButton);
				break;
			case sf.key.UP:
				if(bFocusBox == true) {
					$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position'+nIndex);
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffffff'
					});
					nItemNum--;

					if(nSize == -1){
						nItemNum=1;
						nIndex=0;
					}
					else if(bSetUsb==true || nSize == -1) {
						if(nItemNum < 1) {
							nItemNum=1;
						} else {
							nIndex--;
						}
						var nTotalSize = aUsbNum[nItemNum-1].totalSize/1048576;
						var nAvailSize = aUsbNum[nItemNum-1].availSize/1048576;
						var nUsingSize = nTotalSize-nAvailSize;
						alert("bSizeAvailable.........................................."+bSizeAvailable);
						if(bSizeAvailable){
							$('#sf-service-usb-size').html(''+nUsingSize.toFixed(2)+'GB'+' / ' + nTotalSize.toFixed(2)+'GB');
						}
						else{
							$('#sf-service-usb-size').html('');
						}
						$('#sf-service-usb-vendorName').html(''+aUsbNum[nItemNum-1].vendorName);
					} else {
						if(aObjectList[nItemNum].fileInfo.isDir) {
							$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-file');
						} else {
							$('.sf-service-usb-focus').addClass('sf-service-usb-focus-file');
						}

						if(nItemNum < 1) {
							nItemNum=1;
						} else {
							nIndex--;
						}
					}
					if(nIndex < 0) {
						nListPageNum=nListPageNum-MAX_ITEM_NUM;
						if(nListPageNum < 1) {
							nListPageNum=1;
						}
						if(nPageNum <= 1) {
							nPageNum=1;
							nIndex=0;
						} else {
							nPageNum--;
							view.initTextView();
							view.setTextView(nListPageNum);
							nIndex=4;
						}
						view.moveScroll('prev');
					}
					$('.sf-service-usb-focus').addClass('sf-service-usb-focus-position'+nIndex);
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffba19'
					});
				} else if(bFocusBox == false) {
					if(bFocusButton == true) {
						$('.sf-service-usb-okbutton-l').removeClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-okbutton-c').removeClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-okbutton-r').removeClass('sf-service-usb-button-focus-r');
						$('.sf-service-usb-cancelbutton-l').addClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-cancelbutton-c').addClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-cancelbutton-r').addClass('sf-service-usb-button-focus-r');
						bFocusButton=false;
					} else if(bFocusButton == false) {
						$('.sf-service-usb-okbutton-l').addClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-okbutton-c').addClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-okbutton-r').addClass('sf-service-usb-button-focus-r');
						$('.sf-service-usb-cancelbutton-l').removeClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-cancelbutton-c').removeClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-cancelbutton-r').removeClass('sf-service-usb-button-focus-r');
						bFocusButton=true;
					}
				}
				break;
			case sf.key.DOWN:
				if(bFocusBox == true) {
					$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position'+nIndex);
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffffff'
					});
					nItemNum++;
					if(nSize == -1){
						nItemNum=1;
						nIndex=0;
					}
					else if(bSetUsb == true) {
						if(nItemNum > aUsbNum.length) {
							nItemNum=aUsbNum.length;
						} else {
							nIndex++;
						}
						var nTotalSize = aUsbNum[nItemNum-1].totalSize/1048576;
						var nAvailSize = aUsbNum[nItemNum-1].availSize/1048576;
						var nUsingSize = nTotalSize-nAvailSize;
						alert("bSizeAvailable.........................................."+bSizeAvailable);
						if(bSizeAvailable){
							$('#sf-service-usb-size').html(''+nUsingSize.toFixed(2)+'GB'+' / ' + nTotalSize.toFixed(2)+'GB');
						}
						else{
							$('#sf-service-usb-size').html('');
						}
						$('#sf-service-usb-vendorName').html(''+aUsbNum[nItemNum-1].vendorName);
					} else {
						if(nItemNum > aObjectList.length-1 ) {
							nItemNum=aObjectList.length-1;
						} else {
							nIndex++;
						}

						if(aObjectList[nItemNum].fileInfo.isDir) {
							$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-file');
						} else {
							$('.sf-service-usb-focus').addClass('sf-service-usb-focus-file');
						}
					}
					if(nIndex > 4) {
						nPageNum++;
						if(nPageNum > nScrollPage) {
							nPageNum=nScrollPage;
							nIndex=4;
						} else {
							view.initTextView();
							nIndex=0
							nListPageNum = nListPageNum+MAX_ITEM_NUM;
							view.setTextView(nListPageNum);
						}
						view.moveScroll('next');

					}
					$('.sf-service-usb-focus').addClass('sf-service-usb-focus-position'+nIndex);
					$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
						'color' : '#ffba19'
					});

				} else if(bFocusBox == false) {
					if(bFocusButton == true) {
						$('.sf-service-usb-okbutton-l').removeClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-okbutton-c').removeClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-okbutton-r').removeClass('sf-service-usb-button-focus-r');
						$('.sf-service-usb-cancelbutton-l').addClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-cancelbutton-c').addClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-cancelbutton-r').addClass('sf-service-usb-button-focus-r');
						bFocusButton=false;
					} else if(bFocusButton == false) {
						$('.sf-service-usb-okbutton-l').addClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-okbutton-c').addClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-okbutton-r').addClass('sf-service-usb-button-focus-r');
						$('.sf-service-usb-cancelbutton-l').removeClass('sf-service-usb-button-focus-1');
						$('.sf-service-usb-cancelbutton-c').removeClass('sf-service-usb-button-focus-c');
						$('.sf-service-usb-cancelbutton-r').removeClass('sf-service-usb-button-focus-r');
						bFocusButton=true;
					}
				}
				break;

			case sf.key.ENTER:
				if(bFocusBox == true) {
					if(bSetUsb == true) {
						if(nSize == -1) {
							alert('USB None');
						} else {
							alert('USB select');
							defaultDir = '$usb_DIR' + '/' + aUsbNum[nItemNum-1].mountPath;
							var nTotalSize = aUsbNum[nItemNum-1].totalSize/1048576;
							var nAvailSize = aUsbNum[nItemNum-1].availSize/1048576;
							var nUsingSize = nTotalSize-nAvailSize;
							alert("bSizeAvailable.........................................."+bSizeAvailable);
							if(bSizeAvailable){
								$('#sf-service-usb-size').html(''+nUsingSize.toFixed(2)+'GB'+' / ' + nTotalSize.toFixed(2)+'GB');
							}
							else{
								$('#sf-service-usb-size').html('');
							}
							$('#sf-service-usb-vendorName').html(''+aUsbNum[nItemNum-1].vendorName);
							aFilepath.push(defaultDir);
							bSetUsb=false;
							Refresh();
						}
					} else {
						if(nItemNum == 1) {
							if(aFilepath.join('')==defaultDir && aObjectList[nItemNum].fileInfo.isDir) {
								if(aObjectList[nItemNum].fileInfo.isDir) {
									if(aFilepath.join('')==defaultDir) {
										aFilepath=new Array();
										bSetUsb=true;
									} else {
										aFilepath.push(aObjectList[nItemNum].fileInfo.name);
									}
									$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position' + nIndex);
									$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
										'color' : '#ffffff'
									});
									Refresh();
								}
							} else if(aObjectList[nItemNum].fileInfo.isDir) {
								aFilepath.pop();
								$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position' + nIndex);
								$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
									'color' : '#ffffff'
								});
								Refresh();
							} else {
								view.setCheckbox(nIndex);
								view.selectedFile();
							}
						} else if (aObjectList[nItemNum].fileInfo.isDir) {
							aFilepath.push(aObjectList[nItemNum].fileInfo.name);
							$('.sf-service-usb-focus').removeClass('sf-service-usb-focus-position' + nIndex);
							$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
								'color' : '#ffffff'
							});
							Refresh();
						} else {
							view.setCheckbox(nIndex);
							view.selectedFile();
						}
					}
				} else if(bFocusBox == false) {
					alert("bFocusButton222............................................................"+bFocusButton);
					if(bFocusButton == true) {
						PathMerge();
						alert("test__OnReturn............................................................"+aOnReturn)
						refunction(aOnReturn); //merge 된 경로를 리턴함수로 보낸다
						sf.scene.returnFocus();
						sp.UnsubscribeEvent(USB_UNPLUGED);
						sp.UnsubscribeEvent(USB_PLUGED);
						view.hide();
					} else {
						sf.scene.returnFocus();
						sp.UnsubscribeEvent(USB_UNPLUGED);
						sp.UnsubscribeEvent(USB_PLUGED);
						view.hide();
						refunction(null);
					}
					aOnReturn=new Array();
				}
				break;
			case sf.key.RED :
				//체크된 파일을 모두 uncheck
				if(nCount != 0) {
					for(var i=0; i<aObjectList.length; i++) {
						aObjectList[i].checkinfo = 0;
					}
					for(var i=1; i<=MAX_ITEM_NUM; i++) {
						$('#sf-service-usb-item' + i + '-checkbox').sfCheckBox('uncheck');
					}
					nCount=0;
					view.selectedFile();
				}
				break;

			case sf.key.RETURN :
			case sf.key.EXIT :
				sf.scene.returnFocus();
				sf.key.preventDefault();
				sp.UnsubscribeEvent(USB_UNPLUGED);
				sp.UnsubscribeEvent(USB_PLUGED);
				view.hide();
				refunction(null);
				break;
		};
	}
	function PathMerge() {	//check된 파일의 경로를 합친다
		for(var i=0; i<aObjectList.length; i++) {
			if (aObjectList[i].checkinfo == 1) {
				var selected = aFilepath.join('/');
				aOnReturn.push(selected+'/'+aObjectList[i].fileInfo.name);
				alert('[AF usb]aOnReturn:'+aOnReturn);
			}
		}
	}

	function Refresh() { //화면 초기화
		view.initTextView();
		aObjectList = new Array();
		readUsbList(aFilepath);
		nItemNum=1;
		nCount=0;
		nListPageNum=1;
		nPageNum=1;
		nIndex=0;
		for(var i=0; i<aObjectList.length; i++) {
			aObjectList[i].checkinfo=0;
		}

		$('.sf-service-usb-focus').addClass('sf-service-usb-focus-position'+nIndex);
		$('.sf-service-usb-focus').show();
		$('#sf-service-usb-item'+(nIndex+1)+'-label').css({
			'color' : '#ffba19'
		});
		$('#sf-service-usb-keyhelp').sfKeyHelp('show');
		view.setTextView(nListPageNum);
	}

})(sf);
function sfUsbEvent(msg) {
	alert('sfUsbEvent('+msg+')');
	sf.service.USB.handleUSBEvent(msg);
}/*
 *	Title: Service API (MediaBrowser)
 *		Apps Framework MediaBrowser Service API
 *
 *	Version added:
 *		1.1.0
 */
(function (sf) {
	/*
	 *	Namespace : sf.service.MediaBrowser
	 *		Browse the files in the USB memory. And select some media files from it by user.
	 *
	 */
	sf.service.MediaBrowser = {
		MEDIATYPE_PHOTO: 1,
		MEDIATYPE_MOVIE: 2,
		MEDIATYPE_MUSIC: 3,

		/*
		 * Function: show
		 * 	show the media browser window.
		 *
		 * Parameters:
		 *	o callback - (Function(Object)) callback function to be called when the MediaBrowser popup is closed. The result of this MediaBrowser will be passed to this callback. Belows are JSON of the result object:
		 * (start code)
		 * {
		 * 		state: 'ok',	// 'ok' - return selected file. 'cancel' - User cancel this operation(returns Nothing).
		 * 		selectedMedia:	// Each items will be selected file's path, file's type, file's source. This Object will be passed to callback.
		 * 		[
		 *			{
		 *				filepath: '$usb_path/sda1/movies/1.wmv',
		 *				type: 'video',	// 'video', 'music', 'image'
		 *				source: 'USB'	// 'AllShare', 'USB'
		 * 			},
		 * 			...
		 *		]
		 * }
		 * (end)
		 *
		 *
		 * Available From:
		 * 	1.1.0
		 *
		 */

		show: function(options) {
			$.each (options, function (key, value) {
				if (typeof env[key] == 'function') {
					env[key](value);
				}
			});
			if(!bInit) {
				initialize();
			}
			var defaults = {
				zIndex : 100,
				mediaType: [sf.service.MediaBrowser.MEDIATYPE_PHOTO,sf.service.MediaBrowser.MEDIATYPE_MOVIE,sf.service.MediaBrowser.MEDIATYPE_MUSIC]
			};
			opts = $.extend(defaults, options);
			alert("opts.mediaType...................................."+opts.mediaType);
			alert("inArray(MEDIATYPE_PHOTO,opts.mediaType)...................................."+inArray(sf.service.MediaBrowser.MEDIATYPE_PHOTO, opts.mediaType));
			alert("inArray(MEDIATYPE_MOVIE,opts.mediaType)...................................."+inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE, opts.mediaType));
			alert("inArray(MEDIATYPE_MUSIC,opts.mediaType)...................................."+inArray(sf.service.MediaBrowser.MEDIATYPE_MUSIC, opts.mediaType));

			if(!inArray(sf.service.MediaBrowser.MEDIATYPE_PHOTO,opts.mediaType)){
				$('.sf-service-mediabrowser-main-Button-Icon0').css({
					'opacity' : 0.4
				});
				$('.sf-service-mediabrowser-main-Button0').css({
					'opacity' : 0.4
				});
			}

			if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE,opts.mediaType)){
				$('.sf-service-mediabrowser-main-Button-Icon1').css({
					'opacity' : 0.4
				});
				$('.sf-service-mediabrowser-main-Button1').css({
					'opacity' : 0.4
				});
			}

			if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MUSIC,opts.mediaType)){
				$('.sf-service-mediabrowser-main-Button-Icon2').css({
					'opacity' : 0.4
				});
				$('.sf-service-mediabrowser-main-Button2').css({
					'opacity' : 0.4
				});
			}

			$('.sf-service-mediabrowser-main').css({
				'z-index' : opts.zIndex
			});

			$('.sf-service-mediabrowser-file').css({
				'z-index' : opts.zIndex+10
			});

			mainview.setInitMainView();
			readUsb(); //초기 USB 정보 리드 (usb 유/무 , usb plugin )
			mainview.setUsbListView(nUsbPageStartNum);
			$('#'+ANCHOR_ID).focus();
			mainview.show();
		}
	}

	var fs = null;
	var sp = null;
	var sDefaultDir = null;
	var opts = null;
	var bInit = false;
	var bflag = true; //USB 파일이 있는지 없는지 확인하는  flag
	var bMainPage = true;
	var bKeyblock = false; // 썸네일이 만들어 지는 동안 Keyblock 시킨다
	var bThumbnailFlag = true;
	var bThumbnailTableStartFlage = true;
	var bThumbnailPageChangeFlag = false;
	var sPattern=null;
	var refunction = null;
	var USB_UNPLUGED=1;
	var USB_PLUGED=0;
	var CHECKBOX_LEFT = 56;
	var CHECKBOX_TOP = 60;
	var FOCUS_CHECKBOX_LEFT = 46;
	var FOCUS_CHECKBOX_TOP = 52;
	var CHECK_LEFT = 56;
	var CHECK_TOP = 60;
	var FOCUS_CHECK_LEFT = 46;
	var FOCUS_CHECK_TOP = 52;
	var NOR_LABEL_FONTSIZE = 22;
	var NOR_LABEL_WIDTH = 125;
	var NOR_LABEL_HEIGHT = 26;
	var FOCUS_LABEL_FONTSIZE = 24;
	var FOCUS_LABEL_WIDTH = 125;
	var FOCUS_LABEL_HEIGHT = 27;
	var NOR_IMAGE_TOP = 73;
	var NOR_IMAGE_LEFT = 70;
	var THUMBNAIL_WIDTH = 114;
	var THUMBNAIL_HEIGHT = 84;
	var FOCUS_IMAGE_TOP = 66;
	var FOCUS_IMAGE_LEFT = 61;
	var THUMBNAIL_FOCUS_WIDTH = 132;
	var THUMBNAIL_FOCUS_HEIGHT = 98;
	var MAX_MAIN_BUTTON_NUM = 2;
	var MAX_FILE_BUTTON_NUM = 2;
	var MAX_USB_FOCUS_NUM = 2;
	var MIN_USB_FOCUS_NUM = 0;
	var UPPERFOLDER = 0;
	var FOLDER = 1;
	var FILE = 2;
	var FOLDERVIEW = 0;
	var THUMBNAILDEFAULTPATH = '/dtv/FamilyHub/'
	var NOFILE = null;
	var MAX_DEVICE_NUM = 3;
	var MAX_ROW = 4;
	var MAX_COL = 6;
	var MAX_PAGEITEMNUM = 24;
	var nfileType = 0;
	var nThumbnailIndex = 0;
	var nThumbnailName = 0;
	var aImaArr = new Array('png','jpg','bmp','gif');
	var	aVideoArr = new Array('avi','wma','mp4','wmv');
	var	aMusicArr = new Array('mp3');
	var aMonth = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')
	var sThumbnailFullPath = new Array();
	var aDeviceNum = new Array();
	var nThumbnailPageChangeRow = 0;
	var nThumbnailPageChangeCol = 0;
	var nThumbnailRow = 0;
	var nThumbnailCol = 0;
	var nThumbnailTableStartRow = 0;
	var nThumbnailTableStartCol = 0;
	var nThumbnailCategoryCol = 0;
	var nThumbnailCategoryRow = 0;
	var nMaxCategoryNum = 0;
	var nPageStartNum = 1;
	var nSize = 0;
	var nUsbPageStartNum = 0;
	var nGetLastIndex = 0;
	var nUsbPageNum = 1;
	var nMaxDevicePageNum = 0;
	var aObjectList = new Array();
	var aCheckList = new Array();
	var aFileTable;
	var aThumbnailTable;
	var aThumbnailFileTable;
	var nFileScrollHeight=0;
	var nFileScrollBody = 0;
	var nFileScrollMove = 0;
	var nMainScrollHeight=0;
	var nMainScrollBody = 0;
	var nMainScrollMove = 0;
	var oMadeThumbnailFile = [];
	var SefPlugin = null;

	var env = {
		callback : function (value) {
			if (typeof value == 'function')
				refunction = value;
			else
				alert ('[AF mediabrowser] value must be a Function.');
		}
	}
	function initialize() {
		mountPath = null;
		dirPath =null;
		fs = new FileSystem();
		object = null // usb_object
		aObjectList = new Array(); //aObjectList
		aDeviceNum =new Array();
		aFilepath = new Array(); //attr
		aUserFilepath = new Array();
		mainview = new MainView();
		fileview = new FileView();
		alert(window.location.search);

		$('<a href="javascript:void(0);" id="'+ANCHOR_ID+'"></a>').appendTo('body');
		$('#'+ANCHOR_ID).keydown( function() {
			sf.service.MediaBrowser.handleKeydown();
		});
		bInit = true;
	}

	function inArray(find, list){
		alert("inArray(" + find + ", " + list + ")");
		for (var i = 0; i < list.length; i++) {
			if (list[i] == find)
				return true;
		}
		return false;
	}

	function readUsb() {
		// watchdog off
		sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_TRUE);
		sp = sf.core.plugin('Storage');
		nSize = sp.GetUSBListSize();
		var nlistNum = 0;
		alert('[AF mediabrowser] GetUSBListSize :'+nSize);
		//SefPlugin = sf.core.sefplugin('ContentsMgr');
		var nCreateContentsMgr = deviceapis._plugin('ContentsMgr', 'CreateContentsMgr');
		alert("CreateContentsMgr...................................................."+nCreateContentsMgr);
		var nGetDeviceListNum = deviceapis._plugin('ContentsMgr', 'GetDeviceListNum');
		alert("GetDeviceListNum...................................................."+nGetDeviceListNum);


		deviceapis._plugin('ContentsMgr').OnEvent = OnEvent;

		//SefPlugin.OnEvent=OnEvent;

		if(nSize != -1){ //연결된 USB 정보확인
			for (var i=0; i<nSize; i++) {
				var defaultID = sp.GetUSBDeviceID(i);
				var partitionNum=sp.GetUSBPartitionNum(defaultID); //USB partition
				for(var j=0; j<partitionNum; j++) {
					var nPartition = j+1;
					aDeviceNum[nlistNum] = {};
					if(partitionNum>=2){
						aDeviceNum[nlistNum].vendorName = sp.GetUSBVendorName(defaultID)+'&nbsp;&nbsp;('+nPartition+')';
					}
					else {
						aDeviceNum[nlistNum].vendorName = sp.GetUSBVendorName(defaultID);
					}
					aDeviceNum[nlistNum].mountPath = sp.GetUSBMountPath(defaultID,j);
					aDeviceNum[nlistNum].deviceType = 'USB';
					alert("aDeviceNum[nlistNum].mountPath......................................"+aDeviceNum[nlistNum].mountPath);
					nlistNum++;
				}
			}
		}
		if (nGetDeviceListNum > 1){ //연결된 DLNA 정보 확인
			for(var i=1; i<nGetDeviceListNum; i++){
				var sGetDeviceName = deviceapis._plugin('ContentsMgr', 'GetDeviceName', i);
				var nGetDeviceType = deviceapis._plugin('ContentsMgr', 'GetDeviceType', i);
				var sGetVenderName = deviceapis._plugin('ContentsMgr', 'GetVenderName', i);
				var nGetPartitionNum = deviceapis._plugin('ContentsMgr', 'GetPartitionNum', i);
				alert("GetDeviceName..........................................."+sGetDeviceName);
				alert("GetDeviceType..........................................."+nGetDeviceType);
				alert("GetVenderName..........................................."+sGetVenderName);
				alert("GetPartitionNum..........................................."+nGetPartitionNum);
				if(nGetDeviceType != 1){
					for(var j=0; j<nGetPartitionNum; j++){
						aDeviceNum[nlistNum] = {};
						var nPartition = j+1;
						if(nGetPartitionNum>=2){
							aDeviceNum[nlistNum].vendorName = sGetDeviceName+'&nbsp;&nbsp;('+nPartition+')';
						}
						else {
							aDeviceNum[nlistNum].vendorName = sGetDeviceName;
						}
						alert("nlistNum......................................................................."+nlistNum);
						alert("i......................................................................."+i);
						alert("j......................................................................."+j);
						aDeviceNum[nlistNum].mountPath = deviceapis._plugin('ContentsMgr', 'GetMountPath', i, j);
						aDeviceNum[nlistNum].partitionkey = deviceapis._plugin('ContentsMgr', 'GetPartitionKey', i, j);
						aDeviceNum[nlistNum].deviceType = 'DLNA';
						alert("aDeviceNum[nlistNum].mountPath..........................................."+aDeviceNum[nlistNum].mountPath);
						alert("aDeviceNum[nlistNum].partitionkey..........................................."+aDeviceNum[nlistNum].partitionkey);
						alert("aDeviceNum[nlistNum].vendorName..........................................."+aDeviceNum[nlistNum].vendorName);
						nlistNum++;
					}
				}
			}
		}

		if(nGetDeviceListNum <= 1){ //DLNA,USB 둘다 없을때
			alert("nlistNum1......................................"+nlistNum);
			aDeviceNum[nlistNum] = {};
			aDeviceNum[nlistNum].vendorName = ''
			alert("aDeviceNum[nlistNum].vendorName......................................"+aDeviceNum[nlistNum].vendorName);
			aDeviceNum[nlistNum].deviceType = 'USBNONE';
			alert("aDeviceNum[nlistNum].deviceType......................................"+aDeviceNum[nlistNum].deviceType);
			nlistNum++;
			aDeviceNum[nlistNum] = {};
			aDeviceNum[nlistNum].vendorName = '';
			alert("aDeviceNum[nlistNum].vendorName......................................"+aDeviceNum[nlistNum].vendorName);
			aDeviceNum[nlistNum].deviceType = 'DLNANONE';
			alert("aDeviceNum[nlistNum].deviceType......................................"+aDeviceNum[nlistNum].deviceType);
			alert("nlistNum2......................................"+nlistNum);
		}
		else if(nGetDeviceListNum > 1 && nSize == -1) { //USB 가 없을때
			aDeviceNum[nlistNum] = {};
			aDeviceNum[nlistNum].vendorName = ''
			aDeviceNum[nlistNum].deviceType = 'USBNONE';
			nlistNum++;
		}
		else if(nGetDeviceListNum > 1 && nSize != -1){ //DLNA 가 없을때
			if((nGetDeviceListNum-nSize) <= 1){
				aDeviceNum[nlistNum] = {};
				aDeviceNum[nlistNum].vendorName = '';
				aDeviceNum[nlistNum].deviceType = 'DLNANONE';
				nlistNum++;
			}
		}

		nMaxDevicePageNum = Math.ceil(aDeviceNum.length/MAX_DEVICE_NUM);
		alert("nMaxDevicePageNum................................"+nMaxDevicePageNum);
		if(nMaxDevicePageNum <= 1){
			$("#sf-service-mediabrowser-main-scroll").hide();
		}
		else {
			$("#sf-service-mediabrowser-main-scroll").show();
		}
		setMainScroll(nMaxDevicePageNum);


		// watchdog on
		sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_FALSE);

	}

	function setMainScroll(nScrollPageNum) {
		nMainScrollHeight = parseFloat($("#sf-service-mediabrowser-main-scroll").css('height'),10);
		var nTopHeight = parseFloat($("#sf-service-mediabrowser-main-scroll-body-top").css('height'),10);
		var nBottomHeight = parseFloat($("#sf-service-mediabrowser-main-scroll-body-bottom").css('height'),10);
		nMainScrollBody = nMainScrollHeight / nScrollPageNum;
		var nMiddleHeight = nMainScrollBody-(nTopHeight+nBottomHeight);
		if(nMiddleHeight <= 14) {
			nMiddleHeight = 14;
		}
		$('#sf-service-mediabrowser-main-scroll-body-middle').css({
			'height' : nMiddleHeight
		});

		nMainScrollMove = (nMainScrollHeight-(nTopHeight+nBottomHeight+nMiddleHeight)) / (nScrollPageNum-1);
	}

	function setFileType(value) { //main button 설정 값에 따라 파일 타입을 결정
		if (value == 0) {
			nfileType = 0;
			sPattern = 'image';
			NOFILE = sf.lang.TV_SID_NO_PHOTO_FILE_AVAILABLE_MSG;
			alert('sPattern==========='+sPattern);
		} else if (value == 1) {
			nfileType = 2;
			sPattern = 'video';
			NOFILE = sf.lang.TV_SID_NO_VIDEO_FILE_AVAILABLE_MSG;
			alert('sPattern==========='+sPattern);
		} else if (value == 2) {
			nfileType = 1;
			sPattern = 'music';
			NOFILE = sf.lang.TV_SID_NO_MUSIC_FILE_AVAILABLE_MSG;
			alert('sPattern==========='+sPattern);
		} else {
			alert('[AF mediabrowser] Not Supported type')
		}
	}

	function readUsbList(Path) { //USB 선택후 기본 주소를 통해 내부 list 확인
		sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_TRUE);
		alert('[AF usb] Path:'+Path);
		var fullPath = Path.join('');
		var ThumbnailPath = null;
		alert("fullPath....................................."+fullPath);
		object = fs.readDir(fullPath);
		var aDate = null;
		var nNumFolder=0;
		var nNumFile=0;

		for (var i=0; i<object.length; i++) { //directory가 우선 보여야 하므로 먼저 리드한다.
			if (object[i].isDir) {
				aObjectList[nNumFolder]= {
					filename : null,
					date : null,
					size : null,
					isDir : true,
					type : sPattern
				};
				aObjectList[nNumFolder].filename=object[i].name; //aObjectList : 경로에 따른 usb 내부 file 및 directory 등을  list로 보관
				nNumFolder++;
			}
		}
		nNumFile=nNumFolder; //folder 뒤로 file을 저장
		for (var i=0; i<object.length; i++) {
			if (!object[i].isDir) { //초기에 선언된 fileType(image,video,all..) 에 따라 패턴 검사
				var sExtName = object[i].name.match(/\.(\w+)$/i);
				var nRlt=0;
				if(sExtName !=null) {
					sExtName = sExtName[1].toLowerCase();
				}
				if(sPattern=='image') {
					nRlt = $.inArray(sExtName, aImaArr);
				} else if(sPattern=='video') {
					nRlt = $.inArray(sExtName, aVideoArr);
				} else if(sPattern=='music') {
					nRlt = $.inArray(sExtName, aMusicArr);
				}
				if(nRlt >= 0 ) {
					aObjectList[nNumFile]= {
						filename : null,
						date : null,
						size : null,
						isDir : false,
						type : sPattern
					};
					aObjectList[nNumFile].filename = object[i].name;
					//aObjectList[nNumFile].date=object[i].mtime;
					//alert("aObjectList[nNumFolder].date=object[i].mtime................................."+aObjectList[nNumFile].date);
					/*
					aDate = object[i].mtime;
					alert('aDate.............................................'+aDate);
					var aSplitDate = aDate.split(' ');
					alert("aSplitMonth................................."+aSplitDate);
					var nMonth = $.inArray(aSplitDate[2], aMonth);
					nMonth++;
					alert("nMonth................................."+nMonth);
					if(nMonth<10){
						var sFileMonth = '0'+nMonth.toString();
						alert("sFileMonth................................."+sFileMonth);
					}
					aObjectList[nNumFile].date = aSplitDate[3]+'-'+sFileMonth+'-'+aSplitDate[1];
					alert("aObjectList[nNumFile].date................................."+aObjectList[nNumFile].date);
					aObjectList[nNumFile].size=object[i].size;
					alert("aObjectList[nNumFolder].date=object[i].size................................."+aObjectList[nNumFile].size);
					*/
					nNumFile++;
				}
			}
		}
		var nCateNum = Math.ceil((aObjectList.length-1)/MAX_COL);
		if(nCateNum <=4) {
			nMaxPageNum = 1;
			$("#sf-service-mediabrowser-file-scroll").hide();
		} else {
			nMaxPageNum = (nCateNum - 4 )+1;	//fileview 의 네비게이터 정책상 스크롤의 크기를 구함. 4행(1),5행(2), 6행(3), 7행(4)...
			$("#sf-service-mediabrowser-file-scroll").show();
		}

		setFileScroll(nMaxPageNum);

		// watchdog on
		sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_FALSE);
	}




	function setFileScroll(nScrollPageNum) {
		nFileScrollHeight = parseFloat($("#sf-service-mediabrowser-file-scroll").css('height'),10);
		var nTopHeight = parseFloat($("#sf-service-mediabrowser-file-scroll-body-top").css('height'),10);
		var nBottomHeight = parseFloat($("#sf-service-mediabrowser-file-scroll-body-bottom").css('height'),10);
		nFileScrollBody = nFileScrollHeight / nScrollPageNum;
		var nMiddleHeight = nFileScrollBody-(nTopHeight+nBottomHeight);
		if(nMiddleHeight <= 14) {
			nMiddleHeight = 14;
		}
		$('#sf-service-mediabrowser-file-scroll-body-middle').css({
			'height' : nMiddleHeight
		});

		nFileScrollMove = (nFileScrollHeight-(nTopHeight+nBottomHeight+nMiddleHeight)) / (nScrollPageNum-1);
	}

	function getObjString(obj, prefix) {
		var result = '';
		try {
			for (var elmt in obj) {
				var pre = '';
				if (obj.constructor == Array) {
					pre = prefix + '[' + elmt + ']';
				} else {
					pre = prefix + '.' + elmt;
				}

				if (typeof obj[elmt] == 'object') {
					result += getObjString(obj[elmt], pre);
				} else {
					result += pre + ' = ' + obj[elmt] + '\n';
				}
			}
		} catch (e) {
			alert(e.message+' - '+e.filename+' ('+e.linenumber+')');
		}

		return result;
	}

	function setMainUsbDataInit(){
		for(var i=0; i<MAX_DEVICE_NUM; i++) {
			$('#sf-service-mediabrowser-main-item' + i + '-label').html('');
			$('#sf-service-mediabrowser-main-device-Icon'+i).removeClass();
			$('#sf-service-mediabrowser-main-usb-not_connected'+i).hide();
			$('#sf-service-mediabrowser-main-dlna-not_connected'+i).hide();
		}
		nDeviceIndex = 0;
		nDeviceFocus = 0;
		nUsbPageNum = 1;
		nUsbPageStartNum = 0;
		nMainScrollBodyTop = 0;
		$('#sf-service-mediabrowser-main-scroll-body').css({
			'top' : '0'
		});
	}

	function setFileDataInit() {
		if(bflag == true){
			fileview.removeFocus(nFocusRow,nFocusCol);
		}
		$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).removeClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
		$('#sf-service-mediabrowser-file-scroll-body').css({
			'top' : '0'
		});
		aObjectList = new Array();
		aFilepath = new Array();
		aUserFilepath = new Array();
		oSelectedPath = {};
		bAllCheck =true;
		nFileButtonFocus = 0;
		nFocusRow=0;
		nFocusCol=0;
		bMainPage = true;
		nFileScrollBodyTop = 0;
		sDefaultDir = null;
	}

	function setRefresh() {
		$('#sf-service-mediabrowser-file-loading').sfLoading('show');
		for(var i=0; i<MAX_ROW; i++) {
			for(var j=0; j<MAX_COL; j++) {
				$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-upperfolder-focus');
				$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-upperfolder');
				$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-folder-focus');
				$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-folder');
				$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-focus');
			}
		}
		$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).removeClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
		$('#sf-service-mediabrowser-file-category'+nFocusRow+'-item'+nFocusCol+'-label').addClass('sf-service-mediabrowser-file-category-ellipsis');
		$('#sf-service-mediabrowser-file-category'+nFocusRow+'-item'+nFocusCol+'-label').removeClass('sf-service-mediabrowser-file-label-focus');
		$('#sf-service-mediabrowser-file-category'+nFocusRow+'-item'+nFocusCol+'-label').css({
			'color' : '#ffffff',
			'font-size' : NOR_LABEL_FONTSIZE+'px',
			'width' : NOR_LABEL_WIDTH+'px',
			'height':NOR_LABEL_HEIGHT+'px'
		});
		$('#sf-service-mediabrowser-file-category'+nFocusRow+'-item'+nFocusCol+'-checkbox').css({
			'left' : CHECKBOX_LEFT+'px',
			'top' : CHECKBOX_TOP+'px'
		});
		$('#sf-service-mediabrowser-file-category'+nFocusRow+'-item'+nFocusCol+'-check').css({
			'left' : CHECK_LEFT+'px',
			'top' : CHECK_TOP+'px'
		});
		fileview.setInitFileView();
		nPageNum=1;
		nPageStartNum=1;
		bAllCheck =true;
		bFileFocusBox = false;
		nThumbnailIndex = 0;
		nThumbnailName = 0;
		nFocusRow=0;
		nFocusCol=0;
		if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
			for(key in oMadeThumbnailFile){
				delete oMadeThumbnailFile[key];
			}
			aObjectList = new Array();
			readUsbList(aFilepath);
		}
		fileview.showFileList(nPageStartNum);
		if(bflag == true){
			fileview.changeFocus(nFocusRow,nFocusCol);
		}


	}

	function ImageAppendTo(id, toDivObj) {
		//$('#'+id).add(toDivObj);
		var oId = (typeof id == 'string')? document.getElementById(id):id;
		if(typeof toDivObj == 'string'){
			oId.innerHTML += toDivObj;
		}
		else {
			oId.appendChild(toDivObj);
		}
	}

	function createImageObj(url, width, height) {
		var imgObj = null;
		imgObj = document.createElement('img');
		imgObj.style.position = 'absolute';
		imgObj.width = width;
		imgObj.height = height;
		imgObj.src = url;
		return imgObj;
	}


	function makeCheckList(){
		var nMoveToFirst = deviceapis._plugin('ContentsMgr', 'MoveToFirst');
	    alert("nMoveToFirst....................................................."+nMoveToFirst);
	    aCheckList = new Array();
	    var nCheckListNum = 0;
	    do{
	    	var nGetCurrentIndex = deviceapis._plugin('ContentsMgr', 'GetCurrentIndex');
			alert("nGetCurrentIndex....................................................."+nGetCurrentIndex);
			var bIsDIR = deviceapis._plugin('ContentsMgr', 'GetValue', 7);
			alert("bIsDIR....................................................."+bIsDIR);
			if(bIsDIR == FILE){
				aCheckList[nCheckListNum]= {
					filename : null,
					date : null,
					isDir : false,
					thumbnail : null,
					path : null,
					PlayTime : null,
					type : sPattern
				};


				aCheckList[nCheckListNum].path = deviceapis._plugin('ContentsMgr', 'GetValue', 46);
				aCheckList[nCheckListNum].filename = deviceapis._plugin('ContentsMgr', 'GetValue', 1);
				aCheckList[nCheckListNum].thumbnail = deviceapis._plugin('ContentsMgr', 'GetValue', 48);
				aCheckList[nCheckListNum].PlayTime = deviceapis._plugin('ContentsMgr', 'GetValue', 16);
				alert("aCheckList[nCheckListNum].path..........................!"+aCheckList[nCheckListNum].path);
				alert("aCheckList[nCheckListNum].filename..........................!"+aCheckList[nCheckListNum].filename);
				alert("aCheckList[nCheckListNum].thumbnail..........................!"+aCheckList[nCheckListNum].thumbnail);
				alert("aCheckList[nCheckListNum].PlayTime..........................!"+aCheckList[nCheckListNum].PlayTime);
				nCheckListNum++;
			}
			var nMoveToNext = deviceapis._plugin('ContentsMgr', 'MoveToNext');
			alert("nMoveToNext.........................................."+nMoveToNext);
			var nGetCurrentIndex = deviceapis._plugin('ContentsMgr', 'GetCurrentIndex');
			alert("nGetCurrentIndex2..........................k..........................."+nGetCurrentIndex);
		}while(nMoveToNext != -1);
	}

	function makeFileObj(listNum){
		alert("listNum......................................................."+listNum);
		var nMoveToIndex = deviceapis._plugin('ContentsMgr', 'MoveToIndex',listNum-1);
		alert("nMoveToIndex......................................................."+nMoveToIndex);
		var nMaxListNum = listNum + MAX_PAGEITEMNUM;
		do{
			var nGetCurrentIndex = deviceapis._plugin('ContentsMgr', 'GetCurrentIndex');
			alert("nGetCurrentIndex....................................................."+nGetCurrentIndex);
			var bIsDIR = deviceapis._plugin('ContentsMgr', 'GetValue', 7);
			alert("bIsDIR....................................................."+bIsDIR);
			if(bIsDIR == UPPERFOLDER || bIsDIR == FOLDER ){
				aObjectList[listNum]= {
					filename : null,
					date : null,
					isDir : true,
					thumbnail : null,
					path : null,
					PlayTime : null,
					type : sPattern
				};
			}
			else if(bIsDIR == FILE){
				aObjectList[listNum]= {
					filename : null,
					date : null,
					isDir : false,
					thumbnail : null,
					path : null,
					PlayTime : null,
					type : sPattern
				};
			}

			aObjectList[listNum].path = deviceapis._plugin('ContentsMgr', 'GetValue', 46);
			aObjectList[listNum].filename = deviceapis._plugin('ContentsMgr', 'GetValue', 1);
			aObjectList[listNum].thumbnail = deviceapis._plugin('ContentsMgr', 'GetValue', 48);
			aObjectList[listNum].PlayTime = deviceapis._plugin('ContentsMgr', 'GetValue', 16);

			alert("listNum4.........................."+listNum);
			alert("aObjectList[nNumFile].path..........................!"+aObjectList[listNum].path);
			alert("aObjectList[nNumFile].filename..........................!"+aObjectList[listNum].filename);
			alert("aObjectList[nNumFile].thumbnail..........................!"+aObjectList[listNum].thumbnail);
			alert("aObjectList[listNum].PlayTime..........................!"+aObjectList[listNum].PlayTime);
			listNum++;
			var nMoveToNext = deviceapis._plugin('ContentsMgr', 'MoveToNext');
			alert("nNumFile2.........................."+listNum);
			alert("nMoveToNext.........................................."+nMoveToNext);
			var nGetCurrentIndex = deviceapis._plugin('ContentsMgr', 'GetCurrentIndex');
			alert("nGetCurrentIndex2....................................................."+nGetCurrentIndex);
		}while(nMoveToNext != -1 && nGetCurrentIndex < nMaxListNum-1);
		setTimeout( function() {
			$('#sf-service-mediabrowser-main-loading').sfLoading('hide');
		},10)
		alert("makeFileObjDONE.....................................................");
	}

	function makeThumbnail(path){ // 처음 만들어질 썸네일
		bKeyblock = true;
		alert("makeThumbnail............................................................"+path);
		var ThumbnailRequest = deviceapis._plugin('ContentsMgr', 'ThumbnailRequest', path,nfileType,THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
		alert("ThumbnailRequest......................................................"+ThumbnailRequest);
	}

	function OnEvent(event, data1, data2){
	    alert('OnEvent()');
	    alert('event : ' + event);
	    alert('data1 : ' + data1);
	    alert('data2 : ' + data2);
	    if(data1 == 'COMPLETE_THUMBNAIL'){   // RequestThumbnail 완료
	    	alert("COMPLETE_THUMBNAIL........................................................");
	    	var GetThumbnailPath = deviceapis._plugin('ContentsMgr', 'GetThumbnailPath',nThumbnailIndex);
	    	alert("GetThumbnailPath....................................................................."+GetThumbnailPath);
	    	if(GetThumbnailPath == -1){ //리퀘스트를 한번더
	    		alert("nThumbnailRow.........................................."+nThumbnailCategoryRow);
	    		alert("nThumbnailCol.........................................."+nThumbnailCategoryCol);
	    		var ThumbnailRequest = deviceapis._plugin('ContentsMgr', 'ThumbnailRequest',aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol],nfileType,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
	    		alert("ThumbnailRequest.........................................."+ThumbnailRequest);
	    		alert("Request Fail...............!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	    	}
	    	else {
	    		var ntime = new Date();
		    	var nNow = ntime.getTime();
		    	alert("nThumbnailRow....................................."+nThumbnailCategoryRow);
		    	alert("nThumbnailCol....................................."+nThumbnailCategoryCol);
		    	aThumbnailFileTable[nThumbnailCategoryRow][nThumbnailCategoryCol] = nThumbnailIndex+'.bmp?'+ntime; // 만들어질 썸네일 파일 명 결정
		    	alert("aThumbnailTable[nThumbnailRow][nThumbnailCol]....................................."+aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol]);
		    	alert("aThumbnailFileTable[nThumbnailRow][nThumbnailCol]....................................."+aThumbnailFileTable[nThumbnailCategoryRow][nThumbnailCategoryCol]);
	    	}
	    	alert("END___COMPLETE_THUMBNAIL........................................................");
	    }
	    else if(data1 == 'MAKE_THUMBNAIL' || data1 == 'FAIL_THUMBNAIL' ){ // getThumbnail 완료
	    	alert("MAKE_THUMBNAIL.......................................................");
	    	alert("nThumbnailCategoryRow....................................."+nThumbnailCategoryRow);
	    	alert("nThumbnailCategoryCol....................................."+nThumbnailCategoryCol);
	    	if(oMadeThumbnailFile[aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol]] === undefined) {
	    		oMadeThumbnailFile[aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol]] = {
		    		ThumbnailFileName:aThumbnailFileTable[nThumbnailCategoryRow][nThumbnailCategoryCol]
		    	};
		    	nThumbnailName = aThumbnailFileTable[nThumbnailCategoryRow][nThumbnailCategoryCol];
		    	alert("nThumbnailName1............................................................"+nThumbnailName);

		    	var sThumbnaileFilePath = THUMBNAILDEFAULTPATH + nThumbnailName;
		    	var sThumbnailObj = null;



		    	if(data1 == 'MAKE_THUMBNAIL' ){
		    		alert("MAKE_THUMBNAIL...............................................");
		    		if(bThumbnailPageChangeFlag == true && nThumbnailPageChangeRow == nThumbnailCategoryRow && nThumbnailPageChangeCol == nThumbnailCategoryCol) { //포커스가 간 위치일 경우 큰 이미지 생성
			    		$('#sf-service-mediabrowser-file-category'+nThumbnailCategoryRow+'-image'+nThumbnailCategoryCol).removeClass('sf-service-mediabrowser-file-category-image-defaultVideo');
			    		sThumbnailObj = createImageObj(sThumbnaileFilePath, THUMBNAIL_FOCUS_WIDTH, THUMBNAIL_FOCUS_HEIGHT);
						ImageAppendTo('sf-service-mediabrowser-file-category'+nThumbnailCategoryRow+'-image'+nThumbnailCategoryCol, sThumbnailObj);
						bThumbnailPageChangeFlag = false;
			    	}
			    	else {
			    		$('#sf-service-mediabrowser-file-category'+nThumbnailCategoryRow+'-image'+nThumbnailCategoryCol).removeClass('sf-service-mediabrowser-file-category-image-defaultVideo');
			    		sThumbnailObj = createImageObj(sThumbnaileFilePath, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
						ImageAppendTo('sf-service-mediabrowser-file-category'+nThumbnailCategoryRow+'-image'+nThumbnailCategoryCol, sThumbnailObj);
			    	}
		    	}
		    	else if(data1 == 'FAIL_THUMBNAIL') {
		    		alert("FAIL_THUMBNAIL...............................................");
		    		$('#sf-service-mediabrowser-file-category'+nThumbnailCategoryRow+'-image'+nThumbnailCategoryCol).removeClass();
		    		$('#sf-service-mediabrowser-file-category'+nThumbnailCategoryRow+'-image'+nThumbnailCategoryCol).addClass('sf-service-mediabrowser-file-category-image-defaultVideo');
		    	}

		    	nThumbnailCategoryCol++;
		    	if(nThumbnailCategoryCol >= MAX_COL){	//Table 의 길이에 따라 옮겨질 row/col 값 결정
		    		nThumbnailCategoryRow++;
		    		if(nThumbnailCategoryRow >= aThumbnailTable.length){
		    			nThumbnailCategoryRow--;
		    		}
		    		else {
		    			nThumbnailCategoryCol = 0;
		    		}

		    	}
		    	if(aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol] != undefined){ //썸네일로 만들 파일의 유/무를 판단
		    		alert("aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol]................................................................."+aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol]);
		    		alert("nfileType................................................................."+nfileType);
		    		alert("THUMBNAIL_WIDTH................................................................."+THUMBNAIL_WIDTH);
		    		alert("THUMBNAIL_HEIGHT................................................................."+THUMBNAIL_HEIGHT);
		    		var ThumbnailRequest = deviceapis._plugin('ContentsMgr', 'ThumbnailRequest',aThumbnailTable[nThumbnailCategoryRow][nThumbnailCategoryCol],nfileType,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
		    		alert("ThumbnailRequest////////////////////////////////////////////////////////////////////////"+ThumbnailRequest);
		    		nThumbnailIndex++;
		    	}
		    	else {
		    		setTimeout( function() {
						$('#sf-service-mediabrowser-file-loading').sfLoading('hide');
					},10)
		    		bKeyblock = false;
	    		}
	    	}
	    	else{
	    		setTimeout( function() {
					$('#sf-service-mediabrowser-file-loading').sfLoading('hide');
				},10)
	    		bKeyblock = false;
	    	}
	    	alert("END___MAKE_THUMBNAIL........................................................");
	    }

	    else if(data1 == 'ADD_DEVICE' || data1 == 'REMOVE_DEVICE'){
	    	alert("DEVICE EVENT........................................................");
	    	setMainUsbDataInit();
			aDeviceNum = new Array();
			readUsb();
			mainview.setUsbListView(nUsbPageStartNum);
	    }

	    else if(data1 == 'COMPLETE_CLUSTERING'){
	    	var nMoveToLast = deviceapis._plugin('ContentsMgr', 'MoveToLast');
	    	alert("nMoveToLast....................................................."+nMoveToLast);
	    	nGetLastIndex = deviceapis._plugin('ContentsMgr', 'GetCurrentIndex');
	    	alert("nGetLastIndex....................................................."+nGetLastIndex);
	    	var nMoveToFirst = deviceapis._plugin('ContentsMgr', 'MoveToFirst');
	    	alert("nMoveToFirst....................................................."+nMoveToFirst);
	    	var nNumFile = 1;
	    	aObjectList = new Array();
	    	makeFileObj(nNumFile);
	    	alert("test.....................................................");
			var nCateNum = Math.ceil((nGetLastIndex+1)/MAX_COL);
			if(nCateNum <=4) {
				nMaxPageNum = 1;
				$("#sf-service-mediabrowser-file-scroll").hide();
			} else {
				nMaxPageNum = (nCateNum - 4 )+1;	//fileview 의 네비게이터 정책상 스크롤의 크기를 구함. 4행(1),5행(2), 6행(3), 7행(4)...
				$("#sf-service-mediabrowser-file-scroll").show();
			}
			setFileScroll(nMaxPageNum);
			if(bMainPage == true){
				sViewControl = 'file';
				fileview.show();
			}
			bMainPage = false;
			setRefresh();
	    }
	}

	function MainView() {
		$('<div class="sf-service-mediabrowser-main"></div>').html([
		'<div class="sf-service-mediabrowser-main-bg">'+
		'<div id="sf-service-mediabrowser-main-bg">'+
		'<div id="sf-service-mediabrowser-main-bg-bottom"></div>'+
		'<div id="sf-service-mediabrowser-main-title"></div>'+
		'<div id="sf-service-mediabrowser-main-keyhelp"></div>'+
		'<div class="sf-service-mediabrowser-main-text">'+
		'<div class="sf-service-mediabrowser-main-usb-focus"></div>'+
		'<div id="sf-service-mediabrowser-main-text-line0"></div>'+
		'<div id="sf-service-mediabrowser-main-text-line1"></div>'+
		'<div id="sf-service-mediabrowser-main-item0">'+
		'<div id="sf-service-mediabrowser-main-item0-label"/></div>'+
		'<div id="sf-service-mediabrowser-main-item1">'+
		'<div id="sf-service-mediabrowser-main-item1-label"/></div>'+
		'<div id="sf-service-mediabrowser-main-item2">'+
		'<div id="sf-service-mediabrowser-main-item2-label"/></div>'+
		'<div id="sf-service-mediabrowser-main-device-Icon0"></div>'+
		'<div id="sf-service-mediabrowser-main-device-Icon1"></div>'+
		'<div id="sf-service-mediabrowser-main-device-Icon2"></div>'+
		'<div id="sf-service-mediabrowser-main-usb-not_connected0"></div>'+
		'<div id="sf-service-mediabrowser-main-usb-not_connected1"></div>'+
		'<div id="sf-service-mediabrowser-main-usb-not_connected2"></div>'+
		'<div id="sf-service-mediabrowser-main-dlna-not_connected0"></div>'+
		'<div id="sf-service-mediabrowser-main-dlna-not_connected1"></div>'+
		'<div id="sf-service-mediabrowser-main-dlna-not_connected2"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-main-Button-highlight0"></div>'+
		'<div class="sf-service-mediabrowser-main-Button-highlight1"></div>'+
		'<div class="sf-service-mediabrowser-main-Button-highlight2"></div>'+
		'<div class="sf-service-mediabrowser-main-Button-Icon0"></div>'+
		'<div class="sf-service-mediabrowser-main-Button-Icon1"></div>'+
		'<div class="sf-service-mediabrowser-main-Button-Icon2"></div>'+
		'<div class="sf-service-mediabrowser-main-list-panel-bg">'+
		'<div class="sf-service-mediabrowser-main-Button0">'+sf.lang.SID_PHOTOS+'</div>'+
		'<div class="sf-service-mediabrowser-main-Button1">'+sf.lang.SID_VIDEOS+'</div>'+
		'<div class="sf-service-mediabrowser-main-Button2">'+sf.lang.SID_MUSIC_DIF_KR+'</div>'+
		'</div>'+
		'<div id="sf-service-mediabrowser-main-loading"></div>'+
		'<div id="sf-service-mediabrowser-main-scroll">'+
		'<div id="sf-service-mediabrowser-main-scroll-body">'+
		'<div id="sf-service-mediabrowser-main-scroll-body-top"></div>'+
		'<div id="sf-service-mediabrowser-main-scroll-body-middle"></div>'+
		'<div id="sf-service-mediabrowser-main-scroll-body-bottom"></div>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div>'
		].join('')).appendTo('body')

		var label1 = '';
		var label2 = '';
		var label3 = '';

		$('#sf-service-mediabrowser-main-label').sfLabel({
			text: ''
		});

		$('#sf-service-mediabrowser-main-item0-label').sfLabel({
			text: label1
		});

		$('#sf-service-mediabrowser-main-item1-label').sfLabel({
			text: label2
		});

		$('#sf-service-mediabrowser-main-item2-label').sfLabel({
			text: label3
		});

		$('#sf-service-mediabrowser-main-keyhelp').sfKeyHelp({
			'move': sf.lang.SID_MOVE,
			'enter': sf.lang.SID_SELECT,
			'return': sf.lang.SID_RETURN
		});

		this.show = function() {
			//nButtonFocus=0;
			if(!inArray(sf.service.MediaBrowser.MEDIATYPE_PHOTO,opts.mediaType)){
				nButtonFocus=1;
				if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE,opts.mediaType)){
					nButtonFocus=2;
				}
			}
			else{
				nButtonFocus=0;
			}
			bMainFocusBox = false;
			$('.sf-service-mediabrowser-main').show();
			$('.sf-service-mediabrowser-main-Button'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-focus');
			$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
			$('.sf-service-mediabrowser-main-Button'+nButtonFocus).show();
			$('.sf-service-mediabrowser-main-Button-highlight0').hide();
			$('.sf-service-mediabrowser-main-Button-highlight1').hide();
			$('.sf-service-mediabrowser-main-Button-highlight2').hide();
			$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).show();
			$('.sf-service-mediabrowser-main-usb-focus').hide();
		}
		this.setInitMainView = function() {
			for(var i=0; i<MAX_DEVICE_NUM; i++) {
				$('#sf-service-mediabrowser-main-item' + i + '-label').html('');
				$('#sf-service-mediabrowser-main-device-Icon'+i).hide()
			}
			nButtonFocus=0;
			nDeviceFocus = 0;
			aDeviceNum = new Array();
			bMainFocusBox = false;
		}
		this.setUsbListView = function(listNum) {
			var max_page=listNum+MAX_DEVICE_NUM;
			var nItem = 0;

			if (max_page > aDeviceNum.length) {
				max_page=aDeviceNum.length;
			}
			for(var i=listNum; i<max_page; i++) {
				nItem = (i % MAX_DEVICE_NUM);
				$('#sf-service-mediabrowser-main-item' + nItem + '-label').html('' + aDeviceNum[i].vendorName);
				if(aDeviceNum[i].deviceType == 'USB'){
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).removeClass();
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).addClass('sf-service-mediabrowser-main-usb-norIcon');
				}
				else if(aDeviceNum[i].deviceType == 'DLNA'){
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).removeClass();
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).addClass('sf-service-mediabrowser-main-dlna-norIcon');
				}
				else if(aDeviceNum[i].deviceType == 'USBNONE') {
					alert("sample---------------------------------------------------------------------1");
					$('#sf-service-mediabrowser-main-usb-not_connected'+nItem).html(''+sf.lang.SID_USB_IS_NOT_CONNECTED);
					$('#sf-service-mediabrowser-main-usb-not_connected'+nItem).show();
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).removeClass();
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).addClass('sf-service-mediabrowser-main-usb-NonIcon');
				}
				else if(aDeviceNum[i].deviceType == 'DLNANONE') {
					alert("sample---------------------------------------------------------------------2");
					$('#sf-service-mediabrowser-main-dlna-not_connected'+nItem).html(''+sf.lang.TV_SID_NO_ALLSHARE_CONNECTED_ENJOY_CONTENT_MSG);
					$('#sf-service-mediabrowser-main-dlna-not_connected'+nItem).show();
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).removeClass();
					$('#sf-service-mediabrowser-main-device-Icon'+nItem).addClass('sf-service-mediabrowser-main-dlna-NonIcon');
				}
				$('#sf-service-mediabrowser-main-device-Icon'+nItem).show()

			}
		}
		this.moveMainScroll = function(Index) {
			if(Index == 'prev') {
				nMainScrollBodyTop =nMainScrollBodyTop-nMainScrollMove;
				$('#sf-service-mediabrowser-main-scroll-body').css({
					'top' : nMainScrollBodyTop
				});
				if(nMainScrollBodyTop < 0 ) {
					$('#sf-service-mediabrowser-main-scroll-body').css({
						'top' : '0'
					});
				}
			} else if(Index == 'next') {
				nMainScrollBodyTop =nMainScrollBodyTop+nMainScrollMove;
				$('#sf-service-mediabrowser-main-scroll-body').css({
					'top' : nMainScrollBodyTop
				});
				if(nMainScrollBodyTop >= nMainScrollHeight) {
					nMainScrollBodyTop = nMainScrollBodyTop-nMainScrollMove;
				}
			}
		}
	}

	function FileView() {
		$('<div class="sf-service-mediabrowser-file"></div>').html([
		'<div class="sf-service-mediabrowser-file-bg">'+
		'<div id="sf-service-mediabrowser-file-bg">'+
		'<div id="sf-service-mediabrowser-file-bg-bottom"></div>'+
		'<div id="sf-service-mediabrowser-file-path">'+sf.lang.SID_PATH+':&nbsp;'+'</div>'+
		'<div id="sf-service-mediabrowser-file-title"></div>'+
		'<div id="sf-service-mediabrowser-file-keyhelp"></div>'+
		'<div class="sf-service-mediabrowser-file-Button0">'+sf.lang.SID_OK+'</div>'+
		'<div class="sf-service-mediabrowser-file-Button1">'+sf.lang.SID_CANCEL+'</div>'+
		'<div class="sf-service-mediabrowser-file-Button2">'+sf.lang.SID_SELECT_ALL+'</div>'+
		'<div id="sf-service-mediabrowser-file-nofile"></div>'+
		'<div id="sf-service-mediabrowser-file-bg-mediaBox">'+
		'<div id="sf-service-mediabrowser-file-category0">'+
		'<div class="sf-service-mediabrowser-file-category0-item0">'+
		'<div id="sf-service-mediabrowser-file-category0-image0"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item0-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item0-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item0-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category0-item1">'+
		'<div id="sf-service-mediabrowser-file-category0-image1"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item1-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item1-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item1-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category0-item2">'+
		'<div id="sf-service-mediabrowser-file-category0-image2"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item2-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item2-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item2-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category0-item3">'+
		'<div id="sf-service-mediabrowser-file-category0-image3"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item3-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item3-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item3-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category0-item4">'+
		'<div id="sf-service-mediabrowser-file-category0-image4"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item4-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item4-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item4-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category0-item5">'+
		'<div id="sf-service-mediabrowser-file-category0-image5"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item5-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item5-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category0-item5-check"></div>'+
		'</div>'+
		'</div>'+
		'<div id="sf-service-mediabrowser-file-category1">'+
		'<div class="sf-service-mediabrowser-file-category1-item0">'+
		'<div id="sf-service-mediabrowser-file-category1-image0"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item0-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item0-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item0-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category1-item1">'+
		'<div id="sf-service-mediabrowser-file-category1-image1"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item1-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item1-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item1-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category1-item2">'+
		'<div id="sf-service-mediabrowser-file-category1-image2"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item2-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item2-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item2-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category1-item3">'+
		'<div id="sf-service-mediabrowser-file-category1-image3"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item3-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item3-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item3-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category1-item4">'+
		'<div id="sf-service-mediabrowser-file-category1-image4"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item4-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item4-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item4-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category1-item5">'+
		'<div id="sf-service-mediabrowser-file-category1-image5"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item5-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item5-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category1-item5-check"></div>'+
		'</div>'+
		'</div>'+
		'<div id="sf-service-mediabrowser-file-category2">'+
		'<div class="sf-service-mediabrowser-file-category2-item0">'+
		'<div id="sf-service-mediabrowser-file-category2-image0"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item0-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item0-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item0-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category2-item1">'+
		'<div id="sf-service-mediabrowser-file-category2-image1"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item1-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item1-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item1-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category2-item2">'+
		'<div id="sf-service-mediabrowser-file-category2-image2"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item2-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item2-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item2-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category2-item3">'+
		'<div id="sf-service-mediabrowser-file-category2-image3"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item3-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item3-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item3-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category2-item4">'+
		'<div id="sf-service-mediabrowser-file-category2-image4"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item4-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item4-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item4-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category2-item5">'+
		'<div id="sf-service-mediabrowser-file-category2-image5"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item5-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item5-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category2-item5-check"></div>'+
		'</div>'+
		'</div>'+
		'<div id="sf-service-mediabrowser-file-category3">'+
		'<div class="sf-service-mediabrowser-file-category3-item0">'+
		'<div id="sf-service-mediabrowser-file-category3-image0"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item0-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item0-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item0-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category3-item1">'+
		'<div id="sf-service-mediabrowser-file-category3-image1"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item1-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item1-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item1-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category3-item2">'+
		'<div id="sf-service-mediabrowser-file-category3-image2"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item2-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item2-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item2-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category3-item3">'+
		'<div id="sf-service-mediabrowser-file-category3-image3"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item3-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item3-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item3-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category3-item4">'+
		'<div id="sf-service-mediabrowser-file-category3-image4"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item4-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item4-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item4-check"></div>'+
		'</div>'+
		'<div class="sf-service-mediabrowser-file-category3-item5">'+
		'<div id="sf-service-mediabrowser-file-category3-image5"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item5-label"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item5-checkbox"></div>'+
		'<div id="sf-service-mediabrowser-file-category3-item5-check"></div>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'<div id="sf-service-mediabrowser-file-loading"></div>'+
		'<div id="sf-service-mediabrowser-file-scroll">'+
		'<div id="sf-service-mediabrowser-file-scroll-body">'+
		'<div id="sf-service-mediabrowser-file-scroll-body-top"></div>'+
		'<div id="sf-service-mediabrowser-file-scroll-body-middle"></div>'+
		'<div id="sf-service-mediabrowser-file-scroll-body-bottom"></div>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div>'
		].join('')).appendTo('body')

		$('#sf-service-mediabrowser-file-keyhelp').sfKeyHelp({
			'move': sf.lang.SID_MOVE,
			'enter': sf.lang.SID_SELECT,
			'return': sf.lang.SID_RETURN
		});

		this.show = function() {
			$('.sf-service-mediabrowser-file').show();
			$('#sf-service-mediabrowser-file-path').addClass('sf-service-mediabrowser-file-category-ellipsis');
			if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
				$('#sf-service-mediabrowser-file-title').html(sf.lang.SID_USB);
			}
			else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
				$('#sf-service-mediabrowser-file-title').html(sf.lang.SID_DLNA);
			}



			for(var i=0; i<MAX_ROW; i++) {
				for(var j=0; j<MAX_COL; j++) {
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-checkbox').hide();
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-check').hide();
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-label').addClass('sf-service-mediabrowser-file-category-ellipsis');
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-label').html('');
				}
			}
			setTimeout( function() {
				$('#sf-service-mediabrowser-main-loading').sfLoading('hide');
			},10)
		}
		this.setInitFileView = function() {
			for(var i=0; i<MAX_ROW; i++) {
				for(var j=0; j<MAX_COL; j++) {
					$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-upperfolder-focus');
					$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-upperfolder');
					$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-folder-focus');
					$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-folder');
					$('.sf-service-mediabrowser-file-category'+i+'-item'+j).removeClass('sf-service-mediabrowser-file-category-focus');
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-checkbox').hide();
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-check').hide();
					$('.sf-service-mediabrowser-file-category'+i+'-item'+j).hide();
					$('#sf-service-mediabrowser-file-category'+i+'-item'+j+'-label').html('');
					$('#sf-service-mediabrowser-file-category'+i+'-image'+j).html('');
					$('#sf-service-mediabrowser-file-category'+i+'-image'+j).css({
						'top' : NOR_IMAGE_TOP+'px',
						'left' : NOR_IMAGE_LEFT+'px',
						'width' : THUMBNAIL_WIDTH+'px',
						'height':THUMBNAIL_HEIGHT+'px'
					});
				}
			}
			nThumbnailIndex = 0;
			nThumbnailName = 0;
		}

		this.showFileList = function(listpage) {
			nMaxCategoryNum = Math.ceil(aObjectList.length/MAX_COL);
			var filePath = aFilepath.join('');
			var fileFullPath = null;
			var nFileListLength = aObjectList.length;
			bThumbnailFlag = true;
			bThumbnailTableStartFlage = true;
			nThumbnailRow = 0;
			nThumbnailCol = 0;
			aFileTable = new Array();
			aThumbnailTable = new Array();
			aThumbnailFileTable = new Array();

			if(aFilepath.join('')==sDefaultDir) {
				listpage++;// sDefaultDir 일 경우 상위폴더로 가는  '..' 폴더를 숨기기 위하여 listpage 보다 하나 큰 값을 넣는다.
				if(aObjectList.length == 2){
					bflag = false;
					$('#sf-service-mediabrowser-file-nofile').html(NOFILE);
					$('#sf-service-mediabrowser-file-Button0').hide();
					$('#sf-service-mediabrowser-file-Button1').hide();
					$('#sf-service-mediabrowser-file-Button2').hide();
					alert("test...............................444444444");
				}
				else{
					bflag = true;
					$('#sf-service-mediabrowser-file-nofile').html('');
					for (var row=0; row<MAX_ROW; row++) {
						aFileTable[row] = new Array();
						aThumbnailTable[row] = new Array();
						aThumbnailFileTable[row] = new Array();
						for(var col=0; col<MAX_COL ; col++) {
							alert('listpage1--------------'+listpage);
							alert('nFileListLength--------------'+nFileListLength);
							alert('col1------------------------'+col);
							if(listpage >= nFileListLength) { //read한 파일 및 폴더의 마지막일 경우 break
								break;
							}
							aFileTable[row][col]=aObjectList[listpage];
							if(aFilepath.join('')!=sDefaultDir && aFileTable[row][col].filename == '..') {
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-label').html(''+sf.lang.SID_UPPER_FOLDER);
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).addClass('sf-service-mediabrowser-file-category-upperfolder');
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).show();
							} else if (aFileTable[row][col].isDir) {
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-label').html(''+aFileTable[row][col].filename);
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).addClass('sf-service-mediabrowser-file-category-folder');
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).show();
							} else {
								if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
									fileFullPath = filePath+aFileTable[row][col].filename;
									aThumbnailTable[row][col] = fileFullPath.replace('$usb_DIR','/dtv/usb');
									if(oMadeThumbnailFile[aThumbnailTable[row][col]] !== undefined){
										if(bThumbnailTableStartFlage == true){
											nThumbnailTableStartRow = row;
											nThumbnailTableStartCol = col;
											bThumbnailTableStartFlage = false;
										}
										alert("oMadeThumbnailFile////////////////////////////////////////////////////");
										var key = aThumbnailTable[row][col];
										if(key in oMadeThumbnailFile){
											var sThumbnailFilePath = THUMBNAILDEFAULTPATH+oMadeThumbnailFile[key].ThumbnailFileName;
										}
										var ImgObj = createImageObj(sThumbnailFilePath, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
										ImageAppendTo('sf-service-mediabrowser-file-category'+row+'-image'+col, ImgObj);
									}
									else {
										alert("testROW............................................"+row);
										alert("testCOL............................................"+col);
										alert("aThumbnailTable[row][col]................"+aThumbnailTable[row][col]);
										if(bThumbnailFlag == true) { // 첫번째 파일의 시작 위치를 확인하기 위한 Flag
											nThumbnailRow = row;
											nThumbnailCol = col;
											nThumbnailCategoryRow = row;
											nThumbnailCategoryCol = col;
											bThumbnailFlag = false;
										}
									}
								} else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA') {
									aThumbnailTable[row][col] = aFileTable[row][col].thumbnail;
									alert('aThumbnailTable[row][col].......................................'+aThumbnailTable[row][col]);
									if(oMadeThumbnailFile[aThumbnailTable[row][col]] !== undefined){
										if(bThumbnailTableStartFlage == true){
											nThumbnailTableStartRow = row;
											nThumbnailTableStartCol = col;
											bThumbnailTableStartFlage = false;
										}
										alert("oMadeThumbnailFile////////////////////////////////////////////////////");
										var key = fileFullPath;
										if(key in oMadeThumbnailFile){
											var sThumbnailFilePath = THUMBNAILDEFAULTPATH+oMadeThumbnailFile[key].ThumbnailFileName;
										}
										var ImgObj = createImageObj(sThumbnailFilePath, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
										ImageAppendTo('sf-service-mediabrowser-file-category'+row+'-image'+col, ImgObj);
									}
									else {
										alert("testROW............................................"+row);
										alert("testCOL............................................"+col);
										alert("aThumbnailTable[row][col]................"+aThumbnailTable[row][col]);
										if(bThumbnailFlag == true) { // 첫번째 파일의 시작 위치를 확인하기 위한 Flag
											nThumbnailRow = row;
											nThumbnailCol = col;
											nThumbnailCategoryRow = row;
											nThumbnailCategoryCol = col;
											bThumbnailFlag = false;
										}
									}
								}
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-label').html(''+aFileTable[row][col].filename);
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).show();
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-checkbox').show();
								fileview.showChecked(aFilepath,row,col);//폴더간 이동시에 이미 체크 되었던 파일을 다시 체크 표시 하기 위하여  수행한다.
							}
							listpage++;
						}
						if(listpage >= nFileListLength) {
							break;
						}
					}
				}

			} else { // sDefaultDir 가 아닐 경우 상위폴더로가는 '..' 표현
				if(aObjectList.length < 1){
					bflag = false;
					$('#sf-service-mediabrowser-file-nofile').html(NOFILE);
					$('#sf-service-mediabrowser-file-Button0').hide();
					$('#sf-service-mediabrowser-file-Button1').hide();
					$('#sf-service-mediabrowser-file-Button2').hide();
				}
				else {
					for (var row=0; row<MAX_ROW; row++) {
						aFileTable[row] = new Array();
						aThumbnailTable[row] = new Array();
						aThumbnailFileTable[row] = new Array();
						for(var col=0; col<MAX_COL; col++) {
							if(listpage >= nFileListLength) { //read한 파일 및 폴더의 마지막일 경우 break
								break;
							}
							aFileTable[row][col]=aObjectList[listpage];
							if(aFilepath.join('')!=sDefaultDir && aFileTable[row][col].filename == '..') {
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-label').html(''+sf.lang.SID_UPPER_FOLDER);
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).addClass('sf-service-mediabrowser-file-category-upperfolder');
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).show();
							} else if (aFileTable[row][col].isDir) {
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-label').html(''+aFileTable[row][col].filename);
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).addClass('sf-service-mediabrowser-file-category-folder');
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).show();
							} else {
								if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
									fileFullPath = filePath+aFileTable[row][col].filename;
									aThumbnailTable[row][col] = fileFullPath.replace('$usb_DIR','/dtv/usb');
									if(oMadeThumbnailFile[aThumbnailTable[row][col]] !== undefined){
										var key = aThumbnailTable[row][col];
										if(key in oMadeThumbnailFile){
											var sThumbnailFilePath = THUMBNAILDEFAULTPATH+oMadeThumbnailFile[key].ThumbnailFileName;
											alert("sThumbnailFilePath................................................"+sThumbnailFilePath);
										}
										var ImgObj = createImageObj(sThumbnailFilePath, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
										ImageAppendTo('sf-service-mediabrowser-file-category'+row+'-image'+col, ImgObj);
									}
									else {
										alert("testROW............................................"+row);
										alert("testCOL............................................"+col);
										alert("aThumbnailTable[row][col]................"+aThumbnailTable[row][col]);
										if(bThumbnailFlag == true) { // 첫번째 파일의 시작 위치를 확인하기 위한 Flag
											nThumbnailRow = row;
											nThumbnailCol = col;
											nThumbnailCategoryRow = row;
											nThumbnailCategoryCol = col;
											bThumbnailFlag = false;
										}
									}
								} else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA') {
									aThumbnailTable[row][col] = aFileTable[row][col].thumbnail;
									alert('aThumbnailTable[row][col].......................................'+aThumbnailTable[row][col]);
									if(oMadeThumbnailFile[aThumbnailTable[row][col]] !== undefined){
										if(bThumbnailTableStartFlage == true){
											nThumbnailTableStartRow = row;
											nThumbnailTableStartCol = col;
											bThumbnailTableStartFlage = false;
										}
										alert("oMadeThumbnailFile////////////////////////////////////////////////////");
										var key = fileFullPath;
										if(key in oMadeThumbnailFile){
											var sThumbnailFilePath = THUMBNAILDEFAULTPATH+oMadeThumbnailFile[key].ThumbnailFileName;
										}
										var ImgObj = createImageObj(sThumbnailFilePath, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
										ImageAppendTo('sf-service-mediabrowser-file-category'+row+'-image'+col, ImgObj);
									}
									else {
										alert("testROW............................................"+row);
										alert("testCOL............................................"+col);
										alert("aThumbnailTable[row][col]................"+aThumbnailTable[row][col]);
										if(bThumbnailFlag == true) { // 첫번째 파일의 시작 위치를 확인하기 위한 Flag
											nThumbnailRow = row;
											nThumbnailCol = col;
											nThumbnailCategoryRow = row;
											nThumbnailCategoryCol = col;
											bThumbnailFlag = false;
										}
									}
								}
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-label').html(''+aFileTable[row][col].filename);
								$('.sf-service-mediabrowser-file-category'+row+'-item'+col).show();
								$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-checkbox').show();
								fileview.showChecked(aFilepath,row,col);//폴더간 이동시에 이미 체크 되었던 파일을 다시 체크 표시 하기 위하여  수행한다.
							}
							listpage++;
						}
						if(listpage >= nFileListLength) {
							break;
						}
					}
				}
			}

			if(aThumbnailTable[nThumbnailRow][nThumbnailCol] != undefined){
				alert("makeThumbnailTable.............................................."+aThumbnailTable[nThumbnailRow][nThumbnailCol]);
				makeThumbnail(aThumbnailTable[nThumbnailRow][nThumbnailCol]);
			}

			else{
				setTimeout( function() {
					$('#sf-service-mediabrowser-file-loading').sfLoading('hide');
				},10)
			}
		}
		this.moveFileScroll = function(Index) {
			if(Index == 'prev') {
				nFileScrollBodyTop =nFileScrollBodyTop-nFileScrollMove;
				$('#sf-service-mediabrowser-file-scroll-body').css({
					'top' : nFileScrollBodyTop
				});
				if(nFileScrollBodyTop < 0 ) {
					$('#sf-service-mediabrowser-file-scroll-body').css({
						'top' : '0'
					});
				}
			} else if(Index == 'next') {
				nFileScrollBodyTop =nFileScrollBodyTop+nFileScrollMove;
				$('#sf-service-mediabrowser-file-scroll-body').css({
					'top' : nFileScrollBodyTop
				});
				if(nFileScrollBodyTop >= nFileScrollHeight) {
					nFileScrollBodyTop =nFileScrollBodyTop-nFileScrollMove;
				}
			}

		}

		this.changeFocus= function(rowIndex,colIndex) {
			if(aFilepath.join('')!=sDefaultDir && aFileTable[rowIndex][colIndex].filename == '..') {
				$('.sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).addClass('sf-service-mediabrowser-file-category-upperfolder-focus');
			} else if (aFileTable[rowIndex][colIndex].isDir) {
				$('.sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).addClass('sf-service-mediabrowser-file-category-folder-focus');
			} else {
				$('.sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).addClass('sf-service-mediabrowser-file-category-focus');
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-image'+colIndex).html('');
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-image'+colIndex).css({
					'top' : FOCUS_IMAGE_TOP+'px',
					'left' : FOCUS_IMAGE_LEFT+'px',
					'width' : THUMBNAIL_FOCUS_WIDTH+'px',
					'height':THUMBNAIL_FOCUS_HEIGHT+'px'
				});

				var filePath = THUMBNAILDEFAULTPATH
				var sThumbnailPath = null;
				alert("aThumbnailTable[rowIndex][colIndex]....................................."+aThumbnailTable[rowIndex][colIndex]);
				if(oMadeThumbnailFile[aThumbnailTable[rowIndex][colIndex]] !== undefined){
					alert("test................................................................11");
					var key = aThumbnailTable[rowIndex][colIndex];
					alert("test................................................................22"+key);
					if(key in oMadeThumbnailFile){
						var sThumbnailFilePath = filePath+oMadeThumbnailFile[key].ThumbnailFileName;
						alert("sThumbnailFilePath................................................................11"+sThumbnailFilePath);
					}
					var imgObj = createImageObj(sThumbnailFilePath, THUMBNAIL_FOCUS_WIDTH, THUMBNAIL_FOCUS_HEIGHT);
					ImageAppendTo('sf-service-mediabrowser-file-category'+rowIndex+'-image'+colIndex, imgObj);
				}
			}

			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).show();
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').addClass('sf-service-mediabrowser-file-label-focus');
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').removeClass('sf-service-mediabrowser-file-category-ellipsis');
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').css({
				'color' : '#ffba19',
				'font-size' : FOCUS_LABEL_FONTSIZE+'px',
				'padding-top' : '6px',
				'width' : FOCUS_LABEL_WIDTH+'px',
				'height': FOCUS_LABEL_HEIGHT+'px'
			});

			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-checkbox').css({
				'left' : FOCUS_CHECKBOX_LEFT+'px',
				'top' : FOCUS_CHECKBOX_TOP+'px'
			});
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-check').css({
				'left' : FOCUS_CHECK_LEFT+'px',
				'top' : FOCUS_CHECK_TOP+'px'
			});
		}
		this.removeFocus = function(rowIndex,colIndex) {
			if(aFilepath.join('')!=sDefaultDir && aFileTable[rowIndex][colIndex].filename == '..') {
				$('.sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).removeClass('sf-service-mediabrowser-file-category-upperfolder-focus');
			} else if (aFileTable[rowIndex][colIndex].isDir) {
				$('.sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).removeClass('sf-service-mediabrowser-file-category-folder-focus');
			} else {
				$('.sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex).removeClass('sf-service-mediabrowser-file-category-focus');
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-image'+colIndex).html('');
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-image'+colIndex).css({
					'top' : NOR_IMAGE_TOP+'px',
					'left' : NOR_IMAGE_LEFT+'px',
					'width' : THUMBNAIL_WIDTH+'px',
					'height': THUMBNAIL_HEIGHT+'px'
				});

				var filePath = THUMBNAILDEFAULTPATH
				var sThumbnailPath = null;
				alert("Remove_aThumbnailTable[rowIndex][colIndex]....................................."+aThumbnailTable[rowIndex][colIndex]);
				if(oMadeThumbnailFile[aThumbnailTable[rowIndex][colIndex]] !== undefined){
					var key = aThumbnailTable[rowIndex][colIndex];
					if(key in oMadeThumbnailFile){
						var sThumbnailFilePath = filePath+oMadeThumbnailFile[key].ThumbnailFileName;
						alert("sThumbnailFilePath................................................................11"+sThumbnailFilePath);
					}
					imgObj = createImageObj(sThumbnailFilePath, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
					ImageAppendTo('sf-service-mediabrowser-file-category'+rowIndex+'-image'+colIndex, imgObj);
				}
			}


			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').addClass('sf-service-mediabrowser-file-category-ellipsis');
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').removeClass('sf-service-mediabrowser-file-label-focus');
			document.getElementById('sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').scrollLeft = 0;
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-label').css({
				'color' : '#ffffff',
				'font-size' : NOR_LABEL_FONTSIZE+'px',
				'width' : NOR_LABEL_WIDTH+'px',
				'height': NOR_LABEL_HEIGHT+'px'
			});
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-checkbox').css({
				'left' : CHECKBOX_LEFT+'px',
				'top' : CHECKBOX_TOP+'px'
			});
			$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-check').css({
				'left' : CHECK_LEFT+'px',
				'top' : CHECK_TOP+'px'
			});

		}
		this.showChecked = function (Path,rowIndex,colIndex) { //showFileList 수행시 check 여부 확인
			var PathSelected = Path;
			PathSelected = PathSelected.join('');
			alert("showChecked1.................................................."+PathSelected);
			var selectfilePath = PathSelected+'/'+aFileTable[rowIndex][colIndex].filename; //fullpath filename 생성
			if(oSelectedPath[selectfilePath] !== undefined) { // file이 객체에 존재하면 check
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-check').show();

			} else { //존재하지 않으면 uncheck
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-check').hide();
			}
		}
		this.setFileCheck = function (Path,rowIndex,colIndex) { //file enter 수행시 file check
			if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
				alert("Path......................................................"+Path);
				var PathSelected = Path;
				PathSelected = PathSelected.join('');
				alert("PathSelected......................................................"+PathSelected);
				var selectfilePath = PathSelected+'/'+aFileTable[rowIndex][colIndex].filename; //fullpath filename 생성
			} else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA') {
				var selectfilePath = aFileTable[rowIndex][colIndex].path; //fullpath filename 생성
			}

			if(oSelectedPath[selectfilePath] !== undefined) { // file이 객체에 존재하면 uncheck
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-check').hide();
				delete oSelectedPath[selectfilePath];
			} else { //존재하지 않으면 check
				oSelectedPath[selectfilePath] = {
					type:sPattern
				};
				$('#sf-service-mediabrowser-file-category'+rowIndex+'-item'+colIndex+'-check').show();
			}
		}
		this.setSelectAll = function (Path) { // path 안에 모든 file을 check/uncheck
			sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_TRUE);
			$('#sf-service-mediabrowser-file-loading').sfLoading('show');
			if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
				var PathSelected = Path;
				PathSelected = PathSelected.join('');
				for(var i=1; i<aObjectList.length; i++) { // path 내에 check 된 파일이 있으면 uncheck를 수행 하기 위함.
					if(!aObjectList[i].isDir) {
						var selectfilePath = PathSelected+'/'+aObjectList[i].filename;
						if(oSelectedPath[selectfilePath] !== undefined) { //file이 객체에 존재하면
							bAllCheck =false; // uncheck를 수행하게 한다.
						}
					}
				}
			} else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
				makeCheckList();
				for(var i=0; i<aCheckList.length; i++) { // path 내에 check 된 파일이 있으면 uncheck를 수행 하기 위함.
					var selectfilePath = aCheckList[i].path;
					if(oSelectedPath[selectfilePath] !== undefined) { //file이 객체에 존재하면
						bAllCheck =false; // uncheck를 수행하게 한다.
					}
				}
			}


			if(bAllCheck == true) {
				if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
					for(var i=1; i<aObjectList.length; i++) {
						if(!aObjectList[i].isDir) {
							var selectfilePath = PathSelected+'/'+aObjectList[i].filename;
							oSelectedPath[selectfilePath] = {
								type:sPattern
							};
						}
					}
				} else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
					for(var i=0; i<aCheckList.length; i++) { // path 내에 check 된 파일이 있으면 uncheck를 수행 하기 위함.
						var selectfilePath = aCheckList[i].path;
						oSelectedPath[selectfilePath] = { //file이 객체에 존재하면
							type:sPattern
						};
					}
				}

				for(var row=0; row<aFileTable.length; row++) {
					for(var col=0; col<aFileTable[row].length; col++) {
						if(!aFileTable[row][col].isDir) {
							alert('[row]----------------'+row);
							alert('[col]----------------'+col);
							$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-check').show();

						}
					}
				}
				bAllCheck = false;

			} else if(bAllCheck == false) {
				if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
					for(var i=1; i<aObjectList.length; i++) {
						if(!aObjectList[i].isDir) {
							var selectfilePath = PathSelected+'/'+aObjectList[i].filename;
							delete oSelectedPath[selectfilePath];
						}
					}
				} else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
					for(var i=0; i<aCheckList.length; i++) { // path 내에 check 된 파일이 있으면 uncheck를 수행 하기 위함.
						var selectfilePath = aCheckList[i].path;
						delete oSelectedPath[selectfilePath];
					}
				}
				for(var row=0; row<aFileTable.length; row++) {
					for(var col=0; col<aFileTable[row].length; col++) {
						if(!aFileTable[row][col].isDir) {
							$('#sf-service-mediabrowser-file-category'+row+'-item'+col+'-check').hide();

						}
					}
				}
				bAllCheck =true;
			}

			setTimeout( function() {
				$('#sf-service-mediabrowser-file-loading').sfLoading('hide');
			},10)

			//watchdog on
			sf.core.plugin('TVMW').SetWatchDog(sf.core.PLR_FALSE);
		}
	}

	var oSelectedPath = {};
	var ANCHOR_ID = 'sf-service-mediabrowser';
	var nButtonFocus = 0;
	var nDeviceFocus = 0; //하이라이트 위치
	var nDeviceIndex = 0; //USB 리스트 안에서 위치
	var bMainFocusBox = false;
	var nFocusRow = 0;
	var nFocusCol = 0;
	var MAX_CATEGORY_ROW = 3;
	var MAX_CATEGORY_COL = 5;
	var nPageNum = 1;
	var bAllCheck = true;
	var bFileFocusBox = false;
	var nFileButtonFocus =0;
	var nCategoryNum = 1;
	var nStateUsbNum = 0;
	var nMaxPageNum = 0;
	var nFileScrollBodyTop = 0;
	var nMainScrollBodyTop = 0;
	var sViewControl = 'main';



	sf.service.MediaBrowser.handleKeydown = function () {
		Control[sViewControl](event.keyCode);
	}
	var Control = {


		main : function (keyCode) {

			alert('[AF ui] sfPopup keyctl('+keyCode+')');
			switch (keyCode) {
				case sf.key.LEFT:
					if(bMainFocusBox == false) { //포커스가 button에 있을때
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-selIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-selIcon');
						}
						$('.sf-service-mediabrowser-main-usb-focus').addClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						$('.sf-service-mediabrowser-main-usb-focus').show();
						setFileType(nButtonFocus); //usb box로 포커스를 옮기면서 file Type을 결정한다.
						bMainFocusBox = true;
					} else if(bMainFocusBox == true) { //포커스가 usb box에 있을때
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-focus');
						$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).show();
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).show();
						$('.sf-service-mediabrowser-main-usb-focus').removeClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						$('.sf-service-mediabrowser-main-usb-focus').hide();
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-norIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-norIcon');
						}
						nDeviceIndex = nUsbPageStartNum;
						nDeviceFocus = 0;
						bMainFocusBox = false;
					}
					break;
				case sf.key.RIGHT:
					if(bMainFocusBox == false) { //포커스가 button에 있을때
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-selIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-selIcon');
						}
						$('.sf-service-mediabrowser-main-usb-focus').addClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						$('.sf-service-mediabrowser-main-usb-focus').show();
						setFileType(nButtonFocus); //usb box로 포커스를 옮기면서 file Type을 결정한다.
						bMainFocusBox = true;
					} else if(bMainFocusBox == true) { //포커스가 usb box에 있을때
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-focus');
						$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).show();
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).show();
						$('.sf-service-mediabrowser-main-usb-focus').removeClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						$('.sf-service-mediabrowser-main-usb-focus').hide();
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-norIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-norIcon');
						}
						nDeviceIndex = nUsbPageStartNum;
						nDeviceFocus = 0;
						bMainFocusBox = false;
					}
					break;
				case sf.key.UP:
					if(bMainFocusBox == false) { //포커스가 button에 있을때
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).removeClass('sf-service-mediabrowser-main-Button-focus');
						$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).removeClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
						if(nButtonFocus == 0){
							nButtonFocus = 0;
						}
						else if(nButtonFocus == 1){
							nButtonFocus = 0;
							if(!inArray(sf.service.MediaBrowser.MEDIATYPE_PHOTO,opts.mediaType))
							{
								nButtonFocus = 1;
							}
						}
						else if(nButtonFocus == 2){
							nButtonFocus = 1;
							if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE,opts.mediaType) && inArray(sf.service.MediaBrowser.MEDIATYPE_PHOTO,opts.mediaType)){
								nButtonFocus = 0;
							}
							else if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE,opts.mediaType) && !inArray(sf.service.MediaBrowser.MEDIATYPE_PHOTO,opts.mediaType))
							{
								nButtonFocus = 2;
							}
						}
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-focus');
						$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).show();
					} else if(bMainFocusBox == true) {  //포커스가 usb box에 있을때
						$('.sf-service-mediabrowser-main-usb-focus').removeClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-norIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-norIcon');
						}
						nDeviceFocus--;
						nDeviceIndex--;
						if(nDeviceFocus < MIN_USB_FOCUS_NUM) {
							if(nUsbPageNum == 1) {
								nDeviceFocus = MIN_USB_FOCUS_NUM;
								nDeviceIndex = MIN_USB_FOCUS_NUM;
							}
							else {
								nDeviceFocus = MAX_USB_FOCUS_NUM;
								nUsbPageStartNum = nUsbPageStartNum - MAX_DEVICE_NUM;
								if(nUsbPageStartNum < 0){
									nUsbPageStartNum = 0;
								}
								for(var i=0; i<MAX_DEVICE_NUM; i++) {
									$('#sf-service-mediabrowser-main-item' + i + '-label').html('');
									$('#sf-service-mediabrowser-main-device-Icon'+i).hide()
								}
								mainview.setUsbListView(nUsbPageStartNum);
								mainview.moveMainScroll('prev');
								nUsbPageNum--;

							}
						}
						$('.sf-service-mediabrowser-main-usb-focus').addClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-selIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-selIcon');
						}
					}
					break;
				case sf.key.DOWN:
					if(bMainFocusBox == false) { //포커스가 button에 있을때
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).removeClass('sf-service-mediabrowser-main-Button-focus');
						$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).removeClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
						if(nButtonFocus == 0){
							nButtonFocus = 1;
							if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE,opts.mediaType) && inArray(sf.service.MediaBrowser.MEDIATYPE_MUSIC,opts.mediaType)){
								nButtonFocus = 2;
							}
							else if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MUSIC,opts.mediaType) && !inArray(sf.service.MediaBrowser.MEDIATYPE_MOVIE,opts.mediaType))
							{
								nButtonFocus = 0;
							}

						}
						else if(nButtonFocus == 1){
							nButtonFocus = 2;
							if(!inArray(sf.service.MediaBrowser.MEDIATYPE_MUSIC,opts.mediaType))
							{
								nButtonFocus = 1;
							}
						}
						else if(nButtonFocus == 2){
								nButtonFocus = 2;
						}
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).show();
						$('.sf-service-mediabrowser-main-Button'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-focus');
						$('.sf-service-mediabrowser-main-Button-Icon'+nButtonFocus).addClass('sf-service-mediabrowser-main-Button-Icon'+nButtonFocus+'-focus'+nButtonFocus);
					} else if(bMainFocusBox == true) { //포커스가 usb box에 있을때
						$('.sf-service-mediabrowser-main-usb-focus').removeClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-norIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-norIcon');
						}
						nDeviceFocus++;
						nDeviceIndex++;
						if(nDeviceIndex >= aDeviceNum.length){
							nDeviceIndex = aDeviceNum.length-1;
							nDeviceFocus--;
						}
						else if(nDeviceFocus > MAX_USB_FOCUS_NUM) {
								nUsbPageNum++;
								if(nUsbPageNum > nMaxDevicePageNum){
									nUsbPageNum = nMaxDevicePageNum;
									nDeviceFocus = MAX_USB_FOCUS_NUM;
								}
								else {
									nDeviceFocus = MIN_USB_FOCUS_NUM;
									nUsbPageStartNum = nUsbPageStartNum + MAX_DEVICE_NUM;
									for(var i=0; i<MAX_DEVICE_NUM; i++) {
										$('#sf-service-mediabrowser-main-item' + i + '-label').html('');
										$('#sf-service-mediabrowser-main-device-Icon'+i).hide()
									}
									mainview.setUsbListView(nUsbPageStartNum);
									mainview.moveMainScroll('next');
								}
						}
						$('.sf-service-mediabrowser-main-usb-focus').addClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-selIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-selIcon');
						}

					}
					break;

				case sf.key.ENTER:
					if(bMainFocusBox == false ) { //포커스가 button에 있을때
						setFileType(nButtonFocus);	//usb box로 포커스를 옮기면서 file Type을 결정한다.
						$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
						$('.sf-service-mediabrowser-main-usb-focus').addClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
						$('.sf-service-mediabrowser-main-usb-focus').show();
						//$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-selIcon'+nDeviceFocus);
						if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-usb-selIcon');
						}
						else if (aDeviceNum[nDeviceIndex].deviceType == 'DLNA')  {
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
							$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).addClass('sf-service-mediabrowser-main-dlna-selIcon');
						}
						bMainFocusBox = true;
					} else if(bMainFocusBox == true ) {	//포커스가 usb box에 있을때
						if(aDeviceNum.length >= 1) {
							if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
								$('#sf-service-mediabrowser-main-loading').sfLoading('show');
								alert('defaultPath============'+sDefaultDir);
								sDefaultDir = '$usb_DIR' + '/' + aDeviceNum[nDeviceIndex].mountPath+'/';
								aFilepath.push(sDefaultDir); //선택한 USB에 따라 최초 경로 설정
								aUserFilepath.push(aDeviceNum[nDeviceFocus].vendorName+'/'); //사용자 관점에서 제조사명으로 경로 시작
								$('#sf-service-mediabrowser-file-path').html(''+sf.lang.SID_PATH+':&nbsp;'+ aUserFilepath);
								sViewControl = 'file';
								fileview.show();
								setRefresh();
							}
							else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
								$('#sf-service-mediabrowser-main-loading').sfLoading('show');
								var createBrowser = deviceapis._plugin('ContentsMgr', 'CreateBrowser', aDeviceNum[nDeviceIndex].partitionkey, 1, nfileType); //파티션키, 타입(임시), 필터링 타입
								alert("nfileType.............................................."+nfileType);
								alert("aDeviceNum[nDeviceIndex].partitionkey.............................................."+aDeviceNum[nDeviceIndex].partitionkey);
								alert("createBrowser.............................................."+createBrowser);
								alert("aDeviceNum[nDeviceIndex].mountPath.............................................."+aDeviceNum[nDeviceIndex].mountPath);
								aFilepath.push('/');
								var sFilepath = aFilepath.join('');
								aUserFilepath.push(aDeviceNum[nDeviceFocus].vendorName+'/'); //사용자 관점에서 제조사명으로 경로 시작
								alert("sFilepath.............................................."+DeviceClustering);
								$('#sf-service-mediabrowser-file-path').html(''+sf.lang.SID_PATH+':&nbsp;'+ aUserFilepath);
								var DeviceClustering = deviceapis._plugin('ContentsMgr', 'DeviceClustering', FOLDERVIEW, sFilepath);
								alert("DeviceClustering.............................................."+DeviceClustering);

							}
						}
					}
					break;
				case sf.key.RED :
					break;
				case sf.key.RETURN :
				case sf.key.EXIT :
					alert("returntest............................................................................");
					$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
					$('.sf-service-mediabrowser-main-usb-focus').removeClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
					$('.sf-service-mediabrowser-main-usb-focus').hide();
					$('.sf-service-mediabrowser-main-Button'+nButtonFocus).removeClass('sf-service-mediabrowser-main-Button-focus');
					$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
					alert("returntest3............................................................................");
					alert("returntest4............................................................................");
					nSize=0;
					setMainUsbDataInit();
					aObjectList = new Array();
					sDefaultDir = null;
					sf.scene.returnFocus();
					sf.key.preventDefault();
					$('.sf-service-mediabrowser-main').hide();

					refunction({state:'cancel'});
					break;
			};
		},
		file : function (keyCode) {
			alert('[AF ui] sfPopup keyctl('+keyCode+')');
			if(!bKeyblock){
				switch (keyCode) {
					case sf.key.LEFT:
						if(bflag == true){
							if(bFileFocusBox == false) { //포커스가 file list 일 때
								fileview.removeFocus(nFocusRow,nFocusCol);
								nFocusCol--;

								if(nFocusRow==0 && nFocusCol < 0) { //(0,0) 에서 left 키 누르면  화면의 표시 제일 끝으로 이동
									nFocusRow=aFileTable.length;
									nFocusRow--;
									nFocusCol=aFileTable[nFocusRow].length;
									nFocusCol--;
								} else if(nFocusRow!=0 && nFocusCol < 0) { //첫행이 아니면, 바로위 행의 마지막 열로 이동
									nFocusRow--;
									nFocusCol=MAX_CATEGORY_COL;
								}
								fileview.changeFocus(nFocusRow,nFocusCol); // 포커스 이동을 위한 행, 열 값 변화 후 포커스 이동
							} else if(bFileFocusBox == true) { //포커스가 button 일때
								$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).removeClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
								fileview.changeFocus(nFocusRow,nFocusCol);
								bFileFocusBox = false;
							}
						}
						break;
					case sf.key.RIGHT:
						if(bflag == true){
							if(bFileFocusBox == false) { //포커스가 file list 일 때
								fileview.removeFocus(nFocusRow,nFocusCol);
								nFocusCol++;
								if(nFocusRow >= (aFileTable.length)-1 && nFocusCol > (aFileTable[nFocusRow].length)-1) { //배열의 아이템 수 보다 아이템이 작을때 더이상 움직이지 않는다
									nFocusCol=aFileTable[nFocusRow].length;
									nFocusCol--;
									$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).addClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
									bFileFocusBox = true;
								} else if(nFocusCol > (aFileTable[nFocusRow].length)-1 ) { // 포커스가 열의 마지막일 경우 버튼으로 포커스 이동
									nFocusCol=aFileTable[nFocusRow].length;
									nFocusCol--;
									$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).addClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
									bFileFocusBox = true; //포커스가 button일 경우 right 키는 동작하지 않는다.
								} else {
									fileview.changeFocus(nFocusRow,nFocusCol);
								}
							}
						}
						break;
					case sf.key.UP:
						if(bflag == true){
							if(bFileFocusBox == false) { //포커스가 file list 일 때
								fileview.removeFocus(nFocusRow,nFocusCol);
								nFocusRow--;
								if(nFocusRow < 0) { //첫 행일 경우
									nFocusRow=0;
									nCategoryNum--;
									if(nCategoryNum < 1) { //첫번째 페이지면
										nCategoryNum = 1;
									}
									if(nPageStartNum != 1) {
										$('#sf-service-mediabrowser-file-loading').sfLoading('show');
										var nThumbnailTableLastRow = aThumbnailTable.length-1
										alert("aThumbnailTable.length-1................"+aThumbnailTable.length-1);
										alert("aThumbnailTable[nThumbnailTableLastRow].length................"+aThumbnailTable[nThumbnailTableLastRow].length);
										for(var i=0; i< aThumbnailTable[nThumbnailTableLastRow].length; i++){
											var key = aThumbnailTable[nThumbnailTableLastRow][i];
											alert("key........................"+key);
											if(key in oMadeThumbnailFile){
												delete oMadeThumbnailFile[key];
											}
										}
										nPageStartNum = nPageStartNum-MAX_COL; //
										nFocusRow = MAX_CATEGORY_ROW; //마지막 행으로 이동
										nPageNum--;
										bThumbnailPageChangeFlag = true;
										nThumbnailPageChangeRow = nFocusRow;
										nThumbnailPageChangeCol = nFocusCol;
										fileview.moveFileScroll('prev');
										fileview.setInitFileView();
										if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
											makeFileObj(nPageStartNum);
										}
										fileview.showFileList(nPageStartNum);

									}
								}
								fileview.changeFocus(nFocusRow,nFocusCol);
							} else if(bFileFocusBox == true) { //포커스가 button 일때
								$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).removeClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
								nFileButtonFocus--;
								if(nFileButtonFocus <= 0) {
									nFileButtonFocus =0;
								}
								$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).addClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);

							}
						}

						break;
					case sf.key.DOWN:
						if(bflag == true){
							if(bFileFocusBox == false) { //포커스가 file list 일 때
								fileview.removeFocus(nFocusRow,nFocusCol);
								nFocusRow++;
								if(nFocusRow >= aFileTable.length) { //한 페이지에서 마지막 행일때
									nFocusRow = aFileTable.length;
									if(nFocusRow > MAX_CATEGORY_ROW && nPageNum !=nMaxPageNum ) { //남은 페이지가 있으면
										$('#sf-service-mediabrowser-file-loading').sfLoading('show');
										nFocusRow=0;
										nCategoryNum++;
										if(nCategoryNum > nMaxCategoryNum) { //전체 행의 마지막 행이면 동작안함
											nCategoryNum = nMaxCategoryNum;
										}
										if(nCategoryNum != nMaxCategoryNum) { //전체 행의 마지막 행이 아니면 다음 페이지를 보여준다.
											alert("aThumbnailTable[nThumbnailTableStartRow].length................"+aThumbnailTable[nThumbnailTableStartRow].length);
											alert("nThumbnailTableStartCol................"+nThumbnailTableStartCol);
											for(var i=nThumbnailTableStartCol; i< aThumbnailTable[nThumbnailTableStartRow].length; i++){
												var key = aThumbnailTable[nThumbnailTableStartRow][i];
												alert("key........................"+key);
												if(key in oMadeThumbnailFile){
													delete oMadeThumbnailFile[key];
												}
											}
											nPageStartNum = nPageStartNum+MAX_COL;
										}
										if(nFocusCol >= aFileTable[nFocusRow].length) { //열의 길이가 이전 행의 열의 길이보다 작으면
											nFocusCol = aFileTable[nFocusRow].length; //마지막 열로 포커스 이동
											nFocusCol--;
										}
										bThumbnailPageChangeFlag = true;
										nThumbnailPageChangeRow = nFocusRow;
										nThumbnailPageChangeCol = nFocusCol;
										fileview.setInitFileView();
										alert("test.............................................................1");
										if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
											alert("test.............................................................2");
											makeFileObj(nPageStartNum);
										}
										alert("test.............................................................3");
										fileview.showFileList(nPageStartNum);
										alert("test.............................................................4");
										fileview.moveFileScroll('next');
										nPageNum++;
									} else { //남은 페이지가 없으면
										nFocusRow=aFileTable.length-1;
									}
								} else { //한 페이지에서 마지막 행이 아니면
									if(nFocusCol >= aFileTable[nFocusRow].length) {	//열의 길이가 이전 행의 열의 길이보다 작으면
										nFocusCol = aFileTable[nFocusRow].length;	//마지막 열로 포커스 이동
										nFocusCol--;
									}
								}
								fileview.changeFocus(nFocusRow,nFocusCol);
							} else if(bFileFocusBox == true) { //포커스가 button 일때
								$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).removeClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
								nFileButtonFocus++;
								if(nFileButtonFocus >= MAX_FILE_BUTTON_NUM) {
									nFileButtonFocus=MAX_FILE_BUTTON_NUM;
								}
								$('.sf-service-mediabrowser-file-Button'+nFileButtonFocus).addClass('sf-service-mediabrowser-file-Button-focus-position'+nFileButtonFocus);
							}
						}
						break;
					case sf.key.ENTER:
						if(bflag == true){
							if(bFileFocusBox == false) { //포커스가 file list 일 때
								if(aFileTable[nFocusRow][nFocusCol].isDir) {	//폴더이면
									if(aFileTable[nFocusRow][nFocusCol].filename == '..') { //상위 폴더이면
										aFilepath.pop();
										aUserFilepath.pop();
										var sUserFullpath = aUserFilepath.join('');
										$('#sf-service-mediabrowser-file-path').html(''+sf.lang.SID_PATH+':&nbsp;'+ sUserFullpath);
										if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
											setRefresh();
										}
										else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
											var nIndexMetaData = nPageNum * (nFocusRow+nFocusCol);
											alert("nIndexMetaData.............................................."+nIndexMetaData);
											var nMoveToIndex = deviceapis._plugin('ContentsMgr', 'MoveToIndex',nIndexMetaData);
											var aDlnaFilepath = aFilepath.join('');
											var DeviceClustering = deviceapis._plugin('ContentsMgr', 'DeviceClustering', FOLDERVIEW, aDlnaFilepath);
											alert("DeviceClustering.............................................."+DeviceClustering);
										}
									} else { //일반 폴더이면
										aFilepath.push(aFileTable[nFocusRow][nFocusCol].filename+'/');
										aUserFilepath.push(aFileTable[nFocusRow][nFocusCol].filename+'/');
										var sUserFullpath = aUserFilepath.join('');
										alert("aDeviceNum[nDeviceIndex].deviceType..........................................."+aDeviceNum[nDeviceIndex].deviceType);
										$('#sf-service-mediabrowser-file-path').html(''+sf.lang.SID_PATH+':&nbsp;'+ sUserFullpath);
										if(aDeviceNum[nDeviceIndex].deviceType == 'USB'){
											setRefresh();
										}
										else if(aDeviceNum[nDeviceIndex].deviceType == 'DLNA'){
											var nIndexMetaData = ((nPageNum-1)*MAX_COL)+(nFocusRow+nFocusCol);
											alert("nIndexMetaData.............................................."+nIndexMetaData);
											var nMoveToIndex = deviceapis._plugin('ContentsMgr', 'MoveToIndex',nIndexMetaData);
											var aDlnaFilepath = aFilepath.join('');
											alert("aDlnaFilepath.............................................."+aDlnaFilepath);
											var DeviceClustering = deviceapis._plugin('ContentsMgr', 'DeviceClustering', 0, aDlnaFilepath);
											alert("DeviceClustering.............................................."+DeviceClustering);
										}
									}
								} else { //파일일 경우
									fileview.setFileCheck(aFilepath,nFocusRow,nFocusCol);
								}
							}

							if(bFileFocusBox == true) { //포커스가 button 일때
								if(nFileButtonFocus == 0) { //add button
									var oReturnValue = {};
									oReturnValue.state = 'ok';
									oReturnValue.selectedMedia = [];
									for(var key in oSelectedPath) {
										oReturnValue.selectedMedia.push({
											filepath: key,
											type: oSelectedPath[key].type,
											source: aDeviceNum[nDeviceIndex].deviceType
										});
									}
									for(var i=0; i<oReturnValue.selectedMedia.length; i++) {
										alert('return________________'+oReturnValue.selectedMedia[i].filepath);
									}
									$('#sf-service-mediabrowser-main-device-Icon'+nDeviceFocus).removeClass();
									$('.sf-service-mediabrowser-main-usb-focus').removeClass('sf-service-mediabrowser-main-usb-focus-position'+nDeviceFocus);
									$('.sf-service-mediabrowser-main-usb-focus').hide();
									$('.sf-service-mediabrowser-main-Button'+nButtonFocus).removeClass('sf-service-mediabrowser-main-Button-focus');
									$('.sf-service-mediabrowser-main-Button-highlight'+nButtonFocus).hide();
									setFileDataInit();
									setMainUsbDataInit();
									sf.scene.returnFocus();
									sViewControl = 'main';
									$('.sf-service-mediabrowser-file').hide();
									$('.sf-service-mediabrowser-main').hide();
									refunction(oReturnValue); // 수행후 체크된 파일을 콜백 함수에 넘겨준다.
									sf.key.preventDefault();
								} else if(nFileButtonFocus ==1) { //cancel button
									var oReturnValue = {};
									oReturnValue.state = 'cancel';
									refunction(oReturnValue);
									setFileDataInit();
									$('.sf-service-mediabrowser-file').hide();
									sViewControl = 'main';
									sf.key.preventDefault();
								} else { //All select button
									alert("allckeck!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
									fileview.setSelectAll(aFilepath);
								}
							}
						}
						break;
					case sf.key.EXIT :
					case sf.key.RETURN :
						setFileDataInit();
						$('.sf-service-mediabrowser-file').hide();
						sViewControl = 'main';
						sf.key.preventDefault();
						if (keyCode == sf.key.EXIT) {	// EXIT 키 누르면 모든 창을 닫음. main에도 EXIT키 처리(main닫기)를 하도록 함.
							Control.main(sf.key.EXIT);
						}
						break;
				};
			}
		}
	}
})(sf);/*
 * Title: UI Components (Core)
 * 	Apps Framework Core UI Components.
 *
 * Version added:
 * 	1.1.0
 */

(function ($) {
	/* ********************************************************************** */
	/* {{{ WidgetBase class                                                   */

	/* Simple JavaScript Inheritance
	 * By John Resig http://ejohn.org/
	 * MIT Licensed.
	 * @see: http://ejohn.org/blog/simple-javascript-inheritance/
	 *
	 * Modifications: <a href="mailto:m.stankiewic@samsung.com">Maciej Stankiewicz</a>
	 */

	var initializing = false,
		fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/,
		WidgetBase,
		DO_NOT_OVERRIDE = [null,		// List of fields/methods that can not be overrided with _extend/extend method.
								// REASON:
			"__setter",			// no need to override
			"_extend",			// unsafe
			"eventNamespace",	// ? i'm not sure
			"widgetName",		// ? i'm not sure
			"baseCssClass",		// ? i'm not sure
			"callbacks",		// dedicated function for callbacks is setCallback()
			"options",			// dedicated function for options i option()
			"templates",		// templates are private
			"view",				// view is private
			null].join("|");

	/**
	 * The base Class implementation (does nothing).
	 * @ignore
	 */
	WidgetBase = function () {};

	/**
	 * @ignore
	 * Create a new Class that inherits from this class
	 * @return {Function} Constructor of new class
	 */
	WidgetBase.extend = function (initializer, extensions) {
		var _super = this.prototype,
			basePrototype,
			name;

		if (!extensions) {
			extensions = initializer;
			initializer = "_createWidget";
		}

		/* {{{ Instantiate a base class */
		// (but only create the instance, don't run the init constructor)
		initializing = true;
		basePrototype = new this();
		for (name in basePrototype) {
			if ($.isPlainObject(basePrototype[name])) {
				basePrototype[name] = $.extend(true, {}, basePrototype[name]);
			}
		} // for (name in extensions) }}}
		initializing = false;
		/* Instantiate a base class }}} */

		// Copy the properties over onto the new prototype
		for (name in extensions) {
			// Check if we're overwriting an existing function
			if ($.isFunction(extensions[name]) && $.isFunction(_super[name]) && fnTest.test(extensions[name])) {
				basePrototype[name] = (function (name, fn) {
					return function () {
						var backup_super = this._super,
							ret;

						// Add a new _super() method that is the same method
						// but on the super-class
						this._super = _super[name];

						// The method only need to be bound temporarily, so we
						// remove it when we're done executing
						ret = fn.apply(this, arguments);
						this._super = backup_super;

						return ret;
					};
				}(name, extensions[name]));
			} else if ($.isPlainObject(extensions[name])) {
				// Add copy, not reference
				basePrototype[name] = $.extend(true, {}, basePrototype[name], extensions[name]);
			} else {
				basePrototype[name] = extensions[name];
			}
		} // for (name in extensions) }}}

		/**
		 * @ignore
		 * The dummy class constructor
		 */
		function Widget() {
			// All construction is actually done in the init method
			if (!initializing && this[initializer] && typeof this[initializer] === "function") {
				this[initializer].apply(this, arguments);
			}
		}
		Widget.prototype = basePrototype;	// Populate our constructed prototype object
		Widget.constructor = this;			// Enforce the constructor to be what we expect
		Widget.extend = arguments.callee;	// And make this class extendable

		return Widget;
	};
	/* WidgetBase class }}}                                                   */
	/* ********************************************************************** */


	/* ********************************************************************** */
	/* {{{ sf.ui                                                            */

	/**
	 * @namespace Apps Framework Widgets core functions
	 */
	/*
		Namespace: sf.ui
			Apps Framework UI core functions
	*/
	sf.ui = {

		/**
		 * Add widget selector to jQuery selectors
		 * @param {String} name Selector - it should be the same name that was passed to {@link sf.ui.bridge} function.
		 * @see sf.ui.bridge
		 * @see sf.ui.widgetFactory
		 */
		/*
			Function: addSelector
				Add widget selector to jQuery selectors

			Parameters:
				o (String) name - Selector. it should be the same name that was passed to <sf.ui.bridge at sf.ui.bridge> function.

			See Also:
				<sf.ui.bridge> <sf.ui.widgetFactory>
		*/
		addSelector: function (name) {
			//TODO: validation
			if (!!name) {
				$.expr[":"][name] = function (element) {
					return !!$.data(element, name);
				};
			}
		}, // addSelector()


		/**
		 * Binds widget class with jQuery plugin function.
		 * @param {String} name jQuery plugin function name we want to bind widget class to
		 * @param {Function} Widget Widget class constructor
		 * @see sf.ui.addSelector
		 * @see sf.ui.widgetFactory
		 */
		/*
		 * Function: bridge
		 * 	Binds widget class with jQuery plugin function.
		 *
		 * Parameters:
		 *		o (String) name - jQuery plugin function name we want to bind widget class to
		 *		o (Function) Widget - Widget class constructor
		 *
		 * See Also:
		 *		<sf.ui.addSelector> <sf.ui.widgetFactory>
		 */
		bridge: function (name, Widget) {
			$.fn[name] = function(options) {
				var isMethodCall = typeof options === "string",
					args = Array.prototype.slice.call(arguments, 1),
					returnValue = this;

				// allow multiple hashes to be passed on init
				if (!isMethodCall && args.length) {
					options = $.extend.apply(null, [true, options].concat(args));
				}

				// prevent calls to internal methods
				if (isMethodCall && options.charAt(0) === "_") {
					return returnValue;
				}

				if (isMethodCall) {
					this.each(function () {
						var instance = $.data(this, name) || $.data(this, name, new Widget({}, this)),
							methodValue = $.isFunction(instance[options]) ?
								instance[options].apply(instance, args) :
								instance;

						if (methodValue !== instance) {
							returnValue = methodValue;
							return false;
						}
					});
				} else {
					this.each(function () {
						var instance = $.data(this, name);
						if (instance) {
							// 2011-09-02 : kookheon.kim@samsung.com
							// Problem : There's case that the DOM element is detached from previous parent when Object is added to its parent in Maple Browser.
							// Solution : Reassign the jQuery element to every jQuery plugin calls.
							instance.element = $(this);
							instance.option(options || {})._init();
						} else {
							$.data(this, name, new Widget(options || {}, this));
						}
					});
				}

				return returnValue;
			};
		}, // bridge()


		/*
		 * Function: widgetFactory
		 * 	Widget factory function.
		 *
		 * Parameters:
		 * 	o base - (Object) Base widget constructor (default: sf.ui.Widget)
		 *	o extensions - (Function) Literal object with fields and functions that will be added to new widget (fields and functions will extend or override base widget prototype).
		 *
		 * Returns:
		 * 	(Function) New widget class
		 *
		 * Example 1:
		 * 	Define new widget with sf.ui.Widget as its base.
		 * 	(start code)
		 * MyWidget = sf.ui.widgetFactory({
		 *     foo: "foofoo", // custom field
		 *
		 *     templates: {
		 *         item: "<p>${name}</p>"
		 *     },
		 *
		 *     _create: function () {
		 *         this.view.item = $.tmpl(this.templates.item, {
		 *             name: this.foo
		 *         });
		 *         this.element.append(this.view.item);
		 *     },
		 *
		 *     _destroy: function () {
		 *         this.view.item.remove();
		 *     },
		 *
		 *     widget: function () {
		 *         return this.view.item;
		 *     }
		 * });
		 *
		 * // make a bridge
		 * sf.ui.bridge("myWidget", MyWidget);
		 *
		 * // define selector
		 * sf.ui.addSelector("myWidget");
		 * (end)
		 *
		 * Exmaple 2:
		 * 	Define new widget with MyWidget as its base
		 * 	(start code)
		 * MySuperWidget = sf.ui.widgetFactory(MyWidget, {
		 *     options: {
		 *         counter: 0
		 *     },
		 *
		 *     templates: {
		 *         item: "<p>${name} was initialized</p>",
		 *         counter: "<span />"
		 *     },
		 *
		 *     _create: function () {
		 *         this._super();
		 *         this.view.counter = $(this.templates.counter);
		 *         this.view.item.append(this.view.counter);
		 *     },
		 *
		 *     _init: function () {
		 *         this.options.counter += 1;
		 *         this.view.counter.text(" " + this.options.counter + " times");
		 *     },
		 *
		 *     _destroy: function () {
		 *         this.view.counter.remove();
		 *         this._super();
		 *     },
		 *
		 *     _setOption: function (name, value) {
		 *         if (name !== "counter") {
		 *             this._super(name, value);
		 *         }
		 *     }
		 * });
		 *
		 * // make a bridge
		 * sf.ui.bridge("mySuperWidget", MySuperWidget);
		 *
		 * // define selector
		 * sf.ui.addSelector("mySuperWidget");
		 * 	(end)
		 *
		 * See Also:
		 * 	<sf.ui.addSelector>, <sf.ui.bridge>
		 *
		 * History:
		 *
		 */
		widgetFactory: function (base, extensions) {
			/* {{{ validate arguments */
			if (!extensions) {
				extensions = base;
				base = sf.ui.Widget;
			}
			//TODO: validate important fields in extensions object
			if (!extensions.widgetName) {
				$.error('[sf.ui.widgetFactory] widgetName is required');
			}
			extensions.eventNamespace = (extensions.eventNamespace || extensions.widgetName).toLowerCase();
			/* validate arguments }}} */

			return base.extend(extensions);
		}, // widgetFactory()


		/**
		 * @class Base class for all widgets
		 */
		/*
		 * Class: sf.ui.Widget
		 * 	Base class for all widgets
		 */
		Widget: WidgetBase.extend({
			/*
			 * Property: baseCSSClass
			 * 	Base CSS class that will be added to widget instance.
			 *	It will be added to DOM element returned by {@link #widget} function.
			 *
			 * Type:
			 * 	String
			 *
			 * Default:
			 * 	"" (empty string)
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 *
			 * History:
			 *
			 */
			baseCssClass: "",

			/*
			 * Property: element
			 * 	Element in DOM, that keeps widget instance.
			 *
			 * Type:
			 * 	jQuery object
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 */

			/*
			 * Property: eventNamespace
			 * 	Event namespace added by default to every event triggered by widget.
			 *	It is usualy lower case of {@link #widgetName}
			 *
			 * Type:
			 * 	String
			 *
			 * Default:
			 * 	"widget"
			 *
			 * See Also:
			 * 	jQuery event.namespace documentation (<http://api.jquery.com/event.namespace/>), <addCallback>, <removeCallback>, <removeAllCallbacks>
			 *
			 * History:
			 *
			 */
			eventNamespace: "widget",

			/*
			 * Property: initialState
			 * 	Object to store initial state of element to which we want to apply widget.
			 *	Initial state data would be useful during destroying widget instance and restoring its initial state.
			 *
			 * Type:
			 * 	Object
			 *
			 * Default:
			 * 	{}
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 */

			/*
			 * Property: options
			 * 	Object to store state of the widget instance.
			 * 	All elements in options object can be set/get with <option> function.
			 *
			 * Type:
			 * 	Object
			 *
			 * Default:
			 * 	{}
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 */

			/*
			 * Property: templates
			 * 	Object with templates used to render widget instance.
			 * 	Templates can be defined when widget class is defined <sf.ui.widgetFactory>,
			 * 	or when widget instance is initialized.
			 *
			 * Type:
			 * 	Object
			 *
			 * Default:
			 * 	{}
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 *
			 * Example 1:
			 * 	define templates for widget class
			 * 	(start code)
			 * MyWidget = sf.ui.widgetFactory({
			 *     ...
			 *     templates: {
			 *         list: "<ul>${items}</ul>",
			 *         item: "<li>${name}</ul>"
			 *     }
			 *     ...
			 * });
			 *
			 * sf.ui.bridge("myWidget", MyWidget); // bridge jQuery plugin function to widget
			 * (end)
			 *
			 * Example 2:
			 * 	redefine templates for instance (call jQuery plugin function defined abowe)
			 * 	(start code)
			 * $("#instance").myWidget({
			 *     templates: {
			 *         // override list template
			 *         list: "<div>${titleItem}" +
			 *                   <ol>${items}</ol>" +
			 *               "</div>",
			 *         // add extra template
			 *         title: "<p>${title}</p>"
			 *     }
			 * });
			 * 	(end)
			 *
			 * See Also:
			 * 	jQuery Templates plugin documentation (<http://api.jquery.com/category/plugins/templates/>)
			 */

			/*
			 * Property: themeCssClass
			 * 	Additional CSS class that can be added to widget instance for theming purpose.
			 *
			 * Type:
			 * 	String
			 *
			 * Default:
			 * 	""
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 */

			/*
			 * Property: view
			 * 	Object to store references to DOM elements.
			 * 	If there is internal element of widget that is used very often (for example button in popup window),
			 * 	it is good candidate to store it as reference in view object to easy access.
			 *
			 * Type:
			 * 	Object
			 *
			 * Default:
			 * 	{}
			 *
			 * Field Of:
			 * 	sf.ui.Widget.prototype
			 */

			/*
			 * Property: widgetName
			 * 	Widget name.
			 *
			 * Type:
			 * 	String
			 *
			 * Default:
			 * 	"Widget"
			 *
			 * History:
			 *
			 */
			widgetName: "Widget",


			/**
			 * Magic setter function
			 * @param {Function} fn This function will be called for each pair key:value
			 * @param {String|Object} key Key name or literal object of pairs key:value
			 * @param {Mixed} value Value for key or <tt>undefined</tt> if key is <tt>Object</tt>
			 * @private
			 */
			__setter: function (fn, key, value) {
				var k;

				if (key !== null) {
					if (typeof key === 'string') {
						fn.call(this, key, value);
					} else {
						for (k in key) {
							if (key.hasOwnProperty(k)) {
								fn.call(this, k, key[k]);
							}
						}
					}
				}
			}, // __setter()


			/**
			 * Add one or more callbacks for given event.
			 * @param {String} eventName Name of the event
			 * @param {Function|Array<Function>} fn Callback function or Array of callback functions
			 * @private
			 */
			_addCallback: function (eventName, fn) {
				//alert("[sf.ui.Widget] _addCallback(" + eventName + ", " + fn + ")");

				this.callbacks[eventName] = (this.callbacks[eventName] || []).concat(fn);
			}, // _addCallback()


			/**
			 * This function is called once on instance initialization.
			 * By default it adds {@link baseCssClass} and themeCssClass to element returned by {@link #widget} function.
			 * If there is need to add custom initialization of widget, _create function can be added to widget class that inherits from {@link sf.ui.Widget}.
			 * @example
			 * MyWidget = sf.ui.widgetFactory({
			 *     ...
			 *     _create: function () {
			 *         // Custom initialization code before call to _create() of base class.
			 *         // Let's save initial state of element
			 *         this.initialState.height = this.element.height();
			 *         // and set variable for _init() function
			 *         this.initCounter = 0;
			 *         ...
			 *         // Call to _create() function of base class (optional but highly recommended)
			 *         this._super();
			 *         // Custom initialization code after call to _create() of base class
			 *         this.view.innerDiv = $("&lt;div />");
			 *         this.widget().append(this.view.someElement);
			 *         ...
			 *     }
			 *     ...
			 * });
			 * @see #_init
			 * @public
			 */
			_create: function () {
				alert("[sf.ui.Widget] _create()");

				this.widget().addClass(this.baseCssClass).addClass(this.options.themeCssClass);
			}, // _create()


			/**
			 * In fact this is real constructor for Widget class.
			 * There is no need to override it so keep it private.
			 * @param {Object} params
			 * @param {HTML element} element
			 * @private
			 */
			_createWidget: function (params, element) {
				alert("[sf.ui.Widget] _createWidget()");

				var options = $.extend({}, params);

				// $.widget.bridge stores the plugin instance, but we do it anyway
				// so that it's stored even before the _create function runs
				$.data(element, this.widgetName, this);
				this.element = $(element);

				// {{{ baseCssClass
				if (options.baseCssClass) {
					this.baseCssClass = options.baseCssClass;
					delete options.baseCssClass;
				}
				// baseCssClass }}}

				// {{{ Override instance
				if (options.overrides) {
					this.extend(options.overrides);
					delete options.overrides;
				}
				// Override instance }}}

				// {{{ Templates
				this.templates = $.extend(true, {}, this.templates, options.templates);
				delete options.templates;
				// Templates }}}

				// {{{ Callbacks
				this.callbacks = $.extend(true, {}, this.callbacks);
				// Add callbacks
				if (options.callbacks) {
					this.addCallback(options.callbacks);
					delete options.callbacks;
				}
				// Callbacks }}}

				// {{{ initialState
				this.initialState = {};
				// initialState }}}

				// {{{ view
				this.view = {};
				// view }}}

				// {{{ Set options
				this.options = $.extend(true, {}, this.options, options);
				// Set options }}}

				var self = this;
				this.element.bind("destroy." + this.eventNamespace, function() {
					self.destroy();
				});

				this._create();
				this._trigger("create");
				this._init();
			}, // _createWidget()


			/**
			 * Performs custom action while destroying widget.
			 * This is the best place to provide custom code for destroying widget.
			 * It is invoked at the begining of {@link #destroy} function.
			 * <br>By default it is empty function and does nothing.
			 * <br>If widget inherits from another widget (not {@link sf.ui.Widget}) which has its own _destroy method,
			 * it could be necessary to call _destroy method of base widget.
			 * In such a case You can call it as this._super(), see example.
			 * @example
			 * MyWidget = sf.ui.widgetFactory({
			 *     ...
			 *     _destroy: function () {
			 *         // Custom destroy code before call to _destroy() of base class.
			 *         this.view.innerDiv.remove();
			 *         ...
			 *         // Call to _destroy() function of base class
			 *         this._super();
			 *         // Custom destroy code after call to _destroy() of base class
			 *         this.element.height(this.initialState.height);
			 *         ...
			 *     }
			 *     ...
			 * });
			 * @see #destroy
			 * @public
			 */
			_destroy: function () {
			}, // _destroy()


			/**
			 * Allows to extend/override field/method of instance
			 * @param {String} name Property name
			 * @param {Mixed} value Value of property
			 * @private
			 */
			_extend: function (name, value) {
				//alert("[sf.ui.Widget] _extend(" + name + ", " + value + ")");

				if (DO_NOT_OVERRIDE.indexOf("|" + name + "|") === -1) {
					this[name] = value;
				}
			}, // _extend()


			/**
			 * Set/update widget instance state.
			 * It is invoked:
			 * <ul>
			 *   <li>once after {@link #_create} functino while initialization</li>
			 *   <li>every time the literal object is passed to widget plugin call form</li>
			 * </ul>
			 * By default it is empty function and does nothing.
			 * @example
			 * MyWidget = sf.ui.widgetFactory({
			 *     ...
			 *     _init: function () {
			 *         // Custom code before call to _init() of base class.
			 *         this.initCounter += 1;
			 *         ...
			 *         // Call to _init() function of base class (optional but highly recommended)
			 *         this._super();
			 *         // Custom code after call to _init() of base class
			 *         this.view.innerDiv.height(this.initCounter * 10);
			 *         ...
			 *     }
			 *     ...
			 * });
			 * @see #_create
			 * @public
			 */
			_init: function () {
				//alert("[sf.ui.Widget] _init()");
			}, // _init()


			/**
			 * Setter for {@link #options} object.
			 * If {@link #option} function is invoked as setter, this function is run for every pair name:value.
			 * If some members of {@link #options} object should be "read only", this is the best place to filter them.
			 * @example
			 * MyWidget = sf.ui.widgetFactory({
			 *     ...
			 *     _setOption: function (name, value) {
			 *         switch (name) {
			 *             case "foo":  // foo is read only
			 *                 break;
			 *
			 *             case "bar":  // bar should be a valid Number or null if NaN
			 *                 this.options.bar = parseInt(value, 10) || null;
			 *                 break;
			 *
			 *             default:     // For all the rest perform default action which is just to set value for named field.
			 *                 this._super(name, value);
			 *         }
			 *     }
			 *     ...
			 * });
			 * @param {String} name Field in {@link #options} object
			 * @param {Mixed} value Value for field
			 * @see #option
			 * @public
			 */
			_setOption: function (name, value) {
				//alert("[sf.ui.Widget] _setOption(" + name + ", " + value + ")");

				this.options[name] = value;
			}, // _setOption()


			/**
			 * Set widget template
			 * @param {String} name
			 * @param {jQuery Template} template @see http://api.jquery.com/category/plugins/templates/
			 * @private
			 */
			_setTemplate: function (name, template) {
				//alert("[sf.ui.Widget] _setTemplate(" + name + ", " + template + ")");

				this.templates[name] = template;
			}, // setTemplate()


			/**
			 * Trigger event of given type and call all callback functions if exist.
			 * @param {String} type Event name
			 * @param {Object|null} [event] Event object or <tt>null</tt>
			 * @param {Object} [data={}] Data object to pass within event object
			 * @return {Boolean} <tt>false</tt> if any of callback functions returns <tt>false</tt> or <tt>event.isDefaultPrevented()</tt> is false
			 * @private
			 */
			_trigger: function (type, event, data) {
				//alert("[sf.ui.Widget] _trigger(" + type + ")");

				var triggeredType = (type + "." + this.eventNamespace).toLowerCase(),
					callbacks,
					i,
					iLen,
					prop,
					preventFromCallback = false;

				// We add namespace to event type by default
				if (!event) {
					event = $.Event(triggeredType);
				} else {
					event = $.Event(event);
					event.type = triggeredType;
				}
				data = data || {};

				// copy original event properties over to the new event
				// this would happen if we could call $.event.fix instead of $.Event
				// but we don't have a way to force an event to be fixed multiple times
				if (event.originalEvent) {
					for (i = $.event.props.length, prop; i;) {
						i -= 1;
						prop = $.event.props[i];
						event[prop] = event.originalEvent[prop];
					}
				}

				// Run callbacks if exist
				callbacks = this.callbacks[type];
				if (callbacks) {
					for (i = 0, iLen = callbacks.length; i < iLen; i += 1) {
						if ($.isFunction(callbacks[i])) {
							preventFromCallback = preventFromCallback || callbacks[i].call(this.element[0], event, data) === false;
						}
					}
				}

				this.element.trigger(event, data);

				return !(preventFromCallback || event.isDefaultPrevented());
			}, // _trigger()


            /*
             Function: addCallback
             	Add callback function for given event

             Parameters:
             	o eventName - (String|Object) eventName Name of the event or literal object with pairs eventName:Function|Array<Function>. It should be event name without <eventNamespace>
             	o fn - (Function|Array<Function>) fn Callback function or Array of callback functions

             Returns:
             	(Object) this

             Description:
             	Add callback function for given event
             	All callbacks will be invoked before triggering the event
             	Scope of the callback will be widget instance

             Example 1:
             	This example add more callbacks for various events
				(start code)
				$("#instance").myWidget("addCallback", {
					"show": function () {
						alert("Can You see me?");
					},
					"beforeshow": foo,
					"hide": [foo, Obj.bar]
				});
				(end)

             Available From:
            	 1.1.0

             History:
             */
			addCallback: function (eventName, fn) {
				alert("[sf.ui.Widget] addCallback(" + eventName + ", " + fn + ")");

				this.__setter(this._addCallback, eventName, fn);

				return this;
			}, // addCallback()

			/*
			Function: destroy
				Destroy widget instance

			Parameters:
				N/A

			Returns:
				(Object) this

			Description:
				Destroy widget instance

			Example 1:
				This example destroy widget instance
				(start code)
				$("#instance").myWidget("destroy");
				(end)

			Available From:
				1.1.0

			History:
			*/
			destroy: function () {
				alert("[sf.ui.Widget] destroy()");

				this._destroy();

				this.element
					.unbind("." + this.eventNamespace)
					.removeData(this.widgetName);
				this.widget()
					.unbind("." + this.eventNamespace)
					.removeClass((this.baseCssClass || "") + " " + (this.options.themeCssClass || ""));
				this.removeAllCallbacks();

				return this;
			}, // destroy()


			/*
			Function: extend
				Extend widget instance

			Parameters:
				o name - (String|Object) Property name or literal object of pairs name:value
				o value - (Function|Array<Function>) Callback function or Array of callback functions

			Returns:
				(Object) this

			Description:
			 Extend widget instance
			 You can extend or override widget instance functionality after its instantiation

			Example 1:
				This example extend widget instance
				(start code)
				$("#instance")
				   .myWidget()
				   .myWidget("extend", "newFunction", function (value) {
				       alert(value);
				       return this; // let's make new function chainable
				   })
				   .myWidget("newFunction", "Is there anybody out there?");
				(end)

			Available From:
				1.1.0

			History:
			*/
			extend: function (name, value) {
				alert("[sf.ui.Widget] extend(" + name + ", " + value + ")");

				this.__setter(this._extend, name, value);

				return this;
			}, // extend()


			/*
			Function: hide
				Hide element

			Parameters:
				N/A

			Returns:
				(Object) this

			Description:
				Hide element returned by "widget" function

			Example 1:
				This example hide element
				(start code)
				$("#instance").myWidget("hide");
				(end)

			Available From:
				1.1.0

			History:
			*/
			hide: function () {
				alert("[sf.ui.Widget] hide()");

				if (this._trigger("beforehide") !== false) {
					this.widget().hide();
					this._trigger("hide");
				}

				return this;
			}, // hide()


			/*
			Function: option
				Setter/getter for properties of  "options" object

			Parameters:
				o name - (String|Object)name
				o value - (Any types)value

			Returns:
				(Any Types) - if the "value" is not defined, returns the value. (get option)
				(Object) - if the "value" is defined, returns "this". (set option)

			Description:
				Setter/getter for properties of  "options" object

			Example 1:
				This example set value for 'bigValue' property
				(start code)
				$("#instance").myWidget("option", "bigValue", 12345678);
				(end)

			Available From:
				1.1.0

			History:
			*/
			option: function (name, value) {
				alert("[sf.ui.Widget] option(" + name + ", " + value + ")");

				if (arguments.length === 0) {
					alert("    > get all options");
					// don't return a reference to the internal hash
					return $.extend({}, this.options);
				}

				if (typeof name === "string") {
					if (value === undefined) {
						alert("    > get '" + name + "'");
						return this.options[name];
					}
				}
				alert("    > set '" + name + "'");
				this.__setter(this._setOption, name, value);

				return this;
			}, // option()


			/*
			Function: removeAllCallbacks
				Remove all callbacks

			Parameters:
				N/A

			Returns:
				(Object) this

			Description:
				Remove all callbacks
				Only callbacks added with "addCallbacks" function or passed on initialization of instance will be removed

			Example 1:
				This example remove all callbacks for widget instance
				(start code)
				$("#instance").myWidget("removeAllCallbacks");
				(end)

			Available From:
				1.1.0

			History:
			*/
			removeAllCallbacks: function () {
				alert("[sf.ui.Widget] removeAllCallbacks()");

				this.callbacks = {};

				return this;
			}, // removeAllCallbacks()


			/*
			Function: removeCallback
				Remove callback or all calbacks for given event

			Parameters:
				o eventName - (String) Name of event. It should be event name without "eventNamespace".
				o fn - (Function) Callback function to remove to

			Returns:
				(Object) this

			Description:
				Remove callback or all calbacks for given event
				Only callbacks added with "addCallbacks" function or passed on initialization of instance can be removed with this function.
				If *fn* is undefined, all callbacks for given event will be removed

			Example 1:
				(start code)
				// Remove specific callback for event 'show'
				function onShow() {
					alert("Can you see me?");
				}

				$("#instance").myWidget("addCallback", "show", onShow);

				...

				$("#instance").myWidget("removeCallback", "show", onShow);
				(end)

			Example 2:
				(start code)
				// Remove all callbacks for event 'show'
				$("#instance").myWidget("removeCallback", "show");
				(end)

			Example 2:
				(start code)
				// Callbacks passed "on the fly" to addCallback function can not be removed
				$("#instance").myWidget("addCallback", "show", function () {
					alert("Can you see me?");
				});

				// This will not work
				$("#instance").myWidget("removeCallback", "show", function () {
					alert("Can you see me?");
				});
				(end)

			Available From:
			 1.1.0

			History:
			*/
			removeCallback: function (eventName, fn) {
				alert("[sf.ui.Widget] removeCallback(" + eventName + ", " + fn + ")");

				var callbacks = this.callbacks[eventName];

				if (callbacks) {
					if (fn) {
						this.callbacks[eventName] = $.grep(callbacks, function (callback) {
							return (callback !== fn);
						});
					} else {
						delete this.callbacks[eventName];
					}
				}

				return this;
			}, // removeCallback()


			/*
			Function: show
			 Show element returned by "widget" function

			Parameters:
				N/A

			Returns:
				(Object) this

			Example 1:
				(start code)
				$("#instance").myWidget("show");
				(end)

			Available From:
				1.1.0

			History:
			*/
			show: function () {
				alert("[sf.ui.Widget] show()");

				if (this._trigger("beforeshow") !== false) {
					this.widget().show();
					this._trigger("show");
				}

				return this;
			}, // show()


			/**
			 * Returns DOM element (wrapped as jQuery object) that represents widget instance.
			 * It is by default element passed to constructor.
			 * @example
			 * var widgetHandler = $("#instance").myWidget("widget");
			 *
			 * @return {jQuery object}
			 */
			/*
			Function: widget
				Returns DOM element (wrapped as jQuery object) that represents widget instance.
				It is by default element passed to constructor

			Parameters:
				N/A

			Returns:
				(Object) jQuery object

			Example 1:
				(start code)
				var widgetHandler = $("#instance").myWidget("widget");
				(end)

			Available From:
				1.1.0

			History:
			*/
			widget: function () {
				alert("[sf.ui.Widget] widget()");

				return this.element;
			} // widget()
		}) // sf.ui.Widget
	}; // sf.ui{}








	/**
	 * Widget factory function.
	 * @param {String} name UI Helper name with namespace excluding $
	 * @param {Function} base Base widget constructor
	 * @param {Object} prototype Literal object wits fields and methods that will be added to new UI Helper
	 */
	sf.ui.widget = function (name, base, prototype) {
		var nameTokens,	// local
			namespace = $,	// local: namespace reference
			widgetFullName, // name as in $.fn[widgetFullName]
			widgetName,		// name as in $.namespace.widgetName
			baseCssClass;	// widget CSS base class

		/* {{{ validate arguments */
		if (!name) {
			$.error('[sf.ui.widget] Plugin name is empty');
		}
		if (!prototype) {
			prototype = base;
			base = sf.ui.Widget;
		}
		nameTokens = name.split(".");
		if (nameTokens.length < 2) {
			$.error('[sf.ui.widget] Plugin name without namespace');
		}
		/* validate arguments }}} */

		baseCssClass = nameTokens.join('-').toLowerCase();
		widgetFullName = nameTokens.join('');

		/* {{{ create selector for plugin */
		$.expr[":"][widgetFullName] = function (element) {
			return !!$.data(element, widgetFullName);
		};
		/* create selector for plugin }}} */

		// {{{ Create namespaced Widget class
		widgetName = nameTokens.pop();
		// 1/2 Make namespace (we allow nested namespaces)
		$.each(nameTokens, function (index, namePart) {
			namespace = namespace[namePart] = namespace[namePart] || {};
		});
		// 2/2 Make Widget
		namespace[widgetName] = base.extend($.extend(true, {
			widgetName: widgetFullName,
			eventNamespace: widgetFullName.toLowerCase(),
			baseCssClass: baseCssClass
		}, prototype ));
		// Create namespaced Widget class }}}

		// Make bridge to widget
		sf.ui.bridge(widgetFullName, namespace[widgetName]);
	}; // sf.ui.widget()


	/* sf.ui }}}                                                            */
	/* ********************************************************************** */

}(jQuery));

alert('[AF UI] ui.js included');/**
 * Additions to UI Components (Core)
 *
 * @author Maciej Stankiewicz <<a href='mailto:m.stankiewic@samsung.com'>m.stankiewic@samsung.com</a>>
 * @date 2011-11-17
 *
 *
 *******************************************************************************
 *
 * Copyright (c) 2011 Samsung Electronics, Visual Display Division.
 * All Rights Reserved.
 *
 ******************************************************************************/

(function () {
	/**
	 * @namespace
	 */
	sf.ui = sf.ui || {};

	/**
	 * View width
	 * @type Number
	 */
	sf.ui.width = parseInt(curWidget.width, 10);

	/**
	 * View height
	 * @type Number
	 */
	sf.ui.height = parseInt(curWidget.height, 10);

	/**
	 * Grid base unit size
	 * @see sf.ui.height
	 * @type Number
	 */
	sf.ui.grid = sf.ui.height / 9;

	/**
	 * Path to image resources, includes resolution
	 * @type String
	 */
	sf.ui.images = sf.core._afPath.images;

	sf.ui.init = function () {
		alert("[SF UI] init()");
		this.images = sf.core._afPath.images;
	};
}());
/*jslint sloppy: true, white: true, vars: false, undef: true, nomen: true, eqeq: false, plusplus: true, bitwise: true, regexp: true, newcap: true */
/*global sf, alert, curWidget */
/**
 * Apps Framework UI Accordion Component
 *
 * @author  Grzegorz Pabian <<a href='mailto:g.pabian@samsung.com'>g.pabian@samsung.com</a>>
 * @author  Marcin Kierski <<a href='mailto:m.kierski@samsung.com'>m.kierski@samsung.com</a>>
 * @date    21.12.2011
 *
 *
 *****************************************************************************
 *
 * Copyright (c) 2011 Samsung Electronics, Visual Display Division.
 * All Rights Reserved.
 *
 ****************************************************************************/

(function ($) {
	var widgetName = "sfAccordion";
	/**
	 * SF UI Accordion Component
	 * @class
	 * Allows to visualize data as accordion in various templates
	 * @augments sf.ui.Widget
	 */
	sf.ui.Accordion = sf.ui.widgetFactory({
		/** @lends sf.ui.Accordion.prototype */

		/**
		 * Widget name
		 * @default 'sfAccordion'
		 * @type String
		 * @private
		 */
		widgetName: widgetName,

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-accordion'
		 * @type String
		 */
		baseCssClass: "sf-ui-accordion",

		/**
		 * Event namespace
		 * @default "sfaccordion"
		 * @type String
		 */
		eventNamespace: "sfaccordion",

		/**
		 * @type Object
		 * @description
		 * Widget configuration options<br>
		 * Allowed options:
		 * <ul>
		 * <li><b>expandable</b> - enable/disable accordion items expanding</li>
		 * <li><b>focusCssClass</b> - focused item css class</li>
		 * <li><b>index</b> - focused item index</li>
		 * <li><b>items</b> - array of accordion items</li>
		 * <li><b>loop</b> - enable/disable list looping (jumping from first to last element)</li>
		 * <li><b>type</b> - accordion template type, available values: text, text2, text3,
		 * thumbnail2, thumbnail3, video, comment</li>
		 * </ul>
		 */
		options: {
			expandable: true,
			focusCssClass: "sf-ui-accordion-focus",
			index: 0,
			items: [],
			loop: true,
			type: "text"
		},

		/**
		 * @type Object
		 * @description
		 * Default widget templates<br /><br />
		 * <u>Available templates:</u>
		 * text, text2, text3, thumbnail2, thumbnail3, video, comment
		 */
		templates: {
			comment: {
				height: [76, 102, 152],
				thumb: [60, 80, 120],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>' +
					'<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span></div>'
			},
			cursor: {
				size: [4, 6, 8],
				tmpl: "<div></div>"
			},
			line: {
				height: [2, 2, 4],
				tmpl: '<div class="${lineClass}" style="height: ${height}px;">' +
					'<div class="line-left"></div>' +
					'<div class="line-center"></div>' +
					'<div class="line-right"></div>' +
					"</div>"
			},
			list: "<div></div>",
			shadow: {
				size: [30, 40, 60],
				offset: [10, 17, 20],
				tmpl: '<div class="${bcc}-shadow" style="width: ${shadowSize}px; height: ${shadowSize}px; top: -${shadowOffset}px; left: -${shadowOffset}px;">' +
					'<div class="${bcc}-shadow-lt"></div>' +
					'<div class="${bcc}-shadow-t" style="width: ${innerSize}px;"></div>' +
					'<div class="${bcc}-shadow-rt"></div>' +
					'<div class="${bcc}-shadow-r" style="height: ${innerSize}px;"></div>' +
					'<div class="${bcc}-shadow-rb"></div>' +
					'<div class="${bcc}-shadow-b"style="width: ${innerSize}px;"></div>' +
					'<div class="${bcc}-shadow-lb"></div>' +
					'<div class="${bcc}-shadow-l" style="height: ${innerSize}px;"></div>' +
					"</div>"
			},
			text: {
				height: [58, 78, 116],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="right">${right}</span></div>'
			},
			text2: {
				height: [58, 78, 116],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span>' +
					'<span class="right">${right}</span></div>'
			},
			text3: {
				height: [73, 98, 146],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span>' +
					'<span class="bottom">${bottom}</span>' +
					'<span class="right">${right}</span></div>'
			},
			thumbnail2: {
				height: [58, 78, 116],
				thumb: [48, 64, 96],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>${tmpl.text2}'
			},
			thumbnail3: {
				height: [73, 98, 146],
				thumb: [60, 80, 120],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>${tmpl.text3}'
			},
			title: {
				height: [30, 40, 60],
				tmpl: '<span class="title">${title}</span>'
			},
			video: {
				height: [78, 102, 152],
				thumb: [60, 80, 120],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>' +
					'<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span>' +
					'<span class="bottom">${bottom}</span></div>'
			}
		},

		/**
		 * Refresh indicator
		 * @private
		 * @type Boolean
		 */
		doRefresh: false,

		/**
		 * Widget focus state
		 * @private
		 * @type Boolean
		 */
		isFocused: false,

		/**
		 * Height of item used in current template
		 * @private
		 * @type Number
		 */
		itemHeight: 0,

		/**
		 * Visible items array
		 * @private
		 * @type Array
		 */
		items: [],

		/**
		 * Number of displayed items
		 * @private
		 * Number
		 */
		itemsPerPage: 0,

		/**
		 * Used to determine accorion items alignment (top or bottom of widget)
		 * @private
		 * @type Number
		 */
		offset: 0,

		/**
		 * Application resolution: 0 - 540p, 1 - 720p, 2 - 1080p
		 * @private
		 * @type Number
		 */
		resolution: 0,

		/**
		 * List starting index
		 * @private
		 * @type Number
		 */
		start: 0,

		/**
		 * Accordion element title height
		 * @private
		 * @type Number
		 */
		titleHeight: 0,

		/**
		 * Calculated widget height
		 * @private
		 * @type Number
		 */
		widgetHeight: 0,

		/************************************************
		 *					API METHODS					*
		 ************************************************/

		/**
		 * Add item to accordion
		 * @param {Object} item Accordion item to add
		 * @param {Number} [index] Index of added item or it's parent
		 * @example
		 * var item = {text: "Item text"};
		 * $("#Accordion").sfAccordion("addItem", item, 0);
		 */
		addItem: function (item, index) {
			var o = this.options,
				max = o.items.length;

			if (item) {
				if (!isNaN(index)) {
					index = index < 0 ? 0 : index > max ? max : index;
				} else {
					index = max;
				}

				if (o.items[index]) {
					if (item.items) {
						o.items.splice(index, 0, item);
					} else {
						o.items[index].items.push(item);
					}
				} else if (item.items) {
					o.items[index] = item;
				}
			}

			this.start = 0;
			o.index = this._constrainIndex(o.index);
			this._init();

			return this;
		},

		/**
		 * Blur widget elements
		 * @private
		 */
		_blur: function () {
			var v = this.view;

			v.cursor.hide();
			v.focus.hide();
			v.items && v.items.removeClass(this.options.focusCssClass);
			this.isFocused = false;
		},

		/**
		 * Blur widget elements
		 * @example
		 * $("#Accordion").sfAccordion("blur");
		 */
		blur: function () {
			alert("[sf.ui.Accordion] blur()");
			this._blur();
			return this;
		},

		/**
		 * Calculate maximum number of items displayed on page
		 * @private
		 */
		_calculateIPP: function () {
			var wH = this.widgetHeight,
				tH = this.titleHeight;

			this.itemsPerPage = wH >= tH ? Math.ceil(wH / tH) : 0;
		},

		/**
		 * Clear all accordion elements
		 * @example
		 * $("#Accordion").sfAccordion("clear");
		 */
		clear: function () {
			alert("[sf.ui.Accordion] clear()");
			this._blur();
			this.view.wrap.empty();
			this.items = [];
			this.options.items = [];

			return this;
		},

		/**
		 * Collapse accordion item
		 * @private
		 */
		_collapse: function () {
			var index = this.options.index;

			if (this._isTitle(index) && this.items[index].expand) {
				this.items[index].expand = false;
				this._refresh();
			}
		},

		/**
		 * Collapse accordion item
		 * @example
		 * $("#Accordion").sfAccordion("collapse");
		 */
		collapse: function () {
			alert("[sf.ui.Accordion] collapse()");
			if (this.isFocused && this.options.expandable) {
				this._collapse();
			}
			return this;
		},

		/**
		 * Collapse all accordion items
		 * @private
		 */
		_collapseAll: function () {
			var item,
				iLen,
				i;

			for (i = 0, iLen = this.items.length; i < iLen; i++) {
				item = this.items[i];
				if (item && item.items && item.expand) {
					item.expand = false;
				}
			}

			this.doRefresh = true;
			this.start = 0;
			this.offset = 0;
			this.view.list.css("top", this.offset + "px");
			this.options.index = this._findParent();
			this._setVisibleItems();
			this._focus(this.options.index);
		},

		/**
		 * Collapse all accordion items
		 * @example
		 * $("#Accordion").sfAccordion("collapseAll");
		 */
		collapseAll: function () {
			alert("[sf.ui.Accordion] collapseAll()");
			if (this.options.expandable) {
				this._collapseAll();
			}
			return this;
		},

		/**
		 * Constrain index value
		 * @param {Number} index Item index
		 * @returns Constrained index
		 */
		_constrainIndex: function (index) {
			var max = this.items.length - 1;

			return this.options.loop ? (index < 0 ? max : index > max ? 0 : index) :
				(index < 0 ? 0 : index > max ? max : index);
		},

		/**
		 * Destroy component
		 * @private
		 */
		_destroy: function () {
			alert("[sf.ui.Accordion] _destroy()");
			var w = this.widget();

			w.empty();
		},

		/**
		 * Draws mouse over indicator
		 * @param {Number} index Relative index of highlighed item
		 * @private
		 */
		_drawCursor: function (index) {
			var v = this.view,
				top = $(v.items[index]).position().top,
				height;

			height = this._isTitle(index + this.start) ? this.titleHeight : this.itemHeight;

			v.cursor.css({
				"height": height,
				"display": "block",
				"top": top + "px"
			});
		},

		/**
		 * Draw focus indicator
		 * @param {Number} index Relative index of focused item
		 * @param {Number} top Focus top position
		 * @param {Boolean} hide Focus visibility flag
		 * @private
		 */
		_drawFocus: function (index, top, hide) {
			var v = this.view,
				o = this.options,
				display = hide ? "none" : "block";

			v.items && v.items.removeClass(o.focusCssClass);
			$(v.items[index]).addClass(o.focusCssClass);

			v.focus.css({
				"display": display,
				"top": top + "px"
			});
		},

		/**
		 * Draw widget elements
		 * @param {Array} items Array of visible elements
		 * @private
		 */
		_drawItems: function (items) {
			var o = this.options,
				v = this.view,
				t = this.templates,
				bcc = this.baseCssClass,
				itemTemplate = t[o.type].tmpl,
				titleTemplate = t.title.tmpl,
				html = [],
				arrow,
				item,
				iLen,
				i;

			v.wrap.empty();

			for (i = 0, iLen = items.length; i < iLen; i++) {
				item = $.extend({bcc: bcc}, items[i]);
				// draw accordion item title
				if (item.items) {
					arrow = o.expandable ? '<div class="' + (item.expand ? "up" : "down")+ '"></div>' : "";
					html[i] = this._tmpl('<div class="' + bcc + '-item ' +
						bcc + '-title" style="height: ' +
						this.titleHeight + 'px;">' +
						titleTemplate + arrow + "</div>",
						item);
				// draw accordion item
				} else {
					// add styling for thumbnail
					if (item.thumbnail && !item.thumbstyle) {
						item.thumbstyle = this._thumbSize(item.thumbnail);
					}
					html[i] = this._tmpl('<div class="' + bcc + '-item ' +
						'" style="height: ' + this.itemHeight + 'px;">' +
						itemTemplate + "</div>",
						item.thumbnail ? $.extend({shadow: v.shadow}, item) : item);

					// check if separating line is needed
					if (items[i + 1] && !items[i + 1].items) {
						html[i] += v.line;
					}
				}
			}

			v.wrap.html(html.join(""));
			v.items = v.wrap.find("." + bcc + "-item");
		},

		/**
		 * Expand accordion item
		 * @private
		 */
		_expand: function () {
			var index = this.options.index;

			if (this._isTitle(index) && !this.items[index].expand) {
				this.items[index].expand = true;
				this._refresh();
			}
		},

		/**
		 * Expand accordion item
		 * @example
		 * $("#Accordion").sfAccordion("expand");
		 */
		expand: function () {
			alert("[sf.ui.Accordion] expand()");
			if (this.isFocused && this.options.expandable) {
				this._expand();
			}
			return this;
		},

		/**
		 * Expand all accordion elements
		 * @private
		 */
		_expandAll: function () {
			var item,
				iLen,
				id,
				i;

			for (i = 0, iLen = this.items.length; i < iLen; i++) {
				item = this.items[i];
				if (item && item.items && !item.expand) {
					item.expand = true;
				}
			}
			id = this._findParent();
			this.doRefresh = true;
			this._setVisibleItems();
			this.start = 0;
			this.options.index = this._findIndex(id);
			this._focus(this.options.index);
		},

		/**
		 * Expand all accordion elements
		 * @example
		 * $("#Accordion").sfAccordion("expandAll");
		 */
		expandAll: function () {
			if (this.options.expandable) {
				this._expandAll();
			}
			return this;
		},

		/**
		 * Find index of item with provided id
		 * @param {Number} id Item's id
		 * @returns Item's index
		 */
		_findIndex: function (id) {
			var iLen,
				i;

			for (i = 0, iLen = this.items.length; i < iLen; i++) {
				if (this.items[i].id === id) {
					return i;
				}
			}
			return false;
		},

		/**
		 * Find accordion item's parent id
		 * @param {Number} index Item index
		 * @returns Item's parent id
		 */
		_findParent: function (index) {
			var items = this.items,
				i;

			i = index > -1 ? index : this.options.index;

			if (this._isTitle(i)) {
				return items[i].id;
			} else {
				while (items[i]) {
					if (this._isTitle(i)) {
						return items[i].id;
					}
					i--;
				}
				return false;
			}
		},

		/**
		 * Focus on accordion item
		 * @param {Number} index Focused item index
		 * @private
		 */
		_focus: function (index) {
			var start = this.start,
				end = start + this.itemsPerPage;

			if (!isNaN(index)) {
				this.isFocused = true;
				index = this._constrainIndex(index);

				if (index > end - 1) {
					this.doRefresh = true;
					end = index + 1;
					start = end - this.itemsPerPage < 0 ? 0 : end - this.itemsPerPage;
				} else if (index < start) {
					this.doRefresh = true;
					start = index;
					end = start + this.itemsPerPage;
				}

				this.options.index = index;
				this.start = start;
				this._renderPage(start, end);
			}
		},

		/**
		 * Focus on accordion item
		 * @param {Number} [index] Focused item index
		 * @example
		 * $("#Accordion").sfAccordion("focus", 1);
		 */
		focus: function (index) {
			alert("[sf.ui.Accordion] focus()");
			this._focus(typeof index === "undefined" ? this.options.index : index);
			return this;
		},

		/**
		 * Get focused item index
		 * @returns Focused item index
		 * @example
		 * var index = $("#Accordion").sfAccordion("getIndex");
		 * alert("Focused item index = " + index);
		 */
		getIndex: function () {
			return this.options.index;
		},

		/**
		 * Get list item, if no index provided, returns focused element
		 * @param {Number} index Index of item to return
		 * @returns Accordion element
		 * @example
		 * var item = $("#Accordion").sfAccordion("getItem", 1);
		 * alert("Item 1 text = " + item.text);
		 */
		getItem: function (index) {
			alert("[sf.ui.Accordion] getItem()");
			index = index > -1 ? index : this.options.index;
			return this.items[index];
		},

		/**
		 * Calculate focused item's top position
		 * @param {Number} index Focused item index
		 * @param {Array} items Displayed list items
		 * @returns Focused item's top position
		 */
		_getTop: function (index, items) {
			var r = this.resolution,
				t = this.templates,
				lineHeight = t.line.height[r],
				top = 0,
				i;

			for (i = 0; i < index; i++) {
				if (items[i].items) {
					top += this.titleHeight;
				} else {
					top += this.itemHeight;
					if (items[i + 1] && !items[i + 1].items) {
						top += lineHeight;
					}
				}
			}

			return top;
		},

		/**
		 * Initialize component
		 * @private
		 */
		_init: function () {
			alert("[sf.ui.Accordion] _init()");
			var o = this.options,
				v = this.view,
				w = this.widget(),
				t = this.templates;

			this._super();

			w.empty();

			if (typeof o.items !== "object" || (typeof o.items === "object" && !o.items.length)) {
				o.items = [];
			} else {
				this._setVisibleItems();
			}

			if (typeof t[o.type] === "undefined") {
				o.type = "text";
			}

			this._pickResolution();
			this.itemHeight = t[o.type].height[this.resolution];
			this.titleHeight = t.title.height[this.resolution];
			this.widgetHeight = w.height();
			this._calculateIPP();

			v.list = $(t.list)
				.addClass(this.baseCssClass + "-list " + this.baseCssClass + "-list-" + o.type)
				.width(w.width());

			w.append(v.list);

			v.focus = $("<img />")
				.attr("src", sf.ui.images + "/ui.textlist/list_highlighted.png")
				.addClass(o.focusCssClass + "-bg")
				.width(w.width())
				.height(this.itemHeight)
				.css("display", "none");

			v.cursor = $(t.cursor.tmpl)
				.addClass(this.baseCssClass + "-cursor")
				.height(this.itemHeight - t.cursor.size[this.resolution])
				.width(w.width() - t.cursor.size[this.resolution])
				.css("display", "none");

			v.wrap = $("<div></div>")
				.addClass(this.baseCssClass + "-listbox")
				.width(w.width());

			v.list.append(v.focus, v.cursor, v.wrap);

			v.shadow = this._tmpl(t.shadow.tmpl, this._shadowSize());

			v.line = this._tmpl(t.line.tmpl, {
				lineClass: this.baseCssClass + "-line",
				height: t.line.height[this.resolution]
			});

			if (o.items.length && this.itemsPerPage > 0) {
				this._setVisibleItems();
				this.doRefresh = true;
				this._renderPage();
			} else {
				v.wrap.html("No items or not enough space");
			}
			window.wtf = this;
		},

		/**
		 * Check if item is a title
		 * @param {Number} [index] Index of checked item
		 * @returns True if item is title
		 * @private
		 */
		_isTitle: function (index) {
			index = index > -1 ? index : this.options.index;
			return !!(this.items[index] && this.items[index].items);
		},

		/**
		 * Focus on next accordion item
		 * @example
		 * $("#Accordion").sfAccordion("next");
		 */
		next: function () {
			alert("[sf.ui.Accordion] next()");
			this._focus(this.options.index + this.isFocused);
			this._trigger("next");
			return this;
		},

		/**
		 * Pick current application resolution
		 * @private
		 */
		_pickResolution: function () {
			var h = sf.ui.height;
			this.resolution = h === 540 ? 0 : h === 720 ? 1 : h === 1080 ? 2 : 0;
		},

		/**
		 * Focus on previous accordion item
		 * @example
		 * $("#Accordion").sfAccordion("prev");
		 */
		prev: function () {
			alert("[sf.ui.Accordion] prev()");
			this._focus(this.options.index - this.isFocused);
			this._trigger("prev");
			return this;
		},

		/**
		 * Redraw list items
		 * @private
		 */
		_refresh: function () {
			this.doRefresh = true;
			this._setVisibleItems();
			this._calculateIPP();
			this._renderPage();
		},

		/**
		 * Redraw list items
		 * @example
		 * $("#Accordion").sfAccordion("refresh");
		 */
		refresh: function () {
			alert("[sf.ui.Accordion] refresh()");
			this._refresh();
			return this;
		},

		/**
		 * Remove item from accordion.
		 * If no indexes specified, focused element is removed
		 * @param {Number} [index] Index of item to remove
		 * @param {Number} [child] Index of child item to remove
		 * @example
		 * $("#Accordion").sfAccordion("removeItem", 1);
		 */
		removeItem: function (index, child) {
			var o = this.options,
				parent,
				item;

			if (index > -1) {
				if (!isNaN(child)) {
					o.items[index].items.splice(child, 1);
				} else {
					o.items.splice(index, 1);
				}
			} else {
				index = o.index;
				if (this._isTitle(index)) {
					o.items.splice(this.items[index].id, 1);
				} else {
					item = this.items[index];
					parent = o.items[this._findParent(index)];
					parent.items.splice(parent.items.indexOf(item), 1);
				}
			}

			this.start = 0;
			o.index = 0;
			this._init();
		},

		/**
		 * Render accordion page
		 * @param {Number} [start] First shown element index
		 * @param {Number} [end] Last shown element index
		 * @private
		 */
		_renderPage: function (start, end) {
			var o = this.options,
				top,
				items;

			start = typeof start === "undefined" ? this.start : start;
			end = typeof end === "undefined" ? start + this.itemsPerPage : end;
			items = this._setItems(start, end);

			if (start !== this.start || end !== this.start + this.itemsPerPage || this.doRefresh) {
				this._drawItems(items);
				this.doRefresh = false;
			}

			top = this._getTop(o.index - start, items);
			this._setOffset(top);

			if (this.isFocused) {
				this._drawFocus(o.index - start, top, this._isTitle());
			}
			this._trigger("render");
		},

		/**
		 * Returns values of object properties
		 * @param {String} key Object property name
		 * @param {Object} obj Searched object
		 * @private
		 * @returns Object's property value, template or empty string
		 */
		_repl: function (key, obj) {
			var props = key.split(".");

			return typeof obj[props[0]] !== "undefined"
				? typeof obj[props[0]] === "object"
					? typeof obj[props[0]][props[1]] !== "undefined"
						? obj[props[0]][props[1]]
						: ""
					: obj[props[0]]
				: props[0] === "tmpl" && typeof this.templates[props[1]] !== "undefined"
					? this._tmpl(this.templates[props[1]].tmpl, obj)
					: "";
		},


		/**
		 * Prepare set of items to show
		 * @param {Number} start First shown element index
		 * @param {Number} end Last shown element index
		 * @returns Array of items
		 * @private
		 */
		_setItems: function (start, end) {
			return this.items.slice(start, end);
		},

		/**
		 * Set list alignment to top or bottom of widget
		 * @param {Number} top Focused item top position
		 * @private
		 */
		_setOffset: function (top) {
			var o = this.options,
				wHeight = this.widgetHeight,
				oldOffset = this.offset,
				v = this.view,
				iHeight;

			iHeight = this._isTitle() ? this.titleHeight : this.itemHeight;

			/* focused item doesn't fit from bottom */
			if (top + iHeight >= wHeight &&
				/* prevent from scrolling while moving up */
				(wHeight - (top + iHeight + this.templates.line.height[this.resolution]) <= this.offset ||
				/* scroll if narrow items is last visible item and is focused */
				(this._isTitle(o.index) && o.index === this.start + this.itemsPerPage - 1))) {
				this.offset = wHeight - (top + iHeight);
			/* focused item doesn't fit from bottom */
			} else if (top < -this.offset) {
				this.offset = -top;
			}
			/* change css if needed */
			if (this.offset !== oldOffset) {
				v.list.css("top", this.offset + "px");
			}
		},

		/**
		 * Set component option's value
		 * @param {String} name Option's name
		 * @param {Array|Number|String} value Option's value
		 * @private
		 */
		_setOption: function (name, value) {
			this.options[name] = value;
			this._init();
		},

		/**
		 * Checks which elements should be visible
		 * and assign them to widget instance "items" property
		 * @private
		 */
		_setVisibleItems: function () {
			var o = this.options,
				items = o.items,
				exp = o.expandable,
				item,
				iLen,
				jLen,
				i,
				j,
				k = 0;

			this.items = [];

			for (i = 0, iLen = items.length; i < iLen; i++) {
				item = items[i];
				this.items[k++] = item;
				if (item.items) {
					item.id = i;
					if (item.expand || !exp) {
						for (j = 0, jLen = item.items.length; j < jLen; j++) {
							this.items[k++] = item.items[j];
						}
					}
				}
			}
		},

		/**
		 * Calculates shadow size
		 * @returns Object describing thumbnail shadow
		 * @private
		 */
		_shadowSize: function () {
			var o = this.options,
				t = this.templates,
				r = this.resolution,
				tSize = t[o.type].thumb ? t[o.type].thumb[r] : 0,
				border = r === 2 ? 2 : 1,
				s = t.shadow;

			return {
				shadowSize: tSize + 2 * (s.size[r] - s.offset[r]) - 2 * border,
				innerSize: tSize - 2 * s.offset[r]  - 2 * border,
				shadowOffset: s.size[r] - s.offset[r],
				bcc: this.baseCssClass
			};
		},

		/**
		 * Calculate size for thumbnail
		 * @param {Object} thumb Thumbnail to tyle
		 * @returns Thumbnail style
		 * @private
		 */
		_thumbSize: function (thumb) {
			var o = this.options,
				t = this.templates,
				tSize = t[o.type].thumb ? t[o.type].thumb[this.resolution] : 0,
				width,
				height,
				top = 0,
				left = 0,
				ratio;

			if (thumb && thumb.width && thumb.height) {
				ratio = thumb.width / thumb.height;
				if (ratio < 1) {
					width = tSize;
					height = Math.round(width / ratio);
					top = Math.round((tSize - height) / 2);
				} else {
					height = tSize;
					width = Math.round(height * ratio);
					left = Math.round((tSize - width) / 2);
				}
				return "width: " + width + "px; height: " + height +"px;" +
					"top: " + top + "px; left: " + left + "px;";
			} else {
				return this.resolution === 2 ? "top: -2px; left: -2px;" : "top: -1px; left: -1px;";
			}
		},

		/**
		 * Replace markup with object properties
		 * @param {String} tmpl Template markup
		 * @param {Object} obj Searched object
		 * @returns Parsed string with object values
		 * @private
		 */
		_tmpl: function (tmpl, obj) {
			var that = this;

			return typeof tmpl === "string" && typeof obj === "object" ?
				tmpl.replace(/\$\{([a-zA-Z0-9_\.]*)\}/g, function (m, key) {
					return that._repl(key, obj, m);
				}) : "";
		},

		/**
		 * Toggle expand accordion item
		 * @param {Number} index Index of toggled item
		 * @example
		 * $("#Accordion").sfAccordion("toggle", 2);
		 */
		toggle: function (index) {
			alert("[sf.ui.Accordion] toggle()");
			if (this.options.expandable) {
				this._toggleExpand(index);
			}
			return this;
		},

		/**
		 * Toggle expand accordion item
		 * @param {Number} [index] Index of toggled item
		 * @private
		 */
		_toggleExpand: function (index) {
			index = index > -1 ? index : this.options.index;

			if (this._isTitle(index)) {
				this.items[index].expand = !this.items[index].expand;
				this._refresh();
			}
		}
	});

	sf.ui.bridge(widgetName, sf.ui.Accordion);

	sf.ui.addSelector(widgetName);

}(jQuery));/*
 * Title: UI Components (Button)
 * 	Apps Framework Button UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfButton";

	/*
		Class: sf.ui.Button (jQuery.sfButton)
		Apps Framework Class for Button UI

		Function: jQuery.sfButton
			Generates button or button set.

		Rendered HTML:
			If the "text" option is String
			(start code)
			Before init: <div id="button"></div>
			After  init: <div id="button" class="sf-ui-button sf-ui-button-blur" >
				              <div class="sf-ui-button-l" ></div>
				              <div class="sf-ui-button-c sf-ui-common-ellipsis">text</div>
				              <div class="sf-ui-button-r"></div>
				         </div>
			(end)

			Or if the "text" option is Array of String
			(start code)
			Before init:	<div id="buttons"></div>
			After  init:	<div id="buttons" class="sf-ui-button sf-ui-button-blur">
								<div class="sf-ui-button-l" ></div>
								<div class="sf-ui-button-c sf-ui-common-ellipsis">text 1</div>
								<div class="sf-ui-button-c sf-ui-common-ellipsis">text 2</div>
								<div class="sf-ui-button-c sf-ui-common-ellipsis">text 3</div>
								<div class="sf-ui-button-r"></div>
							</div>
			(end)

		Example 1
			Button setOption() method usage example (change buttons text in button set):</u>
			(start code)
			$('#buttons').sfButton({,
			 	text: [ "Button 1", "Button 2", "Button 3" ]
			});
			$('#buttons').sfButton("option", "text", ["new text 1", "new text 2", "new text 3"]);
			(end)

			*Rendered HTMLS*
			(start code)
			Before: <div id="button" class="sf-ui-button sf-ui-button-blur">
				         <div class="sf-ui-button-l"></div>
				         <div class="sf-ui-button-c sf-ui-common-ellipsis">text 1</div>
				         <div class="sf-ui-button-c sf-ui-common-ellipsis">text 2</div>
				         <div class="sf-ui-button-c sf-ui-common-ellipsis">text 3</div>
				         <div class="sf-ui-button-r"></div>
				     </div>

			After:  <div id="button" class="sf-ui-button sf-ui-button-blur">
				         <div class="sf-ui-button-l"></div>
				         <div class="sf-ui-button-c sf-ui-common-ellipsis">new text 1</div>
				         <div class="sf-ui-button-c sf-ui-common-ellipsis">new text 2</div>
				         <div class="sf-ui-button-c sf-ui-common-ellipsis">new text 3</div>
				         <div class="sf-ui-button-r"></div>
				     </div>
			(end)
	*/
	sf.ui.Button = sf.ui.widgetFactory({
		widgetName: widgetName,

		eventNamespace: "sfbutton",

		baseCssClass: 'sf-ui-button',

		options: {
			text: [''],
			leftCssClass: 'sf-ui-button-l',
			centerCssClass: 'sf-ui-button-c',
			rightCssClass: 'sf-ui-button-r',
			widgetFocusCssClass: 'sf-ui-button-focus'
		},

		templates: {
			left: '<div class="${leftCssClass}"></div>',
			center: '<div class="${centerCssClass} sf-ui-common-ellipsis">${label}</div>',
			right: '<div class="${rightCssClass}"></div>'
		},

		buttonNum: 0,

		_init: function() {
			alert("[sf.Button] _init()");
			var o = this.options,
				v = this.view,
				t = this.templates,
				data = [];

			this.widget().css('visibility', 'hidden');	// 스타일 수정과정을 보이지않게

			if ( !o.text ) {
				this._destroy();
			}

			if (typeof o.text === 'string') {
				o.text = [o.text];
			}

			this.buttonNum = o.text.length;

			$.each(o.text, function(index, value){
				data.push({
					centerCssClass  : o.centerCssClass,
					label : value
				});
			});

			this._clearHtml();

			v.buttons   = $.tmpl(t.center, data);
			v.leftSide  = $.tmpl(t.left, {leftCssClass: o.leftCssClass || ''});
			v.rightSide = $.tmpl(t.right,{rightCssClass: o.rightCssClass || ''});

			this.blur();

			this._makeButtons();
			this._adjustButtonSpacing();
			this.widget().css('visibility', 'visible');
			/*
			this.widget().find('.sf-ui-button-c').each(function (key, value) {
				alert("key: " + key);
				$(this).bind('mouseover', function () {
					$(this).sfButton('focus', key);
				});
				$(this).bind('mouseout', function () {
					$(this).sfButton('blur');
				});
			});
			*/
		},

		/**
		 * Adds compiled templates to DOM
		 * @private
		 */
		_makeButtons: function() {
			alert("[sf.Button] _makeButtons()");
			var v = this.view,
				w = this.widget();

			$.each([v.leftSide, v.buttons, v.rightSide ], function(index,value){
					value.appendTo(w);
			});

			/*w.append(v.leftSide, v.buttons, v.rightSide);

			$.each([v.leftSide, v.buttons, v.rightSide ], function(index,value){
					value.appendTo(w);
			});*/
		}, // _makeButtons()


		/**
		 * Adjusts buttons width <br /><br />
		 * In detail: <br />
		 * Button width is adjusted so that width of all buttons won't exceed wrapper container. <br />
		 * Note: wrapper container is element on which component was instantiated.
		 * @private
		 */
		_adjustButtonSpacing: function() {
			alert("[sf.Button] _adjustButtonSpacing()");
			var v = this.view,
				w = this.widget(),
				contSide = 0,
				self = this;

			$.each( [v.leftSide, v.rightSide], function( index,value ) {
				if ( value ) {
					contSide += parseInt(value.css('width'), 10 );
				}
			});

			if ( this.buttonNum === 0 ) {
				throw "no buttons found";
			}

			v.buttons.width(( w.width() - contSide ) / self.buttonNum);

		}, // _adjustButtonSpacing()


		/**
		 * Removes elements added by button. <br />
		 */
		_clearHtml: function(){
			alert("[sf.Button] _clearHtml()");
			var v = this.view;

			$.each(v, function(index,value){
				if ( value ) {
					value.remove();
				}
			});
		}, // _clearHtml()


		/**
		 * @private
		 */
		_setOption: function(name, value) {
			alert("[sf.Button] _setOption(" + name + ", " + value + ")");
			var o = this.options,
				v = this.view;

			switch (name) {
				case 'text':
					o.text = ((typeof value) == 'string') ? [value] : value;
					$.each(o.text, function(index) {
						v.buttons.eq(index).html(o.text[index]);
					});
					break;
				case 'leftCssClass':	// INTENTIONAL FALLBACK
				case 'centerCssClass': // INTENTIONAL FALLBACK
				case 'rightCssClass':
					if (value) {
						$.each(v, function(i, element){
							if ( element.hasClass(o[name]) ){
								element
									.removeClass(o[name])
									.addClass(value);
							}
							o[name] = value;
						});
					}
					break;
			} // switch(name)
		}, // _setOption()

		_destroy: function() {
			alert("[sf.adjustButtonSpacing] _destroy()");
			var w = this.widget(),
				o = this.options;

			w.removeClass(o.widgetFocusCssClass);

			this._clearHtml();
		}, // _destroy()

		/*
		Function: focus
		 Sets focus on particular button

		Parameters:
		 o (Number) index - n-th item to be focused (default:0)

		Returns:
		 object - jQuery

		Description:
		 Adds 'focus' CSS class to choosed button.
		 Adds CSS class provided widgetFocusCssClass.
		 Removes 'focus' CSS class from any focused button and sets focus on choosed button.
		 It also sets 'focus CSS class on elements containing rounded corners if focused button is
		 side button in whole button set.

		Example 1:
		 This example set focus on particular button
		 (start code)
		  $('#buttons').sfButton({,
			text : [ "Button 1", "Button 2", "Button 3" ]
		  });
		  $('#button1').sfButton('focus');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		focus: function(index) {
			alert("[sf.adjustButtonSpacing] focus("+index+")");
			var w = this.widget(),
				v = this.view,
				o = this.options;

			w.addClass(o.widgetFocusCssClass);

			if ( !index ) {
				index = 0;
			}

			if( index < 0 || index >= this.buttonNum) {
				alert("[AF ui] wrong sfButton focus index");
				return this;
			}

			//remove class 'focus' from left, center and right parts
			$.each(v, function(i, value){
				value.removeClass('focus');
			});

			v.buttons.eq(index).addClass('focus');

			if(index === 0) {
				v.leftSide.addClass('focus');
			}

			if(index === this.buttonNum - 1) {
				v.rightSide.addClass('focus');
			}

			return this;
		}, // focus()


		/*
		Function: blur
		 Removes focus state from component and ads blur state

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Removes CSS class provided in *widgetFocusCssClass*

		Example 1:
		 This example blur focus on particular button
		 (start code)
		  $('#buttons').sfButton({,
			text : [ "Button 1", "Button 2", "Button 3" ]
		  });
		  $('#button1').sfButton('blur');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		blur: function() {
			alert("[sf.adjustButtonSpacing] blur()");
			var o = this.options,
				w = this.widget();

			w.removeClass(o.widgetFocusCssClass);

			return this;
		} // blur()

	}); // sf.ui.Button()

	sf.ui.bridge(widgetName, sf.ui.Button);

    sf.ui.addSelector(widgetName);

}(jQuery));/*
 * Title: UI Components (Checkbox)
 * Apps Framework Checkbox UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($, undefined) {

	var widgetName = "sfCheckBox";

	/**
	 * @class
	 * @augments sf.ui.Widget
	 * @example
	 * example of Use:
	 *
	 * $('#checkbox').sfCheckBox();
	 * $('#checkbox').sfCheckBox('check');
	 * $('#checkbox').sfCheckBox('uncheck');
	 */

	/*
		Class: sf.ui.CheckBox (jQuery.sfCheckBox)
		Apps Framework Class for checkbox UI

		Function: jQuery.sfCheckBox

		Rendered HTML:
			Before init:
				<div id="checkbox3"></div>

			After init:
				<div id="checkbox3">
				    <div class="sf-ui-checkbox sf-ui-checkbox-unchecked"></div>
				</div>

		Example 1:
		(start code)
		$('#checkbox').sfCheckBox();
		$('#checkbox').sfCheckBox('check');
		$('#checkbox').sfCheckBox('uncheck');
		(end)
	*/
	sf.ui.CheckBox = sf.ui.widgetFactory({
		/* @lends sf.ui.CheckBox.prototype */
		/** @lends sf.ui.CheckBox.prototype */

		/**
		 * Widget name
		 * @default 'sfuiCheckBox'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Event namespace added by default to every event triggered by Scroll.
		 * @default "sfcheckbox"
		 * @type String
		 */
		eventNamespace: "sfcheckbox",


		/**
		 * Object with templates used to render checkbox.
		 * <dl class="detailList">
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>item</b> (defaults to '&lt;div&gt;&lt;/div&gt;')
		 *   </dt>
		 *   <dd>template for checkbox item</dd>
		 * </dl>
		 * @type Object
		 */
		templates: {
			item: $.template(null, '<div></div>')
        },

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-icon'
		 * @type String
		 */
		baseCssClass: 'sf-ui-checkbox',

		/**
		 *
		 * default options:
		 * @example
		 * icon: sf-ui-icon
		 * checkClass: sf-ui-icon-checkbox-checked
		 * uncheckClass: sf-ui-icon-checkbox-unchecked
		 * focusClass: sf-ui-icon-checkbox-focused
		 */
        options: {
            icon: 'sf-ui-checkbox',
            checkClass: 'sf-ui-checkbox-checked',
            uncheckClass: 'sf-ui-checkbox-unchecked',
            focusClass: 'sf-ui-checkbox-focused'
        },
		/**
		 * create element and sets all the required options
		 */
        _create: function () {
			    alert("[sf.CheckBox] _create()");
				$.tmpl(this.templates.item).appendTo(this.widget());
				this.widget().children().first()
					.addClass(this.options.icon+' '+this.options.uncheckClass);
        },

		/*
		Function: focus
		 	sets focus on the element

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 sets focus on the element

		Example 1:
		 This example sets focus on the element
		 (start code)
		  $('#checkbox').sfCheckBox('focus');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
        focus: function () {
			alert("[sf.CheckBox] focus()");
			this.widget().addClass(this.options.focusClass);

			return this;
        },

		/*
		Function: blur
		 removes the focus from the element

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 removes the focus from the element

		Example 1:
		 This example removes the focus from the element
		 (start code)
		  $('#checkbox').sfCheckBox('blur');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
        blur: function () {
			alert("[sf.CheckBox] blur()");
            this.widget().removeClass(this.options.focusClass || '');
			return this;
        },

		/*
		Function: check
		 check element

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 check element

		Example 1:
		 This example check element
		 (start code)
		  $('#checkbox').sfCheckBox('check');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		check: function () {
			alert("[sf.CheckBox] check()");
            this.widget().children().first().removeClass(this.options.uncheckClass || '').addClass(this.options.checkClass);
			return this;
        },


		/*
		Function: uncheck
		 uncheck element

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 uncheck element

		Example 1:
		 This example uncheck element
		 (start code)
		  $('#checkbox').sfCheckBox('uncheck');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
        uncheck: function () {
			alert("[sf.CheckBox] uncheck()");
            this.widget().children().first().removeClass(this.options.checkClass || '').addClass(this.options.uncheckClass);
			return this;
        },


		/*
		Function: getChecked
		 Get whether the checkbox is checked or not.

		Parameters:
		 N/A

		Returns:
		 (Boolean) - true if checked, false if unchecked.

		Example 1:
		 This example uncheck element
		 (start code)
		  $('#checkbox').sfCheckBox('getChecked');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
        getChecked: function () {
			alert("[sf.CheckBox] getChecked()");
			return this.widget().children().first().hasClass(this.options.checkClass);
        }
    });

	sf.ui.bridge(widgetName, sf.ui.CheckBox);
    sf.ui.addSelector(widgetName);
}(jQuery));
/*
 * Title: UI Components (Image)
 * Apps Framework Image UI Components
 *
 * Version added:
 * 1.1.0
 */

/*
   Class: sf.ui.Image (jQuery.sfImage)
   Apps Framework Class for Image UI
*/

(function ($) {

    var widgetName = "sfImage";

    /**
    * Allows to change 'src' attribute of element
    * @class
    * @augments sf.ui.Widget
    */
    sf.ui.Image = sf.ui.widgetFactory({
        /** @lends sf.ui.Image.prototype */

        widgetName: widgetName,

        /**
        * Widget base CSS class
        * @default Empty String
        * @type String
        */
        baseCssClass: '',

		/*
		 	Function: jQuery.sfImage
		 		shows the image.

			Example 1:
				sfImage initializing example
				(start code)
		        $("#imageAnchor").sfImage({
		               'src' : '/images/bg.jpg'
		        });
				(end)

		     	*Rendered HTMLs*
		     	(start code)
		        Before: < img id="imageAnchor" >   ...   < /img>
		        After : < img id="imageAnchor" src="/images/bg.jpg" >  ...  < /img>
				(end)

			Example 2:
				sfImage *setOption()* method usage example (change 'src' attribute)
				(start code)
				$("#imageAnchor").sfImage("option", "src", "/icons/pinkIcon.png");
				(end)

				*Rendered HTMLs*
				(start code)
				Before: < img id="imageAnchor" src="/images/bg.jpg" >       ...  < /img>
				After : < img id="imageAnchor" src="/icons/pinkIcon.png" >  ...  < /img>
				(end)

			Example 3:
				sfImage *destroy* method usage example
				(start code)
		        $("#imageAnchor").sfImage({
		               'src' : '/pictures/pic.jpg'
		        });

        		$("#imageAnchor").sfImage("<b>destroy</b>");
		        (end)

     			*Comments* : Destroy method restores 'src' attribute to 'before initialization' state
				*Rendered HTMLs*
				(start code)
		        Before init:    < img id="imageAnchor" src="/images/bg.jpg" >    ...  < /img>
		        After  init:    < img id="imageAnchor" src="/pictures/pic.jpg" > ...  < /img>
		        After  destroy: < img id="imageAnchor" src="/images/bg.jpg" >    ...  < /img>
        		(end)
 		*/
        options: {
            src: ''
        },

        /**
        * Work as a constructor.
        * Stores original 'src' attribute value in 'initialState' object
        *
        * @private
        **/
        _create: function () {
            alert("[sf.sfImage] _create() ");

            this._super();
            this.initialState.src = this.element.attr('src');
            this._updateSrc();
        }, // _create()


        /**
        * Restores element to original state.
        * In details: </br>
        *    Restore 'src' parameter value set during initialization in '_create()' method
        *
        * @private
        **/
        _destroy: function () {
            alert("[sf.sfImage] _destroy()");

            this.element.attr('src', this.initialState.src);
        }, // _destroy()


        /**
        * @private
        **/
        _setOption: function (name, value) {
            alert("[sf.sfImage] _setOption(" + name + ", " + value + ")");

            switch ( name ) {
                case "src":
                    this.options.src = value;
                    this._updateSrc();
                break;
            } // switch( name )

            return this;
        }, // _setOption()


        /**
        * Validate and update 'src' attribute of element
        * @private
        **/
        _updateSrc: function () {
            if ( typeof ( this.options.src ) === 'string' ) {
                this.element.attr('src', this.options.src);
            }
        } // _updateSrc()

    }); // sf.ui.Image()

    sf.ui.bridge(widgetName, sf.ui.Image);

    sf.ui.addSelector(widgetName);
}(jQuery));/*
 * Title: UI Components (KeyHelp)
 * Apps Framework KeyHelp UI Components
 *
 * Version added:
 * 1.1.0
 */



(function ($) {

    var widgetName = "sfKeyHelp";

    var templates = {
        item: $.template('sfKeyHelpTpl_item',
            '{{if margin}}'+
            '<td class="sf-ui-keyhelp-margin-items"></td>'+
            '{{/if}}'+
            '<td class="sf-ui-keyhelp-icon ${iconclass}" key="${key}"></td>'+
            '<td class="sf-ui-keyhelp-margin-icon" key="${key}"></td>'+
            '<td class="sf-ui-keyhelp-text" key="${key}">${label}</td>'
        ),

        main: $.template(null,
            '<table cellpadding="0px" cellspacing="0px" border="0px" valign="middle" style="width:100%;height:100%;"><tr>'+
            '<td class="sf-ui-keyhelp-margin-left"></td>'+
            '{{if userItem}}'+
	            '<td style="text-align:left;">'+
	            '<table cellpadding="0px" cellspacing="0px" border="0px" valign="middle" align="left" class="sf-ui-keyhelp-userid"><tr>'+
	            '{{tmpl(userItem) "sfKeyHelpTpl_item"}}'+
	            '</tr></table>'+
	            '<td style="width:10px;"></td>'+
	            '</td>'+
            '{{/if}}'+
            '<td style="text-align:right;">'+
            '<table cellpadding="0px" cellspacing="0px" border="0px" style="table-layout:fixed;" align="right"><tr>'+
            '{{tmpl(items) "sfKeyHelpTpl_item"}}'+
            '<td class="sf-ui-keyhelp-margin-right"></td>'+
            '</tr></table>'+
            '</td>'+
            '</tr></table>'
        ),

		notification: $.template(null, '<div class="notification">'+
			'<div class="bg"><div class="left"></div><div class="right"></div></div>'+
			'<div class="icons"></div>'+
			'<div class="text sf-ui-common-ellipsis"></div>'+
			'</div>'
		)
    }; //templates

	var iconkeymap = {
		RED: sf.key.RED,
		GREEN: sf.key.GREEN,
		YELLOW: sf.key.YELLOW,
		BLUE: sf.key.BLUE,
		INFO: sf.key.INFO,
		TOOLS: sf.key.TOOLS,
		PLAY: sf.key.PLAY,
		STOP: sf.key.STOP,
		PAUSE: sf.key.PAUSE,
		ENTER: sf.key.ENTER,
		RETURN: sf.key.RETURN,
		REW: sf.key.REW,
		FF: sf.key.FF
	};

	/*
   		Class: sf.ui.KeyHelp (jQuery.sfKeyHelp)
   			Apps Framework Class for KeyHelp UI
	*/
    sf.ui.KeyHelp = sf.ui.widgetFactory({
        /** @lends sf.ui.KeyHelp.prototype */

        widgetName: widgetName,

        /**
        * Widget base CSS class
        * @default 'sf-ui-keyhelp'
        * @type String
        */
        baseCssClass: 'sf-ui-keyhelp',

		/*
			Function: jQuery.sfKeyHelp
				This is the Apps Framework class for KeyHelp UI components.

			Parameter:
				o (Object) options - options for keyhelp
					theme - (Object) Allows to choose one of defined UI theme set. Avaliable sets : 'WHITE', 'BLACK', 'TRANSPARENT'.(Default Value: 'WHITE')
					USER - (String) Place here the name of logged user. Usefull when application requires login operation. If the fileld is not added then the icon is not displayed.
					NUMBER - (String) Label for icon informing that number buttons can be used
					IME - (String) Label for icon informing that IME control can be used
					TOOLS - (String) Label for icon informing that TOOLS button can be used
					INFO - (String) Label for icon informing that INFO button can be used
					ENTER - (String) Label for icon informing that ENTER button can be used
					RETURN - (String) Label for icon informing that RETURN button can be used
					UPDOWN - (String) Label for icon informing that UPDOWN buttons can be used
					INFO - (String) Label for icon informing that INFO button can be used
					LEFTRIGHT - (String) Label for icon informing that LEFT and RIGHT buttons can be used
					MOVE - (String) Label for icon informing that MOVE button can be used
					REW - (String) Label for icon informing that REW button can be used
					REWFF - (String) Label for icon informing that REWFF button can be used
					FF - (String) Label for icon informing that FF button can be used
					PLAY - (String) Label for icon informing that PLAY button can be used
					PAUSE - (String) Label for icon informing that PAUSE button can be used
					STOP - (String) Label for icon informing that STOP button can be used
					RED - (String) Label for icon informing that RED button can be used
					GREEN - (String) Label for icon informing that GREEN button can be used
					YELLOW - (String) Label for icon informing that YELLOW button can be used
					BLUE - (String) Label for icon informing that BLUE button can be used

			Returns:
				(Object) jQuery object

			Rendered HTML:
				Before init:
				(start code)
				<div id="keyhelp"></div>
				(end)

				Initialize:
				(start code)
				$('#keyhelp').sfKeyHelp({
					'user': 'keyHelp',
					'enter': 'Enter',
					'red':'red',
			        'green' : 'green',
			        'yellow':'yellow',
			        'blue':'blue',
			        'tools':'tools',
			        'leftright':'left/right',
			        'return': 'Return'
				});
				(end)

				After init:
				(start code)
				<div id="keyhelp" class="sf-ui-keyhelp" style="display:block;">
				    <table cellpadding="0px" cellspacing="0px" border="0px" valign="middle" style="width:100%;height:100%;">
					    <tr>
					        <td class="sf-ui-keyhelp-margin-left"></td>
					        <td style="text-align:left;">
					            <table cellpadding="0px" cellspacing="0px" border="0px" valign="middle" align="left" class="sf-ui-keyhelp-userid">
					                <tr>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-user"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">keyHelp</td>
					                </tr>
					            </table>
					        </td>
					        <td style="width:10px;"></td>
					        <td style="text-align:right;">
					            <table cellpadding="0px" cellspacing="0px" border="0px" style="table-layout:fixed;" align="right">
					                <tr>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-enter"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">Enter</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-red"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">red</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-green"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">green</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-yellow"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">yellow</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
										<td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-blue"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">blue</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
										<td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-tools"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">tools</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-leftright"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">left/right</td>
					                    <td class="sf-ui-keyhelp-margin-items"></td>
					                    <td class="sf-ui-keyhelp-icon sf-ui-keyhelp-icon-return"></td>
					                    <td class="sf-ui-keyhelp-margin-icon"></td>
					                    <td class="sf-ui-keyhelp-text">Return</td>
					                    <td class="sf-ui-keyhelp-margin-right"></td>
					                </tr>
					            </table>
					        </td>
					    </tr>
					</table>
				</div>
				(end)
		 */
		options: {
			theme: 'black'
        },

		themeClasses: {'WHITE':'sf-ui-keyhelp-white', 'TRANSPARENT':'sf-ui-keyhelp-transparent', 'BLACK':'sf-ui-keyhelp-black'},
		themeCssClass: null,

        iconSet: {
            USER        : 'sf-ui-keyhelp-icon-user',
            NUMBER      : 'sf-ui-keyhelp-icon-number sf-ui-keyhelp-icon-long',
            IME         : 'sf-ui-keyhelp-icon-ime',
            TOOLS       : 'sf-ui-keyhelp-icon-tools',
            INFO        : 'sf-ui-keyhelp-icon-info',
            ENTER       : 'sf-ui-keyhelp-icon-enter',
            RETURN      : 'sf-ui-keyhelp-icon-return',
            UPDOWN      : 'sf-ui-keyhelp-icon-updown',
            LEFTRIGHT   : 'sf-ui-keyhelp-icon-leftright',
            MOVE        : 'sf-ui-keyhelp-icon-move',
            REW         : 'sf-ui-keyhelp-icon-rew',
            REWFF       : 'sf-ui-keyhelp-icon-rewff sf-ui-keyhelp-icon-long',
            FF          : 'sf-ui-keyhelp-icon-ff',
            PLAY        : 'sf-ui-keyhelp-icon-play',
            PAUSE       : 'sf-ui-keyhelp-icon-pause',
            STOP        : 'sf-ui-keyhelp-icon-stop',
            RED         : 'sf-ui-keyhelp-icon-red',
            GREEN       : 'sf-ui-keyhelp-icon-green',
            YELLOW      : 'sf-ui-keyhelp-icon-yellow',
            BLUE        : 'sf-ui-keyhelp-icon-blue'
        }, // _iconClass


        _create: function () {
            alert("[sf.ui.KeyHelp] _create() ");
            this._super();
        }, // _create

        _init: function () {
            alert("[sf.ui.KeyHelp] _init() ");

            var o = this.options,
                w = this.widget();

            this._super();

            //fill templates and append to element
            try {
				if(o.theme) {
					o.theme = o.theme.toUpperCase();
					w.removeClass(this.themeCssClass);
					this.themeCssClass = this.themeClasses[o.theme];
					w.addClass(this.themeCssClass);
				}

                //rest options are appended via templates
                this._redraw();

            } catch(e) {
                alert("[sf.KeyHelp] _init() Exception:" + e);
                this.destroy();
            }
        }, // _init()


        /**
        * Restores element to original state.
        * In details: </br>
        * Removes 'ICONSET' theme and restore 'background-image' style property.
        * Clear temporary data object.
        *
        * @private
        **/
        _destroy: function () {
            alert("[sf.ui.KeyHelp] _destroy() ");

            var i = this.initialState,
                w = this.widget(),
                d = this.data;

            w.empty()
             .removeClass(this.themeCssClass);
        }, // _destroy()

        _redraw: function () {
            alert("[sf.ui.KeyHelp] _redraw() ");
            //
			var data = {
				items: []
			};
			var w = this.widget(),
				o = this.options;

			for(var key in o) {
				var icon = key.toUpperCase();
				alert(key + " = " + o[key]);
				if(this.iconSet[icon]) {
					if (icon == 'USER') {
						data.userItem = {
							margin: false,
							iconclass: this.iconSet[icon],
							label: o[key],
							key: icon
						}
					}
					else {
						data.items.push({
							margin: data.items.length > 0,
							iconclass: this.iconSet[icon],
							label: o[key],
							key: icon
						});
					}
					delete o[key];
				}
			}
			w.empty();
			$.tmpl(templates.main, data).appendTo(w);

			for(var key in iconkeymap) {
				key = key.toUpperCase();
				this.widget().find('td[key="'+key+'"]').each(function () {
					//alert("bind handler: " + key);
					$(this).bind('click', (function (key) {
						return function(){
							//console.warn("KeyHelp item pressed: " + key + "(" + iconkeymap[key] + ")");
							fireKeyEvent(iconkeymap[key]);
						}
					})(key)).css('cursor', 'pointer');
				});
			}
        }, // _redraw()

		/*
		 * Function: showNotice
		 * 	This function shows notification message on left side of KeyHelp.
		 *
		 * Paramters:
		 * 	o options - (Object) Options for notification.
		 * 		(start code)
		 * 		{
		 * 			icons: ['voice', 'gesture', 'mouse'],	// (Array<String>) Icons of notification. This icon will be shown on the right of text. ('voice', 'gesture', 'mouse', 'printer')
		 * 			text: 'Available',						// (String) Text of notification.
		 * 			timeout: 5000							// Timeout of hiding notification in milliseconds. Sets no timeout if you pass 0 for this.
		 * 		}
		 * 		(end)
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Example:
		 * 	(start code)
		 * 	$("#keyhelp1").sfKeyHelp('showNotice', {icons:['voice', 'gesture', 'mouse'], text: 'Available', timeout: 5000});
		 * 	(end)
		 */
		showNotice: function (options) {
			alert('[sf.ui.KeyHelp] showNotice()');
			var w = this.widget(),
				o = this.options,
				v = this.view;
			if(!options.icons) {
				options.icons = [];
			}
			if(!options.text) {
				options.text = '';
			}
			if(!options.timeout && options.timeout !== 0) {
				options.timeout = 5000;
			}

			alert(w.find('.notification').get(0));
			if(!w.find('.notification').get(0)) {
				$.tmpl(templates.notification, null).appendTo(w);
				v.notification = w.find('.notification');
			}
			//var width = v.notification.width();
			v.notification.show().css('opacity', '0.0');

			v.notification.find('.icons').empty();
			for(var i=0; i<options.icons.length; i++) {
				alert("Icon : " + options.icons[i]);
				v.notification.find('.icons').append('<div class="'+options.icons[i]+'"></div>');
			}

			var iconsWidth = 0;
			v.notification.find('.icons div').each(function() {
				iconsWidth += $(this).outerWidth(true);
			});
			alert("Icons Width : " + iconsWidth);
			v.notification.find('.icons').width(iconsWidth);
			iconsWidth = v.notification.find('.icons').outerWidth(true);

			alert("Text : " + options.text);
			var bgWidth = v.notification.find('.bg .left').outerWidth(true);
			v.notification.find('.text').html(options.text).css({
				'left': iconsWidth + 'px',
				'width': (bgWidth - iconsWidth) + 'px'
			});

			///*
			var _THIS_ = this;
			if(this.hideTimeout) {
				clearTimeout(this.hideTimeout);
				this.hideTimeout = null;
			}
			v.notification.stop(true).animate({
				'opacity': '1'
			}, {
				duration: 500,
				complete: function () {
					alert("Noti show complete!!!");
					if (options.timeout > 0) {
						alert("Sets noti hide timeout: " + options.timeout);
						_THIS_.hideTimeout = setTimeout(function(){
							_THIS_.hideTimeout = null;
							v.notification.stop(true).animate({
								opacity: 0
							}, {
								duration: 500,
								complete: function(){
									alert("Noti hide complete!!!");
								}
							});
						}, options.timeout);
					}
				}
			});
			//*/

			alert('[sf.ui.KeyHelp] showNotice() END');
		},

		/*
		 * Function: hideNotice
		 * 	This function hides notification message of KeyHelp. This function ignore the hiding timeout passed to 'showNotice' method.
		 *
		 * Paramters:
		 * 	N/A
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Example:
		 * 	(start code)
		 * 	$("#keyhelp1").sfKeyHelp('hideNotice');
		 * 	(end)
		 */
		hideNotice: function () {
			alert('[sf.ui.KeyHelp] hideNotice()');
			var w = this.widget();
			if (w.find('.notification').get(0)) {
				w.find('.notification').hide();
			}
			alert('[sf.ui.KeyHelp] hideNotice() END');
		}

    }); // sf.ui.KeyHelp()

	function fireKeyEvent(keycode) {
		if (document.createEvent) {
			//console.warn("using document.createEvent");
			var eventObj = document.createEvent('Events');
			eventObj.initEvent('keydown', true, true);
			eventObj.keyCode = keycode;
			// this event dispatcher is changed to work fin with wibug
			document.getElementById("_sf_master_anchor_").dispatchEvent(eventObj);
		}
		else if (document.createEventObject) {
			//console.warn("using document.createEventObject");
			var eventObj = document.createEventObject();
			eventObj.keyCode = keycode;
			document.activeElement.fireEvent("onkeydown", eventObj);
		}
	}

    sf.ui.bridge(widgetName, sf.ui.KeyHelp);

    sf.ui.addSelector(widgetName);

}(jQuery));
/*
 * Title: UI Components (Label)
 * Apps Framework Label UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfLabel";

	/*
		Class: sf.ui.Label (jQuery.sfLabel)
			Apps Framework Class for Label UI
	*/
	/*
		Function: jQuery.sfLabel
			Adds simple text to element.

		Example 1:
		    Label initializing example
			(start code)
	        $('#label').sfLabel({,
	            text: "My label text"
	        });
			(end)

		Example 2:
		    Label *setOption()* method usage example (change Label's text)
			(start code)
			$('#label').sfLabel("option", "text", "New label text");
			(end)

		Example 3:
		    Label *destroy* method usage example
			(start code)
	        $('#label').sfLabel({,
	            text: "My label text"
	        });

	        $('#label').sfLabel('destroy');
	        (end)

		Comments:
			Destroy method restores element's html to 'before initialization' state

		Rendered HTML:
			Before init:
				(start code)
				<div id="label"><b>My original text</b>< /div>
				(end)
	        After init:
				(start code)
				<div id="label" class="sf-ui-common-ellipsis"><b>My label text</b>< /div>
				(end)
	*/
	sf.ui.Label = sf.ui.widgetFactory({
		/** @lends sf.ui.Label.prototype */

		/**
		 * Widget name
		 * @default 'sfLabel'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-common-ellipsis'
		 * @type String
		 */
		baseCssClass: 'sf-ui-label sf-ui-common-ellipsis',

		/**
		 * Label instance state object.
		 * <dl class="detailList">
		 *   <dt class="heading">Properties:</dt>
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>text</b> (defaults to '')
		 *   </dt>
		 *   <dd>Text inserted into instance element.</dd>
		 * </dl>
		 * @type Object
		 */
		options: {
			text: ''
		},


		/**
         * Works as a constructor.
         * Stores element's original text in initialState object.
         * @private
         */
		_create: function() {
			alert("[sf.Label] _create()");
			this.initialState.content = this.widget().html();
			this._super();
		},

		/**
		 * Runs _setText method which deals with Label text
		 * @private
		 */
		_init: function() {
			alert("[sf.Label] _init()");
			this._super();
			this._setText(this.options.text);
		},


		/**
		 * @private
		 */
		_setOption: function(name, value) {
			alert("[sf.Label] _setOption(" + name + ", " + value + ")");
			if (name === 'text') {
				this.options.text = value;
				this._setText(this.options.text);
			}
		}, // _setOption()


		/**
         * Restores element to original state. </br><br />
         * In details: </br>
         * Removes text provided at Label instantiation and restors element's original html
         * @private
         **/
		_destroy: function () {
			alert("[sf.Label] _destroy()");
			this._setText(this.initialState.content);
		}, // _destroy()


		/**
		 * Clears element's current html and sets provided text.
		 * @param {String} text
		 * @private
		 */
		_setText: function(text) {
			alert("[sf.Label] _setText()");
			this.widget().html(text);
		} // _setText()

	}); // sf.ui.Label()

	sf.ui.bridge(widgetName, sf.ui.Label);

    sf.ui.addSelector(widgetName);

}(jQuery));/*
 * Title: UI Components (List)
 * Apps Framework List UI Components
 *
 * Version added:
 * 2.0.0
 */

(function ($) {
	var widgetName = "sfList",
		STATE = {
			BLUR: 1,
			FOCUS: 2,
			SELECT: 3
		};
	var moving

	/*
		Class: sf.ui.List (jQuery.sfList)
		Apps Framework Class for List UI
	*/
	/*
	 	Function: jQuery.sfList
	 		List instance state object.

	 	Parameter:
	 		o options  - (Object) Detailed structure below
	 			data 			- (Array<String>) data for list items (defaults to [])
	 			index			- (Number) Index of selected/focused list item (defaults to null)

		Returns:
			(Object) jQuery object

		Rendered HTML:
			Before init:
			(start code)
			<div id="list1"></div>
			(end)

			After init:
			(start code)
			<div id="list1">
				<!-- item -->
				<div class="sf-ui-list-blured | sf-ui-list-focused | sf-ui-list-selected">
					<table width="100%" cellpadding="0px" cellspacing="0px">
			            <tr>
							<td class="sf-ui-list-item-left"></td>
			                <td class="sf-ui-list-item-center">first item</td>
			                <td class="sf-ui-list-item-right"></td>
			            </tr>
			        </table>
				</div>
				<!-- other items -->
			</div>
			(end)
		 */
	sf.ui.List = sf.ui.widgetFactory({
		/** @lends sf.ui.List.prototype */

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-list'
		 * @type String
		 */
		baseCssClass: "sf-ui-list",

		/**
		 * Event namespace added by default to every event triggered by Scroll.
		 * @default "sflist"
		 * @type String
		 */
		eventNamespace: "sflist",

		options: {
			data: [],
			index: null,
			itemsPerPage: 5,
			focusOffset: {
				x: 0,
				y: -10
			}
		},

		/**
		 * Object with templates used to render list.
		 * <dl class="detailList">
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>item</b> (defaults to '${}')
		 *   </dt>
		 *   <dd>template for list item</dd>
		 * </dl>
		 * @type Object
		 */
		templates: {
			item: '<div class="sf-ui-list-item">' +
					 '<div class="left"></div>' +
					 '<div class="center"></div>' +
					 '<div class="right"></div>' +
					 '</div>',
			focus: '<div class="sf-ui-list-focus">' +
					 '<div class="left"></div>' +
					 '<div class="center"></div>' +
					 '<div class="right"></div>' +
					 '</div>'
		},

		widgetName: widgetName,

		_destroy: function () {
			this.view.items.remove();
		}, // destroy()


		_init: function () {
			alert("[sf.ui.List] _init()");

			var o = this.options;

			this._super();

			this._addItemWrappers();
			this._setItems(o.index);
			this.focus(o.index);

			this._blur();
		}, // _init()


		_addItemWrappers: function () {
			alert("[sf.ui.List] _addItemWrappers()");
			var o = this.options,
				w = this.widget(),
				v = this.view;

			var styleVisibility = w.css('visibility');
			w.css('visibility', 'hidden');
			w.empty();
			var width = w.width();
			alert("List Width : " + width);

			// add focus cursor
			$.tmpl(this.templates.focus, {}).appendTo(w);
			var left = w.find('.sf-ui-list-focus .left').width();
			var right = w.find('.sf-ui-list-focus .right').width();
			w.find('.sf-ui-list-focus .center').width(width-left-right);
			v.focus = w.find('.sf-ui-list-focus');

			// add items wrappers
			w.append('<div class="sf-ui-list-itemarea"></div>');
			for (var i = 0; i < o.itemsPerPage; i++) {
				$.tmpl(this.templates.item, {item: ''}).appendTo(w.find('.sf-ui-list-itemarea'));
				w.find('.sf-ui-list-item .center').width(width-left-right);
			}
			v.items = w.find('.sf-ui-list-item');
			v.items.each(function(index, elmt) {
				left = $(elmt).find('.left').width();
				right = $(elmt).find('.right').width();
				$(elmt).find('.center').width(width-left-right);
			});

			var focusheight = w.find('.sf-ui-list-focus .center').eq(0).outerHeight(true);
			this.itemHeight = w.find('.sf-ui-list-item .center').eq(0).outerHeight(true);
			this.focusYOffset = parseInt((this.itemHeight - focusheight)/2, 10);
			w.css('visibility', styleVisibility || '');
		}, // _addItemWrappers()


		_setItems: function (focusIndex) {
			alert('[sf.ui.List] _setItems('+focusIndex+')');

			var o = this.options,
				v = this.view,
				t = this.templates,
				firstIndex = this._getPage(focusIndex) * o.itemsPerPage;
			//alert("index: " + focusIndex);
			//alert("itemsPerPage: " + o.itemsPerPage);
			//alert("firstIndex : " + firstIndex);

			v.items.each(function (index, item) {
				//alert("set : " + index);
				if (firstIndex + index < o.data.length) {
					//alert("\ttext : " + o.data[firstIndex + index]);
					$(item).find('.center').html(o.data[firstIndex + index]);
				} else {
					$(item).find('.center').html('');
				}
			});
		}, // _setItems()


		_focus: function (itemIndex) {
			alert('[sf.ui.List] _focus(' + itemIndex + ')');
			var v = this.view;
			if(itemIndex == undefined) {
				itemIndex = this.options.index;
			}
			if(itemIndex > this.options.data.length-1) {
				alert("itemindex out of range. item count : " + this.options.data.length);
				return;
			}
			var w = this.widget();

			var firstIndex = this._getPage(itemIndex) * this.options.itemsPerPage;
			var relIndex = itemIndex-firstIndex;

			var focusTop = (relIndex * this.itemHeight) + this.focusYOffset;
			//alert("relIndex : " + relIndex);
			//alert("this.itemHeight : " + this.itemHeight);
			//alert("this.focusYOffset : " + this.focusYOffset);
			//alert("Move focus to : " + focusTop);

			v.focus.css('display', 'block');
			if (itemIndex != this.options.index) {
				v.items.removeClass('focused');
				v.focus.css({
					'top': focusTop + 'px'
				}, {
					duration: 0,
					complete: function(){
						//alert("[sf.ui.List] moving focus complete!");
						v.items.eq(relIndex).addClass('focused');
					}
				});
				v.items.eq(relIndex).addClass('focused');	// not use animate
			}
			else {
				v.focus.css({
					'top': focusTop + 'px'
				});
				v.items.eq(relIndex).addClass('focused');
			}
			this.options.index = itemIndex;
		}, // _focus()


		_blur: function () {
			alert("[sf.ui.List] _blur()");
			var v = this.view;
			v.focus.css({
				'display': 'none'
			});
		},


		_getPage: function (index) {
			alert("[sf.ui.List] _getPage("+index+")");
			return Math.floor(index / this.options.itemsPerPage);
		}, // _getPage()


		/**
		 * @private
		 */
		_setOption: function (name, value) {
			//alert("[sf.ui.List] _setOption(" + name + ", " + SPRC.debug.stringify(value) + ")");

			var o = this.options;

			switch (name) {
				case "data":
					o.data = [].concat(value);
					o.index = 0;
					this._init();
					break;

				case "index":
					this.move(value);
					break;

				case "itemsPerPage":
					value = parseInt(value, 10);
					if (value > 0) {
						o.itemsPerPage = value;
						this._addItemWrappers();
						this._setItems(value);
						this._focus(value);
						if (!this._getFocused()) {
							this._blur();
						}
					}
					break;
			} // switch(name)
		}, // _setOption()


		_getFocused: function () {
			return v.focus.css('display')=='block';
		},


		/*
		Function: blur
		 Blur list items

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Blur list items

		Example 1:
		 This example blur list items
		 (start code)
          $('#list').sfList('blur');
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		blur: function () {
			alert('[sf.ui.List] blur()');
			this._blur();
			return this;
		}, // blur()


		/*
		Function: clear
		 clear list

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 clear list

		Example 1:
		 This example clear list
		 (start code)
          $('#list').sfList('clear');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		clear: function () {
			alert('[sf.ui.List] clear()');

			this.blur();
			this._setOption('data', []);

			return this;
		}, // clear()


		/*
		Function: focus
		 focus list items

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 focus list items

		Example 1:
		 This example focus list items
		 (start code)
          $('#list').sfList('focus');
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		focus: function () {
			alert('[sf.ui.List] focus()');
			this._focus();
			return this;
		}, // focus()


		/*
		Function: getIndex
		 Returns index of selected/focused list item

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Returns index of selected/focused list item

		Example 1:
		 This example returns index of selected/focused list item
		 (start code)
         var currentFocused = $('#list').sfList('index');
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		getIndex: function () {
			alert('[sf.ui.List] getIndex()');

			return this.option('index');
		}, // getIndex()


		/*
		Function: getSelectedItem
		 Returns value from data property (see {@link #options}) for selected/focused list item

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Returns value from data property (see {@link #options}) for selected/focused list item

		Example 1:
		 This example returns value from data property (see {@link #options}) for selected/focused list item
		 (start code)
          $('#list').sfList('getSelectedItem');
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		getSelectedItem: function () {
			alert('[sf.ui.List] getSelectedItem()');
			var o = this.options;

			return o.items[o.index];
		}, // getSelectedItem()


		/*
		Function: move
		 Move focus to requested index

		Parameters:
		 o (Number) value - list index to move

		Returns:
		 object - jQuery

		Description:
		 Move focus to requested index
		 If value is out of range, move function has no effect
		 Move function does not change the list state, so if the list was cleared, non of items will be focused

		Example 1:
		 This example move focus to requested index
		 (start code)
          $('#list').sfList('move', 3);
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		move: function (value) {
			alert('[sf.ui.List] move(' + value + ')');
			var o = this.options;
			value = parseInt(value, 10);

			if (!isNaN(value) && value >= 0 && value < o.data.length) {
				if(this._getPage(o.index) != this._getPage(value)) {
					this._setItems(value);
				}
				this._focus(value);
			}

			return this;
		}, // move()


		/*
		Function: next
		 Move focus to next list item

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Move focus to next list item
		 If list is blured or cleared, this item gains focus
		 If index (see {@link #options}) is set to last item, the first item will be focused

		Example 1:
		 This example move focus to next list item
		 (start code)
          $('#list').sfList('next');
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		next: function () {
			alert('[sf.ui.List] next()');
			this.move((this.options.index+1)%this.options.data.length);
			return this;
		}, // next()


		/*
		Function: prev
		 Move focus to prev list item

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Move focus to prev list item
		 If list is blured or cleared, this item gains focus
		 If index (see {@link #options}) is set to first item, the last item will be focused

		Example 1:
		 This example move focus to prev list item
		 (start code)
          $('#list').sfList('prev');
		 (end)

		Available From:
		 2.0.0

		History:
		*/
		prev: function () {
			alert('[sf.ui.List] prev()');
			this.move((this.options.index-1+this.options.data.length)%this.options.data.length);
			return this;
		} // prev()

	}); // sf.ui.List()

	sf.ui.bridge(widgetName, sf.ui.List);

	sf.ui.addSelector(widgetName);

}(jQuery));

/*
 * Title: UI Components (Loading)
 * Apps Framework Loading UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfLoading";

	/*
		Class: sf.ui.Loading (jQuery.sfLoading)
			Apps Framework Class for Loading UI
	*/
	/*
		Function: jQuery.sfLoading
			Shows or hides popup with animation indicating user to wait for some processes to complete.

		Parameter:
			o (Object) options - options for loading
				stepIntervalTime - (Number) (defaults to 500) Interval time to animate the loading images. (milliseconds)

		Returns:
			(Object) jQuery object

		Rendered HTML:
			Before init:
			(start code)
			<div id="loading1"></div>
			(end)

			After init:
			(start code)
			<div id="loading" class="sf-ui-loading" style="display:block;">
			    <div class="sf-ui-loading-circle sf-ui-loading-circle-1|sf-ui-loading-circle-2|..."></div>
			</div>
			(end)

		Example 1:
			Loading initializing and showing example
			(start code)
        	$('#loading').sfLoading('show');
        	(end)

		Example 2:
    		Loading initializing example with options for faster animation
			(start code)
	        $('#loading').sfLoading({
	            stepIntervalTime: 250
	        });
			(end)

		Example 3:
    		Loading overriding animation frame logic example
			(start code)
	        $('#loading').sfLoading({
	            overrides: {
	                animationStep: function() {
	                    var cur = this.stepIndex,
	                        next = (cur+1) % this.options.stepTotal;

	                    $("#animated_element_id").removeClass("customCssClass" + "-" + cur.toString());
	                    $("#animated_element_id").addClass("customCssClass" + "-" + cur.toString());

	                    this.stepIndex = next;
	                }
	            }
	        });
	        (end)

	    Comments :
	    	Loading component animates by switching CSS classes on every frame.
	        *animationStep* method is responsible for switching those classes on particular frame.
	        It is recommended to use component's 'this.stepIndex' private variable to store
	        information about frame number.

	*/
	sf.ui.Loading = sf.ui.widgetFactory({
		/** @lends sf.ui.Loading.prototype */

		/**
		 * Widget name
		 * @default 'sfLoading'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Event namespace added by default to every event triggered by Loading.
		 * @default "sfloading"
		 * @type String
		 */
		eventNamespace: "sfloading",

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-loading'
		 * @type String
		 */
		baseCssClass: 'sf-ui-loading',


		/**
		 * Loading instance state object.
		 * <dl class="detailList">
		 *   <dt class="heading">Properties:</dt>
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>animatedCssClass</b> (defaults to '-sf-ui-loading-circle')
		 *   </dt>
		 *   <dd>CSS class of animated element nested inside sfLoading</dd>
		 *
		 *   <dt>
		 *     <span class="light fixedFont">{Number}</span> <b>stepIntervalTime</b> (defaults to 500)
		 *   </dt>
		 *   <dd>Time (ms) after which animation goes to next frame</dd>
		 *
		 *   <dt>
		 *     <span class="light fixedFont">{Number}</span> <b>stepTotal</b> (defaults to 12)
		 *   </dt>
		 *   <dd>Animation's total frame number</dd>
		 * </dl>
		 * @type Object
		 */
		options: {
			animatedCssClass: 'sf-ui-loading-circle',
			stepIntervalTime: 100,
			stepTotal: 18
		},

		/**
		 * Object with templates used to render sfLoading instance.
		 * <dl class="detailList">
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>animation</b> (defaults to '&lt;div class="${animatedCssClass}"&gt;&lt;/div&gt;')
		 *   </dt>
		 *   <dd>template for sfLoading nested element</dd>
		 * </dl>
		 * @type Object
		 */
		templates: {
			animation: '<div class="${animatedCssClass}"></div>'
		},

		/**
		 * @type Number
		 */
		stepIndex: 0,

		/**
		 * @type Boolean
		 */
		interval: false,

		/**
         * Works as a constructor.
         * Compiles and applies template.
         * @private
         */
		_create: function () {
			alert("[sf.Loading] _create()");
			this._super();
			this.view.animatedElement = $.tmpl(this.templates.animation, {animatedCssClass: this.options.animatedCssClass});
			this.widget().html(this.view.animatedElement);
		}, // _create()

		/**
         * Removes element's html content.
         * @private
         **/
		_destroy: function () {
			alert("[sf.Loading] _destroy()");
			this.view.animatedElement.remove();
		}, // destroy()


		/**
		 * Cycles frames using interval.
		 * In details: <br />
		 * animation speed can be controlled with stepIntervalTime parameter set in options
		 * @private
		 */
		_animate: function () {
			alert("[sf.Loading] _animate()");
			var self = this;
			this.interval = setInterval(function() {
				self.animationStep();
			}, this.options.stepIntervalTime);
		}, // _animate()


		/**
		 * Holds single frame logic. <br /><br />
		 * In details: <br />
		 * Cycles CSS classes of animated element basing on: <br />
		 * - element provided in templates, <br />
		 * - CSS class provided in animatedCssClass, <br />
		 * - frame number <br />
		 * Increases animation frame number basing on total number of frames hold in <b>stepTotal</b> field in <b>options</b> object.
		 */
		animationStep: function () {
			alert("[sf.Loading] animationStep()");
			var cur = this.stepIndex,
				next = (cur+1) % this.options.stepTotal;
			this.view.animatedElement
				.removeClass(this.options.animatedCssClass + '-' + cur.toString())
				.addClass(this.options.animatedCssClass + '-' + next.toString());
			this.stepIndex = next;
		}, // animationStep()

		/*
		Function: show
			Shows sfLoading component and starts its animation

		Parameters:
			N/A

		Returns:
			(Object) jQuery object

		Description:
			Shows sfLoading component and starts its animation

		Example 1:
			This example to show sfLoading component and starts its animation
			(start code)
			$('#loading').sfLoading('show');
			(end)

		Available From:
			1.1.0

		History:
		*/
		show: function () {
			alert("[sf.Loading] _show()");
			this.widget().show();
			if (this.interval) {
				return this;
			}
			this._animate();

			return this;
		}, // show()


		/**
		 * Hides sfLoading component and stops its animation
		 * @return {Object} this
		 */
		/*
		Function: hide
			Hides sfLoading component and stops its animation

		Parameters:
			N/A

		Returns:
			(Object) jQuery object

		Description:
			Hides sfLoading component and stops its animation

		Example 1:
			This example to hide sfLoading component and stops its animation
			(start code)
			$('#loading').sfLoading('hide');
			(end)

		Available From:
			1.1.0

		History:
		*/
		hide: function () {
			alert("[sf.Loading] _hide()");
			this.widget().hide();
			clearInterval(this.interval);
			this.interval = null;

			return this;
		} // hide()

	}); // sf.ui.Loading()

	sf.ui.bridge(widgetName, sf.ui.Loading);

    sf.ui.addSelector(widgetName);

}(jQuery));/*
 * Title: UI Components (Scroll)
 * Apps Framework Scroll UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfScroll";

	/*
		Class: sf.ui.Scroll (jQuery.sfScroll)
		Apps Framework Class for Scroll UI
	*/
	sf.ui.Scroll = sf.ui.widgetFactory({
		/** @lends sf.ui.Scroll.prototype */

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-scroll'
		 * @type String
		 */
		baseCssClass: 'sf-ui-scroll',

		/**
		 * Event namespace added by default to every event triggered by Scroll.
		 * @default "sfscroll"
		 * @type String
		 */
		eventNamespace: "sfscroll",

		/*
		 	Function: jQuery.sfScroll
				Scroll instance state object.

			Parameter:
				o (Object) options - options for scroll
					pages - (Number) (defaults to 0) Number of pages represented by scroll
					currentPage - (Number) (defaults to 0) Current page shown on scroll
					bgCssClass - (String) (defaults to 'sf-ui-scroll-bg') CSS class of background element of the scroll
					bgTopCssClass - (String) (defaults to 'sf-ui-scroll-bg-top') CSS class of background top element of the scroll
					bgMiddleCssClass - (String) (defaults to 'sf-ui-scroll-bg-middle') CSS class of background middle element of the scroll
					bgBottomCssClass - (String) (defaults to 'sf-ui-scroll-bg-bottom') CSS class of background bottom element of the scroll
					bodyCssClass - (String) (defaults to 'sf-ui-scroll-body') CSS class of body element of the scroll
					topCssClass - (String) (defaults to 'sf-ui-scroll-body-top') CSS class of scroll top part element
					middleCssClass - (String) (defaults to 'sf-ui-scroll-body-middle') CSS class of scroll middl part element
					bottomCssClass - (String) (defaults to 'sf-ui-scroll-body-bottom') CSS class of scroll bottom part element

			Returns:
				(Object) jQuery object

			Rendered HTML:
				Before init:
				(start code)
				<div id="scroll1"></div>
				(end)

				After init:
				(start code)
				<div id="scroll1" class="sf-ui-scroll">
					<div class="sf-ui-scroll-bg">
						<div class="sf-ui-scroll-bg-top"></div>
						<div class="sf-ui-scroll-bg-middle"></div>
						<div class="sf-ui-scroll-bg-bottom"></div>
					</div>
					<div class="sf-ui-scroll-body">
						<div class="sf-ui-scroll-body-top"></div>
						<div class="sf-ui-scroll-body-middle"></div>
						<div class="sf-ui-scroll-body-bottom"></div>
					</div>
				</div>
				(end)
		 */
		options: {
			pages: 0,
			currentPage: 0,
			bgCssClass: 'sf-ui-scroll-bg',
			bgTopCssClass: 'sf-ui-scroll-bg-top',
			bgMiddleCssClass: 'sf-ui-scroll-bg-middle',
			bgBottomCssClass: 'sf-ui-scroll-bg-bottom',
			bodyCssClass: 'sf-ui-scroll-body',
			topCssClass: 'sf-ui-scroll-body-top',
			middleCssClass: 'sf-ui-scroll-body-middle',
			bottomCssClass: 'sf-ui-scroll-body-bottom'
		},

		pageHeight: 0,
		scrollSize: 0,
		templates: {
			bg: '<div class="${bgCssClass}"><div class="${bgTopCssClass}"></div><div class="${bgMiddleCssClass}"></div><div class="${bgBottomCssClass}"></div></div>',
			body: '<div class="${bodyCssClass}"><div class="${topCssClass}"></div><div class="${middleCssClass}"></div><div class="${bottomCssClass}"></div></div>'
		},
		totalHeight: 0,

		widgetName: widgetName,

		_create: function () {
			alert("[sf.Scroll] _create()");

			var o = this.options,
				t = this.templates,
				v = this.view,
				w = this.widget();

			this._super();

			this.totalHeight = w.height();

			$.each(['bg', 'body'], function (index, name) {
				v[name] = $.tmpl(t[name], o);
			});
			w.append(v.bg, v.body);
			alert("SCROLL HTML : " + w.html());
			this._updateScroll();
		}, // _create()

		_destroy: function () {
			var v = this.view;

			$.each(['bg', 'body'], function (index, name) {
				v[name].remove();
			});
		}, // _destroy()


		/*
		 * Move scroll to requested index
		 * @param {Number} newPage Page number to go to
		 * @private
		 */
		_move: function (newPage) {
			//alert('[sf.Scroll] _move(' + newPage + ")");

			var o = this.options,
				oldPage = o.currentPage;

			if (newPage >= 0 && newPage < o.pages) {
				o.currentPage = newPage;
				this._updateScrollPosition();

				//alert('    > current page: ' + o.currentPage);
				this._trigger("move", null, {
					previousPage: oldPage,
					currentPage: newPage
				});
			}

			return this;
		}, // _move()

		_setOption: function (name, value) {
			alert("[sf.Scroll] _setOption(" + name + ", " + value + ")");

			var o = this.options;

			switch (name) {
				case "pages":
					o.pages = value;
					if(o.currentPage > o.pages-1) {
						o.currentPage = o.pages-1;
					}
					this._updateScroll();
					break;

				case "currentPage":
					this.move(value);
					break;

				case "bgCssClass":		// INTENTIONAL FALLBACK
				case "topCssClass":		// INTENTIONAL FALLBACK
				case "middleCssClass":	// INTENTIONAL FALLBACK
				case "bottomCssClass":
					if (value) {
						this._switchCssClass(name, value, this.view[name.split("CssClass")[0]]);
					}
					break;
			} // switch(name)
		}, // _setOption()

		_switchCssClass: function (name, value, element) {
			//alert("[sf.Scroll] _switchCssClass(" + [name, value, element].join(", ") + ")");

			var o = this.options;

			value = value || "";
			element.removeClass(o[name]).addClass(value);
			o[name] = value;
		}, // _switchCssClass()

		_updateScroll: function () {
			alert("[sf.Scroll] _updateScroll()");

			this._updateScrollSettings();
			this._updateScrollPosition();
		}, // _updateScroll()

		_updateScrollSettings: function () {
			alert("[sf.Scroll] _updateScrollSettings()");
			var MIN_SCROLLSIZE = 30;

			var o = this.options,
				v = this.view;

			//v.bg.css("height", this.totalHeight);
			var bgTopheight = v.bg.find('.'+o.bgTopCssClass).height();
			var bgBtmheight = v.bg.find('.'+o.bgBottomCssClass).height();
			v.bg.find('.'+o.bgMiddleCssClass).css("height", this.totalHeight-(bgTopheight+bgBtmheight));
			//alert("TOTAL HEIGHT: " + this.totalHeight);
			//alert("PAGES: " + o.pages);

			this.scrollSize = Math.round(this.totalHeight / o.pages);
			if (this.scrollSize < MIN_SCROLLSIZE) {
				this.scrollSize = MIN_SCROLLSIZE;
			}
			//alert("SCROLL SIZE: " + this.scrollSize);

			this.pageHeight = this.totalHeight - this.scrollSize;
			if (o.pages > 1) {
				this.pageHeight /= o.pages - 1;
			}
			//alert("PAGE STEP: " + this.pageHeight);
			if (true) {//this.scrollSize >= MIN_SCROLLSIZE) {
				var topheight = v.body.find('.'+o.topCssClass).height();
				var btmheight = v.body.find('.'+o.bottomCssClass).height();
				alert("Set middle of body size to : " + (this.scrollSize - (topheight+btmheight)));
				v.body.find('.'+o.middleCssClass).height(this.scrollSize - (topheight+btmheight));	// size - (topHeight+bottomHeight)
			}
		}, // _updateScrollSettings()

		_updateScrollPosition: function () {
			alert("[sf.Scroll] _updateScrollPosition()");

			var v = this.view,
				o = this.options,
				scrollTop;

			scrollTop = Math.ceil(this.pageHeight * o.currentPage);
			v.body.css("top", scrollTop);
			alert("Move Top to : " + scrollTop);
		}, // _updateScrollPosition()

		/*
		Function: move
			Move scroll to requested index

		Parameters:
			o (Number) pageIndex - Page number to go to

		Returns:
			(Object) jQuery object

		Description:
			Move scroll to requested index

		Example 1:
			This example move scroll to requested index
			(start code)
				$('#list').sfScroll('move', 3);
			(end)

		Available From:
			1.1.0

		History:
		*/
		move: function (pageIndex) {
			alert('[sf.Scroll] move(' + pageIndex + ")");

			this._move(pageIndex);

			return this;
		}, // move()

		/*
		Function: next
			Move scroll to next page

		Parameters:
			N/A

		Returns:
			(Object) jQuery object

		Description:
			If currentPage (see {@link #options}) is set to last page, *next* function has no effect.

		Example 1:
			This example move scroll to next page
			(start code)
			$('#list').sfScroll('move', 3);
			(end)

		Available From:
			1.1.0

		History:
		*/
		next: function () {
			alert("[sf.Scroll] next()");
			this._move(this.options.currentPage + 1);

			return this;
		}, // next()

		/*
		Function: prev
			Move scroll to prev page

		Parameters:
			N/A

		Returns:
			(Object) jQuery object

		Description:
			If "currentPage" option is set to first page(0), *prev* function has no effect.

		Example 1:
			This example move scroll to prev page
			(start code)
			$('#list').sfScroll('prev');
			(end)

		Available From:
			1.1.0

		History:
		*/
		prev: function () {
			alert("[sf.Scroll] prev()");

			this._move(this.options.currentPage - 1);

			return this;
		} // prev()

	}); // sf.ui.Scroll()

	sf.ui.bridge(widgetName, sf.ui.Scroll);

	sf.ui.addSelector(widgetName);

}(jQuery));

/*jslint sloppy: true, white: true, vars: false, undef: true, nomen: true, eqeq: false, plusplus: true, bitwise: true, regexp: true, newcap: true */
/*global sf, alert, curWidget */
/**
* Title: UI Components2 (Scroll2)
*
* @author Grzegorz Wysocki <<a href='mailto:g.wysocki@samsung.com'>g.wysocki@samsung.com </a>
* @date 2011-11-22
*
*
*****************************************************************************
*
* Copyright (c) 2011 Samsung Electronics, Visual Display Division.
* All Rights Reserved.
*
****************************************************************************/

(function ($) {
	/**
	 *@class
	 *Allows to visualize place of actual content.
	 *	<b>Example 1</b><br />
	 *		<u>Initializing vertical with pages: </u><br />
	 *
	 *		$("#Scroll2").sfScroll2({"orientation": "vertical", "page": 1, "pages": 15});
	 *
	 *	<hr />
	 *	<b>Example 2</b><br />
	 *		<u>Initializing vertical with items: </u><br />
	 *
	 *		$("#Scroll2").sfScroll2({"orientation": "vertical", "item": 1, "items": 48});
	 *	<hr />
	 *	<b>Example 3</b><br />
	 *		<u>Initializing horizontal with pages: </u><br />
	 *
	 *		$("#Scroll2").sfScroll2({"orientation": "horizontal", "page": 1, "pages": 15});
	 *	<hr />
	 *	<b>Example 4</b><br />
	 *		<u>Initializing horizontal with items: </u><br />
	 *
	 *		$("#Scroll2").sfScroll2({"orientation": "horizontal", "item": 1, "items": 48});
	 *	<hr />
	 *
	 *	@augments $.sf.ui.Widget
	 */

	sf.ui.Scroll2 = sf.ui.widgetFactory(
	/**
	 *@lends sf.ui.Scroll2.prototype
	 */
	{

		/**
		 *Widget name.
		 *@default 'sfScroll2'
		 *@type String
		 */
		widgetName: "sfScroll2",

		/**
		 *Widget base CSS class.
		 *@default 'sf-ui-scroll2'
		 *@type String
		 */
		baseCssClass: "sf-ui-scroll2",

		/**
		 * Event namespace.
		 * @default 'sfscroll2'
		 * @type String
		 */
		eventNamespace: "sfscroll2",

		/**
		 * Depends from scroll2 variant.
		 * For vertical length takes 'width' and horizontal takes 'height'.
		 * @private
		 * @type String
		 */
		length: '',

		/**
		 * Depends from scroll2 variant.
		 * For vertical startSide takes 'left' and horizontal takes 'top'.
		 * @private
		 * @type String
		 */
		startSide: '',

		/**
		 * Depends from scroll2 variant.
		 * For vertical is equal 'vertical' and for horizontal eqal 'horizontal'.
		 * @private
		 * @type String
		 */
		orientation: '',

		/**
		 * Actual top item.
		 * Item in linked content.
		 * @private
		 * @type Number
		 */
		item: -1,

		/**
		 * Number of all items.
		 * Items in linked content.
		 * @private
		 * @type Number
		 */
		items: -1,

		/**
		 * Length of item.
		 * Length of item in scroll.
		 * @private
		 * @type Number
		 */
		itemLength: 0,

		/**
		 * Number of item at top of the last page in linked content.
		 * @private
		 * @type Number
		 */
		itemLast: 0,

		/**
		 * Actual page.
		 * Page in linked content.
		 * @private
		 * @type Number
		 */
		page: -1,

		/**
		 * Number of all pages.
		 * Pages in linked content.
		 * @private
		 * @type Number
		 */
		pages: -1,

		/**
		 * Length of container for middle scroll2 background.
		 * @private
		 * @type Number
		 */
		bgLength: 0,

		/**
		 * Length of back of main container.
		 * @private
		 * @type Number
		 */
		backLength: 0,

		/**
		 * Length of front of main container.
		 * @private
		 * @type Number
		 */
		frontLength: 0,

		/**
		 * Position of first place for scrolled container.
		 * @private
		 * @type Number
		 */
		barTop: -0,

		/**
		 * Length of scroll bar.
		 * @private
		 * @type Number
		 */
		barLength: 0,

		/**
		 * Length of scroll bar container.
		 * @private
		 * @type Number
		 */
		barContLength: 0,

		/**
		 * Object of bar container.
		 * @private
		 * @type Object
		 */
		barContTop: 0,

		/**
		 * Length of bar prev container.
		 * @private
		 * @type Number
		 */
		barPLength: 0, //length of previous and next bar

		/**
		 * Length of bar prev container.
		 * @private
		 * @type Number
		 */
		itemStep: 0, //length of one move unit

		/**
		 * Options to configure widget.
		 * @type Object
		 * @description
		 * Allowed options:
		 * <ul>
		 * <li><b>orientation</b> - sets if scroll is vertical or horizontal, insert "vertical" for vertical, "horizontal" for horizontal - obligatory</li>
		 * <li><b>page</b> - sets act position of scroll (number of actual page), insert number greater than 1 and smaller than "pages" - obligatory "page" or "item"</li>
		 * <li><b>pages</b> - sets length of scroll (number of all pages), insert number greater than "page"</li>
		 * <li><b>item</b> - sets act position of scroll (global number of first item on page), insert number smaller than "items" - obligatory "page" or "item"</li>
		 * <li><b>items</b> - sets number of all items in related content - obligatory if scroll have to do calculations with items<</li>
		 * </ul>
		 */

		options: {
			orientation: '',	// orientation of scroll
			page: -1,			// actual number of page in related content
			pages: -1,			// number of all pages in related content
			item: -1,			// actual number of item in related content
			items: -1			// number of all items in related content
		},

		/**
		 * Default templates
		 * @type Object
		 * @description		 *
		 * <ul>
		 * <li><b>scroll</b> - template of container with all scrolling elements</li>
		 * </ul>
		 */
		templates: {
			scroll2: "<div class='${baseCssClass}-${orientation}-mainBack'>" +
				"<div class='${baseCssClass}-${orientation}-mainFront'>" +
					"<div class='${baseCssClass}-${orientation}-bgPrevoius'></div>" +
					"<div class='${baseCssClass}-${orientation}-bg'></div>" +
					"<div class='${baseCssClass}-${orientation}-bgNext'></div>" +
					"<div class='${baseCssClass}-${orientation}-barContainer'>" +
						"<div class='${baseCssClass}-${orientation}-barPrevious'></div>" +
						"<div class='${baseCssClass}-${orientation}-bar'></div>" +
						"<div class='${baseCssClass}-${orientation}-barNext'></div>" +
					"</div>" +
				"</div>" +
			"</div>"
		},

		/****************************************
		 *				Api methods				*
		 ****************************************/

		/**
		 * Initialize component
		 * @private
		 */
		_init: function(){
			var v = this.view,
				t = this.templates,
				w = this.widget(),
				o = this.options;

			switch(o.orientation) {
				case "vertical":
					this.length = "height";
					this.startSide = "top";
					break;
				case "horizontal":
					this.length = "width";
					this.startSide = "left";
					break;
			}

			this.orientation = o.orientation;

			w.css('visibility', 'hidden');
			v.scroll2 = $.tmpl(t.scroll2, {baseCssClass: this.baseCssClass, orientation: o.orientation});
			w.append(v.scroll2);

			this.backLength = parseInt(w.find("." + this.baseCssClass + "-" + this.orientation + "-mainBack").css(this.length), 10);

			// minium values of scroll lengths
			switch(sf.ui.height){
				case 540:
					if(this.backLength < 60) {
						this.backLength = 60;
					}
					break;

				case 720:
					if(this.backLength < 80) {
						this.backLength = 80;
					}
					break;

				case 1080:
					if(this.backLength < 120) {
						this.backLength = 120;
					}
					break;
			}

			this.frontLength = this.backLength - 2 * parseInt(w.find("." + this.baseCssClass + "-" + this.orientation + "-mainFront").css(this.startSide), 10);

			w.find("." + this.baseCssClass + "-" + this.orientation + "-mainFront").css(this.length, this.frontLength);

			this.bgLength = this.frontLength - (parseInt(w.find("." + this.baseCssClass + "-" + this.orientation + "-bgPrevoius").css(this.length), 10)) * 2;

			w.find("." + this.baseCssClass + "-" + this.orientation + "-bg").css(this.length, this.bgLength);

			this.barPLength = parseInt(w.find("." + this.baseCssClass + "-" + this.orientation + "-barNext").css(this.length), 10) * 2;

			this.barContTop = w.find("." + this.baseCssClass + "-" + this.orientation + "-barContainer");

			if(o.pages > 0){
				this._drawPages(o.pages);
				this._movePage(o.page);
			}else if(o.items > 0){
				this._drawItems(o.items);
				this._moveItem(o.item);
			}

			w.css('visibility', 'visible');
		},

		/**
		 * Drawing scroll for items
		 * @param {Number} items Number of items
		 * @private
		 */
		_drawItems:function(items) {
			var w = this.widget();
			this.items = items;

			this.itemLength = (this.frontLength / this.items);

			this.barLength = this.itemLength - this.barPLength;

			// minimum values of scroll bar lengths
			switch(sf.ui.height){
				case 540:
					if((this.barLength) < 22) {
						this.barLength = 22 - this.barPLength;
					}
					break;

				case 720:
					if((this.barLength) < 32) {
						this.barLength = 32 - this.barPLength;
					}
					break;

				case 1080:
					if((this.barLength) < 44) {
						this.barLength = 44 - this.barPLength;
					}
					break;
			}

			this.barContLength = this.barLength + this.barPLength;

			w.find("." + this.baseCssClass + "-" + this.orientation + "-bar").css(this.length, this.barLength);
			w.find("." + this.baseCssClass + "-" + this.orientation + "-barContainer").css(this.length, this.barContLength);

			this.itemStep = ((this.frontLength - this.barContLength) / (this.items - 1));

		},

		/**
		 * Move scroll to right item position
		 * @param {Number} item Number of focused item
		 * @private
		 */
		_moveItem: function(item){
		if(this.items > 0){
			if(item < 1) {
				item = 1;
			}

			if(item > this.items) {
				item = this.items;
			}

			if(this.item !== item) {
				this.item = item;
				this.barTop = this.itemStep * this.item - this.itemStep;
			}

			this.barContTop.css(this.startSide, this.barTop);
			}
		},

		/**
		 * Drawing scroll for pages
		 * @param {Number} pages Number of pages
		 * @private
		 */
		_drawPages:function(pages) {
			var w = this.widget();
			this.pages = pages;

			this.barLength = (this.frontLength - (this.pages * this.barPLength)) / this.pages;

			this.barContLength = this.barLength + this.barPLength;
			w.find("." + this.baseCssClass + "-" + this.orientation + "-bar").css(this.length, this.barLength);
			w.find("." + this.baseCssClass + "-" + this.orientation + "-barContainer").css(this.length, this.barContLength);
		},

		/**
		 * Move scroll to right page
		 * @param {Number} page Number of current page
		 * @private
		 */
		_movePage: function(page){
			if(this.pages > 0){
				if(page < 1) {
					page = 1;
				}
				else if(page > this.pages) {
					page = this.pages;
				}
				if(this.page !== page){
				this.page = page;
				this.barTop = this.barContLength * this.page - this.barContLength;
				this.barContTop.css(this.startSide, this.barTop);
				}
			}
		},

		/**
		 * Destructor
		 */
		_destroy: function () {
			this.view.scroll2.remove();
		},

		/**
		 * Move scroll to previous item
		 * @example
		 * $("#Scroll2").sfScroll2("previousItem");
		 */
		previousItem: function() {
			alert("[sf.Scroll2 ] previousItem()");
			this._moveItem(this.item - 1);
			return this;
		},

		/**
		 * Move scroll to next item
		 * @example
		 * $("#Scroll2").sfScroll2("nextItem");
		 */
		nextItem: function() {
			alert("[sf.Scroll2 ] nextItem()");
			this._moveItem(this.item + 1);
			return this;
		},

		/**
		 * Move scroll to previous page
		 * @example
		 * $("#Scroll2").sfScroll2("previousPage");
		 */
		previousPage: function(){
			alert("[sf.Scroll2] previousPage()");
			this._movePage(this.page - 1);
			return this;
		},

		/**
		 * Move scroll to next page
		 * @example
		 * $("#Scroll2").sfScroll2("nextPage");
		 */
		nextPage: function() {
			alert("[sf.Scroll2 ] nextPage()");
			this._movePage(this.page + 1);
			return this;
		},

		/**
		 * Move scroll to given item
		 * @param {item} Number of focused item
		 * @example
		 * $("#Scroll2").sfScroll2("moveItem(3)");
		 */
		moveItem: function(item) {
			alert("[sf.Scroll2] moveItem()");
			this._moveItem(item);
			return this;
		},

		/**
		 * Move scroll to given page
		 * @param {page} Number of page
		 * @example
		 * $("#Scroll2").sfScroll2("movePage(3)");
		 */
		movePage: function(page) {
			alert("[sf.Scroll2 ] movePage()");
			this._movePage(page);
			return this;
		},

		/**
		 * Change scroll for items
		 * @param {item} Number of focused item
		 * @param {items} Number of all items
		 * @example
		 * $("#Scroll2").sfScroll2("changeItems(3, 12)");
		 */
		changeItems: function(item, items) {
			alert("[sf.Scroll2 ] changeItems()");
			this._drawItems(items);
			this._moveItem(item);
			return this;
		},

		/**
		 * Change scroll for pages
		 * @param {page} Number of current page
		 * @param {pages} Number of all pages
		 * @example
		 * $("#Scroll2").sfScroll2("changePages(3, 12)");
		 */
		changePages: function(page, pages){
			alert("[sf.Scroll2 ] changePages()");
			this._drawPages(pages);
			this._movePage(page);
			return this;
		},

		/**
		 * Change scroll for items by adding 1 item
		 * @example
		 * $("#Scroll2").sfScroll2("addItem");
		 */
		addItem: function() {
			alert("[sf.Scroll2 ] addItem()");
			this._drawItems(this.item + 1);
			return this;
		},

		/**
		 * Change scroll for items by removing 1 item
		 * @example
		 * $("#Scroll2").sfScroll2("removeItem");
		 */
		removeItem: function() {
			alert("[sf.Scroll2 ] removeItem()");
			this._drawItems(this.item - 1);
			return this;
		},

		/**
		 * Change scroll for pages by adding 1 page
		 * @example
		 * $("#Scroll2").sfScroll2("addPage");
		 */
		addPage: function() {
			alert("[sf.Scroll2 ] addPage()");
			this._drawPage(this.page + 1);
			return this;
		},

		/**
		 * Change scroll for pages by removing 1 page
		 * @example
		 * $("#Scroll2").sfScroll2("removePage");
		 */
		removePage: function() {
			alert("[sf.Scroll2 ] removePage()");
			this._drawPage(this.page - 1);
			return this;
		}
	});

	sf.ui.bridge('sfScroll2', sf.ui.Scroll2 );
	sf.ui.addSelector('sfScroll2');

}(jQuery));/*jslint sloppy: true, white: true, vars: false, undef: true, nomen: false, eqeq: false, plusplus: true, bitwise: true, regexp: true, newcap: true */
/*global sf, alert, curWidget */

/**
 * Title: UI Components (Grid)
 *  Apps Framework Button UI Components
 *
 * Version added:
 *
 * @author Grzegorz Wysocki  <<a href='mailto:g.wysocki@samsung.com'>g.wysocki@samsung.com</a>>
 * @author Tadeusz Wójcicki  <<a href='mailto:t.wojcicki@samsung.com'>t.wojcicki@samsung.com</a>>
 * @date 2011-11-11
 *
 *
 *****************************************************************************
 *
 * Copyright (c) 2011 Samsung Electronics, Visual Display Division.
 *
 * All Rights Reserved.
 *
 ****************************************************************************/


(function ($) {
	/**
	* @class
	* Allows to visualize content in grid form with scrolling and avability to move left/right up/down on a grid.
	* @example
	* <b>Example 1</b>
	*		<u>Initializing example:</u>
	*
	*		$('#grid0').sfGrid({src: Miniatures720175});
	*
	* <hr />
	* <b>Example 2</b>
	*		<u>Initialazing with setting all options:</u>
	*
	*		$('#grid0').sfGrid({
	*		src: Miniatures720175,
	*		picSize: '2',
	*		gridWidth: 600,
	*		gridHeight: 400,
	*		gridBgColor: 'red'
	*	});
	* <hr />
	* <b>Examle 3</b>
	*		<u>Moving along grid - up/down left/right</u>
	*
	*		$('#grid').sfGrid('moveRight'); - moving right
	*
	*		$('#grid').sfGrid('moveUp'); - moving up
	*
	* <hr />
	* <b>Example 4</b>
	*		<u>Moving next/previous page</u>
	*
	*		$('#grid').sfGrid('prevPage'); - preview page.
	*
	* <b>Example 5</b>
	*		<u>Changing pic size</u>
	*
	*		$('#grid').sfGrid({gridPicSize : 's'}); - changing pic size to small
	*
	* <b>Example 5</b>
	*		<u>Changing grid mode</u>
	*
	*		$('#grid').sfGrid({gridMode: 'text'}); - changing grid mode to text
	*
	* <b>Example 6</b>
	*		<u>Getting grid width</u>
	*
	*		$('#grid').sfGrid('getActualWidth');
	*
	* @augments $.sf.ui.Widget
	*/

	sf.ui.Grid = sf.ui.widgetFactory(
		/**
		* @lends sf.ui.Grid
		*/
		{

		/**
		* Widget name.
		*
		* @type String
		*/

		widgetName: 'sfGrid',

		/**
		* Widget base CSS class.
		*
		* @type String
		*/

		baseCssClass: 'sf-ui-grid',

		/**
		* Event namespace.
		*
		* @type String
		*/

		eventNamespace: 'sfgrid',

		itemWidth: 0, // each element size - width/height.
		itemHeight: 0, // height of a poster.
		interval: 0, // interval between each image.
		mainPadding: 0, // padding of a container contains image elements.
		compWidth: 0, // width of a container contains image elements.
		shadowSize: 0, // size of a shadow around images.
		sContainerWidth: 0, // size of a container that contains shadows.
		sContainerHeight: 0, // size of a container that contains shadows.
		sPartSize: 0, // size of a shadow around images.
		sEdgeSize: 0, // size of a shadow around images.

		topPosition: 0, // that variable show if the top property of a Items Container is different then zero.
		items: 0, // number of items supported by user.
		columns: 0, // number of columns that can be putted to the grid container.
		rows: 0, // number of all rows.
		visibleRows: 0, // number of rows that is all the time on top.
		pages: 0, // number of pages.
		actPage: 0, // actual page.
		actRow: 0, // actual row.
		mouseOver: 0, // mouse over item.
		textSize: 0,
		isFocused: false, // is grid focused.

		/**
		* Options to configure the widget.
		* @type Object
		*
		* @description
		* Allowed options:
		* <ul>
		* <li><b>actItem</b> - actual focused image. default: 0</li>
		* <li><b>startingPoint</b> - starting point for grid. default: 0</li>
		* <li><b>gridOrient</b> - grid orientation available: 'vertical' - vertical. default: 'vertical'</li>
		* <li><b>scrollType</b> - how the scroll should behave, available: 'p' - pages, 'p' - rows. default: 'p'</li>
		* <li><b>gridMode</b> - changed grid mode between: 'text' - with title and subtitle under image, 'title' - with title over image, 'poster' - grid with posters, '' - only images. default: ''</li>
		* <li><b>gridSrc</b> - table containing url's for pictures and titlef for it. default: ''</li>
		* <li><b>gridPicSize</b>  - Size of a pictures available: 's' - small, 'm' - medium, 'l' - large. default: 's'</li>
		* <li><b>gridWidth</b>  - width of a grid. default: ''</li>
		* <li><b>gridHeight</b>  - height of a grid. default: ''</li>
		* <li><b>gridCssClass</b>  - class for grid. default: ''</li>
		* <li><b>gridItemsCssClass</b>  - class for div showing only visible part of grid. default: ''</li>
		* </ul>
		*/

		options: {
			actItem: 0,
			page: 0,
			gridOrient: 'vertical',
			scrollType: 'p',
			gridMode: '',
			gridSrc: '', //
			gridPicSize: 's', //
			gridWidth: '',
			gridHeight: '',
			gridCssClass: '', //
			gridItemsCssClass: '',//
			callback: ''
		},

		/**
		* Default templates.
		* @type Object
		*
		* @description
		* <ul>
		* <li><b>grid</b> - template of container for all grid elements</li>
		* <li><b>itemsContainer</b> - template of container containing items and scroll</li>
		* <li><b>item</b> - template of each item</li>
		* <li><b>shadowContainer</b> - template for shadows</li>
		* <li><b>scroll</b> - template of scroll</li>
		* <li><b>title</b> - template of title placed on image</li>
		* <li><b>textTitle</b> - template of title under image</li>
		* <li><b>textSubtitle</b> - template of subtitle under image</li>
		* </ul>
		*/

		templates: {
			grid: '<div class="${baseCssClass}-mainContainer ${gridCssClass}"></div>',
			itemsContainer: '<div class="${baseCssClass}-itemsContainer ${gridItemsCssClass}"></div>',
			item: '<div class="${baseCssClass}-item"></div>',
			shadowContainer: '<div class="${baseCssClass}-shadowContainer">' +
						'<div class="${baseCssClass}-shadow-topLeft"></div>' +
						'<div class="${baseCssClass}-shadow-top" style="width: ${width}px"></div>' +
						'<div class="${baseCssClass}-shadow-topRight"></div>' +
						'<div class="${baseCssClass}-shadow-left" style="height: ${height}px"></div>' +
						'<div class="${baseCssClass}-shadow-right" style="height: ${height}px"></div>' +
						'<div class="${baseCssClass}-shadow-bottomLeft"></div>' +
						'<div class="${baseCssClass}-shadow-bottom" style="width: ${width}px"></div>' +
						'<div class="${baseCssClass}-shadow-bottomRight"></div>' +
						'</div>',
			scroll: '<div class="${baseCssClass}-scroll"></div>',
			title: '<div class="${baseCssClass}-item-title"></div>',
			textTitle: '<div class="${baseCssClass}-item-textTitle"></div>',
			textSubtitle: '<div class="${baseCssClass}-item-textSubtitle"></div>'
		},

		/**
		* Options setter.
		*/

		_setOption: function (name, value) {
			switch (name) {
			case 'actItem':
				this.setActItem(value);
				break;

			case 'actPage':
				this.setPage(value);
				break;

			case 'gridSrc':
				this.setGridSrc(value);
				break;

			case 'gridPicSize':
				this.setGrid(value);
				break;

			case 'gridCssClass':
				this.setGridCssClass(value);
				break;

			case 'gridItemsCssClass':
				this.setGridItemsCssClass(value);
				break;

			case 'gridMode':
				this.setGrid(value);
				break;

			default:
				alert('Wrong option name');
				break;
			}
		},

		/**
		* Sets the CSS class of the scrollbar track.
		* @param {String} css CSS class name.
		*/

		setActItem: function (value) {
			alert('[sf.Grid][setActItem()]');
			this._move(value);
		},

		/**
		* Sets the page o.
		* @param {Number} css CSS class name.
		*/

		setPage: function (value) {
			alert('[sf.Grid][setPage()]');
			this._fillingItems(value);
			this._move(this.options.actItem);
		},

		/**
		* Sets the source of a pictures.
		* @param {Object} source Pictures source.
		*/

		setGridSrc: function (value) {
			alert('[sf.Grid][setGridSrc()]');
			this.options.gridSrc = value;
			this._fillingItems(0);
		},

		/**
		* Sets the grid size or grid mode.
		* @param {String} id Size id.
		*/

		setGrid: function (value) {
			var o = this.options;
			alert('[sf.Grid][setGrid()]');
			if (value === 's' || value === 'm' || value === 'l') {
				o.gridPicSize = value;
			}
			else {
				o.gridMode = value;
			}
			this.view.itemsContainer.remove();
			$(this.view.scroll).sfScroll2('destroy');
			this._choosingPicSize();
			if (this.columns < 0) {
				alert('[sf.Grid][setGridPicSize()] Error: Grid width is to small!!!');
			}
			this._calculatingVariables();
			o.actItem = 0;
			o.page = 0;
			this._addingContainers();
			this._fillingItems(0);
			this._move(0);
		},

		/**
		* Sets the CSS class of the whole grid.
		* @param {String} css CSS class name.
		*/

		setGridCssClass: function (value) {
			alert('[sf.Grid][setGridCssClass()]');
			$('.' + this.baseCssClass + '-mainContainer').removeClass(this.options.gridCssClass);
			this.options.gridCssClass = value;
			$('.' + this.baseCssClass + '-mainContainer').addClass(this.options.gridCssClass);
		},

		/**
		* Sets the CSS class only for grid without scroll.
		* @param {String} css CSS class name.
		*/

		setGridItemsCssClass: function (value) {
			alert('[sf.Grid][setGridItemsCssClass()]');
			$('.' + this.baseCssClass + '-itemsContainer').removeClass(this.options.gridItemsCssClass);
			this.options.gridCssClass = value;
			$('.' + this.baseCssClass + '-itemsContainer').addClass(this.options.gridItemsCssClass);
		},

		/**
		* Getting index of highlighted item on the screen.
		*/

		getHighlightedItem: function () {
			return this.options.actItem;
		},

		/**
		* Getting index of highlighted item from source.
		*/

		getHighlightedItemFromSource: function () {
			return this.options.actItem + this.options.page * this.visibleItems;
		},

		/**
		* Getting index of mouse-over item on the screen.
		*/

		getMouseOverItem: function () {
			return this.mouseOver;
		},

		/**
		* Getting index of mouse-over item from source.
		*/

		getMouseOverFromSource: function () {
			return this.mouseOver + this.options.page * this.visibleItems;
		},

		/**
		 * Get actual width.
		 */

		getActualWidth: function () {
			return this.compWidth + 2 * this.mainPadding;
		},

		/**
		* Calculating pic size.
		* @private
		*/

		_choosingPicSize: function () {
			alert('[sf.Grid][_choosingPicSize()]');
			var o = this.options;

			switch (sf.ui.height) {
			case 540:
				alert('[sf.Grid][_calculatingPicSize()] Resolution: ' + sf.ui.height);
				this.shadowSize = 30;
				this.sPartSize = 20;
				this.interval = 30;
				this.mainPadding = 0;
				break;
			case 720:
				alert('[sf.Grid][_calculatingPicSize()] Resolution: ' + sf.ui.height);
				this.shadowSize = 40;
				this.sPartSize = 23;
				this.interval = 40;
				this.mainPadding = 0;
				break;
			case 1080:
				alert('[sf.Grid][_calculatingPicSize()] Resolution: ' + sf.ui.height);
				this.shadowSize = 60;
				this.sPartSize = 40;
				this.interval = 60;
				this.mainPadding = 0;
				break;
			}
			alert('[sf.Grid][_calculatingPicSize()] Shadow Size: ' + this.shadowSize + ' | ' + 'Part of a shadow - size: ' + this.sPartSize);

			switch (o.gridPicSize) {
			case 's':
				alert('[sf.Grid][_calculatingPicSize()] Grid pic size: ' + "small");
				switch (sf.ui.height) {
				case 540:
					this.itemWidth = 125;
					if (o.gridMode === 'poster') {
						this.itemHeight = 186;
					} else {
						this.itemHeight = 125;
					}
					break;
				case 720:
					this.itemWidth = 175;
					if (o.gridMode === 'poster') {
						this.itemHeight = 262;
					} else {
						this.itemHeight = 175;
					}
					this.interval = 30;
					this.mainPadding = 10;
					break;
				case 1080:
					this.itemWidth = 250;
					if (o.gridMode === 'poster') {
						this.itemHeight = 372;
					} else {
						this.itemHeight = 250;
					}
					break;
				}
				break;//
			case 'm':
				alert('[sf.Grid][_calculatingPicSize()] Grid pic size: ' + 'medium');
				switch (sf.ui.height) {
				case 540:
					this.itemWidth = 156;
					if (o.gridMode === 'poster') {
						this.itemHeight = 234;
					} else {
						this.itemHeight = 156;
					}
					break;
				case 720:
					this.itemWidth = 208;
					if (o.gridMode === 'poster') {
						this.itemHeight = 312;
					} else {
						this.itemHeight = 208;
					}
					break;
				case 1080:
					this.itemWidth = 312;
					if (o.gridMode === 'poster') {
						this.itemHeight = 468;
					} else {
						this.itemHeight = 312;
					}
					break;
				}
				break;
			case 'l':
				alert('[sf.Grid][_calculatingPicSize()] Grid pic size: ' + 'large');
				switch (sf.ui.height) {
				case 540:
					this.itemWidth = 205;
					this.itemHeight = 205;
					this.interval = 24;
					this.mainPadding = 6;
					break;
				case 720:
					this.itemWidth = 270;
					this.itemHeight = 270;
					break;
				case 1080:
					this.itemWidth = 405;
					this.itemHeight = 405;
					break;
				}
				break;
			}

			if (o.gridMode === 'text') {
				switch (sf.ui.height) {
				case 540:
					this.textSize = 60;
					break;
				case 720:
					this.textSize = 80;
					break;
				case 1080:
					this.textSize = 120;
					break;
				}
				this.textSize -= this.interval;
			} else {
				this.textSize = 0;
			}

			alert('[sf.Grid][_calculatingPicSize()] Item width: ' + this.itemWidth + ' | ' + 'Item height: ' + this.itemHeight + '|' +
				'Interval between images: ' + this.interval + ' | ' +
				'Items container padding: ' + this.mainPadding);

			this.columns = (Math.floor((o.gridWidth - this.interval - 2 * this.mainPadding) / (this.itemWidth + this.interval)));

			alert('[sf.Grid][_calculatingPicSize()] Number of collumns that can be putted depending of size declared: ' + this.columns);
		},

		/**
		* Calculating variables needed to show grid.
		* @private
		*/

		_calculatingVariables: function () {
			alert('[sf.Grid][_calculatingVariables()]');

			var o = this.options;

			this.items = o.gridSrc.length;

			alert('[sf.Grid - _calculatingVariables] Number of items: ' + this.items);

			this.rows = Math.ceil(this.items / this.columns);

			alert('[sf.Grid - _calculatingVariables] Number of rows: ' + this.rows);

			this.visibleRows = Math.ceil((o.gridHeight - this.interval - 2 * this.mainPadding) / (this.itemHeight + this.interval + this.textSize));

			alert('[sf.Grid - _calculatingVariables] Number of visible rows: ' + this.visibleRows);

			this.visibleItems = this.visibleRows * this.columns;

			alert('[sf.Grid - _calculatingVariables] Number of visible items: ' + this.visibleItems);

			this.compWidth = this.columns * (this.itemWidth + this.interval) + this.interval;

			alert('[sf.Grid - _calculatingVariables] Width of Items container: ' + this.compWidth);

			this.sContainerWidth = this.itemWidth + 2 * this.sPartSize;

			this.sContainerHeight = this.itemHeight + 2 * this.sPartSize;

			alert('[sf.Grid - _calculatingVariables] Width of Container with shadows: ' + this.sContainerWidth + 'Height of Container with shadows: ' + this.sContainerHeight);

			this.sEdgeWidth = this.sContainerWidth - 2 * this.shadowSize;

			this.sEdgeHeight = this.sContainerHeight - 2 * this.shadowSize;

			alert('[sf.Grid - _calculatingVariables] Shadow edge width: ' + this.sEdgeWidth + 'Shadow edge height: ' + this.sEdgeHeight);
		},

		/**
		* Filling generated grid with items.
		* @param {Number} position Starting position.
		* @private
		*/

		_fillingItems: function (pageNumber) {
			var that = this,
				w = this.widget(),
				o = this.options,
				allImages = $(w.find('.' + this.baseCssClass + '-itemsContainer')).find('img'),
				startingPosition = pageNumber * this.visibleItems;
			o.page = pageNumber;
			allImages.each(function (index) {
				if ((startingPosition + index) < o.gridSrc.length) {
					$(this).parent().show();
					$(this).attr('src', o.gridSrc[startingPosition + index].url);
					if (o.gridMode === 'title' && o.gridSrc[startingPosition + index].title !== '') {
						$(this).parent().find('.' + that.baseCssClass + '-item-title').text(o.gridSrc[startingPosition + index].title);
					} else if (o.gridMode === 'text') {
						$(this).parent().find('.' + that.baseCssClass + '-item-textTitle').text(o.gridSrc[startingPosition + index].title);
						$(this).parent().find('.' + that.baseCssClass + '-item-textSubtitle').text(o.gridSrc[startingPosition + index].subtitle);
					}
					alert('[sf.Grid][_fillingItems()]' + index + ':' + o.gridSrc[startingPosition + index].url);
				} else {
					$(this).parent().hide();
				}
			});
		},

		/**
		* Adding each item container with shadows and img.
		* @private
		*/

		_addingContainers: function () {
			alert('[sf.Grid][_addingContainers()]');
			var o = this.options,
				v = this.view,
				t = this.templates,
				actAddingItem = 0;

			v.grid.css({'height': o.gridHeight, 'width': this.compWidth + 2 * this.mainPadding});

			v.itemsContainer = $.tmpl(t.itemsContainer, {'baseCssClass' : this.baseCssClass, 'gridItemsCssClass': o.gridItemsCssClass});
			v.itemsContainer.css({'width': this.compWidth, 'padding': this.mainPadding, 'padding-top': this.mainPadding + this.interval});
			v.grid.append(v.itemsContainer);

			for (actAddingItem = 0; actAddingItem < this.visibleItems; ++actAddingItem) {
				v.item = $.tmpl(t.item, {'baseCssClass' : this.baseCssClass});
				//v.item.attr('id', 'item-' + actAddingItem);

				v.item.css({'height': this.itemHeight + 'px', 'width': this.itemWidth + 'px', 'margin-bottom': this.interval + this.textSize + 'px', 'margin-left': this.interval + 'px'});
				v.item.append('<img src="" width="' + this.itemWidth + 'px" height="' + this.itemHeight + 'px" />');

				v.shadowContainer = $.tmpl(t.shadowContainer, {'baseCssClass' : this.baseCssClass, 'width': this.sEdgeWidth, 'height': this.sEdgeHeight});
				v.shadowContainer.css({'width': this.sContainerWidth + 'px', 'height': this.sContainerHeight + 'px', 'top': '-' + this.sPartSize + 'px', 'left': '-' + this.sPartSize + 'px'});
				v.item.append(v.shadowContainer);
				v.itemsContainer.append(v.item);

				if (o.gridMode === 'title') {
					v.title = $.tmpl(t.title, {'baseCssClass' : this.baseCssClass});
					v.item.append(v.title);
					v.title.css({'width': (this.itemWidth - 22)});
				} else if (o.gridMode === 'text') {
					v.textTitle = $.tmpl(t.textTitle, {'baseCssClass' : this.baseCssClass});
					v.item.append(v.textTitle);
					v.textTitle.css({'width': this.itemWidth});
					v.textSubtitle = $.tmpl(t.textSubtitle, {'baseCssClass' : this.baseCssClass});
					v.item.append(v.textSubtitle);
					v.textSubtitle.css({'width': this.itemWidth});
				}
			}
			this._mouseEvents();
			this._addingScroller();
		},

		/**
		* Adding scroller container
		* @private
		*/

		_addingScroller: function () {
			alert('[sf.Grid][_addingScroller()]');
			var o = this.options,
				v = this.view,
				t = this.templates;

			v.scroll = $.tmpl(t.scroll, {'baseCssClass' : this.baseCssClass});
			v.scroll.css({'right': (this.interval + this.mainPadding) / 4 - 1});
			v.grid.append(v.scroll);

			this.pages = Math.ceil(this.rows / this.visibleRows);

			this.actPage = Math.floor((this.options.page * this.visibleItems +  this.options.actItem) / this.visibleItems) + 1;

			this.actRow = Math.floor((this.options.page * this.visibleItems +  this.options.actItem) / this.columns) + 1;

			if (o.scrollType === 'r') {
				$(v.scroll).sfScroll2({"orientation": o.gridOrient, "page": this.actRow, "pages": this.rows});
			}
			else {
				$(v.scroll).sfScroll2({"orientation": o.gridOrient, "page": this.actPage, "pages": this.pages});
			}
		},

		/**
		* Binds mouse events to list elements.
		* @private
		*/

		_mouseEvents: function () {
			var o = this.options,
				that = this;

			// bind events to every list item
			$("." + this.baseCssClass + "-item").each(function (index) {
				$(this).bind({
					mouseover: function () {
						that._cursorOverOn(index);
					},
					mouseout: function () {
						that._cursorOverOff(index);
					},
					click: function () {
						that.mouseOver = index;
						o.callback({"getMouseOverItem": index, "getMouseOverFromSource": index + o.page * that.visibleItems});
					}
				});

			});
		},

		/**
		* Move to position
		* @private
		*/

		_move: function (newPosition) {
			if (this.isFocused) {
				alert('[sf.Grid][_move(newPosition)]');
				var w = this.widget(),
					z = 0,
					o = this.options,
					v = this.view,
					allItems = $(w.find('.' + this.baseCssClass + '-itemsContainer')).find('div.' + this.baseCssClass + '-item');

				alert('[sf.Grid][_move()] actItem:' + o.actItem);
				alert('[sf.Grid][_move()] New Position:' + newPosition);

				z = (this.visibleRows * (this.itemHeight + this.interval + this.textSize) + this.interval + 2 * this.mainPadding) - o.gridHeight;
				alert('[sf.Grid][_move()] z:' + z);

				this._highlightOff(o.actItem);

				if (newPosition >= this.visibleItems) {
					if ((o.page * this.visibleItems + this.visibleItems) < this.items) {
						alert('[sf.Grid][_move()] Moving to another page');
						this._fillingItems(o.page + 1);
						newPosition = newPosition - this.visibleItems;
						$('.' + this.baseCssClass + '-itemsContainer').css('top', '0px');
						this.topPosition = 0;
					} else {
						alert('[sf.Grid][_move()] Bottom of the list');
					}
				} else if (newPosition < 0) {
					if ((o.page - 1) * this.visibleItems >= 0) {
						alert('[sf.Grid][_move()] Moving to another page');
						this._fillingItems(o.page - 1);
						newPosition = this.visibleItems - Math.abs(newPosition);
						$('.' + this.baseCssClass + '-itemsContainer').css('top', -Math.abs(z) + 'px');
						this.topPosition = 1;
					} else {
						alert('[sf.Grid][_move()] Top of the list');
					}
				}
				if ((newPosition >= this.visibleItems - this.columns) &&
					(newPosition < this.visibleItems) &&
					(o.page * this.visibleItems + newPosition < this.items)) {
					if (Math.abs(z) > 0) {
						$('.' + this.baseCssClass + '-itemsContainer').css('top', -Math.abs(z) + 'px');
						this.topPosition = 1;
					}
				} else if ((newPosition < this.visibleItems - (this.visibleRows - 1) * this.columns) && (newPosition >= 0)) {
					$('.' + this.baseCssClass + '-itemsContainer').css('top', '0px');
					this.topPosition = 0;
				}
				if (o.page * this.visibleItems + newPosition >= this.items) {
					newPosition = this.items - o.page * this.visibleItems - 1;
				}
				if (newPosition < 0) {
					newPosition = 0;
				}
				o.actItem = newPosition;

				this._highlightOn(o.actItem);

				if (o.scrollType === 'r') {
					if (this.actRow > Math.floor((o.page * this.visibleItems +  o.actItem) / this.columns) + 1) {
						this.actRow = Math.floor((o.page * this.visibleItems +  o.actItem) / this.columns) + 1;
						$(v.scroll).sfScroll2("previousPage");
					}
					else if (this.actRow < Math.floor((o.page * this.visibleItems +  o.actItem) / this.columns) + 1) {
						this.actRow = Math.floor((o.page * this.visibleItems +  o.actItem) / this.columns) + 1;
						$(v.scroll).sfScroll2("nextPage");
					}
				}
				else {
					if (this.actPage > Math.floor((o.page * this.visibleItems +  o.actItem) / this.visibleItems) + 1) {
						this.actPage = Math.floor((o.page * this.visibleItems +  o.actItem) / this.visibleItems) + 1;
						$(v.scroll).sfScroll2("previousPage");
					}
					else if (this.actPage < Math.floor((o.page * this.visibleItems +  o.actItem) / this.visibleItems) + 1) {
						this.actPage = Math.floor((o.page * this.visibleItems +  o.actItem) / this.visibleItems) + 1;
						$(v.scroll).sfScroll2("nextPage");
					}
				}
				alert('[sf.Grid][_move()] actItem:' + o.actItem);
			}
		},

		/**
		* Moving right
		*/

		moveRight: function () {
			alert('[sf.Grid][rightItem()]');
			this._move(this.options.actItem + 1);

		},

		/**
		* Moving left
		*/

		moveLeft: function () {
			alert('[sf.Grid][leftItem()]');
			this._move(this.options.actItem - 1);

		},

		/**
		* Moving Down
		*/

		moveDown: function () {
			alert('[sf.Grid][moveDown()]');
			this._move(this.options.actItem + this.columns);

		},

		/**
		* Moving Up
		*/

		moveUp: function () {
			alert('[sf.Grid][moveUp()]');
			this._move(this.options.actItem - this.columns);

		},

		/**
		* Next page
		*/

		nextPage: function () {
			alert('[sf.Grid][nextPage()]');
			this._move(this.options.actItem + this.visibleItems);
		},

		/**
		* Previous page
		*/

		prevPage: function () {
			alert('[sf.Grid][prevPage()]');
			this._move(this.options.actItem - this.visibleItems);
		},

		_highlightOn: function (itemNumber) {
			alert('[sf.Grid][_highlightOn()]');
			var w = this.widget(),
				allItems = $(w.find('.' + this.baseCssClass + '-itemsContainer')).find('div.' + this.baseCssClass + '-item');
			$(allItems[itemNumber]).addClass('highlighted');
		},

		_highlightOff: function (itemNumber) {
			alert('[sf.Grid][_highlightOff()]');
			var w = this.widget(),
				allItems = $(w.find('.' + this.baseCssClass + '-itemsContainer')).find('div.' + this.baseCssClass + '-item');
			$(allItems[itemNumber]).removeClass('highlighted');
		},

		_cursorOverOn: function (itemNumber) {
			alert('[sf.Grid][_highlightOn()]');
			if (this.isFocused) {
				var w = this.widget(),
					allItems = $(w.find('.' + this.baseCssClass + '-itemsContainer')).find('div.' + this.baseCssClass + '-item');
				$(allItems[itemNumber]).addClass('cursor-over');
			}
		},

		_cursorOverOff: function (itemNumber) {
			alert('[sf.Grid][_highlightOff()]');
			var w = this.widget(),
				allItems = $(w.find('.' + this.baseCssClass + '-itemsContainer')).find('div.' + this.baseCssClass + '-item');
			$(allItems[itemNumber]).removeClass('cursor-over');
		},

		/**
		 * Focus
		 */

		focus: function() {
			this.isFocused = true;
			this._highlightOn(this.options.actItem);
		},

		/**
		 * Blur
		 */

		blur: function() {
			this.isFocused = false;
			this._highlightOff(this.options.actItem);
		},

		/**
		 * Constructor
		 */

		_create: function () {
			alert('[sf.Grid][_create()]');
			var o = this.options,
				v = this.view,
				t = this.templates,
				w = this.widget();

			w.css('visibility', 'hidden');

			this._choosingPicSize();

			if (this.columns > 0) {
				this._calculatingVariables();
				v.grid = $.tmpl(t.grid, {baseCssClass : this.baseCssClass, gridCssClass: o.gridCssClass});
				w.append(v.grid);
				this._addingContainers();
				this._fillingItems(this.options.page);
				this._move(this.options.actItem);
			} else {

				alert('[sf.Grid][_create()] Error: Grid width is to small!!!');
			}

			w.css('visibility', 'visible');
		},

		/**
		 * Destructor
		 */

		_destroy: function () {
			this.widget().remove();
		}
	});

	sf.ui.bridge('sfGrid', sf.ui.Grid);
	sf.ui.addSelector('sfGrid');


})(jQuery);
/*
 * Title: UI Components (HScroll)
 * Apps Framework HScroll UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfHScroll";

	/*
		Class: sf.ui.HScroll (jQuery.sfHScroll)
		Apps Framework Class for HScroll UI
	*/
	sf.ui.HScroll = sf.ui.widgetFactory({
		/** @lends sf.ui.HScroll.prototype */

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-HScroll'
		 * @type String
		 */
		baseCssClass: 'sf-ui-hscroll',

		/**
		 * Event namespace added by default to every event triggered by HScroll.
		 * @default "sfHScroll"
		 * @type String
		 */
		eventNamespace: "sfHScroll",

		/*
		 	Function: jQuery.sfHScroll
				Horizontal Scroll instance state object.

			Parameter:
				o (Object) options - options for scroll
					pages - (Number) (defaults to 0) Number of pages represented by hscroll
					currentPage - (Number) (defaults to 0) Current page shown on hscroll
					bgCssClass - (String) (defaults to 'sf-ui-hscroll-bg') CSS class of background element of the hscroll
					bgLeftCssClass - (String) (defaults to 'sf-ui-hscroll-bg-left') CSS class of background left element of the hscroll
					bgCenterCssClass - (String) (defaults to 'sf-ui-hscroll-bg-center') CSS class of background center element of the hscroll
					bgRightCssClass - (String) (defaults to 'sf-ui-hscroll-bg-right') CSS class of background right element of the hscroll
					bodyCssClass - (String) (defaults to 'sf-ui-hscroll-body') CSS class of body element of the hscroll
					leftCssClass - (String) (defaults to 'sf-ui-hscroll-body-left') CSS class of hscroll left part element
					centerCssClass - (String) (defaults to 'sf-ui-hscroll-body-center') CSS class of hscroll center part element
					rightCssClass - (String) (defaults to 'sf-ui-hscroll-body-right') CSS class of hscroll right part element

			Returns:
				(Object) jQuery object

			Rendered HTML:
				Before init:
				(start code)
				<div id="hscroll1"></div>
				(end)

				After init:
				(start code)
				<div id="hscroll1" class="sf-ui-hscroll">
					<div class="sf-ui-hscroll-bg">
						<div class="sf-ui-hscroll-bg-left"></div>
						<div class="sf-ui-hscroll-bg-center"></div>
						<div class="sf-ui-hscroll-bg-right"></div>
					</div>
					<div class="sf-ui-hscroll-body">
						<div class="sf-ui-hscroll-body-left"></div>
						<div class="sf-ui-hscroll-body-center"></div>
						<div class="sf-ui-hscroll-body-right"></div>
					</div>
				</div>
				(end)
		 */
		options: {
			pages: 0,
			currentPage: 0,
			bgCssClass: 'sf-ui-hscroll-bg',
			bgLeftCssClass: 'sf-ui-hscroll-bg-left',
			bgCenterCssClass: 'sf-ui-hscroll-bg-center',
			bgRightCssClass: 'sf-ui-hscroll-bg-right',
			bodyCssClass: 'sf-ui-hscroll-body',
			leftCssClass: 'sf-ui-hscroll-body-left',
			centerCssClass: 'sf-ui-hscroll-body-center',
			rightCssClass: 'sf-ui-hscroll-body-right'
		},

		pageWidth: 0,
		scrollSize: 0,
		templates: {
			bg: '<div class="${bgCssClass}"><div class="${bgLeftCssClass}"></div><div class="${bgCenterCssClass}"></div><div class="${bgRightCssClass}"></div></div>',
			body: '<div class="${bodyCssClass}"><div class="${leftCssClass}"></div><div class="${centerCssClass}"></div><div class="${rightCssClass}"></div></div>'
		},
		totalWidth: 0,

		widgetName: widgetName,

		_create: function () {
			alert("[sf.HScroll] _create()");

			var o = this.options,
				t = this.templates,
				v = this.view,
				w = this.widget();

			this._super();

			this.totalWidth = w.width();
			alert("totalWidth = " + this.totalWidth);

			$.each(['bg', 'body'], function (index, name) {
				v[name] = $.tmpl(t[name], o);
			});
			w.append(v.bg, v.body);
			this._updateScroll();
			alert("SCROLL HTML : " + w.html());
		}, // _create()

		_destroy: function () {
			var v = this.view;

			$.each(['bg', 'body'], function (index, name) {
				v[name].remove();
			});
		}, // _destroy()


		/*
		 * Move scroll to requested index
		 * @param {Number} newPage Page number to go to
		 * @private
		 */
		_move: function (newPage) {
			//alert('[sf.HScroll] _move(' + newPage + ")");

			var o = this.options,
				oldPage = o.currentPage;

			if (newPage >= 0 && newPage < o.pages) {
				o.currentPage = newPage;
				this._updateScrollPosition();

				//alert('    > current page: ' + o.currentPage);
				this._trigger("move", null, {
					previousPage: oldPage,
					currentPage: newPage
				});
			}

			return this;
		}, // _move()

		_setOption: function (name, value) {
			alert("[sf.HScroll] _setOption(" + name + ", " + value + ")");

			var o = this.options;

			switch (name) {
				case "pages":
					o.pages = value;
					if(o.currentPage > o.pages-1) {
						o.currentPage = o.pages-1;
					}
					this._updateScroll();
					break;

				case "currentPage":
					this.move(value);
					break;

				case "bgCssClass":		// INTENTIONAL FALLBACK
				case "leftCssClass":	// INTENTIONAL FALLBACK
				case "centerCssClass":	// INTENTIONAL FALLBACK
				case "rightCssClass":
					if (value) {
						this._switchCssClass(name, value, this.view[name.split("CssClass")[0]]);
					}
					break;
			} // switch(name)
		}, // _setOption()

		_switchCssClass: function (name, value, element) {
			//alert("[sf.HScroll] _switchCssClass(" + [name, value, element].join(", ") + ")");

			var o = this.options;

			value = value || "";
			element.removeClass(o[name]).addClass(value);
			o[name] = value;
		}, // _switchCssClass()

		_updateScroll: function () {
			alert("[sf.HScroll] _updateScroll()");

			this._updateScrollSettings();
			this._updateScrollPosition();
		}, // _updateScroll()

		_updateScrollSettings: function () {
			alert("[sf.HScroll] _updateScrollSettings()");
			var MIN_SCROLLSIZE = 30;

			var o = this.options,
				v = this.view;

			//v.bg.css("height", this.totalWidth);
			var bgLeftWidth = v.bg.find('.'+o.bgLeftCssClass).width();
			var bgRightWidth = v.bg.find('.'+o.bgRightCssClass).width();
			v.bg.find('.'+o.bgCenterCssClass).css("width", this.totalWidth-(bgLeftWidth+bgRightWidth));
			//alert("TOTAL HEIGHT: " + this.totalHeight);
			//alert("PAGES: " + o.pages);

			this.scrollSize = Math.round(this.totalWidth / o.pages);
			if (this.scrollSize < MIN_SCROLLSIZE) {
				this.scrollSize = MIN_SCROLLSIZE;
			}
			//alert("SCROLL SIZE: " + this.scrollSize);

			this.pageWidth = this.totalWidth - this.scrollSize;
			if (o.pages > 1) {
				this.pageWidth /= o.pages - 1;
			}
			//alert("PAGE STEP: " + this.pageWidth);
			if (true) {//this.scrollSize >= MIN_SCROLLSIZE) {
				var leftwidth = v.body.find('.'+o.leftCssClass).width();
				var rightwidth = v.body.find('.'+o.rightCssClass).width();
				alert("Set middle of body size to : " + (this.scrollSize - (leftwidth+rightwidth)));
				v.body.find('.'+o.centerCssClass).width(this.scrollSize - (leftwidth+rightwidth));	// size - (topHeight+bottomHeight)
			}
		}, // _updateScrollSettings()

		_updateScrollPosition: function () {
			alert("[sf.HScroll] _updateScrollPosition()");

			var v = this.view,
				o = this.options,
				scrollLeft;

			scrollLeft = Math.ceil(this.pageWidth * o.currentPage);
			v.body.css("left", scrollLeft);
			alert("Move Left to : " + scrollLeft);
		}, // _updateScrollPosition()

		/*
		Function: move
			Move scroll to requested index

		Parameters:
			o (Number) pageIndex - Page number to go to

		Returns:
			(Object) jQuery object

		Description:
			Move scroll to requested index

		Example 1:
			This example move scroll to requested index
			(start code)
				$('#list').sfScroll('move', 3);
			(end)

		Available From:
			1.1.0

		History:
		*/
		move: function (pageIndex) {
			alert('[sf.HScroll] move(' + pageIndex + ")");

			this._move(pageIndex);

			return this;
		}, // move()

		/*
		Function: next
			Move scroll to next page

		Parameters:
			N/A

		Returns:
			(Object) jQuery object

		Description:
			If currentPage (see {@link #options}) is set to last page, *next* function has no effect.

		Example 1:
			This example move scroll to next page
			(start code)
			$('#list').sfScroll('move', 3);
			(end)

		Available From:
			1.1.0

		History:
		*/
		next: function () {
			alert("[sf.HScroll] next()");
			this._move(this.options.currentPage + 1);

			return this;
		}, // next()

		/*
		Function: prev
			Move scroll to prev page

		Parameters:
			N/A

		Returns:
			(Object) jQuery object

		Description:
			If "currentPage" option is set to first page(0), *prev* function has no effect.

		Example 1:
			This example move scroll to prev page
			(start code)
			$('#list').sfScroll('prev');
			(end)

		Available From:
			1.1.0

		History:
		*/
		prev: function () {
			alert("[sf.HScroll] prev()");

			this._move(this.options.currentPage - 1);

			return this;
		} // prev()

	}); // sf.ui.Scroll()

	sf.ui.bridge(widgetName, sf.ui.HScroll);

	sf.ui.addSelector(widgetName);

}(jQuery));

/*
 * Title: UI Components (Popup)
 * Apps Framework Popup UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfPopup";
	/*
		Class: sf.ui.Popup (for jQuery.sfPopup)
		Apps Framework Class for Popup UI

		Function: jQuery.sfPopup
			Popup component can display simple one or two button popup.

		Parameters:
			o options	- (Object) details below.
				text	- (String) popup message.
				buttons	- (String|Array<String>) (default to []) Strings of each buttons
				defaultFocus	- (Number) (default to 0) index of default focused button. this indicates index of "buttons" option. (zero-based)
				callback	- (Function(index)) (default to null) callback to be called when the popup is closed. "index" is index of selected button. -1 will be passed when the popup is closed by timeout or RETURN/EXIT button.

		Returns:
			(Object) jQuery object

		Rendered HTML:
			if the "text" option is "Submit this data?".

			Before init:
			(start code)
			<div id="popup1"></div>
			(end)

			After init:
			(start code)
			<div id="popup1" class="sf-ui-popup"">
				<!-- background -->
				<div class="sf-ui-popup-bg">
					<table cellpadding="0px" cellspacing="0px">
					    <tbody>
					        <tr height="75px"><td class="sf-ui-popup-bg-t-l"></td><td class="sf-ui-popup-bg-t-c"></td><td class="sf-ui-popup-bg-t-r"></td></tr>
					        <tr><td class="sf-ui-popup-bg-m-l"></td><td class="sf-ui-popup-bg-m-c"></td><td class="sf-ui-popup-bg-m-r"></td></tr>
					        <tr height="45px"><td class="sf-ui-popup-bg-b-l"></td><td class="sf-ui-popup-bg-b-c"></td><td class="sf-ui-popup-bg-b-r"></td></tr>
					    </tbody>
					</table>
				</div>

				<!-- text -->
				<div class="sf-ui-popup-text">Submit this data?</div>

				<!-- line between text and buttons. if buttons are not exist, this line will be hidden -->
				<div class="sf-ui-popup-line"><div class="sf-ui-popup-line-l"></div><div class="sf-ui-popup-line-c"></div><div class="sf-ui-popup-line-r"></div></div>

				<!-- buttons. if "buttons" options is not defined or the length is 0, this will be hidden -->
				<div class="sf-ui-popup-buttons">
				    <div class="sf-ui-button sf-ui-button-focus">
				        <div class="sf-ui-button-l focus"></div>
				        <div class="sf-ui-button-c focus" style="width:100px;">OK</div>
				        <div class="sf-ui-button-r focus"></div>
				    </div>
					... and other buttons
				</div>
				<a href="javascript:void(0);" onkeydown="$('#popup1').sfPopup('key');"></a>
			</div>
			(end code)

		Example 1:
		    One button Popup initializing example
		    (start code)
	        $('#popup').sfPopup({
	            text: "Would You like to close this popup?",
	            buttons: "OK"
	        });
	        (end)

		Example 2:
		    Two button Popup initializing example
		    (start code)
		    $('#popup').sfPopup({
		        text: "Would You like to close this popup?",
		        buttons: ["Yes", "No"],
		        defaultFocus: 1,	// index of default focused button. this indicates array index of "buttons" option (zero-based)
		        callback: function(selectedIndex) {
		            alert("selected button index: " + selectedIndex);
		        }
		    });
		    (end)

		Example 3:
		    Popup 'setOption()' method usage example (change popup text)
		    (start code)
	        $('#popup').sfPopup({
	            text: "Would You like to close this popup?",
	            buttons: "OK"
	        });

	        $('#popup').sfPopup("option", "text", "Changed text");
	        (end)
	*/
	sf.ui.Popup = sf.ui.widgetFactory({
	/** @lends sf.ui.Popup.prototype */

		/**
		 * Widget name
		 * @default 'sfPopup'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Event namespace added by default to every event triggered by Popup.
		 * @default "sfpopup"
		 * @type String
		 */
		eventNamespace: "sfpopup",

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-popup'
		 * @type String
		 */
		baseCssClass: 'sf-ui-popup',

		/**
		 * Popup instance state object.
		 * <dl class="detailList">
		 *   <dt class="heading">Properties:</dt>
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>text</b> (defaults to '')
		 *   </dt>
		 *   <dd>Text message displayed in popup.</dd>
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>focus</b> (defaults to "right")
		 *   </dt>
		 *   <dd>Sets default focus on right or left button.</dd>
		 *
		 *   <dt>
		 *     <span class="light fixedFont">{Object}</span> <b>timeOut</b> (defaults to {one: 10000, two: 30000})
		 *   </dt>
		 *   <dd>Time (ms) after which, popup will close. <b>one</b> field of <b>timeOut</b> object holds time after which one button type popup will close. Respectively <b>two</b> field holds closing time of two button type popup.</dd>
		 *
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>callback</b> (defaults to null)
		 *   </dt>
		 *   <dd>Function which will be called when user closes popup or when it closes by itself after specified time.</dd>
		 * </dl>
		 * @type Object
		 */
		options: {
			text: '',
			buttons: null,
			focus: 0,
			defaultFocus: 0,
			timeout: 10000,	// timeout to close this popup
			callback: null
		},

		/**
		 * Object with templates used to render sfPopup instance.
		 * <dl class="detailList">
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>popupTpl</b>
		 *   </dt>
		 *   <dd>Main template for sfPopup.</dd>
		 *
		 *   <dt>
		 *     <span class="light fixedFont">{String}</span> <b>buttonText</b>
		 *   </dt>
		 *   <dd>Template for sfPopup button.</dd>
		 * </dl>
		 * @type Object
		 */
		templates: {
			popupTpl:
				'<div class="sf-ui-popup-bg alpha">'+
				'<div class="sf-ui-popup-bg-t"><div class="sf-ui-popup-bg-t-l"></div><div class="sf-ui-popup-bg-t-c"></div><div class="sf-ui-popup-bg-t-r"></div></div>'+
				'<div class="sf-ui-popup-bg-m"><div class="sf-ui-popup-bg-m-l"></div><div class="sf-ui-popup-bg-m-c"></div><div class="sf-ui-popup-bg-m-r"></div></div>'+
				'<div class="sf-ui-popup-bg-b"><div class="sf-ui-popup-bg-b-l"></div><div class="sf-ui-popup-bg-b-c"></div><div class="sf-ui-popup-bg-b-r"></div></div>'+
				'</div>'+
				'<div class="sf-ui-popup-bg">'+
				'<div class="sf-ui-popup-bg-t"><div class="sf-ui-popup-bg-t-l"></div><div class="sf-ui-popup-bg-t-c"></div><div class="sf-ui-popup-bg-t-r"></div></div>'+
				'<div class="sf-ui-popup-bg-m"><div class="sf-ui-popup-bg-m-l"></div><div class="sf-ui-popup-bg-m-c"></div><div class="sf-ui-popup-bg-m-r"></div></div>'+
				'<div class="sf-ui-popup-bg-b"><div class="sf-ui-popup-bg-b-l"></div><div class="sf-ui-popup-bg-b-c"></div><div class="sf-ui-popup-bg-b-r"></div></div>'+
				'</div>'+
				'<div class="sf-ui-popup-text"></div>'+
				//'<div class="sf-ui-popup-line"><div class="sf-ui-popup-line-l"></div><div class="sf-ui-popup-line-c"></div><div class="sf-ui-popup-line-r"></div></div>'+
				'<div class="sf-ui-popup-buttons"></div>'+
				'<a href="javascript:void(0);" onkeydown="$(\'#${id}\').sfPopup(\'key\');"></a>',		// anchor
			buttonItem: '<div class="sf-ui-button">'+
						'<div class="sf-ui-button-l"></div>'+
						'<div class="sf-ui-button-c">${buttontext}</div>'+
						'<div class="sf-ui-button-r"></div>'+
						'</div>'
		},

		/**
		 * Holds timer which end results in closing popup and calling callback function.
		 * @type Object
		 */
		timer: null,

		// index is matched to number of buttons. (two button popup closed after 10sec.)
		// If the buttons are more than 2, the timeout is N x 5secs.(3 button popup closed after 15secs)
		timeOutSecs: [5, 5, 10],

		/**
		 * @type Number
		 */
		popupWidth: 0,
		popupHeight: 0,


		/**
		 * Creates popup. <br />
		 * In details: <br />
		 * Depending on <b>num</b> option builds one or two button popup.
		 * @private
		 */
		_init: function() {
			alert("[sf.Popup] _init()");
			var o = this.options,
				v = this.view,
				w = this.widget();

			this._beforeLayoutSetting();
			w.empty().append($.tmpl(this.templates.popupTpl, {id: w.attr('id')}));
			v.text = w.find('.sf-ui-popup-text');

			if (o.buttons) {
				if (typeof o.buttons == "string") {	// one button
					o.buttons = [o.buttons];
				}
				alert("popup with " + o.buttons.length + " buttons");
			}
			else {
				o.buttons = [];
				alert("popup with no button");
			}
			this._createButtons();

			this._setPopupLayout();
			this._centerPopup();

			o.timeout = this.timeOutSecs.length-1 >= o.buttons.length ? this.timeOutSecs[o.buttons.length]*1000 : 10000;

			this._afterLayoutSetting();
			w.hide();
			alert("[sf.Popup] _init() END");
		}, // _init()


		/**
		 * Creates two or one button type popup.
		 * If it's two button type popup it sets initial focus on choosed button and sets type of two button type popup.
		 * @param {Number} buttonsNr
		 * @private
		 */
		_createButtons: function() {
			alert("[sf.Popup] _createButtons()");
			var o = this.options,
				v = this.view,
				w = this.widget();

			var btnContainer = w.find('.sf-ui-popup-buttons');
			if(!btnContainer)	return;	// ERROR
			btnContainer.empty();

			v.buttons = null;
			if (o.buttons) {
				v.buttons = [];
				alert(o.buttons.length+" buttons");
				for (var i = 0; i < o.buttons.length; i++) {
					var button = $.tmpl(this.templates.buttonItem, {buttontext: o.buttons[i]});
					/* 이걸 하고나면 레이아웃이 깨짐.
					if(button.find('.sf-ui-button-c').width() < 100) {
						button.find('.sf-ui-button-c').width()
					}
					*/
					btnContainer.append(button);
					v.buttons.push(button);
				}
				/*
				w.find('.sf-ui-button-c').each(function (index) {
					alert(this.innerHTML + " : " + $(this).width());
					if($(this).width() < 100) {
						$(this).width(100);
					}
				});*/
			}
		}, // _createButtons()

		_setPopupLayout: function () {
			alert("[sf.Popup] _setPopupLayout()");
			var o = this.options,
				v = this.view,
				w = this.widget();

			this.popupWidth = w.width() || 500;
			alert("this.popupWidth: " + this.popupWidth);

			// Get buttons width
			if (v.buttons && v.buttons.length > 0) { // one or more buttons
				w.find('.sf-ui-button-c').each(function(index){
					if ($(this).width() < 100) {
						$(this).width(100);
					}
				});
			}
			var buttonsWidth = w.find('.sf-ui-popup-buttons').width();

			// Set background width
			var leftWidth = w.find('.sf-ui-popup-bg-m-l').width();
			var rightWidth = w.find('.sf-ui-popup-bg-m-r').width();
			alert("Calculated width from popup width : " + (this.popupWidth-(leftWidth+rightWidth)));
			alert("Calculated width from button width : " + buttonsWidth);
			var bgCenterWidth = Math.max(this.popupWidth-(leftWidth+rightWidth), buttonsWidth);	// width - (left+right)
			alert("bg center width : " + bgCenterWidth);
			w.find('.sf-ui-popup-bg-t-c').width(bgCenterWidth);
			w.find('.sf-ui-popup-bg-m-c').width(bgCenterWidth);
			w.find('.sf-ui-popup-bg-b-c').width(bgCenterWidth);
			this.popupWidth = bgCenterWidth+(leftWidth+rightWidth);
			alert("update popupWidth : " + this.popupWidth);
			w.css('width', this.popupWidth+'px');

			// Set text width
			w.find('.sf-ui-popup-text').css('padding-top', '0px');
			var textLeft = parseInt(w.find('.sf-ui-popup-text').css('left'), 10);
			var textWidth = this.popupWidth-(2*textLeft);		// width - (2*textLeft)
			alert("text width: " + textWidth);
			v.text.width(textWidth).html(o.text);

			// Get text height
			var textheight = w.find('.sf-ui-popup-text').height();
			alert("textheight: " + textheight);			// adjusted text height

			// Set button position and calculate popup height
			var textTop = parseInt(w.find('.sf-ui-popup-text').css('top'), 10);
			var bgTop = w.find('.sf-ui-popup-bg-t').height();
			var bgBottom = w.find('.sf-ui-popup-bg-b').height();
			// one or more buttons
			if(v.buttons && v.buttons.length > 0) {
				var TOPMARGIN_BUTTON = 10;
				var BTMMARGIN_BUTTON = 30;
				w.find('.sf-ui-popup-buttons').css({'left': parseInt((this.popupWidth-buttonsWidth)/2,10)+'px', 'top': (textTop + textheight + TOPMARGIN_BUTTON) + 'px'}).show();
				var buttonsHeight = w.find('.sf-ui-popup-buttons').height();
				this.popupHeight = textTop+textheight+TOPMARGIN_BUTTON+buttonsHeight+BTMMARGIN_BUTTON;
			}
			else {
				w.find('.sf-ui-popup-buttons').hide();
				this.popupHeight = textTop+textheight+textTop;

				// 텍스트가 적어서 팝업에비해 텍스트가 위로 치우치는것을 막음
				if(this.popupHeight-(bgTop+bgBottom) < 0) {
					var paddingtop = parseInt(((bgTop+bgBottom)-this.popupHeight)/2, 10);
					w.find('.sf-ui-popup-text').css('padding-top', paddingtop + 'px');
				}
			}

			// set popup height
			var middleHeight = Math.max(this.popupHeight-(bgTop+bgBottom), 0);
			alert("middle height : " + middleHeight);
			w.find('.sf-ui-popup-bg-m').height(middleHeight);	// height - top - bottom
			w.find('.sf-ui-popup-bg-m div').height(middleHeight);	// height - top - bottom

			w.height(this.popupHeight);
		},

		/**
		 * @private
		 */
		_centerPopup: function() {
			alert("[sf.Popup] _centerPopup()");
			var w = this.widget();

			w.css('left', ($(window).width() - this.popupWidth) / 2); // left offset
			w.css('top', ($(window).height() - this.popupHeight) / 2); // top offset
		}, // _centerPopup()

		/**
		 * Sets focus on choosed Button. Ads 'sf-ui-popup-select' CSS class on choosed button and removes it from the other one.
		 * @param {String} focusOn Button to set focus on.
		 * @private
		 */
		_focus: function(focusedIndex) {
			alert("[sf.Popup] _focus(" + focusedIndex + ")");
			var	o = this.options,
				v = this.view;

			for(var i=0; i<v.buttons.length; i++) {
				if(i == focusedIndex) {
					v.buttons[i].addClass('sf-ui-button-focus').find('div').addClass('focus');
				}
				else {
					v.buttons[i].removeClass('sf-ui-button-focus').find('div').removeClass('focus');
				}
			}

			o.focus = focusedIndex;
		}, // _focus()


		/**
		 * Sets timer basing on fields provided in <b>timeOut</b> field in <b>options</b>.
		 * When timer finishes it closes popup and calls provided callback function.
		 * @private
		 */
		_setTimer: function() {
			alert("[sf.Popup] _setTimer()");
			var self = this,
				o = self.options,
				time = o.timeout;

			this._clearTimer();

			this.timer = setTimeout(function() {
				self.widget().hide();
				if (sf.scene) {
					sf.scene.returnFocus();
				}
				if (typeof o.callback === 'function') {
					o.callback(-1);
				}
			}, time);
			alert("\ttimeout: " + time);
		}, // _setTimer()


		/**
		 * @private
		 */
		_clearTimer: function() {
			alert("[sf.Popup] _clearTimer()");
			if (this.timer) {
				clearTimeout(this.timer);
				this.timer = null;
			}
		}, // _clearTimer()

		_beforeLayoutSetting: function () {
			alert("[sf.ui.Popup] _beforeLayoutSetting()");
			var w = this.widget();
			if(this._bLayoutSetting) {
				alert("\tAlready on setting");
				return;
			}
			this._styleVisibility = w.css('visibility');
			this._styleDisplay = w.css('display');
			this._bLayoutSetting = true;
			//alert("innerHTML : " + document.body.innerHTML);
			w.css({
				'visibility': 'hidden',
				'display': 'block'
			});
			//alert("\tSaved style: visibility - '" + this._styleVisibility + "'");
			//alert("\tSaved style: display - '" + this._styleDisplay + "'");
			//alert("innerHTML : " + document.body.innerHTML);
		},
		_afterLayoutSetting: function () {
			alert("[sf.ui.Popup] _afterLayoutSetting()");
			if(!this._bLayoutSetting) {
				alert("\tNot on setting");
				return;
			}
			var w = this.widget();
			//alert("\tSaved style: visibility - '" + this._styleVisibility + "'");
			//alert("\tSaved style: display - '" + this._styleDisplay + "'");
			//alert("innerHTML : " + document.body.innerHTML);
			w.css({
				'visibility': this._styleVisibility || '',
				'display': this._styleDisplay || ''
			});
			//alert("\tstyle: visibility - '" + w.css('visibility') + "'");
			//alert("\tstyle: display - '" + w.css('display') + "'");
			this._styleVisibility = null;
			this._styleDisplay = null;
			this._bLayoutSetting = false;
			//alert("innerHTML : " + document.body.innerHTML);
		},

		/**
		 * @private
		 */
		_setOption: function(name, value) {
			alert("[sf.Popup] _setOption(" + name + ", " + value + ")");
			var o = this.options,
				w = this.widget();

			this._beforeLayoutSetting();
			switch (name) {
				case 'text':
					if (value) {
						o.text = value;
						this._setPopupLayout();
						this._centerPopup();
					}
					break;
				case 'callback':
					if (value) {
						o.callback = value;
					}
					break;
				case 'timeout':
					if ($.isPlainObject(value)) {
						o.timeout = value;
					}
					else {
						alert('[AF ui] sfPopup could not set timeOut. Bad value.');
					}
					break;
				case 'focus':
					if (value >= 0 && value < o.buttons.length) {
						this._focus(value);
					}
					break;
				case 'buttons':
					if (typeof value == 'string') {
						o.buttons = [value];
					}
					else if(!value.length) {
						o.buttons = null;
					}
					else {
						o.buttons = value;
					}
					this._createButtons();
					this._setPopupLayout();
					this._centerPopup();
					break;
			} // case(name)
			this._afterLayoutSetting();
		}, // _setOption()


		/**
		 * Removes CSS classes added to component element. Empties element from any html.
		 * @private
		 */
		_destroy: function() {
			var w = this.widget();
			w.empty();
		}, // _destroy()


		/**
		 * Shows popup, sets focus on choosed button and initiates timer. Will steal focus from the Scene and return focus when popup is closed.
		 * @return {Object} this
		 */
		/*
		Function: show
		 Shows popup

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Shows popup, sets focus on choosed button and initiates timer. Will steal focus from the Scene and return focus when popup is closed.

		Example 1:
		 This example shows popup
		 (start code)
          $('#list').sfPopup('show');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		show: function() {
			alert("[sf.Popup] show()");
			var w = this.widget(),
				v = this.view,
				o = this.options;

			o.focus = o.defaultFocus;

			w.show();
			this._setTimer(); // popup will close after specific time
			w.find('a').focus();

			// toggle focus css class on choosed button
			this._focus(o.focus);

			return this;
		}, // show()


		/**
		 * Handles key inputs when popup is shown.<br /><br />
		 * In details: <br />
		 * With left/right directional keys user can change button focus.
		 * When pressing ENTER on focused button user closes popup which also results in calling provided callback function.
		 */
		key: function() {

			alert("[sf.Popup] key()");
			alert('[AF ui] sfPopup keyctl('+keyCode+')');
			var keyCode = event.keyCode,
				o = this.options,
				v = this.view,
				w = this.widget();


			this._setTimer();

			switch (keyCode) {
				case (sf.key.LEFT):
					if ( !o.buttons || o.buttons.length <= 1 ) {
						return;
					}
					this._focus((o.focus-1+o.buttons.length)%o.buttons.length);
					break;
				case (sf.key.RIGHT):
					if ( !o.buttons || o.buttons.length <= 1 ) {
						return;
					}
					this._focus((o.focus+1)%o.buttons.length);
					break;

				case (sf.key.ENTER):
					w.hide();
					this._clearTimer();
					if (sf.scene) {
						sf.scene.returnFocus();
					}
					if (typeof o.callback === 'function') {
						o.callback( o.buttons&&o.buttons.length ? o.focus : -1);	// returns selected index if buttons are exist.
					}
					break;

				case (sf.key.RETURN):
				case (sf.key.EXIT):
					sf.key.preventDefault();
					w.hide();
					this._clearTimer();
					if (sf.scene) {
						sf.scene.returnFocus();
					}
					if (typeof o.callback === 'function') {
						o.callback(-1);
					}
					break;
			}
		}

	}); // sf.ui.Popup()

	sf.ui.bridge(widgetName, sf.ui.Popup);

    sf.ui.addSelector(widgetName);

}(jQuery));/*
 * Title: UI Components (Datepicker)
 * Apps Framework Datepicker UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfDatepicker";

	var MARGIN_ITEMS = 4;

	/*
		Class: sf.ui.datepicker (jQuery.sfDatepicker)
		Apps Framework Class for datepicker UI

		Function: jQuery.sfDatepicker
			This widget allows the user to select the time and hours
			By default opens with current  date with formte yyyy-MM-dd
			You can customize the:
			- the return date format
			- set date
			- date picker display time

		Example 1:
			(start code)
			$('#datepicker').sfDatepicker({
			    timePicker : false,
			    format : "yyyy-MM-dd",
			    callback : callbackfn
			});

			function callbackfn(date) {
			    $("#label1").html("Date format : yyyy-MM-dd");
			    $("#label2").html("Return value  :  "+date);
			}
			(end)
	*/
	sf.ui.Datepicker = sf.ui.widgetFactory({
		/* @lends sf.ui.Datepicker.prototype */
		/** @lends sf.ui.Datepicker.prototype */
		baseCssClass: 'sf-ui-datepicker',
		/**
         * Widget name
         * @default 'sfuiDatepicker'
         * @type String
         */
        widgetName: widgetName,

        dayNormalYear: [31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        dayLeapYear: [31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        inputElement: [
            "time_year",
			"time_month",
			"time_day",
			"time_AMPM",
			"time_hour",
			"time_minute"
        ],
        options: {
			timePicker: false,
			time: 60,
			format: "yyyy-MM-dd",
			callback: null,
			date: new Date()
        },
        templates: {
            item: $.template(null,""+
								"<div class='bg alpha'>"+
									"<div class='bgTop'>"+
										"<div class='leftTop'></div>"+
										"<div class='centerTop' style='width:${popupInnerWidth}px'></div>"+
										"<div class='rightTop'></div>"+
									"</div>"+
									"<div class='bgMid'>"+
										"<div class='leftMid'></div>"+
										"<div class='centerMid' style='width:${popupInnerWidth}px;height:${popupInnerHeight}px'></div>"+
										"<div class='rightMid'></div>"+
									"</div>"+
									"<div class='bgBtm'>"+
										"<div class='leftBtm'></div>"+
										"<div class='centerBtm' style='width:${popupInnerWidth}px;'></div>"+
										"<div class='rightBtm'></div>"+
									"</div>"+
								"</div>"+
								"<div class='bg'>"+
									"<div class='bgTop'>"+
										"<div class='leftTop'></div>"+
										"<div class='centerTop' style='width:${popupInnerWidth}px'></div>"+
										"<div class='rightTop'></div>"+
									"</div>"+
									"<div class='bgMid'>"+
										"<div class='leftMid'></div>"+
										"<div class='centerMid' style='width:${popupInnerWidth}px;height:${popupInnerHeight}px'></div>"+
										"<div class='rightMid'></div>"+
									"</div>"+
									"<div class='bgBtm'>"+
										"<div class='leftBtm'></div>"+
										"<div class='centerBtm' style='width:${popupInnerWidth}px;'></div>"+
										"<div class='rightBtm'></div>"+
									"</div>"+
								"</div>"+
								"<div class='titletext' style='width:${popupInnerWidth}px;'>Time Setting</div>"+
								"<div class='keyhelp' style='width:${popupInnerWidth}px;'></div>"+
								"<div class='time_unit' style='width:${popupInnerWidth}px;'>"+
									"<div class='unit_year'>Year</div>"+
									"<div class='unit_month'>Month</div>"+
									"<div class='unit_day'>Day</div>"+
									"{{if timePicker}}" +
									"<div class='unit_AMPM'>AM/PM</div>"+
									"<div class='unit_hour'>Hour</div>"+
									"<div class='unit_minute'>Minute</div>"+
									"{{/if}}" +
								"</div>"+
								"<div class='time_text' style='width:${popupInnerWidth}px;'></div>"+
								"<div class='focus'>"+
									"<div class='bgImg'></div>"+
									"<div class='upImg'></div>"+
									"<div class='downImg'></div>"+
								"</div>"+
								'<a href="javascript:void(0);" onkeydown="$(\'#${id}\').sfDatepicker(\'key\');"></a>'),
			inputCell: $.template(null,"<div class='${selectId}'>${selectText}</div>")
        },
        /**
         * @name default date
         */
        tempDate: {
            year: 0,
            month: 0,
            day: 0,
            ampm: "AM",
            hour: 0,
            minute: 0
        },
        /**
         * @name data helpers
         */
        dataDefault: {
            focusIndex: 0,
            patternParts: /^(yy(yy)?|M(M(M(M)?)?)?|d(d)?|EEE(E)?|a|H(H)?|h(h)?|m(m)?|s(s)?|S)/,
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        },
        /**
         * create
         */
        _create: function () {
            alert("[sf.Datepicker] _create()");
			this._super();
            //this.widget().hide();
        },

        /**
         * initialize widget date picker
         */
        _init: function () {
            alert("[sf.Datepicker] _init()");
            this._createTpl();
            this.widget().hide();
            this._setDate(this.options.date);
        },
        /**
         * create template and add event key
         */
        _createTpl: function () {
            var leftPx, widthPx, parentId;

            //data template
            var dataTpl = {
				timePicker: this.options.timePicker,
				popupInnerWidth: (!this.options.timePicker) ? 3*(85+MARGIN_ITEMS*2) : 6*(85+MARGIN_ITEMS*2),
				popupInnerHeight: (!this.options.timePicker) ? 0 : 0,
				id: this.widget().attr('id'),
				afimagepath: sf.core._afPath.images
            }

            //compile template
			// display가 none인경우, 레이아웃구성후 크기를 못가져오므로 block으로 설정함.
			this.widget().empty().css({
				'visibility': 'hidden',
				'display': 'block'
			});
            $.tmpl(this.templates.item, dataTpl).appendTo(this.widget());

			var popupWidth = this.widget().find('.leftTop').outerWidth(true) + this.widget().find('.centerTop').outerWidth(true) + this.widget().find('.rightTop').outerWidth(true);
			var popupHeight = this.widget().find('.bgTop').outerHeight(true) + this.widget().find('.bgMid').outerHeight(true) + this.widget().find('.bgBtm').outerHeight(true);
			this.widget().find('.bg').width(popupWidth);
			this.widget().find('.bg').height(popupHeight);

			this.widget().css({
				'left': ($(window).width()-this.widget().find('.bg').width())/2 + 'px',
				'top': ($(window).height()-this.widget().find('.bg').height())/2 + 'px'
			});

			// add keyhelp to datepicker
            this.widget().find('.keyhelp').sfKeyHelp({
				'marginLeft': 0,
				'marginRight': 0,
                'iconset': 'WHITE',
                'updown': 'Change',
                'enter': 'Input',
                'return': 'Return'
            });

			this.widget().css('visibility', '');
        },
       /**
		 * set option
		 * @public
		 * @example
		 *
		 * 	format option may take the following formats:
		 *
         *  @example
         *  yy    - Returns the year (last two digits of the year)
         *  yyyyy - Returns the year (four digits)
         *  MMMM  - Returns the month in the form of a string (June)
         *  MMM   - Returns the month in the form of a string, only the
         *          first three characters (Jun)
         *  MM    - Returns the month (from 01-12)
         *  M     - Returns the month (from 1-12)
         *  dd    - Returns the day of the month (from 01-31)
         *  d     - Returns the day of the month (from 1-31)
         *  EEEE  - Returns the day in the form of a string (Monday)
         *  EEE   - Returns the day in the form of a string,
         *          only the first three characters (Jun)
         *  HH    - Returns the hour (from 00-23)
         *  H     - Returns the hour (from 0-23)
         *  hh    - Returns the hour (from 0-23)
         *  h     - Returns the hour (from 0-23)
         *  mm    - Returns the minutes (from 00-59)
         *  m     - Returns the minutes (from 0-59)
         *  a     - Return  AM/PM
		 *
		 * $('#datepicker').sfuiDatepicker('option','format','yyyy-MM-dd HH:mm:ss');
		 *
		 * ------------------------------------------------------------------------
		 *
		 * $('#datepicker').sfuiDatepicker('option','timePicker',true);
		 *
		 *
		 * $('#datepicker1').sfuiDatepicker('option','date',new Date('2000','09','10','17','11'));
		 *
		 */
        _setOption: function (name, value) {
            switch (name) {
            case 'timePicker':
                this.options.timePicker = value;
                this.widget().empty();
                this._createTpl();
                this._updateTimeHtml();
                break;

            case "date":
                this.options.date = value;
                this._setDate(this.options.date)
                this._updateTimeHtml();
                break;
            case "format":
                this.options.format = value;
                break;
            }
        },

        /**
         * function displays the popup from date picker  and  sets current   the date or  time,
         * initialize popup timer and set focus on first element
         * @example
		 * $('#datepicker').sfDatepicker({
	     *   timePicker : false,
	     *   format : "yyyy-MM-dd",
	     *   callback : callbackfn
	     *  });
	     *
		 * $('#datepicker').sfDatepicker('show')
		 * @return {Object} this
         */

		/*
		Function: show
		 displays the popup from date picker and sets current the date or time
		 initialize popup timer and set focus on first element

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 displays the popup from date picker and sets current the date or time
		 initialize popup timer and set focus on first element

		Example 1:
		 This example set focus on particular button
		 (start code)
		  $('#datepicker').sfDatepicker({
		   timePicker : false,
		   format : "yyyy-MM-dd",
		   callback : callbackfn
		  });
		  $('#datepicker').sfDatepicker('show')
		 (end)

		Available From:
		 1.1.0

		History:
		*/
        show: function () {

            alert("[sf.Datepicker] _show()");
            this._setDate(this.options.date);
            //set value
            this._updateTimeHtml();
            this.widget().show();
			//set focus first element
            this._setFocus(0);
            //set Time-popup
            this._popupTimerProcess(this.options.time);

			this.widget().find('a').focus();

            return this;
        },
        /**
         *  function set Focuse on input elements
         */
        _setFocus: function (focusIndex) {
            alert("[sf.Datepicker] _setFocus("+focusIndex+")");
            this.dataDefault.focusIndex = focusIndex;
			var left = parseInt(this.widget().find('.time_unit').css('left'), 10);
			var width = parseInt(this.widget().find('.time_unit div').eq(0).width(), 10);
			alert("FOCUS LEFT: " + (left + focusIndex*(width+MARGIN_ITEMS*2)));
            this.widget().find('.focus').css({
                "left": left + focusIndex*(width+MARGIN_ITEMS*2) + MARGIN_ITEMS + 1
            });
        },

        /**
         *  function return format date
         */
        _getDateFormat: function (date, pattern) {
            alert("[sf.Datepicker] _getDateFormat("+date+","+pattern+")");
            var matched, dd = this.dataDefault,result = [];

            while (pattern.length > 0) {
                dd.patternParts.lastIndex = 0;
                matched = dd.patternParts.exec(pattern);

                if (matched) {
                    result.push(this.patternValue[matched[0]].call(this, date));

                    pattern = pattern.slice(matched[0].length);
                } else {
                    result.push(pattern.charAt(0));
                    pattern = pattern.slice(1);
                }
            }
            return result.join('');
        },
        /**
         * function set time
         * @param {Object} date
         */
        _setDate: function (date) {
            alert("[sf.Datepicker] _setDate(): " + date);
            var ted = this.tempDate;
            if (typeof date === "object") {
                ted.year = date.getFullYear();
                ted.month = this._toFixedWidth(parseInt(date.getMonth(),10) + 1, 2, '0');
                ted.day = this._toFixedWidth(date.getDate(), 2, '0');

                if (date.getHours() > 0 && date.getHours() <= 12) {
                    ted.ampm = "AM";
                    ted.hour = this._toFixedWidth(date.getHours(), 2, '0');
                } else {
                    ted.ampm = "PM";
					if (date.getHours() == 0) {
						ted.hour = this._toFixedWidth(12, 2, '0');
					}
					else {
						ted.hour = this._toFixedWidth(parseInt(date.getHours(), 10) - 12, 2, '0');
					}
                }
                ted.minute = this._toFixedWidth(date.getMinutes(), 2, '0');
            }
        },


        /**
         *  check day in month and year
         * @param {Object} iYear
         * @param {Object} iMonth
         */
        _daysInMonth: function (iYear, iMonth) {
            alert("[sf.Datepicker] _daysInMonth");
            var endDay, isLunar;

            isLunar = (((iYear % 4) === 0) || ((iYear % 4) === 0 && ((iYear % 100) !== 0 || (iYear % 400) === 0)));
			endDay = (isLunar) ? this.dayLeapYear[parseInt(iMonth, 10)] : this.dayNormalYear[parseInt(iMonth, 10)];
            return endDay;

        },
		/**
		 * function formats the data
		 * @param {Object} value
		 * @param {Object} length
		 * @param {Object} fill
		 */
        _toFixedWidth: function (value, length, fill) {
            alert("[sf.Datepicker] _toFixedWidth()");
			var n;
            if (!fill) {
                fill = '0';
            }
            var result = value.toString();
            var padding = length - result.length;

            if (padding < 0) {
                result = result.substr(-padding);
            } else {
                for (n = 0; n < padding; n++) {
                    result = fill + result;
                }
            }
            return result;
        },

        /**
         *  update time
         */
        _updateTimeHtml: function () {
            alert("[sf.Datepicker] _updateTimeHtml()");
            var inpEle = this.inputElement,
                td = this.tempDate,
                dateValue = [];

            dateValue = [{
                selectId: inpEle[0],
                selectText: td.year
            }, {
                selectId: inpEle[1],
                selectText: td.month
            }, {
                selectId: inpEle[2],
                selectText: td.day
            }];


            if (this.options.timePicker) {
                dateValue.push({
                    selectId: inpEle[3],
                    selectText: td.ampm
                }, {
                    selectId: inpEle[4],
                    selectText: td.hour
                }, {
                    selectId: inpEle[5],
                    selectText: td.minute
                });
            }


	    	this.widget().find('.time_text').html($.tmpl(this.templates.inputCell, dateValue));

        },

        /**
         * formater date
         */
        _getDateObject: function (date) {
            alert("[sf.Datepicker] _getDateObject("+date+")");
            var year = date.year,
                month = date.month,
                day = date.day,
                hour;

			var dateString = '';
            if (this.options.timePicker) {
                hour = (date.ampm === "AM") ? date.hour : parseInt(date.hour, 10) + 12;
				hour = hour == 24? 0 : hour;
				return new Date(year, month-1, day, hour, date.minute, 0);
            } else {
				return new Date(year, month-1, day);
            }
			/*
			// Date constructor have to be setted with UTC Time, But the time above is not UTC Time. So, reset with timezone calcuated time.
			alert('\tTime before calculate timezoneoffset: ' + date);
			alert('\tTimeZoneOffset: ' + date.getTimezoneOffset());
			date.setTime(date.getTime() - date.getTimezoneOffset()*60*1000);
			alert('\tTime after calculate timezoneoffset: ' + date);
			return date;
			*/
        },
        /**
         *  it is the Object witch contein patterns date
         *  @example
         *  yy    - Returns the year (last two digits of the year)
         *  yyyyy - Returns the year (four digits)
         *  MMMM  - Returns the month in the form of a string (June)
         *  MMM   - Returns the month in the form of a string, only the
         *          first three characters (Jun)
         *  MM    - Returns the month (from 01-12)
         *  M     - Returns the month (from 1-12)
         *  dd    - Returns the day of the month (from 01-31)
         *  d     - Returns the day of the month (from 1-31)
         *  EEEE  - Returns the day in the form of a string (Monday)
         *  EEE   - Returns the day in the form of a string,
         *          only the first three characters (Jun)
         *  HH    - Returns the hour (from 00-23)
         *  H     - Returns the hour (from 0-23)
         *  hh    - Returns the hour (from 0-23)
         *  h     - Returns the hour (from 0-23)
         *  mm    - Returns the minutes (from 00-59)
         *  m     - Returns the minutes (from 0-59)
         *  a     - Return  AM/PM
         */
        patternValue: {
            yy: function (date) {
                return this._toFixedWidth(date.getFullYear(), 2);
            },
            yyyy: function (date) {
                return date.getFullYear().toString();
            },
            MMMM: function (date) {
                return this.dataDefault.monthNames[date.getMonth()];
            },
            MMM: function (date) {
                return this.dataDefault.monthNames[date.getMonth()].substr(0, 3);
            },
            MM: function (date) {
                return this._toFixedWidth(date.getMonth() + 1, 2);
            },
            M: function (date) {
                return date.getMonth() + 1;
            },
            dd: function (date) {
                return this._toFixedWidth(date.getDate(), 2);
            },
            d: function (date) {
                return date.getDate();
            },
            EEEE: function (date) {
                return this.dataDefault.dayNames[date.getDay()];
            },
            EEE: function (date) {
                return this.dataDefault.dayNames[date.getDay()].substr(0, 3);
            },
            HH: function (date) {
                return this._toFixedWidth(date.getHours(), 2);
            },
            H: function (date) {
                return date.getHours();
            },
            hh: function (date) {
                var hours = date.getHours();
                return this._toFixedWidth(hours > 12 ? hours - 12 : hours, 2);
            },
            h: function (date) {
                return date.getHours() % 12;
            },
            mm: function (date) {
                return this._toFixedWidth(date.getMinutes(), 2);
            },
            m: function (date) {
                return date.getMinutes();
            },
            ss: function (date) {
                return this._toFixedWidth(date.getSeconds(), 2);
            },
            s: function (date) {
                return date.getSeconds();
            },
            S: function (date) {
                return this._toFixedWidth(date.getMilliseconds(), 3);
            },
            a: function (date) {
                return date.getHours() < 12 ? 'AM' : 'PM';
            }
        },
		/**
		 * function navigates the elements and sets the focus
		 * @param {Object} nav
		 *
		 */
        _navigate: function (nav) {
            var next, prev;
            if (nav && this.dataDefault.focusIndex < (this.options.timePicker?6:3)-1) {	// next
				this._setFocus(this.dataDefault.focusIndex+1);
            }
			else if (!nav && this.dataDefault.focusIndex > 0) {	// prev
                this._setFocus(this.dataDefault.focusIndex-1);
            }
        },

        /**
         * Key Event
         */
        key: function () {
            alert("[sf.Datepicker] key()");
            var EKC = event.keyCode,
                fn = this.inputElement[this.dataDefault.focusIndex].replace("_", "");


            this._resetCloseTimer();

            switch (EKC) {

            case sf.key.LEFT:
                this._navigate(false);
                break;
            case sf.key.RIGHT:
                this._navigate(true);
                break;
            case sf.key.UP:
                this._setInput[fn].call(this, true);
				if(fn == 'timeyear' || fn == 'timemonth') {
					var days = this._daysInMonth(this.tempDate.year, this.tempDate.month);
					if (this.tempDate.day > days) {
						this.tempDate.day = days;
					}
				}
                this._updateTimeHtml();
                break;

            case sf.key.DOWN:
                this._setInput[fn].call(this, false);
				if(fn == 'timeyear' || fn == 'timemonth') {
					var days = this._daysInMonth(this.tempDate.year, this.tempDate.month);
					if (this.tempDate.day > days) {
						this.tempDate.day = days;
					}
				}
                this._updateTimeHtml();
                break;

            case sf.key.ENTER:
                this.options.date = this._getDateObject(this.tempDate);
                $.data(this.widget(), 'date', this.options.date);
				alert(this.options.date);

                //return focus
				if (sf.scene) {
					sf.scene.returnFocus();
				}
                this.widget().hide();

                if (typeof this.options.callback != null) {
                    this.options.callback(this._getDateFormat(this.options.date, this.options.format));
                }
                break;
            case sf.key.RETURN:
            case sf.key.EXIT:
                sf.key.preventDefault();
                this.widget().hide();
				if (sf.scene) {
					sf.scene.returnFocus();
				}
                break;
            }
        },
        /**
         *  set popup timer default is 60 second
         * @param {Object} timer
         */
        _popupTimerProcess: function (timer) {
            alert("[sf.Datepicker] _popupTimerProcess()");
            $.data(this.widget(), 'gPopupTimer', (timer * 1000));
            this._resetCloseTimer();
        },
        /**
         *  reset time
         */
        _resetCloseTimer: function () {
            alert("[sf.Datepicker] _resetCloseTimer()");
            var self = this,
                tem;

            if ($.data(this.widget(), 'gPopupInstance') != undefined) {
                clearTimeout($.data(this.widget(), 'gPopupInstance'));
            }


            //$.data(this.widget(), 'gPopupTimer');
            tem = setTimeout(function () {
                self._closePopup();
            }, $.data(this.widget(), 'gPopupTimer'));
            $.data(this.widget(), 'gPopupInstance', tem);
        },
        /**
         *  hide poup and return focus
         */

        _closePopup: function () {
            alert("[sf.Datepicker] _closePopup()");
            $.data(this.widget(), 'callback');
            this.widget().hide();
			if (sf.scene) {
				sf.scene.returnFocus();
			}
        },
		/**
		 * function sets the field values
		 * @param {Object} action
		 * @private
		 */
        _setInput: {
			/**
			 * @inner
			 */
            timeyear: function (action) {
                //action =true ++, action =false --
                var ted = this.tempDate;
                if (action) {
                    if (ted.year < 2900) {
                        ted.year++;
                    }
                } else {
                    if (ted.year > 1900) {
                        ted.year--;
                    }
                }

            },
			/**
			 * @inner
			 */
            timemonth: function (action) {
                //action =true ++, action =false --
                var ted = this.tempDate;
                if (action) {
                    if (ted.month < 12) {
                        ted.month++;
                        ted.month = this._toFixedWidth(ted.month, 2, '0');
                    } else {
                        ted.month = this._toFixedWidth(1, 2, '0');
                    }
                } else {
                    if (ted.month > 1) {
                        ted.month--;
                        ted.month = this._toFixedWidth(ted.month, 2, '0');
                    } else {
                        ted.month = 12;
                    }
                }
            },
			/**
			 * @inner
			 */
            timeday: function (action) {
                //action true ++, action false --
                var ted = this.tempDate;
                if (action) {
                    var endDay = this._daysInMonth(ted.year, ted.month);
                    if (parseInt(this.tempDate.day,10) < endDay) {
                        ted.day++;
                        ted.day = this._toFixedWidth(ted.day, 2, '0');
                    } else {
                        ted.day = this._toFixedWidth(1, 2, '0');
                    }
                } else {
                    if (parseInt(ted.day, 10) > 1) {
                        ted.day--;
                        ted.day = this._toFixedWidth(ted.day, 2, '0');
                    } else {
                        ted.day = this._daysInMonth(ted.year, ted.month);
                    }
                }
            },
			/**
			 * @inner
			 */
            timehour: function (action) {
                //action =true ++, action =false --
                var ted = this.tempDate;
                if (action) {
                    if (ted.hour < 12) {
                        ted.hour++;
                        ted.hour = this._toFixedWidth(ted.hour, 2, '0');
                    } else {
                        ted.hour = this._toFixedWidth(1, 2, '0');
                    }
                } else {
                    if (ted.hour > 1) {
                        ted.hour--;
                        ted.hour = this._toFixedWidth(ted.hour, 2, '0');
                    } else {
                        ted.hour = 12;
                    }
                }
            },
			/**
			 * @inner
			 */
            timeAMPM: function () {
                this.tempDate.ampm = (this.tempDate.ampm === "AM") ? "PM" : "AM";
            },
			/**
			 * @inner
			 */
            timeminute: function (action) {
                //action =true ++, action =false --
                var ted = this.tempDate;
                if (action) {
                    if (ted.minute < 59) {
                        ted.minute++;
                        ted.minute = this._toFixedWidth(ted.minute, 2, '0');
                    } else {
                        ted.minute = this._toFixedWidth(0, 2, '0');
                    }
                } else {
                    if (ted.minute > 0) {
                        ted.minute--;
                        ted.minute = this._toFixedWidth(ted.minute, 2, '0');
                    } else {
                        ted.minute = 59;
                    }
                }
            }
        }

    });

	sf.ui.bridge(widgetName, sf.ui.Datepicker);
    sf.ui.addSelector(widgetName);

}(jQuery));
/*
 * Title: UI Components (Maps)
 * Apps Framework Maps UI Components
 *
 * Version added:
 * 1.1.1
 */
(function($){

    var widgetName = "sfMaps";

	var sCountryCode = sf.core.getEnvValue('country').toUpperCase();
    var bDebugMode = true;

    function mapsLog(msg){
        if (bDebugMode)
            alert("[AF ui.maps] " + msg);
    }
    function mapsMessage(msg){
        alert("[AF ui.maps] " + msg);
    }

	var sImageSrcDir = '/ui.maps';

    var oDefaultMarker = {
        url: sImageSrcDir + '/sfmaps_marker.png',
        anchorpos: [11, 31]
    }

	var oMapsClasses = {};
    // Google Maps API (http://code.google.com/apis/maps/documentation/javascript/)
    // sfMaps의 스펙은 대부분 Google Maps API를 따름.
    oMapsClasses['google'] = {
        checkAvailFunc: function(){
            return window.google && window.google.maps;
        },
        cls: function(elementid, options){
            mapsLog('[sfMapsGoogle(' + elementid + ')]create');
            var oGoogleMapType = {
                'roadmap': google.maps.MapTypeId.ROADMAP,
                'hybrid': google.maps.MapTypeId.HYBRID,
                'satellite': google.maps.MapTypeId.SATELLITE,
                'terrain': google.maps.MapTypeId.TERRAIN
            }
            var oSFMapType = {}
            for (var type in oGoogleMapType) {
                oSFMapType[oGoogleMapType[type]] = type + '';
            }
            mapsLog("Country : " + sCountryCode + oCountryLocations[sCountryCode]);
            var aDefaultLoc = sCountryCode && oCountryLocations[sCountryCode] ? oCountryLocations[sCountryCode] : oCountryLocations['default'];
            this.elmtID = elementid;
            this.bMoving = false;
            this.size = {
                width: parseInt($('#' + elementid).width(), 10),
                height: parseInt($('#' + elementid).height(), 10)
            }

            // Initialize the Map object
            var oDefaultOpts = {
                center: [aDefaultLoc[0], aDefaultLoc[1]],
                zoom: getProjectedScale(this.size.width, this.size.height, aDefaultLoc[2]),
                maptype: 'roadmap',
                disableDefaultUI: true
            };
            options = $.extend(oDefaultOpts, options);
            var mapDiv = document.getElementById(this.elmtID);
            this.objMap = new google.maps.Map(mapDiv, {
                center: new google.maps.LatLng(options.center[0], options.center[1]),
                zoom: options.zoom,
                mapTypeId: oGoogleMapType[options.maptype],
                disableDefaultUI: true
            });

			var _THIS_ = this;
			google.maps.event.addListener(this.objMap, 'idle', function () {
				mapsLog('[sfMapsGoogle(' + this.elmtID + ')] EVENT: idle');
				_THIS_.bMoving = false;
			});

            // Method defines
            this.moveTo = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]moveTo');
                if (!options || !options.center)
                    return;
                if (this.bMoving) {
                    mapsLog('\tmap is already moving...');
                    return;
                }
                this.objMap.panTo(new google.maps.LatLng(options.center[0], options.center[1]));
                this.bMoving = true;
                var _THIS_ = this;
            }
            this.moveBy = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]moveBy');
                if (!options || options.x == undefined || options.y == undefined)
                    return;
                if (this.bMoving)
                    return;

                this.objMap.panBy(options.x, options.y);
                this.bMoving = true;
            }
            this.zoomIn = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]zoomIn');
				this.bMoving = true;
                this.objMap.setZoom(this.objMap.getZoom() + 1);
            }
            this.zoomOut = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]zoomOut');
				this.bMoving = true;
                this.objMap.setZoom(this.objMap.getZoom() - 1);
            }
            this.setMapType = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]setMapType');
                if (options.type == undefined) {
                    mapsLog('\ttype option is not defined...');
                    return;
                }
                if (!oGoogleMapType[options.type]) {
                    mapsLog('\tthere is no such a type : ' + options.type);
                    return;
                }
                return this.objMap.setMapTypeId(oGoogleMapType[options.type]);
            }
            this.getMapType = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]getMapType');
                return oSFMapType[this.objMap.getMapTypeId()];
            }
            this.setCenter = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]setCenter');
                if (!options.center && !options.center instanceof Array && options.center.length != 2) {
                    return false;
                }
                mapsLog('\tCenter: ' + options.center[0] + ', ' + options.center[1]);
                return this.objMap.setCenter(new google.maps.LatLng(options.center[0], options.center[1]));
            }
            this.getCenter = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]getCenter');
                if (!this.objMap) {
                    return null;
                }
                var center = this.objMap.getCenter();
                mapsLog('\tCenter : ' + center.toString());
                return new Array(center.lat(), center.lng());
            }
            /*
             this.checkstreetViewAvailable = function(options){
             mapsLog('[sfMapsGoogle(' + this.elmtID + ')]checkStreetViewAvailable');
             if (!options.callback) {
             return false;
             }
             if (options.radius == undefined) {
             options.radius = 50;
             }
             var service = new google.maps.StreetViewService();
             var _THIS_ = this;
             service.getPanoramaByLocation(_THIS_.objMap.getCenter(), options.radius, function(data, status){
             mapsLog('\tCallback of getPanoramaByLocation');
             if (status == google.maps.StreetViewStatus.OK) {
             mapsLog('\tStat is OK');
             options.callback([data.location.latLng.lat(), data.location.latLng.lng()]);
             }
             else {
             mapsLog('\tStat is not OK');
             }
             });
             }
             this.setstreetView = function(options){
             mapsLog('[sfMapsGoogle(' + this.elmtID + ')]setStreetView');
             if (options.panoramaObject && this.objMap) {
             this.objMap.setStreetView(options.panoramaObject);
             }
             }
             */
            this.setZoom = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]setZoom');
                return this.objMap.setZoom(options.zoom);
            }
            this.getZoom = function(){
                var level = this.objMap.getZoom();
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]getZoom returns ' + level);
                return level;
            }
            this.addMarker = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]addMarker');
                if (!options.image) {
                    options.image = $.extend(oDefaultMarker, {});
                }
                if (!options.location || !options.location instanceof Array || options.location.length != 2) {
                    mapsLog("\tlocation is not defined");
                    return false;
                }
                mapsLog("\tlocation: " + options.location[0] + ", " + options.location[1]);

                return new sfMapsGMarker({
                    location: new google.maps.LatLng(options.location[0], options.location[1]),
                    map: this.objMap,
                    image: options.image
                });
            }
            this.addInfoWindow = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]addInfoWindow');
                if (!options.location || !options.location instanceof Array || options.location.length != 2) {
                    mapsLog("\tlocation is not defined");
                    return false;
                }
                mapsLog("\tlocation: " + options.location[0] + ", " + options.location[1]);
                return new sfMapsGWindow({
                    location: new google.maps.LatLng(options.location[0], options.location[1]),
                    map: this.objMap,
                    content: options.content || '',
                    fixedwidth: options.fixedwidth || 300
                });
            }
            function sfMapsGMarker(option){
                // Now initialize all properties.
                this.location = option.location;
                this.image = option.image;
                if (this.image && !this.image.anchorpos) {
                    this.image.anchorpos = [0, 0];
                }
                this.map = option.map;

                // We define a property to hold the image's
                // div. We'll actually create this div
                // upon receipt of the add() method so we'll
                // leave it null for now.
                this.div = null;

                // Explicitly call setMap() on this overlay
                this.setMap(this.map);
            }
            sfMapsGMarker.prototype = new google.maps.OverlayView();
            sfMapsGMarker.prototype.onAdd = function(){
                var div = document.createElement('DIV');
                div.style.border = "none";
                div.style.borderWidth = "0px";
                div.style.position = "absolute";

                var img = document.createElement("img");
                img.src = this.image.url;
                div.appendChild(img);
                this.div = div;

                var panes = this.getPanes();
                panes.overlayLayer.appendChild(div);
            }
            sfMapsGMarker.prototype.draw = function(){
                var overlayProjection = this.getProjection();
                mapsLog("\tsfMapsGMarker-pos : " + this.location.lat() + ", " + this.location.lng());
                var pos = overlayProjection.fromLatLngToDivPixel(this.location);
                mapsLog("\tsfMapsGMarker-projected pos : " + pos.x + ", " + pos.y);

                var div = this.div;
                div.style.left = (pos.x - this.image.anchorpos[0]) + 'px';
                div.style.top = (pos.y - this.image.anchorpos[1]) + 'px';
            }
            sfMapsGMarker.prototype.onRemove = function(){
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
            sfMapsGMarker.prototype.remove = function(){
                this.setMap(null);
            }

            function sfMapsGWindow(option){
                this.location = option.location;
                this.map = option.map;
                this.content = option.content || '';

                this.div = null;
                this.setMap(this.map);
				this.id = 'sf-ui-maps-infowin-' + (new Date()).getTime();
            }
            sfMapsGWindow.prototype = new google.maps.OverlayView();
            sfMapsGWindow.prototype.onAdd = function(){
                var div = document.createElement('DIV');
                div.style.border = "none";
                div.style.borderWidth = "0px";
                div.style.position = "absolute";
				div.id = this.id;

                var windowHTML = '';
                windowHTML += '<table border="0px" cellpadding="0px" cellspacing="0px" width="230px">';
                windowHTML += '<tr height="44px">';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_top_left.png) no-repeat;"></div></td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_top_center.png" width="300px"></td>';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_top_right.png) no-repeat;"></div></td>';
                windowHTML += '</tr>';
                windowHTML += '<tr height="20px">';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_middle_left.png" width="15px"> </td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_middle_center.png" width="200px"><div class="sf-ui-maps-infowin-content">' + this.content + '</div></td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_middle_right.png" width="15px"> </td>';
                windowHTML += '</tr>';
                windowHTML += '<tr height="44px">';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_bottom_left.png) no-repeat;"></div></td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_bottom_center.png" width="300px" align="center"><div id="sf-ui-maps-infowin-arrow" style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_bottom_arrow.png) no-repeat;"></div></td>';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_bottom_right.png) no-repeat;"></div></td>';
                windowHTML += '</tr>';
                windowHTML += '</table>';

                div.innerHTML = windowHTML;
                this.div = div;

                var panes = this.getPanes();
                panes.overlayLayer.appendChild(div);

                var content = div.getElementsByClassName('sf-ui-maps-infowin-content')[0];
                this.offsetx = $('#'+this.id).width()/2;
                this.offsety = $('#'+this.id).height()-12;
                mapsLog("\tOffset : " + this.offsetx + ", " + this.offsety);
            }
            sfMapsGWindow.prototype.draw = function(){
                var overlayProjection = this.getProjection();
                mapsLog("\tsfMapsGMarker-pos : " + this.location.lat() + ", " + this.location.lng());
                var pos = overlayProjection.fromLatLngToDivPixel(this.location);
                mapsLog("\tsfMapsGMarker-projected pos : " + pos.x + ", " + pos.y);

                var div = this.div;
                div.style.left = (pos.x - this.offsetx) + 'px';
                div.style.top = (pos.y - this.offsety) + 'px';
            }
            sfMapsGWindow.prototype.onRemove = function(){
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
            sfMapsGWindow.prototype.remove = function(){
                this.setMap(null);
            }

            this.search = function(options){
                mapsLog('[sfMapsGoogle(' + this.elmtID + ')]search');
                var geocoder = new google.maps.Geocoder();

                if ((!options.address && !options.location) || !options.callback) {
                    mapsLog("\t(address and location) or callback is not defined");
                    return false;
                }
				var geocodeOpt = {};
				if (options.address) {
					geocodeOpt.address = options.address;
				}
				else if(options.location) {
					geocodeOpt.latLng = new google.maps.LatLng(options.location[0], options.location[1]);
				}

                geocoder.geocode(geocodeOpt, function(results, status){
                    if (status == google.maps.GeocoderStatus.OK) {
						options.callback({
							location: [results[0].geometry.location.lat(), results[0].geometry.location.lng()],
							address: results[0].formatted_address
						});
                    }
                    else {
                        options.callback(null);
                    }
                });
            }
        }
    };

	// Daum Map API v3 (http://dna.daum.net/apis/maps)
    // - level이 google과는 반대로, 숫자가 커질수록 축소임. (google의 10이 다음의 10과 비슷하고 그 이상 이하는 반대로 증가/감소. Google과 맞추지는 않고, 숫자가 커지면 확대되는 방식으로만 맞춤.)
    oMapsClasses['daum'] = {
        checkAvailFunc: function(){
            return window.daum && window.daum.maps;
        },
        cls: function(elementid, options){
            mapsLog("[sfMapsDaum(" + elementid + ")]create");
            this.elmtID = elementid;
            this.objMap = null;
            this.objRoadView = null;
            this.bMoving = false;

            var oDaumMapType = {
                'roadmap': daum.maps.MapTypeId.ROADMAP,
                'skyview': daum.maps.MapTypeId.SKYVIEW,
                'hybrid': daum.maps.MapTypeId.HYBRID
            }
            var oSFMapType = {}
            for (var type in oDaumMapType) {
                oSFMapType[oDaumMapType[type]] = type + '';
            }
            this.size = {
                width: parseInt($('#' + elementid).width(), 10),
                height: parseInt($('#' + elementid).height(), 10)
            }

            function convLevelSF2Daum(zoom){
                return 11 - zoom;
            }
            function convLevelDaum2SF(zoom){
                return zoom - 11;
            }

            // Initialize the Map object
            var oDefaultOpts = {
                center: [37.258848, 127.054205],
                zoom: 0,
                maptype: 'roadmap'
            };
            options = $.extend(oDefaultOpts, options);
            var mapDiv = document.getElementById(this.elmtID);
            this.objMap = new daum.maps.Map(mapDiv, {
                center: new daum.maps.LatLng(options.center[0], options.center[1]),
                level: convLevelSF2Daum(options.zoom),
                mapTypeId: oDaumMapType[options.maptype]
            });

            // Method defines
            this.moveTo = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]moveTo');
                if (!options || !options.center)
                    return false;
                if (this.bMoving)
                    return false;

                this.objMap.panTo(new daum.maps.LatLng(options.center[0], options.center[1]));
                this.bMoving = true;
                var _THIS_ = this;
                setTimeout(function(){
                    _THIS_.bMoving = false;
                }, 500);
            }
            this.moveBy = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]moveBy');
                if (this.bMoving)
                    return false;
                if (!options || options.x == undefined || options.y == undefined)
                    return false;
                mapsLog("\tMove : " + options.x + ", " + options.y);
                var size = this.size;
                var bound = this.objMap.getBounds();
                mapsLog("\tbound : " + bound);
                var sw = bound.getSouthWest();
                var ne = bound.getNorthEast();
                var latPerPixel = (ne.getLat() - sw.getLat()) / size.height;
                mapsLog("\tlatPerPixel : " + latPerPixel);
                var lngPerPixel = (ne.getLng() - sw.getLng()) / size.width;
                mapsLog("\tlngPerPixel : " + lngPerPixel);
                var center = this.objMap.getCenter();
                mapsLog("\tcenter : " + center);

                var newCenter = new daum.maps.LatLng(center.getLat() + (-options.y) * latPerPixel, center.getLng() + options.x * lngPerPixel);
                mapsLog("\tnewCenter : " + newCenter);

                this.objMap.panTo(newCenter);
                this.bMoving = true;
                var _THIS_ = this;
                setTimeout(function(){
                    _THIS_.bMoving = false;
                }, 500);
            }
            this.zoomIn = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]zoomIn');
                this.objMap.setLevel(this.objMap.getLevel() - 1);
            }
            this.zoomOut = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]zoomOut');
                this.objMap.setLevel(this.objMap.getLevel() + 1);
            }
            this.setMapType = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]setMapType');
                if (options.type == undefined) {
                    mapsLog('\ttype option is not defined...');
                    return;
                }
                if (!oDaumMapType[options.type]) {
                    mapsLog('\tNo Such a maptype : ' + options.type);
                    return;
                }
                return this.objMap.setMapTypeId(oDaumMapType[options.type]);
            }
            this.getMapType = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]getMapType');
                return oSFMapType[this.objMap.getMapTypeId()];
            }
            this.setZoom = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]setZoom');
                if (!options.zoom) {
                    return false;
                }
                return this.objMap.setLevel(convLevelSF2Daum(options.zoom));
            }
            this.getZoom = function(options){
                var level = convLevelDaum2SF(this.objMap.getLevel());
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]getZoom returns ' + level);
                return level;
            }
            this.setCenter = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]setCenter');
                if (!options.center && !options.center instanceof Array && options.center.length != 2) {
                    return false;
                }
                return this.objMap.setCenter(new daum.maps.LatLng(options.center[0], options.center[1]));
            }
            this.getCenter = function(options){
                var latlng = this.objMap.getCenter();
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]getCenter returns ' + latlng.getLat() + ', ' + latlng.getLng());
                return new Array(latlng.getLat(), latlng.getLng());
            }
            /*
             this.openRoadView = function (options) {
             mapsLog('[sfMapsDaum]openRoadView ' + this.elmtID);
             if(!this.objRoadView && options.elementid) {
             var latlng = new daum.maps.LatLng(37.529196714213114, 126.92506196011036);
             var opt = {point:latlng,width:600,height:400,pan:10,tilt:-30,zoom:2};
             this.objRoadView = new daum.maps.RoadView(options.elementid, opt);
             }
             }
             */
            this.addMarker = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]addMarker');
                if (!options.image) {
                    options.image = $.extend(oDefaultMarker, {});
                }
                if (!options.location || !options.location instanceof Array || options.location.length != 2) {
                    mapsLog("\tlocation is not defined");
                    return false;
                }
                mapsLog("\tlocation: " + options.location[0] + ", " + options.location[1]);

                var marker = new sfMapsDMarker({
                    location: new daum.maps.LatLng(options.location[0], options.location[1]),
                    map: this.objMap,
                    image: options.image
                });

                return marker;
            }
            this.addInfoWindow = function(options){
                mapsLog('[sfMapsDaum(' + this.elmtID + ')]addInfoWindow');
                if (!options.location || !options.location instanceof Array || options.location.length != 2) {
                    mapsLog("\tlocation is not defined");
                    return false;
                }
                mapsLog("\tlocation: " + options.location[0] + ", " + options.location[1]);

                return new sfMapsDWindow({
                    location: new daum.maps.LatLng(options.location[0], options.location[1]),
                    map: this.objMap,
                    content: options.content || '',
                    fixedwidth: options.fixedwidth || 300
                });
            }
            function sfMapsDMarker(option){
                this.location = option.location;
                this.image = option.image;
                if (this.image && !this.image.anchorpos) {
                    this.image.anchorpos = [0, 0];
                }
                this.map = option.map;
                this.div = null;
                this.setMap(this.map);
            }
            sfMapsDMarker.prototype = new daum.maps.AbstractOverlay();
            sfMapsDMarker.prototype.onAdd = function(){
                var div = document.createElement('DIV');
                div.style.border = "none";
                div.style.borderWidth = "0px";
                div.style.position = "absolute";

                var img = document.createElement("img");
                img.src = this.image.url;
                div.appendChild(img);
                this.div = div;

                var panes = this.getPanels();
                panes.overlayLayer.appendChild(div);
            }
            sfMapsDMarker.prototype.draw = function(){
                var overlayProjection = this.getProjection();

                mapsLog("\tsfMapsDMarker-pos : " + this.location.getLat() + ", " + this.location.getLng());
                var pos = overlayProjection.pointFromCoords(this.location);
                mapsLog("\tsfMapsDMarker-projected pos : " + pos.x + ", " + pos.y);

                // Resize the image's DIV to fit the indicated dimensions.
                var div = this.div;
                div.style.left = (pos.x - this.image.anchorpos[0]) + 'px';
                div.style.top = (pos.y - this.image.anchorpos[1]) + 'px';
            }
            sfMapsDMarker.prototype.onRemove = function(){
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
            sfMapsDMarker.prototype.remove = function(){
                this.setMap(null);
            }

            function sfMapsDWindow(option){
                this.location = option.location;
                this.map = option.map;
                this.content = option.content || '';

                this.div = null;
                this.setMap(this.map);
				this.id = 'sf-ui-maps-infowin-' + (new Date()).getTime();
            }
            sfMapsDWindow.prototype = new daum.maps.AbstractOverlay();
            sfMapsDWindow.prototype.onAdd = function(){
                var div = document.createElement('DIV');
                div.style.border = "none";
                div.style.borderWidth = "0px";
                div.style.position = "absolute";
				div.id = this.id;

                var windowHTML = '';
                windowHTML += '<table border="0px" cellpadding="0px" cellspacing="0px" width="230px">';
                windowHTML += '<tr height="44px">';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_top_left.png) no-repeat;"></div></td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_top_center.png" width="300px"></td>';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_top_right.png) no-repeat;"></div></td>';
                windowHTML += '</tr>';
                windowHTML += '<tr height="20px">';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_middle_left.png" width="15px"> </td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_middle_center.png" width="200px"><div class="sf-ui-maps-infowin-content">' + this.content + '</div></td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_middle_right.png" width="15px"> </td>';
                windowHTML += '</tr>';
                windowHTML += '<tr height="44px">';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_bottom_left.png) no-repeat;"></div></td>';
                windowHTML += '<td background="' + sImageSrcDir + '/tooltip_bottom_center.png" width="300px" align="center"><div id="sf-ui-maps-infowin-arrow" style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_bottom_arrow.png) no-repeat;"></div></td>';
                windowHTML += '<td width="44px"><div style="width:44px;height:44px;background: url(' + sImageSrcDir + '/tooltip_bottom_right.png) no-repeat;"></div></td>';
                windowHTML += '</tr>';
                windowHTML += '</table>';

                div.innerHTML = windowHTML;
                this.div = div;

                var panes = this.getPanels();
                panes.overlayLayer.appendChild(div);

                var content = div.getElementsByClassName('sf-ui-maps-infowin-content')[0];
                this.offsetx = $('#'+this.id).width()/2;
                this.offsety = $('#'+this.id).height()-12;
                mapsLog("\tOffset : " + this.offsetx + ", " + this.offsety);
            }
            sfMapsDWindow.prototype.draw = function(){
                var overlayProjection = this.getProjection();

                mapsLog("\tsfMapsDMarker-pos : " + this.location.getLat() + ", " + this.location.getLng());
                var pos = overlayProjection.pointFromCoords(this.location);
                mapsLog("\tsfMapsDMarker-projected pos : " + pos.x + ", " + pos.y);
                mapsLog("\tOffset : " + this.offsetx + ", " + this.offsety);

                // Resize the image's DIV to fit the indicated dimensions.
                var div = this.div;
                div.style.left = (pos.x - this.offsetx) + 'px';
                div.style.top = (pos.y - this.offsety) + 'px';
            }
            sfMapsDWindow.prototype.onRemove = function(){
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
            sfMapsDWindow.prototype.remove = function(){
                this.setMap(null);
            }

			this.search = function(options){
				mapsLog('[sfMapsDaum(' + this.elmtID + ')]search is not supported in daum map API');
				if(options.callback) {
					setTimeout(function () {
						options.callback(null);
					}, 0);
				}
			}
        }
    };

	var oCountryLocations = {
        "default": [16.29905, 149.41406, 2], // default world
        "AD": [42.53486, 1.60057, 11], // Andorra
        "AE": [23.90592, 53.86596, 7], // UAE
        "AL": [41.15333, 20.16833, 7], // Albania
        "AM": [40.06909, 45.03818, 7], // Armenia
        "AR": [-38.41609, -63.61667, 4], // Argentina
        "AT": [47.51623, 14.55007, 7], // Austria
        "AU": [-25.27439, 133.77513, 4], // Australia
        "AZ": [40.14310, 47.57692, 7], // Azerbaijan
        "BA": [43.91588, 17.67907, 7], // Bosnia and Herzegovina
        "BB": [13.19388, -59.54319, 11], // Barbados
        "BE": [50.50388, 4.46993, 7], // Belgium
        "BG": [42.73388, 25.48583, 7], // Bulgaria
        "BH": [26.04691, 50.56045, 10], // BAHRAIN
        "BO": [-16.29015, -63.58865, 5], // Bolivia
        "BR": [-14.23500, -51.92528, 4], // Brazil
        "BY": [53.70980, 27.95338, 6], // Belarus
        "CA": [56.13036, -106.34677, 4], // Canada
        "CH": [46.81818, 8.22751, 8], // Switzerland
        "CI": [7.53998, -5.54708, 7], // IVORY COAST
        "CL": [-35.67514, -71.54296, 4], // Chile
        "CN": [35.86166, 104.19539, 4], // China
        "CO": [4.57086, -74.29733, 5], // Colombia
        "CR": [9.74891, -83.75342, 7], // Costa Rica
        "CY": [35.12641, 33.42985, 9], // Cyprus
        "CZ": [49.81749, 15.47296, 7], // Czech Republic
        "DE": [51.16569, 10.45152, 6], // Germany
        "DK": [56.26392, 9.50178, 7], // Denmark
        "DM": [15.41499, -61.37097, 10], // Dominica
        "DZ": [28.03388, 1.65962, 5], // Algeria
        "EC": [-1.83123, -78.18340, 6], // Ecuador
        "EE": [58.59527, 25.01360, 7], // Estonia
        "EG": [26.82055, 30.80249, 6], // Egypt
        "ES": [40.46366, -3.74922, 6], // Spain
        "FI": [64.71787, 25.22460, 5], // Finland
        "FR": [46.22763, 2.21374, 6], // France
        "GB": [53.72271, -3.51562, 6], // United Kingdom
        "GD": [12.11049, -61.65596, 11], // Grenada
        "GE": [32.15743, -82.90712, 6], // Georgia
        "GF": [3.93388, -53.12578, 7], // French Guiana
        "GR": [39.07420, 21.82431, 6], // Greece
        "GT": [15.78347, -90.23075, 7], // Guatemala
        "GY": [4.86041, -58.93018, 6], // Guyana
        "HK": [22.39642, 114.10949, 10], // Hong Kong
        "HN": [15.19999, -86.24190, 7], // Honduras
        "HR": [45.1, 15.2, 7], // Croatia
        "HU": [47.16249, 19.50330, 7], // Hungary
        "ID": [-0.78927, 113.92132, 5], // Indonesia
        "IE": [53.41291, -8.24389, 6], // Ireland
        "IL": [31.04605, 34.85161, 6], // ISRAEL
        "IN": [20.59368, 78.96288, 5], // India
        "IS": [64.96305, -19.02083, 6], // Iceland
        "IT": [41.87194, 12.56738, 6], // Italy
        "JM": [18.11191, -77.30804, 9], // Jamaica
        "JO": [31.05293, 36.51855, 7], // JORDAN
        "KE": [-0.02355, 37.90619, 6], // KENYA
        "KG": [41.20438, 74.76609, 7], // Kyrgyzstan
        "KR": [35.90775, 127.76692, 6], // South Korea
        "KW": [29.31166, 47.48176, 8], // KUWAIT
        "KZ": [48.01957, 66.92368, 5], // Kazakhstan
        "LB": [33.85472, 35.86228, 8], // LEBANON
        "LT": [55.16943, 23.88127, 7], // Lithuania
        "LU": [49.81527, 6.12958, 9], // Luxembourg
        "LV": [56.87963, 24.60318, 7], // Latvia
        "LY": [26.3351, 17.22833, 5], // LIBYA
        "MA": [31.79170, -7.09262, 6], // MOROCCO
        "MD": [46.91275, 28.36669, 7], // Moldova
        "ME": [42.70867, 19.37439, 8], // Montenegro
        "MK": [41.60863, 21.74527, 8], // Macedonia
        "MX": [23.63450, -102.55278, 5], // Mexico
        "MY": [4.21048, 101.97576, 5], // Malaysia
        "NG": [9.08199, 8.67527, 6], // NIGERIA
        "NI": [12.86541, -85.20722, 7], // Nicaragua
        "NL": [52.13263, 5.29126, 7], // Netherlands
        "NO": [63.43086, 8.87695, 5], // Norway
        "NZ": [-40.90055, 174.88597, 5], // New Zealand
        "PA": [8.53798, -80.78212, 7], // Panama
        "PE": [-9.18996, -75.01515, 5], // Peru
        "PH": [12.87972, 121.77401, 5], // Philippines
        "PK": [30.37532, 69.34511, 5], // PAKISTAN
        "PL": [51.91943, 19.14513, 6], // Poland
        "PR": [18.22083, -66.59014, 7], // Puerto Rico
        "PT": [39.39987, -8.22445, 6], // Portugal
        "PY": [-23.44250, -58.44383, 6], // Paraguay
        "QA": [25.35482, 51.18388, 8], // QATAR
        "RO": [45.94316, 24.96676, 6], // Romania
        "RS": [44.01652, 21.00585, 7], // Serbia
        "RU": [61.52401, 105.31875, 3], // Russia
        "SA": [23.88594, 45.07916, 5], // Saudi Arabia
        "SE": [61.33353, 16.30371, 5], // Sweden
        "SG": [1.35208, 103.81983, 11], // Singapore
        "SI": [46.15124, 14.99546, 8], // Slovenia
        "SK": [48.66902, 19.69902, 7], // Slovakia
        "SN": [14.49740, -14.45236, 7], // SENEGAL
        "SR": [3.91930, -56.02778, 7], // Surinam
        "SV": [13.79418, -88.89653, 9], // El Salvador
        "TH": [12.46876, 100.85449, 5], // Thailand
        "TJ": [38.35027, 71.26831, 7], // Tajikistan
        "TM": [38.96971, 59.55627, 6], // Turkmenistan
        "TN": [33.88691, 9.53749, 6], // TUNISIA
        "TR": [38.96374, 35.24332, 6], // TURKEY
        "TT": [10.65790, -61.20483, 9], // TrinidadTobago
        "TW": [23.69781, 120.96051, 7], // Taiwan
        "UA": [48.37943, 31.16558, 6], // Ukraine
        "US": [37.09024, -95.71289, 4], // USA
        "UY": [-32.52277, -55.76583, 7], // Uruguay
        "UZ": [41.37749, 64.58526, 6], // Uzbekistan
        "VE": [6.42375, -66.58973, 6], // Venezuela
        "VN": [14.05832, 108.27719, 5], // Vietnam
        "ZA": [-30.55948, 22.93750, 5]
    }
    function getProjectedScale(w, h, scale){ // 지도의 가로, 세로, 960x540기준의 scale
        mapsLog("getProjectedScale(" + w + ", " + h + ", " + scale + ")");
        //return Math.floor(scale*Math.sqrt(w*w+h*h)/Math.sqrt(960*960+540*540));       // 수정필요
        var ratio = Math.sqrt(w * w + h * h) / Math.sqrt(960 * 960 + 540 * 540);
        if (ratio - 1 < 0.2 && ratio - 1 > -0.2) {
            mapsLog("\tReturn as the scale is.");
            return scale;
        }
        if (ratio - 1 >= 0.2) {
            mapsLog("\tReturn increased scale");
            return scale + Math.floor(ratio);
        }
        else {
            mapsLog("\tReturn decreased scale");
            return Math.floor(scale - (Math.log(ratio) / Math.log(2)));
        }
    }
    for (var country in oCountryLocations) {
        oCountryLocations[country][2]
    }


    /*
     Class: sf.ui.Maps (jQuery.sfMaps)
    	Apps Framework Class for Maps UI
     */
    /*
     * Function: jQuery.sfMaps
     * 	Adds maps to element.
     *
     * Parameter:
	 * 	o (Object) options - options for maps
	 * 		api - (String) Maps API. 'google' or 'daum'. Daum map service is only for Korea. (Default Value: 'google')
	 * 		center - (Array<Number>) Center of the map. First array item should be Latitude and Second item should be Longitude. (Default Value: current country in the Smart Hub settings.)
	 * 		zoom - (Number) Initial zoom level for maps. Higher number shows detailed(zoomed) map. (Default Value: 0)
	 *
	 * Example:
	 * 	Initialize with Google maps API.
	 * 	(start code)
	 * 	$("#maps").sfMaps({
	 * 		api: 'google',
	 * 		center: [37, 127],	// South Korea
	 * 		zoom: 9
	 * 	});
	 * 	(end)
	 */
    sf.ui.Maps = sf.ui.widgetFactory({

        widgetName: widgetName,

        baseCssClass: 'sf-ui-maps',

		mapObject: null,

        options: {
            api: 'google',
            center: [0, 0],
            zoom: 0,
            maptype: 'roadmap'
        },

        _create: function(){
            alert("[sf.service.Maps] _create()");
            this._super();
        },

        _init: function(){
            alert("[sf.service.Maps] _init()");
            this._super();

			if (navigator.userAgent.toLowerCase().indexOf('applewebkit') < 0) {
				alert("Not Supported browser!! Only available in webkit browser.");
				this.widget().html("Not supported in this browser.");
				return;
			}

			var options = this.options;
			sImageSrcDir = sf.core._afPath.images + '/ui.maps';
			oDefaultMarker.url = sImageSrcDir + '/sfmaps_marker.png';

			if (options && options.api) {
                mapsLog("Requested API : " + options.api);
                options.api = options.api.toLowerCase();
                if (oMapsClasses[options.api] &&
						// check api available
	                	oMapsClasses[options.api].checkAvailFunc &&
	                	typeof oMapsClasses[options.api].checkAvailFunc == 'function' &&
	                	oMapsClasses[options.api].checkAvailFunc() &&

						// check map class available
	               		oMapsClasses[options.api].cls &&
	                	typeof oMapsClasses[options.api].cls == 'function') {

					if(!this.widget().attr('id')) {
						this.widget().attr('id', 'maps'(new Date()).getTime());
					}
                    this.mapObject = new oMapsClasses[options.api].cls(this.widget().attr('id'), options);
					alert("MAP OBJECT: " +this.mapObject);
                }
                else {
                    mapsLog("Can not find suitable maps class!");
                }
            }
            else {
                mapsLog("API is not defined");
            }
        },

        _destroy: function(){
            alert("[sf.service.Maps] _destroy()");
        }, // _destroy()

		/*
		 * Function: moveBy
		 * 	Move by directions.
		 *
		 * Parameters:
		 * 	o (Object) - indicates directions of panning.
		 * 		x		- (Number) x-axis(Horizontal) movement(Pixel). Positive number indicates right direction of the map.
		 * 		y		- (Number) y-axis(Vertical) movement(Pixel). Positive number indicates down direction of the map.
		 * 		animate	- (Boolean) animate the movement or not.
		 *
		 * Returns:
		 * 	Nothing
		 *
		 * Example:
		 * 	Move the map to left by 200 pixel with animate.
		 * 	(start code)
		 * 	$("#maps").sfMaps('moveBy', {
		 * 		x: -200,
		 * 		y: 0,
		 * 		animate: true
		 * 	});
		 * 	(end)
		 */
		moveBy: function (option) {
			alert("[sf.service.Maps] moveBy("+option+")");
			if(this.mapObject && this.mapObject.moveBy) {
				this.mapObject.moveBy(option);
			}
			else {
				alert("\tThere's no moveBy function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: zoomIn
		 * 	Zoom in the map.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	Nothing
		 *
		 */
		zoomIn: function () {
			alert("[sf.service.Maps] zoomIn()");
			if(this.mapObject && this.mapObject.zoomIn) {
				this.mapObject.zoomIn();
			}
			else {
				alert("\tThere's no zoomIn function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: zoomOut
		 * 	Zoom out the map.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	Nothing
		 *
		 */
		zoomOut: function () {
			alert("[sf.service.Maps] zoomOut()");
			if(this.mapObject && this.mapObject.zoomOut) {
				this.mapObject.zoomOut();
			}
			else {
				alert("\tThere's no zoomOut function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: moveTo
		 * 	Move Center of the map with animation.
		 *
		 * Parameters:
		 * 	o (Object) - indicates coodination of center to move.
		 * 		center	- (Array<Number>) center of the map to move. [Latitude, Longitude]
		 *
		 * Returns:
		 * 	Nothing
		 *
		 * See Also:
		 * 	sfMaps('setCenter') - 'setCenter' move center without animation.
		 *
		 * Example:
		 * 	Move the map to Samsung Electronics HQ.
		 * 	(start code)
		 * 	$("#maps").sfMaps('moveTo', {
		 *		center: [37.259, 127.054]	// Coordinate of Samsung HQ
		 *	});
		 *	(end)
		 */
		moveTo: function (option) {
			alert("[sf.service.Maps] moveTo("+option+")");
			if(this.mapObject && this.mapObject.moveTo) {
				this.mapObject.moveTo(option);
			}
			else {
				alert("\tThere's no moveTo function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: setMapType
		 * 	Set type of the map.
		 *
		 * Parameters:
		 * 	o (Object) - indicates type of the map.
		 * 		type	- (String) map type.
		 *
		 *  Map type of Google Maps API : 'roadmap', 'hybrid', 'satellite', 'terrain'.
		 *  Map type of Daum Maps API : 'roadmap', 'skyview', 'hybrid'.
		 *
		 * Returns:
		 * 	Nothing
		 *
		 * Example:
		 * 	Set current map to 'hybrid' map type.
		 * 	(start code)
		 * 	$("#maps").sfMaps('setMapType', {type: 'hybrid'});
		 * 	(end)
		 */
		setMapType: function (option) {
			alert("[sf.service.Maps] setMapType("+option+")");
			if(this.mapObject && this.mapObject.setMapType) {
				this.mapObject.setMapType(option);
			}
			else {
				alert("\tThere's no setMapType function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: getMapType
		 * 	Get current map's type.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(String) map type.
		 * 	Map type of Google Maps API : 'roadmap', 'hybrid', 'satellite', 'terrain'.
		 *  Map type of Daum Maps API : 'roadmap', 'skyview', 'hybrid'.
		 *
		 */
		getMapType: function (option) {
			alert("[sf.service.Maps] getMapType("+option+")");
			if(this.mapObject && this.mapObject.getMapType) {
				return this.mapObject.getMapType(option);
			}
			else {
				alert("\tThere's no getMapType function in this map class: " + this.options.api);
			}
		},

		/*
		 * Function: setCenter
		 * 	Set center of the map without animation.
		 *
		 * Parameters:
		 * 	o (Object) - indicates center of the map to move.
		 * 		center	- (Array<Number>) center of the map to move. [Latitude, Longitude]
		 *
		 * Returns:
		 * 	Nothing
		 *
		 * See Also:
		 * 	sfMaps('moveTo') - 'moveTo' moves center with animation.
		 *
		 * Example:
		 * 	Move the map to Samsung Electronics HQ.
		 * 	(start code)
		 * 	$("#maps").sfMaps('setCenter', {
		 *		center: [37.259, 127.054]	// Coordinate of Samsung HQ
		 *	});
		 *	(end)
		 */
		setCenter: function (option) {
			alert("[sf.service.Maps] setCenter("+option+")");
			if(this.mapObject && this.mapObject.setCenter) {
				this.mapObject.setCenter(option);
			}
			else {
				alert("\tThere's no setCenter function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: getCenter
		 * 	Get current center coordinate of the map.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Array<Number>) [Latitude, Longitude]
		 *
		 * Example:
		 * 	Shows current zoom level with alert.
		 * 	(start code)
		 * 	var center = $("#maps").sfMaps('getCenter');
		 * 	alert("Center(Latitude,Longitude) : " + center[0] + ", " + center[1])
		 * 	(end)
		 */
		getCenter: function (option) {
			alert("[sf.service.Maps] getCenter("+option+")");
			if(this.mapObject && this.mapObject.getCenter) {
				return this.mapObject.getCenter(option);
			}
			else {
				alert("\tThere's no getCenter function in this map class: " + this.options.api);
			}
		},

		/*
		 * Function: setZoom
		 * 	Set zoom level of the map. Higher number of level indicates detailed(zoomed) level.
		 *
		 * Parameters:
		 * 	o (Object) - indicates directions of panning.
		 * 		zoom - zoom level of the map.
		 *
		 * Returns:
		 * 	Nothing
		 *
		 * Example:
		 * 	Set zoom level to 5.
		 * 	(start code)
		 * 	$("#maps").sfMaps('setZoom', {zoom: 5});
		 * 	(end)
		 */
		setZoom: function (option) {
			alert("[sf.service.Maps] setZoom("+option+")");
			if(this.mapObject && this.mapObject.setZoom) {
				this.mapObject.setZoom(option);
			}
			else {
				alert("\tThere's no setZoom function in this map class: " + this.options.api);
			}

			try {
				executeImageGC();
			}
			catch (e) {
				alert("EXCEPTION: executeImageGC is not supported in this platform!!!");
			}
		},

		/*
		 * Function: getZoom
		 * 	Get current zoom level of the map.
		 *
		 * Parameters:
		 * 	N/A
		 *
		 * Returns:
		 * 	(Number) current zoom level of the map.
		 */
		getZoom: function (option) {
			alert("[sf.service.Maps] getZoom("+option+")");
			if(this.mapObject && this.mapObject.getZoom) {
				return this.mapObject.getZoom(option);
			}
			else {
				alert("\tThere's no getZoom function in this map class: " + this.options.api);
			}
		},

		/*
		 * Function: addMarker
		 * 	Add marker to specified point.
		 *
		 * Parameters:
		 * 	o (Object) - marker options
		 * 		location	- (Array<Number>) Point of the map to mark. [Latitude, Longitude]
		 * 		image		- (Object) (Optional) {url:<String>, anchorpos:[<Number>, <Number>]}
		 *
		 * Returns:
		 * 	(Object) marker object. this object support "remove()" method to remove the marker.
		 *
		 * Example:
		 * 	Add marker to center of the map and remove it.
		 * 	(start code)
		 * 	// add marker to center of the map.
		 * 	var marker = $("#maps").sfMaps('addMarker', {
		 * 		location: $("#maps").sfMaps('getCenter')
		 * 	});
		 *
		 * 	// Now... remove the marker just added.
		 * 	marker.remove();
		 * 	(end)
		 */
		addMarker: function (option) {
			alert("[sf.service.Maps] addMarker("+option+")");
			if(this.mapObject && this.mapObject.addMarker) {
				return this.mapObject.addMarker(option);
			}
			else {
				alert("\tThere's no addMarker function in this map class: " + this.options.api);
			}
		},

		/*
		 * Function: addInfoWindow
		 * 	Add InfoWindow to specified point.
		 *
		 * Parameters:
		 * 	o (Object) - info window options
		 * 		location	- (Array<Number>) Point of the map to show infowindow. [Latitude, Longitude]
		 * 		content		- (String) content of infowindow.
		 *
		 * Returns:
		 * 	(Object) infowindow object. this object support "remove()" method to remove the infowindow.
		 *
		 * Example:
		 * 	Add infowindow to center of the map and remove it.
		 * 	(start code)
		 * 	// Add infowindow to center of the map.
		 * 	var infowin = $("#maps").sfMaps('addInfoWindow', {
		 * 		location: $("#maps").sfMaps('getCenter'),
		 * 		content: '<div style="text-align:center;">TEST</div>'
		 * 	});
		 *
		 * 	...
		 * 	// Remove the infowindow
		 * 	infowin.remove();
		 * 	(end)
		 */
		addInfoWindow: function (option) {
			alert("[sf.service.Maps] addInfoWindow("+option+")");
			if(this.mapObject && this.mapObject.addInfoWindow) {
				return this.mapObject.addInfoWindow(option);
			}
			else {
				alert("\tThere's no addInfoWindow function in this map class: " + this.options.api);
			}
		},

		/*
		 * Function: search
		 * 	Geocode the request
		 *
		 * Parameters:
		 * 	o (Object) - Geocode options.
		 * 		address		- (String) (Optional) Query string to search.
		 * 		location	- (Array<Number>) (Optional) Postion about which to search.
		 * 		callback	- (Function(Object)) Callback function to be called when searching is completed.
		 *
		 * Returns:
		 * 	nothing
		 *
		 * Description:
		 * 	Here's JSON of the returned object
		 * 	(start code)
		 * 	{
		 * 		"location": [37.259, 120.053],	// Searched location. [latitude, longitude]
		 * 		"address":	"Sydney, NSW"		// Address of searched location
		 * 	}
		 * 	(end)
		 *
		 * Example 1:
		 * 	Search with address and move to the location.
		 * 	(start code)
		 * 	$("#maps").sfMaps('search', {
		 * 		address: 'Seoul, Korea',
		 * 		callback: function (result) {
		 * 			alert("Search Callback");
		 * 			if (result) {
		 * 				$("#maps").sfMaps('setCenter', {
		 * 					center: result.location
		 * 				});
		 * 				alert("Search result: "+ result.address + "<br>"+result.location);
		 * 			}
		 * 			else {
		 * 				alert("Failed to search");
		 * 			}
		 * 		}
		 * 	});
		 * 	(end)
		 *
		 * Example 2:
		 * 	Search current center and print it with alert.
		 * 	(start code)
		 * 	$("#maps").sfMaps('search', {
		 * 		location: $("#maps").sfMaps('getCenter'),
		 * 		callback: function (result) {
		 * 			alert("Search Callback");
		 * 			if (result) {
		 * 				alert("Search result: "+ result.address + "<br>"+result.location);
		 * 			}
		 * 			else {
		 * 				alert("Failed to search");
		 * 			}
		 * 		}
		 * 	});
		 * 	(end)
		 */
		search: function (option) {
			alert("[sf.service.Maps] search("+option+")");
			if(this.mapObject && this.mapObject.search) {
				this.mapObject.search(option);
			}
			else {
				alert("\tThere's no getZoom function in this map class: " + this.options.api);
			}
			return null;
		}

    }); // sf.ui.Maps()
    sf.ui.bridge(widgetName, sf.ui.Maps);

    sf.ui.addSelector(widgetName);

}(jQuery));
/*
 * Title: UI Components (ToggleButton)
 * 	Apps Framework ToggleButton UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfToggleButton";

	/*
		Class: sf.ui.ToggleButton (jQuery.sfToggleButton)
		Apps Framework Class for ToggleButton UI

		Function: jQuery.sfToggleButton
			Generates ToggleButton.

		Rendered HTML:
			If the "text" option is String
			(start code)
			Before init: <div id="ToggleButton"></div>
			After  init: <div id="ToggleButton" class="sf-ui-ToggleButton sf-ui-ToggleButton-blur" >
				              <div class="sf-ui-ToggleButton-l" ></div>
				              <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text</div>
				              <div class="sf-ui-ToggleButton-r"></div>
				         </div>
			(end)

			Or if the "text" option is Array of String
			(start code)
			Before init:	<div id="ToggleButtons"></div>
			After  init:	<div id="ToggleButtons" class="sf-ui-ToggleButton sf-ui-ToggleButton-blur">
								<div class="sf-ui-ToggleButton-l" ></div>
								<div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text 1</div>
								<div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text 2</div>
								<div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text 3</div>
								<div class="sf-ui-ToggleButton-r"></div>
							</div>
			(end)

		Example 1
			ToggleButton setOption() method usage example (change ToggleButtons text in ToggleButton set):</u>
			(start code)
			$('#ToggleButtons').sfToggleButton({,
			 	text: [ "ToggleButton 1", "ToggleButton 2", "ToggleButton 3" ]
			});
			$('#ToggleButtons').sfToggleButton("option", "text", ["new text 1", "new text 2", "new text 3"]);
			(end)

			*Rendered HTMLS*
			(start code)
			Before: <div id="ToggleButton" class="sf-ui-ToggleButton sf-ui-ToggleButton-blur">
				         <div class="sf-ui-ToggleButton-l"></div>
				         <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text 1</div>
				         <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text 2</div>
				         <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">text 3</div>
				         <div class="sf-ui-ToggleButton-r"></div>
				     </div>

			After:  <div id="ToggleButton" class="sf-ui-ToggleButton sf-ui-ToggleButton-blur">
				         <div class="sf-ui-ToggleButton-l"></div>
				         <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">new text 1</div>
				         <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">new text 2</div>
				         <div class="sf-ui-ToggleButton-c sf-ui-common-ellipsis">new text 3</div>
				         <div class="sf-ui-ToggleButton-r"></div>
				     </div>
			(end)
	*/
	sf.ui.ToggleButton = sf.ui.widgetFactory({
		/** @lends sf.ui.ToggleButton.prototype */

		/**
		 * Widget name
		 * @default 'sfToggleButton'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Event namespace added by default to every event triggered by ToggleButton.
		 * @default "sfToggleButton"
		 * @type String
		 */
		eventNamespace: "sfToggleButton",

		baseCssClass: 'sf-ui-togglebutton',

		options: {
			text: {
				on: 'On',
				off: 'Off'
			},
			isOn: false,
			state: 'blur',
			widgetFocusCssClass: 'focused'
		},

		templates: {
			item: $.template(null,"<div class='bg'><div class='left'></div><div class='center'></div><div class='right'></div></div>"
								+"<div class='light'></div>"
								+"<div class='button'></div>"
			)
		},

		_init: function() {
			alert("[sf.ToggleButton] _init()");
			var o = this.options,
				w = this.widget();

			this.widget().css('visibility', 'hidden');	// 스타일 수정과정을 보이지않게

			this._setLayout();

			this._setOn(o.isOn, false);

			this.widget().css('visibility', 'visible');
		},


		/**
		 * @private
		 */
		_setLayout: function () {
			alert("[sf.ui.ToggleButton] _setLayout()");
			var w = this.widget(),
				t = this.templates,
				o = this.options;

			w.empty();
			$.tmpl(t.item, {}).appendTo(w);

			var width = w.width();
			var widthLeft = w.find('.bg .left').width();
			var widthRight = w.find('.bg .right').width();
			//alert("Button width : " + width);
			//alert("BG left width : " + widthLeft);
			//alert("BG right width : " + widthRight);
			w.find('.bg .center').width(width-widthLeft-widthRight);
			//alert("Set BG center width : " + w.find('.bg .center').width());

			w.find('.button').sfButton({text: o.isOn?o.text.on:o.text.off});

			width = w.find('.button').width();
			widthRight = w.find('.button .sf-ui-button-r').width();

			w.find('.light').css('left', (width - widthRight)+'px');
			//alert("Set light left : " + w.find('.light').css('left'));

			alert("ToggleButton HTML : " + w.html());
		},

		_setOn: function (bIsOn, animate) {
			alert("[sf.ui.ToggleButton] _setOn("+bIsOn+")");
			var w = this.widget(),
				t = this.templates,
				o = this.options;
			if(animate == undefined) {
				animate = true;
			}
			o.isOn = bIsOn;
			var offleft = w.width()-w.find('.button').width();
			//alert("offleft : " + offleft);
			w.find('.button').sfButton('option', 'text', o.isOn?o.text.on:o.text.off);

			if (!o.isOn) {
				w.find('.light').css('display', 'none');
			}
			w.find('.button').animate({
				'left': o.isOn ? '0px' : (offleft + 'px')
			}, {
				duration: (animate?200:0),
				complete: function () {
					w.find('.light').css('display', o.isOn?'block':'none');
				}
			});
		},
		_setOption: function(name, value) {
			alert("[sf.ui.ToggleButton] _setOption(" + name + ", " + value + ")");
			var o = this.options,
				v = this.view;

			switch (name) {
				case 'text':
					break;
			} // switch(name)
		}, // _setOption()

		_destroy: function() {
			alert("[sf.ui.ToggleButton] _destroy()");
			var w = this.widget(),
				o = this.options;

			w.removeClass(o.widgetFocusCssClass);

			w.empty();
		}, // _destroy()

		toggle: function () {
			alert("[sf.ui.ToggleButton] toggle()");
			var o = this.options;
			this._setOn(!o.isOn);
		},

		/*
		Function: focus
		 Sets focus on particular ToggleButton

		Parameters:
		 o (Number) index - n-th item to be focused (default:0)

		Returns:
		 object - jQuery

		Description:
		 Adds 'focus' CSS class to choosed ToggleButton.
		 Removes CSS class provided in widgetBlurCssClass and adds CSS class provided widgetFocusCssClass.
		 Removes 'focus' CSS class from any focused ToggleButton and sets focus on choosed ToggleButton.
		 It also sets 'focus CSS class on elements containing rounded corners if focused ToggleButton is
		 side ToggleButton in whole ToggleButton set.

		Example 1:
		 This example set focus on particular ToggleButton
		 (start code)
		  $('#ToggleButtons').sfToggleButton({,
			text : [ "ToggleButton 1", "ToggleButton 2", "ToggleButton 3" ]
		  });
		  $('#ToggleButton1').sfToggleButton('focus');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		focus: function() {
			alert("[sf.adjustToggleButtonSpacing] focus()");
			var w = this.widget(),
				v = this.view,
				o = this.options;

			w.addClass(o.widgetFocusCssClass);
			w.find('.button').sfButton('focus');

			return this;
		}, // focus()


		/*
		Function: blur
		 Removes focus state from component and ads blur state

		Parameters:
		 N/A

		Returns:
		 object - jQuery

		Description:
		 Removes CSS class provided in *widgetFocusCssClass* and adds class provided in *widgetBlurCssClass*

		Example 1:
		 This example blur focus on particular ToggleButton
		 (start code)
		  $('#ToggleButtons').sfToggleButton({,
			text : [ "ToggleButton 1", "ToggleButton 2", "ToggleButton 3" ]
		  });
		  $('#ToggleButton1').sfToggleButton('blur');
		 (end)

		Available From:
		 1.1.0

		History:
		*/
		blur: function() {
			alert("[sf.adjustToggleButtonSpacing] blur()");
			var o = this.options,
				w = this.widget();

			w.removeClass(o.widgetFocusCssClass);
			w.find('.button').sfButton('blur');

			return this;
		} // blur()

	}); // sf.ui.ToggleButton()

	sf.ui.bridge(widgetName, sf.ui.ToggleButton);

    sf.ui.addSelector(widgetName);

}(jQuery));/*
 * Title: UI Components (TextInput)
 * Apps Framework TextInput UI Components
 *
 * Version added:
 * 1.1.1
 */
(function($){

    var widgetName = "sfTextInput";
    /*
     Class: sf.ui.TextInput (jQuery.sfTextInput)
    	Apps Framework Class for Text Input
     */
    /*
     * Function: jQuery.sfTextInput
     * 	Adds Text Input Box to element.
     *
     * Parameter:
	 * 	o (Object) options - options for textinput
	 * 		maxlength
	 *
	 * Example:
	 * 	(start code)
	 * 	(end)
	 */
    sf.ui.TextInput = sf.ui.widgetFactory({

        widgetName: widgetName,

        baseCssClass: 'sf-ui-textinput',

		mapObject: null,

        options: {
            maxlength: -1,
			text: ""
        },
		templates: {
			bg: '<div class="bg"><div class="left"></div><div class="center"></div><div class="right"></div>',
			input: '<div class="input"><input id="${id}" type="" maxlength="" value="${text}"></input></div>'
		},
		objIME: null,
		idIME: null,
		fnReturn: null,

        _create: function(){
            alert("[sf.ui.TextInput] _create()");
            this._super();
			var w = this.widget();
			var o = this.option;
			var v = this.view;

			this.idIME = 'sf-ui-textinput-'+(new Date()).getTime();

			$.tmpl(this.templates.bg, {}).appendTo(w);
			$.tmpl(this.templates.input, {id: this.idIME, text: o.text}).appendTo(w);

			v.input = w.find('.input input');
        },

        _init: function(){
            alert("[sf.ui.TextInput] _init()");
            this._super();

			this._setLayout();
			this._setText();
			this._setIMEObj();
        },

		_setLayout: function () {
			alert("[sf.ui.TextInput] _setLayout()");
			var w = this.widget();
			var v = this.view;
			var width = w.width();
			var leftWidth = w.find('.bg .left').width();
			var rightWidth = w.find('.bg .right').width();
			w.find('.bg .center').css('width', (width-leftWidth-rightWidth)+'px');

			var inputLeft = parseInt(w.find('.input').css('left'), 10);
			w.find('.input').css('width', (width-inputLeft*2)+'px');
			w.find('.input input').css('width', (width-inputLeft*2)+'px');


			alert(w.html());
		},

		_setText: function () {
			alert("[sf.ui.TextInput] _setText()");
			var w = this.widget();
			var o = this.option;
			var v = this.view;
			alert("TEXT : " + o.text);
			w.find('.input input').val(o.text);
		},

		_setIMEObj: function () {
			alert("[sf.ui.TextInput] _setIMEObj()");
			var w = this.widget();
			this.objIME = new IMEShell(this.idIME, function(){
			});
			var _THIS_ = this;
			this.objIME.setKeyFunc(sf.key.RETURN, function () {
				sf.scene.returnFocus();
				_THIS_._blur();
				if(_THIS_.fnReturn && _THIS_.fnReturn instanceof Function) {
					_THIS_.fnReturn();
					_THIS_.fnReturn = null;
				}
			});

			var offsetLeft = w.offset().left;
			var offsetTop = w.offset().top;
			this.objIME.setKeypadPos(offsetLeft + w.width(), offsetTop, 200);
		},

        _destroy: function(){
            alert("[sf.ui.TextInput] _destroy()");
        }, // _destroy()

		_focus: function(returncallback) {
			alert("[sf.ui.TextInput] _focus()");
			var w = this.widget();
			w.addClass('focused');
			w.find('.input input').focus();
			this.fnReturn = returncallback;
		},

		_blur: function() {
			alert("[sf.ui.TextInput] _blur()");
			var w = this.widget();
			w.removeClass('focused');
		},

		focus: function (returncallback) {
			alert('[sf.ui.TextInput] focus()');
			this._focus(returncallback);
			return this;
		}, // focus()

		blur: function () {
			alert('[sf.ui.TextInput] blur()');
			this._blur();
			return this;
		}, // blur()

		ime: function () {
			alert('[sf.ui.TextInput] ime(): ' + arguments[0]);
			if(this.objIME && this.objIME[arguments[0]]) {
				this.objIME.call(arguments);
			}
		}

    }); // sf.ui.TextInput()
    sf.ui.bridge(widgetName, sf.ui.TextInput);

    sf.ui.addSelector(widgetName);

}(jQuery));
/*
 * Title: UI Components (ProgressBar)
 * Apps Framework ProgressBar UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfProgressBar";

	/*
		Class: sf.ui.ProgressBar (jQuery.sfProgressBar)
		Apps Framework Class for ProgressBar UI
	*/
	sf.ui.ProgressBar = sf.ui.widgetFactory({
		/** @lends sf.ui.ProgressBar.prototype */

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-ProgressBar'
		 * @type String
		 */
		baseCssClass: 'sf-ui-progressbar',

		/**
		 * Event namespace added by default to every event triggered by ProgressBar.
		 * @default "sfProgressBar"
		 * @type String
		 */
		eventNamespace: "sfProgressBar",

		/*
		 	Function: jQuery.sfProgressBar
				Progress bar instance state object.

			Parameter:
				o (Object) options - options for scroll
					value
					max
					type - ('progress', 'loading', 'buffering', 'status')

			Returns:
				(Object) jQuery object

			Rendered HTML:
				Before init:
				(start code)
				<div id="progress1"></div>
				(end)

				After init:
				(start code)
				(end)
		 */
		options: {
			value: 0,
			max: 100,
			type: 'progress', // 'progress', 'loading', 'buffering', 'status'
		},

		stepWidth: 0,
		templates: {
			bg: '<div class="bg"><div class="left"></div><div class="center"></div><div class="right"></div></div>',
			bar: '<div class="bar"></div>',
			anibar: '<div class="ani"></div>'
		},
		totalWidth: 0,

		widgetName: widgetName,

		_create: function () {
			alert("[sf.ProgressBar] _create()");

			var o = this.options,
				t = this.templates,
				v = this.view,
				w = this.widget();

			this._super();

			this.totalWidth = w.width();

			$.tmpl(t.bg, {}).appendTo(w);
			$.tmpl(t.bar, {}).appendTo(w);
			$.tmpl(t.anibar, {}).appendTo(w);

			v.bar = w.find('.bar');
			v.anibar = w.find('.ani');

			alert(w.html());
		}, // _create()

		_init: function () {
			alert("[sf.ProgressBar] _init()");
			this._super();
			var o = this.options,
				v = this.view,
				w = this.widget();

			var leftWidth = w.find('.bg .left').width();
			var rightWidth = w.find('.bg .right').width();
			alert("Left : " + leftWidth + ", Right : " + rightWidth);
			w.find('.bg .center').css('width', (this.totalWidth-leftWidth-rightWidth)+'px');

			this._setType(o.type);
			this._setValue(o.value);
			this._setAnimation();
			alert(w.html());
		},

		_setType: function (type) {
			alert("[sf.ProgressBar] _setType("+type+")");
			var v = this.view;
			switch(type) {
				case 'progress':
					v.bar.show();
					v.anibar.show();
					v.anibar.addClass('normal').removeClass('loading');
					break;
				case 'buffering':
					v.bar.hide();
					v.anibar.show();
					v.anibar.addClass('normal').removeClass('loading');
					break;
				case 'loading':
					v.bar.hide();
					v.anibar.show();
					v.anibar.removeClass('normal').addClass('loading');
					break;
				case 'status':
					v.bar.show();
					v.anibar.hide();
					break;
			}
		},

		_setValue: function (value) {
			alert("[sf.ProgressBar] _setValue("+value+")");
			var o = this.options;
			var v = this.view;
			o.value = value;
			alert("Range : " + 0 + " ~ " + o.max);
			o.value = Math.max(o.value, 0);
			o.value = Math.min(o.value, o.max);

			var left = parseInt(v.bar.css('left'));
			var len = (this.totalWidth-left*2) * (o.value / o.max);

			v.bar.css('width', parseInt(len, 10)+'px');
			v.anibar.css('width', parseInt(len, 10)+'px');
		},

		_setAnimation: function () {
			alert("[sf.ProgressBar] _setAnimation()");
			var v = this.view;
			var _THIS_ = this;
			this.anistep = 0;
			this.anistepcount = 4;
			this.tmrAnimation = setInterval(function () {
				_THIS_._nextAnimation();
			}, 200);
		},
		_nextAnimation: function () {
			//alert("[sf.ProgressBar] _nextAnimation()");
			var v = this.view;
			v.anibar.removeClass('step'+this.anistep);
			this.anistep = (this.anistep+1)%this.anistepcount;
			v.anibar.addClass('step'+this.anistep);
		},
		_clearAnimation: function () {
			alert("[sf.ProgressBar] _clearAnimation()");
			if(this.tmrAnimation) {
				clearInterval(this.tmrAnimation);
				this.tmrAnimation = null;
			}
		},

		_destroy: function () {
			var v = this.view;
			this._clearAnimation();
		}, // _destroy()


		show: function () {
			alert("[sf.ProgressBar] show()");
			this._setAnimation();
		},
		hide: function () {
			alert("[sf.ProgressBar] hide()");
			this._clearAnimation();
		},
		next: function() {
			alert("[sf.ProgressBar] next()");
			var o = this.options;
			alert(o.value);
			this._setValue(o.value+1);
		},
		prev: function() {
			alert("[sf.ProgressBar] prev()");
			var o = this.options;
			alert(o.value);
			this._setValue(o.value-1);
		},
		setValue: function (val) {
			alert("[sf.ProgressBar] setValue("+val+")");
			this._setValue(val);
		},
		getValue: function () {
			alert("[sf.ProgressBar] getValue()");
			return this.option.value;
		},

		_setOption: function (name, value) {
			alert("[sf.ProgressBar] _setOption(" + name + ", " + value + ")");

			var o = this.options;

			switch (name) {
				case "value":
					o.value = value;
					this._setValue(o.value);
					break;
				case "max":
					o.max = value;
					this._setValue(o.value);
					break;
				case "type":
					o.type = value;
					this._setType(o.type);
					break;
			} // switch(name)
		}, // _setOption()


	}); // sf.ui.Scroll()

	sf.ui.bridge(widgetName, sf.ui.ProgressBar);

	sf.ui.addSelector(widgetName);

}(jQuery));

/*
 * Title: UI Components (PageIndicator)
 * Apps Framework Page Indicator UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfPageIndicator";

	/*
		Class: sf.ui.PageIndicator (jQuery.sfPageIndicator)
		Apps Framework Class for PageIndicator UI
	*/
	sf.ui.PageIndicator = sf.ui.widgetFactory({
		/** @lends sf.ui.PageIndicator.prototype */

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-PageIndicator'
		 * @type String
		 */
		baseCssClass: 'sf-ui-pageindicator',

		/**
		 * Event namespace added by default to every event triggered by PageIndicator.
		 * @default "sfPageIndicator"
		 * @type String
		 */
		eventNamespace: "sfPageIndicator",

		/*
		 	Function: jQuery.sfPageIndicator
				Progress bar instance state object.

			Parameter:
				o (Object) options - options for scroll
					index
					count

			Returns:
				(Object) jQuery object

			Rendered HTML:
				Before init:
				(start code)
				<div id="progress1"></div>
				(end)

				After init:
				(start code)
				(end)
		 */
		options: {
			index: 0,
			count: 100
		},

		templates: {
			dot: '<div class="dot"></div>'
		},

		widgetName: widgetName,

		_create: function () {
			alert("[sf.PageIndicator] _create()");

			var o = this.options,
				t = this.templates,
				v = this.view,
				w = this.widget();

			this._super();

			var data = [];
			for(var i=0; i<o.count; i++) {
				data.push({});
			}
			$.tmpl(t.dot, data).appendTo(w);
		}, // _create()

		_init: function () {
			alert("[sf.PageIndicator] _init()");
			this._super();
			var o = this.options,
				w = this.widget();

			this._setIndex(o.index);
			alert(w.html());
		},

		_setIndex: function (index) {
			alert("[sf.PageIndicator] _setIndex("+index+")");
			var v = this.view,
				w = this.widget(),
				o = this.options;

			o.index = index;
			o.index = Math.max(o.index, 0);
			o.index = Math.min(o.index, o.count);

			w.find('.dot').removeClass('selected');
			w.find('.dot').eq(o.index).addClass('selected');
		},

		_destroy: function () {
			var v = this.view;
		}, // _destroy()

		next: function() {
			alert("[sf.PageIndicator] next()");
			var o = this.options;
			this.setIndex(o.index+1);
		},
		prev: function() {
			alert("[sf.PageIndicator] prev()");
			var o = this.options;
			this.setIndex(o.index-1);
		},
		setIndex: function (index) {
			alert("[sf.PageIndicator] setIndex("+index+")");
			this._setIndex(index);
		},
		getIndex: function () {
			alert("[sf.PageIndicator] getIndex()");
			return this.option.index;
		},

		_setOption: function (name, value) {
			alert("[sf.PageIndicator] _setOption(" + name + ", " + value + ")");

			var o = this.options;

			switch (name) {
				case "index":
					o.index = value;
					this._setIndex(o.value);
					break;
				case "count":
					o.count = value;
					this._setIndex(o.value);
					break;
			} // switch(name)
		}, // _setOption()


	}); // sf.ui.Scroll()

	sf.ui.bridge(widgetName, sf.ui.PageIndicator);

	sf.ui.addSelector(widgetName);

}(jQuery));

/*
 * Title: UI Components (Label)
 * Apps Framework Slider UI Components
 *
 * Version added:
 * 1.1.0
 */

(function ($) {

	var widgetName = "sfSlider";

	/*
		Class: sf.ui.Slider (jQuery.sfSlider)
			Apps Framework Class for Slider UI
	*/
	/*
		Function: jQuery.sfSlider
			Adds Slider object to element.

		Rendered HTML:
			Before init:
				(start code)
				<div id="slider1"></div>
				(end)
	        After init:
				(start code)
				<div id="slider1" class="sf-ui-slider"></div>
				(end)
	*/
	sf.ui.Slider = sf.ui.widgetFactory({

		widgetName: widgetName,

		baseCssClass: 'sf-ui-slider',

		options: {
			vertical: false,
			value: 0,
			max: 100,
			showtooltip: false,
			showprogress: false,
			reverse: false
		},

		templates: {
			bg: '<div class="bg"><div class="begin"></div><div class="mid"></div><div class="end"></div></div>',
			bar: '<div class="bar"><div class="begin"></div><div class="mid"></div><div class="end"></div></div>',
			thumb: '<div class="thumb"></div>',
			tooltip: '<div class="tooltip"></div>'
		},

		scrollSize: 0,

		_create: function() {
			alert("[sf.ui.Slider] _create()");
			this._super();
			var w = this.widget(),
				t = this.templates,
				o = this.options;

			$.tmpl(t.bg, {}).appendTo(w);
			$.tmpl(t.bar, {}).appendTo(w);
			$.tmpl(t.thumb, {}).appendTo(w);
			$.tmpl(t.tooltip, {}).appendTo(w);
		},

		_init: function() {
			alert("[sf.ui.Slider] _init()");
			this._super();

			var w = this.widget(),
				t = this.templates,
				o = this.options;

			if(o.vertical && !w.hasClass('vertical')) {
				w.addClass('vertical');
			}
			else if(!o.vertical && w.hasClass('vertical')) {
				w.removeClass('vertical');
			}
			this._setLayout();
			this._updateValue();
		},

		_setLayout: function () {
			alert("[sf.ui.Slider] _setLayout()");
			var w = this.widget(),
				t = this.templates,
				o = this.options;

			if(o.vertical) {
				var height = w.height();
				var topHeight = w.find('.bg .begin').height();
				var bottomHeight = w.find('.bg .end').height();
				w.find('.bg .mid').css({
					'height': (height - topHeight - bottomHeight) + 'px',
					'width': ''
				});
				this.scrollSize = height - w.find('.thumb').height();
			}
			else {
				var width = w.width();
				var leftWidth = w.find('.bg .begin').width();
				var rightWidth = w.find('.bg .end').width();
				w.find('.bg .mid').css({
					'width': (width-leftWidth-rightWidth)+'px',
					'height': ''
				});
				this.scrollSize = width - w.find('.thumb').width();
			}
		},

		_updateValue: function () {
			alert("[sf.ui.Slider] _updateValue()");
			var w = this.widget(),
				t = this.templates,
				o = this.options;

			if(o.vertical) {
				var thumbTop = parseInt(this.scrollSize*((!o.reverse?o.value:(o.max-o.value))/o.max), 10);
				w.find('.thumb').css({
					'top': thumbTop + 'px',
					'left': ''
				});
				var thumbHeight = w.find('.thumb').height();
				w.find('.bar').css({
					'top': (!o.reverse ? 0 : parseInt(thumbTop + thumbHeight / 2, 10)) + 'px',
					'left': ''
				});
				var topHeight = w.find('.bar .begin').height();
				var btmHeight = w.find('.bar .end').height();
				var height = w.height();
				w.find('.bar .mid').css({
					'height': (!o.reverse ? parseInt(thumbTop+thumbHeight/2, 10)-(topHeight+btmHeight) : (height-parseInt(thumbTop+thumbHeight/2, 10)-(topHeight+btmHeight)))+'px',
					'width': ''
				});
			}
			else {
				var thumbLeft = parseInt(this.scrollSize*((o.reverse?(o.max-o.value):o.value)/o.max), 10);
				w.find('.thumb').css({
					'left': thumbLeft + 'px',
					'top': ''
				});
				var thumbWidth = w.find('.thumb').width();
				w.find('.bar').css({
					'left': (!o.reverse ? 0 : parseInt(thumbLeft+thumbWidth/2, 10))+'px',
					'top': ''
				});
				var leftWidth = w.find('.bar .begin').width();
				var rightWidth = w.find('.bar .end').width();
				var width = w.width();
				w.find('.bar .mid').css({
					'width': (!o.reverse ? parseInt(thumbLeft+thumbWidth/2, 10)-(leftWidth+rightWidth) : (width-parseInt(thumbLeft+thumbWidth/2, 10)-(leftWidth+rightWidth)))+'px',
					'height': ''
				});
			}
		},

		_setOption: function(name, value) {
			alert("[sf.ui.Slider] _setOption(" + name + ", " + value + ")");
			switch (name) {
				case 'vertical':
					this.setVertical((value===undefined||value==true)?true:false);
					break;
				case 'value':
					this.setValue(value);
					break;
				case 'max':
					this.setMax(value);
					break;
				case 'showtooltip':
					this.showTooltip((value===undefined||value==true)?true:false);
					break;
				case 'showprogress':
					this.showProgress((value===undefined||value==true)?true:false);
					break;
				case 'reverse':
					this.setReverse((value===undefined||value==true)?true:false);
					break;
			}
		}, // _setOption()

		_destroy: function () {
			alert("[sf.ui.Slider] _destroy()");
			this.widget().empty();
		}, // _destroy()

		focus: function () {
			alert("[sf.ui.Slider] focus()");
			var w = this.widget();
			if(!w.hasClass('focused')) {
				w.addClass('focused');
			}
		},

		blur: function () {
			alert("[sf.ui.Slider] blur()");
			var w = this.widget();
			if(w.hasClass('focused')) {
				w.removeClass('focused');
			}
		},

		setVertical: function (vertical) {
			alert("[sf.ui.Slider] setVertical("+vertical+")");
			var w = this.widget(),
				o = this.options;
			o.vertical = vertical ? true: false;
			if(o.vertical && !w.hasClass('vertical')) {
				w.addClass('vertical');
			}
			else if(!o.vertical && w.hasClass('vertical')) {
				w.removeClass('vertical');
			}
			this._setLayout();
			this._updateValue();
		},
		isVertical: function () {
			alert("[sf.ui.Slider] isVertical(): " + this.options.vertical);
			return this.options.vertical;
		},

		showTooltip: function (show) {
			alert("[sf.ui.Slider] showTooltip()");
			this.options.showtooltip = (show || show===undefined) ? true : false;
		},

		showProgress: function (show) {
			alert("[sf.ui.Slider] showProgress("+show+")");
			this.options.showprogress = (show || show===undefined) ? true : false;
			var w = this.widget();

			if (this.options.showprogress) {
				w.find('.bar').show();
			}
			else {
				w.find('.bar').hide();
			}
		},

		setValue: function (value) {
			alert("[sf.ui.Slider] setValue("+value+")");
			this.options.value = parseInt(value, 10);
			this.options.value = Math.min(this.options.value, this.options.max);
			this.options.value = Math.max(this.options.value, 0);

			this._updateValue();
		},
		getValue: function () {
			alert("[sf.ui.Slider] getValue(): " + this.options.value);
			return this.options.value;
		},

		setMax: function (max) {
			alert("[sf.ui.Slider] setMax("+max+")");
			this.options.max = parseInt(max, 10);
			this.options.value = Math.min(this.options.value, this.options.max);

			this._updateValue();
		},
		getMax: function () {
			alert("[sf.ui.Slider] getMax(): " + this.options.max);
			return this.options.max;
		},

		setReverse: function (reverse) {
			alert("[sf.ui.Slider] setReverse()");
			this.options.reverse = (reverse || reverse===undefined) ? true : false;
			this._updateValue();
		},
		isReverse: function () {
			alert("[sf.ui.Slider] isReverse(): " + this.options.reverse);
			return this.options.reverse;
		}

	}); // sf.ui.Slider()

	sf.ui.bridge(widgetName, sf.ui.Slider);

    sf.ui.addSelector(widgetName);

}(jQuery));(function($) {
    var widgetName = "sfBackground";

    sf.ui.Background = sf.ui
    .widgetFactory({

        baseCssClass : "sf-ui-background",

        options : {
            texture : 1,
            shadow : 0,
            light : 0,
            column : 0,
            columnShadow : 0,
            //partingLine : 0,
            columnSize : 500
        },

        resolution: 0,

        bottomBarHeight : [33, 45, 67],

        templates : {
            main: $.template(null, '<div class="sf-ui-background">'+
                '{{if light == 1}}'+
                '<div class="sf-ui-background-light" style="left:${lightLeft}px;width:${lightWidth}px;"></div>'+
                '{{/if}}'+

                '{{if shadow == 1}}'+
                '<div class="sf-ui-background-shadow"></div>'+
                '{{/if}}'+

                '{{if column == 1}}'+
                '{{tmpl(subdata) "sf_background_leftColumn"}}'+
                '{{/if}}'+

                '{{if column == 2}}'+
                '{{tmpl(subdata) "sf_background_rightColumn"}}'+
                '{{/if}}'+

                '{{if column == 3}}'+
                '{{tmpl(subdata) "sf_background_horizontalColumn"}}'+
                '{{/if}}'+

                '</div>'),

            leftColumn: $.template("sf_background_leftColumn",
            '<div class="sf-ui-background-column-vertical-left" style="width:${columnSize}px;"></div>'+
                '{{if columnShadow == 1}}'+
                '<div class="sf-ui-background-column-vertical-shadow-right" style="left:${columnSize}px;"></div>'+
                '{{else}}'+
                '<div class="sf-ui-background-column-vertical-parting-line-right" style="left:${columnSize}px;"></div>'+
                '{{/if}}'

        ),

            rightColumn: $.template("sf_background_rightColumn",
            '<div class="sf-ui-background-column-vertical-right" style="width:${columnSize}px;"></div>'+

                '{{if columnShadow == 1}}'+
                '<div class="sf-ui-background-column-vertical-shadow-left" style="right:${columnSize}px;"></div>'+
                '{{else}}'+
                '<div class="sf-ui-background-column-vertical-parting-line-left" style="right:${columnSize}px;"></div>'+
                '{{/if}}'),

            horizontalColumn: $.template("sf_background_horizontalColumn",
            '<div class="sf-ui-background-column-horizontal" style="height: ${columnSize}px;"></div>'+

                '{{if columnShadow == 1}}'+
                '<div class="sf-ui-background-column-horizontal-shadow-bottom " style="bottom:${bottom}px;"></div>'+
                '{{else}}'+
                '<div class="sf-ui-background-column-horizontal-parting-line-bottom" style="bottom:${bottom}px;"></div>'+
                '{{/if}}'

        )


        },

        widgetName : widgetName,

        _create : function() {
            this._super();
            this.view.mainTemplate = this.templates.main;

        }, // _create()

        _init : function() {
            // alert("[sf.Background] _init()");
            this._pickResolution();
            this.data = this._getData();
            this._redraw();



        }, // destroy()

        _destroy : function() {
            this.view.mainTemplate.remove();

        }, // destroy()

        _setOption: function(name, value) {
            alert("[sf.Background] _setOption(" + name + ", " + value + ")");

            switch (name) {
                case 'column':
                    this.options.column = value;
                    break;
                case 'light':
                    this.options.light = value;
                    break;
                case 'shadow':
                    this.options.shadow = value;
                    break;
                case 'columnSize':
                    this.options.columnSize = value;
                    break;
                case 'columnShadow':
                    this.options.columnShadow = value;
                    break;
                case 'image':
                    this.options.image = value;
                    break;
            }
            this.data = this._getData();
            this._redraw();

        },

        _redraw: function () {

            this.widget().html($.tmpl(this.templates.main, this.data));
            //$.tmpl(this.templates.main, this.data).appendTo(this.widget());
        },

        _pickResolution: function () {
            var h = sf.ui.height;

            this.resolution = h === 540 ? 0 : h === 720 ? 1 : h === 1080 ? 2 : 0;
        },

        _getData: function () {

            var self = this,
            o = this.options;

            var lightLeft = 0;
            var lightWidth = sf.ui.width;
            var newData;

            if (o.column == 1) {
                lightWidth = lightWidth - o.columnSize;
                lightLeft = o.columnSize;
            }

            if (o.column == 2) {
                lightWidth = lightWidth - o.columnSize;
            }


            newData = {
                column: o.column,
                light: o.light,
                shadow: o.shadow,
                lightWidth: lightWidth,
                lightLeft: lightLeft,
                subdata: {
                    columnSize: o.columnSize,
                    columnShadow: o.columnShadow,
                    bottom:  o.columnSize + this.bottomBarHeight[this.resolution]
                }
            };

            return newData;
        }


    }); // sf.ui.Background()

    sf.ui.bridge(widgetName, sf.ui.Background);

    sf.ui.addSelector(widgetName);

}(jQuery));
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    var widgetName = "sfScrollFrame";

    sf.ui.ScrollFrame = sf.ui.widgetFactory({
	/**
	* Widget name
	* @default 'sfScrollFrame'
	* @type String
	*/
	widgetName: widgetName,

	/**
	 * Widget base CSS class
	 * @default 'sf-ui-scrollframe'
	 * @type String
	 */
	baseCssClass: "sf-ui-scrollframe",

	eventNamespace: "sfscrollframe",


	/*
	 * @title title of category list. if the 'vertical' option is false, this property is ignored.
	 * @expandable whether expandable the list or not
	 * @vertical whether the list shows vertically or horizontally. (default : true)
	 **/

	options: {
		scrollBy : 'steps', //steps || pixels
	    steps : 10,
		pixels : 10
	},

	templates: {
	   main: $.template(null, '<div class="sf-ui-scrollframe-scroll" style="height: ${sfHeight}px;"><div class="sf-ui-scrollframe-bar"></div></div><div class="sf-ui-scrollframe-content">{{html scrollFrameContents}}</div>')
	},

	_create: function() {
		alert("----------------------CREATE!");
	    alert("[sf.ScrollFrame] _init()");
	    this._super();

	    this.scrollFrameContents = this.widget().html();
		this.steps = this.options.steps;
		this.pixels = this.options.pixels;
		this.scrollBy = this.options.scrollBy;
		this.elem = this.widget();
		this.elemId = this.widget().attr('id');
		this.stepNumber = 0;

	    this.view.mainTemplate = this.templates.main;

		this._redraw();

		this.sfWidth = parseInt($(this.elem).css("width"));
		alert("PPPPPPPPPPPPPPP: " + parseInt($(".sf-ui-scrollframe-content", this.elem).css("padding-left"), 10));
		alert("--------------------sfWidth: "+this.sfWidth);
		this.sfHeight = parseInt($(this.elem).css("height"));
		alert("--------------------sfHeight: "+this.sfHeight);

		this.sfContentWidth = parseInt(this.sfWidth-parseInt($(".sf-ui-scrollframe-scroll", this.elem).css("width"))-parseInt($(".sf-ui-scrollframe-content", this.elem).css("padding-left"), 10)-parseInt($(".sf-ui-scrollframe-content", this.elem).css("padding-right"), 10));
		alert("XXXXXX--------------------sfContentWidth: "+this.sfContentWidth);

		this.innerWidth = $(".sf-ui-scrollframe-content", this.elem).innerWidth();
		alert("--------------------innerWidth: "+this.innerWidth);
		this.innerHeight = $(".sf-ui-scrollframe-content", this.elem).innerHeight();
		alert("--------------------innerHeight: "+this.innerHeight);

		this.scrollHeight = parseInt(this.innerHeight + parseInt($(".sf-ui-scrollframe-content", this.elem).css("padding-top"), 10) + parseInt($(".sf-ui-scrollframe-content", this.elem).css("padding-bottom"), 10) - this.sfHeight);
		alert("--------------------scrollHeight: "+this.scrollHeight);

		this.scrollWidth = parseInt($(".sf-ui-scrollframe-scroll", this.elem).css("width"));
		alert("--------------------scrollWidth: "+this.scrollWidth);

		if(this.scrollBy=='pixels') {
			this.steps = Math.ceil(this.scrollHeight/this.pixels);
		} else {
			this.pixels = Math.ceil(this.scrollHeight/(this.steps-1));
		}

		alert("--------------------scrollBy: "+this.scrollBy);
		alert("--------------------sfHeight: "+this.sfHeight);
		alert("--------------------steps: "+this.steps);
		alert("--------------------pixels: "+this.pixels);

		this.barHeight = Math.ceil(this.sfHeight/this.steps);
		alert("barHeight: "+this.barHeight);

		this.barJump = Math.ceil(parseInt(this.sfHeight-this.barHeight-1)/this.steps);
		alert("barJump: "+this.barJump);

		$(".sf-ui-scrollframe-bar", this.elem).css("height", this.barHeight+"px");
		$(".sf-ui-scrollframe-scroll", this.elem).css("height", this.sfHeight+"px");
		$(".sf-ui-scrollframe-content", this.elem).css("width", this.sfContentWidth+"px");
		alert("sf-ui-scrollframe-scroll HEIGHT: "+$(".sf-ui-scrollframe-scroll", this.elem).css("height"));

	},

	_init: function () {
		alert("----------------------INIT!");

	},

	_destroy: function () {

	},

	_setOption: function (name, value) {

	},

	_redraw: function () {
		alert("----------------------REDRAW!");
		alert("ELEM ID: "+this.elemId);

	    this.view.mainTemplate = $.tmpl(this.templates.main, {"scrollFrameContents": this.scrollFrameContents, "sfContentWidth": this.sfContentWidth+"-sf-ui-scrollframe-content", "sfWidth": this.sfWidth, "sfHeight": this.sfHeight});
	    this.widget().html(this.view.mainTemplate);
	},

	scrollUp: function() {
		alert("SCROLL UP: ");

		var contentPosition = parseInt($(".sf-ui-scrollframe-content", this.elem).css("top"));
		alert("contentPosition PRE: "+contentPosition);
		contentPosition = parseInt(contentPosition+this.pixels);
		alert("contentPosition POST: "+contentPosition);

		var barPosition = parseInt($(".sf-ui-scrollframe-bar", this.elem).css("top"));
		alert("barPosition: "+barPosition);
		barPosition = parseInt(barPosition-this.barJump);
		if(parseInt(barPosition) < 0) {
			barPosition = 0;
		}

		if(this.stepNumber > 0) {
			this.stepNumber--;
			$(".sf-ui-scrollframe-bar", this.elem).css("top", barPosition + "px");
			$(".sf-ui-scrollframe-content", this.elem).css({"top": contentPosition + "px"});
		}

	},

	scrollDown: function() {
		alert("SCROLL DOWN: ");

		if(this.stepNumber < this.steps) {

			var contentPosition = parseInt($(".sf-ui-scrollframe-content", this.elem).css("top"));
			alert("contentPosition PRE: "+contentPosition+"/"+this.scrollHeight);
			contentPosition = parseInt(contentPosition-this.pixels);
			alert("contentPosition POST: "+contentPosition+"/"+this.scrollHeight);

			var barPosition = parseInt($(".sf-ui-scrollframe-bar", this.elem).css("top"));
			barPosition = parseInt(barPosition+this.barJump);

			if( parseInt(barPosition)+parseInt(this.barHeight) >= parseInt(this.sfHeight) ) {
				barPosition = parseInt(this.sfHeight-this.barHeight);
			}

			alert("AAAAAAA -------- barPosition: "+barPosition);
			alert("AAAAAAA -------- barHeight: "+this.barHeight);
			alert("AAAAAAA -------- sfHeight: "+this.sfHeight);
			alert("AAAAAAA -------- steps: "+this.steps);
			alert("AAAAAAA -------- stepNumber: "+this.stepNumber);

			$(".sf-ui-scrollframe-bar", this.elem).css("top", barPosition+"px");
			$(".sf-ui-scrollframe-content", this.elem).css({"top": contentPosition+"px"});

			this.stepNumber++;
		}

	}

    });

    sf.ui.bridge(widgetName, sf.ui.ScrollFrame);
    sf.ui.addSelector(widgetName);

})(jQuery);/*
* Title: UI Components (TitleBar)
* Apps Framework TitleBar UI Components
*
 * Version added:
* 1.1.0
*/

(function ($) {

    var widgetName = "sfTitleBar";

    sf.ui.TitleBar = sf.ui.widgetFactory({
        /** @lends sf.ui.TitleBar.prototype */

        /**
                               * Widget name
                               * @default 'sfTitleBar'
                               * @type String
                               */
        widgetName: widgetName,

        /**
                               * Widget base CSS class
                               * @default 'sf-ui-common-ellipsis'
                               * @type String
                               */
        baseCssClass: 'sf-ui-titlebar',

        /**
                               * Label instance state object.
                               * <dl class="detailList">
                               *   <dt class="heading">Properties:</dt>
                               *   <dt>
                               *     <span class="light fixedFont">{String}</span> <b>text</b> (defaults to '')
                               *   </dt>
                               *   <dd>Text inserted into instance element.</dd>
                               * </dl>
                               * @type Object
                               */

        data : null,
        defaultImg: "/ui.titlebar/picasa.png",//titlebar_blanc_ico.png",

        options: {
            text: '',
            image: "",//titlebar_blanc_ico.png",
            type: 0,
            title: 'Default Title',
            subtitle: 'Default Subtitle'
        },

        templates :

        {
            main: $.template(null, '<div class="sf-ui-titlebar-bar">'+
                '{{if type == 0}}'+
                '{{tmpl(subdata) "sf_titlebar_type0"}}'+
                '{{/if}}'+

                '{{if type == 1}}'+
                '{{tmpl(subdata) "sf_titlebar_type1"}}'+
                '{{/if}}'+

                '{{if type == 2}}'+
                '{{tmpl(subdata) "sf_titlebar_type2"}}'+
                '{{/if}}'+

                '{{if type == 3}}'+
                '{{tmpl(subdata) "sf_titlebar_type3"}}'+
                '{{/if}}'+

                '</div>'),

            type0: $.template('sf_titlebar_type0',
                '<table cellpadding="0" cellspacing="0" border="0" class="sf-ui-titlebar-holder type0">'+
                    '<tr>'+
                        '<td class="sf-ui-titlebar-title-type0" valign="middle" align="left">'+
                            '<span class="sf-ui-titlebar-title-text">${title}</span>'+
                        '</td>'+
                        '<td class="sf-ui-titlebar-subtitle-type0">'+
                            '<span class="sf-ui-titlebar-subtitle-text">${subtitle}</span>'+
                        '</td>'+
                    '</tr>'+
                '</table>'),

            type1: $.template('sf_titlebar_type1',
                '<table cellpadding="0" cellspacing="0" border="0" align="left" class="sf-ui-titlebar-holder">'+
                    '<tr>'+
                        '<td valign="middle" class="sf-ui-icon-type1"><img src="${image}" alt="" /></td>'+
                        '<td valign="middle" class="sf-ui-titlebar-title-type1"><span class="sf-ui-titlebar-title-text">${title}</span></td>'+
                    '</tr>'+
                '</table>'),

            type2: $.template('sf_titlebar_type2',
                '<table cellpadding="0" cellspacing="0" border="0" align="center" class="sf-ui-titlebar-holder">'+
                  '<tr>'+
                       '<td valign="center" class="sf-ui-icon-type2"><img src="${image}" alt="" /></td>'+
                       '<td valign="center" class="sf-ui-titlebar-title-type2"><span class="sf-ui-titlebar-title-text">${title}</span></td>'+
                   '</tr>'+
                '</table>'),

            type3: $.template('sf_titlebar_type3',
                '<table cellpadding="0" cellspacing="0" border="0" class="sf-ui-titlebar-holder">'+
                  '<tr>'+
                      '<td class="sf-ui-titlebar-arrow-left"></td>'+
                     '<td class="sf-ui-titlebar-title-type3" valign="middle" align="center">'+
                          '<span class="sf-ui-titlebar-title-text">${title}</span>'+
                      '</td>'+
                      '<td class="sf-ui-titlebar-arrow-right"></td>'+
                  '</tr>'+
                '</table>')
        },

        /**
         * Works as a constructor.
         * Stores element's original text in initialState object.
         * @private
         */
        _create: function() {
            alert("[sf.TitleBar] _create()");
            //this.initialState.content = this.widget().html(); // ?
            this.view.mainTemplate = this.templates.main;
            this._super();
        },

        /**
         * @private
                */
        _init: function() {
            alert("[sf.TitleBar] _init()");

            //this._super();

            this.data = this._getData();
            this._redraw();
        },


        /**
                               * @private
                               */
        _setOption: function(name, value) {
            alert("[sf.TitleBar] _setOption(" + name + ", " + value + ")");

            switch (name) {
                case 'title':
                    this.options.title = value;
                    break;
                case 'subtitle':
                    this.options.subtitle = value;
                    break;
                case 'type':
                    this.options.type = value;
                    break;
                case 'image':
                    this.options.image = value;
                    break;
            }
            this.data = this._getData();
            this._redraw();

        }, // _setOption()

        _destroy: function () {
            alert("[sf.TitleBar] _destroy()");
            this.view.mainTemplate.remove();
        }, // _destroy()

        _redraw: function () {
            this.view.mainTemplate = $.tmpl(this.templates.main, this.data);
            this.widget().html(this.view.mainTemplate);
           // $.tmpl(this.templates.main, this.data).appendTo(this.widget());
        },

        _getData: function () {

            var self = this,
            o = this.options;

            var newData;

            if (o.image == "") o.image =  sf.ui.images + this.defaultImg;

            newData = {
                type: o.type,
                subdata: {
                    title: o.title,
                    subtitle: o.subtitle,
                    image: o.image
                }
            };

            return newData;
        }

    }); // sf.ui.Label()

    sf.ui.bridge(widgetName, sf.ui.TitleBar);

    sf.ui.addSelector(widgetName);

}(jQuery));
/*jslint sloppy: true, white: true, vars: false, undef: true, nomen: true, eqeq: false, plusplus: true, bitwise: true, regexp: true, newcap: true */
/*global sf, alert, curWidget */
/**
 * Apps Framework UI Text List Component
 *
 * @author  Grzegorz Pabian <<a href='mailto:g.pabian@samsung.com'>g.pabian@samsung.com</a>>
 * @author  Marcin Kierski <<a href='mailto:m.kierski@samsung.com'>m.kierski@samsung.com</a>>
 * @date    16.11.2011
 *
 *
 *****************************************************************************
 *
 * Copyright (c) 2011 Samsung Electronics, Visual Display Division.
 * All Rights Reserved.
 *
 ****************************************************************************/

(function ($) {
	var widgetName = "sfTextList";

	/**
	 * SF UI Text List Component
	 * @class
	 * Allows to visualize data as vertical list in various templates
	 * @example
	 * <b>Example 1</b>
	 * Default initialization:
	 *
	 * var items = [
	 *     {"text": "Item one"},
	 *     {"text": "Item two"},
	 *     ...
	 *     {"text": "Item n"}
	 * ];
	 *
	 * $("#List").sfTextList({
	 *     items: items
	 * });
	 * <hr />
	 * <b>Example 2</b>
	 * Initialization with different template:
	 *
	 * $("#List").sfTextList({
	 *     items: items,
	 *     type: "text3"
	 * });
	 * <hr />
	 * <b>Example 3</b>
	 * Initialization with other options:
	 *
	 * $("#List").sfTextList({
	 *     items: items,
	 *     type: "text2",
	 *     index: 0,
	 *     itemsPerPage: 4
	 * });
	 * <hr />
	 * <b>Example 4</b>
	 * Using checkboxes:
	 *
	 * $("#List").sfTextList({
	 *     items: items,
	 *     selectable: 1,
	 *     selectPos: left,
	 *     selectField: "my_select_field"
	 * });
	 * <hr />
	 * <b>Example 5</b>
	 * Using subtitles:
	 *
	 * var items = [
	 *     {"text": "Subtitle 1", "subtitle": "true"},
	 *     {"text": "Item one"},
	 *     {"text": "Item two"},
	 *     ...
	 *     {"text": "Subtitle 2", "subtitle": "true"},
	 *     {"text": "Item one"},
	 *     ...
	 *     {"text": "Item n"}
	 * ];
	 *
	 * $("#List").sfTextList({
	 *     items: items,
	 *     itemsPerPage: 0,
	 *     scrollSingle: true
	 * });
	 *
	 * It's highly recommended to use subtitles with options <b>itemsPerPage</b> set to 0
	 *
	 * @augments sf.ui.Widget
	 */
	sf.ui.TextList = sf.ui.widgetFactory({
		/** @lends sf.ui.TextList.prototype */

		/**
		 * Widget name
		 * @default 'sfTextList'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Widget base CSS class
		 * @default 'sf-ui-textlist'
		 * @type String
		 */
		baseCssClass: "sf-ui-textlist",

		/**
		 * Event namespace
		 * @default "sftextlist"
		 * @type String
		 */
		eventNamespace: "sftextlist",

		/**
		 * Configuration options for widget
		 * @type Object
		 *
		 * @description
		 * Allowed options:
		 * <ul>
		 * <li><b>items</b> - array of list items</li>
		 * <li><b>type</b> - list template type, available values: text, text2, text3,
		 * thumbnail2, thumbnail3, video, comment, column_left, column_right</li>
		 * <li><b>index</b> - focused item index</li>
		 * <li><b>itemsPerPage</b> - number of items displayed on page, if = 0 list fits to available space</li>
		 * <li><b>selectField</b> - field used in selectable mode, describes selection state</li>
		 * <li><b>disabledField</b> - field used in selectable mode, describes if item is disabled</li>
		 * <li><b>loop</b> - enable/disable list looping (jumping from first to last element)</li>
		 * <li><b>selectable</b> - selection mode: 0 - no, 1 - checkboxes, 2 - radio buttons</li>
		 * <li><b>selectPos</b> - position of checkboxes/radios, left or right</li>
		 * <li><b>selectCssClass</b> - used for styling checkbox/radio</li>
		 * <li><b>disabledCssClass</b> - used for styling checkbox/radio</li>
		 * <li><b>focusCssClass</b> - focused item css class</li>
		 * <li><b>scrollSingle</b> - enable/disable scrolling by 1 item</li>
		 * <li><b>start</b> - index of first shown element</li>
		 * </ul>
		 */
		options: {
			items: [],							// array of list elements
			type: "text",						// list template type
			index: 0,							// focused item index
			itemsPerPage: 0,					// number of items displayed on page
			start: 0,							// first shown element index
			selectField: "selected",			// field used in selectable mode
			disabledField: "disabled",			// field used in selectable mode
			loop: true,							// enable/disable list looping
			selectable: 0,						// 0 - no, 1 - checkboxes, 2 - radio
			selectPos: "left",					// position of checkboxes/radios, left or right
			selectCssClass: "sf-ui-textlist-select",	// used for styling checkbox/radio
			disabledCssClass: "sf-ui-textlist-disabled", // used for styling checkbox/radio
			focusCssClass: "sf-ui-textlist-focus",	// focused item css class
			scrollSingle: false 				// enable/disable scrolling by 1 item
		},

		/**
		 * @type Object
		 * @description
		 * Default widget templates<br /><br />
		 * <u>Available templates:</u><br />
		 * checkbox, column_left, column_right, comment, cursor, line, list, radio,
		 * shadow, subtitle, text, text2, text3, thumbnail2, thumbnail3, video
		 */
		templates: {
			checkbox: '<div></div>',
			column_left: {
				height: [58, 78, 116],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span></div>'
			},
			column_right: {
				height: [58, 78, 116],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span></div>'
			},
			comment: {
				height: [76, 102, 152],
				thumb: [60, 80, 120],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>' +
					'<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span></div>'
			},
			cursor: {
				size: [4, 6, 8],
				tmpl: "<div></div>"
			},
			line: {
				height: [2, 2, 4],
				tmpl: '<div class="${lineClass}" style="height: ${height}px;">' +
					'<div class="line-left"></div>' +
					'<div class="line-center"></div>' +
					'<div class="line-right"></div>' +
					"</div>"
			},
			list: "<div></div>",
			radio: '<div></div>',
			shadow: {
				size: [30, 40, 60],
				offset: [10, 17, 20],
				tmpl: '<div class="${bcc}-shadow" style="width: ${shadowSize}px; height: ${shadowSize}px; top: -${shadowOffset}px; left: -${shadowOffset}px;">' +
					'<div class="${bcc}-shadow-lt"></div>' +
					'<div class="${bcc}-shadow-t" style="width: ${innerSize}px;"></div>' +
					'<div class="${bcc}-shadow-rt"></div>' +
					'<div class="${bcc}-shadow-r" style="height: ${innerSize}px;"></div>' +
					'<div class="${bcc}-shadow-rb"></div>' +
					'<div class="${bcc}-shadow-b"style="width: ${innerSize}px;"></div>' +
					'<div class="${bcc}-shadow-lb"></div>' +
					'<div class="${bcc}-shadow-l" style="height: ${innerSize}px;"></div>' +
					"</div>"
			},
			subtitle: {
				height: [28, 38, 58],
				tmpl: '<span class="text">${text}</span>' +
					'<span class="right">${right}</span>'
			},
			text: {
				height: [58, 78, 116],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="right">${right}</span></div>'
			},
			text2: {
				height: [58, 78, 116],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span>' +
					'<span class="right">${right}</span></div>'
			},
			text3: {
				height: [73, 98, 146],
				tmpl: '<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span>' +
					'<span class="bottom">${bottom}</span>' +
					'<span class="right">${right}</span></div>'
			},
			thumbnail2: {
				height: [58, 78, 116],
				thumb: [48, 64, 96],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>${tmpl.text2}'
			},
			thumbnail3: {
				height: [73, 98, 146],
				thumb: [60, 80, 120],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>${tmpl.text3}'
			},
			video: {
				height: [78, 102, 152],
				thumb: [60, 80, 120],
				tmpl: '<div class="${bcc}-thumb">${shadow}<div class="${bcc}-thumbcrop">' +
					'<img src="${thumbnail.url}" style="${thumbstyle}"/></div></div>' +
					'<div class="${bcc}-wrap"><span class="text">${text}</span>' +
					'<span class="subtext">${subtext}</span>' +
					'<span class="bottom">${bottom}</span></div>'
			}
		},

		/**
		 * Focused state
		 * @private
		 * @type Boolean
		 */
		isFocused: false,

		/**
		 * Application resolution
		 * @private
		 * @type Number
		 */
		resolution: 0,

		/**
		 * Used to determine accorion items alignment (top or bottom of widget)
		 * @private
		 * @type Number
		 */
		offset: 0,

		/**
		 * Calculated page size
		 * Used when itemsPerPage = 0
		 * @private
		 * @type Number
		 */
		calculatedIPP: 0,

		/**
		 * Item height used in current template
		 * @private
		 * @type Number
		 */
		itemHeight: 0,

		/**
		 * Refresh indicator
		 * @private
		 * @type Boolean
		 */
		doRefresh: false,

		/**
		 * Calculated widget height
		 * @private
		 * @type Number
		 */
		widgetHeight: 0,

		/************************************************
		 *					API METHODS					*
		 ************************************************/

		/**
		 * Add item to list
		 * @param {Object} item item to add
		 * @param {Number} [index] position of new item
		 * @example
		 * $("#List").sfTextList("addItem", {"text": "New item"});
		 */
		addItem: function (item, index) {
			alert("[sf.ui.TextList] addItem()");
			var o = this.options,
				max = o.items.length - 1;

			if (index && !isNaN(index)) {
				index = index < 0 ? 0 : index > max ? max : index;
				o.items.splice(index, 0, item);
			} else {
				o.items.push(item);
			}
			this._refresh();

			return this;
		},

		/**
		 * Binds mouse events to list elements.
		 * TODO: Execute it at the end of _drawItems()
		 * @private
		 */
		_bindEvents: function () {
			var v = this.view,
				o = this.options,
				that = this;

			// bind events to every list item
			v.items.each(function (index) {

				$(this).bind({
					mouseover: function () {
						that._drawCursor(index);
					},
					click: function () {
						o.index = index + o.start;
						that.focus();
						//TODO: add callback
					}
				});

			});

			// hide cursor when leaving list
			v.list.bind("mouseleave", function () {
				v.cursor.hide();
			});
		},

		/**
		 * Blur list elements
		 * @private
		 */
		_blur: function () {
			var v = this.view,
				o = this.options;

			v.cursor.hide();
			v.focus.hide();
			v.items && v.items.removeClass(o.focusCssClass);
			this.isFocused = false;
		},

		/**
		 * Blur list elements
		 * @example
		 * $("#List").sfTextList("blur");
		 */
		blur: function () {
			alert("[sf.ui.TextList] blur()");
			this._blur();
			return this;
		},

		/**
		 * Recalculate internal page size
		 * @param {Number} start Calculating start index
		 * @param {Boolean} [asc] Calculate with ascending indexes
		 * @private
		 */
		_calculateIPP: function (start, asc) {
			var o = this.options,
				t = this.templates,
				r = this.resolution,
				wHeight = this.widgetHeight,
				lineHeight = t.line.height[r],
				subHeight = t.subtitle.height[r],
				height = 0,
				i = start;

			asc = typeof asc === "undefined" ? true : asc;

			if (o.itemsPerPage > 0) {
				this.calculatedIPP = o.itemsPerPage;
			} else {
				this.calculatedIPP = 0;
				while (o.items[i] && height < wHeight) {
					height += o.items[i].subtitle ? subHeight : this.itemHeight;
					height += lineHeight;
					i = asc ? i + 1 : i - 1;
					this.calculatedIPP++;
				}
			}
		},

		/**
		 * Change item state indicator
		 * @param {Number} index Changed item's index
		 * @private
		 */
		_changeState: function (index) {
			var o = this.options,
				s = o.selectable,
				v = this.view,
				state = o.items[index][o.selectField],
				sTypes = [null, "checkbox", "radio"],
				klass = [this.baseCssClass, sTypes[s], o.selectPos].join("-"),
				relSelect = $(v.items[index - o.start]).find("." + klass);

			state ? relSelect.addClass(o.selectCssClass) : relSelect.removeClass(o.selectCssClass);
		},

		/**
		 * Clear all list elements
		 * @example
		 * $("#List").sfTextList("clear");
		 */
		clear: function () {
			alert("[sf.ui.TextList] clear()");
			this._blur();
			this.view.wrap.empty();
			this.options.items = [];

			return this;
		},

		/**
		 * Destroy component
		 * @private
		 */
		_destroy: function () {
			alert("[sf.ui.TextList] _destroy()");
			var w = this.widget();

			w.empty();
		},

		/**
		 * Draws mouse over indicator
		 * @param {Number} index Relative index of highlighed item
		 * @private
		 */
		_drawCursor: function (index) {
			var v = this.view,
				top = $(v.items[index]).position().top;

			v.cursor.css({
				"display": "block",
				"top": top + "px"
			});
		},

		/**
		 * Draw focus indicator
		 * @param {Number} index Relative index of focused item
		 * @private
		 */
		_drawFocus: function (index, top) {
			var v = this.view,
				o = this.options;

			v.items.removeClass(o.focusCssClass);
			$(v.items[index]).addClass(o.focusCssClass);

			v.focus.css({
				"display": "block",
				"top": top + "px"
			});
		},

		/**
		 * Draw list items
		 * @param {Array} items Visible items list
		 * @private
		 */
		_drawItems: function (items) {
			var o = this.options,
				v = this.view,
				t = this.templates,
				bcc = this.baseCssClass,
				itemTemplate = t[o.type].tmpl,
				subTemplate = t.subtitle.tmpl,
				columnKlass,
				html = [],
				item,
				iLen,
				i;

			v.wrap.empty();

			columnKlass = o.type === 'column_left' ? bcc + "-column-left" :
				o.type === 'column_right' ? bcc + "-column-right" : "";

			for (i = 0, iLen = items.length; i < iLen; i++) {
				item = $.extend({bcc: bcc}, items[i]);
				if (item.subtitle) {
					html[i] = this._tmpl('<div class="' + bcc + '-item ' +
						bcc + '-subtitle" style="height: ' +
						t.subtitle.height[this.resolution] + 'px;">' + subTemplate + "</div>",
						item);
				} else {
					if (item.thumbnail) {
						item.thumbstyle = this._thumbSize(item.thumbnail);
					}
					html[i] = this._tmpl('<div class="' + bcc + '-item ' + columnKlass +
						'" style="height: ' + this.itemHeight + 'px;">' + (o.selectable > 0 ?
						this._drawSelect(item) + itemTemplate : itemTemplate) + "</div>",
						item.thumbnail ? $.extend({shadow: v.shadow}, item) : item);
				}
				html[i] += v.line;
			}
			v.wrap.html(html.join(""));
			v.items = v.wrap.find("." + bcc + "-item");
		},

		/**
		 * Draw checkbox/radio box template
		 * @private
		 * @returns Template of checkbox/radio
		 */
		_drawSelect: function (item) {
			var o = this.options,
				t = this.templates,
				s = o.selectable,
				sTypes = [null, "checkbox", "radio"],
				klass = [this.baseCssClass, sTypes[s], o.selectPos].join("-");

			return '<div class="' + klass + (item[o.selectField] ? ' ' + o.selectCssClass : '') +
				(item[o.disabledField] ? ' ' + o.disabledCssClass : '') + '">' + t[sTypes[s]] + '</div>';
		},

		/**
		 * Focus on list element
		 * @param {Number} index Focused item index
		 * @private
		 */
		_focus: function (index) {
			var o = this.options,
				start = o.start,
				end = start + this.calculatedIPP,
				page;

			if (!isNaN(index)) {
				this.isFocused = true;
				index = this._validateIndex(index);

				// scrolling by 1 item
				if (o.scrollSingle) {
					// check if new set of items is needed
					if (index > start + (this.calculatedIPP - 1)) {
						this._calculateIPP(index, false);
						end = index + 1;
						start = end - this.calculatedIPP;
					} else if (index < start) {
						this._calculateIPP(index);
						start = index;
						end = start + this.calculatedIPP;
					}
					// show subtitle with index 0 if needed
					if (index === 1 && o.items[0].subtitle && start === 1) {
						start = 0;
						this._calculateIPP(start);
						end = start + this.calculatedIPP;
					}
				// scrolling by pages
				} else {
					page = Math.floor(index / this.calculatedIPP);
					start = page * this.calculatedIPP;
					end = start + this.calculatedIPP;
				}

				o.index = index;
				this._renderPage(start, end);
				o.start = start;
			}
		},

		/**
		 * Focus on list element
		 * @param {Number} [index] Index of focused item
		 * @example
		 * $("#List").sfTextList("focus", 3);
		 */
		focus: function (index) {
			alert("[sf.ui.TextList] focus()");
			this._focus(typeof index === "undefined" ? this.options.index : index);
			return this;
		},

		/**
		 * Get index of focused element
		 * @returns Focused element index
		 * @example
		 * var index = $("#List").sfTextList("getIndex");
		 * alert(index);
		 */
		getIndex: function () {
			alert("[sf.ui.TextList] getIndex()");

			return this.isFocused ? this.options.index : null;
		},

		/**
		 * Get list item. If no index provided, returns focused element
		 * @param {Number} index Index of item to return
		 * @returns List element
		 * @example
		 * var item = $("#List").sfTextList("getItem");
		 * alert(item.text);
		 */
		getItem: function (index) {
			alert("[sf.ui.TextList] getItem()");
			var o = this.options;

			index = index || o.index;
			return o.items[index];
		},

		/**
		 * Get all selected items
		 * @returns Array of selected items or selected item if radiobox is used
		 * @example
		 * var selected = $("#List").sfTextList("getSelected");
		 * alert("Number of selected items: " + selected.length);
		 */
		getSelected: function () {
			alert("[sf.ui.TextList] getSelected()");
			var o = this.options,
				s = o.selectable,
				data = [],
				iLen = o.items.length,
				item,
				i;

			if (s > 0) {
				for (i = 0; i < iLen; i++) {
					item = o.items[i];
					if (item[o.selectField]) {
						if (s === 1) {
							data.push(item);
						} else if (s === 2) {
							return item;
						}
					}
				}
				return data;
			}
		},

		/**
		 * Get indexes of selected items
		 * @returns Array containing indexes of selected items or index of selected item if radio mode
		 * @example
		 * var indexes = $("#List").sfTextList("getSelectedIds");
		 * alert("Selected indexes: " + indexes);
		 */
		getSelectedIds: function () {
			alert("[sf.ui.TextList] getSelectedIds()");
			var o = this.options,
				s = o.selectable,
				data = [],
				iLen = o.items.length,
				i;

			if (s > 0) {
				for (i = 0; i < iLen; i++) {
					if (o.items[i][o.selectField]) {
						if (s === 1) {
							data.push(i);
						} else if (s === 2) {
							return i;
						}
					}
				}
				return data;
			}
		},

		/**
		 * Calculate focused item top position and displayed list height
		 * @param {Number} index Focused item index
		 * @param {Array} items Displayed list items
		 * @returns Object containing list height and item top position
		 */
		_getStyles: function (index, items) {
			var r = this.resolution,
				t = this.templates,
				subHeight = t.subtitle.height[r],
				lineHeight = t.line.height[r],
				height = 0,
				top = 0,
				iLen,
				i;

			for (i = 0, iLen = items.length; i < iLen; i++) {
				height += items[i].subtitle ? subHeight : this.itemHeight;
				height += lineHeight;
				if (i < index) {
					top = height;
				}
			}

			return {
				top: top,
				height: height
			};
		},

		/**
		 * Initialize component
		 * @private
		 */
		_init: function () {
			alert("[sf.ui.TextList] _init()");
			var o = this.options,
				v = this.view,
				w = this.widget(),
				t = this.templates;

			this._super();

			w.empty();

			/* items validation */
			if (typeof o.items !== "object" || (typeof o.items === "object" && !o.items.length)) {
				o.items = [];
			}

			/* type validation */
			if (typeof t[o.type] === "undefined") {
				o.type = "text";
			}

			/* selectable validation */
			if (isNaN(o.selectable) || o.selectable > 2 || o.selectable < 0) {
				o.selectable = 0;
			}

			/* selectPos validation */
			o.selectPos = o.selectPos === "left" ? "left" : o.selectPos === "right" ? "right" : "left";

			this._pickResolution();
			this.itemHeight = t[o.type].height[this.resolution];
			this.widgetHeight = w.height();
			v.list = $(t.list)
				.addClass(this.baseCssClass + "-list " + this.baseCssClass + "-list-" + o.type)
				.width(w.width());

			w.append(v.list);

			v.focus = $("<img />")
				.attr("src", sf.ui.images + "/ui.textlist/list_highlighted.png")
				.addClass(o.focusCssClass + "-bg")
				.width(w.width())
				.height(this.itemHeight)
				.hide();

			v.cursor = $(t.cursor.tmpl)
				.addClass(this.baseCssClass + "-cursor")
				.height(this.itemHeight - t.cursor.size[this.resolution])
				.width(w.width() - t.cursor.size[this.resolution])
				.hide();

			v.wrap = $("<div></div>")
				.addClass(this.baseCssClass + "-listbox")
				.width(w.width());

			v.list.append(v.focus, v.cursor, v.wrap);

			v.shadow = this._tmpl(t.shadow.tmpl, $.extend({bcc: this.baseCssClass}, this._shadowSize()));

			v.line = this._tmpl(t.line.tmpl, {
				lineClass: this.baseCssClass + "-line",
				height: t.line.height[this.resolution]
			});

			if (o.items.length) {
				this._calculateIPP(o.start);
				this.doRefresh = true;
				this._renderPage();
			} else {
				v.wrap.html("No items");
			}
		},

		/**
		 * Focus on next list element
		 * @example
		 * ...
		 * handleKeyDown: function (keyCode) {
		 *     switch(keyCode) {
		 *         case sf.key.DOWN:
		 *             $("#List").sfTextList("next");
		 *             break;
		 *         ...
		 *     }
		 * }
		 */
		next: function () {
			alert("[sf.ui.TextList] next()");
			this._focus(this.options.index + this.isFocused);
			this._trigger("next");
			return this;
		},

		/**
		 * Pick current application resolution
		 * @private
		 */
		_pickResolution: function () {
			var h = sf.ui.height;
			this.resolution = h === 540 ? 0 : h === 720 ? 1 : h === 1080 ? 2 : 0;
		},

		/**
		 * Focus on previous list element
		 * @example
		 * ...
		 * handleKeyDown: function (keyCode) {
		 *     switch(keyCode) {
		 *         case sf.key.UP:
		 *             $("#List").sfTextList("prev");
		 *             break;
		 *         ...
		 *     }
		 * }
		 */
		prev: function () {
			alert("[sf.ui.TextList] prev()");
			this._focus(this.options.index - this.isFocused);
			this._trigger("prev");
			return this;
		},

		/**
		 * Redraw list items
		 * @private
		 */
		_refresh: function () {
			this.doRefresh = true;
			this._renderPage();
		},

		/**
		 * Redraw list items
		 * @example
		 * $("#List").sfTextList("refresh");
		 */
		refresh: function () {
			alert("[sf.ui.TextList] refresh()");
			this._refresh();
			return this;
		},

		/**
		 * Removes list item on specific position
		 * @param {Number} index index of removed item
		 * @example
		 * $("#List").sfTextList("removeItem", 3);
		 */
		removeItem: function (index) {
			alert("[sf.ui.TextList] removeItem()");
			var o = this.options;

			index = index || o.index;

			if (!isNaN(index) && index > -1 && index < o.items.length) {
				o.items.splice(index, 1);
			}

			this._refresh();

			return this;
		},

		/**
		 * Render list page
		 * @param {Number} [start] First shown element index
		 * @param {Number} [end] Last shown element index
		 * @private
		 */
		_renderPage: function (start, end) {
			var o = this.options,
				style,
				items;

			start = typeof start === "undefined" ? o.start : start;
			end = typeof end === "undefined" ? start + this.calculatedIPP : end;
			items = this._setItems(start, end);

			if (start !== o.start || end !== o.start + this.calculatedIPP || this.doRefresh) {
				!o.scrollSingle && this.view.focus.css("display", "none");
				this._drawItems(items);
				this.doRefresh = false;
			}

			style = this._getStyles(o.index - start, items);
			this._setOffset(style.top, style.height);
			this.isFocused && this._drawFocus(o.index - start, style.top);
			this._trigger("render");
		},

		/**
		 * Returns values of object properties
		 * @param {String} key Object property name
		 * @param {Object} obj Searched object
		 * @private
		 * @returns Object's property value, template or empty string
		 */
		_repl: function (key, obj) {
			var props = key.split(".");

			return typeof obj[props[0]] !== "undefined"
				? typeof obj[props[0]] === "object"
					? typeof obj[props[0]][props[1]] !== "undefined"
						? obj[props[0]][props[1]]
						: ""
					: obj[props[0]]
			 	: props[0] === "tmpl" && typeof this.templates[props[1]] !== "undefined"
					? this._tmpl(this.templates[props[1]].tmpl, obj)
					: "";
		},

		/**
		 * Select all list items
		 * @example
		 * $("#List").sfTextList("selectAll");
		 */
		selectAll: function () {
			alert("[sf.ui.TextList] selectAll()");
			var o = this.options,
				iLen = o.items.length,
				item,
				i;

			if (o.selectable === 1) {
				for (i = 0; i < iLen; i++) {
					item = o.items[i];
					if (!item[o.disabledField] && !item[o.selectField]) {
						item[o.selectField] = true;
						this._changeState(i);
					}
				}
			}

			return this;
		},

		/**
		 * Select a list item
		 * @param {Number} [index] Item index
		 * @private
		 */
		_selectItem: function (index) {
			var o = this.options,
				s = o.selectable,
				item;

			index = index || o.index;
			item = o.items[index];

			if (item && !item[o.disabledField] && !item[o.selectField] && s > 0) {
				s === 2 && this._unselectAll();
				item[o.selectField] = true;
				this._changeState(index);
			}
		},

		/**
		 * Select list item
		 * @param {Number} [index] Item index
		 * @example
		 * $("#List").sfTextList("selectItem", 3);
		 */
		selectItem: function (index) {
			alert("[sf.ui.TextList] selectItem()");
			this._selectItem(index);
			return this;
		},

		/**
		 * Prepare set of items to show
		 * @param {Number} start First shown element index
		 * @param {Number} end Last shown element index
		 * @private
		 */
		_setItems: function (start, end) {
			return this.options.items.slice(start, end);
		},

		/**
		 * Set list alignment to top or bottom of widget
		 * @param {Number} top Focused item top position
		 * @param {Number} height List height
		 */
		_setOffset: function (top, height) {
			var wHeight = this.widgetHeight,
				oldOffset = this.offset,
				v = this.view,
				css = [
					{"top": 0, "bottom": ""},
					{"top": "", "bottom": 0}
				];

			this.offset = (!this.offset && top + this.itemHeight > wHeight) ? 1 :
				(this.offset && (wHeight - height + top < 0)) ? 0 :
					(this.offset && height < wHeight) ? 0 : oldOffset;

			oldOffset !== this.offset && v.list.css(css[this.offset]);
		},

		/**
		 * Calculates shadow size
		 * @private
		 * @returns Object describing thumbnail shadow
		 */
		_shadowSize: function () {
			var o = this.options,
				t = this.templates,
				r = this.resolution,
				tSize = t[o.type].thumb ? t[o.type].thumb[r] : 0,
				border = r === 2 ? 2 : 1,
				s = t.shadow;

			return {
				shadowSize: tSize + 2 * (s.size[r] - s.offset[r]) - 2 * border,
				innerSize: tSize - 2 * s.offset[r]  - 2 * border,
				shadowOffset: s.size[r] - s.offset[r],
				bcc: this.baseCssClass
			};
		},

		/**
		 * Calculate size for thumbnail
		 * @param {Object} thumb Thumbnail to tyle
		 * @returns Thumbnail style
		 * @private
		 */
		_thumbSize: function (thumb) {
			var o = this.options,
				t = this.templates,
				tSize = t[o.type].thumb ? t[o.type].thumb[this.resolution] : 0,
				width,
				height,
				top = 0,
				left = 0,
				ratio;

			if (thumb && thumb.width && thumb.height) {
				ratio = thumb.width / thumb.height;
				if (ratio < 1) {
					width = tSize;
					height = Math.round(width / ratio);
					top = Math.round((tSize - height) / 2);
				} else {
					height = tSize;
					width = Math.round(height * ratio);
					left = Math.round((tSize - width) / 2);
				}
				return "width: " + width + "px; height: " + height +"px;" +
					"top: " + top + "px; left: " + left + "px;";
			} else {
				return this.resolution === 2 ? "top: -2px; left: -2px;" : "top: -1px; left: -1px;";
			}
		},

		/**
		 * Replace markup with object properties
		 * @param {String} tmpl Template markup
		 * @param {Object} obj Searched object
		 * @private
		 * @returns Parsed string with object values
		 */
		_tmpl: function (tmpl, obj) {
			var that = this;

			return typeof tmpl === "string" && typeof obj === "object" ?
				tmpl.replace(/\$\{([a-zA-Z0-9_\.]*)\}/g, function (m, key) {
					return that._repl(key, obj);
				}) : "";
		},

		/**
		 * Toggle select a list item
		 * @param {Number} [index] Item index
		 * @private
		 */
		_toggleItem: function (index) {
			var o = this.options,
				s = o.selectable,
				item;

			index = index || o.index;
			item = o.items[index];

			if (s === 1 && item && !item[o.disabledField]) {
				item[o.selectField] = !item[o.selectField];
				this._changeState(index);
			}
		},

		/**
		 * Toggle select a list item
		 * @param {Number} [index] Item index
		 * @example
		 * $("#List").sfTextList("toggleItem", 3);
		 */
		toggleItem: function (index) {
			alert("[sf.ui.TextList] toggleItem()");
			var o = this.options,
				s = o.selectable;

			if (s === 1) {
				this._toggleItem(index);
			} else if (s === 2) {
				this._selectItem(index);
			}

			return this;
		},

		/**
		 * Unselect all list items
		 * @private
		 */
		_unselectAll: function () {
			var o = this.options,
				v = this.view,
				iLen = o.items.length,
				item,
				i;

			for (i = 0; i < iLen; i++) {
				item = o.items[i];
				if (!item[o.disabledField] && item[o.selectField]) {
					item[o.selectField] = false;
				}
			}
			v.items.children("div:not('." + o.disabledCssClass + "')").removeClass(o.selectCssClass);
		},

		/**
		 * Unselect all list items
		 * @example
		 * $("#List").sfTextList("unselectAll");
		 */
		unselectAll: function () {
			alert("[sf.ui.TextList] unselectAll()");
			this._unselectAll();
			return this;
		},

		/**
		 * Unselect list item
		 * @param {Number} [index] Item index
		 * @example
		 * $("#List").sfTextList("unselectItem", 3);
		 */
		unselectItem: function (index) {
			alert("[sf.ui.TextList] unselectItem()");
			var o = this.options,
				s = o.selectable,
				item;

			index = index || o.index;
			item = o.items[index];

			if (s === 1 && item && !item[o.disabledField] && item[o.selectField]) {
				item[o.selectField] = false;
				this._changeState(index);
			}

			return this;
		},

		/**
		 * Validate focus index
		 * @param {Number} index Focus index
		 * @returns Validated index
		 * @private
		 */
		_validateIndex: function (index) {
			var o = this.options,
				max = o.items.length - 1;

			index = o.loop ? (index < 0 ? max : index > max ? 0 : index) :
				(index < 0 ? 0 : index > max ? max : index);

			if (o.items[index].subtitle) {
				if ((o.index === max && index === 0) || index >= o.index) {
					return this._validateIndex(index + 1);
				} else {
					return this._validateIndex(index - 1);
				}
			}

			return index;
		}
	});

	sf.ui.bridge(widgetName, sf.ui.TextList);

	sf.ui.addSelector(widgetName);

}(jQuery));
/**
 * Apps Framework UI Text List Component
 *
 * @author  Pawel Pruszkowski <<a href='mailto:p.pruszkowsk@samsung.com'>p.pruszkowsk@samsung.com</a>>
 * @date    17.11.2011
 *
 *
 *****************************************************************************
 *
 * Copyright (c) YYYY Samsung Electronics, Visual Display Division.
 * All Rights Reserved.
 *
 ****************************************************************************/
/*jslint white: true, onevar: true, undef: true, nomen: false, eqeqeq: true, plusplus: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global jQuery:true, alert: true*/

var sf = sf || {};
sf.ui = sf.ui || {};

(function ($) {

	var widgetName = "sfWizard";

	/**
	 * @class
	 * Creates a graphical representation of a discrete progress bar, where each step represents a scene.
	 * User may move forward/backward, as well as jump to an arbitrary step. Child scenes are not bound to
	 * the component in any way, that is, sfWizard <b>does not</b> check if they even exist.
	 * @example
	 * <b>Example 1</b>
	 * <u>Initialization example:</u>
	 * <code>$("#wizard").sfWizard({
	 *   scenes: ["Scene1", "Scene2", "Scene3"]
	 * });</code>
	 * <b>Example 2</b>
	 * <u>Initialization example with more options:</u>
	 * <code>$("#wizard").sfWizard({
	 *   scenes: ["Scene1", "Scene2", "Scene3"],
	 *   widthInUnits: 6
	 * });</code>
	 * <b>Example 3</b>
	 * <u>Moving one step forward:</u>
	 * <code>$("#wizard").sfWizard('next');</code>
	 * <b>Example 4</b>
	 * <u>Moving to an arbitrary step:</u>
	 * <code>$("#wizard").sfWizard('setStep', 3);</code>
	 * <b>Note:</b> steps are numbered from 0 (zero).
	 * @auguments sf.ui.Widget
	 */
	sf.ui.Wizard = sf.ui.widgetFactory({
		/**
		 * @lends sf.ui.Wizard.prototype
		 */

		/**
		 * Widget name.
		 * @default 'sfWizard'
		 * @type String
		 */
		widgetName: widgetName,

		/**
		 * Widget base CSS class.
		 * @default 'sf-ui-wizard'
		 * @type String
		 * @private
		 */
		baseCssClass: "sf-ui-wizard",

		/**
		 * Event namespace.
		 * @default "sfwizardui"
		 * @type String
		 */
		eventNamespace: "sfwizard",

		/**
		 * Application resolution.
		 * 0: 540p, 1: 720p, 2: 1080p
		 * @type Number
		 * @private
		 */
		resolution: 0,

		/**
		 * Current step. Internal index.
		 * @type Number
		 * @private
		 */
		currentStep: 0,

		/**
		 * Calculated width of central step element.
		 * @type Number
		 * @private
		 */
		calculatedCW: -1,

		/**
		 * Widget width.
		 * @type Number
		 * @private
		 */
		wWidth: null,

		/**
		 * Step states.
		 * @type Array
		 * @private
		 */
		states: ['', '-dim', '-highlighted'],

		/**
		 * Widget options.
		 * @description
		 * Allowed items:
		 * <ul>
		 * <li> <b>scenes:</b> array of scene names. Component does not check if they exist at all, but will not run if this option is omitted or falsy. </li>
		 * <li> <b>width:</b> wizard width in pixels, regardless of screen resolution. </li>
		 * <li> <b>widthInUnits:</b> wizard width in screen-resolution-dependant units. Valid values are in 1-16 range. A unit is screen width divided by 16. </li>
		 * </ul>
		 * @public
		 * @type Object
		 */
		options: {
			scenes: [],			// array of scene identifiers
			width: null,
			widthInUnits: null
		},

		/**
		 * Templates object. Contains templates for elements and default widths of some visible chunks.<br>
		 * <u>wizard</u> template contains keys:
		 * <ul>
		 * <li> <b>total:</b> total default width of all steps, spaces, margins etc.</li>
		 * </ul>
		 * <u>step</u> template contains keys:
		 * <ul>
		 * <li> <b>tip:</b> width of steps' tip.</li>
		 * <li> <b>space:</b> width of space between two steps.</li>
		 * <li> <b>tmpl:</b> string that defines the template itself, along with variables that ae used inside: <code>step</code> and <code>type</code>.</li>
		 * </ul>
		 * @type Object
		 * @private
		 */
		templates: {
			wizard: {
				total: [856, 1138, 1712]
			},
			step: {
				tip: [10, 14, 20],
				space: [9, 12, 18],
				tmpl: '<div id="sf-ui-wizard-step${step}">' +
					'<div class="sf-ui-wizard-left${type}"></div>' +
					'<div class="sf-ui-wizard-center${type}"></div>' +
					'<div class="sf-ui-wizard-right${type}"></div>'+
				'</div>'
			}
		},

		/**
		 * Initialize component.
		 * Method builds HTML code of the wizard using template,
		 * sets width of the component and calls method that renders steps.
		 * @private
		 */
		_init: function () {
			alert("[sf.ui.Wizard] _init()");

			var o = this.options,
				v = this.view,
				w = this.widget(),
				t = this.templates,
				i = 0,
				type = this.states[2],
				steps = [];

			this._super();

			if (!o.scenes || o.scenes.length < 1) {
				alert("[sf.ui.Wizard] ERROR: no scenes defined for Wizard. Exiting.");
				return false;
			}

			for (i; i < o.scenes.length; i++) {
				if (i >= 1) {
					type = this.states[1];
				}
				steps.push($.tmpl(t.step.tmpl, {step: i+1, type: type}));
			}
			// appending steps directly to widget, but outside the loop.
			w.append.apply(w, steps);

			this._pickResolution();

			if (parseInt(o.widthInUnits, 10) > 0 && o.widthInUnits <= 16) {
				this.wWidth = o.widthInUnits * sf.ui.grid;
				$('.sf-ui-wizard').css('padding', 0);
			} else if (parseInt(o.width, 10) > 0) {
				this.wWidth = o.width;
				$('.sf-ui-wizard').css('padding', 0);
			} else {
				this.wWidth = t.wizard.total[this.resolution];
			}
			w.width(this.wWidth);

			this._renderSteps();
		},

		/**
		 * Destroy component.
		 * @private
		 */
		_destroy: function () {
			alert("[sf.ui.Wizard] _destroy()");
			this.widget().empty();
		},

		/**
		 * Pick current application resolution.
		 * @private
		 */
		_pickResolution: function () {
			var h = sf.ui.height;

			this.resolution = h === 540 ? 0 : h === 720 ? 1 : h === 1080 ? 2 : 0;
		},

		/**
		 * Render wizard.
		 * @private
		 */
		_renderSteps: function () {
			var o = this.options,
				v = this.view,
				t = this.templates,
				space = t.step.space[this.resolution],
				tip = t.step.tip[this.resolution],
				num = o.scenes.length,
				i = 0;

			this.calculatedCW = Math.floor((this.wWidth - (num - 1) * space - 2 * num * tip) / num);
			for (i = 0; i < this.states.length; i++) {
				$('.' + this.baseCssClass + '-center' + this.states[i]).width(this.calculatedCW);
			}
		},

		/**
		 * Sets state of step.
		 * @private
		 */
		_setState: function (step, state) {
			var ref = this;
			if (!step) {
				alert("[sf.ui.Wizard] _setState: ERROR: step is falsy");
				return false;
			}
			if (!state) {
				state = '';
			}
			step.children().eq(0)
				.removeClass()
				.addClass(ref.baseCssClass + '-left' + state);
			step.children().eq(1)
				.removeClass()
				.addClass(ref.baseCssClass + '-center' + state);
			step.children().eq(2)
				.removeClass()
				.addClass(ref.baseCssClass + '-right' + state);
		},

		/**
		 * Hides all bound scenes and shows the current one,
		 * according to the internal counter (<code>this.currentStep</code>).
		 * @private
		 */
		_showCurrentScene: function () {
			var ref = this;
			$.each(ref.options.scenes, function (index) {
				sf.scene.hide(ref.options.scenes[index]);
			});
			sf.scene.show(ref.options.scenes[this.currentStep]);
			sf.scene.focus(ref.options.scenes[this.currentStep]);
		},

		/**
		 * Moves wizard to an arbitrary step. Steps are numbered from 0.
		 * @public
		 */
		setStep: function (number) {
			var divs = $('.' + this.baseCssClass + ' > div'),
				ref = this;
			if (number < 0 || number >= divs.length) {
				return false;
			}
			this.currentStep = number;
			divs.each(function (index) {
				if (index < number) {
					ref._setState($(this));
				} else if (index == number) {
					ref._setState($(this), ref.states[2]);
				} else {
					ref._setState($(this), ref.states[1]);
				}
			});
			this._showCurrentScene();
			return this;
		},

		/**
		 * Returns current step number.
		 */
		getStep: function () {
		    return this.currentStep;
		},

		/**
		 * Starts the wizard, which effectively means setting step to zero.
		 * @public
		 */
		start: function () {
			this.setStep(0);
			return this;
		},

		/**
		 * Moves wizard to next step and shows connected scene.
		 * @public
		 */
		next: function () {
			if (this.currentStep >= this.options.scenes.length - 1) {
				return false;
			}
			this.setStep(++this.currentStep);
			return this;
		},

		/**
		 * Moves wizard to previous step and shows connected scene.
		 * @public
		 */
		prev: function () {
			if (this.currentStep === 0) {
				return false;
			}
			this.setStep(--this.currentStep);
			return this;
		},

		/**
		 * Shows the wizard. Does not do anything with connected scenes.
		 * @public
		 */
		show: function () {
			this.widget().show();
			return this;
		},

		/**
		 * Hides the wizard. Does not do anything with connected scenes.
		 * @public
		 */
		hide: function () {
			this.widget().hide();
			return this;
		}
	});
	sf.ui.bridge(widgetName, sf.ui.Wizard);
	sf.ui.addSelector(widgetName);
}(jQuery));
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    var widgetName = "sfCategory";

    sf.ui.Category = sf.ui.widgetFactory({
	/**
	* Widget name
	* @default 'sfCategory'
	* @type String
	*/
	widgetName: widgetName,

	/**
	 * Widget base CSS class
	 * @default 'sf-ui-category'
	 * @type String
	 */
	baseCssClass: "sf-ui-category",

	eventNamespace: "sfcategory",

	/*
	 * @title title of category list. if the 'vertical' option is false, this property is ignored.
	 * @expandable whether expandable the list or not
	 * @vertical whether the list shows vertically or horizontally. (default : true)
	 *
	 **/

	selectedIndex: 0,
	index: 0,
	itemsOnPage: [5, 8],
	page: 0,
	imgSelected: "/ui.titlebar/titlebar_glow.png",
	imgHighlighted: "/ui.category/cate_vertical_highlighted.png",
	type: 0,
	highlightWidth : [60, 80, 120],
	highlightHeight: [45, 60, 90],
	sepWidth: [90, 120, 180],
	resolution: 0,

	options: {
	    items : [],
	    title : 'News Service',
	    focusIndex : 0,
	    selectedIndex: null,
	    expandable : true,
	    vertical : true,
	    width: 300
	},
	/*

	 **/

	templates: {
	    main: $.template(null,
		'{{if vertical}}'+
		'<div class="sf-ui-category-vertical-container" style="width: ${width}px;">'+
		'<ul class="sf-ui-category-vertical">'+
		'{{tmpl(items) "sf_category_item_v"}}'+
		'</ul>'+
		'</div>'+

		'{{else}}'+
		'<div class="sf-ui-category-horizontal-container">'+
		'<div class="sf-ui-category-horizontal-title">${title}</div>'+
		'<ul class="sf-ui-category-horizontal">'+
		'{{tmpl(items) "sf_category_item_h"}}'+
		'</ul>'+
		'</div>'+

		'{{/if}}'+


		'<div id="sf-ui-category-vertical-highlited-bg">'+
		'<img src="${imgHighlighted}" width="80" height="80" />'+
		'</div>'+
		'<div id="sf-ui-category-vertical-selected-bg">'+
		'<img src="${imgSelected}" width="80" height="80" />'+
		'</div>'+
		'<div id="sf-ui-category-vertical-highlited-txt"></div>'+
		'<div id="sf-ui-category-vertical-selected-txt"></div>'),

	    itemHorizontal: $.template('sf_category_item_h', '<li class="sf-ui-category-horizontal-elem"><a href="javascript:void();">${text}</a></li>'),
	    itemVertical: $.template('sf_category_item_v', '<li class="sf-ui-category-vertical-elem">'+
		'<div class="sf-ui-category-vertical-label"><a>${text}</a></div>'+
		'<br clear="all" />'+
		'<div class="sf-ui-category-vertical-sep-ct" style="width: ${sepWidth}px;"></div>'+
		'<div class="sf-ui-category-vertical-sep-rt"></div>'+
		'</li>')
	},



	_create: function() {
	    alert("[sf.Category] _init()");
	    this._super();

	    this.view.mainTemplate = this.templates.main;
	    if (this.options.vertical) this.type = 1;
	    this._redraw();
	},

	_init: function () {
	    var v = this.view;
	    v.list = [];
	    this._pickResolution();

	    this._reloadList();
	},

	_destroy: function () {
	    this.view.list.remove();
	},

	_pickResolution: function () {
	    var h = sf.ui.height;

	    this.resolution = h === 540 ? 0 : h === 720 ? 1 : h === 1080 ? 2 : 0;
	},

	_setOption: function (name, value) {
	    var o = this.options,
	    v = this.view;


	    switch (name) {
		case "items":
		    break;
		case "title":
		    this.options.title = value;
		    break;
		case "expandable":
		    break;
		case "focusIndex":
		    $(".sf-ui-category-horizontal").children().removeClass("selected");


		    if (o.focusIndex == null || typeof(o.focusIndex) != "number" || o.focusIndex < 0 || o.focusIndex >= this.view.list.length) {
			o.focusIndex = 0;
		    }
		    v.list[focusIndex].addClass("");
		    break;
		case "selectedIndex":
		    break;
		case "vertical":
		    if (typeof(value) == "boolean") {
			o.vertical = value;
			if (o.vertical) this.type = 1;
			else this.type = 0;
			this.page = 0;
			o.focusIndex = 0;
		    }
		    break;
		case "width":
		    if ((typeof(value) == "number") && (value > 0)) {
			o.width = value;
		    }
		    break;

	    };
	    this._redraw();
	    this._reloadList();

	    if (o.selectedIndex != null) this._drawSelect();
	    this.focus();
	},

	_redraw: function () {
	    //to do zmienić slice na petle i dodawac do items szerokosc
	    var o = this.options,
	    v = this.view;

	    //var data = o;
	    var data = new Object();
	    data.title = o.title;

	    data.focusIndex = o.focusIndex;
	    data.selectedIndex = o.selectedIndex;
	    //data.expandable = this.expandable;
	    data.vertical = o.vertical;
	    data.imgSelected = sf.ui.images + this.imgSelected;
	    data.imgHighlighted =  sf.ui.images + this.imgHighlighted;
	    data.width = o.width;
	    var sepWidth = o.width - this.sepWidth[this.resolution];

	    var no = this.itemsOnPage[this.type],
	    page = this.page;

	    data.items = o.items.slice(page * no, (page + 1) * no);
	    for (item in data.items) {
		data.items[item].sepWidth = sepWidth;
	    }

	    this.view.mainTemplate = $.tmpl(this.templates.main, data);
	    this.widget().html(v.mainTemplate);

	},

	_reloadList: function() {

	    var v = this.view,
	    o = this.options;

	    v.list = [];

	    if (o.vertical) {
		$('.sf-ui-category-vertical').children().each(function() {
		   v.list.push($(this));
		});
	    }
	    else {

		$('.sf-ui-category-horizontal').children().each(function() {
		    v.list.push($(this));
		});
	    }

	},

	addItem: function(item) {
	    var o = this.options;

	    o.items.push({
		"text": item
	    });

	    this._redraw();
	    this._reloadList();
	    this.focus();
	    this._drawSelect();
	},

	removeItem: function(index) {
	    var items = this.options.items,
	    o = this.options,
	    v = this.view;

	    items.splice(index, 1);
	    if (o.focusIndex > index) {
		o.focusIndex -= 1;
	    }
	    else {
		if (o.focusIndex == index && index > 0)
		    o.focusIndex -= 1
	    }

	    if (index == o.selectedIndex) {
		o.selectedIndex = null;
	    }
	    else{

		if (index < o.selectedIndex) {
		    o.selectedIndex -= 1;
		}
	    }
	    this._redraw();
	    this._reloadList();
	    this.focus();
	    this._drawSelect();

	},

	nextItem: function() {
	    var o = this.options;

	    if (o.vertical) {
		this.removeHighlightVertical();
	    }
	    else {
		var ind = o.focusIndex % this.itemsOnPage[this.type];
		this.view.list[ind].removeClass("focused");
	    }

	    if (o.focusIndex < o.items.length-1) {
		o.focusIndex++;
	    } else if(o.focusIndex == o.items.length-1) {
		o.focusIndex = 0;
	    }

	    if (o.focusIndex == 0) {
		this.page = 0;

		this._redraw();
		this._reloadList();
		this._drawSelect();

	    }
	    if (o.focusIndex == ((this.page+1) * this.itemsOnPage[this.type])) {
		this.page += 1;

		this._redraw();
		this._reloadList();

		this._drawSelect();
	    }

	    this.focus();

	},


	previousItem: function() {

	    var o = this.options;
	    var toRedraw = false;

	    if (o.vertical) {
		this.removeHighlightVertical();
	    }
	    else {
		var ind = o.focusIndex % this.itemsOnPage[this.type];
		this.view.list[ind].removeClass("focused");
	    }

	    if (o.focusIndex == 0) {
		toRedraw = true;
		this.page = Math.ceil(this.options.items.length / this.itemsOnPage[this.type]) - 1;
	    }
	    else {
		if (o.focusIndex == ((this.page) * this.itemsOnPage[this.type])) {
		    toRedraw = true;
		    this.page -= 1;
		}
	    }

	    if (o.focusIndex > 0) {
		o.focusIndex--;
	    }
	    else if(o.focusIndex == 0) {
		o.focusIndex = o.items.length-1;
	    }

	    if (toRedraw) {
		this._redraw();
		this._reloadList();
		this._drawSelect();
	    }

	    this.focus();

	},

	select: function() {
	    var o = this.options;

	    o.selectedIndex = o.focusIndex;
	    this._drawSelect();

	},

	_drawSelect: function() {
	    var o = this.options;
	    var v = this.view;

	    if (o.selectedIndex == null) return;
	    if (o.selectedIndex < (this.page * this.itemsOnPage[this.type])) return;
	    if (o.selectedIndex >= ((this.page+1) * this.itemsOnPage[this.type])) return;

	    if (o.vertical) {
		this.removeSelectVertical();
		this.setSelectVertical();
	    }
	    else {
		$(".sf-ui-category-horizontal").children().removeClass("selected");
		v.list[o.selectedIndex % this.itemsOnPage[this.type]].addClass("selected");
	    }

	},

	clear: function() {
	    $(".sf-ui-category-horizontal").children().removeClass("selected");
	    this.options.selectedIndex = null;
	    this._redraw();
	    this._reloadList();
	    this.focus();
	},


	removeHighlightVertical: function() {
	    $('#sf-ui-category-vertical-highlited-bg').css('top', '-1000px').css('left', '-2000px').css('width', '0px').css('height', '0px').css('visibility', 'hidden');
	    $('#sf-ui-category-vertical-highlited-txt').css('top', '-1000px').css('left', '-2000px').css('width', '0px').css('height', '0px').css('visibility', 'hidden');
	    $('#sf-ui-category-vertical-highlited-txt').html('');
	},

	setHighlightVertical: function() {
	    var index = this.options.focusIndex % this.itemsOnPage[this.type];

	    var elem = $(this.view.list[index]);
	    var out = elem.offset();
	    var txt = elem.text();
	    var w = elem.width();
	    var h = elem.height();

	    var hw = this.highlightWidth[this.resolution];
	    var hh = this.highlightHeight[this.resolution];

	    $('#sf-ui-category-vertical-highlited-bg').css('top', out.top).css('left', out.left).css('width', w+'px').css('height', h+'px').css('visibility', 'visible');
	    $('#sf-ui-category-vertical-highlited-bg img').css('width', w+'px').css('height', h+'px');
	    $('#sf-ui-category-vertical-highlited-txt').css('top', parseInt(out.top+1, 10)).css('left', out.left).css('width', parseInt(w-hw, 10)+'px').css('height', parseInt(h-hh, 10)+'px').css('visibility', 'visible');
	    $('#sf-ui-category-vertical-highlited-txt').html(txt);
	},


	setSelectVertical: function() {
	    var elem = $(this.view.list[this.options.selectedIndex % this.itemsOnPage[this.type]]);
	    var out = elem.offset();
	    var txt = elem.text();
	    var w = elem.width();
	    var h = elem.height();
	    var hw = this.highlightWidth[this.resolution];
	    var hh = this.highlightHeight[this.resolution];
	    $('#sf-ui-category-vertical-selected-bg').css('top', out.top).css('left', out.left).css('width', w+'px').css('height', h+'px').css('visibility', 'visible');
	    $('#sf-ui-category-vertical-selected-bg img').css('width', w+'px').css('height', h+'px');
	    $('#sf-ui-category-vertical-selected-txt').css('top', parseInt(out.top+1, 10)).css('left', out.left).css('width', parseInt(w-hw, 10)+'px').css('height', parseInt(h-hh, 10)+'px').css('visibility', 'visible');
	    $('#sf-ui-category-vertical-selected-txt').html(txt);
	},

	removeSelectVertical: function() {
	    $('#sf-ui-category-vertical-selected-bg').css('top', '-1000px').css('left', '-2000px').css('width', '0px').css('height', '0px').css('visibility', 'hidden');
	    $('#sf-ui-category-vertical-selected-txt').css('top', '-1000px').css('left', '-2000px').css('width', '0px').css('height', '0px').css('visibility', 'hidden');
	    $('#sf-ui-category-vertical-selected-txt').html('');
	},

	focus: function() {
	    var o = this.options,
	    v = this.view;


	    var ind = o.focusIndex % this.itemsOnPage[this.type];

	    if (ind < 0) return;
	    if (ind >= v.list.length) return;

	    if (o.vertical) {
		this.setHighlightVertical();
	    }
	    else {
		v.list[ind].addClass("focused");
	    }
	},

	getSelectedIndex: function() {
	    return this.options.selectedIndex;
	},

	getFocusedIndex: function() {
	    return this.options.focusIndex;
	}

    });

    sf.ui.bridge(widgetName, sf.ui.Category);
    sf.ui.addSelector(widgetName);

})(jQuery);sf.core.init();sf.ui.init();
