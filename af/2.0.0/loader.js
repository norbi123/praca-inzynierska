(function () {
	var bDebugMode = true;
	
	window._AFRootPath = bDebugMode?'af':'$MANAGER_WIDGET/Common/af';
	var files = [];
	files.push(window._AFRootPath+ (bDebugMode?'/2.0.0/deviceapis.js':'/../deviceapis/1.0/deviceapis.js'));
	files.push(window._AFRootPath+'/2.0.0/extlib/jquery.js');
	files.push(window._AFRootPath+'/2.0.0/extlib/jquery.json-2.2.min.js');
	files.push(window._AFRootPath+'/2.0.0/extlib/jquery.maple.patch.js');
	files.push(window._AFRootPath+'/2.0.0/extlib/jquery.tmpl.js');
	//files.push('$MANAGER_WIDGET/Common/IME_XT9/ime.js');
	files.push('$MANAGER_WIDGET/Common/IME/ime2.js');
	files.push(window._AFRootPath+ (bDebugMode?'/2.0.0/sf.js':'/2.0.0/sf.min.js'));
	
	var index = 0;
	
	function loadNext () {
		if(index >= files.length) {
			console.debug("################## LOAD ENDED");
			return;
		}
		var js = files[index++];
		console.debug("################## LOAD: " + js);
		var script = null;
		var head = document.getElementsByTagName('head')[0];
		script = document.createElement('script');
	    script.type = 'text/javascript';
	    script.src = js; //+ '?t='+(new Date()).getTime();
	    script.onload = loadNext;
	    head.appendChild(script);
	}
	
	loadNext();
})();
