(function($){
    if(jQuery._maplePatched) return;
	
	var oriMethod = $.expr.find.CLASS;
	
	$.expr.find.CLASS = function (match, context, isXML) {
		var collection = oriMethod(match, context, isXML);
		if (collection[0]) {
			// match[1] is subset of collection[0].className.
			collection[0].className = collection[0].className;
//			collection[0].className = match[1];
		}
		return collection;
	};

})(jQuery);




/*
 About : Author
 Tomasz Scislo SPRC Poland
 Date : 2011-05-24
 
 In this patch inner jQuery data(elem, name, data) method is redefined in order to handle Maple 5+ defect.
 For all DOM elements data method now uses assignment with setAttribiute instead of extending DOM objects.
 To read information assigned to DOM node data method now uses getAttribiute.
 */
(function(jQuery){
    if(jQuery._maplePatched) return;
    function now(){
        return (new Date()).getTime();
    }
    
    var expando = "jQuery" + now(), uuid = 0, windowData = {}, removeEvent;
    
    removeEvent = document.removeEventListener ?
		function( elem, type, handle ) {
			elem.removeEventListener( type, handle, false );
		} : 
		function( elem, type, handle ) {
			elem.detachEvent( "on" + type, handle );
		};
    
    jQuery.extend({
        cache: {},
        
        expando: expando,
        
        // The following elements throw uncatchable exceptions if you
        // attempt to add expando properties to them.
        noData: {
            "embed": true,
            "object": true,
            "applet": true
        },
        
        /*
         * Function : data
         *
         * Patched $.data method
         *
         */
        data: function(elem, name, data){
        	var id;
            if (elem.nodeName && jQuery.noData[elem.nodeName.toLowerCase()]) {
                return;
            }
            
            elem = elem == window ? windowData : elem;
            
            // orginal jQuery version
            // var id = elem[ expando ], cache = jQuery.cache, thisCache;
            
            // patch
            // for DOM object use Attributes
            if (typeof elem.getAttribute == "function") 
                id = elem.getAttribute(expando);
            // for other use standard extending approach
            else {
                id = elem[expando];
            }
            
            var cache = jQuery.cache, thisCache;
            
            if (!id && typeof name === "string" && data === undefined) {
                return null;
            }
            
            // Compute a unique ID for the element
            if (!id) {
                id = ++uuid;
            }
            
            // Avoid generating a new cache unless none exists and we
            // want to manipulate it.
            if (typeof name === "object") {
                // orginal jQuery version
                // elem[expando] = id;
                // t.scislo patch
                // for other use standard extending approach
                if (typeof elem.setAttribute == "function") 
                    elem.setAttribute(expando, id);
                // for other use standard extending approach
                else {
                    elem[expando] = id;
                }
                thisCache = cache[id] = jQuery.extend(true, {}, name);
                
            } else if (!cache[id]) {
                // orginal jQuery version
                // elem[expando] = id;
                // t.scislo patch
                // for other use standard extending approach
                if (typeof elem.setAttribute == "function") 
                    elem.setAttribute(expando, id);
                // for other use standard extending approach
                else {
                    elem[expando] = id;
                }
                
                cache[id] = {};
            }
            
            thisCache = cache[id];
            
            // Prevent overriding the named cache with undefined values
            if (data !== undefined) {
                thisCache[name] = data;
            }
            
            return typeof name === "string" ? thisCache[name] : thisCache;
        },
        
        /*
         * Patched $.cleanData method
         */
        cleanData: function( elems ) {        	
    		var data, id, cache = jQuery.cache,
    			special = jQuery.event.special,
    			deleteExpando = jQuery.support.deleteExpando;
    			
    		for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {
    			
    			// orginal jQuery version
    			// id = elem[ jQuery.expando ];
    			
    			// g.pabian patch
    			id = elem[jQuery.expando] || elem.getAttribute(jQuery.expando);    			
    			
    			if ( id ) {
    				data = cache[ id ];
    				
    				if (data && data.events ) {
    					for ( var type in data.events ) {
    						if ( special[ type ] ) {
    							jQuery.event.remove( elem, type );

    						} else {
    							removeEvent( elem, type, data.handle );
    						}
    					}
    				}
    				
    				if ( deleteExpando ) {
    					delete elem[ jQuery.expando ];

    				} else if ( elem.removeAttribute ) {
    					elem.removeAttribute( jQuery.expando );
    				}
    				
    				delete cache[ id ];
    			}
    		}
    	},
        
    	/*
         * Patched $.removeData method
         */
        removeData: function(elem, name) {
            if (elem.nodeName && jQuery.noData[elem.nodeName.toLowerCase()]) {
                return;
            }
            
            elem = elem == window ? windowData : elem;
            
            // orginal jQuery version
            // var id = elem[expando], cache = jQuery.cache, thisCache = cache[id];
            
            // g.pabian patch
            var id = elem[expando] || elem.getAttribute(expando), cache = jQuery.cache, thisCache = cache[id];
            
            // If we want to remove a specific section of the element's data
            if (name) {
                if (thisCache) {
                    // Remove the section of cache data
                    delete thisCache[name];
                    
                    // If we've removed all the data, remove the element's cache
                    if (jQuery.isEmptyObject(thisCache)) {
                        jQuery.removeData(elem);
                    }
                }
                
                // Otherwise, we want to remove all of the element's data
            } else {
                if (jQuery.support.deleteExpando) {
                    delete elem[jQuery.expando];
                    
                } else if (elem.removeAttribute) {
                    elem.removeAttribute(jQuery.expando);
                }
                
                // Completely remove the data cache
                delete cache[id];
            }
        }
    });
})(jQuery);
jQuery._maplePatched = true;